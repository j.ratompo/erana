
class Ground{

	constructor() {

		this.isVisible=true;

		this.position=new Position(this);
		
		this.img=new GameImage(this);


	}
	
	
	// INIT

	init(gameConfig){
		
		this.gameConfig=gameConfig;

		this.position.center=this.imageConfig._2D.center;
		
		this.getGameImage().init(gameConfig);
	
	}
	
	// METHODS
	getGameImage(){
		return this.img;
	}
	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	
	draw(ctx,camera,lightsManager){
		this.getGameImage().draw(ctx,camera,lightsManager);
	}

}
