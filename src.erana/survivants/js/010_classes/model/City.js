class City{

	constructor() {

		this.isVisible=true;
		
		this.weather=null;
		this.grounds=null;
		this.buildings=null;
		this.characters=null;
	}

	// INIT

	// METHODS
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	
	getDrawables(){
		return[this.grounds,this.buildings,this.characters];
	}
	
}
