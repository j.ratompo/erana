class SurvivantsScreen extends GameScreen2D {

	constructor(mainId,config) {
		super(mainId,config,"survivants-saved");
		
		this.chosenCharacterName=null;
		this.chosenCountryName=null;

		
//		// TESTBOT TRACKING
//		if(testbot){
// //		testbot.tag(this,"initOnStartScreenAtLevelStart");
// //		testbot.tag(this,"placeSelectedCharacter");
// //		testbot.tag(this,"getLevelName");
// //		testbot.tag(this,"getSelectedCharacter");
//			
//			testbot.tag(this,"chooseCharacter");
//		}
		
		
	}
	
	// INIT ON START

	initOnStartScreenAtLevelStart() {

		super.initOnStartScreenAtLevelStart();
		
		
		const rootContainer=this.getSubclassRootContainer();
		
		// TRACE
		console.log("initOnStartScreenAtLevelStart SurvivantsScreen",rootContainer);
		
		
		// TODO : FIXME : This should be not necessary...!
//		// We re-place once more the user character at the center of the scene. 
//		this.placeSelectedCharacter();
		
	}

	
	//=============== MODEL METHODS ===============
	
	
//	/*private*/placeSelectedCharacter(){
//		
//		// (...because all characters were placed randomly by a previous sceneManager.placeAllOnInit(...) method call.)
//		var selectedCharacter=this.getCharacterByName(this.chosenCharacterName);
//		if(selectedCharacter){
//			selectedCharacter.position.setLocation({x:0,y:0,z:0});
//		}
//		
//	}
	
	/*protected*/getSubclassRootContainer(){
		return this.currentLevel.city;
	}
	
	
	// FACULTATIVE PUBLIC METHODS
	isPreviousGameDetected() {
		return this.currentLevel!=null;
	}


	// METHODS
	createCharacter(prototypeName){
		return super.createInstance("Character",prototypeName);
	}


	// FACULTATIVE PUBLIC METHODS
	getCharacterByName(characterName){
		
		var prototypeName=PROTO_PREFIX+characterName;
		
		var foundCharacter=foreach(this.getSubclassRootContainer().characters,(character)=>{
			if(character.prototypeName!==prototypeName)	return "continue";
			return character;
		});
		
		return foundCharacter;
	}
	
	
	addCharacter(character){
		this.getSubclassRootContainer().characters.push(character);
	}
	
	
	/*private*/getCurrentBackgroundsToOverride(){
		
		let results=[];
		
		let rootContainer=this.getSubclassRootContainer();
		const gameConfig=this.gameConfig;
		this.addToBackgrounds(gameConfig,results,rootContainer,0);
		if(!this.currentLevel.isCinematic){
			this.addToBackgrounds(gameConfig,results,rootContainer.weather,-1);
		}
		
		return results;
	}

	
	
	// =============== CONTROLLER METHODS ===============
	
	// ACTIONS (for each page)

	//homePage
	startNewGame() {	}
	continueGame() {	}
	startCredits() {	}

	//gamePage
	resumeGame() {	}
	
	chooseCharacter(args) {
		let characterName = args[0];
		this.chosenCharacterName=characterName;
	}
	
	chooseCountry(args) {
		let countryName = args[0];
		this.chosenCountryName=countryName;
	}
	
	//creditsPage
	exitCredits() {	super.stopGame(); }
	exitGame() { super.stopGame();	}

	
	// METHODS
	
	// FACULTATIVE PUBLIC METHODS
	getLevelName(pageName){
		
		if(pageName==="gamePage"){
			return "level_"+this.chosenCountryName;
		}
		return null;
	}
	
	getSelectedCharacter(){
		let selectedCharacter=this.getCharacterByName(this.chosenCharacterName);
		
		if(nothing(selectedCharacter)){
			let prototypeName=PROTO_PREFIX+this.chosenCharacterName;
			selectedCharacter=this.createCharacter(prototypeName);
			this.addCharacter(selectedCharacter);
		}

//		// DBG
//		console.log("getSelectedCharacter this.chosenCharacterName:",this.chosenCharacterName);
//		console.log("getSelectedCharacter selectedCharacter:",selectedCharacter);
		
		return selectedCharacter;
	}
	

	// MANDATORY METHODS

	
}


