// DEPENDENCY : init.js

language = "fr";

PROTO_PREFIX="proto_";

// ===============================================================================
// 																			CONFIG
// ===============================================================================


GAME_CONFIG = {

	basePath:"survivants.medias",	
	// view config :
	X_OFFSET:-135,
	Y_OFFSET:-140,
	zoom:1.5,
	chosenResolutionIndex:1,
	getResolution:function(){
		return [{w:getWindowSize("width"),h:getWindowSize("height")},{w:800,h:400}][this.chosenResolutionIndex];
	},
	getViewCenterOffset:function(ignoreOffset=false){
		var result={
				x:Math.floor(this.getResolution().w/2) + (ignoreOffset?0:this.X_OFFSET),
				y:this.getResolution().h + (ignoreOffset?0:this.Y_OFFSET)
		};
		return result;
	},
	// scroll config :
	scroll : "horizontal infinite",
	projection : "2D flat",
	// selection config :
	selectionFixCamera:true,
	selectionCanChange:false,
	selectionMaxSize:1,
	// controls config :
	controlsMode: "screen",
	controlsPosition : "direction action",
	// images config :
	imagesRootUI : "survivants.medias/ui/",
	imagesRootUniverse : "survivants.medias/universe/",
	backgroundClipSize:{w:800,h:400},
	// controller config :
	refreshRateMillis : 100,
	gameCentralServer : null,
};






FLOW_CONFIG = {

	
	splashPage : {

		delayMillis : 500,
		image: "splash/aotraLogo_128.gif",
		goTo : "homePage"

	},
	
	
	
	homePage : {

		startLevelName : "level_home",

		menus : {
			mainMenu : {
				items : [
					{
						label : i18n({
							"fr" : "Commencer",
							"en" : "Start"
						}),
						action : "startNewGame",
						goTo : "characterSelectionPage"
					}, {
						label : i18n({
							"fr" : "Continuer",
							"en" : "Continue"
						}),
						action : "continueGame",
						goTo : "gamePage",
						activeOnlyIf : "isPreviousGameDetected()"
					}, {
						label : i18n({
							"fr" : "Crédits",
							"en" : "Credits"
						}),
						action : "startCredits",
						goTo : "creditsPage"
					}, {
						label : i18n({
							"fr" : "Quitter",
							"en" : "Exit"
						}),
						action : "exitGame",
						goTo : "homePage"
					}
				]
			}
		}
	}

	,characterSelectionPage : {

		startLevelName : "level_home",

		menus : {
			mainMenu : {
				items : [
				  {
						label : i18n({
							"fr" : "Tito",
							"en" : "Tito"
						}),
						action : "chooseCharacter",
						args : [
							"tito"
						],
						goTo : "countrySelectionPage"
					}, 
					{
						label : i18n({
							"fr" : "Jean-Luc",
							"en" : "John-Luke"
						}),
						action : "chooseCharacter",
						args : [
							"jeanluc"
						],
						goTo : "countrySelectionPage"
					}, {
						label : i18n({
							"fr" : "Aristote",
							"en" : "Aristote"
						}),
						action : "chooseCharacter",
						args : [
							"aristote"
						],
						goTo : "countrySelectionPage"
					}, {
						label : i18n({
							"fr" : "Marie",
							"en" : "Mary"
						}),
						action : "chooseCharacter",
						args : [
							"marie"
						],
						goTo : "countrySelectionPage"
					}, {
						label : i18n({
							"fr" : "Fatimah",
							"en" : "Fatimah"
						}),
						action : "chooseCharacter",
						args : [
							"fatimah"
						],
						goTo : "countrySelectionPage"
					}, {
						label : i18n({
							"fr" : "Retour",
							"en" : "Back"
						}),
						goTo : "homePage"
					}
				]
			}
		}
	},

	countrySelectionPage : {

		startLevelName : "level_home",

		menus : {
			mainMenu : {
				items : [
					{
						label : i18n({
							"fr" : "Québec",
							"en" : "Quebec"
						}),
						action : "chooseCountry",
						args : [
							"quebec"
						],
						goTo : "gamePage"
					}, {
						label : i18n({
							"fr" : "France",
							"en" : "France"
						}),
						action : "chooseCountry",
						args : [
							"france"
						],
						goTo : "gamePage"
					}, {
						label : i18n({
							"fr" : "Retour",
							"en" : "Back"
						}),
						goTo : "characterSelectionPage"
					}
				]
			}
		}
	}

	,gamePage : {

		startLevelName : "getLevelName()",
		hasControls:true,

		menus : {
			mainMenu : {
				config : {
					label : i18n({
						"fr" : "Menu",
						"en" : "Menu"
					}),
					action : "showMenu",
					position : "top-right" // default position is "top-left"
				},
				items : [
					{
						label : i18n({
							"fr" : "Reprendre",
							"en" : "Resume"
						}),
						action : "resumeGame",
						resume : true
					}, {
						label : i18n({
							"fr" : "Accueil",
							"en" : "Home"
						}),
						action : "exitGame",
						goTo : "homePage"
					}, {
						label : i18n({
							"fr" : "Quitter",
							"en" : "Exit"
						}),
						action : "exitGame",
						goTo : "homePage"
					}
				]
			}
		}
	}

	,creditsPage : {

		startLevelName : "level_credits",

		menus : {
			mainMenu : {
				items : [
					{
						label : i18n({
							"fr" : "Accueil",
							"en" : "Home"
						}),
						action : "exitCredits",
						goTo : "homePage"
					}
				]
			}
		}
	}

};






// ===============================================================================
// 																			MODEL
// ===============================================================================





// Prototype can define ass well class of things as particular individuals : only by not setting a random variance on them.



PROTOTYPES_CONFIG = {

	allPrototypes : {

		

		GameLevel : {
			
			level_home : {
				City : {
					cardinality : {
						value:"1",
						prototypesInstanciation : {randoms:["proto_quebec", "proto_paris"	]}
					},
				},

//				viewMode : "scroll:horizontal infinite",
			},
			// TODO :
			//	level_intro : {
			//		isCinematic : true
			//	},
			//	level_prologueJeanluc : {
			//		city : [ "proto_quebec", "proto_paris" ]
			//	},
			//	level_prologueAristote: {
			//		city : [ "proto_quebec", "proto_paris" ]
			//	},
			//	level_prologueMarie: {
			//		city : [ "proto_quebec", "proto_paris" ]
			//	},
			//	level_prologueFatimah: {
			//		city : [ "proto_quebec", "proto_paris" ]
			//	},
			level_quebec : {
				selectedItems : "getSelectedCharacter()",
				City : {
					cardinality : {
						value:"1",
						prototypesInstanciation : {randoms:["proto_quebec"]}
					},
				},
//				viewMode : "scroll:horizontal infinite"
			},
			level_france : {
				selectedItems : "getSelectedCharacter()",
				City : {
					cardinality : {
						value:"1",
						prototypesInstanciation : {randoms:["proto_paris"]}
					},
				},
//				viewMode : "scroll:horizontal infinite"
			},
			level_credits :{
				City : {
					cardinality : {
						value:"1",
						prototypesInstanciation : {randoms:["proto_quebec", "proto_paris"]}
					},
				}
			}
				
		},
		
		
		City : {


			proto_occidentalCity : {

				backgroundZIndex : 1,
				backgroundParallax : 3,
				backgroundOffsetY:200,

				containerConfig : "starter",
				

				DaylightManager : {
					hourOfDay : "0->24",
					dayColor : "#F4E542",
					nightColor : "#0F0B4F",
				},


				backgroundImage : "backgroundOccidentalCity.png",

				// first argument is always cardinality, probability to appear, if and only if property is a collection :
				// (cardinality 1 = A weather is always instanciated)
				Weather : {
					cardinality : {
						value:"1",
						prototypesInstanciation : {randoms:["proto_snowy", "proto_sunny", "proto_windy", "proto_rainy"]}
					},
				},
				
				
				
				Ground : {
					cardinality : {
						value:"fill", fillInstanciationMargin:50, spawningZone:{y:25, h:0, margin:-20},
						prototypesInstanciation :
						{
							randoms:["proto_ground1", "proto_ground2","proto_ground3", "proto_ground4", "proto_ground5"]
						},
						
				  },
				},
				
				
				Character : {
					
//				cardinality : { value:"5->10",spawningZone:{y:-5 , h:10} },
//				prototypesInstanciation : {
//					randoms:["proto_femaleCat", "proto_maleCat", "proto_femaleSalary1", "proto_maleSalary1", "proto_maleSalary2"]
//				}

					cardinality : {
						
						value:"groups",
						
						prototypesInstanciation : {
							randoms:[ 
							          {value:"4->6",name:"proto_femaleSalary1", spawningZone:{y:-2, h:4} },
							          {value:"3->5",name:"proto_maleSalary1", spawningZone:{y:-2, h:4} },
							          {value:"2->3",name:"proto_maleSalary2", spawningZone:{y:-2, h:4} }
//											{value:"12->13",name:"proto_aristote", spawningZone:{y:-2 , h:4} }

											],
//							          statics:[
//							                   {value:"proto_femaleCat",position:{x:800,y:0,margin:100}},
//							                   {value:"proto_maleCat",position:{x:-800,y:0,margin:100}}
//							                  ]
					
						}
					},
				
				
				}
				
				
			},


			proto_quebec : {
				usesSuper : "proto_occidentalCity",
				// OVERRIDES

				backgroundImage : "backgroundQuebec.png",

				Weather : {
					cardinality : {
						value:"1",
						prototypesInstanciation : {randoms:["proto_snowy", "proto_sunny", "proto_windy"]}
					},
				},
				// (cardinality fill means buildings always show up on scrolling (on horizontal))

				Building : {
					cardinality : {
						value:"fill", fillInstanciationMargin:200, spawningZone:{y:5 , h:0},
						prototypesInstanciation :
						{
							randoms:["proto_residential1", "proto_residential2","proto_residential2basement", "proto_commercial1", "proto_commercial2basement"]
//						statics:[
//							         {value:"proto_commercial1",position:{x:800,y:0,margin:100}},
//							         {value:"proto_residential1",position:{x:-800,y:0,margin:100}}
//							         ]
						},
						
				  },
				}
			},
			
			proto_paris : {
				usesSuper : "proto_occidentalCity",
				// OVERRIDES

				backgroundImage : "backgroundParis.png",

				Weather : {
					cardinality : {
						value:"1",
						prototypesInstanciation : {randoms:["proto_rainy", "proto_sunny", "proto_windy"]}
					},
				},
				Building : {
					cardinality : {
						value:"fill", fillInstanciationMargin:200, spawningZone:{y:5 , h:0},
						prototypesInstanciation : 
						{
							randoms:["proto_residential1","proto_residential2","proto_residential2basement", "proto_commercial2basement", "proto_commercial1"]
						}
					},
				}
			}
		},

		Weather : {

			proto_rootWeather : {
				backgroundParallax : 5,
			},

			proto_snowy : {
				usesSuper : "proto_rootWeather",

				temperature : "-25->0",
				precipitations : "5->30",
				precipitationsType : "snow",
				wind : "0->20",

				backgroundImage : "backgroundSnow.png",

			},
			proto_sunny : {

				usesSuper : "proto_rootWeather",

				temperature : "-5->30",

				backgroundImage : "backgroundSun.png",

			},
			proto_windy : {
				usesSuper : "proto_rootWeather",

				temperature : "-10->10",
				wind : "10->50",

				backgroundImage : "backgroundWind.png",
			},
			proto_rainy : {
				usesSuper : "proto_rootWeather",

				temperature : "0->15",
				precipitations : "5->30",
				precipitationsType : "rain",
				wind : "0->10",

				backgroundImage : "backgroundRain.png",

			}
		},

		
		
		/***********************************/ 
		
		Ground:{
			
			proto_rootGround : {

				size:{w:81,h:48},

				imageConfig : {
					_2D : {
						horizontal : {
							center:{x:"center",y:"bottom"},
							sourcePath : "proto_rootGround.png",
						}
					}
				},
			},
			
			proto_ground1 : {
				usesSuper : "proto_rootGround",
				
				imageConfig : {
					_2D : {
						horizontal : {
							center:{x:"center",y:"bottom"},
							sourcePath : "proto_ground1.png",
						}
					}
				},
			},
			proto_ground2 : {
				usesSuper : "proto_rootGround",
				
				imageConfig : {
					_2D : {
						horizontal : {
							center:{x:"center",y:"bottom"},
							sourcePath : "proto_ground2.png",
						}
					}
				},
			},
			proto_ground3 : {
				usesSuper : "proto_rootGround",
				
				imageConfig : {
					_2D : {
						horizontal : {
							center:{x:"center",y:"bottom"},
							sourcePath : "proto_ground3.png",
						}
					}
				},
			},
			proto_ground4 : {
				usesSuper : "proto_rootGround",
				
				imageConfig : {
					_2D : {
						horizontal : {
							center:{x:"center",y:"bottom"},
							sourcePath : "proto_ground4.png",
						}
					}
				},
			},
			proto_ground5 : {
				usesSuper : "proto_rootGround",
				
				imageConfig : {
					_2D : {
						horizontal : {
							center:{x:"center",y:"bottom"},
							sourcePath : "proto_ground5.png",
						}
					}
				},
			},
			
		},
		
		
		
		
		Building : {

			proto_rootBuilding : {
				containerConfig : "allow",

				size:{w:177,h:300},

				imageConfig : {
					_2D : {
						horizontal : {
							center:{x:"center",y:"bottom"},
							sourcePath : "proto_rootBuilding.png",
						}
					}
				},
			},

			
			proto_residential1 : {
				usesSuper : "proto_rootBuilding",
				
				size:{w:356,h:300},
				
				imageConfig : {
					_2D : {
						horizontal : {
							center:{x:"center",y:"bottom"},
							sourcePath : "proto_residential1.png",
						}
					}
				},

				floors : "1->3",
				entrances : "1->2",
				widthPerEntrance : "100->200",
				heightPerFloor : "200->400",
				windowsPerFloor : "1->2", // first floor number of widows = (number of widows - number of entrances)
				upWindowsConfig : {
					random : true,
					values : [
						"10 20 standardWindow1.png", "5 20 standardWindow1.png", "10 15 standardWindow2.png"
					]
				},
				firstFloorWindowsConfig : {
					random : true,
					values : [
						"10 20 standardWindow1.png", "10 15 standardWindow2.png"
					]
				},
				doorsConfig : {
					random : true,
					values : [
						"10 30 standardDoor1.png"
					]
				},
				hasStairs : {
					random : true,
					values : [
						true, false
					]
				}
			},


			
			proto_residential2 : {
				usesSuper : "proto_rootBuilding",
				
				size:{w:278,h:300},
				
				imageConfig : {
					_2D : {
						horizontal : {
							center:{x:"center",y:"bottom"},
							sourcePath : "proto_residential2.png",
						}
					}
				},

				floors : "1->3",
				entrances : "1->2",
				widthPerEntrance : "100->200",
				heightPerFloor : "200->400",
				windowsPerFloor : "1->2", // first floor number of widows = (number of widows - number of entrances)
				upWindowsConfig : {
					random : true,
					values : [
						"10 20 standardWindow1.png", "5 20 standardWindow1.png", "10 15 standardWindow2.png"
					]
				},
				firstFloorWindowsConfig : {
					random : true,
					values : [
						"10 20 standardWindow1.png", "10 15 standardWindow2.png"
					]
				},
				doorsConfig : {
					random : true,
					values : [
						"10 30 standardDoor1.png"
					]
				},
				hasStairs : {
					random : true,
					values : [
						true, false
					]
				}
			},
			
			
			proto_residential2basement : {
				usesSuper : "proto_residential1",
				// OVERRIDES
				hasBasement : true,
				
				size:{w:173,h:300},

				imageConfig : {
					_2D : {
						horizontal : {
							center:{x:"center",y:"bottom"},
							sourcePath : "proto_residential2basement.png",
						}
					}
				}
			
			},
			
			
			proto_commercial1 : {
				usesSuper : "proto_rootBuilding",

				size:{w:177,h:300},

				imageConfig : {
					_2D : {
						horizontal : {
							center:{x:"center",y:"bottom"},
							sourcePath : "proto_commercial1.png",
						}
					}
				},
				
				floors : "1->5",
				entrances : "1->1",
				widthPerEntrance : "200->300",
				heightPerFloor : "200->400",
				windowsPerFloor : "1->4" // first floor number of widows = (number of widows - number of entrances)
				,
				upWindowsConfig : {
					random : true,
					values : [
						"10 20 standardWindow1.png"
					]
				},
				firstFloorWindowsConfig : {
					random : true,
					values : [
						"50 50 storeFrontWindow1.png"
					]
				},
				doorsConfig : {
					random : true,
					values : [
						"10 30 standardDoor2.png"
					]
				},
				hasStairs : false,
				canEnter : true
			},
			
			
			
			proto_commercial2basement : {
				usesSuper : "proto_commercial1",
				// OVERRIDES
				hasBasement : true,
				
				size:{w:175,h:300},

				imageConfig : {
					_2D : {
						horizontal : {
							center:{x:"center",y:"bottom"},
							sourcePath : "proto_commercial2basement.png",
						}
					}
				}
			}
		
		
		
		}
		
		

		/*************************/
		
		,Character : {
			proto_femaleCat : {

				size:{w:60,h:60},
				
				imageConfig : {
					_2D : {
						horizontal : {
							correlateYZ:true,
							center:{x:"center",y:"bottom"},
							refreshMillis:200,
							clipSize:{w:140,h:100},
							walkLeft : "walkL.png",
							walkRight : "walkR.png",
							standbyLeft : "standL.png",
							standbyRight : "standR.png",
							idles : {
								triggerAfterMillis : 4000,
								sourcesPaths : [
									"idle1.png", "idle2.png", "idle3.png"
								],
							}
						}
					}
				},

				LifeJauge : {
					health : "150",
					morale : "10->500",
					energy : "50->2000",
					heat : "1500",
					unhunger : "2000",
					unthirst : "40->2000",
					money : "0"
				}
			}
		
			,proto_maleCat : {

				size:{w:60,h:60},
				
				imageConfig : {
					_2D : {
						horizontal : {
							correlateYZ:true,
							center:{x:"center",y:"bottom"},
							refreshMillis:200,
							clipSize:{w:140,h:100},
							walkLeft : "walkL.png",
							walkRight : "walkR.png",
							standbyLeft : "standL.png",
							standbyRight : "standR.png",
							idles : {
								triggerAfterMillis : 4000,
								sourcesPaths : [
									"idle1.png", "idle2.png", "idle3.png"
								],
							}
						}
					}
				},

				LifeJauge : {
					health : "200",
					morale : "10->400",
					energy : "20->1000",
					heat : "1000",
					unhunger : "1000",
					unthirst : "20->1000",
					money : "0"
				}
			}

			,proto_maleHuman : {
				
				size:{w:120,h:120},
				
				imageConfig : {
					_2D : {
						horizontal : {
							correlateYZ:true,
							center:{x:"center",y:"bottom"},
							refreshMillis:200,
							clipSize:{w:87,h:200},
							walkLeft : "walkL.png",
							walkRight : "walkR.png",
							standbyLeft : "standL.png",
							standbyRight : "standR.png",
							idles : {
								triggerAfterMillis : 4000,
								sourcesPaths : [
									"idle1.png", "idle2.png", "idle3.png"
								],
							}
						}
					}
				},
				LifeJauge : {
					health : "1800",
					morale : "40->100",
					energy : "20->1800",
					heat : "2500",
					unhunger : "1500->2000",
					unthirst : "1500->2000",
					money : "200->700"
				}
			}


			,proto_femaleHuman : {

				size:{w:120,h:120},

				imageConfig : {
					_2D : {
						horizontal : {
							correlateYZ:true,
							center:{x:"center",y:"bottom"},
							refreshMillis:200,
							clipSize:{w:87,h:200},
							walkLeft : "walkL.png",
							walkRight : "walkR.png",
							standbyLeft : "standL.png",
							standbyRight : "standR.png",
							idles : {
								triggerAfterMillis : 4000,
								sourcesPaths : [
									"idle1.png", "idle2.png", "idle3.png"
								],
							}
						}
					}
				},

				LifeJauge : {
					health : "2000",
					morale : "10->50",
					energy : "30->2000",
					heat : "2000",
					unhunger : "1500->2000",
					unthirst : "1500->2000",
					money : "100->500"
				}
			}


			,proto_femaleSalary1 : {

				usesSuper : "proto_femaleHuman",

				imageConfig : {
					_2D : {
						horizontal : {
							correlateYZ:true,
							center:{x:"center",y:"bottom"},
							refreshMillis:200,
							clipSize:{w:87,h:200},
							walkLeft : "walkL.png",
							walkRight : "walkR.png",
							standbyLeft : "standL.png",
							standbyRight : "standR.png",
							idles : {
								triggerAfterMillis : 4000,
								sourcesPaths : [
									"idle1.png", "idle2.png", "idle3.png"
								],
							}
						}
					}
				},

			}

			,proto_maleSalary1 : {

				usesSuper : "proto_maleHuman",

				imageConfig : {
					_2D : {
						horizontal : {
							correlateYZ:true,
							center:{x:"center",y:"bottom"},
							refreshMillis:200,
							clipSize:{w:87,h:200},
							walkLeft : "walkL.png",
							walkRight : "walkR.png",
							standbyLeft : "standL.png",
							standbyRight : "standR.png",
							idles : {
								triggerAfterMillis : 4000,
								sourcesPaths : [
									"idle1.png", "idle2.png", "idle3.png"
								],
							}
						}
					}
				},
			}

			,proto_maleSalary2 : {

				usesSuper : "proto_maleHuman",

				imageConfig : {
					_2D : {
						horizontal : {
							correlateYZ:true,
							center:{x:"center",y:"bottom"},
							refreshMillis:200,
							clipSize:{w:87,h:200},
							walkLeft : "walkL.png",
							walkRight : "walkR.png",
							standbyLeft : "standL.png",
							standbyRight : "standR.png",
							idles : {
								triggerAfterMillis : 4000,
								sourcesPaths : [
									"idle1.png", "idle2.png", "idle3.png"
								],
							}
						}
					}
				},

				LifeJauge : {
					health : "1900",
					morale : "190",
					energy : "1900",
					heat : "2900",
					unhunger : "1900",
					unthirst : "1900",
					money : "90"
				}
			}

			// PLAYABLE CHARACTERS :

			,proto_tito : {
				usesSuper : "proto_maleCat",
				// OVERRIDE

				imageConfig : {
					_2D : {
						horizontal : {
							correlateYZ:true,
							center:{x:"center",y:"bottom"},
							refreshMillis:200,
							clipSize:{w:140,h:100},
							walkLeft : "walkL.png",
							walkRight : "walkR.png",
							standbyLeft : "standL.png",
							standbyRight : "standR.png",
							idles : {
								triggerAfterMillis : 4000,
								sourcesPaths : [
									"idle1.png", "idle2.png", "idle3.png"
								],
							}
						}
					}
				},

				LifeJauge : {
					// OVERRIDE
					morale : "400",
					energy : "1000",
					heat : "1000",
					unhunger : "1000",
					unthirst : "20->1000"
				}
			}

			,proto_jeanluc : {
				usesSuper : "proto_maleHuman",

				imageConfig : {
					_2D : {
						horizontal : {
							correlateYZ:true,
							center:{x:"center",y:"bottom"},
							refreshMillis:200,
							clipSize:{w:87,h:200},
							walkLeft : "walkL.png",
							walkRight : "walkR.png",
							standbyLeft : "standL.png",
							standbyRight : "standR.png",
							idles : {
								triggerAfterMillis : 4000,
								sourcesPaths : [
									"idle1.png", "idle2.png", "idle3.png"
								],
							}
						}
					}
				},

				LifeJauge : {
					health : "1900",
					morale : "190",
					energy : "1900",
					heat : "2900",
					unhunger : "1900",
					unthirst : "1900",
					money : "90"
				}
			}

			,proto_aristote : {
				usesSuper : "proto_maleHuman",

				imageConfig : {
					_2D : {
						horizontal : {
							correlateYZ:true,
							center:{x:"center",y:"bottom"},
							refreshMillis:200,
							clipSize:{w:87,h:200},
							walkLeft : "walkL.png",
							walkRight : "walkR.png",
							standbyLeft : "standL.png",
							standbyRight : "standR.png",
							idles : {
								triggerAfterMillis : 4000,
								sourcesPaths : [
									"idle1.png", "idle2.png", "idle3.png"
								],
							}
						}
					}
				},

				LifeJauge : {
					health : "1900",
					morale : "190",
					energy : "1900",
					heat : "2900",
					unhunger : "1900",
					unthirst : "1900",
					money : "90"
				}
			}
			
			,proto_fatimah : {
				usesSuper : "proto_femaleHuman",

				imageConfig : {
					_2D : {
						horizontal : {
							correlateYZ:true,
							center:{x:"center",y:"bottom"},
							refreshMillis:200,
							clipSize:{w:87,h:200},
							walkLeft : "walkL.png",
							walkRight : "walkR.png",
							standbyLeft : "standL.png",
							standbyRight : "standR.png",
							idles : {
								triggerAfterMillis : 4000,
								sourcesPaths : [
									"idle1.png", "idle2.png", "idle3.png"
								],
							}
						}
					}
				},

				LifeJauge : {
					health : "1900",
					morale : "190",
					energy : "1900",
					heat : "2900",
					unhunger : "1900",
					unthirst : "1900",
					money : "90"
				}
			}
			
			,proto_marie : {
				usesSuper : "proto_femaleHuman",

				imageConfig : {
					_2D : {
						horizontal : {
							correlateYZ:true,
							center:{x:"center",y:"bottom"},
							refreshMillis:200,
							clipSize:{w:87,h:200},
							walkLeft : "walkL.png",
							walkRight : "walkR.png",
							standbyLeft : "standL.png",
							standbyRight : "standR.png",
							idles : {
								triggerAfterMillis : 4000,
								sourcesPaths : [
									"idle1.png", "idle2.png", "idle3.png"
								],
							}
						}
					}
				},

				LifeJauge : {
					health : "1900",
					morale : "190",
					energy : "1900",
					heat : "2900",
					unhunger : "1900",
					unthirst : "1900",
					money : "90"
				}
			}


		}
	}
};
