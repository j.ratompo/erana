/* ## Utility gamu methods - utility part
 *
 * This set of methods gathers utility game-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils-gamu» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 *  
 */

// ======================== Other utility methods ========================


function createSprite(imgSrc,xOffset,yOffset,widthStep,delayMillis){
	//TODO...
	return {};
}

function createSimpleCollisionBox(movingObject,w,h){
	//TODO...
	return {};
}


function getCollidedObject(obj,otherObjects,projection="2D",scroll="horizontal"){
	
	let result=null;
	foreach(otherObjects,(otherObj)=>{

		if(collidesWith(obj, otherObj, projection, scroll)){
			result=otherObj;
		}
		
	},(otherObj)=>{	return otherObj!==obj; });
	
	return result;
}


function collidesWith(obj,otherObj,projection="2D",scroll="horizontal"){
	
	if(obj===otherObj) {
		// TRACE
		console.log("WARN : Object is the same. Does not collides.");
		return false;
	}
	let pos1=obj.position;
	let pos2=otherObj.position;
	
	if(!pos1 || !pos2) {
		// TRACE
		console.log("WARN : One of the objects has no position. Does not collides.");
		return false;
	}
	
	if(contains(projection, "2D")) {
	
		// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
		if(containsOneOf(scroll,["horizontal","bidimensional"])) {
			
			let extremities1=null;
			if(obj.getExtremities){
				extremities1=obj.getExtremities();
			}else{
				let size1=(obj.size || !obj.getSize ? obj.size : obj.getSize());
				if(size1){
					extremities1=obj.position.getExtremities();
				}
			}

			let extremities2=null;
			if(otherObj.getExtremities){
				extremities2=otherObj.getExtremities();
			}else{
				let size2=(otherObj.size || !otherObj.getSize ? otherObj.size : otherObj.getSize());
				if(size2){
					extremities2=otherObj.position.getExtremities();
				}
			}
			if(extremities1 && extremities2){
				if(    (extremities1.min.x < extremities2.min.x && extremities2.min.x < extremities1.max.x)
					|| (extremities1.min.x < extremities2.max.x && extremities2.max.x < extremities1.max.x)
					|| (extremities2.min.x < extremities1.min.x && extremities1.min.x < extremities2.max.x)
					|| (extremities2.min.x < extremities1.max.x && extremities1.max.x < extremities2.max.x)
				){
					if(contains(scroll,"horizontal"))	return true;
					return (
					   (extremities1.min.y < extremities2.min.y && extremities2.min.y < extremities1.max.y)
					|| (extremities1.min.y < extremities2.max.y && extremities2.max.y < extremities1.max.y)
					|| (extremities2.min.y < extremities1.min.y && extremities1.min.y < extremities2.max.y)
					|| (extremities2.min.y < extremities1.max.y && extremities1.max.y < extremities2.max.y));
				}
			}else if(!extremities1){
				let x=obj.position.x;
				if( extremities2.min.x < x && x < extremities2.max.x ){
					if(contains(scroll,"horizontal"))	return true;
					return (
						extremities2.min.y < y && y < extremities2.max.y 
					);
				}
				
			}else if(!extremities2){
				let x=otherObj.position.x;
				if( extremities1.min.x < x && x < extremities1.max.x ){
					if(contains(scroll,"horizontal"))	return true;
					return (
						extremities1.min.y < y && y < extremities1.max.y
					);
				}
				
			}
			
		}else{
			// TODO : ...
		}
	}
	
	return false;
}

