class GameBackgroundColor{

	constructor(color,gameConfig) {

		this.color=color;
		
		this.gameConfig=gameConfig;
		
		
	}
	
	// INIT

	
	// METHODS
	
	getIsForeground(){
		return false;
	}
	
	
	draw(ctx,camera,lightsManager){
		
		const zooms=this.gameConfig.zooms;
		let zoomedResolution=getZoomedSize(this.gameConfig.getResolution(), zooms);
		
		ctx.save();
		ctx.fillStyle=this.color;
		ctx.fillRect(0,0,zoomedResolution.w, zoomedResolution.h);
		ctx.restore();
		
	}

	
}
