class GameBackground{

	constructor(zIndex=1,pathImageSrc,config=null,gameConfig,parallax=1,imageSrc="",
		imageDarkSrc=null,
		clipSize=null, backgroundOffsetY=0, isForeground=false
	) {

		this.zIndex=zIndex;
		
		/*TODO : FIXME : MAGIC NUMBER : PARALLAX 5 IS NO RELATIVE MOTION !!!*/
		this.parallax=(parallax==0?1:parallax); // (cannot be 0 becaus we'll have to invert it later.)'
		
		this.clipSize=clipSize;
		this.backgroundOffsetY=backgroundOffsetY;
		
		this.image=new Image();
		this.image.src=pathImageSrc+imageSrc;
		

		if(imageDarkSrc){
		 this.imageDark=new Image();
		 this.imageDark.src=pathImageSrc+imageDarkSrc;
		}
		
		
		if(config && config.globalCrimson){
			const crimsonImageSrc=config.globalCrimson.image;
			if(crimsonImageSrc){ // case global crimson image
				this.imageCrimsonGlobal=new Image();
				this.imageCrimsonGlobal.src=pathImageSrc+crimsonImageSrc;
			}
		}
		
				
		if(config && config.globalStars){
			const starsImageSrc=config.globalStars.image;
			if(starsImageSrc){ // case global stars image
				this.imageStarsGlobal=new Image();
				this.imageStarsGlobal.src=pathImageSrc+starsImageSrc;
			}
		}
		
		
		this.isForeground=isForeground;
		
		this.config=config;
		this.gameConfig=gameConfig;
		
		
	}
	
	// INIT

	
	// METHODS
	
	getIsForeground(){
		return this.isForeground;
	}
	
	

	
	
	draw(ctx,camera,lightsManager){

		let imageParam=this.image;

		const zooms=this.gameConfig.zooms;
		
		const drawingWidth=imageParam.width;
		const drawingHeight=imageParam.height;

		if(!drawingWidth || !drawingHeight)	return;
		
		// (We have to spare divisions calculii !)
		let parallaxFactor=(1/this.parallax);
		
		const resolution=this.gameConfig.getResolution();
		const clipSizeW=(this.clipSize?this.clipSize.w : resolution.width);
		const clipSizeH=(this.clipSize?this.clipSize.h : resolution.height);
		
		// We display all tiles :
		let cameraPosition=camera.position.getParentPositionOffsetted();
		let clipX= cameraPosition.x*parallaxFactor;
		let clipY=-cameraPosition.y*parallaxFactor; // Caution ! y coordinate is
																								// inverted
		let loopedX=(clipX % imageParam.width);
		let loopedY=(clipY % imageParam.height);
		
		let projection = this.gameConfig.projection;
		// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
		let scroll = this.gameConfig.scroll;
		if(contains(projection, "2D")) {
			if(containsOneOf(scroll,["horizontal","bidimensional"])) {
				// Horizontal line looping tile:
				let imageParamX;
				let imageParamY=loopedY;
				if(loopedX<0){
					imageParamX=loopedX+drawingWidth;
				}else{
					imageParamX=loopedX-drawingWidth;
				}

				this.drawBackgroundImage(ctx,lightsManager,imageParam,imageParamX,imageParamY,clipSizeW,clipSizeH,camera.position.getAngle2D());
			}
			
			// Vertical line looping tile :
			// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
			if(containsOneOf(scroll,["vertical","bidimensional"])) {
				// Vertical line looping tile:
				let imageParamX=loopedX;
				let imageParamY;
				if(loopedY<0){
					imageParamY=loopedY+drawingHeight;
				}else{
					imageParamY=loopedY-drawingHeight;
				}
				
				this.drawBackgroundImage(ctx,lightsManager,imageParam,imageParamX,imageParamY,clipSizeW,clipSizeH,camera.position.getAngle2D());
			}
			
			// Diagonal lines looping tile :
			// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
			if(containsOneOf(scroll, ["horizontal","vertical","bidimensional"])) {

				let imageParamX=loopedX;
				let imageParamY=loopedY;
				if(loopedX<0 && loopedY<0){
					imageParamX=loopedX+drawingWidth;
					imageParamY=loopedY+drawingHeight;
				}else if(0<loopedX && 0<loopedY){
					imageParamX=loopedX-drawingWidth;
					imageParamY=loopedY-drawingHeight;
				}else if(loopedX<0 && 0<loopedY){
					imageParamX=loopedX+drawingWidth;
					imageParamY=loopedY-drawingHeight;
				}else if(0<loopedX && loopedY<0){
					imageParamX=loopedX-drawingWidth;
					imageParamY=loopedY+drawingHeight;
				}
				
				this.drawBackgroundImage(ctx,lightsManager,imageParam,imageParamX,imageParamY,clipSizeW,clipSizeH,camera.position.getAngle2D());
			}
	
			// Center :
			// This tile is always displayed:
			this.drawBackgroundImage(ctx,lightsManager,imageParam,loopedX,loopedY,clipSizeW,clipSizeH,camera.position.getAngle2D());
		}	
		
		
	}
	
	
	
	/*private*/drawBackgroundImage(ctx,lightsManager,imageParam,x,y,clipSizeW,clipSizeH,angleRadians){
		
		const zooms=this.gameConfig.zooms;

		if(!imageParam.width || !imageParam.height)	return;
		
		const drawingWidth=imageParam.width * zooms.zx;
		const drawingHeight=imageParam.height * zooms.zy;
		
		
		const backgroundOffsetY=this.backgroundOffsetY * zooms.zy;
		
		ctx.save();
		
//		drawImageAndCenterWithZooms(ctx,
//			imageParam,
//			// Drawing :
//			0, -backgroundOffsetY,
//			null,
//			{x:"left",y:"top"},
//			1,1,
//			drawingWidth,drawingHeight,
//			1,
//			true,
//			// Clip :
//			{x:x,y:y,w:clipSizeW,h:clipSizeH},
//			// TODO : FIXME :
//			//Math.coerceAngle(-angleRadians,true,true)
//			0
//			);
		
		ctx.drawImage(imageParam,
				// Clip :
				x, y,
				clipSizeW, clipSizeH,
				// Drawing :
				0, -backgroundOffsetY,
				drawingWidth, drawingHeight);
			
		
		
		if(this.imageDark && this.imageDark.width){
			
			
			const globalAlpha=Math.roundTo(1-(lightsManager.getLightsPercent()*.01), 2);
			ctx.globalAlpha=globalAlpha;
			
			ctx.globalCompositeOperation = "darken"; // OR ELSE : STRANGE BUG !...
			
			
			ctx.drawImage(this.imageDark,
					// Clip :
					x, y,
					clipSizeW, clipSizeH,
					// Drawing :
					0, -backgroundOffsetY,
					drawingWidth, drawingHeight);
			
		}
		
		ctx.restore();
		
		
		this.drawCrimson(ctx, lightsManager,x, y, clipSizeW, clipSizeH, drawingWidth, drawingHeight);
		
		this.drawStars(ctx, lightsManager,x, y, clipSizeW, clipSizeH, drawingWidth, drawingHeight);



	}
	
	
	/*private*/drawCrimson(ctx,lightsManager,x,y,clipSizeW,clipSizeH,drawingWidth,drawingHeight){

		if(this.imageCrimsonGlobal){ // case global crimson image
			ctx.save();
			
			const zooms=this.gameConfig.zooms;
			
			const backgroundOffsetY=this.backgroundOffsetY * zooms.zy;

			const config=this.config;
			let minMaxPercents={min:25,max:75};
			if(config.globalCrimson.triggerRange){
				minMaxPercents=Math.getMinMax(config.globalCrimson.triggerRange,"=>");
			}
			const minFactor=minMaxPercents.min*.01;
			const maxFactor=minMaxPercents.max*.01;
			
			const lightsPercent=lightsManager.getLightsPercent();
			const lightsFactor=lightsPercent*.01;
			const crimsonFactor=Math.curveTriggered(minFactor, maxFactor, lightsFactor);
			
			const globalAlpha=Math.roundTo(crimsonFactor, 2);
			
			ctx.globalAlpha=globalAlpha;
			
//			ctx.globalCompositeOperation = "darken"; // OR ELSE : STRANGE BUG !...
			
			ctx.drawImage(this.imageCrimsonGlobal,
					// Clip :
					x, y,
					clipSizeW, clipSizeH,
					// Drawing :
					0, -backgroundOffsetY,
					drawingWidth, drawingHeight);
			
			ctx.restore();
		}
	}
	
	/*private*/drawStars(ctx,lightsManager,x,y,clipSizeW,clipSizeH,drawingWidth,drawingHeight){

		if(this.imageStarsGlobal){ // case global stars image
			ctx.save();
		
			const zooms=this.gameConfig.zooms;
			
			const backgroundOffsetY=this.backgroundOffsetY * zooms.zy;


			const config=this.config;
			let minMaxPercents={min:25,max:75};
			if(config.globalStars.triggerRange){
				minMaxPercents=Math.getMinMax(config.globalStars.triggerRange,"=>");
			}
			const minFactor=minMaxPercents.min*.01;
			const maxFactor=minMaxPercents.max*.01;
			
			const lightsPercent=lightsManager.getLightsPercent();
			const lightsFactor=lightsPercent*.01;
			const starsFactor=Math.slopeTriggered(minFactor, maxFactor, lightsFactor,"descending");
			
			
			
			const globalAlpha=Math.roundTo(starsFactor, 2);
			
			ctx.globalAlpha=globalAlpha;
			
			
			ctx.drawImage(this.imageStarsGlobal,
					// Clip :
					x, y,
					clipSizeW, clipSizeH,
					// Drawing :
					0, -backgroundOffsetY,
					drawingWidth, drawingHeight);
			
			ctx.restore();
		}
	}
	

}

