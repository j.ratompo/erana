/* ## Utility gamu methods - view part
 *
 * This set of methods gathers utility game-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils-gamu» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 *  
 */

// ======================== CONSTANTS ========================

const DRAW_CHILDREN_AFTER=false;



// ======================== View utility methods ========================

// IMAGES

/*public static*/function loadImageInUniverse(gameConfig,imageOnlySrc,concernedObject=null,overriddenBasePath=null,doOnLoad=null){

	let resultImage=new Image();
	
	let universePath=null;
	if(overriddenBasePath){
		universePath=overriddenBasePath;
	}
	// (will use introspection to determine the specific base path, if no overriding path is provided :)
 	resultImage.src=getConventionalFilePath(gameConfig.basePath, nonull(universePath,concernedObject), "universe")+imageOnlySrc;

	resultImage.onload=function(event){
		
		if(!concernedObject || !concernedObject.size){
			resultImage.scaleW=1;
			resultImage.scaleH=1;
		}else{
			if(concernedObject.size.w && resultImage.width!=0)	resultImage.scaleW=concernedObject.size.w/resultImage.width;
			else	resultImage.scaleW=1;
			
			if(concernedObject.size.h && resultImage.height!=0)	resultImage.scaleH=concernedObject.size.h/resultImage.height;
			else	resultImage.scaleH=1;
		}
		
		resultImage.isReady=true;
		if(doOnLoad)	doOnLoad(resultImage);
		
	};
		
	return resultImage;
}


// SOUNDS

/*public static*/function loadSoundInUniverse(gameConfig, soundOnlySrc, concernedObject=null, overriddenBasePath=null, doOnLoad=null){
	
	let universePath=null;
	if(overriddenBasePath){
		universePath=overriddenBasePath;
	}
	// (will use introspection to determine the specific base path, if no overriding path is provided :)
	let src=getConventionalFilePath(gameConfig.basePath, nonull(universePath,concernedObject), "universe")+soundOnlySrc;
	let resultSound=loadSound(src,doOnLoad);
	
	return resultSound;
}





// SCENE & CAMERA


// TODO : FIXME : Move to its own class GameObjectsManager2D.
/*private*/function getObjectsManager(gameConfig,camera){
	
	
	var projection = gameConfig.projection;
	
	let self={
		
			
		uiVisibilities:{objectsUI:true},
		setUIVisibility:function(visibilityName,visibility){
			self.uiVisibilities[visibilityName]=visibility
		},
		drawAll2D:function(ctx,drawablesParam,camera,lightsManager,drawLevel="default"){
			
//			// DBG
//			let time=getNow();
			

			foreach(drawablesParam,(drawable1)=>{

				if(!isArray(drawable1)){
					
					if(drawLevel==="default"){

						if(!DRAW_CHILDREN_AFTER && drawable1.getDrawables)	self.drawAll2D(ctx,drawable1.getDrawables(),camera,lightsManager);
						if(drawable1.draw)	drawable1.draw(ctx,camera,lightsManager);
						if(DRAW_CHILDREN_AFTER && drawable1.getDrawables)		self.drawAll2D(ctx,drawable1.getDrawables(),camera,lightsManager);
					
					}else if(drawLevel==="back" && drawable1.drawInBack){

						if(!DRAW_CHILDREN_AFTER && drawable1.getDrawables)	self.drawAll2D(ctx,drawable1.getDrawables(),camera,lightsManager,"back");
						if(drawable1.drawInBack)	drawable1.drawInBack(ctx,camera,lightsManager);
						if(DRAW_CHILDREN_AFTER && drawable1.getDrawables)		self.drawAll2D(ctx,drawable1.getDrawables(),camera,lightsManager,"back");

					}else if(drawLevel==="front" && drawable1.drawInFront){

						if(!DRAW_CHILDREN_AFTER && drawable1.getDrawables)	self.drawAll2D(ctx,drawable1.getDrawables(),camera,null,"front");
						if(drawable1.drawInFront)	drawable1.drawInFront(ctx,camera);
						if(DRAW_CHILDREN_AFTER && drawable1.getDrawables)		self.drawAll2D(ctx,drawable1.getDrawables(),camera,null,"front");

//						if(!DRAW_CHILDREN_AFTER && drawable1.getDrawables)	self.drawAll2D(ctx,drawable1.getDrawables(),camera,lightsManager,"front");
//						if(drawable1.drawInFront)	drawable1.drawInFront(ctx,camera,lightsManager);
//						if(DRAW_CHILDREN_AFTER && drawable1.getDrawables)		self.drawAll2D(ctx,drawable1.getDrawables(),camera,lightsManager,"front");
				
					}else if(drawLevel==="ui" && drawable1.drawInUI){

						if(self.uiVisibilities.objectsUI){
							
							
							if(!DRAW_CHILDREN_AFTER && drawable1.getDrawables)	self.drawAll2D(ctx,drawable1.getDrawables(),camera,null,"ui");
							if(drawable1.drawInUI)	drawable1.drawInUI(ctx,camera);
							if(DRAW_CHILDREN_AFTER && drawable1.getDrawables)		self.drawAll2D(ctx,drawable1.getDrawables(),camera,null,"ui");
	
	//						if(!DRAW_CHILDREN_AFTER && drawable1.getDrawables)	self.drawAll2D(ctx,drawable1.getDrawables(),camera,lightsManager,"ui");
	//						if(drawable1.drawInUI)	drawable1.drawInUI(ctx,camera);
	//						if(DRAW_CHILDREN_AFTER && drawable1.getDrawables)		self.drawAll2D(ctx,drawable1.getDrawables(),camera,lightsManager,"ui");
							
							
						}
						
					}

					
				}else{

					foreach(drawable1,(drawable2)=>{

						if(drawLevel==="default"){

							if(!DRAW_CHILDREN_AFTER && drawable2.getDrawables)	self.drawAll2D(ctx,drawable2.getDrawables(),camera,lightsManager);
							if(drawable2.draw)	drawable2.draw(ctx,camera,lightsManager);
							if(DRAW_CHILDREN_AFTER && drawable2.getDrawables)		self.drawAll2D(ctx,drawable2.getDrawables(),camera,lightsManager);
						
						}else if(drawLevel==="back" && drawable2.drawInBack){

							if(!DRAW_CHILDREN_AFTER && drawable2.getDrawables)	self.drawAll2D(ctx,drawable2.getDrawables(),camera,lightsManager,"back");
							if(drawable2.drawInBack)	drawable2.drawInBack(ctx,camera,lightsManager);
							if(DRAW_CHILDREN_AFTER && drawable2.getDrawables)		self.drawAll2D(ctx,drawable2.getDrawables(),camera,lightsManager,"back");
						
						}else if(drawLevel==="front" && drawable2.drawInFront){

							if(!DRAW_CHILDREN_AFTER && drawable2.getDrawables)	self.drawAll2D(ctx,drawable2.getDrawables(),camera,null,"front");
							if(drawable2.drawInFront)	drawable2.drawInFront(ctx,camera);
							if(DRAW_CHILDREN_AFTER && drawable2.getDrawables)		self.drawAll2D(ctx,drawable2.getDrawables(),camera,null,"front");

//							if(!DRAW_CHILDREN_AFTER && drawable2.getDrawables)	self.drawAll2D(ctx,drawable2.getDrawables(),camera,lightsManager,"front");
//							if(drawable2.drawInFront)	drawable2.drawInFront(ctx,camera,lightsManager);
//							if(DRAW_CHILDREN_AFTER && drawable2.getDrawables)		self.drawAll2D(ctx,drawable2.getDrawables(),camera,lightsManager,"front");
					
						}else if(drawLevel==="ui" && drawable2.drawInUI){

							if(self.uiVisibilities.objectsUI){

								if(!DRAW_CHILDREN_AFTER && drawable2.getDrawables)	self.drawAll2D(ctx,drawable2.getDrawables(),camera,null,"ui");
								if(drawable2.drawInUI)	drawable2.drawInUI(ctx,camera);
								if(DRAW_CHILDREN_AFTER && drawable2.getDrawables)		self.drawAll2D(ctx,drawable2.getDrawables(),camera,null,"ui");
	
	//							if(!DRAW_CHILDREN_AFTER && drawable2.getDrawables)	self.drawAll2D(ctx,drawable2.getDrawables(),camera,lightsManager,"ui");
	//							if(drawable2.drawInUI)	drawable2.drawInUI(ctx,camera);
	//							if(DRAW_CHILDREN_AFTER && drawable2.getDrawables)		self.drawAll2D(ctx,drawable2.getDrawables(),camera,lightsManager,"ui");
								
							}
							
							
						}
						
						
					}
					// (We do not use the .getIsVisible() here, because other treatments might depend on this draw(...) execution 
					// even if the item is not visible ! We delegate it to the drawn objects to use this function or not.)
					,(drawable2)=>{ return !!drawable2; /*(forced to boolean)*/ }
//					,(drawable2)=>{ return drawable2 && typeof(drawable2.draw)!=="undefined"; }
					,(i1,i2)=>{
							// TODO : FIXME : DUPLICATED CODE !:
							// OLD : if(!i1.position || !i2.position)	return 1; // Case array of drawables;
							// NEW :
							if(!i1.position && !i2.position)	return 0; // Case array of drawables;
							// The sort is inverted : we have to draw first object having the lowest z-index :
							if(!i1.position)	return -1; // Case array of drawables;
							if(!i2.position)	return 1; // Case array of drawables;
							return i1.position.z-i2.position.z;
						}
					);
					
				}
				
				
			}
			// (We do not use the .getIsVisible() here, because other treatments might depend on this draw(...) execution 
			// even if the item is not visible ! We delegate it to the drawn objects to use this function or not.)
			,(drawable1)=>{ return !!drawable1; /*(forced to boolean)*/ }
//		,(drawable1)=>{ return drawable1 && (isArray(drawable1) || typeof(drawable1.draw)!=="undefined"); }
			,(i1,i2)=>{
					// TODO : FIXME : DUPLICATED CODE !:
//					OLD : if(!i1.position || !i2.position)	return 1; // Case array of drawables;
					// NEW :
					if(!i1.position && !i2.position)	return 0; // Case array of drawables;
					// The sort is inverted : we have to draw first object having the lowest z-index :
					if(!i1.position)	return -1; // Case array of drawables;
					if(!i2.position)	return 1; // Case array of drawables;
					return i1.position.z-i2.position.z;
				}
			);
			
//			// DBG
//			let duration=getNow()-time;
//			if(10<duration) lognow("draw ("+duration+"ms):",drawablesParam);

		},
		
		moveAll2D:function(currentContainer, movablesParam, 
				parentMovable=null// For offsetting position only
		){
			
			// Container moving :
			if(currentContainer.getIsMovable && currentContainer.getIsMovable() && currentContainer.move)	
				currentContainer.move();
			
			
			foreach(movablesParam,(movable1)=>{
				
				if(!isArray(movable1)){
					
					if(parentMovable)
						movable1.position.setParentPosition(parentMovable.position);
					
					if(typeof(movable1.getMover)!=="undefined" && movable1.getMover() && movable1.getMover().isStarted() && typeof(movable1.move)!=="undefined")
						movable1.move();
					
					if(movable1.getPlaceables || movable1.getDrawables){
						let placeables=(movable1.getPlaceables?movable1.getPlaceables():movable1.getDrawables());
						self.moveAll2D(currentContainer, placeables, movable1);
					}
					
				}else{
					
					foreach(movable1,(movable2)=>{
						
						if(parentMovable)
							movable2.position.setParentPosition(parentMovable.position);
						
						if(typeof(movable2.getMover)!=="undefined" && movable2.getMover() && movable2.getMover().isStarted() && typeof(movable2.move)!=="undefined")
							movable2.move();

						if(movable2.getPlaceables || movable2.getDrawables){
							let placeables=(movable2.getPlaceables?movable2.getPlaceables():movable2.getDrawables());
							self.moveAll2D(currentContainer, placeables, movable2);
						}


					},(movable2)=>{return movable2 && (typeof(movable2.getIsMovable)!=="undefined" && movable2.getIsMovable());});
					
				}
				
			},(movable1)=>{	return movable1 && ((isArray(movable1) || (typeof(movable1.getIsMovable)!=="undefined" && movable1.getIsMovable()))) ; });
					
		},
		
	};
	return self;
}


// TODO : FIXME : Move to its own class GameFXsManager2D.
/*private*/function getFXsManager(gameConfig){

	var projection = gameConfig.projection;

	
	const self={
		
		drawAll2D:function(ctx,camera){			
			// TODO
		}
		
	};
	
	return self;
}



// TODO : FIXME : Move to its own class GameBackgroundsManager2D.
/*private*/function getBackgroundsManager(gameConfig, backgrounds) {

	const projection = gameConfig.projection;
	
	const self={
		backgrounds : backgrounds,
		
		drawAll2D:function(ctx,camera,lightsManager,drawForegrounds=false){
			
			foreach(self.backgrounds,(b)=>{
				b.draw(ctx,camera,lightsManager);
			}
			
			,(b)=>{	return drawForegrounds===b.getIsForeground(); }
			// The sort is inverted : we have to draw first object having the lowest z-index :
			,(b1,b2)=>{	return b2.zIndex-b1.zIndex; }
			);
			
			
		}
	};


	return self;
}



// TODO : FIXME : Move to its own class GameSceneManager2D.
function getSceneManager2D(gameConfig, backgrounds, view, controller, onCameraMove=null){
	

	if(!controller){
		// !!! CAUTION : THIS NAME IS A CONVENTION TO BE RESPECTED IN CLIENT CODE !!!
		controller=window.eranaScreen;
	}
	
	
	const canvasId = "canvasLayer";
	let canvas = document.getElementById(canvasId);
	if(!canvas) {

		canvas = document.createElement("canvas");
		canvas.id = canvasId;

		// NOPE : will cause canvas pixel bugs !
//	var width = getWindowSize("width") + "px";
//	var height = getWindowSize("height") + "px";

		const zooms=gameConfig.zooms;
		const zoomedResolution=getZoomedSize(gameConfig.getResolution(), zooms);
		canvas.width=zoomedResolution.w;
		canvas.height=zoomedResolution.h;

		canvas.style.position = "fixed";
		canvas.style["z-index"] = "1";

		const mainElement = view.mainElement;
		mainElement.appendChild(canvas);
		
		// To correct a little bug :
		document.body.style.margin="0";
		
	}

	let sceneManagerSelf = {
		view:view,
		controller:controller,
		canvas : canvas,
		camera : getCamera2D(gameConfig,onCameraMove),
		lightsManager:getLightsManager(gameConfig),
		ctx:null,
		backgroundsManager : getBackgroundsManager(gameConfig, backgrounds),
		objectsManager : getObjectsManager(gameConfig,this.camera),
		fxsManager : getFXsManager(gameConfig),
		uiManager : getUIManager(gameConfig, controller),
		
		setUIVisibility:function(visibilityName,visibility){
			if(visibilityName==="objectsUI")	sceneManagerSelf.objectsManager.setUIVisibility(visibilityName,visibility);
			else 															sceneManagerSelf.uiManager.setUIVisibility(visibilityName,visibility);
		},
		
		
		placeAllOnInit:function(rootContainer){
			
			const viewPort=sceneManagerSelf.camera.getViewPort();
			let width= viewPort.w;
			let height=viewPort.h;
			
			if(rootContainer.cardinalities){
				
				foreach(rootContainer.cardinalities,(cardinality,key)=>{
					
					// 1- We place for the first time the previously instantiated objects :
					if(cardinality.value==="fill"){
	
						let spawningZone=nonull(getSpawningZone(cardinality),{x:0,y:0,w:width,h:height});
						
						if(contains(gameConfig.projection, "2D")) {
						
							// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
							if(contains(gameConfig.scroll, "horizontal")) {
								
								let totalWidth=width;
								
								// We use the «alternating left-right» population method :
								let widthCountRight=0;
								let itemWidtRightOld=0;
		
								let widthCountLeft=0;
								let itemWidthLeftOld=0;
								
								let xOffsettingCounter=0;
								let i=0;
								foreach(rootContainer[key], (item,itemIndex)=>{

									let itemWidth=item.size.w + nonull(spawningZone.w,0);
									if(i%2!=0){ // Left direction
										widthCountLeft -= (itemWidthLeftOld*.5 + itemWidth*.5);
										itemWidthLeftOld = itemWidth;
										xOffsettingCounter=widthCountLeft;
									}else{ // Right direction
										widthCountRight += (itemWidtRightOld*.5 + itemWidth*.5);
										itemWidtRightOld = itemWidth;
										xOffsettingCounter=widthCountRight;
									}
									
									// (Here, in «fill» mode, then there cannot be overlapping ! (because of how the «fill» mode positioning is done)) 
									let pos=getPositionFor2DObject(spawningZone, xOffsettingCounter);
									sceneManagerSelf.setPositionFirstTime(item, pos);
									
									i++;
	
								});
								
								
							}
						}
					
					}else{
						// Groups, ratio or interval collections cardinalities :
	
	
						
						// (cardinality 5/10 = An object has 5 chances out of 10 scroll events to appear)
						// (cardinality 5->10 = Objects total number on display area will always be between 5 min and 10 max on scroll events)
	
						let arr;
						
						// We wrap in an array if we have a single attribute, and not a collection :
						if(!isArray(rootContainer[key]))	arr=[rootContainer[key]];
						else 															arr=rootContainer[key];
						
						
						let alreadyPositionnedItems=[];
						let itemsToRepositionBecauseOfOverlap=[];
						
						let spawningZone=null;
						foreach(arr,(item,itemIndex)=>{
							
							
							if(!item.prototypeName)		return "continue";
							
							
							if(cardinality.value==="staticGroups" || cardinality.value==="oneGroupAtRandom"
								 || isNumber(cardinality.value)){ // Static or Random groups Collection cardinality :
								
								let groupsConfigs=
									(cardinality.prototypesInstantiation.staticInstantiations?
											cardinality.prototypesInstantiation.staticInstantiations:cardinality.prototypesInstantiation.randomInstantiations);
			
								// OLD :
	//						spawningZone=null;
								// If we find no zone, then we use the previous one of the collection :
								let spawningZoneLocal=foreach(groupsConfigs,(conf,index)=>{
									if(index===itemIndex)
										return getSpawningZone(conf);
								});
								if(spawningZoneLocal)	spawningZone=spawningZoneLocal;
								
								
							}
							
							
							if(spawningZone){
								
								if(contains(gameConfig.projection, "2D")) {
								
									// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
									if(containsOneOf(gameConfig.scroll, ["horizontal", "bidimensional"])) {
										
										
										let pos=getRandomPositionInZone(spawningZone, 0, 0, 0, cardinality.avoidOverlap, item.size);
										
										manageOverlappingItems(alreadyPositionnedItems,gameConfig,cardinality,item,pos);
										
										
										// If we have an horizontal scrolling, we correlate the z-index to the y coordinate 
										// at the first placement of all objects to emulate an isometric projection:
										if(item.imageConfig && item.imageConfig._2D.horizontal && item.imageConfig._2D.horizontal.correlateYZ){
											pos.z=pos.y;
										}
										
										
										sceneManagerSelf.setPositionFirstTime(item, pos, rootContainer);
										
									}else{
										// TODO ...
									}
								}
								
								
							}
	
							
						
						});
						
					}
	
				});
				
				
			}else{ // Case direct classes objects or arrays definitions :
				
			
				let item=rootContainer;
				let overridingPos=item.overridingPosition;
				if(overridingPos){
					// It is impossible (and makes no sense if it happens)
					// that we have an attribute overridingPosition for direct definition declared arrays !
					sceneManagerSelf.setPositionFirstTime(item, overridingPos);
				}
				
				
			}
			
			
			
			// !!! CAUTION : EVEN TO JUST PLACE ELEMENTS, THEY HAVE TO APPEAR IN 
			// THE .getDrawables() OR getPlaceables() FUNCTION !!!
			// We place all the children containers :
			if(rootContainer.getPlaceables || rootContainer.getDrawables){
				let placeables=(rootContainer.getPlaceables?rootContainer.getPlaceables():rootContainer.getDrawables());
				foreach(placeables,(child)=>{
					if(!isArray(child)) {
						sceneManagerSelf.placeAllOnInit(child);
					}else{
						foreach(child,(c)=>{
							sceneManagerSelf.placeAllOnInit(c);
						},(c)=>{return !!c; /*(forced to boolean)*/
//							return typeof(c.cardinalities)!=="undefined";
						}); // Case c represents an empty collection.
					}
				}
				// Case c represents an empty collection.
				,(c)=>{ return !!c; /*(forced to boolean)*/
//							return typeof(c.cardinalities)!=="undefined";
					}
				);
				
				// OLD
//			rootContainer.setIsVisible(true);
				
			}
			
			
			// Followers management :
			let followers=[];
			if(gameConfig.cameraFixed==="selection")	followers.push(sceneManagerSelf.camera);
			
			
			return followers;
		},
		
		// A hook for additional drawing :
		doDrawing:function(doOnDraw){
			if(!doOnDraw)	return;
			doOnDraw(sceneManagerSelf.ctx);
		},
		
		moveAll:function(currentContainer, movablesParam){
			controller.doLock();
			if(contains(gameConfig.projection, "2D")) {
				// Objects moving :
				sceneManagerSelf.objectsManager.moveAll2D(currentContainer, movablesParam);
			}				
			controller.doUnlock();
		},
		
		drawAllInUI:function(ctx,camera,lightsManager,drawablesParam,currentContainer){

			// Container UI drawing :
			if(currentContainer.getIsVisible && currentContainer.getIsVisible() && currentContainer.drawInUI)
				currentContainer.drawInUI(ctx, camera);

			// UI elements attached to objects drawing in UI :
			sceneManagerSelf.objectsManager.drawAll2D(ctx,drawablesParam,camera,lightsManager,"ui");
			
			// UI elements drawing :
			sceneManagerSelf.uiManager.drawAll2D(ctx,camera,sceneManagerSelf.controller.selectionManager);
			
		},
		
		drawAll2D:function(currentContainer, drawablesParam){
			

			if(contains(gameConfig.projection, "2D")) {

				if(!sceneManagerSelf.ctx)		sceneManagerSelf.ctx=sceneManagerSelf.canvas.getContext("2d");

				
				
				let ctx=sceneManagerSelf.ctx;
				
				
				//!!!CAUTION : DRAW ORDER IS IMPORTANT!!!

				const canvasWidth=gameConfig.w;
				const canvasHeight=gameConfig.h;
				ctx.clearRect(0, 0, canvasWidth, canvasHeight);
				
				const  camera=sceneManagerSelf.camera;
				const lightsManager=sceneManagerSelf.lightsManager;
				
				// Container drawing :
				if(currentContainer.getIsVisible && currentContainer.getIsVisible() && currentContainer.draw)
					currentContainer.draw(ctx, camera, lightsManager);
				
				// Backgrounds drawing :
				sceneManagerSelf.backgroundsManager.drawAll2D(ctx,camera,lightsManager);

				// Objects drawing in back :
				sceneManagerSelf.objectsManager.drawAll2D(ctx,drawablesParam,camera,lightsManager,"back");
				
				// Objects drawing :
				sceneManagerSelf.objectsManager.drawAll2D(ctx,drawablesParam,camera,lightsManager);

				// Objects drawing in front :
				sceneManagerSelf.objectsManager.drawAll2D(ctx,drawablesParam,camera,lightsManager,"front");
				
				// FXs drawing :
				sceneManagerSelf.fxsManager.drawAll2D(ctx,camera);
				
				// Foregrounds drawing :
				sceneManagerSelf.backgroundsManager.drawAll2D(ctx,camera,lightsManager,true);

			
				// All UI drawing :
				sceneManagerSelf.drawAllInUI(ctx,camera,lightsManager,drawablesParam,currentContainer);

			}


			
		},
		
		
		/*private*/hasDirectAncestorUnclickable:function(item){
			if(!item)	return false;
			if(!item.parent)	return false;
			if(
//IMPORTANT :	!item.doOnClick
						!item.isClickable
					 || !item.isClickable())
				return true;
			return sceneManagerSelf.hasDirectAncestorUnclickable(item.parent);
		},
		
		/*public*/setPositionFirstTime:function(item, pos, parentItem=null){
		
			// CAUTION : ONLY ITEMS WITH A SPAWNING POSITION IN PARENT PROTOTYPE CONFIG AND IN A getDrawables() RESULT ARE SET A POSITION FOR FIRST TIME WITH THIS METHOD !
			
			if(item.imageConfig && item.imageConfig._2D ) { 
				if(item.imageConfig._2D && item.imageConfig._2D.center){
					let center=item.imageConfig._2D.center;
					pos.center={x:center.x,y:center.y};
				}
				if(!nothing(item.imageConfig._2D.zIndex)){
					pos.z=item.imageConfig._2D.zIndex;
				}
			}
			
			
			// If this item has no position, then we create one for him :
			if(!item.position || item.overridingPosition) {
				if(item.overridingPosition)		item.position=new Position(item, item.overridingPosition.x, item.overridingPosition.y, item.overridingPosition.z, item.overridingPosition.center, null, null,
																				 item.overridingPosition.b, item.overridingPosition.g, item.overridingPosition.a);
				else							item.position=new Position(item, pos.x, pos.y, pos.z, pos.center, null, null, pos.b, pos.g, pos.a);
			}else{
				item.position.setLocation(pos);
			}
			
			// parallax is for 2D positionned objects only :
			if(item.imageConfig && item.imageConfig._2D && pos.parallax)	item.position.setParallax(pos.parallax);
			
			
			// If we have a parent, then we add its coordinates to the current child :
			// (CAUTION : Only in no-«fill» cardinalities ! -on today-)
			if(parentItem && parentItem.position){
// OLD, NOT WORKING :			item.position.setOffsets(parentItem.position);
//			let offsettedParentPosition=parentItem.position.getParentPositionOffsetted();
//			item.position.setOffsets({x:offsettedParentPosition.x,y:offsettedParentPosition.y});
				item.position.setParentPosition(parentItem.position);
			}

			// OLD 
//			item.setIsVisible(true);
			
			if(sceneManagerSelf.lightsManager)
				sceneManagerSelf.lightsManager.registerLights(item);
			
			if(item.imageConfig && item.imageConfig._2D && !nothing(item.imageConfig._2D.zIndex))
				item.position.z=item.imageConfig._2D.zIndex;
			
			
			let itemIsClickable= !!(item.doOnClick && item.isClickable && item.isClickable()); /*(forced to boolean)*/
			if(			sceneManagerSelf.view.controlsManager 
					&& !sceneManagerSelf.hasDirectAncestorUnclickable(parentItem)
					&&	itemIsClickable
				){
				sceneManagerSelf.view.controlsManager.registerClickable(item);
			}
			
			// Useful for ongoing instanciated objects ! Or objects instnaciated way after the level is loaded !
			if(item.initAfterSetPositionFirstTime)	item.initAfterSetPositionFirstTime();	
			
		}
	
	
	
	
	};

	
	return sceneManagerSelf;
}

// ----------------------------------------------




/*private*/function getCamera2D(gameConfig,onCameraMove=null) {

	let camera = {
		gameConfig:gameConfig,
		getViewPort:()=>{
			const zooms=gameConfig.zooms;
			const resolutions=gameConfig.getResolution();
			if(!zooms)	return resolutions;
			const zoomedResolution=getZoomedSize(resolutions, zooms);
			return zoomedResolution;
		}
	};

	let projection = gameConfig.projection;
	if(contains(projection, "2D")) {
		camera.position=new Position(camera,0,0,0,{x:"center",y:"center"},onCameraMove);
	}
	
	return camera;
}






/*public*/function manageOverlappingItems(alreadyPositionnedItems,gameConfig,cardinality,item,pos){
	
	let avoidOverlap=cardinality.avoidOverlap; 
	if(avoidOverlap && item.size){
		
		if(gameConfig.wrapping){
			if(gameConfig.wrapping.x) {
				let itemW=item.size.w;
				if(itemW && contains(avoidOverlap,"x")){
					if(gameConfig.wrapping.x<=alreadyPositionnedItems.length*itemW){
						// TRACE
						lognow("WARN : Too many objects to position on the horizontal scrolling, cannot reposition to avoid overlap.");
					}
				}
			}
			if(gameConfig.wrapping.y) {
				let itemH=item.size.h;
				if(itemH && contains(avoidOverlap,"y")){
					if(gameConfig.wrapping.y<=alreadyPositionnedItems.length*itemH){
						// TRACE
						lognow("WARN : Too many objects to position on the vertical scrolling, cannot reposition to avoid overlap.");
					}
				}
			}
		}
		
		// For overlapping management :
		foreach(alreadyPositionnedItems,(itemToPosition)=>{
			if(isInZone(itemToPosition.item.position,itemToPosition.spawningZone)){
				if(contains(avoidOverlap,"x")){
					let itemW=item.size.w;
					if(gameConfig.wrapping && gameConfig.wrapping.x)	pos.x+=(itemW + 5)%gameConfig.wrapping.x;
					else																							pos.x+=(itemW + 5);
				}
				if(contains(avoidOverlap,"y")){
					let itemH=item.size.h;
					if(gameConfig.wrapping && gameConfig.wrapping.y)	pos.y+=(itemH + 5)%gameConfig.wrapping.y;
					else																							pos.y+=(itemH + 5);
				}
			}
		});
		
		alreadyPositionnedItems.push({item:item, spawningZone:{x:pos.x, y:pos.y, w:item.size.w, h:item.size.h}});
	}

}




/*public*/function getDrawableZoomedIfNecessaryCoordinates2D(gameConfig, position, camera=null, size=null, center=null, zooms={zx:1,zy:1}){

	let resolution=gameConfig.getResolution();
	let vcenter=gameConfig.getViewCenterOffset();
	
	
	//const zoomCorrectionX=((vcenter.x+gameConfig.X_OFFSET)*(1-zooms.zx));
	//const zoomCorrectionY=((vcenter.y-gameConfig.Y_OFFSET)*(1-zooms.zy));
	const zoomCorrectionX=0;
	const zoomCorrectionY=0;

	let x;
	let y;
	
	let positionX=position.x;
	let positionY=position.y;
	

	if(!camera){
		x=(positionX+vcenter.x)+zoomCorrectionX;
		//(CAUTION : y coordinate is inverted !
		// We invert the coordinates system drawing:
		y=(resolution.h-(positionY+vcenter.y))+zoomCorrectionY;
	}else{
		let cameraPosition=camera.position.getParentPositionOffsetted();
		x=(positionX-cameraPosition.x+vcenter.x)+zoomCorrectionX;
		//(CAUTION : y coordinate is inverted !
		// We invert the coordinates system drawing:
		y=(resolution.h-(positionY-cameraPosition.y+vcenter.y))+zoomCorrectionY;
	}
	
	
	// For parallax :
	if(position.parallax){
		let parallaxFactor=(1/position.parallax);
		x*=parallaxFactor;
		y*=parallaxFactor;
	}
	
	let a=position.a;
	
	if(center && size && size.w && size.h){
		let zoomedCenteredCoords=getZoomedCenteredZoneCoords(x, y, size.w, size.h, center, zooms);
		return {x:zoomedCenteredCoords.x, y:zoomedCenteredCoords.y, a:a, center:center};
	}
	
	return {x:x*zooms.zx, y:y*zooms.zy, a:a, center:center};
}


/*public static*/function isInZoomedVisibilityZone(gameConfig,camera,position,size=null){
	
	const zooms=gameConfig.zooms;
	const resolution=gameConfig.getResolution();
	let totalWidth=Math.floor(resolution.w/zooms.zx);
	let totalHeight=Math.floor(resolution.h/zooms.zy);

	let result=Math.isInScreen(position,size,position.center,totalWidth,totalHeight,camera);
	return result;
}


/*public static*/function isZoomedVisibilityZoneCollidingWithZone(gameConfig,camera,zone){
	
	const zooms=gameConfig.zooms;
	const resolution=gameConfig.getResolution();
	let totalWidth=Math.floor(resolution.w/zooms.zx);
	let totalHeight=Math.floor(resolution.h/zooms.zy);

	let result=isZoneCollidingWithZone({x:camera.position.x,y:camera.position.y,w:totalWidth,h:totalHeight},getZoomedZone(zone,zooms));
	return result;
}

/*private static*/function getZoomedZone(zone,zooms={zx:1,zy:1}){
	if(zooms.zx<=0)	zooms.zx=1;
	if(zooms.zy<=0)	zooms.zy=1;
	return {x:zone.x*zooms.zx, y:zone.y*zooms.zy, w:zone.w*zooms.zx, h:zone.h*zooms.zy};
}




/*public*/window.getPositionFor2DObject=function(positionZone, xOffset=0, yOffset=0, avoidOverlap=null, size=null){
	let pos=getRandomPositionInZone(positionZone, xOffset, yOffset, null, avoidOverlap, size);
	let x=pos.x;
	let y=pos.y;
	
	let a=pos.a; /*angle on axis Z*/
	
	return {x:x,y:y,a:a, parallax:positionZone.parallax};
}

