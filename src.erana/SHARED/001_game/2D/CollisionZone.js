
class CollisionZone{

	constructor(){
	
	
		this.points=[];
	
	}

	init(gameConfig){
	
		this.gameConfig=gameConfig;
	}

	
	initAfterLevelLoading(){
		
		const concernedObject=this.parent;
		const src=getConventionalFilePath(this.gameConfig.basePath, concernedObject, "universe")+this.src;

		
		const self=this;
		(async ()=>{

			window.eranaScreen.tmpSvg.data=src;
		
//			document.body.appendChild(window.eranaScreen.tmpSvg);
			
			// Because the <object> element does not launch an onload event, and the <svg> element does not support an exterior SVG file reference !
			await ((milliseconds=500) => new Promise(resolve => setTimeout(resolve, milliseconds))) (500);
		
			self.collisionPolygon=self.getPolygonFromLayer(window.eranaScreen.tmpSvg.contentDocument);
			
//			document.body.removeChild(window.eranaScreen.tmpSvg);
		
		})();
		
		
	}
	

	/*private*/getPolygonFromLayer(svgDocument){
	
		const layer=svgDocument.getElementsByTagName("g")[0];
		const path=layer.firstElementChild;
		
		const polygonPointsCoordsString=path.getAttribute("d");
		
		
		this.points=getPointsFromSVGDString(polygonPointsCoordsString);
		
//		const polygonPointsCoordsStrings=path.getAttribute("d").split(" ");
//		const self=this;
//		foreach(polygonPointsCoordsStrings,(pointString)=>{
//			const splits=pointString.split(",");
//			self.points.push({x:parseFloat(splits[0]), y:parseFloat(splits[1])});
//		},(pointString)=>contains(pointString,","));


		// DBG
		console.log("POINTS:",this.points);
		
		
		
		this.points=getScaledPolygon(this.points, this.scaleX, this.scaleY);
		
		
	}
	
	
	
	
	// -------------------------------------
	
	
	draw(ctx,camera,lightsManager){
	
		if(empty(this.points))	return false;
		
		const parentSize=this.parent.size;
		const refPosition=this.parent.position.getParentPositionOffsetted();
		// CAUTION : collision zones are always centered at (x:"center";y:"center"), at this time !
		let points=getTranslatedPolygon(this.points,refPosition.x-parentSize.w*.5,refPosition.y-parentSize.h*.5);
		
		const angleRadians=this.parent.position.getAngle2D();
		points=getRotatedPolygon(points, angleRadians, refPosition);
		
	
		
		// DBG
		const self=this;
		ctx.save;
		ctx.beginPath();
		ctx.strokeStyle="#FF8888";
		foreach(points,(p,i)=>{
			let d=getDrawableZoomedIfNecessaryCoordinates2D(self.gameConfig, p, camera, null, null, self.gameConfig.zooms);
			if(i==0)	ctx.moveTo(d.x, d.y);
			else		ctx.lineTo(d.x, d.y);
		});
		ctx.closePath();
		ctx.stroke();
		ctx.lineWidth=2;
		ctx.restore();
		
	
	
		return true;
	}
	


}