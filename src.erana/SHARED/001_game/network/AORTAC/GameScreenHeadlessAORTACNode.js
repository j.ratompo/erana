
class GameScreenHeadlessAORTACNode extends GameScreenHeadless{

	constructor(mainId, config, storageKey) {
		super(config, storageKey);
		
		this.selfServerURL=nonull(getConsoleParam(0), "ws://127.0.0.1:40001");
		this.connectivityFactor=nonull(getConsoleParam(1), 0.5);
		
		this.aortacNode=null;
		
		
	}
	
	// INIT ON START
	/*protected*/initOnStartScreenAtLevelStart() {
		super.initOnStartScreenAtLevelStart();
	
		const aortacConfig=this.config.aortac;

		const model=this;
		const controller=this;

		if(!this.getSubclassRootContainer().isCinematic){
			const numberToGet=Math.round(getArraySize(aortacConfig.nodesURLs)*this.connectivityFactor);
			const outcomingNodes=Math.getRandomsInArray(aortacConfig.nodesURLs, numberToGet);
			this.aortacNode=getAORTACNode("node_"+getUUID("short"), this.selfServerURL, outcomingNodes, model, controller);
			this.aortacNode.start();
		}


	}
		
	// AORTAC METHODS
	
	/*public*/connectAORTACNode(){
		this.getNode().connect();
	}
	
	
	//=============== MODEL METHODS ===============
	
	// ***
	/*OVERRIDES*/split(){
		////
		return new GameModel(this.storageKey);
	}
	// ***
	/*OVERRIDES*/getObjectsWithinBoundaries(boundaries=null){
		const objects=[];
		////
		return objects;
	}
	// ***
	/*OVERRIDES*/replaceBy(otherModel){
		////
	}
	
	
	//=============== CONTROLLER METHODS ===============

	// ***
	/*OVERRIDES*/setNode(aortacNode){
		this.aortacNode=aortacNode;
	}

	// ***
	/*OVERRIDES*/getNode(){
		return this.aortacNode;
	}
	
	// ***
	/*OVERRIDES*/interpretInputs(inputs, subBoundaries){
		const modifiedObjects=[];
		
//		// We check if this inputs changes concerns our objects :
//		const modelObjects=this.model.getObjectsWithinBoundaries(subBoundaries);
//		if(empty(modelObjects))	return modifiedObjects;
//		
//		if(inputs.label==="XXXX"){
//			// We modify the model's objects of this node here : 
//			foreach(modelObjects, obj=>{
//				obj.label=inputs.label;
//				modifiedObjects.push(obj);
//			});
//			const modifiedObjectsIds=this.model.getObjectsIds(modifiedObjects);
//			return this.getNode().sendUpdatedObjects(modifiedObjects, modifiedObjectsIds);
//		}else if(inputs.label==="YYYY"){
//			
//			const newObjects=this.model.createObjects();
//
//			// We modify the model's objects of this node  here :
//			this.model.clientAddObjects(newObjects);
//
//			const newObjectsIds=this.model.getObjectsIds(newObjects);
//			return this.getNode().sendNewObjects(newObjects, newObjectsIds);
//		}

		return modifiedObjects;	
	}
	

}
