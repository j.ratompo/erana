
class GameScreenWithViewAORTACClient extends GameScreenWithView {

	constructor(mainId,config,storageKey) {
		super(mainId,config,storageKey);
		
		this.aortacClient=null;
	}
	
	// INIT ON START
	/*protected*/initOnStartScreenAtLevelStart() {
		super.initOnStartScreenAtLevelStart();
	
		const aortacConfig=this.config.aortac;
		
		
		const modelClient=this;
		const viewClient=this;
		
		if(!this.getSubclassRootContainer().isCinematic){
			// We connect to any of the starting server nodes :
			const serverNode=Math.getRandomInArray(aortacConfig.nodesURLs);
			this.aortacClient=getAORTACClient("client_"+getUUID(), serverNode, modelClient, viewClient);
		}
		
	}
	
	// AORTAC METHODS

	//=============== MODEL METHODS ===============
	
	/*OVERRIDES*/clientAddObjects(newObjects){
		////
	}
	// ***
	/*OVERRIDES*/clientUpdateObjects(otherObjects){
		////
	}
	// ***
	/*OVERRIDES*/clientMergeWith(otherModel){
		////
	}	
	
	
	//=============== VIEW METHODS ===============

	// ***
	/*OVERRIDES*/getBoundaries(){
		const boundaries=this.sceneManager.camera.getViewPort();
		return boundaries;
	}

	// ***
	/*OVERRIDES*/setClient(aortacClient){
		this.aortacClient=aortacClient;
	}

	// ***
	/*OVERRIDES*/getClient(){
		return this.aortacClient;
	}

	// ***
	/*OVERRIDES*/sendInputs(inputs, subBoundaries=null){
		if(!this.aortacClient)	return;
		this.getClient().sendInputs(inputs, subBoundaries);
	}

	// ***
	/*OVERRIDES*/display(){
		// DEBUG
		lognow(" VIEW DISPLAYS MODEL : ",this.model);
		
		
	}
	

}


