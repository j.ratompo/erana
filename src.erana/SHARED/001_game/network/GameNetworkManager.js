
	

class GameNetworkManager{

	constructor(gameConfig, gameScreen, controller, model) {


		this.controller=controller;
		this.model=model;

		this.gameConfig=gameConfig;

		// CONTROLLER
		this.eranaClient=null;
		this.gameScreen=gameScreen;
		
		// TECHNICAL ATTRIBUTES
		this.clientIDHolderElement=null;
		
		
	}
	

	/*public*/init(gameConfig){
		this.gameConfig=gameConfig;
		
		return this;
	}
	

	
	
	// MANDATORY METHODS
		
	/*private*/handleNetworkMechanicsClientSide(resolveAfterAllInitsMethod){
		
		const gameConfig=this.gameConfig;
		if(!gameConfig.gameCentralServer)	return false;
		
		
//		if(typeof(io)==="undefined"){
//			// TRACE
//			lognow("ERROR : «io» client subsystem not present, cannot launch connection to server. Aborting connection to server.");
//			return false;
//		}
		
		const gameCentralServerConfig=gameConfig.gameCentralServer;
		
		if(!gameCentralServerConfig.modelStrategy)	return false
		
		if(gameCentralServerConfig.modelStrategy==="polyverse"){
			
			// 0- Client creates (restores its model)
			// (at this point, it's already done.)
			
			let self=this;
			
			// We try to connect to the central server :
			self.eranaClient=initClient(false,false,function(serverSocket){
				
				// Client wants to register to server beforehand :
				serverSocket.send("protocol", {type:"1_registerClient"});
				
				serverSocket.receive("protocol", function(message) {
					
					// TRACE
					lognow("INFO : CLIENT : Server sent a message on the protocol channel");
					lognow("INFO : CLIENT : message from server :",message);

					
					if(message.error){
						const errorMessage="ERROR : CLIENT : Server returned an error : «"+message.error+"». Cannot proceed further in protocol.";
						
						// TRACE
						lognow(errorMessage);
						
						// DBG
						alert(i18n({"fr":"Erreur retournée du serveur :«"+errorMessage+"».","en":"Error returned from server :«"+errorMessage+"»."}));
						
						return;
					}
					
					
					// ============== CLIENT REGISTRATION PHASE ==============
					
					// 1- Client registers to server :
					if(message.type === "2_clientRegistered") {
						
						const clientID=message.clientID;
						// TRACE
						lognow("INFO : CLIENT : Client «"+clientID+"» registered to server.");
						
						self.eranaClient.clientID=clientID;
						self.eranaClient.isFollower=false;

						self.doOnNetworkRegistered(self.eranaClient.clientID);
						
						self.gameSCreen.doOnScreenReady(resolveAfterAllInitsMethod);

						// End of registration phase.
						return;
					}else if(message.type === "2_requestModel"){
						// ============== CLIENT JOIN GAME PHASE ==============
						

						// TRACE
						lognow("INFO : CLIENT : Server asked for model.");
						
						
						let modelToSend=self.gameScreen.getCurrentLevel();
						
						
						// We have to uncycle the model object we want to send :
//						let uncycledModelToSend=JSON.stringifyDecycle(modelToSend);
						
						let uncycledModelConfigToSend=self.controller.levelsManager.extractStaticConfig(self.prototypesConfig.allPrototypes, modelToSend);
						
						
						// 2- Concerned client must provide its model to server, so that server will then provide it to clients wanting to join its game :
						serverSocket.send("protocol", {type:"3_provideModel", clientIDToJoin:self.getClientID(), model:uncycledModelConfigToSend});



						
					// ============== CLIENT JOIN GAME PHASE ==============
					}else if(message.type === "4_clientJoined"){

						// 3- Client has already sent a join an existing game request, and is now receiving its model :
//						const model=message.model;
						const modelConfig=message.model;
						const clientIDToJoin=message.clientIDToJoin;
						
						
						
						// We have to recycle the model object we want to send :
//						const recycledModel=JSON.parseRecycle(model);
						
						let currentLevelName=self.controller.levelsManager.currentLevelName;
						let currentLevelConfig=modelConfig["GameLevel"][currentLevelName];
						
						let model=self.model.createLevelState(currentLevelName, currentLevelConfig, modelConfig);
						
						
						self.gameScreen.setCurrentLevel(model);
						
						
						// TRACE
						lognow("INFO : CLIENT : Client model is fully ready :",self.currentLevel);
						
						
						self.eranaClient.isFollower=true;
						self.eranaClient.joinedClientID=clientIDToJoin;
						
						
						// We notify all the other clients that we have joined :
						serverSocket.send("protocol", {type:"5_followerClientIsReady", clientIDToJoin:clientIDToJoin, clientID:self.getClientID()});
						
						
						self.gameSCreen.doOnScreenReady(resolveAfterAllInitsMethod);
						
						return;
					}else if(message.type === "6_followerClientIsReadyNotice"){

						const participantsClientsIDs=message.participantsClientsIDs;

						// DBG
						console.log("6_followerClientIsReadyNotice:",message);

						self.doOnNetworkUpdateParticipantsList(message.newClientID, participantsClientsIDs);
						
					}else if(message.type === "2_clientLeft"){
						
						self.eranaClient.isFollower=false;
						self.eranaClient.joinedClientID=null;
						
						return;
					}// End of joining game phase.
					
					
				});
				
				
				// ============== CLIENT ACTIONS PHASE ==============
				// 3)
				serverSocket.receive("clientProcessings", function(message) {
					
//					// TRACE
//					console.log("INFO : CLIENT : Client has received a processing to execute : ",message);
					
					self.executeProcessing(message, !!self.eranaClient.isFollower);// (forced to boolean)
								
					
				});
				
				
				self.registerAdditionalNetworkEvents(serverSocket);
				
			},
//		self.doOnNetworkError,
			gameCentralServerConfig.url,
			gameCentralServerConfig.port,
			gameCentralServerConfig.isSecure,
			gameCentralServerConfig.timeout,
			self).client.start();
			

			return true;
		}else{
			// Case «monoverse»
			// (This case is only for games where we don't want cheat, that is to say rules calculations are only made on server-side !)
			// TODO : DEVELOP
			throw new Error("ERROR : Unsupported network mode : «monoverse».");
		}
			
	}
	
	
	/*public*/getClientID(){
		if(!this.eranaClient || !this.isConnectedToServer() || !this.isConnectedAndRegisteredToServer())	return null;
		return this.eranaClient.clientID;
	}
	
	joinGame(){
		
		// ============== CLIENT JOIN GAME PHASE ==============

		if(!this.isConnectedToServer()){
			// TRACE
			lognow("WARN : No connection to erana server. Cannot join any game.");
			return;
		}

		// 1- Client wants to join an existing game :
		let self=this;
		promptWindow("Please enter game to join UUID :","textbox","",
			function(value){
			
				// Client wants to join an existing game :
				const clientIDToJoin=value;
				
				self.eranaClient.client.socketToServer.send("protocol", {type:"1_joinGame", clientID:self.getClientID(), clientIDToJoin:clientIDToJoin});
			
			}
		);
		
		
	}
	
	leaveGame(){
		
		// ============== CLIENT JOIN GAME PHASE ==============

		if(!this.isConnectedAndRegisteredToServer()){
			// TRACE
			lognow("WARN : No connection to erana server. Cannot leave any game.");
			return;
		}

		// 1- Client wants to leave a previously joined game :
		const clientIDToLeave=this.eranaClient.joinedClientID;
		this.eranaClient.client.socketToServer.send("protocol", {type:"1_leaveGame", clientID:this.getClientID(), clientIDToLeave:clientIDToLeave});
		
	}
	
	// If necessary, we replace the model with one from the network :
	/*private*/doOnNetworkRegistered(clientID){
		let element=this.clientIDHolderElement
		if(element){
			element.innerHTML=element.innerHTML.replace("%value%",clientID);
		}
	}
	
		
	
	
	
///*private*/doOnNetworkError(){
//		// TRACE
//		lognow("ERROR : Network error.");
//	}
	
	
	// CAUTION ! MUST NEVER BE CALLED IN THIS CLASS ! USE GameScreen.doOnScreenReady(...) INSTEAD ALWAYS !!
	/*NOT private*/doOnScreenReady(resolveAfterAllInitsMethod){
		const self=this;
		// An offline processing :		
		if(self.isConnectedToServer()){
			self.executeProcessing({type:"gameStarted", starterClientID:self.getClientID()},!!self.eranaClient.isFollower);// (forced to boolean));
		}
	}
	
	/*protected*/registerAdditionalNetworkEvents(serverSocket){
		/*DO NOTHING : OVERRIDE IN CHILD CLASSES*/
	}
	
	/*private*/doOnNetworkUpdateParticipantsList(newClientID, participantsClientsIDs){

		// An offline processing :		
		if(this.isConnectedToServer()){
			this.executeProcessing({type:"setAllPlayers", newClientID:newClientID, participantsClientsIDs:participantsClientsIDs },!!this.eranaClient.isFollower);// (forced to boolean));
		}

	}
	
	
	// A processing is an action or an event :
	// 1)
	dispatchProcessing(type, args){
		if(!this.isConnectedAndRegisteredToServer())		return false;
		let isFollower=(!!this.eranaClient.isFollower);// (forced to boolean)
		const processingToSend={originatingClientID:this.getClientID(), isFollower:isFollower, type:type};
		// (Ceci est just epour éviter de passer par «.args.» à chaque fois dans le code :)
		foreach(args,(arg, argName)=>{		processingToSend[argName]=arg;		});
		this.eranaClient.client.socketToServer.send("clientProcessings", processingToSend);
		return true;
	}
	
	/*private*/isConnectedToServer(){
		return !!this.eranaClient;
	}
	
	/*private*/isConnectedAndRegisteredToServer(){
		return !!(this.isConnectedToServer() && this.eranaClient.clientID);
	}
	
	// 4)
	/*private*/executeProcessing(rootContainer, processing, isFollower){
	
		// Delegating if possible to any business class container we find :
		if(!rootContainer.executeProcessing)	return false;
		rootContainer.executeProcessing(processing,isFollower);
		
		return true;
	}
	
	
	/*protected*/abandonGame(){
		if(this.isConnectedAndRegisteredToServer() && this.eranaClient.isFollower && this.eranaClient.joinedClientID){
			this.leaveGame();
		}
	}
	
	
	

}
