class GameCharacter{

	constructor() {

		this.isVisible=true;
		this.isMovable=true;
		
		
		this.idleTime=getNow();
		
		this.position=new Position(this);
		
		// TODO : Add speeds configuration.
		let moverConfig=nonull(this.moverConfig,{liberty:"2D",interruptable:true});
		this.mover=getMoverMonoThreaded(this,false,moverConfig);
		
		this.spt=new GameCharacterSprite(this);
		
	}

	// INIT
	init(gameConfig){
		
		this.gameConfig=gameConfig;
	
		if(contains(gameConfig.projection,"2D")){
			// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
			if(containsOneOf(gameConfig.scroll,["horizontal","bidimensional"])) {

				// Default direction :
				if(contains(gameConfig.scroll,"horizontal")) {
					this.direction="right";
					this.mover.setAbsoluteSpeedX(10);
				}else if(contains(gameConfig.scroll,"bidimensional")) {
					this.direction="S";
					this.mover.setBidimensionalSpeed(10);
				}

				this.position.center=this.imageConfig._2D.center;
			
			}else{
				// TODO : ...
			}
		}
		
		this.getGameCharacterSprite().init(gameConfig);
		
		
		this.getMover().setDurationTimeFactorHolder(window.eranaScreen);
		
	}
	
	
	// METHODS
	getGameCharacterSprite(){
		return this.spt;
	}
	
	getMover(){
		return this.mover;
	}
	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}

	getIsMovable(){
		return this.isMovable;
	}
	setIsMovable(isMovable){
		this.isMovable=isMovable;
	}
	
	
	startMovingTo(destination,followers=null,doOnEndSecondaryFunction=null,bidimensionalSpeed=null){
		let mover=this.getMover();
		if(!mover || (mover.isStarted() && mover.interruptable==false))	return;
		
		if(doOnEndSecondaryFunction)	mover.setDoOnStopSecondaryFunction(doOnEndSecondaryFunction);
		mover.setBidimensionalSpeed(bidimensionalSpeed);
		
		let gameConfig=this.gameConfig;
		if(contains(gameConfig.projection,"2D")){
			// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
			if(contains(gameConfig.scroll,"horizontal")) {
				
				this.direction=(mover.doStartUntilDestinationReach(destination, followers).x<0 ? "left":"right");
				
			}else if(contains(gameConfig.scroll,"bidimensional")) {
			
				mover.doStartUntilDestinationReach(destination, followers);
				this.direction=Math.getBidimensional8Direction(this.position, destination);

			}else{
				// TODO ...
			}
			
		}
		
		this.getGameCharacterSprite().loadSprite2DAndSet();
	}
	
	
	stopMoving(){
		this.getMover().stop();
	}
	
	// Do on stop method :
	doOnStop(){
		const self=this;
		self.getGameCharacterSprite().loadSprite2DAndSet();
		self.idleTime=getNow();
	}
	

	move(){
		
		// Mover stepping :
		// Only for monothreaded movers :
		this.getMover().doStep();
		
	}
	
	draw(ctx,camera,lightsManager){
		return this.getGameCharacterSprite().draw(ctx,camera,lightsManager);
	}


}
