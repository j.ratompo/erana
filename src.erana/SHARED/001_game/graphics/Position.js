class Position{

	constructor(parent,x=0,y=0,z=null,center=null,onChangePosition=null,centerOffsets=null,b=null/*on axis X*/,g=null/*on axis Y*/,a=null/*on axis Z (strange, but in the specification !)*/,parentPosition=null) {
		
		
		// CAUTION : We must have an access to the concerned object's size for some calculus !
		this.parent=parent;
		
		this.x=x;
		this.y=y;
		this.z=z;// NULLABLE
		this.parallax=null;// NULLABLE
		
		this.b=b;// NULLABLE
		this.g=g;// NULLABLE
		this.a=a;// NULLABLE
		
		// Caution : At this point, we cannot be sure that all concerned objects attributes have been set !
		
		// 2D
		this.center=nonull(center,{x:"left",y:"top"});
	
		this.centerOffsets=nonull(centerOffsets,{x:0,y:0,z:0, b:0,g:0,a:0});
		
		this.onChangePosition=onChangePosition;
		
		this.old=null;
		
		this.parentPosition=parentPosition;
		
		
	}
	
	setLocation(destination){

		this.old=this.clone();

		if(destination.x!=null)	this.x=destination.x;
		if(destination.y!=null)	this.y=destination.y;
		if(destination.z!=null)	this.z=destination.z;

		if(destination.b!=null)	this.b=destination.b;
		if(destination.g!=null)	this.g=destination.g;
		if(destination.a!=null)	this.a=destination.a;

		if(destination.parallax!=null)	this.parallax=destination.parallax;
		if(destination.center!=null)	this.center=destination.center;

		if(this.onChangePosition)	this.onChangePosition(this.parent);
		
		return this;
	}
	
	setLocationByDeltas(destinationDeltas){
		let destination={};
		if(destinationDeltas.x!=null)	destination.x=this.x+destinationDeltas.x;
		if(destinationDeltas.y!=null)	destination.y=this.y+destinationDeltas.y;
		if(destinationDeltas.z!=null)	destination.z=this.z+destinationDeltas.z;
		
		if(destinationDeltas.b!=null)	destination.b=this.x+destinationDeltas.b;
		if(destinationDeltas.g!=null)	destination.g=this.y+destinationDeltas.g;
		if(destinationDeltas.a!=null)	destination.a=this.z+destinationDeltas.a;
		this.setLocation(destination);
	}
	
	setRotation(angleX,angleY,angleZ){
		if(angleX!=null)	this.b=angleX;
		if(angleY!=null)	this.g=angleY;
		if(angleZ!=null)	this.a=angleZ;
		if(this.onChangePosition)	this.onChangePosition(this.parent);
	}
	

	clone(){
		return new Position(this.parent, this.x, this.y, this.z, this.center, this.onChangePosition, this.centerOffsets, this.b, this.g, this.a, this.parentPosition)
				 .setParallax(this.parallax);
	}
	
	
	setParallax(parallax){
		this.parallax=parallax;
		return this;
	}
	
	// CAUTION : Avoid to use this method directly, use setParentPosition(...) instead
	setOffsets(offsettedPos){
		this.centerOffsets.x=offsettedPos.x;
		this.centerOffsets.y=offsettedPos.y;
		this.centerOffsets.z=offsettedPos.z;

		this.centerOffsets.b=offsettedPos.b;
		this.centerOffsets.g=offsettedPos.g;
		this.centerOffsets.a=offsettedPos.a;
		return this;
	}
	
	/*public*/setParentPosition(parentPosition){
		this.parentPosition=parentPosition;
		return this;
	}
	/*public*/getParentPosition(){
		return this.parentPosition;
	}
	
	// OLD : NO ! :
//	isInScreen(screenSize,camera){...
	
	// 2D
	getExtremities(scaleFactor=1){
		return {min:{x:this.getLeft(scaleFactor),y:this.getBottom(scaleFactor)},max:{x:this.getRight(scaleFactor),y:this.getTop(scaleFactor)}};
	}

	getDeltas(){
		if(!this.old)	return {x:0,y:0,z:0, b:0,g:0,a:0};
		return {x:this.x-this.old.x, y:this.y-this.old.y, z:this.z-this.old.z,
				b:(this.b==null?null:Math.coerceAngle(this.b-this.old.b,true,false)),
				g:(this.g==null?null:Math.coerceAngle(this.g-this.old.g,true,false)),
				a:(this.a==null?null:Math.coerceAngle(this.a-this.old.a,true,false))
		};
	}
	
	
//	collidesWith(otherObject){
//		// TODO : Develop...
//	}

	// 2D
	getLeft(scaleFactor=1){
		var size=(this.parent.size || !this.parent.getSize ? this.parent.size : this.parent.getSize());
		if(!size)	return this.x;
		if(this.center.x==="center"){
			return this.x-Math.floor(size.w*scaleFactor*.5);
		}else if(this.center.x==="right"){
			return this.x-size.w*scaleFactor;
		}else{
			// Case "left"
			return this.x;
		}
	}
	
	// 2D
	getRight(scaleFactor=1){
		var size=(this.parent.size || !this.parent.getSize ? this.parent.size : this.parent.getSize());
		if(!size)	return this.x;
		if(this.center.x==="center"){
			return this.x+Math.floor(size.w*scaleFactor*.5);
		}else if(this.center.x==="right"){
			return this.x;
		}else{
			// Case "left"
			return this.x+size.w*scaleFactor;
		}
	}

	// 2D
	getBottom(scaleFactor=1){
		var size=(this.parent.size || !this.parent.getSize ? this.parent.size : this.parent.getSize());
		if(!size)	return this.y;
		
		if(this.center.y==="center"){
			return this.y-Math.floor(size.h*scaleFactor*.5);
		}else if(this.center.y==="bottom"){
			return this.y;
		}else{
			// Case "top"
			return this.y-size.h*scaleFactor;
		}
	}
	
	// 2D
	getTop(scaleFactor=1){
		var size=(this.parent.size || !this.parent.getSize ? this.parent.size : this.parent.getSize());
		if(!size)	return this.y;
		if(this.center.y==="center"){
			return this.y+Math.floor(size.h*scaleFactor*.5);
		}else if(this.center.y==="bottom"){
			return this.y+size.h*scaleFactor;
		}else{
			// Case "top"
			return this.y;

		}
	}
	
	
	
	
	// CAUTION : getOffsetted() returns an object that is not another Position object, instead, it's a lightweight object !!
	/*private*/getOffsetted(parentPositionParam=null){
	
	
		let parentPositionOffsets=(parentPositionParam?parentPositionParam.getParentPositionOffsetted():{x:0,y:0});
		const offsets=this.centerOffsets;
		const result={
					x:this.x+offsets.x+parentPositionOffsets.x,
					y:this.y+offsets.y+parentPositionOffsets.y,/*(CAUTION : y coordinate is inverted !)*/
					z:(this.z==null?null:(this.z+nonull(offsets.z,0)+nonull(parentPositionOffsets.z,0))),

					b:(this.b==null?null:Math.coerceAngle(this.b+nonull(offsets.b,0)+nonull(parentPositionOffsets.b,0),true,true)),
					g:(this.g==null?null:Math.coerceAngle(this.g+nonull(offsets.g,0)+nonull(parentPositionOffsets.g,0),true,true)),
					a:(this.a==null?null:Math.coerceAngle(this.a+nonull(offsets.a,0)+nonull(parentPositionOffsets.a,0),true,true)),
					
					parallax:this.parallax,
					parent:this.parent,
					center:this.center,
					centerOffsets:this.centerOffsets,
					onChangePosition:this.onChangePosition,
					old:this.old,
					getAngle2D:()=>{	return result.a;	},
					setAngle2D:(a)=>{	result.a=a;	},
		};
		return result;
	}
	
	// CAUTION : getOffsetted() returns an object that is not another Position object, instead, it's a lightweight object !!
	/*public*/getParentPositionOffsetted(){
		if(!this.getParentPosition())	return this.getOffsetted();
		return this.getOffsetted(this.getParentPosition());
	}
	
	
	
	
	
	
	hasChanged(){
		if(!this.old)	return false;
		return this.old.x!==this.x || this.old.y!==this.y || this.old.z!==this.z
			|| this.old.b!==this.b || this.old.g!==this.g || this.old.a!==this.a;
	}
	
	// 2D
	hasReached(goalZone){
		let point=this.getParentPositionOffsetted();
		
		//	if(destination.x!=null && destination.x-margins.x<offsetted.x && offsetted.x<destination.x+margins.x)

		return window.isInZone(point, goalZone);
	}
	
	
	// CAUTION : getGlued() returns an object that is not another Position object, instead, it's a lightweight object !!
	getGlued2D(){
	
		if(!this.getParentPosition())	return this.getParentPositionOffsetted();
	
		
		const parentPos=this.getParentPosition().getParentPositionOffsetted();
		const parentPos2D={x:parentPos.x, y:parentPos.y, a:parentPos.a};
		
		const pos=this.getParentPositionOffsetted();
		const transformedPoint2D=Math.rotateAround(
								Math.coerceAngle(parentPos2D.a,true,true),
								pos.x,pos.y,
								parentPos2D.x, parentPos2D.y
								);

								
		const transformedAngle2D=Math.coerceAngle(this.getAngle2D()+parentPos2D.a,true,true);
		
		const result={
				x:transformedPoint2D.x,
				y:transformedPoint2D.y,
				z:this.z,

				b:this.b,
				g:this.g,
				a:transformedAngle2D,
				getAngle2D:()=>{	return result.a;	},
				setAngle2D:(a)=>{	result.a=a;	},
		};
		
		return result;
	}
	

	getAngle2D(){
		return this.a;
	}
	setAngle2D(angle){
		this.a=angle;
	}
	
	
}