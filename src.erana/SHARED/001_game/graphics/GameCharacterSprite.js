class GameCharacterSprite extends GameAnimation{

	// Two-states : started, stopped.

	constructor(concernedObject) {
		super(concernedObject);
		
	}
	

	// INIT
	init(gameConfig, actionTypeParam=null){
		super.init(gameConfig, actionTypeParam);

//		this.gameConfig=gameConfig;
//
////	this.imageScale=this.concernedObject.imageConfig._2D.imageScale;
		
		this.loadSprite2DAndSet();
		
		
	}
	
	
	// METHODS


	/*OVERRIDES*//*private*/loadSprite2DAndSet(actionTypeParam=null){

		this.actionType=actionTypeParam;
		
		
		let imageConfig=this.concernedObject.imageConfig;
		
		let gameConfig=this.gameConfig;

		let imageOnlySrcConfig;
		if(contains(gameConfig.projection,"2D")){
		
			let direction=this.concernedObject.direction;
			let isMoving=this.concernedObject.getMover().isStarted();
		
			// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
			if(contains(gameConfig.scroll,"horizontal")) {
				
				if(!isMoving){
		
					// Standing animations :
		
					if(this.actionType==="idle"){
						// Idle animations :
						let idleImagesPaths=imageConfig._2D.idles.sourcesPaths;
						imageOnlySrcConfig=Math.getRandomInArray(idleImagesPaths);
						
					}else{ // "stand"
						if(direction==="right")	imageOnlySrcConfig=imageConfig._2D.horizontal.standbyRight;
						else					imageOnlySrcConfig=imageConfig._2D.horizontal.standbyLeft;

						this.actionType="stand";
					}
				
				}else{ // "walk"
		
					// Moving animations :
					if(direction==="right")	imageOnlySrcConfig=imageConfig._2D.horizontal.walkRight;
					else					imageOnlySrcConfig=imageConfig._2D.horizontal.walkLeft;
					
					this.actionType="walk";
				}


			}else if(contains(gameConfig.scroll,"bidimensional")) {
			
			
				if(!isMoving){
		
					// Standing animations :
		
					if(this.actionType==="idle"){
						// Idle animations :
						let idleImagesPaths=imageConfig._2D.idles.sourcesPaths;
						imageOnlySrcConfig=Math.getRandomInArray(idleImagesPaths);
						
					}else{ // "stand"
					
						imageOnlySrcConfig=imageConfig._2D.bidimensional8.standby[direction];
						
						this.actionType="stand";
					}
				
				}else{ // "walk"
		
					// Moving animations :
					imageOnlySrcConfig=imageConfig._2D.bidimensional8.walk[direction];
					
					this.actionType="walk";
				}



			}else{
				// TODO : ...
			
			}
		}

		if(imageOnlySrcConfig){
			let imgSrc=null;
			if(isString(imageOnlySrcConfig)){
				imgSrc=imageOnlySrcConfig;
			}else{
				if(imageOnlySrcConfig.clipSize)				this.clipSize=imageOnlySrcConfig.clipSize;
				if(imageOnlySrcConfig.refreshMillis)	this.refreshMillis=imageOnlySrcConfig.refreshMillis;
				imgSrc=imageOnlySrcConfig.src;
			}
	
			super.loadAnimatedImageClip(imgSrc);
		}
		
	}
	

	draw(ctx,camera,lightsManager){
		
		let gameConfig=this.gameConfig;
		
		// CAUTION ! Coordinates for DRAWING are not the same than for CALCULUS !
		const drawableUnzoomedCoordinates=getDrawableZoomedIfNecessaryCoordinates2D(gameConfig, super.getOffsettedRefPosition(), camera);
		if(!super.canDraw(camera))	return false;


		if(contains(gameConfig.projection,"2D")){
		
			// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
			if(containsOneOf(gameConfig.scroll,["horizontal","bidimensional"])) {
								
				// Idle animations :
				var delayIdleMillis=this.concernedObject.imageConfig._2D.idles.triggerAfterMillis;
				let durationTimeFactor=window.eranaScreen.getDurationTimeFactor();
				if(		!this.concernedObject.getMover().isStarted() 
						&& hasDelayPassed(this.concernedObject.idleTime, delayIdleMillis*durationTimeFactor)){
					this.concernedObject.idleTime=getNow();
					this.loadSprite2DAndSet("idle");
				}
				
				const zooms=gameConfig.zooms;
				
				// CAUTION ! Coordinates for DRAWING are not the same than for CALCULUS !
				const x=drawableUnzoomedCoordinates.x;
				const y=drawableUnzoomedCoordinates.y;
				
				
				// TODO : FIXME : ASPECT RATIO SEEMS WRONG !
				this.animation.drawOnEachStepZoomedIfNeeded(ctx, x, y);
				
				
				// TODO : ...
//				// Lighting effects :
//				if(lightsManager)	lightsManager.drawLightsZoomedIfNeeded(ctx, camera, realImageCoords, 1, this.darkImage, this.bumpImage, this.image);
			
				
			}else{
				// TODO ...
			}
		}

		return true;
	}
	
	
	
}
