const DEFAULT_SHADOW_WIDTH=100;

class GameSunShadow{

	constructor() {

		this.isVisible=true;
		

	}
	
	// INIT
	init(gameConfig){
		
		this.gameConfig=gameConfig;
		
		
	}
	

	
	// METHODS
	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	
	
	
	draw(ctx, camera, lightsManager){
		
		if(!this.getIsVisible() || !this.parent.getGameImage().canDraw(camera))	return false;
		
		let coords=this.parent.position.getParentPositionOffsetted();
		
		let lightPercent=lightsManager.getLightsPercent();
		let thickness=this.imageConfig._2D.horizontal.thickness;
		let groundHeight=this.imageConfig._2D.horizontal.groundHeight;
		let sunSide=this.imageConfig._2D.horizontal.sunSide;
		let shadowColor=this.shadowColor;
		let blur=this.blur;
		
		
		
		ctx.save();
		
		// Caution : Here, darkening is inverted :
		ctx.globalAlpha=Math.roundTo( (Math.coerceInRange(lightPercent+this.minimalDarkPercent,0,100)*.01 ), 2);
		
		
		let demiWidth=Math.floor(this.parent.size.w/2);
		
		
		let angle=(lightPercent*.01)*(this.maxAngle-this.minAngle)+this.minAngle;
		if(sunSide==="left"){
			angle=Math.coerceAngle(180-angle);
		}

		let groundY=coords.y-groundHeight;
		
		let extremityRight= {x:coords.x+demiWidth, y:coords.y};
		let projectedExtremityRight=getSunShadowProjectedPoint(extremityRight,angle,groundY);

		let extremityLeft= {x:coords.x-demiWidth, y:coords.y};
		let projectedExtremityLeft=getSunShadowProjectedPoint(extremityLeft,angle,groundY, 10, 2);
		
		const zooms=this.gameConfig.zooms;
		let drawableProjectedExtremityZoomedLeft=getDrawableZoomedIfNecessaryCoordinates2D(this.gameConfig, projectedExtremityLeft, camera, null, null, zooms);
		let drawableProjectedExtremityZoomedRight=getDrawableZoomedIfNecessaryCoordinates2D(this.gameConfig, projectedExtremityRight, camera, null, null, zooms);
		
		
//		let deltaX=extremityLeft.x-projectedExtremityLeft.x;
//		ctx.transform(1, 0, -deltaX*.1, 1, 0, 0);
//		ctx.fillStyle="#000000";
//		ctx.fillRect(drawableProjectedExtremityZoomedLeft.x, drawableProjectedExtremityZoomedLeft.y,
//				drawableProjectedExtremityZoomedRight.x-drawableProjectedExtremityZoomedLeft.x,
//				thickness); 

		ctx.beginPath();

		
		ctx.moveTo(drawableProjectedExtremityZoomedLeft.x, drawableProjectedExtremityZoomedLeft.y);
		ctx.lineTo(drawableProjectedExtremityZoomedRight.x,drawableProjectedExtremityZoomedRight.y);
		


		
		
		ctx.shadowColor = shadowColor;
		ctx.shadowBlur = blur;
		ctx.shadowOffsetY = -thickness/2;
		
		
//		// TODO : FIXME : Use a nice image instead.
//		// Gradient
//		let gradient = ctx.createLinearGradient(x, y-demiHeight, x, y+demiHeight);
////	let gradient = ctx.createRadialGradient(x, y, 10, x, y, thickness);
//		gradient.addColorStop(0, "#000000");
////	gradient.addColorStop(.5, "#000000");
//		gradient.addColorStop(1, "transparent");
//		ctx.strokeStyle=gradient;
		
		
//	ctx.lineWidth=thickness;
		ctx.strokeStyle=shadowColor;
		ctx.stroke();
		
		
//	ctx.fillStyle=shadowColor;
//	ctx.fill();
		
		
		ctx.restore();

		
		
		
		return true;
	}
	
	
}
