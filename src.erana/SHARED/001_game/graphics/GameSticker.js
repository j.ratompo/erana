class GameSticker extends GameFlyer{

	constructor() {
		super();

		this.textLinesAlignY="bottom"; // (default value)
		
	}
	
	// INIT
	init(gameConfig){
		super.init(gameConfig);
		


	}
	
	// METHODS
	

	
	/*private*/calculateDynamicVisibilityForGameSticker(camera){
		
		// If we are in the last sticker of the cinematics :
		if(this.goTo && !empty(this.gameLineTriggers)){
			
			let allLinesHasBeenShown=true;
			foreach(this.gameLineTriggers,(line)=>{;
				if(!line.hasBeenShown){
					allLinesHasBeenShown=false;
				}
			});
			if(allLinesHasBeenShown){
				
				window.eranaScreen.pagesManager.goToPage(this.goTo);
				if(this.action) {
					executeControllerAction(window.eranaScreen,this.action,this.args);
				}
				
				return false;
			}
		}
		
		return true;
	}
	
	
	

	getDrawables(){
		return [
//			this.gameInteractables,
				this.gameLineTriggers
			];
	}
	

	
	
	// ---------------------------------------------
	
	drawInUI(ctx,camera){
		/*DO NOTHING : Only to allow its children to draw on the "ui" layer level.*/
	}

	
	draw(ctx, camera, lightsManager){
		
	// NO : We must be able to display GameStickers with no images and only text lines !
//		let img=super.getGameImage();
//		if(!img)	return false;

		let calculatedDynamicVisibility=this.calculateDynamicVisibilityForGameSticker(camera);
		if(!calculatedDynamicVisibility)
			return false;
		
		let canDraw=super.draw(ctx, camera, lightsManager);
		
//		// DBG
//		lognow("canDraw:",canDraw);
		
		return canDraw;
	}
	
	
}
