class GameImage{


	constructor(concernedObject) {

		this.concernedObject=concernedObject;
		this.image=null;
		
		this.extensions={};
		
		this.noImageBoxSize=null;
		this.scaleW=1;
		this.scaleH=1;

	}
	
	
	// INIT
	init(gameConfig,confImageParam=null,overriddenBasePath=null){
		
		this.gameConfig=gameConfig;

		let confImage=nonull(confImageParam,this.concernedObject.imageConfig);
		
		let confImage2D=confImage._2D;
		
		
		let sourcePathConfig=confImage2D;
		// Chirality handling :
		if(this.concernedObject.getChirality && confImage2D.chiralities){
			let chirality=this.concernedObject.getChirality();
			let foundSourcePathConfig=confImage2D.chiralities[chirality];
			if(foundSourcePathConfig)	sourcePathConfig=foundSourcePathConfig;
		}
		
		let imgSrcBaseName=null;
		const imgSrc=(sourcePathConfig.sourcePaths?Math.getRandomInArray(sourcePathConfig.sourcePaths):sourcePathConfig.sourcePath);
		if(imgSrc){
			if(isString(imgSrc)){
				imgSrcBaseName=imgSrc;
				this.image=this.loadImage(imgSrcBaseName,overriddenBasePath);
			}else{
				// TODO : FIXME : Being able to process lights even if we have no base image !
				if(imgSrc.size){
					this.noImageBoxSize=imgSrc.size;
					this.scaleW=1;
					this.scaleH=1;
				}
				if(imgSrc.baseName){
					imgSrcBaseName=imgSrc.baseName;
				}
			}
		}

		if(sourcePathConfig.sourcePathBack)		this.backImage=this.loadImage(sourcePathConfig.sourcePathBack,overriddenBasePath);
		if(sourcePathConfig.sourcePathFront)	this.frontImage=this.loadImage(sourcePathConfig.sourcePathFront,overriddenBasePath);
		
		// Lighting effects :
		if(sourcePathConfig.sourcePathDark)			this.darkImage=this.loadImage(sourcePathConfig.sourcePathDark,overriddenBasePath);
		if(sourcePathConfig.sourcePathBackDark)	this.backDarkImage=this.loadImage(sourcePathConfig.sourcePathBackDark,overriddenBasePath);
		if(sourcePathConfig.sourcePathBump)			this.bumpImage=this.loadImage(sourcePathConfig.sourcePathBump,overriddenBasePath);

//	if(confImage.colorLayer)	this.colorLayer=confImage.colorLayer;

		if(imgSrcBaseName){
			foreach(this.extensions,(extension,extensionName)=>{
				const extensionSourcePath=imgSrcBaseName.replace(".",("."+extensionName+"."));
				extension.img=this.loadImage(extensionSourcePath,overriddenBasePath);
			});
		}
		
	}
	
	// METHODS

	/*private*/loadImage(imageOnlySrc,overriddenBasePath=null){
		let asyncId=window.eranaScreen.addAsyncStart();
		const self=this;
		return loadImageInUniverse(this.gameConfig,imageOnlySrc,this.concernedObject,overriddenBasePath,(image)=>{
				self.scaleW=image.scaleW;
				self.scaleH=image.scaleH;
     		window.eranaScreen.addAsyncEnd(asyncId);
		});
	}
	
	/*public*/addExtensionImage(extensionName, extensionInfos={img:null,opacity:null}){
		this.extensions[extensionName]=extensionInfos;
		return this;
	}

	
	/*public*/canDraw(camera, ignoreParentsPositionsStack=false
//	,realImageCoords=null
	){
		
		if(!this.image && !this.noImageBoxSize)	return false;
		
		if(contains(this.gameConfig.projection,"2D")){
	
			// TODO : Activate later :
			// We skip the calculus and stick to the old visiblity result if position hasn't changed :
	//		if(typeof(this.oldVisibility)!=="undefined"
	//				&& !this.getRefPosition().hasChanged() && !camera.position.hasChanged())
	//			return this.concernedObject.oldVisibility;
				
			if(!this.concernedObject.getIsVisible() || !this.isReady())	return false;
			
			// // DBG
			//if(!this.concernedObject.prototypeName)
			//	console.log("BOGUS",this.concernedObject);
			

			// CAUTION : Coordinates for DRAWING are not the same than for CALCULUS !
			// TODO : FIXME : DUPLICATED CODE
			let pos;
			if(ignoreParentsPositionsStack){
				pos=this.getRefPosition().getOffsetted();
			}else{
				pos=this.getOffsettedRefPosition();
			}

			const inZone=isInZoomedVisibilityZone(this.gameConfig, camera, pos, nonull(this.getRefSize(), this.getImageSize() ) );
//		}
			
			
			if(!inZone) {
				this.oldVisibility=false;
				return false;
			}
			
			this.oldVisibility=true;
			return true;
		}

		return true;
	}
	
	
	/*public*/isReady(){
		if(!this.image && !this.noImageBoxSize)	return false;
		if(this.image)	return this.image.isReady && this.image.width && this.image.height;
		// Case no actual image :
		return true;
	}

	/*public*/getImageSize(){
		if(!this.image && !this.noImageBoxSize)	return null;
		if(this.image)	return {w:this.image.width, h:this.image.height};
		// Case no actual image :
		return this.noImageBoxSize;
	}
	
	/*private*/getRefPosition(){
		return this.concernedObject.position;
	}
	
	/*private*/getRefSize(){
		return this.concernedObject.size;
	}
	
	
	/*private*/getRealImageCoords(camera, ignoreParentsPositionsStack=false){
		
		// CAUTION ! Coordinates for DRAWING are not the same than for CALCULUS !
		
		// TODO : FIXME : DUPLICATED CODE
		let pos;
		if(ignoreParentsPositionsStack){
			pos=this.getRefPosition().getOffsetted();
		}else{
			pos=this.getOffsettedRefPosition();
		}
		
		// CAUTION ! Coordinates for DRAWING are not the same than for CALCULUS !
		const drawableUnzoomedCoordinates=getDrawableZoomedIfNecessaryCoordinates2D(this.gameConfig, pos, camera);
		const x=drawableUnzoomedCoordinates.x;
		const y=drawableUnzoomedCoordinates.y;
		const a=drawableUnzoomedCoordinates.a;
		
		const zooms=this.gameConfig.zooms;
		
		const refSize=this.getRefSize();
		const refImageSize=this.getImageSize();
		return {
			zooms:zooms,
			x:x,y:y, a:a,
			w:refSize?refSize.w:refImageSize.w,
			h:refSize?refSize.h:refImageSize.h,
			center:this.getRefPosition().center,
			scaleW:this.scaleW,
			scaleH:this.scaleH
		};
		
	}

	// TODO : FIXME : DUPLICATED CODE ! (see in GameAnimation class)
	/*private*/getOffsettedRefPosition(){
		const parentOfConcernedObject=this.concernedObject.parent;
		// OLD :
		if(!parentOfConcernedObject || !parentOfConcernedObject.getPosition || parentOfConcernedObject.getPosition().getAngle2D()==null){
			return this.getRefPosition().getParentPositionOffsetted();
		}
		return this.getRefPosition().getGlued2D();
	}
	
	
	
	
	// -----------------------------------------------------

	
	/*public*/drawExtensionImage(ctx,camera,extensionName,opacity=1,useSize=false){
		
		if(empty(this.extensions))	return false;
		if(!this.extensions[extensionName])	return false;
		
		const extensionImageConfig=this.extensions[extensionName];
		const extensionImage=extensionImageConfig.img;
		if(!extensionImage)	return false;
		return this.drawSimpleImage(ctx,camera,extensionImage,nonull(extensionImageConfig.opacity,opacity),useSize);
	}


	/*public*/drawSimpleImage(ctx,camera,image,opacity=1,useSize=false){
		
		if(!image)	return false;
		
		if(contains(this.gameConfig.projection,"2D")){
							
			let realImageCoords=this.getRealImageCoords(camera);
			if(!this.canDraw(camera))	return false;
			
			const refSize=this.getRefSize();
		
			drawImageAndCenterWithZooms(ctx, image,
					realImageCoords.x,
					realImageCoords.y, 
					realImageCoords.zooms,
					realImageCoords.center, 
					(useSize?null:(realImageCoords.scaleW)),
					(useSize?null:(realImageCoords.scaleH)),
					(useSize?(refSize.w):null),
					(useSize?(refSize.h):null),
					opacity,
					false,
					null,
					realImageCoords.a);
		
		}
		
		return true;
	}
	
	
	/*public*/drawAbsolute(ctx,camera,lightsManager=null,opacity=1,useSize=false){
		return this.draw(ctx,camera,lightsManager,opacity,useSize,false,false,true);
	}

	/*public*/draw(ctx,camera,lightsManager=null,opacity=1,useSize=false,preserveContextState=false,ignoreRaysCasting=false,ignoreParentsPositionsStack=false){
		
//		if(!this.image)	return false;

		if(contains(this.gameConfig.projection,"2D")){
							
			let realImageCoords=this.getRealImageCoords(camera, ignoreParentsPositionsStack);
			if(!this.canDraw(camera))	return false;
			
			if(this.image){
				
				const refSize=this.getRefSize();
				drawImageAndCenterWithZooms(ctx, this.image,
					realImageCoords.x,
					realImageCoords.y, 
					realImageCoords.zooms,
					realImageCoords.center, 
					(useSize?null:(realImageCoords.scaleW)),
					(useSize?null:(realImageCoords.scaleH)),
					(useSize?(refSize.w):null),
					(useSize?(refSize.h):null),
					opacity,
					preserveContextState,
					null,
					realImageCoords.a);
			}
				
			
			// Lighting effects :
			if(lightsManager){
				lightsManager.drawLightsZoomedIfNeeded(ctx,camera,realImageCoords, 1, this.darkImage, this.bumpImage, this.image, this.noImageBoxSize, ignoreRaysCasting);
			}

		}
		
		return true;
	}
	
	/*public*/drawInBack(ctx,camera,lightsManager,opacity=1,useSize=false,ignoreRaysCasting=false){
		
		if(!this.drawSimpleImage(ctx,camera,this.backImage,opacity,useSize))	return false;
		
		let realImageCoords=this.getRealImageCoords(camera);

		// Lighting effects :
		if(lightsManager)	
			lightsManager.drawLightsZoomedIfNeeded(ctx,camera,realImageCoords, .75, 
					this.backDarkImage, /*TODO : */null, this.backImage, null,
					ignoreRaysCasting);

		
		return true;
	}


	/*public*/drawInFront(ctx,camera,opacity=1,useSize=false){
		return this.drawSimpleImage(ctx,camera,this.frontImage,opacity,useSize);
	}
	
	
}
