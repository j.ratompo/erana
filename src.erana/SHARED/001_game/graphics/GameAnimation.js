
class GameAnimation{

	// Two-states : started, stopped.
	

	constructor(concernedObject) {

		this.concernedObject=concernedObject;
		this.animation=null;

		// Case not a clip animation, but instead a pool of individual images :
		this.loadedImages=null;
		this.darkImage=null;
		this.scaleW=1;
		this.scaleH=1;

		this.clipSize=null;
		this.refreshMillis=null;
		this.center=null;
		this.offsets=null;
		
		this.started=false;
	}
	
	// INIT

	init(gameConfig, actionTypeParam="default", overriddenBasePath=null){
		
		this.gameConfig=gameConfig;

		const sourcePathConfig=this.concernedObject.imageConfig._2D;
		
		this.clipSize=sourcePathConfig.clipSize;
		this.refreshMillis=sourcePathConfig.refreshMillis;
		

		if(sourcePathConfig.sourcePathDark)			this.darkImage=this.loadImage(sourcePathConfig.sourcePathDark,overriddenBasePath);


//	if(sourcePathConfig.colorLayer)	this.colorLayer=sourcePathConfig.colorLayer;
		
		
		return this;
	}
	
	
	// METHODS
	
	// TODO : FIXME : DUPLICATED CODE ! (see in GameImage class)
	/*private*/loadImage(imageOnlySrc,overriddenBasePath=null){
		let asyncId=window.eranaScreen.addAsyncStart();
		const self=this;
		return loadImageInUniverse(this.gameConfig,imageOnlySrc,this.concernedObject,overriddenBasePath,(image)=>{
				self.scaleW=image.scaleW;
				self.scaleH=image.scaleH;
     		window.eranaScreen.addAsyncEnd(asyncId);
		});
	}


	/*protected*/loadSprite2DAndSet(actionTypeParam="default", otherImageConfigName=null, specificSize=null){

		// Animations by actions :
		this.actionType=actionTypeParam;

		let imageConfig2D=this.concernedObject[nonull(otherImageConfigName,"imageConfig")]._2D;
		if(!imageConfig2D.animations)	return this;
		this.center=imageConfig2D.center;
		this.offsets=imageConfig2D.offsets;

		let imageOnlySrc=imageConfig2D.animations[this.actionType];
		if(!imageOnlySrc)	return this;
		
		if(!this.clipSize)	this.clipSize=imageConfig2D.clipSize;
		
		
		if(this.clipSize){
			if(isArray(imageOnlySrc))	imageOnlySrc=Math.getRandomInArray(imageOnlySrc);
			this.loadAnimatedImageClip(imageOnlySrc, specificSize);
		}else{
			this.loadAnimatedImageRandom(imageOnlySrc, specificSize);
		}

		return this;
	}


	
	/*private*/loadAnimatedImageClip(imageOnlySrc, specificSize=null){
	
		let result=new Image();
		
		result.src=getConventionalFilePath(this.gameConfig.basePath,this.concernedObject,"universe")+imageOnlySrc;
		
		let self=this;
		result.onload=function(event){
			
			const zooms=self.gameConfig.zooms;

			self.imageScaleW=1;
			self.imageScaleH=1;
			if(self.concernedObject.size){

				if(!specificSize){
					let size=self.concernedObject.size;
					if(size.w && self.clipSize.w)	self.imageScaleW=size.w/self.clipSize.w;
					if(size.h && self.clipSize.h)	self.imageScaleH=size.h/self.clipSize.h;
				}else{
					if(specificSize.w && self.clipSize.w)	self.imageScaleW=specificSize.w/self.clipSize.w;
					if(specificSize.h && self.clipSize.h)	self.imageScaleH=specificSize.h/self.clipSize.h;
				}
				
			}
			
			//const zooms=selfParam.gameConfig.zooms;
			self.animation=getSpriteMonoThreaded(result,
					self.refreshMillis,
					self.clipSize,
					self.imageScaleW,
					self.imageScaleH,
					"horizontal",
					(self.offsets?nonull(self.offsets.x,0):0),(self.offsets?nonull(self.offsets.y,0):0),true,false,
					nonull(self.center, self.concernedObject.position.center),
					zooms
			).setDurationTimeFactorHolder(window.eranaScreen);
			
			self.animation.isReady=true;
		};
	}
	
	
	

	
	/*private*/loadAnimatedImageRandom(imagesOnlySrcs, specificSize=null){
	
		this.loadedImages=[];
		
		
		let numberOfImages=imagesOnlySrcs.length;
		

		let self=this;
		foreach(imagesOnlySrcs,(imgSrc)=>{
			
			
			let result=new Image();
			result.self=self;
			
			result.src=getConventionalFilePath(this.gameConfig.basePath,this.concernedObject,"universe")+imgSrc;
			result.onload=function(event){
				
				let img=event.target;
				let selfParam=img.self;
				
				const zooms=self.gameConfig.zooms;

	
				selfParam.imageScaleW=1;
				selfParam.imageScaleH=1;
				if(selfParam.concernedObject.size){
				
					if(!specificSize){
						let size=selfParam.concernedObject.size;
						if(size.w && selfParam.size.w)	selfParam.imageScaleW=size.w/selfParam.size.w;
						if(size.h && selfParam.size.h)	selfParam.imageScaleH=size.h/selfParam.size.h;
					}else{
						if(specificSize.w && img.width)		self.imageScaleW=specificSize.w/img.width;
						if(specificSize.h && img.height)	self.imageScaleH=specificSize.h/img.height;

					}
					
				}
				
				
				selfParam.loadedImages.push(img);
				

//				// DBG
//				console.log("---------------------------");

				
				if(numberOfImages<=selfParam.loadedImages.length && (!selfParam.animation || !selfParam.animation.isReady) ){
					
					//const zooms=selfParam.gameConfig.zooms;
					selfParam.animation=getSpriteMonoThreaded(selfParam.loadedImages,
							selfParam.refreshMillis,
							null,
							selfParam.imageScaleW,
							selfParam.imageScaleH,
							"horizontal",
							(selfParam.offsets?nonull(selfParam.offsets.x,0):0),(selfParam.offsets?nonull(selfParam.offsets.y,0):0),true,false,
							nonull(selfParam.center, selfParam.concernedObject.position.center),
							zooms
					).setDurationTimeFactorHolder(window.eranaScreen);
					
					selfParam.animation.isReady=true;

					
//					// DBG
//					console.log("END OF LOADING selfParam:",selfParam);
					
				}

//				// DBG
//				console.log("selfParam:",selfParam);
//				console.log("selfParam.loadedImages:",selfParam.loadedImages);
//				console.log("img.src:",img.src);
//				console.log("selfParam.loadedImages.length:",selfParam.loadedImages.length);
//				console.log("numberOfImages:",numberOfImages);
//				console.log("===========================");
				
				
			};
		
		
		});
		
	}
	
	
	
	
	
	/*public*/getActionType(){
		return this.actionType;
	}
	
	
	
	canDraw(camera){
		
		// TODO : Activate later :
		// We skip the calculus and stick to the old visiblity result if position hasn't changed :
//		if(typeof(this.oldVisibility)!=="undefined"
//				&& !this.getRefPosition().hasChanged() && !camera.position.hasChanged())
//			return this.concernedObject.oldVisibility;
		
		if(!this.concernedObject.getIsVisible() || !this.animation || !this.animation.isReady || this.animation.width===0 || this.animation.height===0)	return false;
		
		let gameConfig=this.gameConfig;
		
		// CAUTION ! Coordinates for DRAWING are not the same than for CALCULUS !
		let inZone=isInZoomedVisibilityZone(gameConfig,camera,this.getOffsettedRefPosition(),this.concernedObject.size);
		if(!inZone) {
			this.oldVisibility=false;
			return false;
		}
		
		this.oldVisibility=true;
		return true;
		
	}
	
	// TODO : FIXME : DUPLICATED CODE ! (see in GameImage class)
	/*private*/getRefPosition(){
		return this.concernedObject.position;
	}
	
	// TODO : FIXME : DUPLICATED CODE ! (see in GameImage class)
	/*private*/getOffsettedRefPosition(){
		const parentOfConcernedObject=this.concernedObject.parent;
		// OLD :
		if(!parentOfConcernedObject || !parentOfConcernedObject.getPosition || parentOfConcernedObject.getPosition().getAngle2D()==null){
			return this.getRefPosition().getParentPositionOffsetted();
		}
		return this.getRefPosition().getGlued2D();
	}

	
	start(){
		this.started=true;
	}
	
	stop(){
		this.started=false;
	}


	// TODO : FIXME : DUPLICATED CODE ! (SEE GameImage CLASS) :
	
	
	
	/*private*/getRealImageCoords(camera, currentFrameImage, ignoreParentsPositionsStack=false){
		
		// CAUTION ! Coordinates for DRAWING are not the same than for CALCULUS !
		
		// TODO : FIXME : DUPLICATED CODE
		let pos;
		if(ignoreParentsPositionsStack){
			pos=this.getRefPosition().getOffsetted();
		}else{
			pos=this.getOffsettedRefPosition();
		}
		
		
		const drawableUnzoomedCoordinates=getDrawableZoomedIfNecessaryCoordinates2D(this.gameConfig, pos, camera);
		const x=drawableUnzoomedCoordinates.x;
		const y=drawableUnzoomedCoordinates.y;
		const a=drawableUnzoomedCoordinates.a;
		
		const zooms=this.gameConfig.zooms;
		
		const refImageSize={w:currentFrameImage.width,h:currentFrameImage.height};
		return {
			zooms:zooms,
			x:x,y:y, a:a,
			w:refImageSize.w,
			h:refImageSize.h,
			center:this.getRefPosition().center,
			scaleW:this.scaleW,
			scaleH:this.scaleH
		};
		
	}


	// -----------------------------------------------------

	draw(ctx,camera,lightsManager,opacity=1, ignoreRaysCasting=false){

		
		if(!this.started)	return;

		let gameConfig=this.gameConfig;

		if(!this.canDraw(camera))	return;

			// CAUTION ! Coordinates for DRAWING are not the same than for CALCULUS !
			const drawableUnzoomedCoordinates=getDrawableZoomedIfNecessaryCoordinates2D(gameConfig, this.getOffsettedRefPosition(), camera);
			
//			if(this.colorLayer){
//				// TODO : ...
//			}



			if(contains(gameConfig.projection,"2D")){
								
				
				const zooms=gameConfig.zooms;
				
				// CAUTION ! Coordinates for DRAWING are not the same than for CALCULUS !
				const x=drawableUnzoomedCoordinates.x;
				const y=drawableUnzoomedCoordinates.y;
				const angle=drawableUnzoomedCoordinates.a;
				
				
				// TODO : FIXME : ASPECT RATIO SEEMS WRONG !
				this.animation.drawOnEachStepZoomedIfNeeded(ctx, x, y, angle, opacity);
				
				
				if(this.darkImage && lightsManager){
					const realImageCoords=this.getRealImageCoords(camera, this.darkImage);
					
					// TODO : FIXME : Lights won't affect this object, since it has no base image to draw !
					// TODO : FIXME : Being able to process lights even if we have no base image !
	
	
	//				// TODO : FIXME : DISPAY LIGHTS ON DARK IMAGE !
	//				// Lighting effects :
	//				if(lightsManager)	lightsManager.drawLightsZoomedIfNeeded(ctx, camera, realImageCoords, 1, this.darkImage, null, currentFrameImage, {w:this.darkImage.width,h:this.darkImage.height});
				
					// Lighting effects :
					lightsManager.drawLightsZoomedIfNeeded(ctx,camera,realImageCoords, 1, this.darkImage, null, null, {w:this.darkImage.width,h:this.darkImage.height}, ignoreRaysCasting);
					
				}
				
				
		}

		
		
	}
	
	
	
}
