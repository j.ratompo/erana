

class GameThumb{

	constructor() {
		
		this.isVisible=true;
		
		this.sizeFactor=1;
		this.scaleFactor=1;
		
		this.position=new Position();
		
	}
	
	// INIT
	init(gameConfig){
		this.gameConfig=gameConfig;
		
		
	}
	
	

	/*private*/initThumbIfNeeded(){

		let thumbSrc=this.thumbSrc;
		if(!thumbSrc)	return;
		
		if(!this.img){
			

			
			let parent=this.parent;
			
//		// !!! This is the reason why we do it that way, because in the method initFromParent,
//		// the parent position has not been initialized yet !!!
////		let parentPosition=parent.position;
//		let parentSize=parent.size;
			
			let self=this;
			loadImageInUniverse(this.gameConfig,thumbSrc,parent,null,function(image){
			
//			let w;
//			let h;
//			if(parentSize){
//				w=parentSize.w;
//				h=parentSize.h;
//			}else{
//				w=image.width;
//				h=image.height;
//			}
//			let x=Math.floor(parentPosition.x*self.scaleFactor*(image.width/w))+(resolution.w-self.right);
//			let y=parentPosition.y*self.scaleFactor+(resolution.h-self.bottom);
//			self.position=new Position(self,x,y,0,{x:"center",y:"center"});
				
				self.size={w:image.width*self.sizeFactor,h:image.height*self.sizeFactor};
				self.img=image;
				self.doOnClick=function(){	self.doOnClickThumb(self);	};
				self.isClickable=()=>{	return true;	};
				self.isFixed=true;
				
				
				window.eranaScreen.sceneManager.view.controlsManager.registerClickable(self);
				
				
			});
			
			
		}
		
	}
	
	
	// METHODS
	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	
	
	/*public*/getPosition(){
		return this.position;
	}
	
	/*private*/getCalculatedPosition(refObjectOverridingPosition=null){
		
		let self=this;
		

		let parent=self.parent;
		let positionReferenceObject;
		if(parent.getOverridingPositionReferenceObject){
			positionReferenceObject=parent.getOverridingPositionReferenceObject();
		}else{
			positionReferenceObject=parent;
		}
		// !!! This is the reason why we do it that way, because in the method initFromParent,
		// the parent position has not been initialized yet !!!
		let positionReferenceObjectPosition=positionReferenceObject.position;
		
		
		let parentSize=parent.size;
		
		let image=self.img;

		let w;
		let h;
		if(parentSize){
			w=parentSize.w;
			h=parentSize.h;
		}else{
			w=image.width;
			h=image.height;
		}
		
//	let x=Math.floor(positionReferenceObjectPosition.x*self.scaleFactor*(image.width/w))+(resolution.w-self.right);
//	let y=(resolution.h-self.bottom);

		let x=Math.floor(positionReferenceObjectPosition.x*self.scaleFactor*(image.width/w));
		let y=Math.floor(positionReferenceObjectPosition.y*self.scaleFactor*(image.height/h));
		

		
		if(this.offsetsUI){
			x+=nonull(this.offsetsUI.x,0);
			y+=nonull(this.offsetsUI.y,0);
		}
		
		
		if(refObjectOverridingPosition){
			let overriddenPosition=refObjectOverridingPosition.overrideThumbPosition(this,{x:x,y:y});
			if(overriddenPosition){
				x=nonull(overriddenPosition.x,0);
				y=nonull(overriddenPosition.y,0);
			}
		}
		
		
		return {x:x,y:y};
	}
	
	/*private*/doOnClickThumb(selfParam){
		
		// We center the view on this element :

		let camera=window.eranaScreen.controlsManager.view.getCamera();
		let deltaX=selfParam.parent.position.getParentPositionOffsetted().x - camera.position.getParentPositionOffsetted().x;
		let view=window.eranaScreen.controlsManager.view;
		view.setCameraPositionToDeltas({dx:deltaX},true);

	}
	
	
	
	// We draw its thumbnail :
	/*public*/drawDynamicThumbnailUI(ctx,camera,gaugeParam,refObjectOverridingPosition=null,isActuallyDrawn=true){
		
		if(!this.getIsVisible() || !this.thumbSrc)	return null;

		this.initThumbIfNeeded();
		
		let image=this.img;
		if(!image)	return null;

		let w=this.size.w;
		let	h=this.size.h;

		this.position.setLocation(this.getCalculatedPosition(refObjectOverridingPosition));
		
		let pos=this.getPosition();
		let x=pos.x;
		let y=pos.y;

		// TODO : FIXME : ADD EVERYWHERE
		const zooms=(this.unaffectedByZooms?{zx:1,zy:1}:this.gameConfig.zooms);
		return drawImageAndCenterWithZooms(ctx,image,x,y,zooms,null,1,1,w,h,(!isActuallyDrawn?0:1));
	}
	
	
	
}
