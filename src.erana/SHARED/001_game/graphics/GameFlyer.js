class GameFlyer{

	constructor() {
		
		this.isVisible=true;
		
		this.position=new Position(this);
		
		
	}
	
	// INIT
	init(gameConfig){
		
		this.gameConfig=gameConfig;
		
		
		if(this.imageConfig){
			if(contains(this.gameConfig.projection,"2D")){
			
				// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG ! 
				if(containsOneOf(this.gameConfig.scroll,["horizontal","bidimensional"])) {
					
					this.position.center=this.imageConfig._2D.center;
					if(!this.imageConfig._2D.animated){
						this.img=new GameImage(this);
						this.getGameImage().init(gameConfig);
					}else{
						this.anim=new GameAnimation(this);
						this.getGameAnimation().init(gameConfig).loadSprite2DAndSet();
					}
					
				}else{
					// TODO ...
				}
			}
		}
		
		
		if(this.showStrategy && !empty(this.showStrategy.fading)){
			this.fading=this.showStrategy.fading;
		}else{
			this.fading="instantly"; // default value
		}
		
		
		
	}
	
	// METHODS
	
	getGameImage(){
		return this.img;
	}	
	getGameAnimation(){
		return this.anim;
	}
	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	
	
	
	
	

	/*private*/updateSizeToShowZoneOrImage(){
		
		let img=nonull(this.getGameImage(), this.getGameAnimation());
		if(this.imageConfig && this.imageConfig._2D && this.imageConfig._2D.size){
			this.size=this.imageConfig._2D.size;
		}else if(img && img.isReady()){
			this.size=img.getImageSize();
		}

		if(this.showStrategy && this.showStrategy.triggerZone && this.showStrategy.triggerZone._2D){
			this.triggerSize={w:this.showStrategy.triggerZone._2D.w, h:this.showStrategy.triggerZone._2D.h};
		}else{
			this.triggerSize=this.size;
		}
		
	}
	
	
	
	/*private*/getOpacity(camera){
		
		let opacity=1;
		if(this.fading==="proportionalToDistance"){
			
			let persist=(this.showStrategy && this.showStrategy.persist);

			if(!this.hasBeenShowed || !persist){
			
				let offsettedPos=this.position.getParentPositionOffsetted();
				let cameraOffsettedPos=camera.position.getParentPositionOffsetted();
	
				if(  (this.imageConfig && this.imageConfig._2D) 
					|| (this.showStrategy && this.showStrategy.triggerZone && this.showStrategy.triggerZone._2D) ){
				
					let size=this.triggerSize;
					
					let opacityX=null;
					if(size.w){
						let deltaX=offsettedPos.x-cameraOffsettedPos.x;
						if(0<deltaX || !persist){
							let distanceToCameraX=Math.abs(deltaX);
							if(distanceToCameraX){
								let marginWidth=size.w/2;
								if(distanceToCameraX<=marginWidth){
									let ratio=distanceToCameraX/marginWidth;
									opacityX=1-Math.min(1,Math.max(0,ratio));
								}else{
									opacityX=0;
								}
							}
						}
					}
	
					let opacityY=null;
					if(size.h){
						let deltaY=offsettedPos.y-cameraOffsettedPos.y;
						if(0<deltaY || !persist){
							let distanceToCameraY=Math.abs(deltaY);
							if(distanceToCameraY){
								let marginHeight=size.h/2;
								if(distanceToCameraY<=marginHeight){
									let ratio=distanceToCameraY/marginHeight;
									opacityY=1-Math.min(1,Math.max(0,ratio));
								}else{
									opacityY=0;
								}
							}
						}
					}
		
					if(opacityX!=null && opacityY!=null){
						opacity=Math.min(opacityX, opacityY);
					}else{
						opacity=nonull(nonull(opacityX, opacityY),1);
					}
					
				}
			
			}

		}

		return opacity;
	} 
	
	

	/*private*/isInTriggerZone(camera){
	
		if(!this.triggerSize)	return false;

		
		let offsettedPos=this.position.getParentPositionOffsetted();
		
		let zoneOffsetted={x:offsettedPos.x, y:offsettedPos.y, w:this.triggerSize.w, h:this.triggerSize.h};
		
//		// DBG
//		if(offsettedPos.x===0)		lognow("Sticker isInTriggerZone:");

		let result=isInZone(camera.position.getParentPositionOffsetted(), zoneOffsetted , null, this.position.center);
		
//		// DBG
//		if(result)	lognow("Sticker isInTriggerZone == true:",this);
		
		return result;
	
	}

	// Works for those who don't have images too :
	/*private*/calculateDynamicVisibilityForGameFlyer(camera){
		
		if(this.showStrategy && (!this.getIsVisible() || !this.showStrategy.persist)){
			var projection = this.gameConfig.projection;
			if(contains(projection, "2D")) {
				
				if(this.isInTriggerZone(camera)) {
					this.isVisible=true;
					return true;
				} else if(!this.showStrategy.persist){
					this.isVisible=false;
					return false;
				}
				
			}
		}
		
		return true;
	}
	
	
	
	
	// --------------------------------------------------
	
	draw(ctx,camera,lightsManager){
		
		
		// Special case :
		if(!this.size && !this.triggerSize){
			this.updateSizeToShowZoneOrImage();
			return false;
		}
		
		
		let calculatedDynamicVisibility=this.calculateDynamicVisibilityForGameFlyer(camera);
		if(!calculatedDynamicVisibility)	return false;
		
		
		
		// Case GameFlyer with only text lines, and no image, we must be able to display them :
		if(!this.getGameImage() && !this.getGameAnimation())	return true;
		
		
		let opacity=this.getOpacity(camera);
		
		let canDraw=false;
		if(this.getGameImage()){
			canDraw=this.getGameImage().draw(ctx,camera,lightsManager,opacity);
		}else if(this.getGameAnimation()){
			canDraw=this.getGameAnimation().draw(ctx,camera,lightsManager,opacity);
		}
		
		
		if(!this.hasBeenShowed && (!this.oldCalculatedCanDraw && canDraw) && opacity==1)
			this.hasBeenShowed=true; // If we detect a "showing event" :
		this.oldCalculatedCanDraw=canDraw;
		
	
		return canDraw;
		
	}
	
	
}
