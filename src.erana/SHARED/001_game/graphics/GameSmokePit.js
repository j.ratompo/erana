
// DBG
const IS_DEBUG_SMOKE_PIT=false;

class GameSmokePit{

	// Two-states : started, stopped.
	
	constructor() {

		this.isVisible=true;
		
		this.started=false;
		

		// Configurable attributes :
		// (DEFAULT VALUES) :
		this.appearRateMillis=null;
		this.defaultOpacity=1;
		this.puffStartSize=20;
		this.puffEndSize=200;
		this.puffExpandSpeed=20;
		this.maxPuffsNumber=30;
		

		this.position=new Position(this);

		this.imgs={};
		this.puffs=[];
		
		
		
	}
	
	// INIT
	
	init(gameConfig){
		this.gameConfig=gameConfig;
		
	}
	
	
	initWithParent(startImmediately=false){
		
		
	// UNUSED : ADAPTATIVITY :
//	this.maxPuffsNumber=Math.ceil(this.maxPuffsNumber/this.gameConfig.minDurationTimeFactor);


		if(contains(this.gameConfig.projection,"2D")){
		

			this.position.setParentPosition(this.parent.position).center=this.imageConfig._2D.center;
			
			if(startImmediately)	this.start();
			

			if(empty(this.imageConfig._2D.sourcesPaths)){
				// TRACE
				console.log("WARN : Images array is empty, could not choose one for puff.");
			}else{


				for(let i=0;i<this.maxPuffsNumber;i++){
					let puff=this.getNewPuff();
					if(puff)	this.puffs.push(puff);
				}
				
				
			}
				
				
		}
		
	}
	

	
	// METHODS
	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	
	
	/*private*/getNewPuff(existingPuff=null){
	
		if(!this.parent)	return null;
	
		let self=this;

		let puff=nonull(existingPuff,{});
		
		if(contains(this.gameConfig.projection,"2D")){
		
			// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
			if(containsOneOf(this.gameConfig.scroll,["horizontal","bidimensional"])) {
				
				puff.maxSize=this.puffEndSize;// TODO : It could be interesting to add a randomness here...
				puff.getIsVisible=function(){	return self.getIsVisible();/*inherited from parent*/ };
				puff.init=function(){
					puff.opacity=self.defaultOpacity;// NO : CANNOT : TODO : It could be interesting to add a randomness here...
					puff.size={w:self.puffStartSize, h:self.puffStartSize};// TODO : It could be interesting to add a randomness here...
					puff.expandSpeed=self.puffExpandSpeed;// TODO : It could be interesting to add a randomness here...
					
					puff.position=new Position(self).setLocation(self.position.getParentPositionOffsetted());
					
					puff.hasBeenFired=false;
					
					self.initRandomPuffImage(puff);
				};
				puff.init();
			}else{
				// TODO ...
			}
		}
		
		return puff;
	}
	
	
	/*private*/initRandomPuffImage(puff){
		
		let sourcesPaths=this.imageConfig._2D.sourcesPaths;
		let chosenSrc=(sourcesPaths.length===1?sourcesPaths[0] : Math.getRandomInArray(sourcesPaths));
		let img=this.imgs[chosenSrc];
		if(!img){
			img=new GameImage(puff);
			let imgConfig={
					_2D : {
						center:puff.position.center,
						sourcePath:chosenSrc,
					}
			};
			img.init(this.gameConfig,imgConfig,"GameSmokePit/"+this.prototypeName+"/");
			this.imgs[chosenSrc]=img;
		}
		puff.img=img;
		
	}
	
	
	
	/*private*/fireNewPuff(){
		let unfiredPuffs=this.getUnfiredPuffs();
		if(empty(unfiredPuffs))	return null;
		let puff=unfiredPuffs[0];
		puff.init();
		puff.hasBeenFired=true;
		return puff;
	}
	
	
	/*private*/getUnfiredPuffs(){
		let results=[];

// UNUSED : ADAPTATIVITY : //		let durationTimeFactor=window.eranaScreen.getDurationTimeFactor();
//		let minDurationTimeFactor=this.gameConfig.minDurationTimeFactor;
//		let maxPuffsNumberLocal=Math.ceil((this.maxPuffsNumber*minDurationTimeFactor)/timeFactor);
		
		foreach(this.puffs,(p,index)=>{
// UNUSED : ADAPTATIVITY :
//			if(maxPuffsNumberLocal<=index)
//				return "break";
			results.push(p);
		},(p)=>{	return !p.hasBeenFired;	});
		
		return results;
	}
	
	
	/*private*/growPuff(puff){
		
		if(!this.started)			return false;
		
		
		// If the puff has reached its max size :
		if(puff.maxSize<=puff.size.w || puff.opacity<=0){
			puff.init();
			return false;
		}

		if(!puff.hasBeenFired)	return false;
			
		let durationTimeFactor=window.eranaScreen.getDurationTimeFactor();
		let timeFactor=(1/durationTimeFactor);

		let factor=puff.size.w/puff.maxSize;
		puff.opacity=(1-factor)*this.defaultOpacity;
		puff.size.w+=puff.expandSpeed*timeFactor;
		puff.size.h+=puff.expandSpeed*timeFactor;
		
		return true;
	}
	
	
	
	
	start(){
		this.started=true;
	}
	
	stop(){
		this.started=false;
	}
	
	
	
	// -----------------------------------------------------------

	/*public*/updateStateAndDraw(ctx,camera){

		let parent=this.parent
		if(!parent || !parent.canDrawSmokePits)	return;
		
		let smokePit=this;
		if(!parent.canDrawSmokePits()) {
			if(smokePit.started)		smokePit.stop();
		}else{
			if(!smokePit.started)		smokePit.start();
			smokePit.draw(ctx,camera);
		}
		
	}

	
	draw(ctx, camera, lightsManager){

		if(!this.getIsVisible())	return false;

		
		if(this.appearRateMillis){
			
			// Fire a new puff every millis :
			let durationTimeFactor=window.eranaScreen.getDurationTimeFactor();
			let appearDelayHasPassed=(!this.lastTimeFiredSinglePuff || hasDelayPassed(this.lastTimeFiredSinglePuff, 
								this.appearRateMillis*durationTimeFactor
					));
			if(appearDelayHasPassed){
				this.lastTimeFiredSinglePuff=getNow();
				let puff=this.fireNewPuff();
			}
		}else{
		
			// Fire a new puff every time we can :
			this.fireNewPuff();
		}
		
		
		let self=this;
		
		const gameConfig=self.gameConfig;
		const zooms=gameConfig.zooms;
		
		foreach(self.puffs,(puff,index)=>{

			let mustDraw=self.growPuff(puff);
			if(!mustDraw)	return "continue";
			
			let img=puff.img;

			// Smoke puffs position is a bit special : their position is exactly like if they had no parent, but they actually be in several layers of children:
			// So we have to draw them in an absolute position, regardless of their parents tree :

			if(!img.canDraw(camera,true))	return "continue";

			img.drawAbsolute(ctx,camera,lightsManager,puff.opacity,true);


			if(IS_DEBUG_SMOKE_PIT) {
				// DBG
				const drawableUnzoomedCoordinates=getDrawableZoomedIfNecessaryCoordinates2D(self.gameConfig, puff.position.getParentPositionOffsetted(), camera);
				const x=drawableUnzoomedCoordinates.x;
				const y=drawableUnzoomedCoordinates.y;
				drawSolidText(ctx,x,y,index,"#FFFFFF",20,null,null,zooms);
			}
			
		});
		
		
		if(IS_DEBUG_SMOKE_PIT) {
			// DBG
			let dCoords=getDrawableZoomedIfNecessaryCoordinates2D(self.gameConfig, this.position.getParentPositionOffsetted(), camera);
			let x=dCoords.x;
			let y=dCoords.y;
			drawSolidText(ctx,x,y,"X","#FF8888",20,null,null,zooms);
		}		
		
	}
	
	
}
