
class GameToolbar3D{

	constructor(commandedConcernedObject){
		
		
		if(!commandedConcernedObject.isRooted || !commandedConcernedObject.isRooted()){
			throw new Error("ERROR : Adding a toolbar to unrooted objects is not supported yet.");
		}
		
		
		this.commandedConcernedObject=commandedConcernedObject;
		
		this.toolbarLayout="top";
		
		this.buttons=[];
		
	}
	
	// INIT
	init(gameConfig, scene3D, parentObject3D=null){
		
		this.gameConfig=gameConfig;
		this.scene3D=scene3D;
		this.parentObject3D=parentObject3D;
		
		this.group3D=new THREE.Group();
		
		
		if(this.parentObject3D){
			this.scene3D.add(this.group3D);
		}else{
			this.parentObject3D.add(this.group3D);
		}
		
		
		
		
		// TODO : ALLOW WITH ERANA PROTOTYPING !
		
		const lateralMoveButton=new LateralMoveButton3D(this).init(this.gameConfig, this.group3D);
		this.buttons.push(lateralMoveButton);
		this.setupButtonPosition(lateralMoveButton);
		
		const depthMoveButton=new DepthMoveButton3D(this).init(this.gameConfig, this.group3D);
		this.buttons.push(depthMoveButton);
		this.setupButtonPosition(depthMoveButton);

		// DBG
		const verticalFOVChangeButton=new FOVChangeButton3D(this,"vertical").init(this.gameConfig, this.group3D);
		this.buttons.push(verticalFOVChangeButton);
		this.setupButtonPosition(verticalFOVChangeButton);

		const horizontalFOVChangeButton=new FOVChangeButton3D(this,"horizontal").init(this.gameConfig, this.group3D);
		this.buttons.push(horizontalFOVChangeButton);
		this.setupButtonPosition(horizontalFOVChangeButton);
		
		
		
		
		

		this.commandedConcernedObject.getRoot3D().add(this.group3D);
		
		this.initDefaultLocation();
		
	}
	
	/*private*/initDefaultLocation(){
		
			let xOffset=0;
			let yOffset=0;
			if(this.toolbarLayout=="top"){
				const commandeConcernedObjectGeometry=this.commandedConcernedObject.getObject3D().geometry;
				commandeConcernedObjectGeometry.computeBoundingBox();
				const commandeConcernedObjectBox=commandeConcernedObjectGeometry.boundingBox;
				const commandedConcernedObjectHeight=(commandeConcernedObjectBox.max.y-commandeConcernedObjectBox.min.y);
				
				let height=0;
				if(!empty(this.group3D.children)){
					const geometry=this.group3D.children[0].geometry;
					geometry.computeBoundingBox();
					const box=geometry.boundingBox;
					height=(box.max.y-box.min.y);
				}

				yOffset=commandedConcernedObjectHeight*.5+height*.5;
			}
		
			this.group3D.position.set(0,yOffset,0);
			this.group3D.rotation.set(0,0,0);
	}
	
	
	/*private*/setupButtonPosition(button){
		
		
		const buttonOffsets=this.getButtonPositionOffsetsAndCommandedObjectSize(button);
		
		const xOffset=buttonOffsets.x;
		const yOffset=buttonOffsets.y;
		
		button.getObject3D().position.set(xOffset,yOffset,0);
		button.getObject3D().rotation.set(0,0,0);
		
	}
	
	
	
	/*public*/getButtonPositionOffsetsAndCommandedObjectSize(button){

		const MARGIN=2;
		let buttonsWidthsSum=0;
		let xOffset=0;
		let yOffset=0;

		const commandeConcernedObjectGeometry=this.commandedConcernedObject.getObject3D().geometry;
		commandeConcernedObjectGeometry.computeBoundingBox();
		const commandeConcernedObjectBox=commandeConcernedObjectGeometry.boundingBox;
		const commandeConcernedObjectWidth=(commandeConcernedObjectBox.max.x-commandeConcernedObjectBox.min.x);
		const commandeConcernedObjectHeight=(commandeConcernedObjectBox.max.y-commandeConcernedObjectBox.min.y);

		if(this.toolbarLayout=="top"){
			
			let isFirstButton=true;
			foreach(this.buttons,(b)=>{
				const geometry=button.getObject3D().geometry;
				geometry.computeBoundingBox();
				const box=geometry.boundingBox;
				const width=(box.max.x-box.min.x);
				buttonsWidthsSum+=width+(/*we only want margin for the following buttons:*/isFirstButton?-width*.5:MARGIN);
				if(isFirstButton)	isFirstButton=false;
			});
			
			xOffset=commandeConcernedObjectWidth*.5-buttonsWidthsSum;

		}
		
		return {x:xOffset,y:yOffset,w:commandeConcernedObjectWidth,h:commandeConcernedObjectHeight};
	}
	
}

