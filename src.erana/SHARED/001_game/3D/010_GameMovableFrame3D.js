


class GameMovableFrame3D extends GameObject3D{

	constructor(){
		super();
		this.anchorId=null;
		this.ctx=null;
		
		this.onFinishedDrawingListeners=[];
		
		this.gameToolbar3D=null;
		this.scene3D=null;
		
		this.opacity=1;
	}


	init(gameConfig, overriddenBasePath=null){
		super.init(gameConfig, overriddenBasePath);
		
		try{
			this.gameToolbar3D=new GameToolbar3D(this);
		}catch(e){
			lognow(e);
			this.gameToolbar3D=null;
		}
		

	}
	

	/*OVERRIDES*/
	initAfterLevelLoading(){
//		window.eranaScreen.loadObjectInScene(this, this.overriddenBasePath);
		super.initAfterLevelLoading();

		// At this point, this.object3D is still null !
		// It will need to be loaded in order to start inputing/outputing with fusroda server !
	
	}
	

	/*public*/addOnFinishedDrawingListener(doOnFinishedDrawing){
		this.onFinishedDrawingListeners.push(doOnFinishedDrawing);
	}
	

	// TODO : FIXME : DUPLICATED CODE :
	// DOES use loadModel function set by default :
	/*OVERRIDES*/ 
	/*public*/loadModel(controller,models3DLoader/*is always null*/,sceneWEBGL,overriddenBasePath){

		const self=this;

		return new Promise((resolve)=>{
		
				self.scene3D=sceneWEBGL;
				
				// create the plane mesh
				const material=self.setupMaterial();
				
				material.opacity=this.opacity;
				
				material.blending=THREE.NoBlending;
		
				// DBG
				material.side=THREE.DoubleSide;
		
				let w=1;
				let h=1;
				if(self.size){
					w=nonull(self.size.w,w);
					h=nonull(self.size.h,h);
				}
				
				const geometry=new THREE.PlaneGeometry(w,h);
				self.object3D=new THREE.Mesh(geometry, material);
				
				
				self.object3D.material.color.setHex(self.colorHex);
				if(!self.isRooted()){
					// add it to the WebGL scene
					self.scene3D.add(self.object3D);
	
					if(self.gameToolbar3D){
						// TRACE
						lognow("WARN : CAUTION : Toolbar is not supported for unrooted objects!");
					}

					/*super*/self.initLocation(self.object3D);
				}else{
					
					self.root3D=new THREE.Group();
					self.getRoot3D().add(self.object3D);
					
					if(self.gameToolbar3D){
						self.gameToolbar3D.init(self.gameConfig, self.scene3D, self.getRoot3D());
					}
					
					// add it to the WebGL scene
					self.scene3D.add(self.getRoot3D());

	
					/*super*/self.initLocation(self.getRoot3D());
					/*super*/self.initZeroLocation(self.object3D);
				}
				
			
				self.linkToAnchor();

				
				// back-link to self object : 
				self.object3D.interactableEntity=self;
				
				
				self.doOnLoaded(controller);

				
				resolve({object3D:self.object3D});
		});	
	}
	
	// UNUSED :	
//	/*public*/refresh(ctx){
//		
//		
//	}
	
	/*protected*/doOnLoaded(controller){

		this.drawBlankTexture();
	
		window.eranaScreen.addClickableObject3D(this.getObject3D());
	}
	
	
	/*protected*/getCanvasSize(){

		const DEFAULT_CANVAS_WIDTH=100;
		const DEFAULT_CANVAS_HEIGHT=100;

		const result={};
				
		if(this.canvasSize){
			result.width=nonull(this.canvasSize.w,DEFAULT_CANVAS_WIDTH);
			result.height=nonull(this.canvasSize.h,DEFAULT_CANVAS_HEIGHT);
		}else{
			result.width=DEFAULT_CANVAS_WIDTH;
			result.height=DEFAULT_CANVAS_HEIGHT;
		}
		
		return result;
	}
	
	/*private*/setupMaterial(){
		
		// https://threejs.org/manual/?q=texture#en/canvas-textures
			
			

		const localCanvas=document.createElement("canvas");
		this.ctx=localCanvas.getContext("2d");
		
		
		const canvasSize=this.getCanvasSize();
		localCanvas.width=canvasSize.width;
		localCanvas.height=canvasSize.height;
		
		
		this.ctx.fillStyle="#888888"; // (unconnected color)
		this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
		
		this.texture=new THREE.CanvasTexture(this.ctx.canvas);
		
		
		const material=new THREE.MeshBasicMaterial({
		  map: this.texture,
//	  wireframe: true, wireframeLinewidth: 1, side: THREE.DoubleSide
		});
			
			
			// DEBUG ONLY :
	//	const material=new THREE.MeshBasicMaterial({ color: 0x000000, wireframe: true, wireframeLinewidth: 1, side: THREE.DoubleSide });
	//	const material=new THREE.MeshBasicMaterial();
	//	material.color.set("black");
			
		return material;
	}
	
	
	// TODO : FIXME : DUPLICATED CODE :
	/*public*/setLocation(locationParam){
		
		let location;
		if(this.location){
			this.location.setLocation(locationParam);
			location=this.location;
		}else{
			location=locationParam;
		}
	
		if(this.getPositionnedObject3D()){
			this.getPositionnedObject3D().location.set(nonull(location.x,0), nonull(location.y,0), nonull(location.z,0));
		}

		
	}

	/*private*/drawBlankTexture(){
		
		this.ctx.save();
		const localCanvas=this.ctx.canvas;
		this.ctx.fillStyle="#FFFFFF"
		this.ctx.rect(0,0,localCanvas.width,localCanvas.height);
		this.ctx.fill();
		this.ctx.restore();
		
		this.updateTexture();
		
	}


	/*private*/updateTexture(){
		
		if(!this.texture)	return;
		
		this.texture.needsUpdate=true;
		
		//4)
		this.onFinishedDrawingListeners.forEach((listener)=>{listener();});


	}


	/*public*/getPlanarSize(){
		const localCanvas=this.ctx.canvas;
		const width=localCanvas.width;
		const height=localCanvas.height;
		return {w:width,h:height};
	}

	/*private*/getObject3D(){
		return this.object3D;
	}
	
	



	// TODO : FIXME : DUPLICATED CODE !
	/*private*/linkToAnchor(){
		
		if(!this.anchorId)	return;
		
		const anchor=this.parent.findAnchorById(this.anchorId);
		if(anchor)	anchor.addLinkedObject(this);
		
	}
	
	
	/*public*/doOnAnchorLost(){
		
	}
	
	/*public*/doOnAnchorFound(vertices){
		
	}


}

