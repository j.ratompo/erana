

// CURRENT : TODO : Add authentication.

class GameVNCFrame3D extends GameIOFrame3D{


	constructor(){
		super();
		
	
		this.url=null;
		this.port=null;
		
	}


	/*OVERRIDES*/
	initAfterLevelLoading(){
		super.initAfterLevelLoading();
		
		// NO : Because we have to start the fusroda client first :
//		window.eranaScreen.loadObjectInScene(this, this.overriddenBasePath);


		const controller=window.eranaScreen;
		const key=this.getVNCEndpointKey();
		controller.setVNCEndPoint(key, this);
		
		
		controller.connectToVNCServerOnce(this.url, this.port);
		
	}
	

	// DOESN'T uses loadModel function set by default :
	/*OVERRIDES*/
	/*protected*/doOnLoaded(controller){
		super.doOnLoaded(controller);
		
		const key=this.getVNCEndpointKey();
		controller.setVNCEndPointObject(key, this.object3D);
		// OLD :	controller.connectToVNCServerOnce(this.url,this.port);
	}
	
	/*private*/getVNCEndpointKey(){
		// Caution : parent only knows url and server port, so for now only one endpoint can correpsond to this !
		return this.url+":"+this.port;
	}


	



}

