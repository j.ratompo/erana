
class GameObject3D{

	constructor(){
	
		this.object3D=null;
		this.colorHex=0x888888;
		
		
		this.rooted=false;
	}

	// INIT

	init(gameConfig, overriddenBasePath=null){
		
		this.gameConfig=gameConfig;
		this.overriddenBasePath=overriddenBasePath;
		
		if(this.modelConfig){
			if(!empty(this.modelConfig.stl)){
				this.modelType="stl";
				this.modelSrc=this.modelConfig.stl;
			}
			if(this.modelConfig.color)	this.colorHex=parseInt("0x"+this.modelConfig.color.replace("#",""),16);
		}
		
		
		
	}
	
	
	initAfterLevelLoading(){
		
		window.eranaScreen.loadObjectInScene(this, this.overriddenBasePath);
		
		

	}
	
	// Uses loadModel function by default :
///*public*/loadModel(controller,models3DLoader,sceneWEBGL,sceneWEBGLAnchored,overriddenBasePath=null){
	/*public*/loadModel(controller,models3DLoader,sceneWEBGL,overriddenBasePath=null){
		
		
		const self=this;

		return new Promise((resolve)=>{
	
				
				const modelOnlySrc=self.getModelSrc();
				if(empty(modelOnlySrc)){
					// TRACE
					lognow("ERROR : No 3D model source path found for object : ",self);
					return;
				}
				
				
				let asyncId=controller.addAsyncStart();
				
				let scene3D=null;
		//		if(self.anchorId)	scene3D=nonull(sceneWEBGLAnchored,sceneWEBGL);
		//		else							
				scene3D=sceneWEBGL;
				
				if(!scene3D){
					// TRACE
					lognow("ERROR : No 3D scene to load model into.");
					return;
				}
				
				
				loadModelInUniverse(models3DLoader, scene3D, self.gameConfig, modelOnlySrc, self, overriddenBasePath, (object3D)=>{
				
				
					self.object3D=object3D;
					self.getObject3D().material.color.setHex(self.colorHex);
					if(!self.isRooted()){
						self.initLocation(self.getObject3D());
					}else{
						self.root3D=new THREE.Group();
						
						self.initLocation(self.getRoot3D());
						self.initZeroLocation(self.getObject3D());
					}
				
		   		controller.addAsyncEnd(asyncId);
	
					resolve({object3D:object3D});
				});
				
				
				
				// TODO : FIXME : DUPLICATED CODE !
				//	if(sceneWEBGLAnchored)
				self.linkToAnchor();
			
				
		});
	}
	

	/*protected*/initZeroLocation(object3D){
			object3D.position.set(0,0,0);
			object3D.rotation.set(0,0,0);
	}
	
	/*protected*/initLocation(object3D){
			if(this.position){
				object3D.position.set(nonull(this.position.x,0), nonull(this.position.y,0), nonull(this.position.z,0));
				object3D.rotation.set(nonull(this.position.g,0), nonull(this.position.a,0), nonull(this.position.b,0));
			}else{
				this.initZeroLocation(object3D);
			}
	}
	
	// TODO : FIXME : DUPLICATED CODE !
	/*private*/linkToAnchor(){
		
		if(!this.anchorId)	return;
		const anchor=this.parent.findAnchorById(this.anchorId);
		if(anchor)	anchor.addLinkedObject(this);
		
	}



	/*public*/getModelType(){
		return this.modelType;
	}
	
	/*public*/getModelSrc(){
		return this.modelSrc;
	}

	
	/*public*/getPositionnedObject3D(){
		return (this.isRooted()?this.getRoot3D():this.object3D);
	}
	
	/*public*/isRooted(){
		return this.rooted;
	}
	
	/*public*/getRoot3D(){
		return this.root3D;
	}

	/*public*/getObject3D(){
		return this.object3D;
	}

}