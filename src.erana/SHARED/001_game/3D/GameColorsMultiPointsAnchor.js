

// TODO : FIXME : TOO SLOW TO BE USABLE !!

// This can anchor any object 3D to a 4 pointsa using combination of colors


const MINIMAL_NUMBER_OF_ACTIVE_POINTS=4;
const REFRESH_CANVAS_ANALYSIS_DELAYS_MILLIS=800;


class GameColorsMultiPointsAnchor{
	
	constructor(){
		
		this.linkedObjects=[];
		
		this.colorPoints={};
		
		
		this.time=null;
		
	}
	
	init(gameConfig){
		this.gameConfig=gameConfig;
		
		
		// 2)
		const self=this;
		window.eranaScreen.mediaHandlerIsReadyListeners.push((/*UNUSED*/arToolkitContext, scene3D, sceneCSS3/*UNUSED*/)=>{
			
			window.eranaScreen.addWebcamUsingObject(self);
			
			foreach(self.linkedObjects,(linkedObject)=>{
				window.eranaScreen.addWebcamUsingObject(linkedObject);
			},(linkedObject)=>linkedObject.isFlat);		
				
		});
			
	
	}
	
	
	/*public*/addLinkedObject(linkedObject){
		// 1)
		this.linkedObjects.push(linkedObject);
	}
	
	
	/*private*/checkIfAllMarkersAreActive(points){
		const allMarkersAreActive=(MINIMAL_NUMBER_OF_ACTIVE_POINTS<=points.length);
		
		if(!allMarkersAreActive){
			this.doOnMarkerLost();
			return false;
		}// else
		{
			this.doOnMarkerFound();
			return true;
		}
	}


	/*private*/doOnMarkerFound(){
		foreach(this.linkedObjects,(linkedObject)=>{
			if(linkedObject.doOnAnchorFound)	linkedObject.doOnAnchorFound(vertices);
		});
	}
	
	/*private*/doOnMarkerLost(){
		// If no point has been recognized :
		foreach(this.linkedObjects,(linkedObject)=>{
			if(linkedObject.doOnAnchorLost)		linkedObject.doOnAnchorLost();
		});
	}

	


	/*protected*/calculateSeenPoints(webcamImageData){
		const points=[];
		
		// TODO : FIXME : TOO SLOW !:
				
		/*
		if(!hasDelayPassed(this.time,REFRESH_CANVAS_ANALYSIS_DELAYS_MILLIS))	return;
		this.time=getNow();
		
		const totalWidth=webcamImageData.width;
		
		const concernedPixels=[];
		for(let i=0;i<webcamImageData.data.length;i+=4){
			const r=webcamImageData.data[i];
			const g=webcamImageData.data[i+1];
			const b=webcamImageData.data[i+2];
			
			
			const concernedPixel={}; 
			const isPixelConcerned=!!foreach(this.colors,(colorCode)=>{
				const colorsArray=htmlColorCodeToDecimalArray(colorCode);
				if(this.colorValueMargin<Math.abs(colorsArray[0]-r) && this.colorValueMargin<Math.abs(colorsArray[1]-g) && this.colorValueMargin<Math.abs(colorsArray[0]-b)){
					concernedPixel.color=colorCode;
					return true;
				}
			});
			if(!isPixelConcerned)	continue;
			
			const coords2D=getCoordinatesForLinearIndex(i, totalWidth, 4);
			concernedPixel.coords2D=coords2D;
			
		}
		
		if(!empty(concernedPixels)){
			// DBG
			lognow(">>>>>>concernedPixels:",concernedPixels);
			
		}
		*/
		
		return points;
	}
	
	
	
	

	// ---------------------------------------------

	canDraw(){
		/*DO NOTHING*/
		return true;
	}


	draw(ctx, webcamImageData){
		
		const points=this.calculateSeenPoints(webcamImageData);
		if(empty(points))	return;

		
		const isActive=self.checkIfAllMarkersAreActive();
		if(!isActive)	return;

		
		ctx.save();
		ctx.strokeStyle="#FFFF00";
		ctx.lineWidth=1;
		ctx.beginPath();
		
		ctx.moveTo(points.x,points.y);
		
		foreach(points,(p,i)=>{
			if(i<=0)	return "continue";
			ctx.lineTo(p.x, py);
		});
		
		ctx.closePath();
		ctx.stroke();
		ctx.restore();
		


	}
	
	
	
}
