


class GameDrawingFrame3D extends GameMovableFrame3D{

	constructor(){
		super();
		
		this.isDrawing=false;
		
		this.oldDrawingPoint=null;
		
	}
	
	

	
	/*public*/doOnHover2D(coords2D){
		
		if(!this.isDrawing)	return;
		
		if(!this.oldDrawingPoint){

			this.ctx.save();
			this.ctx.strokeStyle="#000000";
			this.ctx.lineWidth = nonull(this.thickness,2);
			this.ctx.beginPath();

			this.ctx.moveTo(coords2D.x,coords2D.y);
		}else{
			
			this.ctx.lineTo(coords2D.x,coords2D.y);
			this.ctx.stroke();
			

			this.updateTexture();

		}

		this.oldDrawingPoint=coords2D;
		
//	this.ctx.arc(coords2D.x,coords2D.y,1,0,Math.TAU);

		
	}
	/*public*/doOnClick2D(coords2D){
		
		if(!this.isDrawing){
			this.isDrawing=true;
		}
		
		
	}
	
	/*public*/doOnStopClick2D(coords2D){
		
		if(this.isDrawing){
			this.isDrawing=false;
			
			this.ctx.closePath();
			this.ctx.restore();
			
			this.oldDrawingPoint=null;
			
			this.updateTexture();

		}

		
	}





}

