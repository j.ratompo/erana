
class GameButton3D{

	constructor(parent){
		
		this.parent=parent;
		
		this.color=0x888888;
		
		this.texture=null;
	}
	
	// INIT
	init(gameConfig, group3D){
	
		this.gameConfig=gameConfig;
		
		const w=3;
		const h=3;
		const material=this.setupMaterial();
		const geometry=new THREE.PlaneGeometry(w,h);
		this.object3D=new THREE.Mesh(geometry, material);
	
				
		group3D.add(this.getObject3D());

		
		// back-link to self object : 
		this.getObject3D().interactableEntity=this;
		
		

		window.eranaScreen.addClickableObject3D(this.getObject3D());
		
		

		
		return this;
	}
	

	
	
		
	
	/*private*/setupMaterial(){
		
			// https://threejs.org/manual/?q=texture#en/canvas-textures
					
			
			if(!this.updateLabel){
				if(this.iconSrc){
					const iconPath=getConventionalFilePath(this.gameConfig.basePath,"icons","ui")+"/"+this.iconSrc;
					this.texture=THREE.ImageUtils.loadTexture(iconPath);
		//		ALTERNATIVELY : texture = new THREE.TextureLoader().load(iconPath);
				}
			}else{
				const localCanvas=document.createElement("canvas");
				localCanvas.width=32;
				localCanvas.height=32;
				this.ctx=localCanvas.getContext("2d");
				
				this.ctx.save();
				this.ctx.fillStyle="#FFFFFF"; // (default color)
				this.ctx.rect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
				this.ctx.fill();
				this.ctx.restore();

				this.texture=new THREE.CanvasTexture(this.ctx.canvas);
			}
			
			 
			
			const material=new THREE.MeshBasicMaterial({
				map: this.texture,
				color: (this.texture?null:this.color),
				// DBG
		  	wireframe: false, wireframeLinewidth: 1, side: THREE.DoubleSide
			});
			
			
			// DEBUG ONLY :
	//	const material=new THREE.MeshBasicMaterial({ color: 0x000000, wireframe: true, wireframeLinewidth: 1, side: THREE.DoubleSide });
	//	const material=new THREE.MeshBasicMaterial();
	//	material.color.set("black");
			
			return material;
	}
	
	
	/*protected*/doOnClick(){
		
	}
	
	
	
	
	/*protected*/updateTexture(){
		if(!this.texture)	return;
		this.texture.needsUpdate=true;
	}

	
	
	/*public*/getObject3D(){
		return this.object3D;
	}
	
}

