/* ## Utility gamu methods - 3D view part
 *
 * This set of methods gathers utility game-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils-gamu» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 *  
 */


// ======================== View utility methods ========================

// 3D MODELS MANAGEMENT

/*public static*/function loadModelInUniverse(models3DLoader, sceneWEBGL, gameConfig, modelOnlySrc, concernedObject=null, overriddenBasePath=null, doOnLoad=null){

	if(!models3DLoader){
		// TRACE
		lognow("ERROR : No 3D models loader found. Cannot load model.");
		return;		
	}
	if(!sceneWEBGL){
		// TRACE
		lognow("ERROR : No WEBGL scene found. Cannot load model.");
		return;		
	}

	
	let universePath=null;
	if(overriddenBasePath){
		universePath=overriddenBasePath;
	}
	// (will use introspection to determine the specific base path, if no overriding path is provided :)
	const path=getConventionalFilePath(gameConfig.basePath, nonull(universePath,concernedObject), "universe")+modelOnlySrc;
	models3DLoader.load(path, (geometry)=>{
		const object3D=new THREE.Mesh(geometry);
		
		if(!concernedObject || !concernedObject.isRooted || !concernedObject.isRooted()){
			sceneWEBGL.add(object3D);
		}else{
			sceneWEBGL.add(concernedObject.getRoot3D());
		}
		
		if(doOnLoad)	doOnLoad(object3D);
	});
	
}



/*public static*/function getSingleCanvas(canvasIdSuffix="",parentElement=document.body,side=null,appendAsFirstChild=false){
	
	const canvasId="canvas"+canvasIdSuffix+(!side?"":("_"+side));
	let canvas=document.getElementById(canvasId);
	if(!canvas) {

		canvas=document.createElement("canvas");
		canvas.id=canvasId;

		canvas.style.position="fixed";
		canvas.style["z-index"]="1";
		if(side==="right")	canvas.style.right=0;

		// // DEBUG
		//if(canvasIdSuffix==="WEBGLLayer")
		//	canvas.style.border="solid 3px green";
		//else
		//	canvas.style.border="solid 3px blue";
		
		const canvasWidth=getWindowSize("width")*(!side?1:.5);
		const canvasHeight=getWindowSize("height");
		canvas.width=canvasWidth;
		canvas.height=canvasHeight;

		// OLD :
//		canvas.style.width=(!side?"100%":"50%");
//		canvas.style.height="100%";
		
		// DBG:
		canvas.style.width=canvasWidth+"px";
		canvas.style.height=canvasHeight+"px";
		canvas.style["pointer-events"]="none";
		
		// // DBG
		// lognow("CREATED CANVAS : ",canvas);
		
		// To correct a little bug :
		document.body.style.margin="0";
		
		if(!appendAsFirstChild)	parentElement.appendChild(canvas);
		else 										appendAsFirstChildOf(canvas,parentElement);
	}
	
	return canvas;
}




/*public static*/function getSingleVideo(videosIdSuffix="",parentElement=document.body,side=null,appendAsFirstChild=false){
	
	const videoId="video"+videosIdSuffix+(!side?"":("_"+side));
	let video=document.getElementById(videoId);
	if(!video) {
		const videoWidth=getWindowSize("width")*(!side?1:.5);
		const videoHeight=getWindowSize("height");

		const containerDiv=document.createElement("div");
		containerDiv.style.width=videoWidth+"px";
		containerDiv.style.height=videoHeight+"px";
		containerDiv.style.overflow="hidden";
		containerDiv.style.position="fixed";
		containerDiv.style["pointer-events"]="none";
		

		video=document.createElement("video");
		video.id=videoId;
//	video.autoplay=true; // set in getMediaHandler(...) method with a videoTag config set.

//	video.style.position="fixed";
		video.style.position="absolute";
//	video.style["z-index"]="1";
//	if(side==="right")	video.style.right=0;
		if(side==="right")	containerDiv.style.right=0;

		video.width=videoWidth;
		video.height=videoHeight;

		video.style.width=videoWidth+"px";
		video.style.height=videoHeight+"px";
		video.style["pointer-events"]="none";

		// OLD (BUT KEEP):
//		// DBG
//		video.style.display="none";
		
		
		// To correct a little bug :
		document.body.style.margin="0";
		

		containerDiv.appendChild(video);
		
//	if(!appendAsFirstChild)	parentElement.appendChild(video);
//	else 										appendAsFirstChildOf(video,parentElement);

		if(!appendAsFirstChild)	parentElement.appendChild(containerDiv);
		else 										appendAsFirstChildOf(containerDiv,parentElement);
		
	}
	
	return video;
}



/*static public*/function getPosition2DFromRayVector(intersected){
	let result={x:0,y:0};
	if(!intersected)	return result;
	
	const interactableEntity=intersected.object.interactableEntity;
	if(!interactableEntity)	return result;
	
	const size=interactableEntity.getPlanarSize();
	const width=size.w;
	const height=size.h;
	
	const uv=intersected.uv;
	const x=Math.floor(uv.x*width);
	const y=Math.floor((1-uv.y)*height);//(inverted y-axis)
	
	result={x:x,y:y};
	return result;
}

