

// TODO : FIXME : Use a gameToolbar3D !!

class GameIFrame3D extends GameElement3DCSS{
	
	constructor(){
		super();
		
	}
	
	initAfterLevelLoading(){
		super.initAfterLevelLoading();
		
		//if(!this.isVNC)
			this.browserElement=this.getBrowserElement(()=>{},this.elementHTML);
		//else
		//	this.browserElement=this.getVNCElement(()=>{},this.elementHTML);

	}
	
	/*protected*/createHTMLElement(){
		let elementHTML=super.createHTMLElement();
		
//	this.setupMovingFeature(elementHTML);


		return elementHTML;
	}


//	/*private*/setupMovingFeature(elementHTML){
//		
//			this.moveButton=document.createElement("button");
//			elementHTML.appendChild(this.moveButton);
//			this.moveButton.innerHTML="MOVE";
//			this.moveButton.isMoving=false;
//			this.moveButton.addEventListener("click",(event)=>{
//				let button=event.target;
//				if(button.isMoving)	button.isMoving=false;
//				else								button.isMoving=true;
//			});
//
//
//			// TODO : FIXME :
//			const self=this;
//			document.body.addEventListener("mousemove",(event)=>{
//				if(!self.moveButton.isMoving)	return;
//				let deltaX=0;
//				let deltaY=0;
//				const pointerCoords=getInputCoords(event);
//				if(self.moveButton.oldMouseX!=null) deltaX=(pointerCoords.x-self.moveButton.oldMouseX);
//				else																self.moveButton.oldMouseX=pointerCoords.x;
//				if(self.moveButton.oldMouseY!=null) deltaY=(pointerCoords.y-self.moveButton.oldMouseY);
//				else																self.moveButton.oldMouseY=pointerCoords.y;
//
//				if(!deltaX && !deltaY)	return;
//				const MOVE_FACTOR=.1;
//				const newLocation={x:self.position.x+deltaX*MOVE_FACTOR,y:self.position.y+deltaY*MOVE_FACTOR};			
//				self.setLocation(newLocation);
//
//			});
//	}
	
	/*private static*/getBrowserElement=function(doOnChangeURL,parentElement=null){
		
		parentElement=nonull(parentElement,document.body);
		
		const container=document.createElement("div");
		parentElement.appendChild(container);
		container.style.display="flex";
		container.style["flex-direction"]="column";
		container.style.width="100%";
		container.style.height="100%";
		container.style.border="solid 2px";
		container.style["pointer-events"]="all";

			const toolbar=document.createElement("div");
			container.appendChild(toolbar);
			toolbar.style.display="flex";
			toolbar.style["flex-direction"]="row";
	
				const previous=document.createElement("button");
				toolbar.appendChild(previous);
				previous.container=container;
				previous.innerHTML="<";
	
				const next=document.createElement("button");
				toolbar.appendChild(next);
				next.container=container;
				next.innerHTML=">";
	
				const searchField=document.createElement("input");
				searchField.container=container;
				toolbar.appendChild(searchField);
				searchField.type="text";
				searchField.width="100";
	//		searchField.value="https://";
				// DBG
				searchField.value="https://google.ca?igu=1";
				container.searchField=searchField;
	
				const go=document.createElement("button");
				toolbar.appendChild(go);
				go.container=container;
				go.innerHTML="GO";
		
			const iframe=document.createElement("iframe");
			iframe.container=container;
			container.appendChild(iframe);
	//		iframe.style.width="100%";
	//		iframe.style.height="100%";
			container.iframe=iframe;
	
			iframe.addEventListener("load",(event)=>{
				// When URL changes :
				const iframeLocal=event.target.container.iframe;
				
				// DBG
				lognow("iframeLocal.document",iframeLocal.document);
				
			});
	
			// Methods :
			
			// TODO :
			previous.addEventListener("click",()=>{
				doOnChangeURL();
			});
			// TODO :
			next.addEventListener("click",()=>{
				doOnChangeURL();
			});
	
			
			const doOnGo=(event)=>{
				const url=event.target.container.searchField.value;
				event.target.container.iframe.src=url;
				doOnChangeURL();
			};
			
			searchField.addEventListener("keydown",(event)=>{
				if(event.keyCode===13)	doOnGo(event);
			});
	
			go.addEventListener("click",doOnGo);
	
		
	}
	
	
	
//	/*private static*/getVNCElement(doOnChangeURL,parentElement=null){
//		parentElement=nonull(parentElement,document.body);
//		
//		const container=document.createElement("div");
//		parentElement.appendChild(container);
//		container.style.display="flex";
//		container.style["flex-direction"]="column";
//		container.style.width="100%";
//		container.style.height="100%";
//		container.style.border="solid 2px";
//		container.style["pointer-events"]="all";
//		
//			const toolbar=document.createElement("div");
//			container.appendChild(toolbar);
//			toolbar.style.display="flex";
//			toolbar.style["flex-direction"]="row";
//	
//				const searchField=document.createElement("input");
//				searchField.container=container;
//				toolbar.appendChild(searchField);
//				searchField.type="text";
//				searchField.width="100";
//				searchField.value=nonull(this.url,"https://");
//				container.searchField=searchField;
//
//				const passwordField=document.createElement("input");
//				passwordField.container=container;
//				toolbar.appendChild(passwordField);
//				passwordField.type="password";
//				passwordField.width="50";
//				passwordField.value=nonull(this.url,"https://");
//				container.passwordField=passwordField;
//	
//				const connect=document.createElement("button");
//				toolbar.appendChild(connect);
//				connect.container=container;
//				connect.innerHTML="CONNECT";
//		
//			const iframe=document.createElement("iframe");
//			iframe.container=container;
//			container.appendChild(iframe);
//	//		iframe.style.width="100%";
//	//		iframe.style.height="100%";
//			container.iframe=iframe;
//			iframe.addEventListener("load",(event)=>{
//				// When URL changes :
//				
//			});
//	
//			// Methods :
//			
//			const doOnConnect=(event)=>{
//				const url=event.target.container.searchField.value;
//				event.target.container.iframe.src=url;
//				doOnChangeURL();
//			};
//			
//			searchField.addEventListener("keydown",(event)=>{
//				if(event.keyCode===13){
//					doOnConnect(event);
//				}
//			});
//			passwordField.addEventListener("keydown",(event)=>{
//				if(event.keyCode===13){
//					doOnConnect(event);
//				}
//			});
//	
//			connect.addEventListener("click",doOnConnect);
//		
//	}
	
	
	
	
}



