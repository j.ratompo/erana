
// TODO : FIXME : MAGIC NUMBER
const MAGIC_CSS3DRENDERER_SCALE_FACTOR=.1;

class GameElement3DCSS{

	constructor(){
	
		
		this.object3D=null;
		this.elementHTML=null;
		this.element3D=null;
		
		this.anchorId=null;
		this.mirroredElement=null;
		
		
	}

	init(gameConfig, overriddenBasePath=null){
		
		this.gameConfig=gameConfig;
		this.overriddenBasePath=overriddenBasePath;
		
		
	}
	

	
	
	initAfterLevelLoading(){
		
		this.elementHTML=this.createHTMLElement();
		window.eranaScreen.loadElementInScene(this, this.overriddenBasePath);

	}
	
	
	/*protected*/createHTMLElement(){
		
		
		let htmlElementType="div";
		if(this.modelConfig && this.modelConfig.htmlElementType)	htmlElementType=this.modelConfig.htmlElementType;
		let elementHTML=document.createElement(htmlElementType);
		
		
		if(this.modelConfig){
			if(!empty(this.modelConfig.htmlElementType)){
				
				if(!empty(this.modelConfig.innerHTML)){
					elementHTML.innerHTML=this.modelConfig.innerHTML;
					
				}else if(!empty(this.modelConfig.src)){
					
					let universePath=null;
					if(overriddenBasePath)	universePath=overriddenBasePath;

					// (will use introspection to determine the specific base path, if no overriding path is provided :)
					elementHTML.src=getConventionalFilePath(this.gameConfig.basePath,nonull(universePath,this), "universe")+this.modelConfig.src;
	
				}
						
			}
		}
		
						
		let CSSWidth=null;
		let CSSHeight=null;
		if(this.size){
			const factor=(1/MAGIC_CSS3DRENDERER_SCALE_FACTOR);
			
			if(this.size.w)		CSSWidth=this.size.w*factor;
			if(this.size.h)		CSSHeight=this.size.h*factor;
			
			if(CSSWidth)		elementHTML.style.width=CSSWidth+"px";
			if(CSSHeight)		elementHTML.style.height=CSSHeight+"px";
		}

		// DBG
//	elementHTML.style.background=new THREE.Color(Math.random() * 0xffffff).getStyle();
		elementHTML.style.overflow="hidden";
		elementHTML.style["z-index"]=0;
				
					
//	// DBG ONLY :
//	elementHTML.id=getUUID();
		// DBG ONLY :
		document.body.appendChild(elementHTML);

		
		
		return elementHTML;
	}
	
	

	
	 // DOESN'T uses loadModel function set by default :
///*public*/loadModel(controller,isStereo,sceneWEBGL,sceneWEBGLAnchored,sceneCSS3,overriddenBasePath){
	/*public*/loadModel(controller,isStereo,sceneWEBGL,sceneCSS3,overriddenBasePath){

		
		const self=this;
		
		return new Promise((resolve)=>{
				
				let object3D=null;
				if(!sceneWEBGL){
					// TRACE
					lognow("ERROR : No WEBGL scene to load model into.");
				}else{
		
					let scene3D=null;
		//			if(self.anchorId)	scene3D=nonull(sceneWEBGLAnchored,sceneWEBGL);
		//			else							
					scene3D=sceneWEBGL;
		
					if(!scene3D){
						// TRACE
						lognow("ERROR : No 3D scene to load model into.");
					}else{
						
						
						// create the plane mesh
						// DEBUG ONLY :
			//			const material=new THREE.MeshBasicMaterial({ color: 0x000000, wireframe: true, wireframeLinewidth: 1, side: THREE.DoubleSide });
						const material=new THREE.MeshBasicMaterial();
						material.color.set("black");
						//DBG
						material.opacity=0.6;
			//		material.opacity=0;
						material.blending=THREE.NoBlending;
						material.side = THREE.DoubleSide;
				
						let w=1;
						let h=1;
						if(self.size){
							w=nonull(self.size.w,w);
							h=nonull(self.size.h,h);
						}
						
						const geometry=new THREE.PlaneGeometry(w,h);
						object3D=new THREE.Mesh(geometry, material);
			
						// CAUTION : CSS 3D objects are not rootable :
						// add it to the WebGL scene
						scene3D.add(object3D);
					}
					
				}
				
				
				let element3D=null;
				if(!sceneCSS3){
					// TRACE
					lognow("ERROR : No CSS3 scene to load model into.");
					return;
				}
					
				// create the dom Element
				const elementHTML=self.getElementHTML();
		
				// create the object3d for self element
				element3D=new THREE.CSS3DObject(elementHTML);
				
				element3D.scale.multiplyScalar(MAGIC_CSS3DRENDERER_SCALE_FACTOR);
				
				
				
				sceneCSS3.add(element3D);
		
			
				
				
				self.object3D=object3D;
				self.element3D=element3D;
				
				if(self.position){
					if(self.object3D){
						
						// CAUTION : CSS 3D objects are not rootable :
						self.initLocation(self.object3D);
						
					}
					if(self.element3D){
						self.element3D.position.set(nonull(self.position.x,0), nonull(self.position.y,0), nonull(self.position.z,0));
						self.element3D.rotation.set(nonull(self.position.g,0), nonull(self.position.a,0), nonull(self.position.b,0));
		
						if(isStereo){
		
							if(!self.isMirrored){
							
								// Necessary to add a duplicate HTML element to the other side :
								controller.drawMainLoopToOverride("css3","right");
		
								const forcedElement=elementHTML.cloneNode(true);
								controller.sceneManager.renderers3D["left"]["css3"].domElement.children[0].appendChild(forcedElement);
		
		
							}else{
								
								
									convertHTMLToImage(elementHTML,(image)=>{
										
		//							// DBG
		//							elementHTML.style.display="none";
		//
		//							// Necessary to add a duplicate HTML element to the other side :
		//							controller.drawMainLoopToOverride("css3","right");
		
										self.mirroredElement=elementHTML.cloneNode(true);
										controller.sceneManager.renderers3D["left"]["css3"].domElement.children[0].appendChild(self.mirroredElement);
											
									  while (self.mirroredElement.firstChild) {
								       self.mirroredElement.removeChild(self.mirroredElement.firstChild);
								    }
										
										self.mirroredElement.style["background-image"]="url('"+image.src+"')";
										
									},"png",0.5);
									
							
							
		//						const mirroredElement=elementHTML.cloneNode(true);
		//						self.mirroredElement=mirroredElement;
		//						controller.sceneManager.renderers3D["left"]["css3"].domElement.children[0].appendChild(mirroredElement);
		//					
		//					  while (mirroredElement.firstChild) {
		//				    	mirroredElement.removeChild(mirroredElement.firstChild);
		//				   	}
		//						self.updateCloneImage(elementHTML);
							}
						}
					
					
					}
		
				}
			
	
				// TODO : FIXME : DUPLICATED CODE !
				//if(sceneWEBGLAnchored)
				self.linkToAnchor();
			

				resolve({object3D:self.object3D,element3D:self.element3D});
		});
		
	}
	
	
	// TODO : FIXME : DUPLICATED CODE
	/*private*/initZeroLocation(object3D){
			object3D.position.set(0,0,0);
			object3D.rotation.set(0,0,0);
	}
	
	// TODO : FIXME : DUPLICATED CODE
	/*private*/initLocation(object3D){
			if(this.position){
				object3D.position.set(nonull(this.position.x,0), nonull(this.position.y,0), nonull(this.position.z,0));
				object3D.rotation.set(nonull(this.position.g,0), nonull(this.position.a,0), nonull(this.position.b,0));
			}else{
				this.initZeroLocation(object3D);
			}
	}
	
	/*public*/refresh(ctx){
		
		const elementHTML=this.getElementHTML();
		if(!elementHTML)	return;
		
		if(this.isMirrored && this.isRefreshedContinuously)		this.updateCloneImage(elementHTML);
		
	}
	
	
	/*private*/updateCloneImage(elementHTML){
		
		
		const QUALITY=0.8;
		
		const self=this;
		convertHTMLToImage(elementHTML,(image)=>{
								
//		// DBG
//		elementHTML.style.display="none";

			if(self.mirroredElement && self.mirroredElement.style)		self.mirroredElement.style["background-image"]="url('"+image.src+"')";

		},"png",QUALITY);
	
	}
	
	
	/*private*/setLocation(locationParam){
		
		let location;
		if(this.location){
			this.location.setLocation(locationParam);
			location=this.location;
		}else{
			location=locationParam;
		}
	
		if(this.object3D){
			this.object3D.location.set(nonull(location.x,0), nonull(location.y,0), nonull(location.z,0));
		}
		if(this.element3D){
			this.element3D.location.set(nonull(location.x,0), nonull(location.y,0), nonull(location.z,0));
		}

		
	}
	
	
	// TODO : FIXME : DUPLICATED CODE !
	/*private*/linkToAnchor(){
		
		if(!this.anchorId)	return;
		
		const anchor=this.parent.findAnchorById(this.anchorId);
		if(anchor)	anchor.addLinkedObject(this);
		
	}
	
	
	/*public*/doOnAnchorLost(){
		
	}
	
	/*public*/doOnAnchorFound(vertices){
		
	}
	

	/*public*/getElementHTML(){
		return this.elementHTML;
	}


}