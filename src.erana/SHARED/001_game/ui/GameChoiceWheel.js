class GameChoiceWheel{



	constructor(concernedObject,choices=[],configParam=null,isRandomizedChoicesOrder=false) {
		
		this.SELECTION_MARGIN=40;
		

		this.isVisible=true;
		
		this.concernedObject=concernedObject;
		
		this.choices=choices;
		this.isRandomizedChoicesOrder=isRandomizedChoicesOrder;
		
		
		const DEFAULT_CONFIG={radius:150,thickness:60,
			colors:{selected:"#00FF00",unselected:"#FF8888",inactive:"#888888",timeout:"#FFFFFF"},
			isDiegetic:false,timeoutConfig:null,hideChoiceLabels:false};
			
		const config=nonull(configParam,DEFAULT_CONFIG);
		this.radius=nonull(config.radius,DEFAULT_CONFIG.radius);
		this.thickness=nonull(config.thickness,DEFAULT_CONFIG.thickness);
		
		this.colors=nonull(config.colors,DEFAULT_CONFIG.colors);
		this.isDiegetic=nonull(config.isDiegetic,DEFAULT_CONFIG.isDiegetic);
		this.timeoutConfig=nonull(config.timeoutConfig,DEFAULT_CONFIG.timeoutConfig); // Example : {delayMillis:8000,defaultIndex:0}
		this.hideChoiceLabels=nonull(config.hideChoiceLabels,DEFAULT_CONFIG.hideChoiceLabels);
		
		this.lastTime=null;
		
		let gapAngle=this.thickness/this.radius;
		this.demiGapAngle=gapAngle*.5;
		this.stepAngle=((Math.PI)/Math.max(0,this.choices.length))*2;
		
		this.choiceIndex=null;

		this.startAngle=Math.PI*.5;

//		this.oldDeltaX=null;
//		this.oldDeltaY=null;


	}
	
	// INIT
	init(gameConfig){
		this.gameConfig=gameConfig;

		return this;
	}
	
	start(){
		if(this.timeoutConfig && this.timeoutConfig.delayMillis)
			this.lastTime=getNow();
		return this;
	}
	
	// METHODS
	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	


	/*private*/getAngleBoundaries(angle){
	
		let min=Math.coerceAngle(angle,true,true);
		let max=Math.coerceAngle(angle+this.stepAngle,true,true);
		
		return {min:min, max:max}
	}
	
	/*public*/reset(){
		this.choiceIndex=null;
	}
	
	
	/*public*/getDefaultChoiceIndex(){
		if(!this.timeoutConfig)	return null;
		return nonull(this.timeoutConfig.defaultIndex,0);
	}
	
	
	/*public*/getChoiceIndex(deltaX,deltaY){
	
		let choiceIndex=null;
		
	
		if(Math.abs(deltaX)<this.SELECTION_MARGIN && Math.abs(deltaY)<this.SELECTION_MARGIN){
			this.reset();
			return null;
		}

//		let derivativeDeltaX=(this.oldDeltaX==null?deltaX:(deltaX-this.oldDeltaX));
//		let derivativeDeltaY=(this.oldDeltaY==null?deltaY:(deltaY-this.oldDeltaY));
//		this.oldDeltaX=deltaX;
//		this.oldDeltaY=deltaY;
		
//		if(Math.abs(derivativeDeltaX)<this.SELECTION_MARGIN && Math.abs(derivativeDeltaY)<this.SELECTION_MARGIN)	return null;
		
//		let selectionAngle=Math.coerceAngle(Math.atan2(derivativeDeltaY, derivativeDeltaX),true,true);

		let selectionAngle=Math.coerceAngle(Math.atan2(deltaY, deltaX),true,true);
		
		
//		// DBG
//		console.log("selectionAngle:",Math.toDegrees(selectionAngle));
		
		let angle=this.startAngle;
		
		
		if(empty(this.choices) || this.choices.length==1){
		
			choiceIndex=0;
			
		}else{
		
			let self=this;
			foreach(this.choices,(choice,index)=>{
				
				let boundaries=self.getAngleBoundaries(angle);
			
				let min=boundaries.min;
				let max=boundaries.max;
				
				if(min<=max){
					if(min<selectionAngle && selectionAngle<max){
						choiceIndex=index;
						return "break";
					}
				
				
				}else{
				
					// We look if the selection angle is OUTSIDE the boundaries ! (inverted arc !)
					if((min<selectionAngle && selectionAngle<360) || (selectionAngle<max && 0<selectionAngle)){
						choiceIndex=index;
						return "break";
					}
					
				}
			
				
				angle=Math.coerceAngle(angle+self.stepAngle);
				
			
			});
		}
	
		
		this.choiceIndex=choiceIndex;
		
		if(	  !empty(this.choices) && this.choices.length!=1 
			&& choiceIndex!=null
			&& this.choices[choiceIndex].isAvailable==false) // (default is available considered as true)
				return null;
		return this.choiceIndex;
	}
	

	
	// ---------------------------------------------
	
	/*private*/doDrawingZoomed(ctx, x, y, camera, lightsManager=null, zooms={zx:1,zy:1}){
	
		// Deactivation reason drawing : 
		if(this.concernedObject.getDeactivationReason){
			let deactivationReason=this.concernedObject.getDeactivationReason();
			if(deactivationReason){
				if(this.choiceIndex!=null){
				
					ctx.save();
					
					const label=deactivationReason;
					let messageWidth=ctx.measureText(label).width;
					
					let xText=(x-messageWidth*.5) * zooms.zx;
					let yText=(y) * zooms.zy;
					
					ctx.font = "bold 5vw Arial";
					ctx.fillStyle="#FFFFFF";
					ctx.fillText(label,xText,yText);
		
					ctx.lineWidth = 4 * zooms.zx;
					ctx.strokeStyle = "#222222";
					ctx.strokeText(label,xText,yText);
				
					ctx.restore;
				}
				
				return false;
			}
		}
	
	
		// Timeout default choice displaying :
		if(this.timeoutConfig && this.lastTime && this.timeoutConfig.delayMillis && !empty(this.choices)){
			let timeDiff=getNow()-this.lastTime;
			let timeoutHasPassed=(this.timeoutConfig.delayMillis<timeDiff);
			if(!timeoutHasPassed){
				
				ctx.save();
			
				ctx.beginPath();
				
				ctx.strokeStyle=this.colors.timeout;
				
				ctx.lineWidth=10 * zooms.zx;
				
				let timeFactor=timeDiff/this.timeoutConfig.delayMillis;
				const timerMinAngle=0;
				const timerMaxAngle=Math.coerceAngle((timeFactor*Math.PI*2),true,true);
				
				ctx.arc(x * zooms.zx, y * zooms.zy, this.radius*.7 * zooms.zx, timerMinAngle, timerMaxAngle);
				
				ctx.stroke();

				ctx.closePath();
				
				ctx.restore;

			}else{
			
				this.choiceIndex=nonull(this.timeoutConfig.defaultIndex,0);

				this.concernedObject.finalizeChoice(this.choiceIndex, true);
				
				return false;
			}
		
		}
		
	
		if(this.choiceIndex==null)	return false;

		
		const isInTriggerZone=(this.concernedObject.isInTriggerZone && this.concernedObject.isInTriggerZone());
		let angleCounter=this.startAngle;
		let self=this;
		foreach(this.choices,(choice,choiceIndexLocal)=>{
			
			ctx.save();
			
			ctx.beginPath();
	
			ctx.lineWidth=this.thickness * zooms.zx;

			let isSelected=(self.choiceIndex==choiceIndexLocal);
			
			if(choice.isAvailable==false){ // (default is available considered as true)
				ctx.strokeStyle=self.colors.inactive;
			}else{
				if(isSelected){
					ctx.strokeStyle=self.colors.selected;
				}else{
					ctx.strokeStyle=self.colors.unselected;
				}
			}
		
			let boundaries=self.getAngleBoundaries(angleCounter);
			
			let min=Math.coerceAngle(boundaries.min+self.demiGapAngle,true,true);
			let max=Math.coerceAngle(boundaries.max-self.demiGapAngle,true,true);
			
			const arcOffset=(isSelected && isInTriggerZone?self.thickness:0);
			ctx.arc(x * zooms.zx, y * zooms.zy, (self.radius+arcOffset) * zooms.zx, min, max);
			
			angleCounter=Math.coerceAngle(angleCounter+self.stepAngle);
			
			ctx.stroke();
			
			ctx.closePath();

			ctx.restore;
			
			
		});
		
		
		
		// We draw the choice label if necessary :
		if(!this.hideChoiceLabels){
			
		
			foreach(this.choices,(choice,choiceIndexLocal)=>{
	
				let isSelected=(self.choiceIndex==choiceIndexLocal);
				
				const label=choice.label;
				if(isSelected && label){
				
					const textSize=ctx.measureText(label);
				
					let messageWidth=textSize.width;
					let messageDemiWidth=(messageWidth*.5);
					let messageHeight=(textSize.actualBoundingBoxAscent+textSize.actualBoundingBoxDescent);
				
					let xText=x-messageDemiWidth;
				
					if(isInTriggerZone){
						ctx.save();
						ctx.strokeStyle="#FFFFFF";
						ctx.lineWidth = 6 * zooms.zx;
						
						ctx.strokeRect((xText) * zooms.zx, (y) * zooms.zy - textSize.actualBoundingBoxAscent, messageWidth, messageHeight); 
					
						ctx.restore;
					}
				
				
					ctx.save();
					
					let yText=y;
					
					ctx.font = "bold "+(5 * zooms.zx)+"vw Arial";
					ctx.fillStyle="#FFFFFF";
					ctx.fillText(label, xText * zooms.zx, yText * zooms.zy);
		
					ctx.lineWidth = 4 * zooms.zx;
					ctx.strokeStyle = "#222222";
					ctx.strokeText(label, xText * zooms.zx, yText * zooms.zy);
				
					ctx.restore;
					
					
				}
				
			});
		}

		
	
		return true;
	}
	
	
	drawInUI(ctx, camera){
		
		if(!this.isDiegetic){
			let resolution=this.gameConfig.getResolution();
			const zooms=this.gameConfig.zooms;
			return this.doDrawingZoomed(ctx, resolution.w*.5, resolution.h*.5, camera, null, zooms);
		}
		
		return false;
	}

	
	draw(ctx, camera, lightsManager){
		
		if(this.isDiegetic){
			let pos=this.concernedObject.position;
			const zooms=this.gameConfig.zooms;
			let x=pos.x;
			let y=pos.y;
			return this.doDrawingZoomed(ctx, x, y ,camera, lightsManager, zooms);
		}
		
		return false;
	}
	
	
}
