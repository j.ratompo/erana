
const ORIENTATION_SMOOTHING=false;
const ORIENTATION_SAMPLE_SIZE=4;

// DEBUG
const DEVICE_ORIENTATION_DEBUG=false;
const LOGS=[];



class GameScreenXR extends GameScreen3D{

	constructor(mainId,config,storageKey) {
		super(mainId,config,storageKey);
		
		// VIEW
		this.backgroundLayers=[];
		


		// INIT ALL
		
		
				
		// Smoothing :
		this.cnt=0;
		this.avg={a:0, b:0, g:0};
		
		
		this.canvas={};
		this.videos={};
		this.webcamHandler=null;
		this.webcamHandlers=null;
		this.fingersPointer=null;
		
	
//		// DBG	
//	this.forceVideoTag=true;
		this.forceVideoTag=false;
		
		
		this.arToolkitSource=null;
		this.arToolkitContext=null;

		
		this.mediaHandlerIsReadyListeners=[];
		
		
		this.webcamUsingObjects=[];
		
		this.azimutalAnglesFactors={"horizontal":1, "vertical":1};


		
		

	}
	

	
	
	// MANDATORY METHODS
	
	
	//=============== CONTROLLER METHODS ===============
	
	initOnStartScreenAtLevelStart() {
		super.initOnStartScreenAtLevelStart();


				
		// DEBUG
		if(DEVICE_ORIENTATION_DEBUG){
			let logContainer=document.createElement("div");
			logContainer.style["margin-top"]="60px";
			logContainer.style.position="fixed";
			logContainer.style["z-index"]=99999;
			document.body.insertBefore(logContainer,document.body.children[0]);
			for(let i=0; i<9; i++)		LOGS.push( createInPageLog(i, logContainer) );
		}


		this.setupVRHandlers();
		

		const gameConfig=this.gameConfig;
		if(gameConfig.configXR && gameConfig.configXR.ARFeed){

			const sources=gameConfig.configXR.ARFeed.sources;
			if(sources){
				
				this.canvas=this.createCanvasFor2DIfNecessary();
				
				if(this.forceVideoTag)	this.videos=this.creatVideosFor2DIfNecessary();

				const self=this;
				if(this.sceneManager.isStereo()){

					const width=this.canvas["left"].width;
					const height=this.canvas["left"].height;


					this.webcamHandlers={};
					
					const indexLeft=this.getIndexFromConfigString("left");
					getMediaHandler({webcamIndex:indexLeft},null,{width:width,height:height},
//												(this.forceVideoTag?{videoToUse:this.videos["left"],videoScale:"auto"}:null),
													(this.forceVideoTag?{videoToUse:this.videos["left"],videoScale:this.gameConfig.configXR.videoTagScale}:null),
													null,null,(mh)=>{
						self.webcamHandlers["left"]=mh;
						
						self.setupControls(mh);
						self.setupAnchorSystem(mh,self.sceneManager.camera.camera3Ds["left"]);

					});

					const indexRight=this.getIndexFromConfigString("right");
					getMediaHandler({webcamIndex:indexRight},null,{width:width,height:height},
//												(this.forceVideoTag?{videoToUse:this.videos["right"],videoScale:"auto"}:null),
													(this.forceVideoTag?{videoToUse:this.videos["right"],videoScale:this.gameConfig.configXR.videoTagScale}:null),
													null,null,(mh)=>{
						self.webcamHandlers["right"]=mh;	
					});
					
				}else{	// NORMAL CASE :
					
					const width=this.canvas.width;
					const height=this.canvas.height;


					const index=this.getIndexFromConfigString();
					getMediaHandler({webcamIndex:index},null,{width:width,height:height},
//												(this.forceVideoTag?{videoToUse:this.videos,videoScale:"auto"}:null),
													(this.forceVideoTag?{videoToUse:this.videos,videoScale:this.gameConfig.configXR.videoTagScale}:null),
													null,null,(mh)=>{
						self.webcamHandler=mh;
						
						self.setupControls(mh);
						self.setupAnchorSystem(mh,self.sceneManager.camera.camera3D);

					});
					
				}
				
				
				
				
				
				
				// this.sceneManager.setupPointer(this.canvas);
				
			}
		}

		
		
		
		
		
	}
	
	/*private*/setupAnchorSystem(mediaHandler,camera3D){
		
		this.arToolkitSource=new THREEx.ArToolkitSource({
			sourceType:"webcam",
		});
		const mainDeviceId=mediaHandler.videoDeviceId;
		if(mainDeviceId)	this.arToolkitSource.deviceId=mainDeviceId;
		this.arToolkitSource.init();
		this.arToolkitSource.domElement.style.display="none";
		
		const cameraParamsFilePath=getConventionalFilePath(this.gameConfig.basePath, "ar/", "config")+"camera_param.dat";
		this.arToolkitContext=new THREEx.ArToolkitContext({
			cameraParametersUrl:cameraParamsFilePath,
			detectionMode: "mono"
		});
		// copy projection matrix to camera when initialization complete
		const self=this;
		this.arToolkitContext.init( function onCompleted(){
			
			const matrix=self.arToolkitContext.getProjectionMatrix();
			camera3D.projectionMatrix.copy(matrix);
			
			const sceneWEBGL=self.sceneManager.scenes3D["webgl"];
			
//		const isAnchored=(self.gameConfig.configXR && self.gameConfig.configXR.hasAnchors);
			let scene3D=null;
//			if(isAnchored){
//				const sceneWEBGLAnchored=self.sceneManager.scenes3D["webgAnchoredl"];
//				scene3D=nonull(sceneWEBGLAnchored,sceneWEBGL);
//			}else						
			scene3D=sceneWEBGL;
				
			const sceneCSS3=self.sceneManager.scenes3D["css3"];
			foreach(self.mediaHandlerIsReadyListeners,(method)=>{method(self.arToolkitContext, scene3D, sceneCSS3);});
				
		});

	}
	
	/*private*/setupControls(mediaHandler){
		
		if(!this.gameConfig.configXR)		return;
		
		if(this.gameConfig.configXR.controls3D=="hands"){
			const config={
			  hands: {
			    enabled: true,
			    // The maximum number of hands to detect [0 - 4]
			    maxNumHands: 1,
			    // Minimum confidence [0 - 1] for a hand to be considered detected
			    minDetectionConfidence: 0.5,
			    // Minimum confidence [0 - 1] for the landmark tracker to be considered detected
			    // Higher values are more robust at the expense of higher latency
			    minTrackingConfidence: 0.5,
	//				setup: {
	//			    video: {
	//			      // This element must contain a [src=""] attribute or <source /> with one
	//			      $el: document.querySelector('#my-video')
	//			    }
	//			  }
			  }
			};
			
	//	if(mediaHandler)	config.hands.setup={video:{$el:mediaHandler.video}};
	//	const config={hands: true};
			
			// DOC : https://handsfree.js.org/ref/model/hands.html#data
			this.hands=new Handsfree(config);
			this.hands.start();
			
			// DBG
			document.getElementsByClassName("handsfree-debugger")[0].style.display="none";
			
		}else if(this.gameConfig.configXR.controls3D=="colorFingers"){
			
			this.fingersPointer=getColorFingersPointer(mediaHandler);
			
		}

		
		
		
		
		
			
	}
	
		
	/*protected*/updateControls(){
			super.updateControls();
			
			if(this.hands){
				
				const handsData=this.hands.data.hands;
				//let thumb=this.hands.data.hands.landmarksVisible[4];
				//let index=this.hands.data.hands.landmarksVisible[8];
				
				// DBG
				lognow("handsData:",handsData);

				// TODO : DEVELOP...
				
			}else if(this.fingersPointer){
				this.fingersPointer.update();
			}


			if(this.arToolkitContext && this.arToolkitSource && this.arToolkitSource.ready)
				this.arToolkitContext.update(this.arToolkitSource.domElement);
	}

	
	/*private*/createCanvasFor2DIfNecessary(){
		
		const mainElement=this.mainElement;
		
		let canvas=null;
		if(this.sceneManager.isStereo()){
			// SPECIAL CASE : if we want a css3 renderer AND a stereo vision :  
			// Then we create a special hacked stereo setup :

			canvas={};
			canvas["left"]=getSingleCanvas("2DLayer",mainElement,"left",true);
			canvas["left"].style["z-index"]=0;
			
			canvas["right"]=getSingleCanvas("2DLayer",mainElement,"right",true);
			canvas["right"].style["z-index"]=0;
			
		}else{ // NORMAL CASE :
			canvas=getSingleCanvas("2DLayer",mainElement,null,true);
			canvas.style["z-index"]=0;

		}
		
		return canvas;
	}
	
	/*private*/creatVideosFor2DIfNecessary(){

		const mainElement=this.mainElement;
		
		let videos=null;
		if(this.sceneManager.isStereo()){
			// SPECIAL CASE : if we want a css3 renderer AND a stereo vision :  
			// Then we create a special hacked stereo setup :

			videos={};
			videos["left"]=getSingleVideo("2DLayer",mainElement,"left",true);
			videos["left"].style["z-index"]=0;
			
			videos["right"]=getSingleVideo("2DLayer",mainElement,"right",true);
			videos["right"].style["z-index"]=0;
			
		}else{ // NORMAL CASE :
			videos=getSingleVideo("2DLayer",mainElement,null,true);
			videos.style["z-index"]=0;

		}
		
		return videos;
	}
	
	/*private*/setupVRHandlers(){
		
			// TODO : FIXME : THIS SHOULD BE IN A ControlsManager, OR SOMETHING LIKE THAT :		

		const gameConfig=this.gameConfig;
		const isForceFallbackControls=(gameConfig.configXR && gameConfig.configXR.forceFallbackControls);
		if(!isForceFallbackControls){
			
			let webxrMode="inline";
			if((!gameConfig.configXR || !gameConfig.configXR.ARFeed) || (gameConfig.configXR && gameConfig.configXR.isAR && !gameConfig.configXR.forceFallbackAR))
					webxrMode="immersive-ar";
			
			
			if(this.sceneManager.isCSS3()){
				// TRACE
				lognow("WARN : CAUTION ! On today (27/11/2021), CSS3DRenderer DOES NOT WORK PROPERLY with the XR camera !");
			}


			if(this.sceneManager.isStereo()){
				
				const rendererLeft=this.sceneManager.renderers3D["left"]["webgl"];
				const VRHandlerLeft=getVRHandler(rendererLeft,webxrMode);
				if(VRHandlerLeft)	VRHandlerLeft.start();
				
				const rendererRight=this.sceneManager.renderers3D["right"]["webgl"];
				const VRHandlerRight=getVRHandler(rendererRight,webxrMode);
				if(VRHandlerRight)	VRHandlerRight.start();
				
				// DOES NOT WORK :
				if(!VRHandlerLeft || !VRHandlerRight)	super.setOrbitalControlsActivation(false);
				
			}else{	// NORMAL CASE :
			
				const renderer=this.sceneManager.renderers3D["webgl"];
				const VRHandler=getVRHandler(renderer,webxrMode);
				
				if(VRHandler)		VRHandler.start();
				// DOES NOT WORK :
				else 						super.setOrbitalControlsActivation(false);
				
			}
		
		}else{
			

			this.setFallBackVRControls();
	
			
		}
		
	}
	
	
	/*public*/setAzimutalAnglesFactor(orientation="vertical",deltaValue=0){
		if(!deltaValue)	return;
		this.azimutalAnglesFactors[orientation]+=deltaValue;
		return this.azimutalAnglesFactors[orientation];
	}
	
	/*private*/setFallBackVRControls(){
		
		const self=this;
		const doOnOrientationChange=(event)=>{
				
			// ANGLES
			// CAUTION : RADIANS !
			
	//		UNUSEFUL : if(!event.absolute) return;
			
			// La valeur DeviceOrientationEvent.alpha représente le mouvement de l'appareil, autour de l'axe « z », en degrés sur une échelle de 0° à 360° ;
			// La valeur DeviceOrientationEvent.beta représente le mouvement de l'appareil autour de l'axe « y » en degrés, sur une échelle de -180° à 180°.  Cela représente le mouvement d'avant en arrière de l'appareil ;
			// La valeur DeviceOrientationEvent.gamma représente le mouvement de l'appareil autour de l'axe « x », exprimée en degrés sur une échelle de -90° à 90°. Cela représente le mouvement de gauche à droite de l'appareil.
			
			let a=nonull(event.alpha,0); // z (0 360)    ; y sur chrome
			let b=nonull(event.beta,0);  // y (-180 180) ; z sur chrome
			let g=nonull(event.gamma,0); // x (-90 90)   
	
			if(ORIENTATION_SMOOTHING && self.cnt<ORIENTATION_SAMPLE_SIZE){
				self.avg.a+=a;
				self.avg.b+=b;
				self.avg.g+=g;
				
				self.cnt++;
			}else{
				
				if(ORIENTATION_SMOOTHING){
					a=Math.floor(self.avg.a/self.cnt);
					b=Math.floor(self.avg.b/self.cnt);
					g=Math.floor(self.avg.g/self.cnt);
	
					self.cnt=0;
					self.avg={a:0, b:0, g:0};
				}
			
				let hacks=stupidOrientationHacks["S5"].hackDegrees(a,b,g);
	
				// DBG
				if(DEVICE_ORIENTATION_DEBUG)	LOGS[0].innerHTML="g(X):"+Math.round(hacks.g)+"a(Y):"+Math.round(hacks.a)+"b(Z):"+Math.round(hacks.b);
	
				let aCorrected=hacks.a*this.azimutalAnglesFactors["horizontal"]; // Y axis
				let bCorrected=hacks.b; // Z axis
				let gCorrected=hacks.g*this.azimutalAnglesFactors["vertical"]; // X axis
				
//				// DBG
//				lognow("BEFORE NORM : a:"+Math.round(Math.toDegrees(a))+",b:"+Math.round(Math.toDegrees(b))+";g:"+Math.round(Math.toDegrees(g)));

				const alphaAngle=Math.toRadians(aCorrected,true);
				const betaAngle=Math.toRadians(bCorrected,true);
				const gammaAngle=Math.toRadians(gCorrected,true);

				self.doOnRotation(alphaAngle,betaAngle,gammaAngle);
				
				if(ORIENTATION_SMOOTHING)	self.cnt=0;
			}
	
		};

		window.addEventListener("deviceorientation", doOnOrientationChange);
	}
	
	
	/*private*/doOnRotation(a,b,g){
		
			const sceneManager=this.sceneManager;
			
			sceneManager.camera.setRotation(g,a,b);

	}
	
	
	// FACULTATIVE PUBLIC METHODS
	
	
	
	
	/*private*/getIndexFromConfigString(configName="default"){
		const gameConfig=this.gameConfig;
		const sources=gameConfig.configXR.ARFeed.sources;
		let index=0;
		if(sources[configName]){
			const i=(contains(sources[configName],"+")?sources[configName].split("+")[1]:sources[configName]);
			if(isNumber(i))	index=parseInt(i);
		}
		return index;
	}
	

	//=============== VIEW METHODS ===============
	
	/*private*/drawAll(webcamHandler, canvas,
//	 video=null
		){
			const ctx=canvas.getContext("2d");

			ctx.save();
			if(!this.forceVideoTag && webcamHandler){
				// Normal case :
				const image=webcamHandler.video;
				this.drawWebcamImage(ctx, image, canvas.width, canvas.height);
			}
			// OLD (BUT KEEP):
//			else if(video){
//				const image=video;
//				ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
//			}
			ctx.restore();
			
			
			
			const rootContainer=this.getSubclassRootContainer();
			if(rootContainer.getDrawables){
				const drawables=rootContainer.getDrawables();
				if(drawables)	super.refreshAll3D(ctx,drawables);
			}

			
	}
	
	
	
	/*private*/drawWebcamImage(ctx, image, width, height){
			
			if(!image)	return;
			
			// OLD (BUT KEEP):
//			drawImageAndCenterWithZooms(ctx,image,0,0,{zx:1,zy:1},{x:"left",y:"top"},null,null,width,height);
			ctx.drawImage(image, 0, 0, width, height);

			
			
			const imageData = ctx.getImageData(0, 0, width, height);
			this.drawWebcamUsingObjects(ctx, imageData);
	}
	
	
	
	/*private*/drawWebcamUsingObjects(ctx, webcamImageData){
	
		foreach(this.webcamUsingObjects,(overlayedObject)=>{
			overlayedObject.draw(ctx, webcamImageData);
		},(overlayedObject)=>overlayedObject.canDraw());
		
	}
	
	/*public*/addWebcamUsingObject(object){
		this.webcamUsingObjects.push(object);
	}

	
	// -----------------------------------------------------------

	/*protected*//*OVERRIDES*/drawMainLoopToOverride(rendererType, side){
		
		if(side){
			if(this.webcamHandlers){
				// OLD (BUT KEEP):
				this.drawAll(this.webcamHandlers["left"],  this.canvas["left"]/*,  this.videos["left"]*/);
				this.drawAll(this.webcamHandlers["right"], this.canvas["right"]/*, this.videos["right"]*/);
			}
		}else{	// NORMAL CASE :
			if(this.webcamHandler){
				// OLD (BUT KEEP):
				this.drawAll(this.webcamHandler, this.canvas/*, this.videos*/);
			}
		}


		let result=super.drawMainLoopToOverride(rendererType, side);
		return result;
	}

	
	
}




const stupidOrientationHacks={
	
	"S5":{
		hackDegrees:function(a,b,g){
			let aCorrected=a; // Y axis
			// CAUTION !!! ON BETA ANGLE, IT DEPENDS ON HOW YOU PUT YOUR PHONE IN THE VR HEADSET !!!
			let bCorrected=b; // Z axis
			let gCorrected=g; // X axis
			
			if(0<g && g<=90){ // Positive gamma hemisphere
				aCorrected=180-a;
				// positive hemidisk :
				if(90<b && b<=180)				bCorrected=180-b;
				else if(-180<b && b<=-90)		bCorrected=-180-b;
				gCorrected=90-g;
			}else if(-90<g && g<=0){ // Negative gamma hemisphere
				// positive hemidisk :
				if(180<a && a<=360)			aCorrected=360-a;
				else if(0<a && a<=180)		aCorrected=-a;
				gCorrected=-90-g;
			}
			
			// CAUTION !!! «-1*» DEPENDS ON HOW YOU PUT YOUR PHONE IN THE VR HEADSET !!!
			// CAUTION !!! «-1*» DEPENDS ON HOW YOU PUT YOUR PHONE IN THE VR HEADSET !!!
			// OLD :
//			aCorrected=-aCorrected-90;

			aCorrected=-aCorrected;
			bCorrected=-bCorrected;
						
			// bottom of the phone to the left (outer camera to the right):		
//			return {a:-aCorrected-270,b:-bCorrected-180,g:gCorrected-90};

			// bottom of the phone to the right (outer camera to the left) :
			return {a:aCorrected, b:bCorrected, g:gCorrected};
		},
	},
}


// DEBUG
// Special logging
function createInPageLog(i,parentContainer=null){

	let logLocal=document.createElement("div");
	logLocal.id="logger_"+i;
	logLocal.style.color="#FFFFFF";
	logLocal.style.display="block";
	if(parentContainer)	parentContainer.insertBefore(logLocal,parentContainer.children[0]);
	else								document.body.insertBefore(logLocal,document.body.children[0]);
	
	return logLocal;
}