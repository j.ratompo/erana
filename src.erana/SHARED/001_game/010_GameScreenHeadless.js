

class GameScreenHeadless extends GameScreen{

	constructor(config, storageKey) {
		super(config, storageKey);
		
		
	}
	
	
	/*protected*/
	drawMainLoopHeadless(selfParam){
		
		if(!this.isModelReady)	return false;
		
		const rootContainer=selfParam.getSubclassRootContainer();
		let drawables=rootContainer.getDrawables();
		
		// Moving
		selfParam.sceneManager.moveAll(rootContainer, drawables);
		
		// Drawing
		// (the view only draws the current level from the model :)		
		selfParam.drawMainLoopToOverride();
		
		return true;
	}
	
	
	
	/*protected*/drawMainLoopToOverride(){
		/*DO NOTHING*/
	}

	
	// -----------------------------------------------------------


}
