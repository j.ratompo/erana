const CONDITION_UI_OPACITY=.4;
const CONDITION_UI_COLOR="#BBBBBB";


class GameGauge{

	constructor() {
	
		this.DISPLAY_GAUGE_MAX=99999; // (for displaying in no max mode only.)

		this.lastStateIsFunctional=true;
		this.lastStateIsAlive=true;

	}

	// INIT
	
	init(gameConfig){
		this.gameConfig=gameConfig;
		
		if(this.noMax || !this.gaugeMax)	this.gaugeMax=Number.MAX_VALUE-1;
	}
	
	

	// METHODS
	
	/*public*/checkFunctionnality(){
		
		let parent=this.parent;
		
		let isFunctional=this.isFunctional();
		if(!this.lastStateIsFunctional && isFunctional){
			if(parent.doOnChangeState)	parent.doOnChangeState("functional",true);
			this.lastStateIsFunctional=true;
		}else if(this.lastStateIsFunctional && !isFunctional){
			if(parent.doOnChangeState)	parent.doOnChangeState("functional",false);
			this.lastStateIsFunctional=false;
		}
		
		let isAlive=this.isAlive();
		if(!this.lastStateIsAlive && isAlive){
			if(parent.doOnChangeState)	parent.doOnChangeState("alive",true);
			this.lastStateIsAlive=true;
		}else if(this.lastStateIsAlive && !isAlive){
			if(parent.doOnChangeState)	parent.doOnChangeState("alive",false);
			this.lastStateIsAlive=false;
		}
		
	}
	
	/*private*/isFunctional(){
		if(this.breakdownThresholdPercent)
			return ((nonull(this.breakdownThresholdPercent,0)*.01)*this.gaugeMax < this.gaugeValue);
		return this.isAlive();
	}
	
	/*private*/isAlive(){
		return (0 < this.gaugeValue);
	}
	
	
	/*private*/isAtMaxCondition(){
		return (this.gaugeMax <= this.gaugeValue);
	}
	
	/*public*/affectCondition(delta){
		if(!delta/*case value is null or 0*/)	return;
		if(delta<0){
			this.damageCondition(-delta);
		}else{
			this.repairCondition(delta);
		}
	}
	
	
	/*public*/repairCondition(amount=null){
		
		// (If no amount, then we repair the whole condition)
		if(amount==null){
			this.gaugeValue=this.gaugeMax;
		}else{
			this.gaugeValue=Math.min(this.gaugeMax, Math.round(this.gaugeValue+amount));
		}
		
		// We reset the thresholds messages :
		if(this.thresholdsMessages){
			foreach(this.thresholdsMessages,(t)=>{
				let thresholdValue=nonull(t.value,(nonull(t.valuePercent,0)*.01)*this.gaugeMax);
				if(thresholdValue<=this.gaugeValue){
					t.hasBeenDisplayed=false;
				}
			},(t)=>{	return t.hasBeenDisplayed;	});
		}
	}

	
	/*protected*/damageCondition(amount){
		
		if(!amount)	return;

		this.gaugeValue=Math.max(0, Math.round(this.gaugeValue-amount));
		
		// We display the thresholds messages :
		if(this.thresholdsMessages){
			foreach(this.thresholdsMessages,(t)=>{
				let thresholdValue=nonull(t.value, (nonull(t.valuePercent,0)*.01)*this.gaugeMax);
				if(this.gaugeValue<thresholdValue){
					// MESSAGE
					window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,t.message);
					t.hasBeenDisplayed=true;
				}
			},(t)=>{	return !t.hasBeenDisplayed;	});
		}
		
	}
	
	
	
	// ======================================================================
	
	/*public*/drawGaugeInUI(ctx,gameThumbParam=null){
		
		let gameThumb=gameThumbParam;
		if(this.gameThumb)	gameThumb=this.gameThumb;
		if(!gameThumb || !gameThumb.img)		return false;
		
		
		let pos=gameThumb.getPosition();
		
		if(!pos/*case not initialized yet*/)		return false;

		if(this.gaugeValue==null) return false;
		
		let conditionRate;
		if(this.noMax)	conditionRate=this.gaugeValue/(!this.gaugeDisplayMax?this.gaugeDisplayMax:this.DISPLAY_GAUGE_MAX);
		else						conditionRate=this.gaugeValue/this.gaugeMax;
		
		// TODO : FIXME : ADD EVERYWHERE
		const zooms=(this.unaffectedByZooms?{zx:1,zy:1}:this.gameConfig.zooms);
		
		let x=pos.x;
		let y=pos.y;
		
		ctx.save();
		
		ctx.globalAlpha=CONDITION_UI_OPACITY;
		
		ctx.fillStyle=CONDITION_UI_COLOR;
		
		
		let gaugeHeight=gameThumb.size.h*conditionRate;
		ctx.fillRect(
					x * zooms.zx, (y+gameThumb.size.h-gaugeHeight) * zooms.zy,
					gameThumb.size.w * zooms.zx, gaugeHeight * zooms.zy);

		ctx.restore();

		
		// Percent :
		if(this.displayNumericValue){
			
			ctx.save();	
			
			ctx.font="bold " + (4.5 * zooms.zx) + "vw Arial";  
			ctx.globalAlpha=.9;
			ctx.fillStyle="#000000";
			
			let numericValue;
			if(!this.noMax){
				const percentValue=Math.round(conditionRate*100)+"%";
				numericValue=Math.round(conditionRate*100)+"%";
			}else{
				numericValue=Math.round(this.gaugeValue);
			}
			
			ctx.fillText(numericValue, (x+gameThumb.size.w*.1) * zooms.zx, (y+gameThumb.size.h) * zooms.zy);
			
			ctx.restore();
		}

		
		return true;
	}
	
	
	/*public*/drawInUI(ctx,camera,gameThumbParam=null){
	
		let gameThumb=gameThumbParam;
		if(this.gameThumb)		gameThumb=this.gameThumb;
		if(!gameThumb)		return false;

		let thumbImagePosition=gameThumb.drawDynamicThumbnailUI(ctx,camera);

		if(!gameThumb.img)		return false;

		
		let hasDrawnGauge=this.drawGaugeInUI(ctx,this.gameThumb);
		return hasDrawnGauge;
	}

}
