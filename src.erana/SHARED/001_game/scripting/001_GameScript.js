
const SCRIPT_REFRESH_RATE_MILLIS=1000;


class GameScript{

	constructor(delayedStartAtBeginning=false) {
		
		
		this.conditionsResults=[];

		this.conditionsDelays=[];
		this.conditionsFunctionsCalls=[];
		this.hasBeenParentTriggered=false;
		

		this.routine=getMonoThreadedRoutine(this,SCRIPT_REFRESH_RATE_MILLIS);

		this.reachAmount=null;
		this.reachTarget=null;
		
		
		this.delayedStartAtBeginning=delayedStartAtBeginning;
	}
	
	// INIT
	init(gameConfig, doAdditionnalTreatmentOnStop=null){
		this.gameConfig=gameConfig;
		this.doAdditionnalTreatmentOnStop=doAdditionnalTreatmentOnStop;
		
		this.routine.setDurationTimeFactorHolder(window.eranaScreen);
		
		
		window.eranaScreen.registerStartable(this);
		
		
	}
	
	// METHODS
	
	
	/*public*/startStartable(concernedObject=null) {
		this.concernedObject=nonull(concernedObject, this.parent);

		
		// We register and initialize the conditions of triggering the script :
		
		
		if(!empty(this.conditionsTriggers)){
			
		
			let self=this;
			foreach(this.conditionsTriggers,(condition,i)=>{
				
				self.conditionsResults.push(false);
				
	
				let reachCondition=condition["REACHES"];
				let hasValueCondition=condition["HAS_VALUE"];
				let isNullCondition=condition["IS_NULL"];
				if(reachCondition){ // amount reaches target condition :
					
					let reachConditionArgs=reachCondition.args;
					self.conditionsFunctionsCalls.push(
						{	
						  comparisionType:reachConditionArgs[1],
							functionName:reachConditionArgs[0], 
							target:reachConditionArgs[2],
							args:reachConditionArgs[3],
							isTriggerableSeveralTimes:condition.isTriggerableSeveralTimes===true/*default is FALSE !*/
						}
					);
				
				}else if(hasValueCondition){
					
					let split=hasValueCondition.split(":");
					
					self.conditionsFunctionsCalls.push(
							{	
								comparisionType:"==",
								attributeName:split[0], 
								target:split[1],
								isTriggerableSeveralTimes:condition.isTriggerableSeveralTimes===true/*default is FALSE !*/
							});
				
				}else if(isNullCondition){
					
					self.conditionsFunctionsCalls.push(
							{	
								comparisionType:"==",
								attributeName:isNullCondition, 
								target:null,
								isTriggerableSeveralTimes:condition.isTriggerableSeveralTimes===true/*default is FALSE !*/
							});
					
				}else if(condition.delayMillis){ // delay condition
					
					self.conditionsDelays.push({time:getNow(),delayMillis:condition.delayMillis});

				}else{ // regular condition :
					
					// We make a foreach, but actually there will be most likely only one function call description here :
					foreach(condition,(functionCallInfos,functionNameToCallKey)=>{
						self.conditionsFunctionsCalls.push(
							{	functionName:functionNameToCallKey, 
								args:functionCallInfos.args,
								isTriggerableSeveralTimes:functionCallInfos.isTriggerableSeveralTimes===true/*default is FALSE !*/
							}
						);
					});
					
				}
				
			});
			
			
		}else{ 
			
			if(!this.unconditionnalStart){
				
			
				// If this script has no condition triggers, then it is only parent-triggered :
				// (i.e its parent has a single childDialog directive, or answers with it as a childDialog in its answers)
				
				this.conditionsResults.push(false);
				this.hasBeenParentTriggered=false;
				
				
			}else{
				
				//	NO : CAUSES BUGS ! : // // If this dialog must start inconditionnally :
				//											 // this.conditionsResults.push(true);
				
				// If this dialog must start inconditionnally (typically the very first starting dialog) :
				this.conditionsResults.push(true);
			}
			
			
		}
		
		this.routine.start();
		
	}
	
	/*public*/stopStartable(){
		this.routine.stop();
		
	}
	
		
	/*public*/triggerParentCondition(){
		this.hasBeenParentTriggered=true;
	}

	/*public*/isStarted(){
		return this.routine.isStarted();
	}
	
	/*public*/doStep(camera){
		this.routine.doStep(this,{camera:camera});
	}
	

	
	/*private*/doOnEachStep(params){
		
		const selfParam=this;
		
		// CAUTION ! Here selfParam and concernedObject ARE NOT THE SAME !
		let self=selfParam;
		
		
		let i=0; // shared !
		foreach(self.conditionsDelays,(conditionDelay)=>{
			
			// CAUTION : SPECIAL CASE : CHILDREN CLASS DIALOGS ARE ALWAYS PLAYED AT 1x TIME FACTOR BUT TRIGGERED AT WHATEVER TIME FACTOR !
			let durationTimeFactor=window.eranaScreen.getDurationTimeFactor();
			if(			hasDelayPassed(conditionDelay.time, conditionDelay.delayMillis*durationTimeFactor)
					&& !self.conditionsResults[i]){
				self.conditionsResults[i]=true;
			}
			
			i++; // shared !
		});
		
		// If this script has no trigger conditions nor delays, then it is is only parent-triggered !
		if(!empty(self.conditionsFunctionsCalls)){
			
			let camera=params.camera;
			
			foreach(self.conditionsFunctionsCalls,(conditionFunctionCall)=>{
				
				let refObject=self.concernedObject.getRefObject?self.concernedObject.getRefObject():self.concernedObject;
				
				if(conditionFunctionCall.isTriggerableSeveralTimes  // if we have isTriggerableSeveralTimes==true then we replace the result.
				|| !self.conditionsResults[i]){
					
					if(conditionFunctionCall.comparisionType   // amount reaches target condition :
						// Function call or attribute value verification:
						){
							
						
						// Function call :
						evaluateCondition(self, refObject, conditionFunctionCall, camera, function(){
							self.conditionsResults[i]=true;	// if we have isTriggerableSeveralTimes==true then we replace the result.
						});
								
						
					}else{ // regular condition :
						
					
//					// DBG
//					if(conditionFunctionCall.functionName=="trainHasReachedPosition")	lognow("regular condition",this);
						
					
						let calculusFunction=refObject[conditionFunctionCall.functionName];
						if(calculusFunction && calculusFunction(refObject, camera, conditionFunctionCall.args)){
						
							self.conditionsResults[i]=true;	// if we have isTriggerableSeveralTimes==true then we replace the result.
						}
	
					}
					
					
				}
				
				
				
				
				i++; // shared !
			});
			
		}else{
			// If this script is only parent-triggered :
		
			
			if(!this.unconditionnalStart){
			
				if(self.hasBeenParentTriggered){
					self.conditionsResults[i]=true;
				}
			}// else { /*DO NOTHING*/ }
			
			
		
		}
		
		
	}

	
	/*private*/terminateFunction(){
		
		let self=this;
		
		// (conjunction) :
		// (this means all trigger conditions must be met to allow the triggering of the script :)
		let result=true;
		foreach(self.conditionsResults,(conditionResult)=>{
			
			if(conditionResult===false) {
				result=false;
				return "break";
			}
			
		});
		
		
//		// DBG
//		if(result && getClassName(self)=="GameDialog")		lognow("SCRIPT TRIGGERED !",self);
		
		
		return result;
	}

	
	/*private*/doOnStop(){
		const selfParam=this;
		
		// CAUTION : «selfParam» is the GameScript / GameDialog / <anything extending GameScript>, here !
		let parentObject=selfParam.parent;

		
		foreach(selfParam.actions,(args, methodNameToCallKey)=>{
			
			if(methodNameToCallKey==="childDialog"){
				
				let childDialogProtoName=args;
				
				// // DBG
				// lognow("TRYING TO START CHILD DIALOG : "+childDialogProtoName);
				
				// TODO : FIXME : DUPLICATED CODE !
				// We trigger the child dialog :
				if(parentObject.getDialogs && parentObject.getDialogs()){
					// CAUTION : Dialog must been started to appear in this list :
					foreach(parentObject.getDialogs(),(childDialog)=>{
						childDialog.triggerParentCondition();
						return "break";
					},(childDialog)=>{		return childDialog.prototypeName===childDialogProtoName;	});
				}
				
				
			}else{
				
				// TODO : FIXME : KINDA DUPLICATED CODE
				let method=selfParam.concernedObject[methodNameToCallKey];
				if(method)	method(selfParam.concernedObject,args);
				
			}
			
			
		});
		
		
		if(selfParam.doAdditionnalTreatmentOnStop)		selfParam.doAdditionnalTreatmentOnStop();
		
		
	}
	
	
	/*public*/getReachAmount(){
		return this.reachAmount;
	}
	
	/*public*/getReachTarget(){
		return this.reachTarget;
	}
	
	
}
