const CINEMATIC_LINE_MARGIN = 20;
const CINEMATIC_TEXT_LINE_DEFAULT_FONT_SIZE=22;

class GameLineTrigger {

	constructor() {
		
		
		this.isVisible=false;

		
		// CAUTION : POSITION HERE IS ONLY FOR THE VISIBILITY ENTERING ZONE ONLY, NOT THE DISPLAY OF THE MESSAGE !
		this.position=new Position(this);

		this.fontSize=CINEMATIC_TEXT_LINE_DEFAULT_FONT_SIZE;
		this.font="monospace";
		this.color="#FFFFFF";
		
	}
	
	// INIT
	
	init(gameConfig){
		
		this.gameConfig=gameConfig;
		
		// CAUTION : AT THIS POINT, WE HAVE NOT THE LINK TO THE PARENT YET :

	}
	
	
	// METHODS
	
	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	
	
	
	/*private*/setDynamicVisibility(camera){
		
		
		let projection = this.gameConfig.projection;
		if(contains(projection, "2D") && !this.hasBeenShown) {
			
		
			if(!this.getIsVisible()){
			
				
				if(this.timeFirstTriggered==null){

					this.updatePositionToParent();
				
					if(this.isInTriggerZone(camera)) {
						this.timeFirstTriggered=getNow();
					}
					
				}else if(
						// TODO : FIXME : Usage of global variables is not recommended, for it makes debugging harder:
						!window.eranaScreen.sceneManager.isLineTriggered){
					
					
					if(this.delay){
						
						let delayHasPassed=hasDelayPassed(this.timeFirstTriggered, this.delay*window.eranaScreen.getDurationTimeFactor());
						if(delayHasPassed){
							
							this.isVisible=true;
							this.timeFirstVisible=getNow();
						
							// TODO : FIXME : Usage of global variables is not recommended, for it makes debugging harder:
							window.eranaScreen.sceneManager.isLineTriggered=true;
						}
					}else{

						this.isVisible=true;
						this.timeFirstVisible=getNow();

						// TODO : FIXME : Usage of global variables is not recommended, for it makes debugging harder:
						window.eranaScreen.sceneManager.isLineTriggered=true;

					}
					
					
				}
				
				
			}else{ // if it is visible :
				
				let duration=nonull(this.duration,this.pauseMillis);
				
				if(duration){
					
					let durationHasPassed=hasDelayPassed(this.timeFirstVisible, duration+nonull(this.delay,0)*window.eranaScreen.getDurationTimeFactor() );
					if(durationHasPassed){
						
						this.isVisible=false;
						this.hasBeenShown=true;

						// TODO : FIXME : Usage of global variables is not recommended, for it makes debugging harder:
						window.eranaScreen.sceneManager.isLineTriggered=false;

					}
				}else if(!this.isInTriggerZone(camera)) {

					this.isVisible=false;
					this.hasBeenShown=true;
					
					// TODO : FIXME : Usage of global variables is not recommended, for it makes debugging harder:
					window.eranaScreen.sceneManager.isLineTriggered=false;


				}
				
				
			}
				
				
				
		}
		
		
	}
	
	
	/*private*/isInTriggerZone(camera){
	
		// CAUTION : POSITION HERE IS ONLY FOR THE VISIBILITY ENTERING ZONE ONLY, NOT THE DISPLAY OF THE MESSAGE !
		// (SINCE  IT IS ALWAYS DISPLAYED AT THE SAME PLACE ON THE SCREEN !)
		
		if(!this.triggerSize)	return false;
		
		
		let offsettedPos=this.position.getParentPositionOffsetted();
		
		
		let zoneOffsetted={x:offsettedPos.x, y:offsettedPos.y, w:this.triggerSize.w, h:this.triggerSize.h};
	
		let result = isInZone(camera.position.getParentPositionOffsetted(), zoneOffsetted , null, this.position.center);
		
		return result;
	}
	
	/*private*/updatePositionToParent(){
		
		let hasATriggerZone=(this.triggerZone && this.triggerZone._2D);

		
		// Position :
		// BECAUSE AT THIS POINT, WE HAVE THE LINK TO THE PARENT NOW :
		
		let parentPositionOffsetted=null;
		if(this.parent && this.parent.position)
			parentPositionOffsetted=this.parent.position.getParentPositionOffsetted();
		if(this.parent && this.parent.position
				&& parentPositionOffsetted.x
				&& parentPositionOffsetted.y  			// (We check if the number is 0 too)
				&& !this.position.centerOffsets.x 
				&& !this.position.centerOffsets.y){		// (We check if the number is 0 too)
	
			let x=0, y=0;
			if(hasATriggerZone){
				x=this.triggerZone._2D.x;
				y=this.triggerZone._2D.y;
			}
			
			this.position.setLocation({x:x, y:y,center:{x:"center",y:"center"}});
					
			this.position.setParentPosition(this.parent.position);
			
		}
		
	
		// Size :
		if(hasATriggerZone){
			this.triggerSize={w:this.triggerZone._2D.w, h:this.triggerZone._2D.h};
		}else{
			if(this.parent) {
				if(this.parent.triggerSize){
					this.triggerSize={w:this.parent.triggerSize.w, h:this.parent.triggerSize.h};			
				}else if(this.parent.size){
					this.triggerSize={w:this.parent.size.w, h:this.parent.size.h};			
				}
			}else {
				if(this.message){
					// CAUTION : DOES NOT SEEM TO WORK WELL :
					let messageWidth=this.message.length*this.fontSize;
					let messageHeight=this.fontSize;
					this.triggerSize={w:messageWidth,h:messageHeight};
				}else{
					this.triggerSize=null;
				}			
			}
		}
		
		

	}
	
	
	drawInUI(ctx,camera){
		
		this.setDynamicVisibility(camera);
		
		if(!this.getIsVisible())	return false;
		
		if(this.message){
			
			const gameConfig=this.gameConfig;
			const resolution=gameConfig.getResolution();
	
			ctx.save();
			
			
			ctx.font="bold "+this.fontSize+"px "+nonull(this.font,"Arial");
			ctx.fillStyle=this.color;
	
			//	DOES NOT SEEM TO WORK WELL : let messageWidth=this.message.length*this.fontSize;
			//	DOES NOT WORK : let messageWidth=ctx.measureText(this.message); // (Necessitates monospace font in order to work)
			let messageHeight=this.fontSize;
			
			
			//		DOES NOT WORK : (because messageWidth doesn't work)
			let x;
//		if(this.parent.textLinesAlignX=="right"){
//			x=resolution.w-messageWidth-CINEMATIC_LINE_MARGIN;
//		}else if(this.parent.textLinesAlignX=="center"){
//			x=(resolution.w-messageWidth)*.5;
//		}else{ // case «left»
//			x=CINEMATIC_LINE_MARGIN;
//		}
			x=CINEMATIC_LINE_MARGIN;
			
			let y;
			if(this.parent.textLinesAlignY=="bottom"){
				y=resolution.h-messageHeight;
			}else if(this.parent.textLinesAlignY=="center"){
				y=(resolution.h-messageHeight)*.5;
			}else{ // case «top»
				y=messageHeight;
			}
			
		
		
			const zooms=gameConfig.zooms;
			x*=zooms.zx;
			y*=zooms.zy;
		
			
			ctx.fillText(this.message,x,y);
			
			ctx.strokeStyle="#000000";
	
			ctx.strokeText(this.message,x,y);
	
			ctx.restore();
		
		}
		
		return true;
	}
	
}
