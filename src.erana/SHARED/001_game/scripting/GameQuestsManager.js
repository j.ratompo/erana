
const DEFAULT_QUESTS_UI_TEXT_FONT="helvetica";
const DEFAULT_QUESTS_UI_TEXT_SIZE=12;
const DEFAULT_QUESTS_UI_TEXT_MODIFYER="bold";

const GLYPH_QUEST_SUCCEED="☑";
const GLYPH_QUEST_FAILED="☒";
const GLYPH_QUEST_CURRENT="☐";


class GameQuestsManager{

	constructor() {
		
		this.isVisible=true;

	}
	
	// INIT
	init(gameConfig){
		this.gameConfig=gameConfig;

	}
	
	// METHODS

	getIsVisible(){
		return this.isVisible;
	}
	
	
	/*private*/getRefObject(){
		return this.parent;
	}
	
	
	/*public*/startQuestByPrototypeName(questProtoName){
		
//	let self=this;
		foreach(this.gameQuests,(gameQuest)=>{
//		gameQuest.start(self);
			gameQuest.startStartable();
		},(q)=>{	return 	q.prototypeName===questProtoName;  });
		
	}
	
	
	/*private*/getNumberOfCompleteQuests(){
		let result=0;
		foreach(this.gameQuests,()=>{	result++;	},(q)=>{	return q.isComplete();	});
		return result;
	}
	
	// -------------------------------------
	
	drawInUI(ctx,camera){

		const gameConfig=this.gameConfig;
		const zooms=gameConfig.zooms;
		
		const resolution=gameConfig.getResolution();

		let ui=this.ui;
		let center=ui.center;
		let color=ui.color;
		
		let stepY=0;
		let self=this;
		let completeQuestsIndex=0;
		let numberOfCompleteQuests=this.getNumberOfCompleteQuests();
		foreach(this.gameQuests,(quest)=>{

			let text=quest.getDirective();
			// If no directive, then we simply hide the quest directive in the UI :
			// (all other treatments are done, though !)
			if(text && quest.getIsKnown()){
				
				if(quest.isComplete() && self.maxNumberOfCompleteQuests){
					completeQuestsIndex++;
					if(1<=completeQuestsIndex && completeQuestsIndex<=(numberOfCompleteQuests-self.maxNumberOfCompleteQuests))
						return "continue";
				}

				
				if(quest.hasOutcome("success")){
					text+=" "+GLYPH_QUEST_SUCCEED;
				}else if(quest.hasOutcome("fail")){
					text+=" "+GLYPH_QUEST_FAILED;
				}else{
					text+=" "+GLYPH_QUEST_CURRENT;
				}
				
				let x;
				if(center.x==="left")	x=0;
				else									x=resolution.w;
				
				let y;
				if(center.y==="top")	y=0;
				else									y=resolution.h;
				
				
				let reachAmount=nonull(quest.getReachAmount(),"");
				let reachTarget=nonull(quest.getReachTarget(),"");
				if(reachAmount!=null && reachTarget!=null){
					text=text.replace("%amount%",reachAmount).replace("%target%",reachTarget);
				}
				
				drawSolidText(ctx,x+ui.xOffset,y+ui.yOffset+stepY,text,color,nonull(ui.textSize,DEFAULT_QUESTS_UI_TEXT_SIZE),DEFAULT_QUESTS_UI_TEXT_FONT,DEFAULT_QUESTS_UI_TEXT_MODIFYER,zooms);

				if(center.y==="top")	stepY+=ui.verticalSpacing;
				else									stepY-=ui.verticalSpacing;

				
			}
			
			
			quest.doStep(camera);
			
		},(q)=>{			return q.isStarted() || q.getIsKnown();	}
		 ,(q1,q2)=>{	return compare(q2.knownTime,q1.knownTime);/*inverted order!*/	});
		
		

	}

	
}
