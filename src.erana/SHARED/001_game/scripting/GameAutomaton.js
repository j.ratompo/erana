const INITIAL_STATE_NAME="INITIAL";
const FINAL_STATE_NAME="FINAL";

class GameAutomaton{

	constructor(){

		this.states={};
		this.states[INITIAL_STATE_NAME]={};

		this.currentStateName=INITIAL_STATE_NAME;
		
		this.started=false;

		this.taskName="default";
		
		this.areAllTreatmentsInDedicatedClass=false;
		
		this.mainTimeout=getMonoThreadedTimeout(this);

	}

	// INIT
	init(gameConfig){
		this.gameConfig=gameConfig;
	}
	
	// METHODS
	/*public*/start(concernedObject) {
		
		if(this.isStarted())	return;
		
		if(!concernedObject){
			// TRACE
			console.log("ERROR : No concerned object to drive for this automaton !");
			return;
		}
		
		this.concernedObject=concernedObject;
		
		this.started=true;

		this.next();
	}
	
	/*private*/stop(){
		this.started=false;
		if(this.mainTimeout)	this.mainTimeout.stop();
	}
		
	/*public*/resume(){
		this.started=true;
	}
	
	/*public*/pause(){
		this.started=false;
	}	
	
	/*public*/isStarted(){
		return this.started;
	}
	
	/*private*/executeMethodAndSetNewState(currentStateAndMethodNameParam){
		
		if(!this.isStarted())	return;
				
		const objectOnWhichToApplyMethods=this.getObjectOnWhichToApplyMethods();
		
		this.currentStateName=currentStateAndMethodNameParam;
		
		if(this.currentStateName!==INITIAL_STATE_NAME && this.currentStateName!==FINAL_STATE_NAME){
			let method=objectOnWhichToApplyMethods[this.currentStateName];
			if(method)
				// cannot write	simply method();  because else in the method, the «this» keyword will be undefined !
				objectOnWhichToApplyMethods[this.currentStateName]();
		}else if(this.currentStateName===INITIAL_STATE_NAME){
			this.next();
		}else if(this.currentStateName===FINAL_STATE_NAME){
			this.stop();
		}

	}
	
	/*private*/getObjectOnWhichToApplyMethods(){
		if(this.areAllTreatmentsInDedicatedClass)	return this;
		return this.concernedObject;
	}
	
	/*public*/getTaskName(){
		return this.taskName;	
	}

	/*public*/doStep(){
		if(!this.isStarted() || !this.mainTimeout)	return;
		this.mainTimeout.doStep();
	}	

	/*public*/next(){

		const state=this.states[this.currentStateName];
				
		// There is a delay :
		const delay=state.delayMillis*window.eranaScreen.getDurationTimeFactor();
		this.mainTimeout.start(delay);
		
	}

	/*private*/doOnStop(){
		const self=this;
		
		const state=this.states[this.currentStateName];
		let transitions=state.transitions;
		if(transitions){
			
			// We stop at the first condition met (the set of conditions is a disjunction in appearence priority order) :
			let isConditionMetForAnyTransition=false;
			
			const objectOnWhichToApplyMethods=self.getObjectOnWhichToApplyMethods();
			
			foreach(transitions,(transitionMethodName,currentStateAndMethodNameParam)=>{
				const isInverted=contains(transitionMethodName,"!");
				if(isInverted)	transitionMethodName=transitionMethodName.replace("!","");
				let conditionFunction=objectOnWhichToApplyMethods[transitionMethodName];
				if(conditionFunction){
					// CAUTION : We cannot write simply «conditionFunction();»  because else in the method, the «this» keyword will be undefined ! And we don't want this !
					if(objectOnWhichToApplyMethods[transitionMethodName]()){
						
						self.executeMethodAndSetNewState(currentStateAndMethodNameParam);
						
						isConditionMetForAnyTransition=(isInverted?false:true);
						return "break";
					}else{
						isConditionMetForAnyTransition=(isInverted?true:false);
					}
				}
			});
			
			// If any condition has been met, 
			// then we already have set the state and executed the method :
			if(isConditionMetForAnyTransition)
				return;
		}
		if(state.onElse){
			// !!! CAUTION !!! The .next() call is ALWAYS assumed to be in client code, or else automaton will not progress !
			// It is only to allow client code to place the .next() call in a callback !
			self.executeMethodAndSetNewState(state.onElse);
			
		}
			
	}


}
