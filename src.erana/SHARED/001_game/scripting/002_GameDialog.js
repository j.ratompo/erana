const PORTRAITS_IMAGE_EXTENSION=".png";
const PORTRAITS_DIRECTORY="portraits/";
const PORTRAIT_DEFAULT_EXPRESSION="default";
const MIN_DIALOG_DURATION_MILLIS=2000;

class GameDialog extends GameScript{

	
	constructor(){
		super();
		
		
		// TECHNICAL ATTRIBUTES
		this.timeouts=[];

		this.cancelled=false;
		
	}
	
	// INIT

	
	
	// METHODS
	
	/*private*/getRefObject(){
		return this.parent;
	}
	
	
	
	
	// Actually, it corresponds to the beginning of the dialog ! (= the end of its conditions watching before being triggered !)
	/*private*//*OVERRIDES*/doOnStop(){
		
		const self=this;
		
		let previousDurationCumulatedMillis=0;
		
		const linesNumber=self.lines.length;
		foreach(self.lines,(dialogLine, index)=>{
			
			
			let isFirstLine=( index==0 );
			let isLastLine=( linesNumber==(index+1) );

			
			if(dialogLine.pauseMillis){
				
				// CAUTION : We must not have a pause as a last line : 
				
				previousDurationCumulatedMillis += dialogLine.pauseMillis;
				
				if(isFirstLine){
					self.doOnDialogFirstLine();
				}
				if(isLastLine){
					self.timeouts.push(setTimeout(function(){
						
						// DBG
						if(self.cancelled)	return;
						
						self.doOnDialogLastLine();
						// CAUTION : SPECIAL CASE : DIALOGS ARE ALWAYS PLAYED AT 1x TIME FACTOR BUT TRIGGERED AT WHATEVER TIME FACTOR !
					},dialogLine.pauseMillis/* SO NO : *window.eranaScreen.getDurationTimeFactor()*/)
					);
				}					
				
				
			}else{
				

				let portrait;
				if(dialogLine.portraits){
					portrait=Math.getRandomInArray(dialogLine.portraits);
				}else{
					portrait=dialogLine.portrait;
				}
				
				// If portrait is a variable , an attribute in the ref object :
				if(contains(portrait,"%")){
					let attributeName=portrait.replace(/%/gim,"");
					let foundPortrait=self.getRefObject()[attributeName];
					// TRACE
					if(!foundPortrait)	lognow("WARN : No portrait value found for attribute name «"+attributeName+"» in ref object.");
					portrait=nonull(foundPortrait,"default"); // (default value)
				}
				
				
				
				previousDurationCumulatedMillis +=
					 self.registerPortrait(self, previousDurationCumulatedMillis, dialogLine, portrait, isFirstLine, isLastLine) + nonull(self.pauseBetweenLinesMillis,0);
			
			}
			
		});
		
		
//		// DBG
//		lognow("	previousDurationCumulatedMillis:"+previousDurationCumulatedMillis,this);
		
	}
	
	
	
	/*private*/doOnDialogFirstLine(){
		this.activateUIsIfNeeded(false);
		
		// TODO : FIXME :
		// As we start the dialog, then we restore the durationTimeFactor to 1 :
		window.eranaScreen.setDurationTimeFactor(1);
		window.eranaScreen.setCanChangeDurationTimeFactor(false);
		
	}
	/*private*/doOnDialogLastLine(){
		this.activateUIsIfNeeded(true);
		
		window.eranaScreen.setCanChangeDurationTimeFactor(true);

		// TODO : FIXME : KINDA DUPLICATED CODE
		if(this.childrenActions){
			let gameQuestsManager=this.parent.getGameQuestsManager();
			foreach(this.childrenActions,(childActionArgs,methodNameToCallKey)=>{
				let method=gameQuestsManager[methodNameToCallKey];
				if(method)	method(gameQuestsManager,childActionArgs);
			});
		}
		
	}
	
	/*private*/activateUIsIfNeeded(active){
		
		if(this.invisibilizesUI){
			let split;
			if(contains(this.invisibilizesUI,",")){
				split=this.invisibilizesUI.split(",");
			}else{
				split=[this.invisibilizesUI];
			}
			foreach(split,(s)=>{
				window.eranaScreen.sceneManager.setUIVisibility(s,active);
				if(s==="selection")		window.eranaScreen.getSelectionManager().setActive(active);
			});
		}
		
	}
	
	
	/*private*/registerPortrait(self, previousDurationCumulatedMillis, dialogLine, portrait, isFirstLine, isLastLine){
		


		let expression=nonull(dialogLine.expression,"default");
		// If no side has been specified, then it is «left» by default. Ie. you have to specify explicitly «side:"right"» to make the portrait appear at the right.
		let side=nonull(dialogLine.side,"left");

		let imageOnlySrc=portrait;
		if(side!=="left")	imageOnlySrc+=".right";
		imageOnlySrc+="."+nonull(expression,PORTRAIT_DEFAULT_EXPRESSION)+PORTRAITS_IMAGE_EXTENSION;

		let messageText=(dialogLine.texts?Math.getRandomInArray(dialogLine.texts):dialogLine.text);
		
		
		let gameQuestsManager=this.parent.getGameQuestsManager();
		
		let durationMillis;
		// If timeoutMillisBeforeFirstChoiceByDefault is null, then we wait indefinitely for the user to click :
		let durationMillisDialogLine=dialogLine.timeoutMillisBeforeFirstChoiceByDefault;
		
		if(durationMillisDialogLine)	durationMillis=durationMillisDialogLine;
		else													durationMillis=Math.max(MIN_DIALOG_DURATION_MILLIS, self.timeByCharacterMillis*messageText.length);
		
		
		// UNUSEFUL :
		// Since there will always be a «linger» time if you set a speedFactor>1 !
		//
//		let gameConfig=this.gameConfig;
//		let minimumTimeMillis=gameConfig.dialogs?nonull(gameConfig.dialogs.minimumTimeMillis,1000):1000;
//		if(gameConfig.dialogs){
//			if(gameConfig.dialogs.autoScroll==="letters"){
//				durationMillis=Math.max(minimumTimeMillis, (durationMillis+minimumTimeMillis));
//			}
//		}
		

		
		
		loadImageInUniverse(
			self.gameConfig,
			imageOnlySrc,
			null,
			PORTRAITS_DIRECTORY,
			function(portraitImage){

				
				if(isFirstLine){
						self.doOnDialogFirstLine();
				}
				
				
				let dialogLineDelayMillis=previousDurationCumulatedMillis;
				self.timeouts.push(setTimeout(function(){

					// DBG
					if(self.cancelled)	return;

					
					if(!empty(dialogLine.answers)){ // If we have choice lines after this dialog :

						
						
						// CAUTION, if the dialog has possible answers, then it cannot have a single child dialog :
//					self.portraitDialogLines.push(
						window.eranaScreen.sceneManager.uiManager.displayDialogMessage(
								// CAUTION : SPECIAL CASE : DIALOGS ARE ALWAYS PLAYED AT 1x TIME FACTOR BUT TRIGGERED AT WHATEVER TIME FACTOR !
								portraitImage,{choiceLines:dialogLine.answers,durationMillis:durationMillis/* SO NO :*window.eranaScreen.getDurationTimeFactor()"*/},messageText,self.offsetY,side,self.parent,
								// CAUTION : Dialog's child dialog is mandatorily null if this dialog has choice lines (this case) !
								null,
								function(choiceLine){
									
									// // DBG
									//lognow("LINE CLICKED !",choiceLine);
									
									if(gameQuestsManager){
										
										// TODO : FIXME : DEVELOP : For now, children quests are only available for choice lines !
										// It should be available also for single non-choice dialog ending.
										foreach(choiceLine.childrenQuests,(childQuest)=>{
											
											// // DBG
											//console.log("START childQuest:",childQuest);
											
											// IMPORTANT : On today, quests are only started from dialogs !!
											gameQuestsManager.startQuestByPrototypeName(childQuest);
											
											
										});
										
										// TODO : FIXME : KINDA DUPLICATED CODE
										if(choiceLine.childrenActions){
											foreach(choiceLine.childrenActions,(childActionArgs,methodNameToCallKey)=>{
												let method=gameQuestsManager[methodNameToCallKey];
												if(method)	method(gameQuestsManager,childActionArgs);
											});
										}
										
									}
									
									if(isLastLine){
											self.doOnDialogLastLine();
									}
									
								});
//							);
						
					}else{ // Case non-interactive dialog line :
						
						let childDialog;
						if(isLastLine){
							// The childDialog in the line overrides the childDialog in the dialog :
							childDialog=nonull(dialogLine.childDialog, self.childDialog);
							
							self.timeouts.push(setTimeout(function(){
								
								// DBG
								if(self.cancelled)	return;
								
								self.doOnDialogLastLine();
								// CAUTION : SPECIAL CASE : DIALOGS ARE ALWAYS PLAYED AT 1x TIME FACTOR BUT TRIGGERED AT WHATEVER TIME FACTOR !
							},durationMillis/* SO NO : *window.eranaScreen.getDurationTimeFactor()*/)
							);

						}
						
						
						// CAUTION, if the dialog has no possible answers, then it has a single child dialog :
//					self.portraitDialogLines.push(
						window.eranaScreen.sceneManager.uiManager.displayDialogMessage(
								// CAUTION : SPECIAL CASE : DIALOGS ARE ALWAYS PLAYED AT 1x TIME FACTOR BUT TRIGGERED AT WHATEVER TIME FACTOR !
								portraitImage,{durationMillis:durationMillis/* SO NO : *window.eranaScreen.getDurationTimeFactor()*/},messageText,self.offsetY,side,self.parent,
								childDialog
								// If we are in this case, then the dialog has no choice lines !:
								,null
						);
//					);
						
						// TODO : FIXME : On today, simple chaining dialogs have not the possibility to launch a mandatory quest...
						// that could be a problem for mandatory ones that could be stalled for ever if the player never choices a choice line ! 
						
					}
				
					// CAUTION : SPECIAL CASE : DIALOGS ARE ALWAYS PLAYED AT 1x TIME FACTOR BUT TRIGGERED AT WHATEVER TIME FACTOR !
				},dialogLineDelayMillis/* SO NO, BECAUSE DIALOGS DURATIONS MUST NOT BE AFFECTED BY THE TIME FACTOR :*window.eranaScreen.getDurationTimeFactor()*/)
				);

			}
		);
		
		// CAUTION : SPECIAL CASE : DIALOGS ARE ALWAYS PLAYED AT 1x TIME FACTOR BUT TRIGGERED AT WHATEVER TIME FACTOR !
		return durationMillis/* SO NO : *window.eranaScreen.getDurationTimeFactor();*/
	}
	
	
	/*OVERRIDES*/
	/*public*/startStartable(concernedObject=null) {
		super.startStartable(concernedObject);
		
		this.cancelled=false;

	}


	/*OVERRIDES*/
	/*public*/stopStartable(){
		
		super.stopStartable();
		
		foreach(this.timeouts,(t)=>{
			clearTimeout(t);
		});

		
		this.cancelled=true;
		
	}
	
}
