
const CINEMATIC_FONT="bold 18px Monospace";

const CINEMATIC_UI_X=6;
const CINEMATIC_UI_Y=15;


class GameCinematic{

	constructor() {
		
		this.isVisible=true;
		this.isMovable=true;

		this.position=new Position(this,0,0,0,{x:"center",y:"bottom"});
		
		this.mover=getMoverMonoThreadedNonStop(this);
		
		this.speed=0;
		
	}
	
	// INIT
	init(gameConfig){
	
		this.gameConfig=gameConfig;
		
		this.getMover().setDurationTimeFactorHolder(window.eranaScreen);
	
		
		
	}
	
	// METHODS
	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	
	getMover(){
		return this.mover;
	}
	
	getIsMovable(){
		return this.isMovable;
	}
	
	setIsMovable(isMovable){
		this.isMovable=isMovable;
	}
	
	/*private*/startMovingNonStop(){
		
		this.speed=this.nonStopSpeed;
		// Speed is inverted, because the cinematic «band» is going backwards while the camera is still ! :
		this.getMover().doStartNonStop({x:-this.speed});
		
	}
	
	/*public*/start(camera){
		
		// CAUTION : We cannot put these methods in the inits() methods, because the GameScreen2D clears all the clickables in its initOnStartScreenAtLevelStart() method :

		let self=this;
		// Cinematic control buttons :
		let buttonPlay={
			visible:true,
			position:null,
			getPosition:function(){	return buttonPlay.position; },
			size:{w:16,h:16},
			doOnClick:function(){ 
				let newSpeed=self.nonStopSpeed; // Speed is not inverted here, because the original direction factor has already been calculated by the mover as -1 !
				self.getMover().setAbsoluteSpeedX(newSpeed);
			},
			isClickable:()=>{	return true;	},
			isFixed:true,
			setIsVisible:function(visible){		buttonPlay.visible=visible;	}, 
			getIsVisible:function(){	return buttonPlay.visible;		},
			glyph:PLAY_GLYPH,
		};
		this.buttonPlay=window.eranaScreen.controlsManager.registerClickable(buttonPlay);
		this.buttonPlay.position=new Position(this.buttonPlay,CINEMATIC_UI_X-6,CINEMATIC_UI_Y+15,0,{x:"left",y:"bottom"});
		
		
		let buttonPause={
				visible:true,
				position:null,
				getPosition:function(){	return buttonPause.position; },
				size:{w:16,h:16},
				doOnClick:function(){
					let newSpeed=0;
					self.getMover().setAbsoluteSpeedX(newSpeed);
				},
				isClickable:()=>{	return true;	},
				isFixed:true,
				setIsVisible:function(visible){		buttonPause.visible=visible;	},
				getIsVisible:function(){	return buttonPause.visible;		},
				glyph:PAUSE_GLYPH,
		};
		this.buttonPause=window.eranaScreen.controlsManager.registerClickable(buttonPause);
		this.buttonPause.position=new Position(this.buttonPause,CINEMATIC_UI_X+10,CINEMATIC_UI_Y+15,0,{x:"left",y:"bottom"});
			

		// We DONT' move the camera too, or else we only see the same portion of the game cinematic !
		this.startMovingNonStop();
		
		this.buttonPlay.setIsVisible(true);
		this.buttonPause.setIsVisible(true);
	}

	/*public*/stop(){
		this.buttonPlay.setIsVisible(false);
		this.buttonPause.setIsVisible(false);
	}
	
	
	
	/*public*/move(){
		// Mover stepping :
		// Only for monothreaded movers :
		this.getMover().doStep();
		// MOVING
	}
	
	
	getDrawables(){
		return [this.gameFlyers];
	}
	
	
	// -----------------------------------------------------------
//	draw(ctx, camera, lightsManager){
//		/*DO NOTHING*/
//	}
	
	drawInUI(ctx, camera){
		
		if(!this.buttonPlay || !this.buttonPause)	return;


		let buttonPlayPosition=this.buttonPlay.getPosition();
		let buttonPausePosition=this.buttonPause.getPosition();
		
		if(!buttonPlayPosition || !buttonPausePosition)	return;
		
		ctx.save();
		ctx.font = CINEMATIC_FONT;
		ctx.lineWidth = 4;
		ctx.strokeStyle = "#000000";
		ctx.fillStyle = "#AAAAAA";
		
		ctx.strokeText(this.buttonPlay.glyph, buttonPlayPosition.x, buttonPlayPosition.y);
		ctx.fillText(this.buttonPlay.glyph, buttonPlayPosition.x, buttonPlayPosition.y);
		
		
		ctx.strokeText(this.buttonPause.glyph, buttonPausePosition.x, buttonPausePosition.y);
		ctx.fillText(this.buttonPause.glyph, buttonPausePosition.x, buttonPausePosition.y);

		
		ctx.restore();
		
		
	}
	

	
	
}
