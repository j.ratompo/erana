class GameQuest{

	
	constructor() {

//		this.isActive=false;
		
		// Caution : all the sub-scripts must not be started at startup with all the other startables !
		this.scriptTrigger=new GameScript(true);
		this.scriptSuccess=new GameScript(true);
		this.scriptFail=new GameScript(true);
		
		
		this.isKnown=false;
		this.outcome=null;

		
	}
	
	// INIT
	init(gameConfig){
		this.gameConfig=gameConfig;
	}
	
	initWithParent(){
		
		let self=this;
		
//	let gameQuestsManager=this.parent;
		let parentForSubScripts=this;
		
		// Trigger script :
		this.scriptTrigger.init(this.gameConfig,function(){

			// When this scripts ends, then we can launch the others :
			let concernedObject=self.concernedObject; // SPECIAL CASE
			self.scriptSuccess.startStartable(concernedObject); // SPECIAL CASE
			self.scriptFail.startStartable(concernedObject); // SPECIAL CASE
			
			self.isKnown=true;
			self.knownTime=getNow();
		
//			// DBG
//			lognow("TRIGGER ENDS 0 ("+self.title+") !:"+self.scriptTrigger.isStarted());
			
		});
		this.scriptTrigger.conditionsTriggers=this.conditionsTriggers;
		this.scriptTrigger.actions=this.actions;
		this.scriptTrigger.parent=parentForSubScripts;
		
		// Success script :
		this.scriptSuccess.init(this.gameConfig,function(){
			let concernedObject=self.concernedObject;

			self.outcome="success";
			self.scriptFail.stopStartable(concernedObject);
			
//			// DBG
//			lognow("TRIGGER ENDS 1 !");
		});
		this.scriptSuccess.conditionsTriggers=this.conditionsSuccess;
		this.scriptSuccess.actions=this.actionsOnSuccess;
		this.scriptSuccess.parent=parentForSubScripts;

		
		// Fail script :
		this.scriptFail.init(this.gameConfig,function(){
			let concernedObject=self.concernedObject;

			self.outcome="fail";
			self.scriptSuccess.stopStartable(concernedObject);
			
//			// DBG
//			lognow("TRIGGER ENDS 2 !");
		});
		this.scriptFail.conditionsTriggers=this.conditionsFail;
		this.scriptFail.actions=this.actionsOnFail;
		this.scriptFail.parent=parentForSubScripts;

		
	
//		window.eranaScreen.registerStartable(this);

		// CAUTION : These dialogs must not be started aat the beginning !
		foreach(this.getDialogs(),(dialog)=>{
			dialog.delayedStartAtBeginning=true;
		});
		
		
	}
	
	
	// METHODS
	
	/*public*/getGameQuestsManager(){
		return this.parent;
	}
	
	/*public*/getDialogs(){
		return this.gameDialogs;
	}

	/*public*/hasOutcome(expectedOutcome){
		return (this.outcome===expectedOutcome);
	}
	
	/*public*/isComplete(){
		return !!this.outcome;
	}
	
	/*public*/startStartable(concernedObject) {
		this.concernedObject=nonull(concernedObject, this.parent);
	
		// (Here, the this.concernedObject, equals to concernedObject, is the GameQuestsManager)
		this.scriptTrigger.startStartable(concernedObject);

//		// DBG
//		lognow("START QUEST:"+this.isStarted());

		
		// We start the quest's dialogs, if needed :
//	let self=this;
		foreach(this.getDialogs(),(dialog)=>{
//		dialog.start(self);
			dialog.startStartable();
		});
		
	}
	
	
	/*public*/stopStartable(){
	
	// (Here, the this.concernedObject, equals to concernedObject, is the GameQuestsManager)
		this.scriptTrigger.stopStartable(this.concernedObject);
		this.scriptSuccess.stopStartable(this.concernedObject);
		this.scriptFail.stopStartable(this.concernedObject);

		
		// We start the quest's dialogs, if needed :
		foreach(this.getDialogs(),(dialog)=>{
			dialog.stopStartable();
		});
		
		
	}
	
	
	
	/*public*/getIsKnown(){
		return this.isKnown;
	}
	
	/*public*/isStarted(){
//		return (this.scriptTrigger.isStarted() || this.scriptSuccess.isStarted() || this.scriptFail.isStarted());
		return this.scriptTrigger.isStarted();
	}
	
	/*public*/doStep(camera){
		
		if(this.scriptTrigger.isStarted())
			this.scriptTrigger.doStep(camera);

		if(this.scriptSuccess.isStarted())
			this.scriptSuccess.doStep(camera);
		
		if(this.scriptFail.isStarted())
			this.scriptFail.doStep(camera);
		
		
		// TODO : FIXME : CENTRALIZE STEPPING ! (in gameScreen in the main loop)
		// Dialogs stepping :
		let self=this;
		foreach(this.getDialogs(),(dialog)=>{
			dialog.doStep(camera);
		});
		
	}
	

	/*public*/getReachAmount(){
		if(this.scriptSuccess.getReachAmount()!=null)	return this.scriptSuccess.getReachAmount();
		if(this.scriptFail.getReachAmount()!=null)	return this.scriptFail.getReachAmount();
		return null;
	}
	
	/*public*/getReachTarget(){
		if(this.scriptSuccess.getReachTarget()!=null)	return this.scriptSuccess.getReachTarget();
		if(this.scriptFail.getReachTarget()!=null)	return this.scriptFail.getReachTarget();
		return null;
	}
	


	/*public*/getTitle(){
		return nonull(this.title,"");
	}
	
	/*public*/getDirective(){
		return nonull(this.directive,"");
	}
	
}
