

const DEFAULT_STARING_POINT=new THREE.Vector3(0,0,-10); // CAUTION : THIS POINT IS USED TO CALIBRATE ALL ! 

//(OLD :) const VERTIVAL_FOV_3D=40; // (vision angle in degrees)
const VERTIVAL_FOV_3D=15; // (in degrees)
const NEAR_3D=200;
const FAR_3D=10000;

class GameCamera3D{

	constructor(canvasWidth,canvasHeight){
	
		this.canvasWidth=canvasWidth;
		this.canvasHeight=canvasHeight;
		
		this.camera3D=null;
		
	
	}
	
	
	init(gameConfig){
		
		this.gameConfig=gameConfig;
	
		const projection=gameConfig.projection;
		const is3D=contains(projection,"3D");
		if(!is3D){
			// TRACE
			lognow("WARN : We initialize a GameCamera3D but we don't have a 3D context.");
			
		}
		
		
		const self=this;
		this.position=new Position(this,0,0,0,null,function(rootContainer){
			self.onCameraMove(rootContainer);
		},null,0,0,0);


		this.camera3D=this.createCamera();

		
		return this;
	}


	/*private*/createCamera(){
		
		let pixelRatio=1;
		if(this.canvasWidth && this.canvasHeight)	pixelRatio=(this.canvasWidth/this.canvasHeight);
		
		
		const camera3D=new THREE.PerspectiveCamera(VERTIVAL_FOV_3D, pixelRatio);
		camera3D.position.set(this.position.x,this.position.y,this.position.z);
		camera3D.rotation.set(this.position.g,this.position.a,this.position.b);
		
		// VERY IMPORTANT, OR ELSE THE HORIZON WILL BE TILTED WHEN LOOKING AT 90 AND 270 DEGREES !
		camera3D.rotation.order="YXZ";


		// UNUSED :
//	this.screenDistance=this.calculateDistance();
		

		return camera3D;
	}
	
	
	// UNUSED :
//	calculateDistance(){
//		const angleRadians=Math.toRadians(VERTIVAL_FOV_3D);
//		return ((this.canvasHeight*.5)/Math.tan(angleRadians*.5));	
//	}

	// UNUSED :
//	getVisorPoint(xOffset=0,yOffset=0){
//		const gamma=this.position.g;
//		const alpha=Math.coerceAngle(this.position.a+Math.PSI,true);
//		const result=Math.polarPositionRadiansToCartesianPosition3D(this.screenDistance,gamma,alpha,xOffset,yOffset);
//		// DBG
//		result.z=-result.z;
//		return result;
//	}

	onCameraMove(rootContainer){
	
	}


	/*public*/setRotation(x,y,z){
		this.camera3D.rotation.set(x,y,z);
		this.camera3D.updateProjectionMatrix();
		this.position.setRotation(x,y,z);
	}

//	getVRControls(){
//		return new THREE.VRControls(this.camera3D);
//	}


// DOES NOT WORK :
//	setFixedObject(object){
//		if(object.getPositionnedObject3D()){
//			const object3D=object.getPositionnedObject3D();
//			this.camera3D.add(object3D);
//		}
//		if(object.element3D){
//			const element3D=object.element3D;
//			this.camera3D.add(element3D);
//		}
//	}
	

}

