
const SCENE_3D_COLOR=new THREE.Color(0x888888);

class GameSceneManager3D{

	constructor(view, controller){
	
		this.view=view;
		this.controller=controller;
			
		this.camera=null;
		this.cameraAnchored=null;
		
		this.renderers3D={};
		this.scenes3D={};
		
		this.controls={};
		
//		this.pointer=null;
	
	}
	
	init(gameConfig){
		this.gameConfig=gameConfig;
	
		return this;
	}

	initOnStartScreenAtLevelStart(){
		
		if(!this.controller){
			// !!! CAUTION : THIS NAME IS A CONVENTION TO BE RESPECTED IN CLIENT CODE !!!
			this.controller=window.eranaScreen;
		}
		this.mainElement=this.controller.mainElement;
		
		this.canvas=this.createCanvasForWEBGLIfNecessary();
		
		this.renderers3D=this.createRenderers();
		
		this.scenes3D=this.createScenes();
		
		this.camera=this.createCamera();
		
		// DOES NOT WORK :
		this.setupAdditionalControls();
		
		
		return this;
	}
	
	

	
	setupAdditionalControls(){
		
		// DOES NOT WORK :
		if(this.gameConfig.configXR && this.gameConfig.configXR.controls3D=="orbital")
			this.setOrbitalControlsActivation(true);
			
		
//		this.controls["vr"]=this.camera.getVRControls();
		
			
	}
	
	/*private*/createCamera(){
		
		let camera;
		
		const isStereo=this.isStereo();
		const canvasWidth=getWindowSize("width")*(!isStereo?1:.5);
		const canvasHeight=getWindowSize("height");
		
		if(isStereo){
			camera=new GameCamera3DStereo(canvasWidth,canvasHeight).init(this.gameConfig);
		}else{ // NORMAL CASE :
			camera=new GameCamera3D(canvasWidth,canvasHeight).init(this.gameConfig);
		}
		
		const isAnchored=(this.gameConfig.configXR && this.gameConfig.configXR.hasAnchors);
		if(isAnchored){
			if(isStereo){
				this.cameraAnchored=new GameCamera3DStereo(canvasWidth,canvasHeight).init(this.gameConfig);
			}else{ // NORMAL CASE :
				this.cameraAnchored=new GameCamera3D(canvasWidth,canvasHeight).init(this.gameConfig);
			}	
		}

		return camera;
	}
	
	

	/*private*/createCanvasForWEBGLIfNecessary(){
		
		if(!contains(this.gameConfig.projection,"webgl")){
			// TRACE
			lognow("WARN : No need to create a canvas in an non-WEBGL context. Aborting.");
			return;
		}

		
		const mainElement=this.mainElement;
		let canvas=null;
		if(this.isStereo()){
			// SPECIAL CASE : if we want a css3 renderer AND a stereo vision :  
			// Then we create a special hacked stereo setup :

			canvas={};
			canvas["left"]=getSingleCanvas("WEBGLLayer",mainElement,"left");
			canvas["right"]=getSingleCanvas("WEBGLLayer",mainElement,"right");

		}else{ // NORMAL CASE :
			canvas=getSingleCanvas("WEBGLLayer",mainElement);
		}
		
	

		return canvas;
	}
	
	

	/*private*/createRenderers(){
		
		
		let renderers3D={};
		
		if(this.isStereo()){
			// SPECIAL CASE : if we want a css3 renderer AND a stereo vision :  
			// Then we create a special hacked stereo setup :
			
			renderers3D["left"]=this.getSingleRenderer("left");
			renderers3D["right"]=this.getSingleRenderer("right");
			
		}else{
			
			renderers3D=this.getSingleRenderer();
			
		}
		
		
		return renderers3D;
	}
	
	
	
	
	/*private*/getSingleRenderer(side=null){
		
		const renderers3DForOneSide={};
		
		
		const gameConfig=this.gameConfig;

		const canvas=(!side?this.canvas:this.canvas[side]);
		const canvasWidth=getWindowSize("width")*(!side?1:.5);
		const canvasHeight=getWindowSize("height");
		
		if(contains(gameConfig.projection,"webgl")){
			
			let conf={canvas:canvas};
			if(gameConfig.configXR && gameConfig.configXR.isAR){
				// AR requires a transparent scene background :
				conf.background="transparent";
			}
			const renderer3D=getThreeRenderer(conf);
			
			if(renderer3D){
			
				// EXTREMELY IMPORTANT, OR ELSE THE WEBGLRENDERER JUST WON'T WORK !!
				renderer3D.setViewport(0, 0, canvasWidth, canvasHeight);
				// EXTREMELY IMPORTANT, OR ELSE THE WEBGLRENDERER JUST WON'T WORK !!
				renderer3D.setSize(canvasWidth,canvasHeight);
				
				renderers3DForOneSide["webgl"]=renderer3D;
			}

		}
		
		if(this.isCSS3()){

			
			const renderer3D=getThreeRenderer({type:"css"});


			// EXTREMELY IMPORTANT, OR ELSE THE CSS3DRENDERER JUST WON'T WORK !!
			renderer3D.setSize(canvasWidth, canvasHeight);


			renderer3D.domElement.id="rendererCSS3"+(!side?"":("_"+side));
			
			renderer3D.domElement.style.position="fixed";
			
			renderer3D.domElement.style.width=canvasWidth+"px";
			renderer3D.domElement.style.height=canvasHeight+"px";

			if(side==="right")	renderer3D.domElement.style.right="0";

			
			const style=renderer3D.domElement.style;
			style.border="solid 2px #888888";
			style.display="flex";
			
			appendAsFirstChildOf(renderer3D.domElement,this.mainElement);
			
			renderers3DForOneSide["css3"]=renderer3D;
			
		}
	
		
		return renderers3DForOneSide;
		
	}


	
	/*private*/createScenes(){
		
		
		const gameConfig=this.gameConfig;
		
		const scenes3D={};
		
		if(contains(gameConfig.projection,"webgl")){
			
			// We create the 3D scene :
			scenes3D["webgl"]=new THREE.Scene();
			if(!gameConfig.configXR || !gameConfig.configXR.isAR){
				scenes3D["webgl"].background=SCENE_3D_COLOR;
			}
			
		}
		
		if(contains(gameConfig.projection,"css3")){
			// We create the 3D scene :
			scenes3D["css3"]=new THREE.Scene();
			scenes3D["css3"].background=SCENE_3D_COLOR;
		}
		
		
		const isAnchored=(this.gameConfig.configXR && this.gameConfig.configXR.hasAnchors);
		if(isAnchored){
			scenes3D["webglAnchored"]=new THREE.Scene();
			if(!gameConfig.configXR || !gameConfig.configXR.isAR){
				scenes3D["webglAnchored"].background=SCENE_3D_COLOR;
			}
		}
	
		
		return scenes3D;
	}
	
	// DOES NOT WORK :
//	setFixedObject(object){
//		this.camera.setFixedObject(object);
//	}



	/*public*/isStereo(){
		const gameConfig=this.gameConfig;
		return gameConfig.configXR && gameConfig.configXR.isStereo;		
	}	

	/*public*/isCSS3(){
		const gameConfig=this.gameConfig;
		return gameConfig.projection && contains(gameConfig.projection,"css3");		
	}
	
	/*public*/isCSS3Stereo(){
		return this.isCSS3() && this.isStereo();		
	}
	
	
	// DOES NOT WORK :
	/*public*/setOrbitalControlsActivation(activation){
		
		// Only available in webgl mode :
		if(!this.renderers3D["webgl"])	return;
		
		// CAUTION : Only available in non-stereo mode :
		const isStereo=this.isStereo();
		if(isStereo){
			// TRACE
			lognow("ERROR : Orbital controls are not allowed in stereo mode. Deactivating.");
			return;
		}
		
		if(activation){
			if(!this.gameConfig.configXR || this.gameConfig.configXR.controls3D!="orbital")		return;
			this.controls["orbital"]=new OrbitControls(this.camera.camera3D, this.renderers3D["webgl"].domElement);
		}else{
			this.controls["orbital"]=null;
		}
	}
	
	
	/*public*/update3DControls(){
		
		// DOES NOT WORK :
		// Only available in webgl mode :
		if(!this.renderers3D["webgl"])	return;
		if(!this.controls["orbital"])	return;
		this.controls["orbital"].update();
		
	}
	
	
	// *********************************
	

	/*public*/placeAllOnInit(rootContainer){
		
		
		const gameConfig=this.gameConfig;
		
		
		const width=this.canvasWidth;
		const height=this.canvasHeight;
		const depth=this.canvasWidth; // (we take the width as depth)

		
		if(rootContainer.cardinalities){
			
			foreach(rootContainer.cardinalities,(cardinality,key)=>{
				
				// 1- We place for the first time the previously instantiated objects :
				if(cardinality.value==="fill"){

					const spawningZone=nonull(getSpawningZone(cardinality),{x:0,y:0,z:0,w:width,h:height,d:depth});
					if(contains(gameConfig.projection, "3D")) {
						// TODO : ...
					}
				
				}else{
					// Groups, ratio or interval collections cardinalities :

					
					// (cardinality 5/10 = An object has 5 chances out of 10 scroll events to appear)
					// (cardinality 5->10 = Objects total number on display area will always be between 5 min and 10 max on scroll events)

					let arr;

					// We wrap in an array if we have a single attribute, and not a collection :
					if(!isArray(rootContainer[key]))	arr=[rootContainer[key]];
					else 															arr=rootContainer[key];
					
					
					let alreadyPositionnedItems=[];
					let itemsToRepositionBecauseOfOverlap=[];
					
					let spawningZone=null;
					foreach(arr, (item,itemIndex)=>{
						
						if(!item.prototypeName)		return "continue";
						
						if(cardinality.value==="staticGroups" || cardinality.value==="oneGroupAtRandom"
							 || isNumber(cardinality.value)){ // Static or Random groups Collection cardinality :
							
							let groupsConfigs=
								(cardinality.prototypesInstantiation.staticInstantiations?
										cardinality.prototypesInstantiation.staticInstantiations:cardinality.prototypesInstantiation.randomInstantiations);
		
							// If we find no zone, then we use the previous one of the collection :
							let spawningZoneLocal=foreach(groupsConfigs,(conf,index)=>{
								if(index===itemIndex) 
									return getSpawningZone(conf);
							});
							if(spawningZoneLocal)	spawningZone=spawningZoneLocal;
							
						}
						
						if(spawningZone){
							if(contains(gameConfig.projection, "3D")) {
								
										let pos=getRandomPositionInZone(spawningZone, 0, 0, 0, cardinality.avoidOverlap, item.size);
										manageOverlappingItems(alreadyPositionnedItems,gameConfig,cardinality,item,pos);
										this.setPositionFirstTime(item, pos, rootContainer);
								
							}
						}
					
					});
					
				}

			});
			
			
		}else{ // Case direct classes objects or arrays definitions :
			
		
			let item=rootContainer;
			let overridingPos=item.overridingPosition;
			if(overridingPos){
				// It is impossible (and makes no sense if it happens)
				// that we have an attribute overridingPosition for direct definition declared arrays !
				this.setPositionFirstTime(item, overridingPos);
			}
			
			
		}
		
		
		
		// !!! CAUTION : EVEN TO JUST PLACE ELEMENTS, THEY HAVE TO APPEAR IN 
		// THE .getDrawables() OR getPlaceables() FUNCTION !!!
		// We place all the children containers :
		if(rootContainer.getPlaceables || rootContainer.getDrawables){
			let placeables=(rootContainer.getPlaceables?rootContainer.getPlaceables():rootContainer.getDrawables());
			foreach(placeables,(child)=>{
				if(!isArray(child)) {
					this.placeAllOnInit(child);
				}else{
					foreach(child,(c)=>{
						this.placeAllOnInit(c);
					},(c)=>{return !!c; /*(forced to boolean)*/
					}); // Case c represents an empty collection.
				}
			}
			// Case c represents an empty collection.
			,(c)=>{ return !!c; /*(forced to boolean)*/
				}
			);
			
		}
		
		// Followers management :
		let followers=[];
		if(this.gameConfig.cameraFixed==="selection")	followers.push(this.camera);
		
		
		return followers;
	}
	
	
	
	/*public*/setPositionFirstTime(item, pos, parentItem=null){
		
	
		// CAUTION : ONLY ITEMS WITH A SPAWNING POSITION IN PARENT PROTOTYPE CONFIG AND IN A getDrawables() RESULT ARE SET A POSITION FOR FIRST TIME WITH THIS METHOD !
		

		// FOr 3D models, the only possibility for the center is «centered» 		
		pos.center={x:"center",y:"center",z:"center"};
		
		
		// If this item has no position, then we create one for him :
		if(!item.position || item.overridingPosition) {
			if(item.overridingPosition)		item.position=new Position(item, item.overridingPosition.x, item.overridingPosition.y, item.overridingPosition.z, item.overridingPosition.center, null, null,
																			 														item.overridingPosition.b, item.overridingPosition.g, item.overridingPosition.a);
			else													item.position=new Position(item, pos.x, pos.y, pos.z, pos.center, null, null, pos.b, pos.g, pos.a);
		}else{
			item.position.setLocation(pos);
		}
		
		
		// If we have a parent, then we add its coordinates to the current child :
		// (CAUTION : Only in no-«fill» cardinalities ! -on today-)
		if(parentItem && parentItem.position){
			item.position.setParentPosition(parentItem.position);
		}

		
		if(this.lightsManager)
			this.lightsManager.registerLights(item);
		
		
		// TODO : Develop... :
//		let itemIsClickable= !!(item.doOnClick && item.isClickable && item.isClickable()); /*(forced to boolean)*/
//		if(			this.view.controlsManager 
//				&& !this.hasDirectAncestorUnclickable(parentItem)
//				&&	itemIsClickable
//			){
//			this.view.controlsManager.registerClickable(item);
//		}
		
		
		// Useful for ongoing instanciated objects ! Or objects instnaciated way after the level is loaded !
		if(item.initAfterSetPositionFirstTime)	item.initAfterSetPositionFirstTime();	
		
	}

	
//	/*public*/setupPointer(canvas){
//		
//		this.pointer=getCanvasPointer(canvas);
//	}
	
	

}