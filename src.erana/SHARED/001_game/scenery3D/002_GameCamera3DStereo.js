

const EYES_GAP=1;

class GameCamera3DStereo extends GameCamera3D{

	constructor(canvasWidth,canvasHeight){
		super(canvasWidth,canvasHeight);
	
		this.camera3Ds={};
		
	}
	
	
	/*OVERRIDES*/
	init(gameConfig){
		
		this.gameConfig=gameConfig;
	
		const projection=gameConfig.projection;
		const is3D=contains(projection,"3D");
		if(!is3D){
			// TRACE
			lognow("WARN : We initialize a GameCamera3D but we don't have a 3D context.");
		}
		
		const self=this;
		this.position=new Position(this,0,0,0,null,function(rootContainer){
			self.onCameraMove(rootContainer);
		},null,0,0,0);


		this.camera3Ds=this.createCameras();

		
		return this;
	}



	/*private*/createCameras(){
		
		const EYES_DEMI_GAP=EYES_GAP*.5;
		
		
		let pixelRatio=1;
		if(this.canvasWidth && this.canvasHeight)	pixelRatio=((this.canvasWidth*.5)/this.canvasHeight);

		const camera3Ds={};

		camera3Ds["left"]=new THREE.PerspectiveCamera(VERTIVAL_FOV_3D, pixelRatio);
		camera3Ds["left"].position.set(this.position.x-EYES_DEMI_GAP,this.position.y,this.position.z);
		camera3Ds["left"].rotation.set(this.position.g,this.position.a,this.position.b);
		// VERY IMPORTANT, OR ELSE THE HORIZON WILL BE TILTED WHEN LOOKING AT 90 AND 270 DEGREES !
		camera3Ds["left"].rotation.order="YXZ";

		camera3Ds["right"]=new THREE.PerspectiveCamera(VERTIVAL_FOV_3D, pixelRatio);
		camera3Ds["right"].position.set(this.position.x+EYES_DEMI_GAP,this.position.y,this.position.z);
		camera3Ds["right"].rotation.set(this.position.g,this.position.a,this.position.b);
		// VERY IMPORTANT, OR ELSE THE HORIZON WILL BE TILTED WHEN LOOKING AT 90 AND 270 DEGREES !
		camera3Ds["right"].rotation.order="YXZ";

	
	// UNUSED :
//		super.screenDistance=super.calculateDistance();
	
	
		return camera3Ds;
	}

	onCameraMove(rootContainer){
	
	}


	/*public*/setRotation(x,y,z){
		this.camera3Ds["left"].rotation.set(x,y,z);
		this.camera3Ds["left"].updateProjectionMatrix();
		
		this.camera3Ds["right"].rotation.set(x,y,z);
		this.camera3Ds["right"].updateProjectionMatrix();
		
		this.position.setRotation(x,y,z);
	}


//	getVRControls(){
//		return {
//			"left": new THREE.VRControls(this.camera3Ds["left"]),
//			"right": new THREE.VRControls(this.camera3Ds["right"])
//		};
//	}

// DOES NOT WORK :
//	setFixedObject(object){
//		if(object.getPositionnedObject3D()){
//			const object3D=object.getPositionnedObject3D();
//			this.camera3Ds["left"].add(object3D);
//			this.camera3Ds["right"].add(object3D);
//		}
//		if(object.element3D){
//			const element3D=object.element3D;
//			this.camera3Ds["left"].add(element3D);
//			this.camera3Ds["right"].add(element3D);
//		}
//	}

}

