

// USE https://webxr.io/marker-generator/



//const AR_ANCHOR_APPARENT_SIZE_IN_METERS=500;
//const AR_ANCHOR_APPARENT_SIZE_IN_METERS=100;
//const AR_ANCHOR_APPARENT_SIZE_IN_METERS=.5; // TODO : FIXME : MAGIC NUMBER !
const AR_ANCHOR_APPARENT_SIZE_IN_METERS=1; // TODO : FIXME : MAGIC NUMBER !

// DOES NOT WORK :
//const IS_MODE_GROUPS_AUTO=false;


// This can anchor any object 3D to a single AR marker

class GameARSingleMarkerAnchor{
	
	constructor(){
		
		this.linkedObjects=[];
		this.arControls={};
		this.arGroups={};
	}
	
	init(gameConfig){
		this.gameConfig=gameConfig;
		
		
		// 2)
		const self=this;
		window.eranaScreen.mediaHandlerIsReadyListeners.push((arToolkitContext, scene3D, sceneCSS3)=>{
			
			self.createGroupsForLinkedObjects(arToolkitContext, scene3D, sceneCSS3);
			self.addMarkerListener(arToolkitContext);
			
			// DOES NOT WORK :
//			foreach(self.linkedObjects,(linkedObject)=>{
//				window.eranaScreen.setFixedObject(linkedObject);
//			});

			foreach(self.linkedObjects,(linkedObject)=>{
				window.eranaScreen.addWebcamUsingObject(linkedObject);
			},(linkedObject)=>linkedObject.isFlat);		
				
		});
			
	
	}
	
	
	/*private*/createGroupsForLinkedObjects(arToolkitContext, scene3D, sceneCSS3){
		
		const patternFilePath=getConventionalFilePath(this.gameConfig.basePath, this, "universe")+this.patternFile;

		
		if(!arToolkitContext){
			// TRACE.
			lognow("ERROR : No arToolkitContext found on this screen. Aborting linking object to AR anchor.");
			return;
		}



		if(scene3D){
			
			this.arGroups["webgl"]=new THREE.Group();
			scene3D.add(this.arGroups["webgl"]);
			this.arControls["webgl"]=new THREEx.ArMarkerControls(arToolkitContext, this.arGroups["webgl"],{
				type: "pattern",
				patternUrl: patternFilePath,
				size:AR_ANCHOR_APPARENT_SIZE_IN_METERS,
			});
		}

		if(sceneCSS3){
			
			this.arGroups["css3"]=new THREE.Group();
			sceneCSS3.add(this.arGroups["css3"]);
			this.arControls["css3"]=new THREEx.ArMarkerControls(arToolkitContext, this.arGroups["css3"],{
				type: "pattern",
				patternUrl: patternFilePath,
				size:AR_ANCHOR_APPARENT_SIZE_IN_METERS,
			});
			
		}
			
		const self=this;


		// DOESN'T WORK : (BUT KEEP CODE !)
//		if(IS_MODE_GROUPS_AUTO){
//			foreach(this.linkedObjects,(linkedObject)=>{
//				if(!linkedObject.isFlat){
//				
//					if(scene3D){
//						scene3D.remove(linkedObject.getPositionnedObject3D());
//						if(linkedObject.getPositionnedObject3D())	self.arGroups["webgl"].add(linkedObject.getPositionnedObject3D());
//					}
//					// DBG
//					lognow("scene3D :",scene3D);
//					
//					if(sceneCSS3){
//						sceneCSS3.remove(linkedObject.element3D);
//						if(linkedObject.element3D)	self.arGroups["css3"].add(linkedObject.element3D);
//					}
//					// DBG
//					lognow("sceneCSS3 :",sceneCSS3);
//				}
//			});
//		}
		
		
		
		
		
	}
	
	/*private*/addMarkerListener(arToolkitContext){
		const self=this;
		const arController=arToolkitContext.arController;

		const group=nonull(self.arGroups["webgl"],self.arGroups["css3"]);
		
		
		// DBG
		arController.addEventListener("getMarker", (event) => {
//			// DBG
//			window.addEventListener("markerFound", (event) => {
				
				const markerData=event.data;
				
				if(markerData.marker.idPatt===-1){
					// If no pattern has been recognized :
					foreach(self.linkedObjects,(linkedObject)=>{
						if(linkedObject.doOnAnchorLost)		linkedObject.doOnAnchorLost();
					});
					
				}else if(group){
						
					const markerPosition=markerData.marker.pos;

					foreach(self.linkedObjects,(linkedObject)=>{
//						if(!IS_MODE_GROUPS_AUTO){ // THIS MODE DOESN'T WORK !
							if(!linkedObject.isFlat){
								
								if(linkedObject.getPositionnedObject3D())		self.adjustPosition(linkedObject,linkedObject.getPositionnedObject3D(), group, markerPosition);
								if(linkedObject.element3D)	self.adjustPosition(linkedObject,linkedObject.element3D, group, markerPosition);
							}
//						}
						
						if(linkedObject.doOnAnchorFound)	linkedObject.doOnAnchorFound(markerData.marker.vertex);
						
					});
				}
		
		});
		
		
	}
	
	/*private*/adjustPosition(linkedObject,object,group,markerPosition){
		
		// DBG
		const point=group.position;
		const angles=group.rotation;


// TODO : FIXME :
//// DBG
//		object.rotation.set(group.rotation._x,group.rotation._y,group.rotation._z);

		
//	object.rotation.set(refPosition.g+angles._x, 
//												refPosition.a+angles._y,
//												refPosition.b+angles._z);


//	object.rotation.set(Math.coerceAngle(refPosition.g+angles._x,true), 
//												Math.coerceAngle(refPosition.a+angles._y,true),
//												Math.coerceAngle(refPosition.b+angles._z,true));

		// TODO : FIXME :

		const refPosition=linkedObject.position;
		object.position.set(refPosition.x+point.x, refPosition.y+point.y, refPosition.z+point.z);

	//object.position.set(point.x, point.y, point.z);
	

	//					// DBG
	//					linkedObject.getPositionnedObject3D().position.set(linkedObject.position.x,linkedObject.position.y,linkedObject.position.z*-1);
	
	}
	
	/*public*/addLinkedObject(linkedObject){
		
		// 1)
		this.linkedObjects.push(linkedObject);
		
	}
		
}
