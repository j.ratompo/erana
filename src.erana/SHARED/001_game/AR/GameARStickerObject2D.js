


// TODO : DEVELOP...:



const AR_ANCHOR_APPARENT_FACTOR=1.6; // TODO : FIXME : MAGIC NUMBER !



class GameARStickerObject2D{
	
	constructor(){
		
		this.isFlat=true;
		this.cornersPoints=[];
		
		this.image=null;
	}
	
	init(gameConfig){
		this.gameConfig=gameConfig;
		
		
	}
		
	initAfterLevelLoading(){
		
		this.linkToAnchor();

		const imageSrc=this.modelConfig.src;
		const self=this;	
		loadImageInUniverse(this.gameConfig,imageSrc,this,null,(image)=>{
			self.image=image;
		}); 

	}
	
	
	doOnAnchorFound(vertices){
		
//		// DBG
//		lognow("GameARStickerObject2D:",markerData);
//		lognow("GameARStickerObject2D:markerData.marker.vertex:",markerData.marker.vertex);
		
		this.cornersPoints[0]=[vertices[0][0]*AR_ANCHOR_APPARENT_FACTOR,vertices[0][1]*AR_ANCHOR_APPARENT_FACTOR];
		this.cornersPoints[1]=[vertices[1][0]*AR_ANCHOR_APPARENT_FACTOR,vertices[1][1]*AR_ANCHOR_APPARENT_FACTOR];
		this.cornersPoints[2]=[vertices[2][0]*AR_ANCHOR_APPARENT_FACTOR,vertices[2][1]*AR_ANCHOR_APPARENT_FACTOR];
		this.cornersPoints[3]=[vertices[3][0]*AR_ANCHOR_APPARENT_FACTOR,vertices[3][1]*AR_ANCHOR_APPARENT_FACTOR];
		
		
	}
	
	// TODO : FIXME : DUPLICATED CODE !
	/*private*/linkToAnchor(){
		
		if(!this.anchorId)	return;
		const anchor=this.parent.findAnchorById(this.anchorId);
		if(anchor)	anchor.addLinkedObject(this);
		
	}


	/*public*/getImage(){
		return this.image;
	}


	// ---------------------------------------------

	canDraw(){
		return !empty(this.cornersPoints);
	}

	draw(ctx, webcamImageData){
		
		if(!this.canDraw())	return;
		
		const points=this.cornersPoints;
		
		// DBG
		ctx.beginPath();
		foreach(points,(p)=>{
			ctx.rect(p[0], p[1], 10, 10);
		});
		ctx.closePath();
		ctx.fill();
		
		
		// TODO : DEVELOP...:
//		const image=this.getImage();
//		if(image){
// 			TODO :
// 			DRAW DEFORMED STICKER IMAGE !	
//		}


	}
	

}
