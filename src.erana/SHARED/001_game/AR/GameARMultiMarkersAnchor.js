

// TODO : FIXME : DOES NOT WORK

// USE https://webxr.io/marker-generator/


// This can anchor any object 3D to a 4 AR markers


const MINIMAL_NUMBER_OF_ACTIVE_MARKERS=4;

class GameARMultiMarkersAnchor{
	
	constructor(){
		
		this.linkedObjects=[];
		
		this.arGroups={};
		this.markers={};
	}
	
	init(gameConfig){
		this.gameConfig=gameConfig;
		
		
		// 2)
		const self=this;
		window.eranaScreen.mediaHandlerIsReadyListeners.push((arToolkitContext, scene3D, sceneCSS3/*UNUSED*/)=>{
			
			self.createGroupsForLinkedObjects(arToolkitContext, scene3D);
			
			self.addMarkerListener(arToolkitContext);
			
			foreach(self.linkedObjects,(linkedObject)=>{
				window.eranaScreen.addWebcamUsingObject(linkedObject);
			},(linkedObject)=>linkedObject.isFlat);		
				
		});
			
	
	}
	
	
	/*private*/createGroupsForLinkedObjects(arToolkitContext, scene3D){
		
		
		if(!arToolkitContext){
			// TRACE.
			lognow("ERROR : No arToolkitContext found on this screen. Aborting linking object to AR anchor.");
			return;
		}
		

		// Only available in webgl scene :
		if(scene3D){
			
			const self=this;
			foreach(this.patternFiles,(patternFile)=>{
				
				
				const group=new THREE.Group();
				const groupUUID=getUUID("short");
				self.arGroups[groupUUID]=group;
				
//			DOES NOT WORK : scene3D.add(group);
				
				const patternFilePath=getConventionalFilePath(self.gameConfig.basePath, self, "universe")+patternFile;
				const control=new THREEx.ArMarkerControls(arToolkitContext, group,{
					type: "pattern",
					patternUrl: patternFilePath,
					size:AR_ANCHOR_APPARENT_SIZE_IN_METERS,
				});
				
				
			});
			
		}

			
		
	}
	
	/*private*/addMarkerListener(arToolkitContext){
		
		const self=this;
		const arController=arToolkitContext.arController;



		window.addEventListener("markerFound", (event)=>{
			
			
			const id=event.detail.id+"";
			if(!self.markers[id])	self.markers[id]={};
			const markerInfo=self.markers[id];
			
			markerInfo.isActive=true;
		});
		

		window.addEventListener("markerLost", (event)=>{
			
			
			const id=event.detail.id+"";
			if(!self.markers[id])	self.markers[id]={};
			const markerInfo=self.markers[id];
			
			markerInfo.isActive=false;
			
		});
			




		foreach(this.arGroups, (group)=>{
			
				arController.addEventListener("getMarker", (event) => {
						
						const markerData=event.data;
						if(markerData.marker.idPatt!=-1 && group){

							
							const id=markerData.marker.id+"";
							if(!self.markers[id])	self.markers[id]={};
							const markerInfo=self.markers[id];
							
							
							const markerPosition=markerData.marker.pos;
							markerInfo.position=[markerPosition[0],markerPosition[1]];
							
							// DBG
							markerInfo.id=id;
							
					}
					self.checkIfAllMarkersAreActive(markerData);
				
				});
				
				
		});
			

	}
	
	/*private*/checkIfAllMarkersAreActive(markerData){
		
		const vertices=[];
		let hasOneMarkerInactive=false;
		foreach(this.markers,(marker)=>{
			vertices.push([marker.position[0],marker.position[1]]);
		},(marker)=>marker.isActive);
		
		const allMarkersAreActive=(MINIMAL_NUMBER_OF_ACTIVE_MARKERS<=vertices.length);
		
		const self=this;
		if(!allMarkersAreActive){
			
			// If no pattern has been recognized :
			foreach(self.linkedObjects,(linkedObject)=>{
				if(linkedObject.doOnAnchorLost)		linkedObject.doOnAnchorLost();
			});
			
		}else{
			
			foreach(self.linkedObjects,(linkedObject)=>{
				if(linkedObject.doOnAnchorFound)	linkedObject.doOnAnchorFound(vertices);
			});
			
		}
		
	}


	

	
	/*public*/addLinkedObject(linkedObject){
		// 1)
		this.linkedObjects.push(linkedObject);
	}
		
}
