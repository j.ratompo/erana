/* ## Utility gamu methods - flow part
 *
 * This set of methods gathers utility game-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils-gamu» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 *  
 */


// ======================== Flow utility methods ========================

/*public*//*static*/function executeControllerAction(controller,actionName,argsParam){

		// CAUTION ! ACTION IS CALLED PRIOR TO ANY OTHER TREATMENT !!
		if(!actionName) return;
		
		// We clean eventual method markers :
		actionName=actionName.replace("()","");
		
		let actionMethod = controller[actionName];
		if(!actionMethod)	return;
		
		if(argsParam) {
			actionMethod.apply(controller, [ argsParam ]); // Caution : apply() translates args array !
		} else {
			actionMethod.apply(controller);
		}
		
}

