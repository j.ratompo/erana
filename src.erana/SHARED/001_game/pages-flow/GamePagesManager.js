
const MENUS_PAGES_MODE="wholePage";


class GamePagesManager{
	
	constructor(gameConfig, flowConfig, controlsManager, controller, view, /*OPTIONAL*/mainId){
		
		
		this.pages={};
		this.pagesImages={};
		
		this.currentPage=null;
		this.controlsManager=controlsManager;
		this.view=view;
		this.flowConfig=flowConfig;
		
		this.init(gameConfig);
		
		this.initPages(controller, mainId);
		
		this.initFlow(controller);
	}
	
	/*private*/init(gameConfig){
		this.gameConfig=gameConfig;
	}
	
	
	// =============== PAGES INIT ===============

	
	/*private*/initPages(controller,/*OPTIONAL*/mainId){
		
		const isStereo=this.isStereo();

		let pagesContainerId="pageLayer";
		let pagesContainer=document.getElementById(pagesContainerId);
		if(!pagesContainer) {
			pagesContainer=document.createElement("div");
			pagesContainer.id=pagesContainerId;
			
			// Click limitations : 
			pagesContainer.style["pointer-events"]="none";
			pagesContainer.onclick=(event)=>{
				event.preventDefault();
				event.stopPropagation();
			};
			
	

			if(MENUS_PAGES_MODE==="wholePage"){
				
				// UNUSEFUL
//			pagesContainer.style.width=(isStereo?"50%":"100%");
				pagesContainer.style.width="100%";
				pagesContainer.style.height="100%";
				
			}else{
				
				// NOPE : will cause canvas pixel bugs !
		//	let width=getWindowSize("width") + "px";
		//	let height=getWindowSize("height") + "px";
				
				const zooms=this.gameConfig.zooms;
				const zoomedResolution=getZoomedSize(this.gameConfig.getResolution(), zooms);
				const width= zoomedResolution.w+"px";
				const height=zoomedResolution.h+"px";
				pagesContainer.style.width=width;
				pagesContainer.style.height=height;
				
			}
			
			pagesContainer.style.position="fixed";
			pagesContainer.style["z-index"]="10";
	
			const mainElement=getMainElement(mainId);
			mainElement.appendChild(pagesContainer);
			
			

		}
		
		
			
		const self=this;
		let firstPage=null;
		foreach(this.flowConfig,(flowConfigItem,pageName)=>{
	
			let baseUIPath=this.gameConfig.basePath+PATH_SEPARATOR+SUB_PATH_UI;
			let page=self.getPage(baseUIPath, pageName, pagesContainer, flowConfigItem, isStereo);
	
			self.pushPage(pageName, page);
	
			if(!firstPage) {
				firstPage=page;
				firstPage.setIsVisible(page.pageElement, true);
			} else {
				page.setIsVisible(page.pageElement, false);
			}
			
			
			// UNUSEFUL
//			if(isStereo){
//				const element=page.pageElement;
//				domtoimage.toPng( element,{quality: 0.2}).then((dataUrl)=>{
// //			domtoimage.toJpeg(element,{quality: 0.2}).then((dataUrl)=>{
//			     	let img=new Image();
//			      img.src=dataUrl;
//						self.pushPageImageLeftEye(pageName, img);
//			    }).catch((error)=>{
//			        console.error("ERROR : Oops, something went wrong!", error);
//			    });
//			}
			
			
		});
		
		
	}
	
	/*private*/isStereo(){
		const gameConfig=this.gameConfig;
		return gameConfig.configXR && gameConfig.configXR.isStereo;
	}
		

	
		
	/*private*/getPage(imagesRootUI, pageName, parentElement, pageConfig, isStereo) {
	
		const self=this;
	
		let pageId=pageName;
		let pageLayer=document.getElementById(pageId);
		if(!pageLayer) {
			pageLayer=document.createElement("div");
			parentElement.appendChild(pageLayer);
			pageLayer.id=pageId;
	//	pageLayer.style.width="inherit";
	//	pageLayer.style.height="inherit";
			pageLayer.style.width="100%";
			pageLayer.style.height="100%";
			pageLayer.style.display=DEFAULT_CSS_DISPLAY;
			pageLayer.style["justify-content"]="center";
			pageLayer.style["align-items"]="center";
			pageLayer.style.background=pageConfig.background;
			
			// Click limitations : 
			pageLayer.style["pointer-events"]="none";
			pageLayer.onclick=(event)=>{
				event.preventDefault();
				event.stopPropagation();
			};
	
			
			// Central image :
			if(pageConfig.image){
				let pageImageSrc=getConventionalFilePath(imagesRootUI,pageConfig.image);
				// OLD :
	//			let centralImage=document.createElement("img");
	//			centralImage.src=pageImageSrc;
	//			pageLayer.appendChild(centralImage);
				pageLayer.style["background-image"]="url('"+pageImageSrc+"')";
				pageLayer.style["background-repeat"]="no-repeat";
	//			pageLayer.style["background-attachment"]="fixed";
				pageLayer.style["background-position"]="center";
				pageLayer.style["background-size"]="fit";
	
			}
	
			// Text :
			if(pageConfig.text){
				
				let textElement=document.createElement("div");
				textElement.innerHTML=pageConfig.text.message;
				textElement.style.width="100%";
				textElement.style.textAlign="center";
				textElement.style.position="absolute";
				textElement.style.color=nonull(pageConfig.text.color,"#000000");
				textElement.style["font-family"]=nonull(pageConfig.text.font,"unset");
				if(pageConfig.text.position){
					if(pageConfig.text.position.x!=null)
						textElement.style.right=pageConfig.text.position.x+"px";
					if(pageConfig.text.position.yTop!=null || pageConfig.text.position.yBottom!=null){
						if(pageConfig.text.position.yTop!=null)
							textElement.style.top=pageConfig.text.position.yTop+"px";
						else if(pageConfig.text.position.yBottom!=null )
							textElement.style.bottom=pageConfig.text.position.yBottom+"px";
					}
				}
				pageLayer.appendChild(textElement);
			}
	
			
			
	
		}
	
		let page={
					name : pageName,
					config:pageConfig,
					menus : [],
					pageElement : pageLayer,
					setIsVisible : function(pageElement, visible) {
						if(visible) {
							pageElement.style.display=DEFAULT_CSS_DISPLAY;
						} else {
							pageElement.style.display="none";
						}
						page.active=visible;
					},
					closeAllHamburgerMenus:function(){
						foreach(page.menus,(menu)=>{
							menu.closeHamburger();
						});
					},
		};
	
		let menusConfig=pageConfig.menus;
		foreach(menusConfig, (menuItem, menuName)=>{
			let menu=self.getMenu(pageName, menuName, pageLayer, menuItem, isStereo);
			page.menus.push(menu);
		});
	
		return page;
	}
	
	pushPage(pageName, page){
		this.pages[pageName]=page;
	}
	
	pushPageImageLeftEye(pageName, pageImage){
		this.pagesImages[pageName]=pageImage;
	}
	
	getPageRefByName(pageName){
		return this.pages[pageName];
	}
	
	/*public*/getCurrentPage(){
		return this.currentPage;
	}
	
	getPageByName(pageName){	
		return this.pages[pageName];
	}
	
	/*private*//*static*/setupDelayedGoToIfNeeded(page,controller){
	
		let pageConfig=page.config;
		let delayMillis=pageConfig.delayMillis;
		if(!delayMillis || !pageConfig.goTo)	return;
		
		let goToPageNameTempPage=pageConfig.goTo;
		if(!goToPageNameTempPage) return;
		
		const self=this;
		page.pageElement.delayedGoto=setTimeout(function(){
			self.goToPage(goToPageNameTempPage,controller);
		},delayMillis);	// Pages are not subjected to durationTimeFactor.

		
		// Skip on click of the splash page :
		page.pageElement.style["pointer-events"]="all";
		page.pageElement.onclick=function(event){
			let delayedGoto=event.target.delayedGoto;
			if(!delayedGoto)	return;
			clearTimeout(delayedGoto);
			self.goToPage(goToPageNameTempPage,controller);
		};
		
	}
	
	
	/*public*/goToPage(goToPageName, controller, restoreLevel=false, fileInputElement=null) {
			
		const self=this;
		
		if(!controller){
			// !!! CAUTION : THIS NAME IS A CONVENTION TO BE RESPECTED IN CLIENT CODE !!!
			controller=window.eranaScreen;
		}
			
		
		let pages=this.pages;

		let resultPage=null;
		
		// If asked page exists :
		let askedPage=this.getPageByName(goToPageName);
		if(askedPage) {

			foreach(pages, (page)=>{
				if(page.name === goToPageName){
					resultPage=page;
					// We show the found page :
					page.setIsVisible(page.pageElement, true);
				}else{
					// We hide all the other pages :
					page.setIsVisible(page.pageElement, false);
				}
				
			});
			
			if(resultPage){
				
				let previousPage=this.currentPage;

				
				// We stop the previous page music, if it has one :
				if(previousPage){
					if(isString(previousPage)){
						previousPage=this.getPageRefByName(previousPage);
					}
					if(previousPage.musicElement){
						// We handle both cases (non-loop and loop) :
						previousPage.musicElement.stopPlaying();
					}
					// We close all the previous page hamburger menus if needed :
					previousPage.closeAllHamburgerMenus();
					
					// We execute the eventual onLeave named method before going to next page :
					let onLeave=previousPage.config.onLeave;
					if(onLeave && controller[onLeave]){
						controller[onLeave]();
					}
					
					// We stop all the previous page sounds :
					controller.stopAllStartables();
					
					
				}
				
				
				// CAUTION : Invisible : they are invisible but occupy the same space
				if(resultPage.config.visibleControls===true)	this.controlsManager.setIsVisible(true);
				else																					this.controlsManager.setIsVisible(false);

				// CAUTION : Hidden : they are invisible but occupy no space		
				if(resultPage.config.hiddenControls===true)	this.controlsManager.setIsHidden(true);
				else																				this.controlsManager.setIsHidden(false);

				
				// We start the current level if needed :
				let pageName=resultPage.name;
				if(pageName && resultPage.config.startLevelName) {
					

					controller.levelsManager.startLevelIfNoCurrent(pageName, resultPage.config.startLevelName, restoreLevel, fileInputElement);

					// // We start all the next page sounds :
					// controller.startAllSounds();
					
					controller.startAllStartables();
					
				}
				
				
				// We execute the eventual onEnter named method when going to next page :
				let onEnter=resultPage.config.onEnter;
				if(onEnter && controller[onEnter]){
					controller[onEnter]();
				}
				

				
				
				// ********************************************
				// At flow on going :

				// Delayed page cascading goto behavior : (If we are supposed to see this page only an amount of time in milliseconds)
				this.setupDelayedGoToIfNeeded(resultPage, controller);
				
			}
		}
		

		
		this.currentPage=resultPage;
		
		
		
		
		// CAUTION : To deal with the «user must interact with the page first before autoplay» user annoyance protection politics :
		if(resultPage){
			

			// ********************************************
			
			// Music instantiation (for the next, clicked, page) :
			let music=resultPage.config.music;
			if(music){
				
				let volumePercent=nonull(music.volumePercent,100);
				
				if(music.loop===false){ // default value is loop by default
					
					let singleTrack=null;
					if(music.track){
						singleTrack=music.track;
					}else{
						singleTrack=Math.getRandomInArray(music.tracks);
					}
				
					if(singleTrack){
						resultPage.musicElement=loadSound(getConventionalFilePath(self.gameConfig.basePath, singleTrack, "audio"),
							function(audio){
								audio.play();
						}).setVolume(volumePercent*.01);
					}
						
				}else{
					
					if(music.track){
						
						resultPage.musicElement=loadSound(
								getConventionalFilePath(self.gameConfig.basePath, music.track, "audio"),
								function(audio){
									audio.loop(music.pauseBetweenMillis,10000,10000);
								}).setVolume(volumePercent*.01);
						
					}else{
						
						let tracksPaths=[];
						foreach(music.tracks,(track)=>{
							tracksPaths.push( getConventionalFilePath(self.gameConfig.basePath, track, "audio") );
						});
						resultPage.musicElement=loadSoundsLoop(tracksPaths,
								(soundsLoop)=>{ soundsLoop.loop(music.pauseBetweenMillis,10000,10000); }
						).setVolume(volumePercent*.01);
					}
					
				}
				
				
				
				
				
			}
			// ********************************************
			
		}
		
		
		return resultPage;
	}
		
		
	/*private*/getMenu(pageName, menuName, parentElement, menuConfig, isStereo) {
		
		const self=this;
	
		let menuId=pageName + DOM_IDS_SEPARATOR + menuName;
		let menuLayer=document.getElementById(menuId);
		if(!menuLayer) {
			menuLayer=document.createElement("div");
			menuLayer.id=menuId;
			menuLayer.style.display=DEFAULT_CSS_DISPLAY;
			menuLayer.style["flex-direction"]="column";
			parentElement.appendChild(menuLayer);
		}
	
		let isHamburger=false;
		if(menuConfig.config) {
			isHamburger=true;
			let hamburgerTriggerId=menuId + "_hamburgerTrigger";
			let hamburgerTrigger=document.getElementById(hamburgerTriggerId);
			if(!hamburgerTrigger) {
				
				hamburgerTrigger=document.createElement("input");
				hamburgerTrigger.type="button";
				hamburgerTrigger.className="action";
				hamburgerTrigger.id=hamburgerTriggerId;

				if(MENUS_PAGES_MODE==="wholePage"){
					
					let positionStyle;
					// UNUSEFUL
//				if(menuConfig.config.position === "top-right")					positionStyle=(isStereo?"top:0;right:50%;":"top:0;right:0;");
//				else if(menuConfig.config.position === "bottom-right")	positionStyle=(isStereo?"bottom:0;right:50%;":"bottom:0;right:0;");
					if(menuConfig.config.position === "top-right")					positionStyle="top:0;right:0;";
					else if(menuConfig.config.position === "bottom-right")	positionStyle="bottom:0;right:0;";
					
					else if(menuConfig.config.position === "bottom-left") 	positionStyle="bottom:0;left:0;";
					else																										positionStyle="top:0;left:0;"; // default
					hamburgerTrigger.style=positionStyle+"position:fixed;";

				}else{					
					
					const zooms=this.gameConfig.zooms;
					const zoomedResolution=getZoomedSize(this.gameConfig.getResolution(), zooms);
					
					let width;
					let height;
		
					if(this.gameConfig.isMobile){
						width= (zoomedResolution.w*(1-MENU_BUTTON_PERCENT_SIZE*.01))+ "px";
						height=(zoomedResolution.h*(1-MENU_BUTTON_PERCENT_SIZE*.01))+ "px";
					}else{
						width= (zoomedResolution.w-MENU_BUTTON_SIZE)+ "px";
						height=(zoomedResolution.h-MENU_BUTTON_SIZE)+ "px";
					}
		
					let positionStyle;
					if(menuConfig.config.position === "top-right")					positionStyle="top:0;left:"+width+";";
					else if(menuConfig.config.position === "bottom-right")	positionStyle="bottom:0;left:"+width+";";
					else if(menuConfig.config.position === "bottom-left") 	positionStyle="bottom:0;left:0;";
					else																										positionStyle="top:0;left:0;"; // default
					hamburgerTrigger.style=positionStyle+"position:fixed;";
					
				}	
					
				
				
				
				if(this.gameConfig.isMobile){
					hamburgerTrigger.style.width= MENU_BUTTON_PERCENT_SIZE+"vw";
					hamburgerTrigger.style.height=MENU_BUTTON_PERCENT_SIZE+"vw"; // default
				}else{
					hamburgerTrigger.style.width= MENU_BUTTON_SIZE+"px";
					hamburgerTrigger.style.height=MENU_BUTTON_SIZE+"px"; // default
				}
				
				hamburgerTrigger.value="≡";
				parentElement.appendChild(hamburgerTrigger);
			}
			
			// ONCLICK
			hamburgerTrigger.onclick=function() {
				let menuElement=document.getElementById(menuId);
	
				if(menuElement.style.display === "none") {
					menuElement.style.display=DEFAULT_CSS_DISPLAY;
				} else {
					menuElement.style.display="none";
				}
				// TODO : add game pause !
			};
			
	
			// Click limitations : 
			hamburgerTrigger.style["pointer-events"]="all";
	
			
			menuLayer.style.display="none";
		}
	
		let menu={
			items : [],
			isHamburger:isHamburger,
			closeHamburger:function(){
				if(!menu.isHamburger)	return;
				let menuElement=document.getElementById(menuId);
				if(menuElement.style.display !== "none") {
					menuElement.style.display="none";
				}
			},
		};
	
		let items=menuConfig.items;
		for (let i=0; i < items.length; i++) {
			let button=self.getMenuItem(pageName, menuName, "buttonConfig" + i, menuLayer, items[i]);
			menu.items.push(button);
		}
	
		return menu;
	}
	
	/*private*/calculateButtonId(pageName,menuName,buttonName,buttonConfig){
		return (empty(buttonConfig.id)?(pageName + DOM_IDS_SEPARATOR + menuName + DOM_IDS_SEPARATOR + buttonName):buttonConfig.id)	
	}
	
	
	/*private*/getMenuItem(pageName, menuName, buttonName, parentElement, buttonConfig) {
		
		const DISABLED_OPACITY=0.8;
	
		const buttonId=this.calculateButtonId(pageName,menuName,buttonName,buttonConfig);
		let buttonElement=document.getElementById(buttonId);
		if(!buttonElement) {
			
			if(!buttonConfig.isUploader){
				
				
				// OLD :
		//	buttonElement=document.createElement("input");
		//	buttonElement.type="button";
		//	buttonElement.value=buttonConfig.label;
				if(buttonConfig.type==="label"){
					buttonElement=document.createElement("label");
		//			buttonElement.style["background"]="#FFFFFF";
				}else{ // Default case is «button»
					buttonElement=document.createElement("button");
				}
				buttonElement.className="action";
				buttonElement.id=buttonId;
				buttonElement.innerHTML=nonull(buttonConfig.label,"");
				buttonElement.setAttribute("data-associatedMenu",parentElement.id);

				
				
				
				// Cases button is deactivated :
				if(buttonConfig.activeOnlyIf) {
					let functionName=buttonConfig.activeOnlyIf.replace("()", "");
					if(this.view[functionName] && !this.view[functionName]()) {
						buttonElement.disabled="disabled";
					}
				}else if(buttonConfig.inactive===true){
					buttonElement.disabled="disabled";
				}
		
				if(buttonElement.disabled){
					buttonElement.style.opacity=DISABLED_OPACITY;
					// Click limitations : 
					buttonElement.style["pointer-events"]="none";
				}else{
					buttonElement.style["font-weight"]="bold"; // CAUTION : Cannot add a <strong> tag on inner label because else it messes with the click event !
					// Click limitations : 
					buttonElement.style["pointer-events"]="all";
				}
		
			
			}else{
				
				
				// CURRENT
				// Add an hidden file input element :
				let fileInputElement=document.createElement("input");
				fileInputElement.id=buttonId+"_fileInputElement";
				fileInputElement.type="file";
				fileInputElement.style="display:none;";
				fileInputElement.buttonConfig=buttonConfig;


				buttonElement=document.createElement("label");
				buttonElement.id=buttonId;
				buttonElement.className="action";
				buttonElement.htmlFor=buttonId+"_fileInputElement";
				// TODO : Find a nice «upload» icon :
				buttonElement.innerHTML=""+nonull(buttonConfig.label,"");;
				buttonElement.fileInputElement=fileInputElement;
				buttonElement.onclick=(e)=>{
					//Because we don't want other onclick interceptors to prevent the file choosing window from opening:
					e.stopPropagation();
				};
				buttonElement.style["text-align"]="center";
				buttonElement.appendChild(fileInputElement);

				
				
				// CAUTION : *NOT* DUPLICATED CODE !
				// Cases button is deactivated :
				if(buttonConfig.activeOnlyIf) {
					let functionName=buttonConfig.activeOnlyIf.replace("()", "");
					if(this.view[functionName] && !this.view[functionName]()) {
						fileInputElement.disabled="disabled";
					}
				}else if(buttonConfig.inactive===true){
					fileInputElement.disabled="disabled";
				}
		
				if(fileInputElement.disabled){
					buttonElement.style.opacity=DISABLED_OPACITY;
					// Click limitations : 
					buttonElement.style["pointer-events"]="none";
				}else{
					buttonElement.style["font-weight"]="bold"; // CAUTION : Cannot add a <strong> tag on inner label because else it messes with the click event !
					// Click limitations : 
					buttonElement.style["pointer-events"]="all";
				}
				
				
			}
			
			buttonElement.style["margin-left"]="0";
			buttonElement.style["margin-bottom"]="5px";
			buttonElement.style.cursor="pointer";

			buttonElement.buttonConfig=buttonConfig;
	
			parentElement.appendChild(buttonElement);
		}
	
		let menuItem={
			element : buttonElement,
		};
	
		return menuItem;
	}

	
	
	
	// =============== FLOW INIT ===============


	/*private*/initFlow(controller){
		
		const self=this;
		
	
		// We cannot start the current level yet at this point!: (initAll has not been called yet !)
		//let isFirstPage=false;
		let isFirstPage=true;
		foreach(this.flowConfig,(pageConfig,pageName)=>{
	
			
			// ********************************************
			// At flow starting :
			
			let delayMillis=pageConfig.delayMillis;
			// Delayed page goto behavior :
			let page=self.getPageByName(pageName);
			if(page.active || isFirstPage){
				
				self.setupDelayedGoToIfNeeded(page,controller);
				
				if(isFirstPage)	isFirstPage=false;
				return "continue";
			}
			
			
			// ********************************************
			// Levels instantiation :
			self.initLevels(pageConfig, pageName, controller);
			
			
		});
		
	}
	
	
	/*private*/initLevels(pageConfig, pageName, controller){
		
		const self=this;
			
	
		// Menus instantiation :
		let menusConfig=pageConfig.menus;
		foreach(menusConfig,(menuConfig,menuName)=>{

			foreach(menuConfig.items,(buttonConfigItem,i)=>{

				let buttonName="buttonConfig" + i;
				const buttonId=self.calculateButtonId(pageName,menuName,buttonName,buttonConfigItem);
				let button=document.getElementById(buttonId);
				if(button){
				
					if(buttonConfigItem.getLabel){
						let getLabeFunction=controller[buttonConfigItem.getLabel];
						if(getLabeFunction){
							let labelValue=getLabeFunction();
							button.innerHTML=button.innerHTML.replace("%value%",labelValue);
						}
					}
					if(buttonConfigItem.clientIDHolder && !controller.clientIDHolderElement)
						controller.clientIDHolderElement=button;
					
					if(!button.disabled) {
	
						
//					button.buttonConfig=buttonConfigItem;
	
	
	
	
						const doOnTrigger=function(event) {
	
							let buttonLocal=event.target;
							let itemLocal=buttonLocal.buttonConfig;
	
							// CAUTION ! THIS ACTION IS CALLED PRIOR TO ANY OTHER TREATMENT !!
							if(itemLocal.actionBefore)
								executeControllerAction(controller, itemLocal.actionBefore, itemLocal.args);
	
							let goToPageName=itemLocal.goTo;
							if(goToPageName) {
								self.goToPage(goToPageName, controller, itemLocal.restoreLevel, 
									// CAUTION : When we are in upload mode, then the fileInputElement is the triggering element itself !
									buttonLocal);
							}else if(itemLocal.resume){
								document.getElementById(buttonLocal.getAttribute("data-associatedMenu")).style.display="none";
							}
							
							// CAUTION ! THIS ACTION IS CALLED AFTER ALL FLOW TREATMENT HAVE BEEN DONE !
							if(itemLocal.actionAfter)
								executeControllerAction(controller, itemLocal.actionAfter, itemLocal.args);
		
						}
						

						if(!buttonConfigItem.isUploader){
							// ONCLICK
							button.onclick=doOnTrigger;
						}else{
							// ONCHANGE
							button.fileInputElement.onchange=doOnTrigger;
						}
						
						
						
						
					}
	
				}
	
			});
		});
			
	}
	
	
	
	
}