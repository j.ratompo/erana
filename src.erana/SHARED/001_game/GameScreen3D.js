

class GameScreen3D extends GameScreenWithView{

	constructor(mainId,config,storageKey) {
		super(mainId,config,storageKey);
		
		// VIEW
		

		// INIT ALL
		

		this.models3DLoaders={};

	}
	
	// MANDATORY METHODS
	
	
		
	//=============== CONTROLLER METHODS ===============

	initOnStartScreenAtLevelStart() {
		super.initOnStartScreenAtLevelStart();
		
		
		const rootContainer=this.getSubclassRootContainer();
		this.sceneManager=new GameSceneManager3D(this,this).init(this.gameConfig).initOnStartScreenAtLevelStart();
		this.followers=this.sceneManager.placeAllOnInit(rootContainer);

		
		this.models3DLoaders["stl"]=new THREE.STLLoader();
		
		
//		const controls = new OrbitControls( camera, renderer2.domElement );
//		controls.minZoom = 0.5;
//		controls.maxZoom = 2;

		

	}

	/*public*/getModelsLoader(modelType){
		return this.models3DLoaders[modelType];
	}
	

	/*public*/loadObjectInScene(gameObject3D, overriddenBasePath=null){
		
		let returnPromise=new Promise((resolve,reject)=>{/*DO NOTHING*/});
		
		let models3DLoader=null;
		if(gameObject3D.getModelType && !empty(gameObject3D.getModelType())){
			const modelType=gameObject3D.getModelType();
			
			models3DLoader=this.getModelsLoader(modelType);
			if(!models3DLoader){
				// TRACE
				lognow("ERROR : No 3D models loader for model type «"+modelType+"». Aborting 3D model loading.");
				return returnPromise;
			}
		}
		
		const sceneWEBGL=this.sceneManager.scenes3D["webgl"];
		if(!sceneWEBGL){
			// TRACE
			lognow("ERROR : No WEBGL scene to load model into.");
			return returnPromise;
		}
		
//		const sceneWEBGLAnchored=this.sceneManager.scenes3D["webglAnchored"];
//		if(!sceneWEBGLAnchored){
//			// TRACE
//			lognow("WARN : No ANCHORED WEBGL scene to load model into.");
//		}
//		gameObject3D.loadModel(this,models3DLoader,sceneWEBGL,sceneWEBGLAnchored,overriddenBasePath);
		returnPromise=gameObject3D.loadModel(this,models3DLoader,sceneWEBGL,overriddenBasePath);
		
		return returnPromise;
	}
	
	
	/*public*/loadElementInScene(gameElement3DCSS, overriddenBasePath=null){

		let returnPromise=new Promise((resolve,reject)=>{/*DO NOTHING*/});

		const sceneWEBGL=this.sceneManager.scenes3D["webgl"];
		if(!sceneWEBGL){
			// TRACE
			lognow("ERROR : No WEBGL scene to load model into.");
			return returnPromise;
		}
		
		const sceneCSS3=this.sceneManager.scenes3D["css3"];
		if(!sceneCSS3){
			// TRACE
			lognow("ERROR : No CSS3 scene to load model into.");
			return returnPromise;
		}
		
//		const sceneWEBGLAnchored=this.sceneManager.scenes3D["webglAnchored"];
//		if(!sceneWEBGLAnchored){
//			// TRACE
//			lognow("WARN : No ANCHORED WEBGL scene to load model into.");
//		}
		const isStereo=this.sceneManager.isStereo();
//		gameElement3DCSS.loadModel(this, isStereo, sceneWEBGL,sceneWEBGLAnchored,sceneCSS3,overriddenBasePath);
		returnPromise=gameElement3DCSS.loadModel(this, isStereo, sceneWEBGL,sceneCSS3,overriddenBasePath);

		return returnPromise;
	}
	

	
	start3DLoop(){
		
		let loops={};
		
		
		// DEFAULT LOOP IS THE WEBGL ONE :
		
		const self=this;
		if(contains(this.gameConfig.projection,"webgl")){
			
			// WEBGL Renderer :
			
			
			if(this.sceneManager.isStereo()){
				
				loops["webgl"]={
					id:null/*(UNUSED)*/,
					renderers:this.sceneManager.renderers3D,
					stop:(selfParam)=>{
						selfParam.renderers["left"]["webgl"].setAnimationLoop(null);
						selfParam.renderers["right"]["webgl"].setAnimationLoop(null);
					},
					start:(selfParam,parentParam)=>{
						selfParam.renderers["left"]["webgl"].setAnimationLoop(()=>{
							parentParam.drawMainLoopToOverride("webgl","left");
						});
						selfParam.renderers["right"]["webgl"].setAnimationLoop(()=>{
							parentParam.drawMainLoopToOverride("webgl","right");
						});
					}
				};
					
				
				
			}else{ // NORMAL CASE :
				
				
				loops["webgl"]={
					id:null/*(UNUSED)*/,
					renderer:this.sceneManager.renderers3D["webgl"],
					stop:(selfParam)=>{
						selfParam.renderer.setAnimationLoop(null);
					},
					start:(selfParam,parentParam)=>{
						selfParam.renderer.setAnimationLoop(()=>{
							parentParam.drawMainLoopToOverride("webgl");
						});
					}
				};
				
			}
			
			
			

			
		}
		
		
		
		if(this.sceneManager.isCSS3()){
			
			
			// CSS3 Renderer :
			if(this.sceneManager.isCSS3Stereo()){
				
				loops["css3"]={
					id:null/*(always changes)*/,
					stop:(selfParam)=>{
						cancelAnimationFrame(selfParam.id);
					},
					start:(selfParam,parentParam)=>{
						const animate=()=>{
							selfParam.id=requestAnimationFrame(animate);
							
							parentParam.drawMainLoopToOverride("css3","left");
							parentParam.drawMainLoopToOverride("css3","right");
							
						};
						selfParam.id=requestAnimationFrame(animate);
					}
				};
				
				
			}else{ // NORMAL CASE :
				
					
				loops["css3"]={
					id:null/*(always changes)*/,
					stop:(selfParam)=>{
						cancelAnimationFrame(selfParam.id);
					},
					start:(selfParam,parentParam)=>{
						const animate=()=>{
							selfParam.id=requestAnimationFrame(animate);
							parentParam.drawMainLoopToOverride("css3");
						};
						selfParam.id=requestAnimationFrame(animate);
					}
				};
				
				
			}

		}

		
		return {
			stop:()=>{
				foreach(loops,(loop)=>{loop.stop(loop);});
			},
			start:()=>{
				foreach(loops,(loop)=>{loop.start(loop,self);});
			},
		};
	}
	
	
	/*public*/drawMainLoopToOverrideFirstTime(){
		
		if(contains(this.gameConfig.projection,"webgl")){
			if(this.sceneManager.isStereo()){
					this.drawMainLoopToOverride("webgl","left");
					this.drawMainLoopToOverride("webgl","right");
			}else{ // NORMAL CASE :
					this.drawMainLoopToOverride("webgl");
			}			
		}
		
		if(this.sceneManager.isCSS3()){
			if(this.sceneManager.isCSS3Stereo()){
					this.drawMainLoopToOverride("css3","left");
					this.drawMainLoopToOverride("css3","right");
			}else{ // NORMAL CASE :
					this.drawMainLoopToOverride("css3");
		
			}
		}
		
		
	}
	
	// MANDATORY METHODS
	/*protected*/onCameraMove(rootContainer){
		
	}

	
	//=============== CONTROLLER METHODS ===============
	
	
	
	// DOES NOT WORK :
	/*public*/setOrbitalControlsActivation(activation){
		this.sceneManager.setOrbitalControlsActivation(activation);
	}
	
	
	/*protected*/updateControls(){
		this.sceneManager.update3DControls();
		
	}


// DOES NOT WORK :
//	setFixedObject(object){
//		this.sceneManager.setFixedObject(object);
//	}
	

	//=============== VIEW METHODS ===============
	


	/*protected*/refreshAll3D(ctx,drawablesParam){
		
		
		const self=this;
		foreach(drawablesParam,(drawable1)=>{

			if(!isArray(drawable1)){
				
					if(!DRAW_CHILDREN_AFTER && drawable1.getDrawables)	self.refreshAll3D(ctx,drawable1.getDrawables());
					if(drawable1.refresh)	drawable1.refresh(ctx);
					if(DRAW_CHILDREN_AFTER && drawable1.getDrawables)		self.refreshAll3D(ctx,drawable1.getDrawables());
					
			}else{
					foreach(drawable1,(drawable2)=>{
						
						if(!DRAW_CHILDREN_AFTER && drawable2.getDrawables)	self.refreshAll3D(ctx,drawable2.getDrawables());
						if(drawable2.refresh)	drawable2.refresh(ctx);
						if(DRAW_CHILDREN_AFTER && drawable2.getDrawables)		self.refreshAll3D(ctx,drawable2.getDrawables());

					}
					// (We do not use the .getIsVisible() here, because other treatments might depend on this draw(...) execution 
					// even if the item is not visible ! We delegate it to the drawn objects to use this function or not.)
					,(drawable2)=>{ return !!drawable2; /*(forced to boolean)*/ }
//					,(drawable2)=>{ return drawable2 && typeof(drawable2.draw)!=="undefined"; }
					,(i1,i2)=>{
							// TODO : FIXME : DUPLICATED CODE !:
							// OLD : if(!i1.position || !i2.position)	return 1; // Case array of drawables;
							// NEW :
							if(!i1.position && !i2.position)	return 0; // Case array of drawables;
							// The sort is inverted : we have to draw first object having the lowest z-index :
							if(!i1.position)	return -1; // Case array of drawables;
							if(!i2.position)	return 1; // Case array of drawables;
							return i1.position.z-i2.position.z;
						});
			}
			
		}
			// (We do not use the .getIsVisible() here, because other treatments might depend on this draw(...) execution 
			// even if the item is not visible ! We delegate it to the drawn objects to use this function or not.)
			,(drawable1)=>{ return !!drawable1; /*(forced to boolean)*/ }
//		,(drawable1)=>{ return drawable1 && (isArray(drawable1) || typeof(drawable1.draw)!=="undefined"); }
			,(i1,i2)=>{
					// TODO : FIXME : DUPLICATED CODE !:
//					OLD : if(!i1.position || !i2.position)	return 1; // Case array of drawables;
					// NEW :
					if(!i1.position && !i2.position)	return 0; // Case array of drawables;
					// The sort is inverted : we have to draw first object having the lowest z-index :
					if(!i1.position)	return -1; // Case array of drawables;
					if(!i2.position)	return 1; // Case array of drawables;
					return i1.position.z-i2.position.z;
				}
			);
		
		
	}


	
	// -----------------------------------------------------------

	
	/*protected*//*OVERRIDES*/drawMainLoopToOverride(rendererType, side=null){

		
		const rootContainer=this.getSubclassRootContainer();

		if(!rootContainer.getDrawables || empty(rootContainer.getDrawables()))	return;


		const sceneManager=this.sceneManager;

		// The view only draws the current level from the model :
		
		
		this.updateControls();
		
		
		const camera3D=(side?sceneManager.camera.camera3Ds[side]:sceneManager.camera.camera3D);
		const renderer=(side?sceneManager.renderers3D[side][rendererType]:sceneManager.renderers3D[rendererType]);
		const scene3D=sceneManager.scenes3D[rendererType];
		
		if(scene3D)		renderer.render(scene3D, camera3D);
		
//		NOT POSSIBLE : ANY LATTER RENDERING ERASES The previous one !
//		// Anchoring :
//		const sceneWEBGLAnchored=sceneManager.scenes3D["webglAnchored"];
//		if(sceneWEBGLAnchored){
//			const camera3DAnchored=(side?sceneManager.cameraAnchored.camera3Ds[side]:sceneManager.cameraAnchored.camera3D);
//			renderer.render(sceneWEBGLAnchored, camera3DAnchored);
//		}
		
		// TODO : DEVELOP...
		
		return true;
	}

	
}
