

class GameScreen2D extends GameScreenWithViewAORTACClient{

	constructor(mainId,config,storageKey) {
		super(mainId,config,storageKey);
		
		// VIEW
		this.backgroundLayers=[];
		this.lockDrawing=false;
		

//		// UI
//		this.controlsManager=getControlsManager(this.gameConfig,this,this,this.mainId); // CAUTION : Here, The controller and the view is the same object.
//		this.pagesManager=new GamePagesManager(this.gameConfig,this.flowConfig,this.controlsManager,this,this,this.mainId);


		// CONTROLLER


		// INIT ALL
		

	}
	
	/*protected*/initOnStartScreenAtLevelStart() {
		super.initOnStartScreenAtLevelStart();
		
		
		const rootContainer=this.getSubclassRootContainer();
		
		
		const backgrounds=this.getCurrentBackgroundsToOverride();
		
		
		let self=this;
		
		// VIEW INIT
		// THIS MUST REMAIN THE ONLY 2D/3D SPECIFIC SUB-COMPONENT IN GameSreen : (because it's working is completly different in 2D than in 3D !)
		this.sceneManager=getSceneManager2D(this.gameConfig, backgrounds, this, this,
				(concernedCamera)=>{ /*concernedCamera and sceneManager.camera are supposed to be the same here*/ 
					self.onCameraMove(rootContainer); 
		});
		this.followers=this.sceneManager.placeAllOnInit(rootContainer);
	
	
	
	
	}
	
	
	
	// MANDATORY METHODS
	
	
	//=============== CONTROLLER METHODS ===============
	
	
	// DEFAULT BEHAVIOR :
	/*protected*/getCurrentBackgroundsToOverride(){
		const results=[];
		const gameConfig=this.gameConfig;
		const rootContainer=this.getSubclassRootContainer();
		this.addToBackgrounds(gameConfig,results,rootContainer,0);
		return results;
	}
	
	
	// FACULTATIVE PUBLIC METHODS
	
	
	
	/*public*/setCameraPositionToDeltas(deltas,isCameraOffsetted=false){
		
		if(!this.canSetCameraPositionTo({x:deltas.dx, y:deltas.dy} ,isCameraOffsetted))	return;
		
		// We have to go in the same direction as the drag :
		let camera=this.getCamera();
		// CAUTION ! With numbers, a simple if(deltas.dx){ is not enough because it is false if its value is 0 ! 
		if(deltas.dx!=null){
			camera.position.centerOffsets.x = (isCameraOffsetted?camera.position.centerOffsets.x:0) + deltas.dx;
		}
		if(deltas.dy!=null){
			camera.position.centerOffsets.y = (isCameraOffsetted?camera.position.centerOffsets.y:0) + deltas.dy;
		}
		
	}
		
	/*private*/canSetCameraPositionTo(position,isCameraOffsetted=false){
		
		let limits=this.currentLevel.limits;
		if(!limits)	return true;
		
		
		let projection = this.gameConfig.projection;
		if(contains(projection, "2D")) {

			let camera=this.getCamera();

			
			// CAUTION ! With numbers, a simple if(position.x){ is not enough because it is false if its value is 0 ! 
			if(position.x!=null){

				let offsetX=(isCameraOffsetted?camera.position.centerOffsets.x:0);
				
				if(limits._2D.x!=null){
					if(limits._2D.x.min!=null && position.x + offsetX < limits._2D.x.min ){
						return false;
					}
					if(limits._2D.x.max!=null && limits._2D.x.max < position.x + offsetX){
						return false;
					}
				}
			}
			if(position.y!=null){
				
				let offsetY=(isCameraOffsetted?camera.position.centerOffsets.y:0);

				if(limits._2D.y!=null){
					if(limits._2D.y.min!=null && position.y + offsetY < limits._2D.y.min){
						return false;
					}
					if(limits._2D.y.max!=null && limits._2D.y.max < position.y + offsetY){
						return false;
					}
				}
			}
			
		}
		
		return true;
	}
	
	
	
	
	
	
	/*protected*/onCameraMove(rootContainer){
		
		
		const allPrototypesConfig=this.prototypesConfig.allPrototypes;
		
		// Ongoing population of the model :
		const sceneManager=this.sceneManager;
		const camera=sceneManager.camera;
		const cardinalities=rootContainer.cardinalities;
	
		if(!cardinalities)	return;
		

		let alreadyPositionnedItems=[];
		
		let self=this;
		foreach(cardinalities,(cardinality,attributeName)=>{
			
			/* !!! CAUTION !!!  WHEN WE ARE IN NOT IN «preinstanciate» MODE, 
			 * «cardinality» REPRESENTS THE NUMBER OF ELEMENTS ART THE SAME TIME INSTANCIATED IN *DISPLAY AREA* !!!
			 * ELSE IT REPRESENT THE *TOTAL* AMOUNT OF INSTANCIATED OBJECTS IN THE WHOLE MODEL !!!
			 */
			
			// We DO NOT only consider drawables for instantiation !
			let collection=rootContainer[attributeName];

			
			// 2- Case we have to instanciate new objects to make the "fill" cardinality work :
			if(cardinality.value==="fill"){

				// If we haven't calculated the max filled yet :
				if(cardinality.xBoundaryLeft==null){
					cardinality.xBoundaryLeft=0;
					cardinality.xBoundaryRight=0;
					
					foreach(collection,(item,key2)=>{
						
							if(contains(self.gameConfig.projection, "2D")) {
							
								// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
								if(contains(self.gameConfig.scroll, "horizontal")) {

									let left=item.position.getLeft();
									if(left<cardinality.xBoundaryLeft){
										cardinality.xBoundaryLeft=left;
									}
									
									let right=item.position.getRight();
									if(cardinality.xBoundaryRight<right){
										cardinality.xBoundaryRight=right;
									}
									
								}
							}						
						
					},(item)=>{ 
						// We skip uninitialized items and items that can never be seen :
						return item && item.getIsVisible && item.getIsVisible() && item.draw;
					});
				}

				// TODO : FIXME : DOES STILL NOT SEEM TO WORK !

				
				let vcenter=self.gameConfig.getViewCenterOffset();
				let spawningZone=getSpawningZone(cardinality);
				
				let cameraPosition=camera.position.getParentPositionOffsetted();
				if(cameraPosition.x-vcenter.x < cardinality.xBoundaryLeft){

					
					// We add an object to the left :
					// We instanciate the object randomly :
					let className=attributeName.charAt(0).toUpperCase()+attributeName.substr(1,attributeName.length-2);
					
//					// DBG
//					console.log("INSTANCIATED NEW FILL OBJECTS LEFT : "+className);
					
					let itemAndZone=self.model.instanciateAndAddToCollectionPrototypeOnly(
							collection,
							className,
							cardinality.prototypesInstantiation,
//							cardinality.prototypeConfig,
							null,
							allPrototypesConfig,
							{x:cardinality.xBoundaryLeft});
					let item=itemAndZone.item;
					if(!spawningZone)	spawningZone=itemAndZone.spawningZone;
					
//					// DBG
//					console.log("cardinality.prototypeConfig:",cardinality.prototypeConfig);

					// (Here, in «fill» mode, then there cannot be overlapping ! (because of how the «fill» mode positioning is done)) 
					let pos=getPositionFor2DObject(spawningZone, cardinality.xBoundaryLeft+item.position.getLeft()-nonull(spawningZone.w,0));
					sceneManager.setPositionFirstTime(item, pos);
					
					// ...and we update the boundary :
					cardinality.xBoundaryLeft-=item.size.w + nonull(spawningZone.w,0);
					
					
					
				}else if(cardinality.xBoundaryRight < cameraPosition.x+vcenter.x){

					
					
					// We add an object to the right :
					// We instanciate the object randomly :
					let className=attributeName.charAt(0).toUpperCase()+attributeName.substr(1,attributeName.length-2);

//					// DBG
//					console.log("INSTANCIATED NEW FILL OBJECTS RIGHT : "+className);
					
					let itemAndZone=self.model.instanciateAndAddToCollectionPrototypeOnly(
							collection,
							className,
							cardinality.prototypesInstantiation,
//							cardinality.prototypeConfig,
							null,
							allPrototypesConfig,
							{x:cardinality.xBoundaryRight});
					let item=itemAndZone.item;
					if(!spawningZone)	spawningZone=itemAndZone.spawningZone;

					
//					// DBG
//					console.log("cardinality.prototypeConfig:",cardinality.prototypeConfig);
					
					let pos=getPositionFor2DObject(spawningZone, cardinality.xBoundaryRight+item.position.getRight()+nonull(spawningZone.w,0), null, cardinality.avoidOverlap, item.size);
					manageOverlappingItems(alreadyPositionnedItems,self.gameConfig,cardinality,item,pos);
					sceneManager.setPositionFirstTime(item, pos);
					
					// ...and we update the boundary :
					cardinality.xBoundaryRight+=item.size.w + nonull(spawningZone.w,0);

				}
				
			
			}else if(cardinality.value==="staticGroups"){ // Static groups Collection cardinality :

				
				if(!cardinality.preinstanciate){ // (If we have preinstantiated, then at this point they're already instanciated !)
					
					foreach(cardinality.prototypesInstantiation.staticInstantiations,(protoConf)=>{
		
						if(!protoConf.preinstanciate // (If we have preinstantiated, then at this point they're already instanciated !)
							&& protoConf.spawningZone && isZoomedVisibilityZoneCollidingWithZone(self.gameConfig,camera,protoConf.spawningZone)
						){

							let protoNameFromConf=getProtoNameFromConf(protoConf);
							
							self.simpleCardinalityInstanciateListOfNewInstances(collection,attributeName,protoConf
							// We have to use the parent cardinality's config :
							,cardinality.prototypesInstantiation
							,protoNameFromConf);
							
						}
						
					});

				}
				
			
			}else if(cardinality.value==="oneGroupAtRandom"){ // Random groups Collection cardinality :
					
					let protoConf=Math.getRandomInArray(cardinality.prototypesInstantiation.randomInstantiations);

					if(!cardinality.preinstanciate && !protoConf.preinstanciate	// (If we have preinstantiated, then at this point they've already been instanciated !)
						&& protoConf.spawningZone && isZoomedVisibilityZoneCollidingWithZone(self.gameConfig,camera,protoConf.spawningZone)
					){ 
						
						let protoNameFromConf=getProtoNameFromConf(protoConf);
						
						self.simpleCardinalityInstanciateListOfNewInstances(collection,attributeName,protoConf
							// We have to use the parent cardinality's config :
							,cardinality.prototypesInstantiation
							,protoNameFromConf);
					}
					
				
			}else if(isCardinalityCollection(cardinality)){  // Ratio or interval collections cardinalities :
				
				// (cardinality 5/10 = An object has 5 chances out of 10 scroll events to appear)
				// (cardinality 5->10 = Objects total number *ON DISPLAY AREA* will always be between 5 min and 10 max on scroll events)
				
				if(!cardinality.preinstanciate){ 	// (If we have preinstantiated, then at this point they've already been instanciated !)
					
					self.simpleCardinalityInstanciateListOfNewInstances(collection,attributeName,cardinality,
						// We have to use the parent cardinality's config :
						cardinality.prototypesInstantiation);
				}
				
			}else{	// CASE SINGLE ATTRIBUTE non pre-instanciated :
			
			// TODO : ... !!!
			
			
			}
			
		});
		
		
	
	}
	


	/*private*/instanciateOneRandomlyInZone(alreadyPositionnedItems,rootContainerCollection, attributeName, prototypesInstantiation, cardinality, spawningZone, chosenPrototypeParam){
	
		const className=attributeName.charAt(0).toUpperCase()+attributeName.substr(1,attributeName.length-2);
		const collection=rootContainerCollection;
		const sceneManager=this.sceneManager;
		const gameConfig=this.gameConfig;
		const allPrototypesConfig=this.prototypesConfig.allPrototypes;

//		// DBG
//		lognow("We have to instanciate in zone : ",spawningZone);
		
//		let randomX=spawningZone.x;
//		let randomY=spawningZone.y;
//		
//		if(spawningZone.w){
//			// CAUTION : ONLY HANDLES «center-positionned» ZONES !
//			let demiWidth=spawningZone.w*.5;
//			randomX=Math.getRandom(spawningZone.x+demiWidth,spawningZone.x-demiWidth);
//		}
//		if(spawningZone.h){
//			// CAUTION : ONLY HANDLES «center-positionned» ZONES !
//			let demiHeight=spawningZone.h*.5;
//			randomY=Math.getRandom(spawningZone.y+demiHeight,spawningZone.y-demiHeight);
//		}
//		

		const itemAndZone= this.model.instanciateAndAddToCollectionPrototypeOnly(
					collection,
					className,
					prototypesInstantiation,
					chosenPrototypeParam,
					allPrototypesConfig);
		const item=itemAndZone.item;
		const pos=getPositionFor2DObject(spawningZone, null, null, cardinality.avoidOverlap, item.size);
		manageOverlappingItems(alreadyPositionnedItems, gameConfig, cardinality, item, pos);
		sceneManager.setPositionFirstTime(item, pos);
	
		// DBG :
		if(!contains(item.prototypeName,ABSTRACT_PREFIX)){
			if(item.init)	item.init(gameConfig);
			if(gameConfig.allowParentLinkForChildren && item.initWithParent)	item.initWithParent();
		}

		return item;
	}
	

	/*private*/getTheoreticalMinimumNumber(cardinality){
	
		let result=0;
		
//		// TODO : ...
//		if(isString(cardinality.value) && contains(cardinality.value,"/")){
//			let splits=cardinality.value.split("/");
//			let numerator=parseInt(splits[0]);
//			let denominator=parseInt(splits[1]);
//			
//			let isToInstanciate=Math.getRandomDice(numerator,denominator);
//			
//			if(isToInstanciate)	result=1;
//			
//		}else
		if(isString(cardinality.value) && contains(cardinality.value,"->")){
			let splits=Math.getMinMax(cardinality.value);
			let min=splits.min;
			let max=splits.max;
			result=Math.getRandomInt(max,min); 
		}else{
			let min=(isNumber(cardinality.value)?cardinality.value:parseInt(cardinality.value));
			result=min; 
		}
		
		return result;
	}
		
		
	/*private*/getNumberOfItemsInZone(rootContainerCollection, zone){
		let result=0;
	
		// We check the number of items of the same collection in the zone :
		foreach(rootContainerCollection,(item,key2)=>{
			if(!item.position)
				return "continue";
			if(isInZone(item.position, zone))
				result++;
		},(item)=>{	
			// We skip uninitialized items and items that can never be seen :
			return item && item.getIsVisible && item.getIsVisible() && item.draw;
		});
		
		return result;
	}

	/*private*/simpleCardinalityInstanciateListOfNewInstances(rootContainerCollection, attributeName, cardinality, prototypesInstantiation, chosenPrototypeParam=null){
	
		const sceneManager=this.sceneManager;
		const camera=sceneManager.camera;
		const gameConfig=this.gameConfig;
		const resolution=gameConfig.getResolution();
		
		
		const visibilityZone={x:camera.position.x, y:camera.position.y, w:resolution.w, h:resolution.h};
		let hasNoSpawningZone=false;
		let zone=getSpawningZone(cardinality);
		if(!zone){
			zone=visibilityZone;
			hasNoSpawningZone=true;
		}
		
		let totalZoneSurface=(zone.h?((zone.w?zone.w:1)*zone.h):zone.w);
		if(totalZoneSurface==0)	return;
		
		let totalTheoreticalNumberOfItems=this.getTheoreticalMinimumNumber(cardinality);
		let intersectionZone;
		if(hasNoSpawningZone){
			intersectionZone=visibilityZone;
		}else{
			intersectionZone=getIntersectionZone(visibilityZone, zone);
		}
		
		
		let visibleZoneSurface=(intersectionZone.h?((intersectionZone.w?intersectionZone.w:1)*intersectionZone.h):intersectionZone.w);

		const zooms=gameConfig.zooms;
		const zoomedVisibleZoneSurface=getZoomedZone(visibleZoneSurface, zooms);

		let totalNumberOfItemsToInstanciateInIntersectionZone=(totalTheoreticalNumberOfItems*zoomedVisibleZoneSurface)/totalZoneSurface;
		
		
		if(totalNumberOfItemsToInstanciateInIntersectionZone==0)	return;
		// CAUTION : If totalNumberOfItemsToInstanciateInIntersectionZone<1 then we only have a probability of instanciating one item :
		if(totalNumberOfItemsToInstanciateInIntersectionZone<1){
			if(Math.random()<=totalNumberOfItemsToInstanciateInIntersectionZone){
				totalNumberOfItemsToInstanciateInIntersectionZone=1;
			}else{
				return;
			}
		}

		
		
		let numberOfItemsPresentInZone=this.getNumberOfItemsInZone(rootContainerCollection, zone);
		
		let numberOfItemsToInstanciateInIntersectionZone=(totalNumberOfItemsToInstanciateInIntersectionZone-numberOfItemsPresentInZone);
		
		// If we already have enough items in the intersection zone, no need to instanciate more :
		if(numberOfItemsToInstanciateInIntersectionZone<1)	return;
		
		
		// DBG
		// lognow("numberOfItemsPresentInZone : ",numberOfItemsPresentInZone);
		
		
		// At this point, we know how many of items we have to instanciate in the intersection zone :

		const alreadyPositionnedItems=[];
		for(let i=0;i<numberOfItemsToInstanciateInIntersectionZone;i++){
		
			// We have to instanciate on the edges of the intersection zone !
			
			let instanciationZone;
			if(hasNoSpawningZone)	instanciationZone=intersectionZone;
			else 					instanciationZone=this.getInstanciationZoneFromEdges(intersectionZone,visibilityZone,zone);


			const item=this.instanciateOneRandomlyInZone(alreadyPositionnedItems,rootContainerCollection, attributeName, prototypesInstantiation, cardinality, instanciationZone, chosenPrototypeParam);
			
			alreadyPositionnedItems.push(item);
		}

	}
	
	/*private*/getInstanciationZoneFromEdges(intersectionZone,zone1,zone2){
		if(areZonesIdentical(zone1,zone2))	return zone1;
		

		if(contains(intersectionZone.edges,"wholeZone1"))	return zone1;
		if(contains(intersectionZone.edges,"wholeZone2"))	return zone2;
		
		let allEdgesZones=[];
		
		// CAUTION : ONLY HANDLES «center-positionned» ZONES !
		let intersectionDemiWidth=intersectionZone.w*.5;
		if(contains(intersectionZone.edges,"left"))		allEdgesZones.push({x:intersectionZone.x-intersectionDemiWidth,y:intersectionZone.y,w:null,h:intersectionZone.h});
		if(contains(intersectionZone.edges,"right"))	allEdgesZones.push({x:intersectionZone.x+intersectionDemiWidth,y:intersectionZone.y,w:null,h:intersectionZone.h});
		// CAUTION : ONLY HANDLES «center-positionned» ZONES !
		let intersectionDemiHeight=intersectionZone.h*.5;
		if(contains(intersectionZone.edges,"bottom"))	allEdgesZones.push({x:intersectionZone.x,y:intersectionZone.y-intersectionDemiHeight,w:intersectionZone.w,h:null});
		if(contains(intersectionZone.edges,"top"))		allEdgesZones.push({x:intersectionZone.x,y:intersectionZone.y+intersectionDemiHeight,w:intersectionZone.w,h:null});
		
		
		
		return Math.getRandomInArray(allEdgesZones);		
	}
	
	
	
	/*protected*/addToBackgrounds(gameConfig,backgroundsArray,backgroundsHolder,zIndexOffset){

		const basePath=gameConfig.basePath;
		const backgroundClipSizeDefault=gameConfig.backgroundClipSize;
		const backgroundOffsetYDefault=backgroundsHolder.backgroundOffsetY;
		const foregroundOffsetYDefault=backgroundsHolder.foregroundOffsetY;

		const pathImageSrc=getConventionalFilePath(basePath,backgroundsHolder,"universe");
		
		
		if(backgroundsHolder.backgroundColor){
			
			let background=new GameBackgroundColor(backgroundsHolder.backgroundColor,gameConfig);
			backgroundsArray.push(background);
			
		}else{
			
			
			if(backgroundsHolder.backgroundImage || backgroundsHolder.backgroundImages){
				let imgConf=(backgroundsHolder.backgroundImages?
										Math.getRandomInArray(backgroundsHolder.backgroundImages):backgroundsHolder.backgroundImage);
				let img=isString(imgConf)?imgConf:imgConf.normal;
				
				let imgDark=imgConf.dark;
					
				let background=
					new GameBackground(
						nonull(backgroundsHolder.backgroundZIndex,0)+zIndexOffset,
						pathImageSrc,
						backgroundsHolder.backgroundConfig,
						gameConfig, 
						backgroundsHolder.backgroundParallax, 
						img,
						imgDark?imgDark:null,
						backgroundClipSizeDefault,
						nonull(backgroundOffsetYDefault,0),
						false);
				backgroundsArray.push(background);
				
			}else if(backgroundsHolder.backgrounds){
				
				foreach(backgroundsHolder.backgrounds,(backgroundInfo)=>{
					let imgConf=(backgroundInfo.images?Math.getRandomInArray(backgroundInfo.images):backgroundInfo.image);
					let img=isString(imgConf)?imgConf:imgConf.normal;
					
					let imgDark=imgConf.dark;
					
					let backgroundClipSize=nonull(backgroundInfo.clipSize,backgroundClipSizeDefault);
					let backgroundOffsetY=nonull(backgroundInfo.offsetY,backgroundOffsetYDefault);
						
					let background=
						new GameBackground(
								nonull(backgroundsHolder.backgroundZIndex,0)+zIndexOffset,
								pathImageSrc,
								backgroundInfo,
								gameConfig, 
								nonull(backgroundInfo.parallaxOverride, backgroundsHolder.backgroundParallax+nonull(backgroundInfo.parallaxAdd,0)), 
								img,
								imgDark?imgDark:null,
								backgroundClipSize,
								nonull(backgroundOffsetY,0)+nonull(backgroundInfo.offsetYAdd,0),
								false);
						backgroundsArray.push(background);
				});
			}
		
		}
			
		if(backgroundsHolder.foregrounds){
			
			
			foreach(backgroundsHolder.foregrounds,(foregroundInfo)=>{
				let imgConf=(foregroundInfo.images?Math.getRandomInArray(foregroundInfo.images):foregroundInfo.image);
				let img=isString(imgConf)?imgConf:imgConf.normal;
				
				let imgDark=imgConf.dark;
				
				let foregroundClipSize=nonull(foregroundInfo.clipSize,backgroundClipSizeDefault);
				let foregroundOffsetY=nonull(foregroundInfo.offsetY,foregroundOffsetYDefault);

				let foreground=
					new GameBackground(
							nonull(backgroundsHolder.backgroundZIndex+999,0)+zIndexOffset,
							pathImageSrc,
							foregroundInfo,
							gameConfig, 
							nonull(foregroundInfo.parallaxOverride, backgroundsHolder.foregroundParallax+nonull(foregroundInfo.parallaxAdd,0)), 
							img,
							imgDark?imgDark:null,
							foregroundClipSize,
							nonull(foregroundOffsetY,0)+nonull(foregroundInfo.offsetYAdd,0),
							true);
					backgroundsArray.push(foreground);
			});
		}
		
	}
	
	
	
	/*public*/startMovingPointOfViewTo(mouseDeltaX=null, mouseDeltaY=null){
	
		if(!this.canSetCameraPositionTo({x: mouseDeltaX, y: mouseDeltaX}))  	return;
		let camera=this.getCamera();

		// We have only one selection (TODO : FIXME : FOR NOW !!!) in this game :
		let singleSelectionObject=this.selectionManager.getSelected();
		if(empty(singleSelectionObject))	return;


		let cameraPosition=camera.position.getParentPositionOffsetted();


//		// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
//		if(containsOneOf(this.gameConfig.scroll,["horizontal","bidimensional"])) {
//		
//		}
	
		
		let destinationX=(mouseDeltaX!=null?(cameraPosition.x+mouseDeltaX):null);
		let destinationY=(mouseDeltaY!=null?(cameraPosition.y+mouseDeltaY):null);
		
		let destination={x:destinationX, y:destinationY};
		
		
		// We move the camera too :
		let followers=window.eranaScreen.followers;
		if(singleSelectionObject.startMovingTo)
			singleSelectionObject.startMovingTo(destination, followers);

	}
	
	
	doOnStartSwipe(mouseX, mouseY){
	
		// We have only one selection (TODO : FIXME : FOR NOW !!!) in this game :
		let singleSelectionObject=this.selectionManager.getSelected();
		if(empty(singleSelectionObject))	return;
		
		
		// We move the camera too :
		let followers=window.eranaScreen.followers;
		if(singleSelectionObject.doOnStartSwipe)
			singleSelectionObject.doOnStartSwipe(mouseX, mouseY, followers);
			
	}
	
	doOnSwiping(deltaMouseX, deltaMouseY, mouseX, mouseY){
		// We have only one selection (TODO : FIXME : FOR NOW !!!) in this game :
		let singleSelectionObject=this.selectionManager.getSelected();
		if(empty(singleSelectionObject))	return;
		
		
		// We move the camera too :
		let followers=window.eranaScreen.followers;
		if(singleSelectionObject.doOnSwiping)
			singleSelectionObject.doOnSwiping(deltaMouseX, deltaMouseY, mouseX, mouseY, followers);
	}
	
	doOnEndSwipe(swipeInputDirectionAngle){
		// We have only one selection (TODO : FIXME : FOR NOW !!!) in this game :
		let singleSelectionObject=this.selectionManager.getSelected();
		if(empty(singleSelectionObject))	return;
		
		
		// We move the camera too :
		let followers=window.eranaScreen.followers;
		if(singleSelectionObject.doOnEndSwipe)
			singleSelectionObject.doOnEndSwipe(swipeInputDirectionAngle, followers);
	}
	
	
		
	startCommand(commandName, event=null){
	

		if(this.startCommandToOverride){
			this.startCommandToOverride(commandName, event);
		}else{
		
			// We have only one selection (TODO : FIXME : FOR NOW !!!) in this game :
			let singleSelectionObject=this.selectionManager.getSelected();
			if(empty(singleSelectionObject))	return;
			
			singleSelectionObject.startCommand(commandName, event);
			
		}
		
		
	}


	//=============== VIEW METHODS ===============
	


	
	// -----------------------------------------------------------

	/*protected*//*OVERRIDES*/drawMainLoopToOverride(){
		
		const rootContainer=this.getSubclassRootContainer();
//	OLD :	let currentContainer=this.currentContainer;
//		let drawables=currentContainer.getDrawables();
		let drawables=rootContainer.getDrawables();
		
		
		// The view only draws the current level from the model :		
//		this.sceneManager.drawAll2D(currentContainer, drawables);
		this.sceneManager.drawAll2D(rootContainer, drawables);
		
		
		return true;
	}

	
	
	
}
