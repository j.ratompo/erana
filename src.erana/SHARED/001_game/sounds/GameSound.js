class GameSound{


	constructor(concernedObject) {

		this.concernedObject=concernedObject;
		this.sound=null;
	
		this.isAudible=false;
		this.previousWasPlayingState=false;
		
		this.isLoop=true // By default, GameSounds are «ambient», so, looping, sounds.
	
	}
	
	
	// INIT

	init(gameConfig,confSoundParam=null,overriddenBasePath=null){
		
		
		this.gameConfig=gameConfig;

		let confSound2D=nonull(confSoundParam,this.concernedObject.soundConfig._2D);
		
		this.perceptionSizeFactor=confSound2D.perceptionSizeFactor;
		this.baseVolumePercent=nonull(confSound2D.baseVolumePercent,100);
		this.baseSpeedPercent=nonull(confSound2D.baseSpeedPercent,100);

		
		
		let sndSrc=(confSound2D.sourcePaths?Math.getRandomInArray(confSound2D.sourcePaths):confSound2D.sourcePath);
		this.sound=this.loadSound(sndSrc,overriddenBasePath).setVolume(this.baseVolumePercent*.01).setSpeed(this.baseSpeedPercent*.01);

		
		window.eranaScreen.registerStartable(this);


	}
	
	// METHODS

	/*private*/loadSound(soundOnlySrc,overriddenBasePath=null){
		let self=this;
		let result = loadSoundInUniverse(this.gameConfig,soundOnlySrc,this.concernedObject,overriddenBasePath).setIsLoop(self.isLoop);
// OLD :
//				,function(audio){
//					// Only to avoid a slight pre-play effect :
//					audio.isAudible=false;
//					
//					self.previousWasPlayingState=true;
//					audio.loop();
//		});

		// DEBUG
		// TODO : FIXME :
		result.setVolume(0);
		
		return result;
	}

	/*public*/startStartable(){
		this.isAudible=false; // Because we want to delegate to the sub-objects in the model the fact that their sounds are audible or not during loop.
		
		// Only to avoid a slight pre-play effect :
		this.previousWasPlayingState=true;

	}
	
	/*public*/stopStartable(){
		this.isAudible=false;
		this.sound.stopPlaying();
	}
	
	
	
	/*private*/canPlay(camera){
		
		if (!this.isAudible)	return false;
		
		if(contains(this.gameConfig.projection,"2D")){
	
			if(
				// Of course, we can hear non-visible objects !
//			!this.concernedObject.getIsVisible() || 
				!this.sound || !this.sound.isReady)	return false;
			
			// // DBG
			//if(!this.concernedObject.prototypeName)
			//	console.log("BOGUS",this.concernedObject);
			
			// CAUTION ! Coordinates for DRAWING are not the same than for CALCULUS !
			let perceptionSizeFactor=this.perceptionSizeFactor;
			
			let referenceObject=camera;
			
			
			let pos=this.concernedObject.position.getParentPositionOffsetted();
			const zooms=this.gameConfig.zooms;
			
			let refPos=referenceObject.position.getParentPositionOffsetted();

			const size=this.getSoundSize();
			let hearingZone={x:pos.x,y:pos.y,w:size.w,h:size.h};
			
			return !!isInZone(refPos, hearingZone, null, null, null, zooms);
		}
	
		return true;
	}

	/*private*/getSoundSize(zooms={zx:1,zy:1}){
		let perceptionSizeFactor=this.perceptionSizeFactor;
		return {w:this.concernedObject.size.w*perceptionSizeFactor * zooms.zx,
						h:this.concernedObject.size.h*perceptionSizeFactor * zooms.zy};
	}
	
	
	/*private*/getAbsoluteDistanceX_2DScroll(camera, zooms){
		let cameraX=camera.position.getParentPositionOffsetted().x * zooms.zx;
		let objectX=this.concernedObject.position.getParentPositionOffsetted().x * zooms.zx;
		
		return Math.abs(objectX-cameraX);
	}
	
	/*private*/getSquaredDistance_2D(camera, zooms){
		const cameraParentPositionOffsetted=camera.position.getParentPositionOffsetted();
		let cameraX=cameraParentPositionOffsetted.x * zooms.zx;
		let cameraY=cameraParentPositionOffsetted.y * zooms.zy;

		const parentPositionOffsetted=this.concernedObject.position.getParentPositionOffsetted();
		let objectX=parentPositionOffsetted.x * zooms.zx;
		let objectY=parentPositionOffsetted.y * zooms.zy;
		
		return Math.pow(objectX-cameraX,2)+Math.pow(objectY-cameraY,2);
	}
	
	/*public*/getVolume(camera){
		
		let maxFactor=this.baseVolumePercent*.01;
		
		if(contains(this.gameConfig.projection,"2D")){
			
			const zooms=this.gameConfig.zooms;
			const zoomedSize=this.getSoundSize(zooms);
		
			// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
			if(contains(this.gameConfig.scroll,"horizontal")) {

				
				let absDeltaXZoomed=this.getAbsoluteDistanceX_2DScroll(camera, zooms);
				if(absDeltaXZoomed===0)	return maxFactor;
			
				// TODO : FIXME : On today, only handles the «center» position center mode :
				let maxWidthZoomed=zoomedSize.w*this.perceptionSizeFactor;
				let maxDemiWidthZoomed=Math.round(maxWidthZoomed*.5);
				if(maxDemiWidthZoomed<absDeltaXZoomed)	return 0;

				let baseSoundFactor=(1-absDeltaXZoomed/maxDemiWidthZoomed)*maxFactor;
				
				
				return baseSoundFactor;
			}else if(contains(this.gameConfig.scroll,"bidimensional")){


				let squaredDistanceZoomed=this.getSquaredDistance_2D(camera, zooms);
				if(squaredDistanceZoomed===0)	return maxFactor;
			
				// TODO : FIXME : On today, only handles the «center» position center mode :
				let maxSquaredDiameterZoomed=(Math.pow(zoomedSize.w,2)+Math.pow(zoomedSize.h,2))*this.perceptionSizeFactor;
				let maxSquaredRadiusZoomed=Math.round(maxSquaredDiameterZoomed*.5);
				if(maxSquaredRadiusZoomed<squaredDistanceZoomed)	return 0;
				

				let baseSoundFactor=(1-squaredDistanceZoomed/maxSquaredRadiusZoomed)*maxFactor;
						
				return baseSoundFactor;
			}else{
				// TODO ...
			}
		}
		
		return maxFactor;
	}
	

	/*public*/verifyPlayEachStep(camera,speed=null){
		
		if(!this.canPlay(camera)) {
			if(this.previousWasPlayingState) {
				this.sound.pauseLoop();
			}
			this.previousWasPlayingState=false;
			return false;
		}else if(!this.sound.isPlaying){
			this.sound.startPlaying();
		}

		if(!this.previousWasPlayingState) {
			this.sound.resumeLoop();
		}
		
		if(contains(this.gameConfig.projection,"2D")){
			
			let volume=Math.roundTo(this.getVolume(camera),3);
			this.sound.setVolume(volume);
			
			// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
			if(containsOneOf(this.gameConfig.scroll,["horizontal","bidimensional"])) {
				if(speed)	this.sound.setSpeed(speed);
			}else{
				// TODO : ...
			}
			
		}
		
		
		this.previousWasPlayingState=true;
		return true;
	}
	
	
	
}
