
const TIME_FACTOR_UI_X=6;
const TIME_FACTOR_UI_Y=15;
	

class GameScreenWithView extends GameScreen{

	constructor(mainId, config, storageKey) {
		super(config, storageKey);
		
		// VIEW
		this.mainId=mainId;
		this.flowConfig=config.flowConfig;
		
		// INIT ALL
		
		// VIEW
		this.controlsManager=getControlsManager(this.gameConfig,this,this,this.mainId); // CAUTION : Here, The controller and the view is the same object.
		this.selectionManager=new GameSelectionManager(this.gameConfig,this.controlsManager);
		this.tmpCanvas=document.createElement("canvas");
		this.tmpCtx=this.tmpCanvas.getContext("2d");
		this.tmpSvg=document.createElement("object");
		this.tmpSvg.contentType="image/svg+xml";
		// DBG
		{
			document.body.appendChild(this.tmpSvg);
			this.tmpSvg.style.visibility="hidden";
			this.tmpSvg.style.zIndex=-1;
			this.tmpSvg.style.position="fixed";
		}
			
		
		// UI
		this.pagesManager=new GamePagesManager(this.gameConfig,this.flowConfig,this.controlsManager,this,this,this.mainId);
		
		
		this.asyncElements=[];
		this.numberOFAsyncLoaded=0;
		
	}
	
	

	/*protected*/initOnStartScreenAtLevelStart() {
		super.initOnStartScreenAtLevelStart();
	
		if(this.controlsManager)	this.controlsManager.clearClickables();

		const gameConfig=this.gameConfig;
		
		// DURATION FACTOR :
	
		const self=this;

		// Time factor buttons :
		let buttonDecreaseTimeFactor={
			visible:true,
			position:null,
			getPosition:function(){	return buttonDecreaseTimeFactor.position; },
			size:{w:16,h:16},
			doOnClick:function(){ 
				if(!self.gameConfig.canChangeDurationTimeFactor)	return;
				if(self.getDurationTimeFactor()<self.gameConfig.maxDurationTimeFactor)	self.setDurationTimeFactor(self.getDurationTimeFactor()*2);
			},
			isClickable:()=>{	return true;	},
			isFixed:true,
			setIsVisible:function(visible){		buttonDecreaseTimeFactor.visible=visible;	}, 
			getIsVisible:function(){	return buttonDecreaseTimeFactor.visible;		},
			glyph:LEFT_ARROW_GLYPH,
		};
		this.buttonDecreaseTimeFactor=this.controlsManager.registerClickable(buttonDecreaseTimeFactor);
		this.buttonDecreaseTimeFactor.position=new Position(this.buttonDecreaseTimeFactor,TIME_FACTOR_UI_X-6,TIME_FACTOR_UI_Y+15,0,{x:"left",y:"bottom"});
		
		let buttonIncreaseTimeFactor={
				visible:true,
				position:null,
				getPosition:function(){	return buttonIncreaseTimeFactor.position; },
				size:{w:16,h:16},
				doOnClick:function(){
					if(!self.gameConfig.canChangeDurationTimeFactor)	return;
					if(self.gameConfig.minDurationTimeFactor<self.getDurationTimeFactor())	self.setDurationTimeFactor(self.getDurationTimeFactor()/2);
				},
				isClickable:()=>{	return true;	},
				isFixed:true,
				setIsVisible:function(visible){		buttonIncreaseTimeFactor.visible=visible;	},
				getIsVisible:function(){	return buttonIncreaseTimeFactor.visible;		},
				glyph:RIGHT_ARROW_GLYPH,
		};
		this.buttonIncreaseTimeFactor=this.controlsManager.registerClickable(buttonIncreaseTimeFactor);
		this.buttonIncreaseTimeFactor.position=new Position(this.buttonIncreaseTimeFactor,TIME_FACTOR_UI_X+10,TIME_FACTOR_UI_Y+15,0,{x:"left",y:"bottom"});
			
		
		this.mainElement=getMainElement(this.mainId);
		

	}

	
	/*private*/areRootControlsVisible(){
		let currentPage=this.pagesManager.getCurrentPage();
		if(!currentPage || !currentPage.config)	return true;
		return (currentPage.config.visibleControls===true && !currentPage.config.hiddenControls);
	}
	


	/*public*/startLevel(){
		
		const superSelf=this;
		if(this.gameConfig.triggerFullscreenOnStart){
			return openFullscreen(document.body)
				.then(()=>{
						return new Promise((resolveAfterAllInitsMethod,reject)=>{
							// Fullscreen may actually take some time to really settle :
							setTimeout(()=>superSelf.doStartAllAtLevelStart(resolveAfterAllInitsMethod),800);
						});
					}
				).catch((error)=> {
						return new Promise((resolveAfterAllInitsMethod,reject)=>{
							superSelf.doStartAllAtLevelStart(resolveAfterAllInitsMethod);
						});
					}
				);
		}else{
			return new Promise((resolveAfterAllInitsMethod,reject)=>{
				superSelf.doStartAllAtLevelStart(resolveAfterAllInitsMethod);
			});
		}
	}
	
	// MANDATORY METHODS
	
	
	/*public*/startAllStartables(){
		
		this.addSpinner();

		super.startAllStartables();
	}
	
	
	/*public*/addAsyncStart(){
		if(this.gameConfig.ignoreSpinner)	return;
	
		let id=getUUID("short");
		this.asyncElements.push(id);
		return id;
	}
	
	/*public*/addAsyncEnd(id){
		if(this.gameConfig.ignoreSpinner)	return;
		
//	if(!remove(this.asyncElements,id))	return;
		if(!contains(this.asyncElements,id))	return;
		this.numberOFAsyncLoaded++;
				
		this.updateSpinner();
		
	}

	/*private*/addSpinner(){
		
		if(this.gameConfig.ignoreSpinner || this.spinner || empty(this.asyncElements))	return;
		
		
		this.spinner=document.createElement("div");
		this.spinner.style.position="fixed";
		this.spinner.style.top="0";
		this.spinner.style.backgroundColor="#FFFFFF";
		this.spinner.style.zIndex="99999";
		this.spinner.style.display="flex";
		this.spinner.style["flex-direction"]="column";
		this.spinner.style["justify-content"]="center";
		this.spinner.style["align-content"]="center";
		this.spinner.style.width="100%";
		this.spinner.style.height="100%";
		document.body.appendChild(this.spinner);
		
		
		let spinnerImg=document.createElement("img");
		spinnerImg.src=getConventionalFilePath(this.gameConfig.basePath+PATH_SEPARATOR+SUB_PATH_UI, this.gameConfig.spinnerImage);
		spinnerImg.style["flex-basis"]="auto";
		spinnerImg.style["flex"]="none";
		spinnerImg.style["flex-shrink"]="1";
		spinnerImg.style["margin"]="auto";
		this.spinner.appendChild(spinnerImg);

		
		this.spinnerLabel=document.createElement("div");
		this.spinnerLabel.innerHTML="0%";
		this.spinnerLabel.style.fontSize="8vw";
		this.spinnerLabel.style["flex"]="none";
		this.spinnerLabel.style["margin"]="auto";
		this.spinner.appendChild(this.spinnerLabel);
		
		document.body.style["pointer-events"]="none";

		this.lockDrawing=true;
		
		
	}
	
	
	
	/*private*/updateSpinner(){
		
		if(this.gameConfig.ignoreSpinner || !this.spinner)	return;
		
		let percent=(empty(this.asyncElements)?0:Math.round((this.numberOFAsyncLoaded/this.asyncElements.length)*100));
		this.spinnerLabel.innerHTML=percent+"%";

		// TODO : FIXME : ADD MORE CALLBACKS TRACKING TO REMOVE THE SPINNER AS SOON AS THE LAST CALLBACKED ELEMENT LOADS !
//	setTimeout(()=>{
		if(100<=percent){
			this.lockDrawing=false;
			document.body.style["pointer-events"]="all";
			document.body.removeChild(this.spinner);
			this.spinner=null;
			this.asyncElements=[];
			this.numberOFAsyncLoaded=0;
			
			this.doOnAllLoaded();
		}
//		},6000);
		

	}
	
	doOnAllLoaded(){
		
		
	// UNUSEFUL (Since on mobile devices, the device orientation events are called almost immediatly, since it's very difficult to make a mobile device stay still) :
//		const gameConfig=this.gameConfig;
//		const isFallbackControls=(gameConfig.configXR && gameConfig.configXR.forceFallbackControls);
//		if(isFallbackControls){
//			if(this.drawMainLoopToOverrideFirstTime){
//				this.drawMainLoopToOverrideFirstTime();
//			}
//		}
		
	}
	
	
	//=============== CONTROLLER METHODS ===============
	
	
	
	

	
	
	
	// FACULTATIVE PUBLIC METHODS
	
	

	goToURLSupport() {
		if(this.gameConfig.gameURLs && this.gameConfig.gameURLs["support"])		window.open(this.gameConfig.gameURLs["support"]); 
	}
	
	goToURLOther() {
		if(this.gameConfig.gameURLs && this.gameConfig.gameURLs["other"])		window.open(this.gameConfig.gameURLs["other"]); 
	}
	
	goToURLMailTo() {
		if(this.gameConfig.gameURLs && this.gameConfig.gameURLs["email"])		window.open(this.gameConfig.gameURLs["email"]); 
	}
	
	
	/*public*/stopAllStartables(){
		super.stopAllStartables();
		
		// We clear all eventual selected objects :
		this.selectionManager.setSelected(null);
	}
	
	
	
	doLock(){
		this.lockDrawing=true;
	}
	
	doUnlock(){
		this.lockDrawing=false;
	}
	
	
	getSelectionManager(){
		return this.selectionManager;
	}
	
	//=============== CONTROLLER METHODS ===============
	
	
	
	
	executeActionOnSelection(event, methodName, inputConfig=null){

		// On today, only handle single-element selection  :
//	let selection = this.selectionManager.getSelected(0);
		let selection = this.selectionManager.getSelected();
		if(!selection)	return;
		
		let param=nonull(event.target.methodParam, event.target.value);
		
		// DBG
		console.log("EXECUTING METHOD "+methodName+" with param "+(param?JSON.stringify(param):"")+" on selection",selection);
		
		if(selection[methodName])		selection[methodName](event, param, inputConfig);
		
	}

	
	
	//=============== VIEW METHODS ===============


	/*private*/drawFPSInUI(ctx, selfParam){
		
		selfParam.deltaTMillis=getNow()-selfParam.probeTime;
		selfParam.probeTime=getNow();
		let fps=Math.round((1/selfParam.deltaTMillis)*1000);
		ctx.save();
		ctx.font = "bold 20px Arial";
		ctx.lineWidth = 4;
		ctx.strokeStyle = "#000000";
		ctx.strokeText(fps+" FPS",30,20);
		ctx.fillStyle = "#FFFFFF";
		ctx.fillText(fps+" FPS",30,20);
		ctx.restore();

	}

	
	/*private*/drawTimeFactorInUI(ctx, selfParam){
			
		// Time factor :
		let buttonDecreaseTimeFactor=selfParam.buttonDecreaseTimeFactor;
		let buttonIncreaseTimeFactor=selfParam.buttonIncreaseTimeFactor;
		
		if(!selfParam.areRootControlsVisible() || !selfParam.gameConfig.canChangeDurationTimeFactor) {
			buttonDecreaseTimeFactor.setIsVisible(false);
			buttonIncreaseTimeFactor.setIsVisible(false);
			return;
		}
		
		buttonDecreaseTimeFactor.setIsVisible(true);
		buttonIncreaseTimeFactor.setIsVisible(true);
		
		
		
		const zooms=selfParam.gameConfig.zooms;


		ctx.save();
		ctx.font = "bold "+(18 * zooms.zx)+"px Arial";
		ctx.lineWidth = 4 * zooms.zx;
		ctx.strokeStyle = "#000000";
		let timeFactor=Math.round(1/selfParam.getDurationTimeFactor());
//		// DBG
//		lognow("GAMESCREEN selfParam.gameConfig:",selfParam.gameConfig);
		
		{
			const x=TIME_FACTOR_UI_X * zooms.zx;
			const y=TIME_FACTOR_UI_Y * zooms.zy;
			ctx.strokeText(timeFactor+FACTOR_GLYPH,x,y);
			ctx.fillStyle = "#AAAAAA";
			ctx.fillText(timeFactor+FACTOR_GLYPH,x,y);
		}
		
		ctx.restore();
		
		ctx.save();
		ctx.font = "bold "+(20 * zooms.zx)+"px Monospace";
		ctx.lineWidth = 4 * zooms.zx;
		ctx.strokeStyle = "#000000";
		ctx.fillStyle = "#AAAAAA";

		{
			const x= buttonDecreaseTimeFactor.getPosition().x * zooms.zx;
			const y= buttonDecreaseTimeFactor.getPosition().y * zooms.zy;
			ctx.strokeText(buttonDecreaseTimeFactor.glyph, x, y);
			ctx.fillText(buttonDecreaseTimeFactor.glyph, x, y);
		}
		{
			const x= buttonIncreaseTimeFactor.getPosition().x * zooms.zx;
			const y= buttonIncreaseTimeFactor.getPosition().y * zooms.zy;
			ctx.strokeText(buttonIncreaseTimeFactor.glyph, x, y);
			ctx.fillText(buttonIncreaseTimeFactor.glyph, x, y);
		}
		
		ctx.restore();
		

	}
	
	
	/*protected*/drawMainLoop2D(selfParam){
		
		if(!selfParam.isModelReady)		return false;
		
		// Some heavy model treatments require the main drawing loop to be paused :
		if(selfParam.lockDrawing===true)	return false;
		
		const rootContainer=selfParam.getSubclassRootContainer();
		let drawables=rootContainer.getDrawables();
		
		// Moving
		selfParam.sceneManager.moveAll(rootContainer, drawables);
		
		// Drawing
		// (the view only draws the current level from the model :)		
		selfParam.drawMainLoopToOverride();
		
		// Hook use for additional drawing :
		selfParam.sceneManager.doDrawing(function(ctx){

			if(!ctx)	return ; // WORKAROUND FOR RARE OCCURENCES : BUGGING WHEN WE ARE IN A TRANSITION BETWEEN PAGES AND THERE IS NO CANVAS (it seems).
			
			selfParam.drawTimeFactorInUI(ctx, selfParam);

			if(IS_DEBUG){
				selfParam.drawFPSInUI(ctx, selfParam);
			}
			
		});
		
		
		return true;
	}
	

	
	
	/*protected*/drawMainLoopToOverride(){
		/*DO NOTHING*/
	}

	
	// -----------------------------------------------------------


}
