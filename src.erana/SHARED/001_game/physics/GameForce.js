class GameForce{


	constructor(x=0,y=0,z=null,norm=0,b=null,g=null,a=0){
		
		// The origin point :
		this.x=x;
		this.y=y;
		this.z=z;// NULLABLE
		this.norm=norm;
		this.baseNorm=norm;
		
		this.b=b;// NULLABLE
		this.g=g;// NULLABLE
		this.a=a;
		
		this.extremity=calculateLinearlyMovedPoint2DPolar({x:this.x,y:this.y},this.a,this.norm);
		
	
	}
	
	getDeltas(){
		const result={x:this.extremity.x-this.x, y:this.extremity.y-this.y};
		if(this.z!=null && this.extremity.z!=null)	result.z=this.extremity.z-this.z;
		return result;	
	}
	
	clone(){
		return new GameForce(this.x,this.y,this.z,this.norm,this.b,this.g,this.a);
	}
	
	setLocation(location){
		this.x=location.x;
		this.y=location.y;
		this.z=location.z;
		this.b=location.b;
		this.g=location.g;
		this.a=location.a;
		this.extremity=calculateLinearlyMovedPoint2DPolar({x:this.x,y:this.y},this.a,this.norm);
	}
	
	setNormByFactor(factor){
		const calculatedNewNorm=this.baseNorm*factor;
		if(this.norm==calculatedNewNorm)	return this;
		this.norm=calculatedNewNorm;
		return this;
	}
	
	// 2D
	getCalculatedMomemtum2D(referencePoint={x:0,y:0,a:0}){
		if(this.x-referencePoint.x==0 && this.y-referencePoint.y==0)	return 0;

		const distanceToRefPoint=Math.getDistance(referencePoint,this);
		
		const angleWithRefPoint=Math.coerceAngle(calculateAngleRadians2D(referencePoint,this)-referencePoint.a,true,true);
		const forceAngle=Math.coerceAngle(this.a-referencePoint.a,true,true);

		const consideredAngle=(forceAngle-angleWithRefPoint);		
		
		const result=(distanceToRefPoint * this.norm*.1 * Math.sin(consideredAngle)); // in N/radian
		
		// // DBG
		// lognow("angleWithRefPoint:"+angleWithRefPoint*(180/Math.PI));
		// lognow("forceAngle:"+forceAngle*(180/Math.PI));
		// lognow("momentum:"+result);
		
		return result;
	}
	
	add2D(force){

		const deltas=this.getDeltas();
		const deltasOtherForce=force.getDeltas();
		const coordinatesComposants={x:deltas.x+deltasOtherForce.x, y:deltas.y+deltasOtherForce.y};
//		if(deltas.z!=null && deltasOtherForce.z!=null)	coordinatesComposants=deltas.z+deltasOtherForce.z;

		this.x=Math.averageInArray([this.x,force.x]);
		this.y=Math.averageInArray([this.y,force.y]);
//		if(this.z!=null && force.z!=null)	this.z=Math.averageInArray([this.z,force.z]);
		
		this.extremity={x:this.x+coordinatesComposants.x, y:this.y+coordinatesComposants.y};
		
//		if(coordinatesComposants.z!=null)	this.extremity.z=this.z+coordinatesComposants.z;
		
		this.norm=Math.getDistance(this, this.extremity);
		
//		this.b=b;// NULLABLE
//		this.g=g;// NULLABLE
		this.a=calculateAngleRadians2D(this, this.extremity);

	}
	
	
	// --------------------------------------------------------------------------------------------
	
	
	draw(gameConfig,ctx,camera,color="#00FF00"){
	
		if(IS_DEBUG){
		
			let force=this;
			let p1=force;
			let p2=force.extremity;
			
			let p1Drawable=getDrawableZoomedIfNecessaryCoordinates2D(gameConfig, p1, camera, null, null, gameConfig.zooms);
			let p2Drawable=getDrawableZoomedIfNecessaryCoordinates2D(gameConfig, p2, camera, null, null, gameConfig.zooms);
	
			ctx.save();
			ctx.beginPath();
			ctx.moveTo(p1Drawable.x,p1Drawable.y);
			ctx.lineTo(p2Drawable.x,p2Drawable.y);
			ctx.strokeStyle=color;
			ctx.lineWidth = 2;			
			ctx.stroke();
			ctx.closePath();
			ctx.restore();
			
			ctx.save();
			ctx.beginPath();
			ctx.moveTo(p2Drawable.x,p2Drawable.y);
			ctx.lineTo(p2Drawable.x,p2Drawable.y-10);
			ctx.strokeStyle="#88FF00";
			ctx.lineWidth = 4;			
			ctx.stroke();
			ctx.closePath();
			ctx.restore();
			
		}
		
	
	}
	


}