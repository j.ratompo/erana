
class GamePhysicsResolver{

	constructor(){
	
		
	
		this.physicalMovers={};
		
		this.maxSpeeds=null;
		
		this.composedForceAndMomentum=null;
		
		
		
		
		
	}
	
	
	
	init(gameConfig){
		this.gameConfig=gameConfig;
		
		if(this.maxSpeeds){
			if(this.maxSpeeds.linear){
				this.maxSpeeds.x=this.maxSpeeds.linear*Math.INVERSE_OF_SQUARE_OF_TWO;
				this.maxSpeeds.y=this.maxSpeeds.linear*Math.INVERSE_OF_SQUARE_OF_TWO;
			}
			if(this.maxSpeeds.angularDegrees){
				this.maxSpeeds.a=this.maxSpeeds.angularDegrees*Math.RATIO_TO_RADIANS;
			}
			
		
		}
	}
	
	
	
	
	initAfterLevelLoading(){

		if(!this.parent){
			// TRACE
			lognow("ERROR : This physics resolver has no parent. Cannot init.");
			return;
		}
		
		if(!this.parent.getPhysicables){
			// TRACE
			lognow("ERROR : This physics resolver's parent has no getPhysicables() method. Cannot init.");
			return;
		}
		

		const self=this;
		foreach(this.parent.getPhysicables(),(physicable)=>{
			
			if(isArray(physicable)){
				foreach(physicable,(pItem)=>{
					self.startPhysicalMoverNonStopForPhysicable(pItem);
				});
			}else{
				self.startPhysicalMoverNonStopForPhysicable(physicable);
			}
		
		});
	
	}
	
	
	
	/*public*/getMaxSpeeds(){
		return this.maxSpeeds;
	}
	
	
	/*private*/startPhysicalMoverNonStopForPhysicable(physicable){
	
		const mover=getMoverMonoThreadedNonStop(physicable).setDurationTimeFactorHolder(window.eranaScreen);
		const newUUID=getUUID("short");
		this.physicalMovers[newUUID]=mover;
		let followers=null;
		if(physicable.isSelected){
			followers=[window.eranaScreen.getCamera()];
		}
		mover.doStartNonStop({x:0,y:0,a:0},followers);
		mover.physicable=physicable;
		physicable.physicableMover=mover;
		
	}
	
	/*public*/getComposedForceAndMomentum(){
		return this.composedForceAndMomentum;
	}
	
	/*public*/calculateComposedForceAndMomentum(refPosition, forces){
	
		if(empty(forces))		return {force:new GameForce(),momentum:0};
		if(forces.length==1){
			const f=forces[0];
			return {force:f, momentum:f.getCalculatedMomemtum2D(refPosition)};
		}
	
		let resultMomentum=0;
		let previousForce=null;
		foreach(forces,(force)=>{
		
			const momentum=force.getCalculatedMomemtum2D(refPosition);
			if(previousForce==null){
				previousForce=force.clone();
				resultMomentum=momentum;
				return "continue";
			}
			resultMomentum+=momentum;
			previousForce.add2D(force);
			
		});

		return {force:previousForce, momentum:resultMomentum};
	}

		
	// --------------------------------------------------------------------------------------------
	
	draw(ctx,camera,lightsManager){
	
		const DEFAULT_INERTIA=100; // In kN
		
		const self=this;
		foreach(this.physicalMovers,(mover)=>{
		
			const physicable=mover.physicable;
			const inertia=nonull(DEFAULT_INERTIA,physicable.inertia);
			if(physicable.getAllForces && !empty(physicable.getAllForces())){
			
				self.composedForceAndMomentum=self.calculateComposedForceAndMomentum(physicable.getPosition().getGlued2D(), physicable.getAllForces());
				const composedForceAndMomentum=self.composedForceAndMomentum;
				
				
				let deltaX=composedForceAndMomentum.force.extremity.x-composedForceAndMomentum.force.x;
				let deltaY=composedForceAndMomentum.force.extremity.y-composedForceAndMomentum.force.y;

				const maxSpeeds=this.getMaxSpeeds();
				if(deltaX && !maxSpeeds || !maxSpeeds.x || (maxSpeeds.x && mover.absoluteSpeeds.x<maxSpeeds.x))
					mover.incrementAbsoluteSpeeds({x:deltaX/inertia});
				if(deltaY && !maxSpeeds || !maxSpeeds.y || (maxSpeeds.y && mover.absoluteSpeeds.y<maxSpeeds.y))
					mover.incrementAbsoluteSpeeds({y:deltaY/inertia});
				if(!maxSpeeds || !maxSpeeds.a || (maxSpeeds.a && Math.abs(mover.angularSpeed)<maxSpeeds.a))
					mover.incrementAngularSpeed(composedForceAndMomentum.momentum/(inertia*1000));
				
						
				// DBG
				if(IS_DEBUG && composedForceAndMomentum.momentum/*case value==0*/){

					composedForceAndMomentum.force.draw(self.gameConfig,ctx,camera,"#FF8800");


					let drawableCoords=getDrawableZoomedIfNecessaryCoordinates2D(this.gameConfig, physicable.getPosition().getGlued2D(), camera, null, null, this.gameConfig.zooms);
					ctx.save();
					ctx.beginPath();
					ctx.moveTo(drawableCoords.x,drawableCoords.y);
					ctx.lineTo(drawableCoords.x,drawableCoords.y+composedForceAndMomentum.momentum);
					ctx.strokeStyle="#0000FF";
					ctx.lineWidth = 3;			
					ctx.stroke();
					ctx.closePath();
					ctx.restore();
				}
				
				
			}else{
				
				// «Unrealistic» friction :
				
				const linearFriction=nonull(this.linearFriction,10);
				const angularRadiansFriction=nonull(this.angularFriction,0.36)*Math.RATIO_TO_RADIANS;
				
				if(0<mover.absoluteSpeeds.x)	mover.setAbsoluteSpeedX(Math.max(0,mover.absoluteSpeeds.x-linearFriction/inertia));
				if(0<mover.absoluteSpeeds.y)	mover.setAbsoluteSpeedY(Math.max(0,mover.absoluteSpeeds.y-linearFriction/inertia));
				if(0<Math.abs(mover.angularSpeed)){
					if(0<mover.angularSpeed)	mover.setAngularSpeed( Math.max(0,mover.angularSpeed-angularRadiansFriction/inertia));
					else						mover.setAngularSpeed( Math.min(0,mover.angularSpeed+angularRadiansFriction/inertia));
				}
						
			}
			mover.doStep();
			
		});
		
	}
	

}