class GameModel{

	construct(storageKey){
		
		this.storageKey=storageKey;
		this.currentLevel=null;
		this.isModelReady=false;
		this.saverLoader = getSaverLoader();
		this.instantiationManager = getInstantiationManager(this.prototypesConfig,this.gameConfig);

		
	}

	/*public*/createInstance(className, prototypeName){
		
		if(!this.prototypesConfig.allPrototypes[className]){
			// TRACE
			console.log("ERROR : No prototype config found for class «"+className+"».");
			return null;
		}
		let protoConfig=this.prototypesConfig.allPrototypes[className][prototypeName];
		if(!protoConfig){
			// TRACE
			console.log("ERROR : No prototype config found for class «"+className+"» and prototype «"+prototypeName+"»");
			return null;
		}
		
		return this.instantiationManager.getSingleInstanceWithPrototype(className,{value:prototypeName});
	}
	
	// DELEGATED
	// CAUTION : PROTOTYPE-ONLY ! (NOT DIRECT DEFINITIONS)
	/*public*/function instanciateAndAddToCollectionPrototypeOnly(
			collection, 
			classNameParam,
			prototypesInstantiation,
			// If no chosen prototype parameter is passed, then it means we want it to be calculated with the prototype instantiation information only :
			chosenPrototypeParam=null,
			allPrototypesConfig=ALL_PROTOTYPES_CONFIG,
			position={x:null,y:null,z:null}){
			
			return this.instantiationManager.instanciateAndAddToCollectionPrototypeOnly(
			collection, 
			classNameParam,
			prototypesInstantiation,
			chosenPrototypeParam,
			allPrototypesConfig,
			position);
		}
	
	
	// DELEGATED
	createLevelState(levelName, levelConfig, allPrototypesConfig=ALL_PROTOTYPES_CONFIG){
		return this.instantiationManager.createLevelState(levelName, levelConfig, allPrototypesConfig);
	}
}