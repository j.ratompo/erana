
const IS_DEBUG=(getURLParameter("debug")=="true");

	

class GameScreen{

	constructor(config, storageKey) {

		this.gameConfig=config.gameConfig;
		this.storageKey=storageKey;
		this.prototypesConfig=config.prototypesConfig;

		// INIT ALL
		
		// MODEL
		this.model=new GameModel(this.storageKey);

		// CONTROLLER
		this.mainLoop=null;
		this.levelsManager=getLevelsManager(this.prototypesConfig, this.storageKey, this, this.model);
		if(this.gameConfig.gameCentralServer){
			this.gameNetworkManager=new GameNetworkManager(this.gameConfig, this, this.model);
		}
		
		// TECHNICAL ATTRIBUTES
//	this.sounds=[]; // TODO : Merge into this.startables
		this.startables=[];
		
		// DURATION FACTOR
		this.canChangeDurationTimeFactor=false; // (default value)
		this.durationTimeFactor=1; // (default value)
		this.minDurationTimeFactor=0.125; // (default value)
		this.maxDurationTimeFactor=1; // (default value)
		
	}
	
	
	/*public*/init(gameConfig){
		this.gameConfig=gameConfig;	
		return this;
	}
	

	/*protected*/initOnStartScreenAtLevelStart() {
		
		const gameConfig=this.gameConfig;
		
		// DURATION FACTOR :
		if(typeof(gameConfig.canChangeDurationTimeFactor)!=="undefined")	this.canChangeDurationTimeFactor=gameConfig.canChangeDurationTimeFactor;
		if(gameConfig.durationTimeFactor)			this.durationTimeFactor=gameConfig.durationTimeFactor;
		if(gameConfig.minDurationTimeFactor)	this.minDurationTimeFactor=gameConfig.minDurationTimeFactor;
		if(gameConfig.maxDurationTimeFactor)	this.maxDurationTimeFactor=gameConfig.maxDurationTimeFactor;
	}

	/*public*/setCurrentLevel(level){
		this.currentLevel=currentLevel;
	}
	/*public*/getCurrentLevel(){
		return this.currentLevel;
	}



	// DURATION FACTOR MANAGEMENT
	setDurationTimeFactor(durationTimeFactor){
		if(this.canChangeDurationTimeFactor){
			this.durationTimeFactor=durationTimeFactor;
		}
	}
	getDurationTimeFactor(){		
		return this.durationTimeFactor;
	}
	setCanChangeDurationTimeFactor(canChangeDurationTimeFactor){
		this.canChangeDurationTimeFactor=canChangeDurationTimeFactor;
	}

	/*public*/getCamera(){
		return this.sceneManager.camera;
	}
	
	
	/*public*/startLevel(){
		const self=this;
		return new Promise((resolveAfterAllInitsMethod,reject)=>{
			self.doStartAllAtLevelStart(resolveAfterAllInitsMethod);
		});
	}
	
	/*protected*/doStartAllAtLevelStart(resolveAfterAllInitsMethod){
		
		const self=this;
		
		// Networking part :
		const isNetworkedConfigured=self.handleNetworkMechanicsClientSide(resolveAfterAllInitsMethod);
		// Case offline :
		if(!isNetworkedConfigured)	self.doOnScreenReady(resolveAfterAllInitsMethod);


		
		const gameConfig=self.gameConfig;
		let fps=gameConfig.fps;
		if(fps)	self.refreshRateMillis=1000/fps;
		else		self.refreshRateMillis=nonull(gameConfig.refreshRateMillis, 500);

		self.probeTime=getNow();
				
		// MAIN GAME LOOP :
		
		
		// We ensure we have a single-threaded main loop:
		if(self.mainLoop)	self.stopGame();
		
		if(contains(self.gameConfig.projection,"HEADLESS")){
			// TODO : FIXME : DUPLICATED CODE !!!
			// Simple loop :
			self.mainLoop={
				id:null,
				stop:(selfParam)=>{
					clearInterval(selfParam.id);
				},
				start:(selfParam,parentParam)=>{
					selfParam.id=setInterval(()=>{
						parentParam.drawMainLoopHeadless(parentParam);
					},parentParam.refreshRateMillis)
				}
			};
			self.mainLoop.start(self.mainLoop,self);

		}else if(contains(self.gameConfig.projection,"2D")){
			// TODO : FIXME : DUPLICATED CODE !!!
			// Simple 2D :			
			self.mainLoop={
				id:null,
				stop:(selfParam)=>{
					clearInterval(selfParam.id);
				},
				start:(selfParam,parentParam)=>{
					selfParam.id=setInterval(()=>{
						parentParam.drawMainLoop2D(parentParam);
					},parentParam.refreshRateMillis)
				}
			};
			self.mainLoop.start(self.mainLoop,self);
		}else{
			// 3D/VR/AR (XR) :
//			const isFallbackControls=(gameConfig.configXR && gameConfig.configXR.forceFallbackControls);
//			if(!isFallbackControls){
				if(self.start3DLoop){
					self.mainLoop=self.start3DLoop();
					self.mainLoop.start(self.mainLoop,self);
				}
//			}
		}
			
			
	}
	
	
	// MANDATORY METHODS

	
	// DELEGATED
	/*private*/handleNetworkMechanicsClientSide(resolveAfterAllInitsMethod){
		if(!this.gameConfig.gameCentralServer)	return false;
		return this.gameNetworkManager.handleNetworkMechanicsClientSide(resolveAfterAllInitsMethod);
	}

	// DELEGATED
	/*public*/getClientID(){
		return this.gameNetworkManager.getClientID();
	}
	
	// DELEGATED
	joinGame(){
		this.gameNetworkManager.joinGame();
	}
	
	// DELEGATED
	leaveGame(){
		this.gameNetworkManager.leaveGame();
	}

	
	/*private*/doOnScreenReady(resolveAfterAllInitsMethod){
		const self=this;
		
		self.initOnStartScreenAtLevelStart();
		self.isModelReady=true;
	
		// DELEGATED	
		if(this.gameNetworkManager)
			this.gameNetworkManager.doOnScreenReady(resolveAfterAllInitsMethod);
		
		resolveAfterAllInitsMethod();

	}
	
	
	// DELEGATED
	// A processing is an action or an event :
	// 1)
	dispatchProcessing(type, args){
		return this.gameNetworkManager.dispatchProcessing(type, args);
	}
	
	
	// DELEGATED
	// 4)
	/*private*/executeProcessing(processing, isFollower){
	
		if(!this.getSubclassRootContainer)	return false;
		
		let rootContainer=this.getSubclassRootContainer();
		if(!rootContainer)	return false;
		
		return this.gameNetworkManager.executeProcessing(rootContainer, processing, isFollower);
	}
	
	
	stopGame(){
//	OLD
		if(this.mainLoop)	this.mainLoop.stop(this.mainLoop);
	// NOT STABLE ENOUGH :
//	cancelAnimationFrame(this.mainLoop);
	}

	// DELEGATED	
	/*protected*/abandonGame(){
		if(this.gameNetworkManager)
			this.gameNetworkManager.abandonGame();
	}
	
//	registerSound(s){
//		if(!contains(this.sounds, s))			this.sounds.push(s);
//	}
//	/*private*/unregisterSounds(){
//		clearArray(this.sounds);
//	}
	
	registerStartable(s){
		if(!contains(this.startables, s))		this.startables.push(s);
	}
	/*private*/unregisterStartables(){
		clearArray(this.startables);
	}
	
	/*public*/startAllStartables(){

		foreach(this.startables,(s)=>{
			s.startStartable();
		},(s)=>{	return !s.delayedStartAtBeginning;	});
		
		
//		foreach(this.sounds,(s)=>{
//				s.startStartable();
//		});
	
	}

	
	/*public*/stopAllStartables(){
//		foreach(this.sounds,(s)=>{	s.stopStartable();	});
//		this.unregisterSounds();
		
		foreach(this.startables,(s)=>{	s.stopStartable();	});
		this.unregisterStartables();
		
		
	}
	
	

	
	// FACULTATIVE PUBLIC METHODS
	
	exitGame() {
		this.stopGame();
		// TRACE
		lognow("TRACE : TODO : Stop game !");
		// Does not work if the game is not launched in a popup/new tab : window.close();
	}
	
	isPreviousGameDetected() {
		return this.saverLoader.hasData(this.storageKey);
	}


	
	
	// DELEGATED
	/*public*/createInstance(className, prototypeName){
		return this.model.createInstance(className, prototypeName);
	}

	// DELEGATED
	/*public*/saveGame(){
		this.levelsManager.saveGame();
	}
	
	
	//=============== VIEW METHODS ===============
	getLastMesuredRefreshedRateMillis(){
		return nonull(this.deltaTMillis,100);
	}



	//=============== MODEL METHODS ===============

	/*private*/getAllObjects(){
		const objects=[];
		const self=this;
		const rootObject=self.getSubclassRootContainer();
		if(empty(rootObject))	return objects;
		objects.push([...self.getAllTransferableDrawables(rootObject)]);
		return objects;
	}
	
	/*private*/getAllTransferableDrawables(rootObject){
		const objects=[];
		const self=this;
		if(empty(rootObject))	return objects;
		objects.push(rootObject);
		if(	!rootObject.getDrawables 
			|| empty(rootObject.getDrawables()))
				return objects;
		foreach(rootObject.getDrawables(),drawable=>{
			objects.push([...self.getAllDrawables(drawable)]);
		},(d)=>(!d.isCinematic)/*We ignore the cinematic objects*/);
		return objects;
	}
	
	// ***
	/*OVERRIDES*/getModelSize(){
		return getArraySize(this.getAllObjects());
	}
	// ***
	/*OVERRIDES*/getAllObjectsIds(){
		return this.getObjectsIds(this.getAllObjects());
	}
	// ***
	/*OVERRIDES*/getObjectsIdsWithinBoundaries(boundaries=null){
		const objects=this.getObjectsWithinBoundaries(boundaries);
		return this.getObjectsIds(objects);
	}
	// ***
	/*OVERRIDES*/getObjectsIds(objects){
		const ids=[];
		foreach(objects,object=>{
			if(!object.id)	object.id=getUUID();
			ids.push(object.id);
		});
		return ids;
	}
	

	// -----------------------------------------------------------


}
