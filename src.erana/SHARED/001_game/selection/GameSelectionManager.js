
class GameSelectionManager{
	
	
	constructor(gameConfig,controlsManager){
		
		this.infoPad=controlsManager.infoPad;

		this.active=true;
		
		this.selectedItem=null;
		
		// TODO : Make so it can handle multiple selection :
//	this.selectedItems=[];
		
		this.init(gameConfig);
		
	}
	
	
	
	/*private*/init(gameConfig){
		this.gameConfig=gameConfig;

	}
		
		
	// TODO : FIXME : On today, only handle single-element selection  :
	setSelected(selection){
		if(!this.active)	return;
		
		if(!selection){
			if(this.selectedItem){
				this.selectedItem.isSelected=false;
				if(this.selectedItem.doOnSelectionChange)	
					this.selectedItem.doOnSelectionChange(false);
			}
			this.selectedItem=null;
			return;
		}
		
		this.selectedItem=selection;
		if(selection.getInfos && selection.getInfos()){
			this.infoPad.displayInfos(selection, selection.getInfos());
		}
		
		this.selectedItem.isSelected=true;
		if(this.selectedItem.doOnSelectionChange)	
			this.selectedItem.doOnSelectionChange(true);
	}
	
	setActive(active){
		this.active=active;
		this.infoPad.setIsVisible(this.active);
	}
	
	getSelected(){
		return this.selectedItem;
	}
	
		
//	setSelected(selection){
//		if(!isArray(selection)) {
//			clearArray(self.selectedItems);
//			self.selectedItems.push(selection);
//			if(selection.getInfos && selection.getInfos()) self.infoPad.displayInfos(selection,selection.getInfos());
//		}else{
//			self.selectedItems=selection;
//		}
//		
//	}
//
//	getSelected(index=null){
//		if(index!=null)	return self.selectedItems[index];
//		return self.selectedItems;
//	}
	
	
	
}

