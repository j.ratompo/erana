/* ## Utility gamu methods - controls part
 *
 * This set of methods gathers utility game-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils-gamu» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 *  
 */


//======================== Global utility methods ========================

/*public*//*static*/function getPosition(item){
	// CAUTION : NEVER INJECT ZOOMS HERE ! For these are coordinates for CALCULUS, not DISPLAYING !
	return (item.getPosition?item.getPosition():item.position);
}
/*public*//*static*/function getZoomedZone(zone, zooms={zx:1,zy:1,zz:1}){
	const zoomedPosition=getZoomedPosition(zone, zooms);
	const zoomedSize=getZoomedSize(zone, zooms);
	const zoomedZone={x:zoomedPosition.x, y:zoomedPosition.y, z:zoomedPosition.z,
										w:zoomedSize.w, 		h:zoomedSize.h, 		d:zoomedSize.d};
	return zoomedZone;
}
/*public*//*static*/function getZoomedSize(size={w:null,h:null,d:null}, zooms={zx:1,zy:1,zz:1}){
	return {w: (size && size.w!=null?size.w*nonull(zooms.zx,1):null),
					h: (size && size.h!=null?size.h*nonull(zooms.zy,1):null),
					d: (size && size.d!=null?size.d*nonull(zooms.zz,1):null)};
}
/*public*//*static*/function getZoomedPosition(position={x:null,y:null,z:null, a:null}, zooms={zx:1,zy:1,zz:1}){
	return {x: (position && position.x!=null?position.x*nonull(zooms.zx,1):null),
					y: (position && position.y!=null?position.y*nonull(zooms.zy,1):null),
					z: (position && position.z!=null?position.z*nonull(zooms.zz,1):null),
					a: (position && position.a!=null?position.a:null)};
}



//======================== Controls utility methods ========================

/*public*/function getControlsManager(gameConfig, controller, view, /*OPTIONAL*/mainId=null) {

	const controlsLayerId = "controlsLayer";
	let controlsLayer = document.getElementById(controlsLayerId);
	if(!controlsLayer) {

		controlsLayer=document.createElement("div");
		controlsLayer.id = controlsLayerId;

		let totalWidth;
		let totalHeight;
		if(gameConfig.getResolution) {
			
			const zooms=gameConfig.zooms;
			const zoomedResolution=getZoomedSize(gameConfig.getResolution(), zooms);
			
			totalWidth=zoomedResolution.w+"px";
			totalHeight=(Math.floor(zoomedResolution.h*gameConfig.controlsPanelHeightRatio))+"px";
			controlsLayer.style.top = (Math.floor(zoomedResolution.h*(1-gameConfig.controlsPanelHeightRatio)))+"px";
		}else{
			controlsLayer.style.bottom = "0";
			totalWidth=getWindowSize("width")+"px";
			totalHeight="25%";
		}
		controlsLayer.style.width = totalWidth;
		controlsLayer.style.height = totalHeight;
		controlsLayer.style.position = "fixed";
		controlsLayer.style["text-align"] = "left";
		controlsLayer.style["z-index"] = "1000";
		controlsLayer.style.overflow="auto";
		if(gameConfig.controlsPanelStyle){
			if(gameConfig.controlsPanelStyle.background)
				controlsLayer.style.background=gameConfig.controlsPanelStyle.background;
			else if(gameConfig.controlsPanelStyle.backgroundGradient)
				controlsLayer.style["background-image"]="linear-gradient("+gameConfig.controlsPanelStyle.backgroundGradient+")";
				
			controlsLayer.style.color=gameConfig.controlsPanelStyle.text;
		}
		// Hidden by default :
		controlsLayer.style.display="none";

		let mainElement = getMainElement(mainId);
		mainElement.appendChild(controlsLayer);
	}


	let self= {
		gameConfig:gameConfig,
		view:view,
		infoPad : getInfoPad(gameConfig, controlsLayer, controller),

		doOnStartSwipe:(mouseX, mouseY)=>{
			controller.doOnStartSwipe(mouseX, mouseY);
		},
		doOnSwiping:(deltaMouseX, deltaMouseY, mouseX, mouseY)=>{
			controller.doOnSwiping(deltaMouseX, deltaMouseY, mouseX, mouseY);
		},
		doOnEndSwipe:(swipeInputDirectionAngle)=>{
			controller.doOnEndSwipe(swipeInputDirectionAngle);
		},
		
		refreshInfoPad:function(selectedItem, uiInfoDataParam=null){
			if(!selectedItem || controller.selectionManager.getSelected()!==selectedItem)	return;
			let uiInfoData;
			if(uiInfoDataParam)	uiInfoData=uiInfoDataParam;
			else								uiInfoData=selectedItem.getInfos();
			self.infoPad.displayInfos(selectedItem,uiInfoData,true);
		},
		
		clickables:[],
		
		registerClickable:function(clickableElement){
			if(	!clickableElement.doOnClick
			 || !clickableElement.isClickable
			 || !clickableElement.isClickable())	return null;
			self.clickables.push(clickableElement);
			
			return clickableElement;
		},
		
		unregisterClickable:function(clickableElement){
			if(	!clickableElement.doOnClick
			 || !clickableElement.isClickable
			 || !clickableElement.isClickable())	return null;
			
			remove(self.clickables,clickableElement);
			
			return clickableElement;
		},

		clearClickables:function(){
			clearArray(self.clickables);
		},
		
		// CAUTION : Invisible : they are invisible but occupy the same space
		setIsVisible:function(isVisible){
			setElementVisible(controlsLayerId, isVisible);
		},
		
		// CAUTION : Hidden : they are invisible but occupy no space		
		setIsHidden:function(isHidden){
			setElementHidden(controlsLayerId, isHidden);
		},

		
		/*private*/isClicked:function(clickableElement,mouseX,mouseY){
			
			if( !clickableElement.doOnClick
			 ||	!clickableElement.isClickable
			 || !clickableElement.isClickable()
			 || (clickableElement.getIsVisible && !clickableElement.getIsVisible())
			)	return false;
			
			let projection = gameConfig.projection;
			if(contains(projection,"2D")) {
			
				let clickableZone;
				let clickableSize=clickableElement.clickableSize;
				
				const zooms=self.gameConfig.zooms;
				let drawableZoomedCoords;
				if(clickableElement.isFixed!==true){
					const camera=self.view.getCamera();
					let offsettedPos=getPosition(clickableElement).getParentPositionOffsetted();
					drawableZoomedCoords=getDrawableZoomedIfNecessaryCoordinates2D(self.gameConfig, {x:offsettedPos.x, y:offsettedPos.y}, camera, null, null, zooms);
				}else{
					let pos=getPosition(clickableElement);
					
					//let w=(clickableSize?clickableSize.w:clickableElement.size.w);
					//let h=(clickableSize?clickableSize.h:clickableElement.size.h);
					drawableZoomedCoords={x:pos.x * zooms.zx, y:pos.y * zooms.zy};
				}
				clickableZone={
						x:drawableZoomedCoords.x,
						y:drawableZoomedCoords.y, 
						w:(clickableSize?clickableSize.w:clickableElement.size.w) * zooms.zx,
						h:(clickableSize?clickableSize.h:clickableElement.size.h) * zooms.zy
				};

				return isInZone({x:mouseX,y:mouseY}, clickableZone, null, getPosition(clickableElement).center, true);
			}
			
			return false;
		},
		
		doOnScreenClick:function(event){
			
			var projection = gameConfig.projection;
			if(contains(projection, "2D")) {
				

				// We ignore elements that already have a click event :
				let target=event.target;
				if(target.onclick || target.onchange 
						// We ignore the promptWindow :
						|| 
//						(
						target.tagName.toLowerCase()!=="canvas"
//						 && target.id!=="interactionLayer") 
						 ) {
					return target;
				}
				
				
				let inputsCoords=getInputCoords(event);
				let mouseX=inputsCoords.clientX;
				let mouseY=inputsCoords.clientY;
	
				let clickedClickable=null;
				let isClickableClicked=nonull(
					foreach(self.clickables,(clickableElement)=>{
						// The clickable element only needs a position, a size, and a doOnClick() method :
						if(self.isClicked(clickableElement,mouseX,mouseY)){ // CAUTION : here the Y axis is NOT inverted !
							clickedClickable=clickableElement;
							
							// On today, only handle single-element selection  :
							if(self.gameConfig.selectionCanChange && clickableElement.isSelectable && clickableElement.isSelectable())
								controller.selectionManager.setSelected(clickableElement);

							// UNUSEFUL control, because  filtering on this has already been made in clickable registration !:
							//	if(clickableElement.doOnClick)
							clickableElement.doOnClick();
							
							return true;
						}
						
					},()=>{return true;}
					 ,(i1,i2)=>{
						if(!getPosition(i1) || !getPosition(i2))	return 1;
						let z1=getPosition(i1).z;
						let z2=getPosition(i2).z;
						return z2-z1;
					}
				),false);
				
				return clickedClickable;
			}
			return null;
		},
		
		
	};
	
	// Only to avoid «Cannot access 'self' before initialization»-type errors :
	self.interactionPad=getInteractionPad(gameConfig, controlsLayer, self, controller, view);
	self.controlsLayer=controlsLayer;
	
	return self;
}


//======================== Controls utility methods ========================

//IN-GAME CONTROLS



function getDirectionController(gameConfig, interactionLayer, parentElement, controlsManager, controller, view) {

//	let directionPosition = nonull(gameConfig.directionPosition, "left");

//	let directionId = "directionLayer";
//	let directionLayer = document.getElementById(directionId);
	let directionLayer = interactionLayer.directionLayer;
	if(!directionLayer) {

//		directionLayer = parentElement.appendChild(document.createElement("div"));
//		directionLayer.id = directionId;
//
//		directionLayer.style.width = "100%";
//		directionLayer.style.height = "100%";
//		directionLayer.style.position = "fixed";
//		directionLayer.style.display="flex";
//
//		if(directionPosition === "left")	directionLayer.style.left = "0";
//		else								directionLayer.style.right = "0";
		
		interactionLayer.directionLayer={};
		directionLayer=interactionLayer.directionLayer;
		
		
//		// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
//			directionLayer.style["flex-direction"]="row";
			
			
//			if(gameConfig.controlsMode==="buttons"){
//				
//				let directionButtonLeft=directionLayer.appendChild(document.createElement("button"));
//	//		directionButtonLeft.value="&gt;";
//				directionButtonLeft.innerHTML="&gt;";
//				directionButtonLeft.onclick=function(event){
//					controller.move("left");
//				};
//				
//				let directionButtonRight=directionLayer.appendChild(document.createElement("button"));
//	//		directionButtonRight.value="&lt;";
//				directionButtonRight.innerHTML="&lt;";
//				directionButtonRight.onclick=function(event){
//					controller.move("right");
//				};
//
//			}else

			if(contains(gameConfig.controlsMode,"screen-click")){
				
				document.body.addEventListener("click",function(event){
					
					// Click management :
					
					if(controlsManager.doOnScreenClick(event))	return;
					
					const projection = gameConfig.projection;
					
					// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
					if(contains(projection, "2D")) {

						const inputCoords=getInputCoords(event);
						let mouseX=inputCoords.clientX;
						let offset=gameConfig.getViewCenterOffset(true);// We have to ignore the offset here, since it is a UI concern only !
						
						let deltaX= mouseX-offset.x;
						let deltaY=null;
						
						const scroll = gameConfig.scroll;
						if(!contains(scroll, "horizontal")) {
							let mouseY=inputCoords.clientY;
							deltaY= mouseY-offset.y;
						}
						
						if(contains(gameConfig.controlsMode,"object")){
							
							controller.startMovingPointOfViewTo(deltaX,(deltaY==null?null:-deltaY)); // CAUTION : here the Y axis is inverted !
							
						}else if(contains(gameConfig.controlsMode,"camera")){
							
							if(gameConfig.cameraFixed && gameConfig.cameraFixed!=="selection")
								view.startMovingCameraTo(deltaX,(deltaY==null?null:-deltaY)); // CAUTION : here the Y axis is inverted !
	
						}
						
						
					}
					
					
				});
				
			}else if(contains(gameConfig.controlsMode,"screen-drag")){
				
				
				// UNUSED
//				const MOUSE_SENSITIVITY_FACTOR=0.2;

				const moveStartFunction=(function(event){
					
					// If we selection an element :
					let clicked = controlsManager.doOnScreenClick(event);
					directionLayer.clicked=clicked;
					
					if(clicked)	return;
					
					
					let mouseX=null, mouseY=null;
					const projection = gameConfig.projection;
					// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
					const scroll = gameConfig.scroll;
					if(contains(projection, "2D")) {
						mouseX=getInputCoords(event).clientX;
						directionLayer.mouseX=mouseX;
						directionLayer.cameraOffsetX=view.getCamera().position.centerOffsets.x;
						if(!scroll || !contains(scroll, "horizontal")) {
							mouseY=getInputCoords(event).clientY;
							directionLayer.mouseY=mouseY;
							directionLayer.cameraOffsetY=view.getCamera().position.centerOffsets.y;
						}
						directionLayer.isMoveStarted=true;
					}
					
					
					if(controlsManager.doOnStartSwipe)		controlsManager.doOnStartSwipe(mouseX, mouseY);
					
//					event.preventDefault();

//					if(contains(gameConfig.controlsMode,"fireSwipingEvenWhenNotMoving") && !directionLayer.onGoingFunctionRoutine){
//						directionLayer.onGoingFunctionRoutine=setInterval(moveGoingFunction,300);
//					}
					
				});
				
				
				const moveGoingFunction=(function(event=null){
				
					
					
					if(!directionLayer.isMoveStarted)	return;
					
					const projection = gameConfig.projection;
					// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
					if(contains(projection, "2D")) {
						const scroll = gameConfig.scroll;


						let oldMouseX=directionLayer.mouseX;
						if(!oldMouseX)	return;
						
						
						let inputsCoords={clientX:directionLayer.mouseX, clientY:directionLayer.mouseY};
						if(event)		inputsCoords=getInputCoords(event);
						
						
						let mouseX=inputsCoords.clientX;

						let offset=gameConfig.getViewCenterOffset(true);// We have to ignore the offset here, since it is a UI concern only !
						let deltaMouseX=(mouseX-oldMouseX-directionLayer.cameraOffsetX);

						
						let deltaMouseY=null;
						let mouseY=null;
						if(!scroll || !contains(scroll, "horizontal")) {		
							
							let oldMouseY=directionLayer.mouseY;
							if(!oldMouseY)	return;
							
							mouseY=inputsCoords.clientY;
							
							deltaMouseY=(mouseY-oldMouseY-directionLayer.cameraOffsetY);
						}
						
						
						if(contains(gameConfig.controlsMode,"object")){
								
							// TODO : Develop...
							
						}else if(contains(gameConfig.controlsMode,"camera")){
							if(gameConfig.cameraFixed && gameConfig.cameraFixed!=="selection"){
								
								// OLD
								// We have to go in the same direction as the drag :
								view.setCameraPositionToDeltas({dx:-deltaMouseX,dy:(deltaMouseY==null?null:deltaMouseY)}); // CAUTION : here the Y axis is inverted ! (so we have NOT the - sign !)
							}
						}

						if(controlsManager.doOnSwiping)		
							controlsManager.doOnSwiping(deltaMouseX, deltaMouseY, mouseX, mouseY);
							
						controlsManager.deltaMouseX=deltaMouseX;
						if(!scroll || !contains(scroll, "horizontal")) {		
							controlsManager.deltaMouseY=deltaMouseY;
						}
						
					}
					
//					event.preventDefault();

				});
				
				
				const moveEndFunction=(function(event){
					directionLayer.isMoveStarted=false;
					
					let projection = gameConfig.projection;
					// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
					if(contains(projection, "2D")) {
					
						if(controlsManager.deltaMouseX==null)	return;
						if(controlsManager.doOnEndSwipe) {
							let swipeInputDirectionAngle;
							if(scroll && contains(scroll, "horizontal")) {		
								swipeInputDirectionAngle=(controlsManager.deltaMouseX<0?Math.PI:0);
							}else{
								swipeInputDirectionAngle=Math.coerceAngle(Math.atan2(controlsManager.deltaMouseY,controlsManager.deltaMouseX),true,true);
							}
							
							controlsManager.doOnEndSwipe(swipeInputDirectionAngle);
							
							controlsManager.deltaMouseX=null;
							if(!scroll || !contains(scroll, "horizontal")) {		
								controlsManager.deltaMouseY=null;
							}
							
						}
					}
//					event.preventDefault();

//					if(directionLayer.onGoingFunctionRoutine){
//						clearInterval(directionLayer.onGoingFunctionRoutine);
//						directionLayer.onGoingFunctionRoutine=null;
//					}
				});
				
				

				
				
				if(!isDeviceMobile()){
					document.body.addEventListener("mousedown",moveStartFunction);
//					if(!contains(gameConfig.controlsMode,"fireSwipingEvenWhenNotMoving"))
					document.body.addEventListener("mousemove",moveGoingFunction);
					document.body.addEventListener("mouseup",moveEndFunction);
				}else{
					document.body.addEventListener("touchstart",moveStartFunction);
//					if(!contains(gameConfig.controlsMode,"fireSwipingEvenWhenNotMoving"))
					document.body.addEventListener("touchmove",moveGoingFunction);
					document.body.addEventListener("touchend",moveEndFunction);
				}
			
				
			}
			
			
			addAdditionalBehaviorHandling(directionLayer, gameConfig, view, controller);
			
//		}
		
		
	}

	return directionLayer;
}

/*private*/function addAdditionalBehaviorHandling(directionLayer, gameConfig, view, controller){

	// Sub-behavior :
	if(contains(gameConfig.controlsMode,"focus-on-double-click")){
		
		document.body.addEventListener("dblclick",function(event){
			
			const projection = gameConfig.projection;
			// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
			if(contains(projection, "2D")) {
				
			
				// If we select an element :
				let clicked = directionLayer.clicked;
				
				if(clicked && getPosition(clicked) && clicked.isFocusable)	{
					
					if(gameConfig.cameraFixed && gameConfig.cameraFixed!=="selection"){

						let clickedPosition=getPosition(clicked);
						let clickedX=clickedPosition.x;
						let clickedY=null;								
						const scroll = gameConfig.scroll;
						if(!contains(scroll, "horizontal")) {
							clickedY=clickedPosition.y
						}
						
						view.setCameraPositionToDeltas({dx:clickedX,dy:(clickedY==null?null:-clickedY)}); // CAUTION : here the Y axis is inverted !
					}
				}
			}
		});
	}
	


	// Custom behavior :
	if(gameConfig.customKeyboardControls){
		
		document.body.addEventListener("keydown",function(event){
			let keyCode=event.code;

			let commandName=gameConfig.customKeyboardControls[keyCode];
			if(!commandName){
				// TRACE
				lognow("WARN: Unrecognized key code «"+keyCode+"» (no command found for this key code).");
				return;
			}
		
			controller.startCommand(commandName);
		
		});
		
		
	}
	
	if(gameConfig.customMouseControls){
		document.body.addEventListener("mousedown",function(event){
			const MOUSE_MAPPING=["Left","Wheel","Right"]; // cf. https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/button
			let mouseButtonCode=MOUSE_MAPPING[event.button];

			let commandName=gameConfig.customMouseControls[mouseButtonCode];
			if(!commandName){
				// TRACE
				lognow("WARN: Unrecognized mouse code «"+mouseButtonCode+"» (no command found for this mouse code).");
				return;
			}
		
			controller.startCommand(commandName, event);
		
		});
	}


}


//function getActionPad(gameConfig, parentElement, interactionLayer, controller, view) {
//
//	var actionPosition = nonull(gameConfig.actionPosition, "right");
//
//
//	var actionId = "actionLayer";
//	var actionLayer = document.getElementById(actionId);
//	if(!actionLayer) {
//
//		actionLayer = document.createElement("div");
//		actionLayer.id = actionId;
//
//		actionLayer.style.width = "10%";
//		actionLayer.style.height = "100%";
//		actionLayer.style.position = "fixed";
//
//		if(actionPosition === "left")
//			actionLayer.style.left = "0";
//		else
//			actionLayer.style.right = "0";
//
//		parentElement.appendChild(actionLayer);
//	}
//
//
//	return actionLayer;
//}


function getInteractionPad(gameConfig, parentElement, controlsManager, controller, view) {

//	let interactionId = "interactionLayer";
//	let interactionLayer = document.getElementById(interactionId);
	let interactionLayer = parentElement.interactionLayer;
	if(!interactionLayer) {

//		interactionLayer = document.createElement("div");
//		interactionLayer.id = interactionId;
//
//		interactionLayer.style.width = "100%";
//		interactionLayer.style.height = "100%";
//		interactionLayer.style.position = "fixed";
//
//		parentElement.appendChild(interactionLayer);
		
		parentElement.interactionLayer={};
		interactionLayer=parentElement.interactionLayer;
	}

	// OLD :
//	let self;
//	if(gameConfig.controlsPosition==="direction action"){
//		self= {
//			directionController : getDirectionController(gameConfig, parentElement, interactionLayer, controlsManager, controller, view),
//			actionPad : getActionPad(gameConfig, parentElement, interactionLayer, controller, view),
//		};
//		
//	}else{
//		// In a different creation order :
//		self= {
//			actionPad : getActionPad(gameConfig, parentElement, interactionLayer, controller, view),
//			directionController : getDirectionController(gameConfig, parentElement, interactionLayer, controlsManager, controller, view),
//		};
//		
//	}
//	return self;

	return {directionController : getDirectionController(gameConfig, parentElement, interactionLayer, controlsManager, controller, view)};
}


/*private*/function getInfoPad(gameConfig, parentElement, controller) {

	var infoId = "infoLayer";
	var infoLayer = document.getElementById(infoId);
	if(!infoLayer) {

		infoLayer = document.createElement("div");
		infoLayer.id = infoId;

		infoLayer.style.width = "100%";
		infoLayer.style.height = "75%"; // To allow space for following (sibling) interaction layer.
		infoLayer.style.margin = "auto";
		infoLayer.style["font-family"]="helvetica";
		infoLayer.style["font-size"]="11px";
		
		parentElement.appendChild(infoLayer);
	}


	let self={
			infoLayer:infoLayer,
			visible:true,
			setIsVisible(visible){
				self.visible=visible;
				self.infoLayer.style.visibility=(self.visible?"visible":"hidden");
			},
			displayInfos:function(selectedItem, uiInfoData){
				
				const DISABLED_OPACITY=0.6;
				
				let title=uiInfoData.title;
				let isElementFirstCreated=false;

				let header=null;
				let element=document.getElementById(title);
				if(!element){

					element=document.createElement("div");
					element.id=title;
					self.infoLayer.appendChild(element);
					isElementFirstCreated=true;

					// Title/description root :					
					const descriptionElement=document.createElement("div");
					element.appendChild(descriptionElement);

					// Header :
					if(selectedItem["getInfosHeaderHTML"]){
						header=document.createElement("div");
						header.id="infosHeader_"+element.id;
						descriptionElement.appendChild(header);
					}

					// Title :
					const h1=document.createElement("h1");
					h1.className="title";
					h1.innerHTML=title;
					descriptionElement.appendChild(h1);
	
					// Description :
					if(uiInfoData.description) {
						const p=document.createElement("p");
						p.className="description";
						p.innerHTML=uiInfoData.description;
						descriptionElement.appendChild(p);
					}
				}
			
				// Header (refresh) :
				if(selectedItem["getInfosHeaderHTML"]){
					if(!header){
						header=document.getElementById("infosHeader_"+element.id);
						header.innerHTML=selectedItem["getInfosHeaderHTML"]();
					}
				}


				
				
				// Actions :
				if(uiInfoData.actions && !empty(uiInfoData.actions)){

					let actionsElementId=title+"_actionsElement";
					// To refresh the UI of the selected item :
					if(!isElementFirstCreated){
						element.removeChild(document.getElementById(actionsElementId));
					}

					let actionsElement=document.createElement("div");
					actionsElement.style="display:flex; flex-wrap:wrap;";
					actionsElement.id=actionsElementId;
					element.appendChild(actionsElement);
					
					foreach(uiInfoData.actions,(action,methodName)=>{
						
						if(!action.type){
							
							// Range inputs :
							if(action.value!=null && isString(action.value) && contains(action.value,"=>")){
								const splits=Math.getMinMax(action.value,"=>");
								const min=splits.min;
								const max=splits.max;
								
								const input=document.createElement("input");
								input.id="input_"+methodName;
								input.className="range";
								input.type="range";
								input.min=min;
								input.max=max;
								
								const inputConfig={min:min, max:max};
								
								// TODO : FIXME : DUPLICATED CODE !
								if(contains(action.type,"inactive") || action.inactive){
									input.disabled=true;
									input.style.opacity=DISABLED_OPACITY;
								}else{
									input.onchange=function(event){
										controller.executeActionOnSelection(event,methodName,inputConfig);
									};
								}
								
								input.innerHTML=action.label;
								input.methodParam=action.value;

								// TODO : FIXME : DUPLICATED CODE !
								if(action.readCheckedStateForValueMethod
										&& selectedItem[action.readCheckedStateForValueMethod]){
									input.value=selectedItem[action.readCheckedStateForValueMethod]();
								}

								if(action.showValues!==false){
									const labelMin=document.createElement("span");
									labelMin.innerHTML=min;
									actionsElement.appendChild(labelMin);
								}
								actionsElement.appendChild(input);
								if(action.showValues!==false){
									const labelMax=document.createElement("span");
									labelMax.innerHTML=max;
									actionsElement.appendChild(labelMax);
								}

							}else{
								// Simple button inputs :
								
								let button=document.createElement("button");
								button.id="input_"+methodName;
								button.className="action";
								if(contains(action.type,"inactive") || action.inactive){
									button.disabled=true;
									button.style.opacity=DISABLED_OPACITY;
								}else{
									button.onclick=function(event){
										controller.executeActionOnSelection(event,methodName);
									};
								}
								button.innerHTML=action.label;
								button.methodParam=action.value;
								actionsElement.appendChild(button);
							}
						
						}else if(contains(action.type,"checkbox") || contains(action.type,"radio")){
							
							// We automatically add a separator :
							{
								let lineBreak=document.createElement("span");
								lineBreak.style="display:flex;flex-grow:1;width:100%;height:10px;";
								actionsElement.appendChild(lineBreak);
							}

							
							// Checkbox or radio buttons inputs :
							let values=nonull(action.values,[methodName]);
								
							foreach(values,(value)=>{

								let fieldElement=document.createElement("span");
								fieldElement.style="width:25%;";
								
							
								let groupButtonId=value+"_"+action.type;
								
								let groupButton=document.createElement("input");
								groupButton.id=groupButtonId;
								groupButton.type=contains(action.type,"radio")?"radio":"checkbox";
								groupButton.className="action";
								if(contains(action.type,"inactive") || action.inactive){
									groupButton.disabled=true;
									groupButton.style.opacity=DISABLED_OPACITY;
								}else{
									groupButton.onchange=function(event){
										controller.executeActionOnSelection(event,methodName);
									};
								}
								groupButton.methodParam=value;
								if(action.readCheckedStateForValueMethod
										&& selectedItem[action.readCheckedStateForValueMethod] 
										&& selectedItem[action.readCheckedStateForValueMethod](value))
											groupButton.checked=true;
								else	groupButton.checked=false;
								fieldElement.appendChild(groupButton);
								
								const label=document.createElement("label");
								label.htmlFor=groupButtonId;
								label.style.display="inline-block";
								label.innerHTML=action.label.replace("%value%",value);
								fieldElement.appendChild(label);

								actionsElement.appendChild(fieldElement);

								if(contains(action.type,"amount")
											&& action.readAmountAndTotalMethod
											&& selectedItem[action.readAmountAndTotalMethod]
											&& selectedItem[action.readAmountAndTotalMethod](value)){
									
									let amountAndTotal=selectedItem[action.readAmountAndTotalMethod](value);
									let amount=amountAndTotal.amount;
									let total=amountAndTotal.total;
									
									let percent=Math.round((amount/total)*100);
									// To display a gauge :
									fieldElement.style.background="linear-gradient(90deg, "+
													nonull(gameConfig.controlsPanelStyle.gaugeForeColor,"#444444")
												+" "+percent+"%, "+
												nonull(gameConfig.controlsPanelStyle.gaugeBackColor,"transparent")
												+" "+percent+"%)";
									fieldElement.style.border="solid 1px";
									fieldElement.style.width="32%";
									fieldElement.style.marginLeft="2px";
									
									label.innerHTML+=" ("+amount+"/"+total+")";
									label.style.width="90%";
									

								}

								
							});
							
							
							// We automatically add a line break separator :
							{
								let lineBreak=document.createElement("span");
								lineBreak.style="display:flex;flex-grow:1;width:100%;height:10px;";
								actionsElement.appendChild(lineBreak);
							}

							
						}else if(contains(action.type,"number")){
							
							// We automatically add a separator :
							{
								let lineBreak=document.createElement("span");
								lineBreak.style="display:flex;flex-grow:1;width:100%;height:10px;";
								actionsElement.appendChild(lineBreak);
							}
							
							if(action.label){
								const label=document.createElement("label");
								label.style.display="inline-block";
								label.innerHTML=action.label;
								actionsElement.appendChild(label);
							}
							
							const input=document.createElement("input");
							const min=action.value.min;
							const max=action.value.max;
							const step=(action.steps && !empty(action.steps)?action.steps[0]:1);
							input.id="input_"+methodName;
							input.className="number";
							input.type="number";
							input.value=0;
							input.min=min;
							input.max=max;
							input.step=step;
							actionsElement.appendChild(input);
							
							const inputConfig={min:min, max:max, step:step};
						
							// TODO : FIXME : DUPLICATED CODE !
							if(contains(action.type,"inactive") || action.inactive){
								input.disabled=true;
								input.style.opacity=DISABLED_OPACITY;
							}else{
								input.onchange=function(event){
									controller.executeActionOnSelection(event,methodName,inputConfig);
								};
							}
							
							// TODO : FIXME : DUPLICATED CODE !
							if(action.readCheckedStateForValueMethod
									&& selectedItem[action.readCheckedStateForValueMethod]){
								input.value=selectedItem[action.readCheckedStateForValueMethod]();
							}

							
							// We automatically add a separator :
							{
								let lineBreak=document.createElement("span");
								lineBreak.style="display:flex;flex-grow:1;width:100%;height:10px;";
								actionsElement.appendChild(lineBreak);
							}

						}
						
						
						
					},(actionInfo)=>{
						if(nothing(actionInfo.visibilityCondition))	return true;
						if(actionInfo.visibilityCondition===false)	return false;
						if(actionInfo.visibilityCondition===true)		return true;
						if(!isString(actionInfo.visibilityCondition) || !selectedItem[actionInfo.visibilityCondition])	return true;
						return selectedItem[actionInfo.visibilityCondition]();
					});
					
				}
				
				setOnlyVisible(self.infoLayer,element,true);

			},
			
			
	};
	
	return self;
}




