/* ## Utility gamu methods - model part
 *
 * This set of methods gathers utility game-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils-gamu» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 *  
 */

//======================== Global constants ======================== 

const ABSTRACT_PREFIX="abstract_";
const SAVE_STRING_MAX_CHARACTERS=50000;

//======================== Conditions utility methods ======================== 


/*public*//*static*/function isCardinalityCollection(cardinality){
	let value=cardinality.value;
	// TRACE
	if(!value){
		lognow("WARN : Cardinality malformed !",cardinality);
		return false;
	}
	if(value==="1" || value===1)	return false;
	if(value=="staticGroups" || value=="oneGroupAtRandom")	return true;
	return true;
};


/*public*//*static*/function evaluateCondition(self, refObject, conditionFunctionCall, camera, callback){
	
	// Function call :
	if(!conditionFunctionCall.attributeName){
		
		let calculusFunction=refObject[conditionFunctionCall.functionName];
		if(calculusFunction){
		
			let args=conditionFunctionCall.args;
			
			
			let amount=calculusFunction(refObject, camera, args);
			let target=conditionFunctionCall.target;

			if(amount!=null && target!=null){
				
				self.reachAmount=amount;
				self.reachTarget=target;
				
				let comparisionType=conditionFunctionCall.comparisionType;
				if(  (comparisionType==="<" && amount<target)
					|| (comparisionType===">" && amount>target)
					|| (comparisionType==="<=" && amount<=target)
					|| (comparisionType===">=" && amount>=target)
					|| (comparisionType==="==" && amount==target)
				){
					callback();	// if we have isTriggerableSeveralTimes==true then we replace the result.
				}
				
			}

		}
		
		
	}else{
		
		// Attribute value verification :

		let attribute=refObject[conditionFunctionCall.attributeName];
		let comparisionType=conditionFunctionCall.comparisionType;
		let targetValue=conditionFunctionCall.target;
		
		if(targetValue){
			// Attribute has value verification :
			if(  (comparisionType==="==" && attribute==targetValue)
				|| (comparisionType==="!=" && attribute!=targetValue) ){
				callback();	// if we have isTriggerableSeveralTimes==true then we replace the result.
			}
			
		}else{
			// Attribute is null verification :
			
			if(  (comparisionType==="==" && attribute==null)
				|| (comparisionType==="!=" && attribute!=null) ){
				callback();	// if we have isTriggerableSeveralTimes==true then we replace the result.
			}
		}
		
	}
	
	
}



//======================== Level utility methods ======================== 



// GAME LEVELS

function getLevelsManager(allPrototypesConfigRoot, allPrototypesStorageKey, controller, model) {

	// DBG (REMOVE WHEN FINISHED TO ALLOW TO RESUME A PREVIOUS GAME!)
//	const DEBUG_MODE_ALWAYS_CREATE_NEW_GAME=true;

	const ALL_PROTOTYPES_CONFIG_ORIGINAL=allPrototypesConfigRoot.allPrototypes;
	const LEVELS_CONFIG_ORIGINAL=ALL_PROTOTYPES_CONFIG_ORIGINAL["GameLevel"];
	
//	// DEBUG ONLY
//	ALL_PROTOTYPES_CONFIG_ORIGINAL["aaaa"]="ORIGINAL !";
	
//	// OLD :	const allPrototypesConfigClone=cloneObjectShallow(allPrototypesConfigRoot).allPrototypes;


	
	

	let self={
		
		currentLevelName:null,
		
//		modeSavingLoading:"MANUAL",// TODO : FIXME : "auto" mode does not work on today (16/08/2020) for saving all the instantiated objects.

		refAttributesByClassName:{},
		refSubObjectsByClassName:{},
		
		// UNUSED : FOR "AUTO" MODE WHICH DOES NOT WORK, ONLY (FIXME)
		/*private*/populateWholeReferenceCurrentObjectConfig:function(allPrototypesConfigParam, referenceCurrentObjectProtoConfig=null, className=null){
			// If we have no class name, then we are at the root of the prototypes config :
			if(!referenceCurrentObjectProtoConfig || !className){
				foreach(allPrototypesConfigParam,(prototypeClassDefinition, prototypeClassDefinitionName)=>{
					foreach(prototypeClassDefinition,(prototypeDefinition, prototypeName/*UNUSED*/)=>{
						self.populateWholeReferenceCurrentObjectConfig(allPrototypesConfigParam, prototypeDefinition, prototypeClassDefinitionName);
					});
				},
				(prototypeClassDefinition, prototypeClassDefinitionName)=>{	
					// If we have a simple attribute, then we skip :
					return startsWithUpperCase(prototypeClassDefinitionName);
				});
				return;
			}
			
			let superPrototypeName=referenceCurrentObjectProtoConfig.usesSuper;
			if(!superPrototypeName)	return;
			// If we don't find the specified super prototype, then we skip and don't do anything :
			let superProto=allPrototypesConfigParam[className][superPrototypeName];
			if(!superProto){
				// TRACE
				lognow("WARN : No super prototype found in global config for class name «"+className+"» and super prototype name «"+superPrototypeName+"»");
				return;
			}
			// If we have a usesSuper directive and we have found the corresponding super prototype
			// then we have to go look for the super prototype and copy its attributes :
			self.populateWholeReferenceCurrentObjectConfig(allPrototypesConfigParam, superProto, className);
			foreach(superProto,(attr, attrName)=>{
				// For each attribute of the super prototype, we copy it in the current prototype :
				referenceCurrentObjectProtoConfig[attrName]=attr;
			});
		},
		
		
		
		

		
		
		/*private*/getObjectPrototype:function(modelObjectParam, prototypeConfigClassName, prototypeName=null, allPrototypesConfigCloneParam){
			let result=null;
			if(!prototypeConfigClassName)	return result;
			if(prototypeConfigClassName==="GameLevel"){
				// We replace the current level prototype :
				if(prototypeName){
					result=allPrototypesConfigCloneParam[prototypeConfigClassName][prototypeName];
				}else{// case direct definition
					result=modelObjectParam.directDefinitionOriginalPrototypeConfig;
				}
			}else{
				if(prototypeName){
					result=allPrototypesConfigCloneParam[prototypeConfigClassName][prototypeName];
				}else{
					result=modelObjectParam.directDefinitionOriginalPrototypeConfig;
					if(isArray(result) && !empty(result)){
						result=result[result.length-1]; // (we take the last item)
					}
				}
			}
			return result;
		},
		
		// 1)
		// This method will only calculate the classes attributes names :
		/*private*/populateAllPrototypesFields:function(modelObjectParam, allPrototypesConfigCloneParam, parentRefConfig, visited=[]){
			
			if(!self.isAutoInstantiatedAttribute(modelObjectParam))	return;
			if(contains(visited, modelObjectParam))		return;
			
			const prototypeConfigClassName=getClassName(modelObjectParam);
			const prototypeName=modelObjectParam.prototypeName;
			
			// Do pre-treatment :
			// We create a parallel ref config with only direct class object declarations :
			let objectPrototype=self.getObjectPrototype(modelObjectParam, prototypeConfigClassName, prototypeName, allPrototypesConfigCloneParam);

			

			let refAttributesNames=[];
			let refSubObjectsNames=[];
			foreach(objectPrototype, (refAttr, refAttrName)=>{
				if(!startsWithUpperCase(refAttrName))	refAttributesNames.push(refAttrName);
				else 																	refSubObjectsNames.push(refAttrName);
			});
			
			
			if(!empty(refAttributesNames)) {
				if(!self.refAttributesByClassName[prototypeConfigClassName])	self.refAttributesByClassName[prototypeConfigClassName]=[];
				self.refAttributesByClassName[prototypeConfigClassName]=merge(self.refAttributesByClassName[prototypeConfigClassName], refAttributesNames);
			}
			// We use the last updated reference :
			refAttributesNames=self.refAttributesByClassName[prototypeConfigClassName];

			
			if(!empty(refSubObjectsNames)) {
				if(!self.refSubObjectsByClassName[prototypeConfigClassName])	self.refSubObjectsByClassName[prototypeConfigClassName]=[];
				self.refSubObjectsByClassName[prototypeConfigClassName]=merge(self.refSubObjectsByClassName[prototypeConfigClassName], refSubObjectsNames);
			}
			// We use the last updated reference :
			refSubObjectsNames=self.refSubObjectsByClassName[prototypeConfigClassName];
			
			
			// End of pre-treatment.
			visited.push(modelObjectParam);
			
			foreach(modelObjectParam, (modelObjectAttr, modelObjectAttrName)=>{
				
				if(modelObjectAttrName==="parent")	return "continue";
				
				// Now for this instantiated object we want to save, we have all the fields we want to put in the ref config direct definition :
				// Case simple attributes : DO NOTHING
				if(!contains(refAttributesNames, modelObjectAttrName)
					&& modelObjectAttr
					&& self.isAutoInstantiatedAttribute(modelObjectAttr/*works with arrays, too, because javascript arrays can hold extra attributes*/)){
					// Case class objects or arrays :
					let classObjectAttributeNameInPrototype=capitalize(modelObjectAttrName);
					if(!isArray(modelObjectAttr)){ // SINGLE OBJECT
						if(contains(refSubObjectsNames,classObjectAttributeNameInPrototype)){
							// RECURSIVE CALL
							self.populateAllPrototypesFields(modelObjectAttr, allPrototypesConfigCloneParam, objectPrototype, visited);
						}
					}else{ // ARRAY
						const length=classObjectAttributeNameInPrototype.length;
						classObjectAttributeNameInPrototype=classObjectAttributeNameInPrototype.substring(0,length-1); // We remove the final «s» character.
						if(contains(refSubObjectsNames,classObjectAttributeNameInPrototype)){
							foreach(modelObjectAttr, (item)=>{
								// RECURSIVE CALL
								self.populateAllPrototypesFields(item, allPrototypesConfigCloneParam, objectPrototype, visited);
							});
						}
					}
				}
			});
			
			
		},

		// TODO : FIXME : KINDA DUPLICATED CODE !
		// 2)
		/*private*/appendDirectDeclarations:function(modelObjectParam, allPrototypesConfigCloneParam, parentRefConfig, visited=[]){
			
			if(!self.isAutoInstantiatedAttribute(modelObjectParam))	return modelObjectParam;
			if(contains(visited, modelObjectParam))		return null;
			
			const prototypeConfigClassName=getClassName(modelObjectParam);
			
			const prototypeName=modelObjectParam.prototypeName;
			
			// Do pre-treatment :
			// We create a parallel ref config with only direct class object declarations :
			let newDirectDefinition={};
			let objectPrototype=self.getObjectPrototype(modelObjectParam, prototypeConfigClassName, prototypeName, allPrototypesConfigCloneParam);
			
			// DOES NOT WORK :
			// NEW prototypeName setting :
//			// prototypeName, inheritedPath and usesSuper are useful to get the images paths contexts !
//			// We set the prototype name :
//			newDirectDefinition.prototypeName=prototypeName;
//			if(objectPrototype){
//				newDirectDefinition.inheritedPath=objectPrototype.inheritedPath;
//			}
			
			
			if(prototypeConfigClassName==="GameLevel"){
				// We replace the current level prototype :
				if(prototypeName){
					allPrototypesConfigCloneParam[prototypeConfigClassName][prototypeName]=newDirectDefinition;
				}else{
					// case direct definition
					if(!isArray(objectPrototype)){
						parentRefConfig[prototypeConfigClassName]=newDirectDefinition;
					}else{
						parentRefConfig[prototypeConfigClassName].push(newDirectDefinition);
					}
				}
			}
			
			
			// We get the previously calculated class attributes names :
			let refAttributesNames=self.refAttributesByClassName[prototypeConfigClassName];
			let refSubObjectsNames=self.refSubObjectsByClassName[prototypeConfigClassName];
			
			// End of pre-treatment.
			visited.push(modelObjectParam);
			
			foreach(modelObjectParam, (modelObjectAttr, modelObjectAttrName)=>{
				
				if(modelObjectAttrName==="parent")	return "continue";
				
				// Now for this instantiated object we want to save, we have all the fields we want to put in the ref config direct definition :
				if(contains(refAttributesNames, modelObjectAttrName)){ // Here we are supposed to have only native typed objects :
					// Case simple attributes :
					let refFieldName=modelObjectAttrName;
					let attributeValue=modelObjectParam[refFieldName];
					newDirectDefinition[refFieldName]=attributeValue;
				
				}else if(modelObjectAttr 
							&& self.isAutoInstantiatedAttribute(modelObjectAttr/*works with arrays, too, because javascript arrays can hold extra attributes*/)){
					// Case class objects or arrays :
					let classObjectAttributeNameInPrototype=capitalize(modelObjectAttrName);
					if(!isArray(modelObjectAttr)){ // SINGLE OBJECT
						
						if(contains(refSubObjectsNames,classObjectAttributeNameInPrototype)){
							// RECURSIVE CALL
							let directDefinition=self.appendDirectDeclarations(modelObjectAttr, allPrototypesConfigCloneParam, objectPrototype, visited);
							newDirectDefinition[classObjectAttributeNameInPrototype]=directDefinition;
						}
					}else{ // ARRAY
						const length=classObjectAttributeNameInPrototype.length;
						classObjectAttributeNameInPrototype=classObjectAttributeNameInPrototype.substring(0,length-1); // We remove the final «s» character.
						if(contains(refSubObjectsNames,classObjectAttributeNameInPrototype)){
							let directDefinition=[];
							foreach(modelObjectAttr, (item)=>{
								// RECURSIVE CALL
								let itemDefinition=self.appendDirectDeclarations(item, allPrototypesConfigCloneParam, objectPrototype, visited);
								directDefinition.push( itemDefinition );
							});
							newDirectDefinition[classObjectAttributeNameInPrototype]=directDefinition;
						}
					}
				}
			});
			
			
			
			// Technical attributes:
			// We have to keep the «subClass» attribute all times :
			newDirectDefinition.subClass=prototypeConfigClassName;
			// DBG
			if(!objectPrototype){
				lognow("ERROR : Could not instanciate object with prototype name «"+prototypeName+"».");
			}
			
			// OLD prototypeName setting :
			// prototypeName, inheritedPath and usesSuper are useful to get the images paths contexts !
			newDirectDefinition.prototypeName=prototypeName;
			if(objectPrototype){
				newDirectDefinition.inheritedPath=objectPrototype.inheritedPath;
			}
			
			
			// SPECIAL CASE : Here we have no usesSuper, because we are in a direct definition, but we have an inherited path :
			// OLD : newDirectDefinition.usesSuper=objectPrototype.usesSuper;
			delete newDirectDefinition.usesSuper;
			if(modelObjectParam.position){
			  let p=modelObjectParam.position;
			  let x=p.x, y=p.y, z=p.z;
			  let center=p.center;
			  let centerOffsets=p.centerOffsets;
				newDirectDefinition["overridingPosition"]=new Position(null,x,y,z,center,null,centerOffsets);
			}
			
			return newDirectDefinition;
		},
		
		
		/*private*//*static*/isAutoInstantiatedAttribute:function(modelObjectParam){
			if(!modelObjectParam)	return false;
			return modelObjectParam.isAutoInstantiated;
		},

		
		/*public*/extractStaticConfig:function(allPrototypesConfigOriginal, modelRoot){
			
			let allPrototypesConfigClone=clone(allPrototypesConfigOriginal);
			
			
			// First of all, we complete the cloned reference config :
			self.populateWholeReferenceCurrentObjectConfig(allPrototypesConfigClone);
				
			// UNUSED
//			const isInPrototypesConfig=function(prototypesConfig, prototypeConfigClassName){
//				if(prototypesConfig[prototypeConfigClassName])	return true;
//				let result=false;
//				foreach(prototypesConfig,(prototypeConfig)=>{
//					if(prototypeConfig[prototypeConfigClassName]){
//						result=true;
//						return "break";
//					}
//				});
//				return result;
//			};
			
			// First we list all the prototypes fields :
			self.populateAllPrototypesFields(modelRoot, allPrototypesConfigClone, allPrototypesConfigClone);

			
//			// DBG
//			lognow("!!!self.refAttributesByClassName:",self.refAttributesByClassName);
//			lognow("!!!self.refSubObjectsByClassName:",self.refSubObjectsByClassName);
			
			
			// Then we browse through all the model :
			self.appendDirectDeclarations(modelRoot, allPrototypesConfigClone, allPrototypesConfigClone);
			
			
			return allPrototypesConfigClone;
		},
		
		
		/*public*/saveGame:function(){
			
			if(!model.currentLevel){
				// TRACE
				lognow("WARN : Cannot save game current level from model : ", model);
				return;
			}
			
			

			let extractedAllPrototypesConfig;
//			if(self.modeSavingLoading==="AUTO"){
			extractedAllPrototypesConfig=self.extractStaticConfig(ALL_PROTOTYPES_CONFIG_ORIGINAL, model.currentLevel);
//			}else{
//				// "MANUAL" MODE :
//				extractedAllPrototypesConfig={};
//			}
			
			model.saverLoader.saveState(extractedAllPrototypesConfig, allPrototypesStorageKey);
		},
		
		
		startLevelIfNoCurrent:function(pageName, /*NULLABLE*/levelName, restoreLevelIfPresent=false, fileInputElement=null) {

			self.currentLevelName=levelName;
			if(contains(self.currentLevelName,"()")){
				let levelMethodName=self.currentLevelName.replace("()","");
				if(!nothing(controller[levelMethodName])){
					self.currentLevelName=controller[levelMethodName].apply(controller,[pageName]);
				}
			}
			
			let currentLevelConfig=null;
			
			
			
			let startLevelPromise;
			

			let mustLoad=false;
			let referenceCurrentLevelConfig;
			if(model.saverLoader.hasData(allPrototypesStorageKey) && restoreLevelIfPresent && fileInputElement) { // If we must load :


//			if(self.modeSavingLoading==="AUTO"){
				// UNUSED : FOR "AUTO" MODE WHICH DOES NOT WORK, ONLY (FIXME)
				// We get the saved current level state if it exists  :
				startLevelPromise=model.saverLoader.loadState(fileInputElement, allPrototypesStorageKey).then((allInstanciatedSavedPrototypes)=>{

					currentLevelConfig=allInstanciatedSavedPrototypes["GameLevel"][self.currentLevelName];
					model.currentLevel=model.createLevelState(self.currentLevelName, currentLevelConfig, allInstanciatedSavedPrototypes);
	//			}else{
	//				// "MANUAL" MODE :
	//				model.currentLevel={};
	//			}
					
					// DBG
					lognow("Loaded model.currentLevel:",model.currentLevel);
				
				});
				
				
				
				
			}else{
				
				startLevelPromise=new Promise((onComplete,onError)=>{
				
					const referenceCurrentLevelConfig=LEVELS_CONFIG_ORIGINAL[self.currentLevelName];
	
					// ...else we start a new state for this current level :
					// (randomized, at this point)
					model.currentLevel=model.createLevelState(self.currentLevelName, referenceCurrentLevelConfig);
					
					
					// TODO : FIXME : Also in loading currentLevel ?
					
					// We set the default selection if needed :
					if(referenceCurrentLevelConfig.selectedItems){
						let selectedItems=referenceCurrentLevelConfig.selectedItems;
						if(contains(selectedItems,"()")){
							let methodName=selectedItems.replace("()","");
							let selectedObject=controller[methodName].apply(controller);
							
							// TODO : Make so it can handle multiple selection :
							// CAUTION : On today, only handle single-element selection  :
	//					if(isArray(selectedObject))
							controller.selectionManager.setSelected(selectedObject);
	//					else
	//						controller.selectionManager.setSelected([selectedObject]);
						}
					}else if(referenceCurrentLevelConfig.selectedItem){
						let selectedItem=referenceCurrentLevelConfig.selectedItem;
						if(contains(selectedItem,"()")){
							let methodName=selectedItem.replace("()","");
							let selectedObject=controller[methodName].apply(controller);
							controller.selectionManager.setSelected(selectedObject);
						}
					}
					
					onComplete();
				});
				
			}
			
			
			
			// We launch the current level with the properly setted model :
			startLevelPromise.then(()=>{return controller.startLevel();}).then(()=>{ // cf. GameScreen.startLevel() method.

				// We init all the objects once all the model has been loaded :
				initAllChildrenAfterLevelLoading(model.currentLevel);
			
			});
			
			


			
		}
	};

	
	// We duplicate the current level config into the levels manager :s
	foreach(LEVELS_CONFIG_ORIGINAL,(currentLevel,key)=>{
		self[key]={
			config : LEVELS_CONFIG_ORIGINAL[key]
		};
	});
	
	

	return self;
}


// ======================== Model utility methods ======================== 

function getSaverLoader() {
	const DEFAULT_OWNER_KEY="data";

	let self= {

		// DBG
		isCompressed:false,
		saveState : function(levelObjectParam, /*NULLABLE*/ownerKey) {

			let key=DEFAULT_OWNER_KEY;
			if(!nothing(ownerKey)) {
				key=ownerKey;
			}

			// (JSON string)
			// DOES NOT WORK (anonymous objects miss their functions and methods :)
//			let savedMap=getAsFlatStructure(object);
//			let savedString=stringifyObject(savedMap);
//			savedString=LZWString.compress(savedString);
			
			// DOES NOT WORK : Stack call overflow :
//			let savedWithNoParents=self.removeParents(object);
//			let savedString=stringifyObject(savedWithNoParents);
			

			//			let savedString="{objects:[";
			//			let comma=false;
			//			for (let i=0; i < objects.length; i++) {
			//				if(comma)
			//					savedString += ",";
			//				else
			//					comma=true;
			//				let o=objects[i];
			//				savedString += stringifyObject(o);
			//			}
			//			savedString += "]}";
			
			
			// We use string compression to spare storage space and render a very little bit difficult to alter the saved game :
			
			// CAUTION : DOES NOT WORK (methods are missing) :		let levelObject=JSON.decycle(levelObjectParam);
			let decycled=JSON.stringifyDecycle(levelObjectParam);
			
			// DBG
			lognow("DECYCLED:",decycled);
			
			
			let savedString=stringifyObject(decycled);
			if(self.isCompressed)		savedString=LZWString.compress(savedString);
			
			// DBG
			lognow("SAVED MODEL STRING : ",savedString);
			
			// OLD :
//		storeString(key, savedString, false, SAVE_STRING_MAX_CHARACTERS);
			// NEW :
			downloadFile(key+".sav", savedString);
			
			storeSingleString(key+"_isPresent", "true");

		},

		loadState:function(fileInputElement,/*NULLABLE*/ownerKey) {
			let key=DEFAULT_OWNER_KEY;
			if(!nothing(ownerKey)) {
				key=ownerKey;
			}
			
			// OLD :
//		let loadedString=getStringFromStorage(key, false, true);
			// NEW :
			// CURRENT
			return readFileContent(fileInputElement).then((loadedString)=>{
				return new Promise((onComplete,onError)=>{
					
					
					if(!loadedString) return null;
					
					// (JSON string)
		
					// DOES NOT WORK (anonymous objects miss their functions and methods :)
		//			loadedString=LZWString.decompress(loadedString);
		//			let modelFlat=parseJSON(loadedString);
		//			let model=getAsTreeStructure(modelFlat);
					
					// DOES NOT WORK : Stack call overflow :
		//			let modelRaw=parseJSON(loadedString);
		//			let model=self.addParents(modelRaw);
		
					// We use string compression to spare storage space and render a very little bit difficult to alter the saved game :
					
					if(self.isCompressed)		loadedString=LZWString.decompress(loadedString);
					let modelLevelRaw=parseJSON(loadedString);
					
					
					// CAUTION : DOES NOT WORK (methods are missing) :		let recycled=JSON.recycle(modelLevelRaw);
					let recycled=JSON.parseRecycle(modelLevelRaw);
					
					// DBG
					lognow("RECYCLED:",recycled);
					
					
					
					// NEW :
					onComplete(recycled);
					
				});
			});
			
//		// OLD :
//		return recycled;
		},

//		,restoreLevel : function(levelName, levelData) {
//			let currentLevel={};
//
//			currentLevel.levelName={};
//
//			
//			// TODO : develop...!
//
//			return currentLevel;
//		}
		
		hasData:function(/*NULLABLE*/ownerKey){
			let key=DEFAULT_OWNER_KEY;
			if(!nothing(ownerKey)) {
				key=ownerKey;
			}
			
			// OLD :
//		const firstChunkName=key+"1";
//		let loadedString=getSingleStringFromStorage(firstChunkName);
			const isPresentVariableName=key+"_isPresent";
			let loadedString=getSingleStringFromStorage(isPresentVariableName);
			// OLD :
//		return loadedString && loadedString.length;
			return loadedString==="true";
		},
		
		
	};
	
	return self;
}


// MODEL INSTANCES GENERATION

///*private*/function addZonesCoordinates(zone1,zone2){
//	returun {x:nonull(zone1.x,0)+nonull(zone2.x,0), y:nonull(zone1.y,0)+nonull(zone2.y,0),
//					 w:nonull(zone1.w,nonull(zone2.w,0)), h:nonull(zone1.h,nonull(zone2.h,0)) };
//}
//
///*public*/function getSpawningZone(holder,previousHolder=null){
//	if(!previousHolder){
//		if(!holder.spawningZone.cumulative){
//			return holder.spawningZone;
//		}	else{
//			holder.calculatedCumulativeSpawningZone=holder.spawningZone;
//			return holder.spawningZone;
//		}
//	}
//
//	if(!holder.spawningZone.cumulative || !previousHolder.calculatedCumulativeSpawningZone)
//		return holder.spawningZone;
//	
//	holder.calculatedCumulativeSpawningZone=addZonesCoordinates(holder.spawningZone,previousHolder.calculatedCumulativeSpawningZone);
//	
//	return holder.calculatedCumulativeSpawningZone;
//
//}


/*public*/function getSpawningZone(holder){
	return holder.spawningZone;
}


/*public*/function getProtoNameFromConf(protoConf){
	if(protoConf.names) {
		return Math.getRandomInArray(protoConf.names);
	}
	let result={value:protoConf.name, spawningZone:getSpawningZone(protoConf)};
	
	// Custom attributes handling :
	if(protoConf.customAttributes)	result.customAttributes=protoConf.customAttributes;
	
	if(protoConf.subClass)	result.subClass=protoConf.subClass;
	return result;
}


function getInstantiationManager(prototypesConfig,gameConfig) {

	const ALL_PROTOTYPES_CONFIG=prototypesConfig.allPrototypes;
	
	
	const GAME_CONFIG=gameConfig;

	/*private*/function getNumberToPreInstanciate(cardinality){
		
		let numberToPreInstanciate=0;

		// Ratio or interval collections cardinalities :
		// (cardinality 5/10 = An object has 5 chances out of 10 scroll events to appear)
		// (cardinality 5->10 = Objects total number on display area will always be between 5 min and 10 max on scroll events)
		// TODO :
//		if(isString(cardinality.value) && contains(cardinality.value,"/")){
//			let splits=cardinality.value.split("/");
//			let numerator=parseInt(splits[0]);
//			let denominator=parseInt(splits[1]);
//			
//			// On init, it is the same as the min max interval cardinality value
//			numberToPreInstanciate=Math.getRandomInt(denominator,numerator);
//		}else 
		if(isString(cardinality.value) && contains(cardinality.value,"->")){
			
			// On init, it is the same as the chances fraction cardinality value
			numberToPreInstanciate=Math.getRandomInRange(cardinality.value);

			
		}else if(isString(cardinality.value)){
			numberToPreInstanciate=parseInt(cardinality.value);
		}else if(isNumber(cardinality.value)){
			numberToPreInstanciate=cardinality.value;
		}
	
		return numberToPreInstanciate;
	};
	
	// CAUTION : PROTOTYPE-ONLY ! (NOT DIRECT DEFINITIONS)
	/*public*/function instanciateAndAddToCollectionPrototypeOnly(
			collection, 
			classNameParam,
			prototypesInstantiation,
			// If no chosen prototype parameter is passed, then it means we want it to be calculated with the prototype instantiation information only :
			chosenPrototypeParam=null,
			allPrototypesConfig=ALL_PROTOTYPES_CONFIG,
			position={x:null,y:null,z:null}){

		let chosenPrototype=chosenPrototypeParam;
		
		let className=classNameParam;
		if(chosenPrototypeParam && chosenPrototypeParam.subClass){
			className=chosenPrototypeParam.subClass;
		}
		
		let customAttributes=null;
		
		// First OF ALL (it is prioritary on everything else) we check if we ever have to 
		// instanciate all static, fixed objects visible at this location :
		if(prototypesInstantiation.staticInstantiations){

			if(!chosenPrototype){ // Case on-move instantiation :
				
				let staticInstantiations=prototypesInstantiation.staticInstantiations;
				let foundPrototype = foreach(staticInstantiations,(staticConf)=>{
					if(staticConf.preinstanciate || isInZone(position, getSpawningZone(staticConf))){
						
						let protoNameFromConf=getProtoNameFromConf(staticConf);
						
						// Custom attributes handling :
						if(staticConf && staticConf.customAttributes)	customAttributes=staticConf.customAttributes;
						
						return protoNameFromConf;
					}
				});
				
				if(foundPrototype)	chosenPrototype=foundPrototype;

			}else{
				// Custom attributes handling :
				if(chosenPrototype.customAttributes)	customAttributes=chosenPrototype.customAttributes;
						
			}
			
		
		}else if(prototypesInstantiation.randomInstantiations){
		
			// If we want to choose from random prototypes (ie. if we have encountered no static prototype and none is provided as parameter) :
			if(!chosenPrototype){ // Case on-move instantiation :
				// We instanciate yet another single random object among array values:
				let randomConf=Math.getRandomInArray(prototypesInstantiation.randomInstantiations);
				chosenPrototype= { value:randomConf };
				
				// Custom attributes handling :
				if(randomConf && randomConf.customAttributes)	customAttributes=randomConf.customAttributes;
				
			}else{
				// Custom attributes handling :
				if(chosenPrototype.customAttributes)	customAttributes=chosenPrototype.customAttributes;
			}

		}else{
			
			// Actually equivalent to {... randomInstantiations:["<a single value>"]} )
			if(!chosenPrototype){  // Case on-move instantiation :
				// We instanciate a determined object :
				chosenPrototype= { value:prototypesInstantiation };
				
				// Custom attributes handling :
				if(prototypesInstantiation && prototypesInstantiation.customAttributes)	customAttributes=prototypesInstantiation.customAttributes;
			
		
			}else{
				// Custom attributes handling :
				if(chosenPrototype.customAttributes)	customAttributes=chosenPrototype.customAttributes;
			}
			
			

		}

		let newItem=getSingleInstanceWithPrototype(className, chosenPrototype, null, allPrototypesConfig);
		collection.push(newItem);
		
		// NO : DONE LATER ! newItem.initWithParent();
		
		// Custom attributes handling :
		if(customAttributes){
			foreach(customAttributes,(value,key)=>{newItem[key]=value;});
		}
		

		return {item:newItem, spawningZone:getSpawningZone(chosenPrototype)};
	}
	
	
	/*public*/function getSingleInstanceWithPrototype(
			classNameParam,
			chosenPrototypeParam=null,
			/*FOR DIRECT CLASSES DEFINITIONS ONLY :*/subPrototypeConfigParam=null,
			allPrototypesConfig=ALL_PROTOTYPES_CONFIG){
		
		let className=classNameParam ;

		if(chosenPrototypeParam && chosenPrototypeParam.subClass){
			className=chosenPrototypeParam.subClass;
		}
		
		// DOES NOT WORK : let instance= Reflect.construct(className,[],new.target);
		
		// TODO : FIXME : POTENTIAL VULNERABILITY !:
		let instance=instanciate(className);
		
		
		// DOES NOT WORK :
//		// NEW prototypeName setting :
//		// We set the prototype name :
//		if(!nothing(chosenPrototypeParam) && !nothing(chosenPrototypeParam.value)){
//			instance.prototypeName=chosenPrototypeParam.value;
//		}


		let prototypeConfig=subPrototypeConfigParam;
		if(!prototypeConfig && chosenPrototypeParam){
			prototypeConfig=allPrototypesConfig[className][chosenPrototypeParam.value];
		}
		
		// ------------------------------- Attributes : -------------------------------
		
		// We loop on the attributes of the prototype config :
		foreach(prototypeConfig,(attribute,key)=>{
			
			
			
			if(!startsWithUpperCase(key)){
				
				// Case plain attribute :

				let attributeName=key;
				if(attribute==null){
					instance[attributeName]=null;
				}else{
					if(attributeName==="usesSuper"){
	
						// If we use the super(parent),
						// then that means this instance prototype is basically its parent prototype :
						
						let prototypedParentObject=getSingleInstanceWithPrototype(className, {value:attribute}, null, allPrototypesConfig );
						instance=prototypedParentObject;
						instance.usesSuper=attribute;
						
					}else{
						
						// simple attribute :
						if(isString(attribute)){
							if(contains(attribute,"->")){
								// If we have a numeric random range
								instance[attributeName]=Math.getRandomInRange(attribute);
							}else if(contains(attribute,"=>")){
								// If we have a string min max range
								instance[attributeName]=Math.getMinMax(attribute,"=>");
							}else{ // Simple string :
								instance[attributeName]=attribute;
							}
						}else if(!isArray(attribute) // (to avoid a strange bug when we have something like ["1.0","1.1"] or [1,2,3]...)
								&& isNumber(parseFloat(attribute))){
							instance[attributeName]=parseFloat(attribute);
						}else if(!nothing(attribute.random)){
							// If we have an array as attribute, we choose one among all the values it holds :
							instance[attributeName]=Math.getRandomInArray(attribute.values);
						
						
						}else if(typeof(attribute)==="object" && !isArray(attribute)){ // Plain objects (only)...:
	
							// We have to clone the prototype simple object attribute :
						
							// OLD : 
							//let clonedAttribute=cloneObjectShallow(attribute,true);
							// NEW :
							let clonedAttribute=clone(attribute);
							
							instance[attributeName]=clonedAttribute;
							
						}else{ // Primitives, strings, arrays...
							
							instance[attributeName]=attribute;
							
						}
	
					}
				}
				
				
			}else{
				
				
				// Case class config attribute is an array :
				
				let attributeName=uncapitalize(key);
				let prototypedSubObject=null;
				
				// If we have a cardinality :
				if(attribute.cardinality){
				
					// If we have an array of prototypes names :
					let prototypeClassName=key;
					let prototypesConfigsObject=attribute;
					let cardinality=prototypesConfigsObject.cardinality;
					
					if(isCardinalityCollection(cardinality))	attributeName+="s";
					
					// Sub-object attribute:
					// Here, attributes holds the prototypes names array or the prototype single value
					prototypedSubObject=getInstancesWithPrototypesConfig(instance, attributeName, prototypeClassName, prototypesConfigsObject, allPrototypesConfig);
					
					
				}else {
					
					if(!isArray(attribute)){
						
						let subPrototypeClassName=key;
						if(!isString(attribute)){
						
							// Else if we have a single direct class definition :
							let subPrototypeConfig=prototypeConfig[subPrototypeClassName];
							let classNameToUse=nonull(subPrototypeConfig.subClass,subPrototypeClassName);
							prototypedSubObject=getSingleInstanceWithPrototype(classNameToUse, null, subPrototypeConfig, allPrototypesConfig);
							prototypedSubObject.directDefinitionOriginalPrototypeConfig=subPrototypeConfig;
						
						}else{ // case "«proto_...»"
						//}else if(contains(attribute,"proto_")){
						
							// ... or simplified prototype ref definition (a single object instanciated by the specified prototype) :
							let classNameToUse=subPrototypeClassName;
							let protoName=attribute;
							prototypedSubObject=getSingleInstanceWithPrototype(classNameToUse, {value:protoName}, null, allPrototypesConfig);
							
						}
						
					}else{ 
						// Case we have an array of direct class definitions :
						
						attributeName+="s";
						
						let subPrototypeClassName=key;
						
						// TODO : FIXME : DUPLICATED CODE !
						// In this case we want all the declared attributes :
						// (It's equivalent to specifying an array with all the declared prototypes names)
						let prototypesConfigsArray= prototypeConfig[subPrototypeClassName] ;
						if(!empty(prototypesConfigsArray) && prototypesConfigsArray[0]==="*") {
//							OLD : const nonAbstractObjects={};
							const nonAbstractObjects=[];
							foreach(allPrototypesConfig[ subPrototypeClassName ],(o,key)=>{	
//								OLD : nonAbstractObjects[key]=o;
								nonAbstractObjects.push(key);
							},(o,key)=>{	return !contains(key,ABSTRACT_PREFIX);	});
							prototypesConfigsArray=nonAbstractObjects;
						}
					
						prototypedSubObject=[];
						foreach(prototypesConfigsArray,(subPrototypeConfig)=>{
							
							let newItem;
							if(!isString(subPrototypeConfig)){
							
								let classNameToUse=nonull(subPrototypeConfig.subClass,subPrototypeClassName);
								newItem=getSingleInstanceWithPrototype(classNameToUse, null, subPrototypeConfig, allPrototypesConfig);
								
							}else{ // case "«proto_...»"
							//}else if(contains(attribute,"proto_")){
							
								// ... or simplified prototype ref definition (array of objects):
								let classNameToUse=subPrototypeClassName;
								let protoName=subPrototypeConfig;
								newItem=getSingleInstanceWithPrototype(classNameToUse, {value:protoName}, null, allPrototypesConfig);
								
							}
							prototypedSubObject.push(newItem);
							
						});
						
						prototypedSubObject.directDefinitionOriginalPrototypeConfig=prototypesConfigsArray;
						
						// Special case : because sub-object, here, is an array :
						// (else, the further instance.isAutoInstantiated=true; instruction will handle it)
						prototypedSubObject.isAutoInstantiated=true;
						
						
					}
				}
				
				
				// If we should, then we create a link to the parent :
				if(GAME_CONFIG.allowParentLinkForChildren){
					
					if(!isArray(prototypedSubObject)){ // Case single attribute :
						prototypedSubObject.parent=instance;

						// Abstract prototyped object are NEVER initialized :
						if(prototypedSubObject.initWithParent && !contains(prototypedSubObject.prototypeName,ABSTRACT_PREFIX)){
							prototypedSubObject.initWithParent();
						}
						
						
					}else{ // Case array attribute :
						foreach(prototypedSubObject,(subItem)=>{
							subItem.parent=instance;

							// Abstract prototyped object are NEVER initialized :
							if(subItem.initWithParent && !contains(subItem.prototypeName,ABSTRACT_PREFIX)){
								subItem.initWithParent();
							}

						});
					}
				}
				
				instance[attributeName]=prototypedSubObject;
			}
			
		},null,(i1,i2)=>{ 
			// Classes objects must be processed at the end, since they can rely on parent's simple attributes :
			let isClass1=startsWithUpperCase(i1.key);
			let isClass2=startsWithUpperCase(i2.key);
			if(!isClass1 && isClass2)	return -1; // «good order»
			if(isClass1 && !isClass2)	return 1;
			return 0;
		});

		
		// ------------------------------- Instance : -------------------------------


		// OLD prototypeName setting :
		if(!nothing(chosenPrototypeParam) && !nothing(chosenPrototypeParam.value)){
			instance.prototypeName=chosenPrototypeParam.value;
		}
		
		
		
		// Abstract prototyped object are NEVER initialized :
		if(!chosenPrototypeParam/*(case no prototype initialization)*/ || !contains(chosenPrototypeParam.value,ABSTRACT_PREFIX)){
			if(instance.init){
				instance.init(GAME_CONFIG);
			}
		}

		
		instance.isAutoInstantiated=true;

		
		return instance;
	};
	
	
	/*private*/function instanciateGroup(protoConf,						
							instanceSingleObjectOrCollection, 
							className,
							cardinality,
							allPrototypesConfig){
	
	
		let protoNameFromConf=getProtoNameFromConf(protoConf);

	
	
		// NEW :
//		// We calculate how many objects we have to preinstanciate in this collection :
		let numberToPreInstanciate=1;
		if(protoConf.value){
			if(protoConf.preinstanciate){
				numberToPreInstanciate=getNumberToPreInstanciate(protoConf);
			}else if(protoConf.spawningZone && isZoomedVisibilityZoneCollidingWithZone(gameConfig,/*DUMMY CAMERA*/{position:{x:0,y:0}},protoConf.spawningZone)){
				// We make sure that even the non-preinstanciated items that have their spawning zone overlapping with the visibility zone are present at first :
				numberToPreInstanciate=getNumberToPreInstanciate(protoConf);
			}
		}


		// OLD :
//		WORKS : 
		// We calculate how many objects we have to preinstanciate in this collection :
//		let numberToPreInstanciate=1;
//		if(protoConf.preinstanciate && protoConf.value)
//			numberToPreInstanciate=getNumberToPreInstanciate(protoConf);

	
		for(let i=0;i<numberToPreInstanciate;i++){
			instanciateAndAddToCollectionPrototypeOnly(instanceSingleObjectOrCollection, 
																		className,
																		cardinality.prototypesInstantiation,
																		protoNameFromConf,
																		allPrototypesConfig);
		}
	
	};
	
	
	// For prototypes instantiation only :
	/*private*/function getInstancesWithPrototypesConfig(
			parentObject,
			attributeName,
			classNameParam,
			prototypesConfigsObject,
			allPrototypesConfig=ALL_PROTOTYPES_CONFIG){
		
		
		let className=classNameParam;
		// The «subClass» eventual attribute in prototype class config overrides and precises the trivial (ie. parent) className : 
		if(prototypesConfigsObject && prototypesConfigsObject.subClass){
			className=prototypesConfigsObject.subClass;
		}
		
		let instanceSingleObjectOrCollection=null;
		let numberToPreInstanciate=0;

		let cardinality=prototypesConfigsObject.cardinality;
		

		if(cardinality){
			
			if(!isCardinalityCollection(cardinality)){ // case SINGLE ATTRIBUTE :
			
			
				let customAttributes=null;
				
				// UNUSEFUL (but true) : 
				// numberToPreInstanciate=1;
				
				// NO : WE DON'T PLACE THEM YET
	//			let spawningLocation={x:0,y:0};
				
				// We calculate here the chosen prototype to instanciate if we have no collection :
				let chosenPrototype=null;
				if(cardinality.prototypesInstantiation.staticInstantiations){
				
					let staticConf=null;
					if(isArray(cardinality.prototypesInstantiation.staticInstantiations)){
						if(!empty(cardinality.prototypesInstantiation.staticInstantiations)){
							// We look into the first (and only) config element :
							staticConf=cardinality.prototypesInstantiation.staticInstantiations[0];
							
							let protoNameFromConf=getProtoNameFromConf(staticConf);
							
							chosenPrototype=protoNameFromConf;
							// CAUTION : WE DON'T PLACE THEM YET
							// SO NO :	if(staticConf.spawningZone)		spawningLocation=getPositionFor2DObject(staticConf.spawningZone);
							
							
							
						}
					}else{
						staticConf=cardinality.prototypesInstantiation.staticInstantiations;
						
						let protoNameFromConf=getProtoNameFromConf(staticConf);
	
						chosenPrototype=protoNameFromConf;
						// CAUTION : WE DON'T PLACE THEM YET
						// SO NO :	if(staticConf.spawningZone)		spawningLocation=getPositionFor2DObject(staticConf.spawningZone);
					}
					
					// Custom attributes handling :
					if(staticConf && staticConf.customAttributes)	customAttributes=staticConf.customAttributes;
					
					
				} else if(cardinality.prototypesInstantiation.randomInstantiations && !empty(cardinality.prototypesInstantiation.randomInstantiations)){
					let randomConf=Math.getRandomInArray(cardinality.prototypesInstantiation.randomInstantiations);
					 
					chosenPrototype={ value:randomConf};
							
					// Custom attributes handling :
					if(randomConf && randomConf.customAttributes)	customAttributes=randomConf.customAttributes;
					
					
				} else if(isString(cardinality.prototypesInstantiation)){
				
					chosenPrototype={ value:cardinality.prototypesInstantiation };
					
					// Custom attributes handling :
					if(cardinality.prototypesInstantiation && cardinality.prototypesInstantiation.customAttributes)	customAttributes=cardinality.prototypesInstantiation.customAttributes;
					
				}
				
				if(chosenPrototype){
	
					// (we don't place them yet : cf. gamu-scenery module for further placing)
					
					instanceSingleObjectOrCollection=getSingleInstanceWithPrototype(className, chosenPrototype, null, allPrototypesConfig);
									
					// We set the cardinalities information for this single attribute :
					if(!parentObject.cardinalities)	parentObject.cardinalities={};
					parentObject.cardinalities[attributeName]=cardinality;
	
	
					// NO : WE DON'T PLACE THEM YET
	//				if(instanceSingleObjectOrCollection.position) {
	//					instanceSingleObjectOrCollection.position.setLocation(spawningLocation);
	//					if(parentObject.position)	instanceSingleObjectOrCollection.position.setParentPosition(parentObject.position);
	//				}
					
					// Custom attributes handling :
					if(customAttributes){
						foreach(customAttributes,(value,key)=>{instanceSingleObjectOrCollection[key]=value;});
					}
					
					
				}

				
				
			}else{ // case COLLECTION ATTRIBUTE :
				
				
				/* !!! CAUTION !!!  
				 * WHEN WE ARE NOT IN «preinstanciate» MODE, 
				 * «cardinality» REPRESENTS THE NUMBER OF ELEMENTS AT THE SAME TIME INSTANCIATED IN *DISPLAY AREA* !!!
				 * WHEN WE ARE IN «preinstanciate» MODE, 
				 * IT REPRESENT THE *TOTAL* AMOUNT OF INSTANCIATED OBJECTS IN THE WHOLE MODEL !!!
				 */
				
				
				// We set the cardinalities information for each collection attribute :
				if(!parentObject.cardinalities)	parentObject.cardinalities={};
				parentObject.cardinalities[attributeName]=cardinality;
	
				
				if(cardinality.value==="fill"){
					
					instanceSingleObjectOrCollection=[];
					const MAX=999;
					
					let spawningZone = getSpawningZone(cardinality);
	
					const gameConfig=GAME_CONFIG;
					if(contains(gameConfig.projection, "2D")) {
					
						// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
						if(contains(gameConfig.scroll, "horizontal")) {
							
							// 0- We instanciate just enough to fill the width :
							// (we don't place them yet : cf. gamu-scenery module for further placing)
							let inverseZoomX=1/gameConfig.zooms.zx;
							let totalWidth=Math.floor( GAME_CONFIG.getResolution().w * inverseZoomX);
				
							let fillInstantiationMargin= Math.floor( nonull(cardinality.fillInstantiationMargin,0) * inverseZoomX );
							
							let widthCount=0;
							for(let i=0;i<MAX && widthCount < totalWidth + fillInstantiationMargin;i++){
	
								// We instanciate the object randomly :
								let itemAndZone=instanciateAndAddToCollectionPrototypeOnly(instanceSingleObjectOrCollection,
																															className,
																															cardinality.prototypesInstantiation,
																															null,
																															allPrototypesConfig);
								let elementInstance=itemAndZone.item;
								if(!spawningZone)	spawningZone=elementInstance.spawningZone;
								
								widthCount+= (elementInstance.size.w + (spawningZone?nonull(spawningZone.w,0):0))  ;
								
							}
							
						}
					}
					
				}else if(cardinality.value==="staticGroups"){ // Static groups cardinality :
					
					
					instanceSingleObjectOrCollection=[];
					
					// (we don't place them yet : cf. gamu-scenery module for further placing)
					let protos=cardinality.prototypesInstantiation;
					if(protos.staticInstantiations){
						
						foreach(protos.staticInstantiations,(protoConf)=>{
						
							// NEW :
							instanciateGroup(protoConf,
								instanceSingleObjectOrCollection, 
								className,
								cardinality,
								allPrototypesConfig);
							
							
//							// OLD :
//							let protoNameFromConf=getProtoNameFromConf(protoConf);
//							// We calculate how many objects we have to preinstanciate in this collection :
//							numberToPreInstanciate=1;
//							if(protoConf.preinstanciate && protoConf.value)
//								numberToPreInstanciate=getNumberToPreInstanciate(protoConf);
//							for(let i=0;i<numberToPreInstanciate;i++){
//								instanciateAndAddToCollectionPrototypeOnly(instanceSingleObjectOrCollection, 
//																							className,
//																							cardinality.prototypesInstantiation,
//																							protoNameFromConf,
//																							allPrototypesConfig);
//							}

							
						});
	
						
					}
					
					
				}else if(cardinality.value==="oneGroupAtRandom"){ // Random groups cardinality :
					
					
					instanceSingleObjectOrCollection=[];
					
					// (we don't place them yet : cf. gamu-scenery module for further placing)
					let protos=cardinality.prototypesInstantiation;
					if(protos.randomInstantiations){
						
						let protoConf=Math.getRandomInArray(protos.randomInstantiations);
						
						// NEW :
						instanciateGroup(protoConf,
							instanceSingleObjectOrCollection, 
							className,
							cardinality,
							allPrototypesConfig);
						
						
//						// OLD :
//						let protoNameFromConf=getProtoNameFromConf(protoConf);
//						// We calculate how many objects we have to preinstanciate in this collection :
//						numberToPreInstanciate=1;
//						if(protoConf.preinstanciate && protoConf.value)
//							numberToPreInstanciate=getNumberToPreInstanciate(protoConf);
//						for(let i=0;i<numberToPreInstanciate;i++){
//							instanciateAndAddToCollectionPrototypeOnly(instanceSingleObjectOrCollection,
//																						className,
//																						cardinality.prototypesInstantiation,
//																						protoNameFromConf,
//																						allPrototypesConfig);
//						}
						
					}
					
					
				}else{ // Ratio or interval collections cardinalities :
					
					
					instanceSingleObjectOrCollection=[];
					let numberToPreInstanciate=getNumberToPreInstanciate(cardinality);
					// We instanciate the objects randomly or not according if we have to :
					for(let i=0;i<numberToPreInstanciate;i++){
						// If no chosen prototype parameter is passed, then it means we want it to be calculated with the prototype instantiation information !
						instanciateAndAddToCollectionPrototypeOnly(instanceSingleObjectOrCollection,
																				  className,
																				  cardinality.prototypesInstantiation,
																				  null,
																				  allPrototypesConfig);
					}
					
				}
				
				// Special case : because sub-object, here, is an array :
				instanceSingleObjectOrCollection.isAutoInstantiated=true;
				
			}
		

		}else{ // Cases direct definitions :
			
			
			if(!isArray(prototypesConfigsObject)){
				
				if(!isString(prototypesConfigsObject)){
					// Case single object direct definition :
				
					let classNameToUse=nonull(prototypesConfigsObject.subClass, className);
					instanceSingleObjectOrCollection=getSingleInstanceWithPrototype(classNameToUse, null, prototypesConfigsObject, allPrototypesConfig);
				
				}else{ // case "«proto_...»"
				//}else if(contains(prototypesConfigsObject,"proto_")){
				
					// ... or simplified prototype ref definition (single object) :
					let classNameToUse=className;
					let protoName=prototypesConfigsObject;
					instanceSingleObjectOrCollection=getSingleInstanceWithPrototype(classNameToUse, {value:protoName}, null, allPrototypesConfig);

				}
			
			
			}else{
				
				// Case array of direct definitions :
				
				instanceSingleObjectOrCollection=[];

				// TODO : FIXME : DUPLICATED CODE !
				// In this case we want all the declared attributes :
				// (It's equivalent to specifying an array with all the declared prototypes names)
				let prototypesConfigsArray= prototypesConfigsObject ;
				if(!empty(prototypesConfigsArray) && prototypesConfigsArray[0]==="*") {
//					OLD : const nonAbstractObjects={};
					const nonAbstractObjects=[];
					foreach(allPrototypesConfig[ className ],(o,key)=>{
//						OLD : nonAbstractObjects[key]=o;
						nonAbstractObjects.push(key);
					},(o,key)=>{	return !contains(key,ABSTRACT_PREFIX);	});
					prototypesConfigsArray=nonAbstractObjects;
				}
				
				foreach(prototypesConfigsArray,(itemConfig)=>{
					
					let newItem;
					if(!isString(itemConfig)){
	
						let classNameToUse=nonull(itemConfig.subClass,className);
						newItem=getSingleInstanceWithPrototype(classNameToUse, null, itemConfig, allPrototypesConfig);
					
					}else{ // case "«proto_...»"
					//}else if(contains(itemConfig,"proto_")){
						
						// ... or simplified prototype ref definition (array of objects):
						let classNameToUse=className;
						let protoName=itemConfig;
						newItem=getSingleInstanceWithPrototype(classNameToUse, {value:protoName}, null, allPrototypesConfig);
						
					}

					instanceSingleObjectOrCollection.push(newItem);
					
					
				});

			}
		}

		
		return instanceSingleObjectOrCollection;
	};
	



	
	



	// ---------------------------------------------------
	
	

	
	// The instantiation manager object instance :
	let self= {
		getSingleInstanceWithPrototype:getSingleInstanceWithPrototype,
		instanciateAndAddToCollectionPrototypeOnly:instanciateAndAddToCollectionPrototypeOnly,
		
		createLevelState : function(levelName, levelConfig, allPrototypesConfig=ALL_PROTOTYPES_CONFIG) {
			
						
//			let currentLevel={
			// We store the current level config inside the current level :
//						config: levelConfig,
//						isCinematic: levelConfig.isCinematic, 
//						limits: levelConfig.limits,
//						levelName:levelName,
//			};
			
			let currentLevel=new GameLevel(levelConfig, levelConfig.isCinematic, levelConfig.limits, levelName);


			// Level is the absolute root object of the model (so it has a special treatment) :
			foreach(levelConfig,(configItem,className)=>{

				if(!startsWithUpperCase(className))	return "continue";
				
				// If we have a class definition : 
				
				// We get the prototype for this root class name :
				let prototypeLevelConfigObject=levelConfig[className];
				

				let attributeName=uncapitalize(className);
				let cardinality=prototypeLevelConfigObject.cardinality;
				if(cardinality){
					// If we have a cardinality :
					if(isCardinalityCollection(cardinality))	
						attributeName+="s";
				}else{
					// cases direct definitions :
					if(isArray(configItem))
						attributeName+="s";
					// ... or simplified prototype ref definition :
				}
				
				currentLevel[attributeName]=getInstancesWithPrototypesConfig(currentLevel, attributeName, className, prototypeLevelConfigObject, allPrototypesConfig);
				
			});
			
			currentLevel.isAutoInstantiated=true;


			
			
			return currentLevel;
		},
	};

	
	
	return self;
}



/*private*/function initAllChildrenAfterLevelLoading(container,visitedChildren=[]){
	if(isPrimitive(container))	return;
	
	foreach(container,(child,childNameOrIndex)=>{
		if(!child || contains(visitedChildren,child))	return "continue";
		if(child.initAfterLevelLoading)	child.initAfterLevelLoading();
		visitedChildren.push(child);
		initAllChildrenAfterLevelLoading(child,visitedChildren);
	},(child)=>!isFunction(child));
};


/* THERE ARE 4 POSSIBLE INIT FUNCTIONS : IN THIS ORDER :
*** At level launching :
1) init(gameConfig)
2) initWithParent()
3) initAfterLevelLoading()
*** Can be anytime during the game launch (for ongoing instanciations) :
4) initAfterSetPositionFirstTime()
*/










