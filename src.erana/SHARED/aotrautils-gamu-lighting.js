/* ## Utility gamu methods - view part : Lighting part
 *
 * This set of methods gathers utility game-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils-gamu» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 *  
 */


// ======================== View : Lighting utility methods ========================

// LIGHTING


/*private*/function getLightsManager(gameConfig){
	
	if(!gameConfig.lighting || (!gameConfig.lighting.dark && !gameConfig.lighting.bump))	return null;
	
	const PRECISION=2;// Magic number. No other value seems to work!
	const BUMP_SHADOW_GRAY_VALUE=64;
	const BUMP_SHADOW_WHITE_VALUE=255;
	
	var self={
			
			
			gameConfig:gameConfig,
			allLights:[],
//		tmpCtx:document.createElement("canvas").getContext("2d"),
			lightPercent:100,
			setLightsPercent:function(lightPercent){
				self.lightPercent=lightPercent;
			},
			getLightsPercent:function(){
				return self.lightPercent;
			},
			registerLights:function(item){
				
				if(!item.imageConfig)	return;
				
				let projection = self.gameConfig.projection;
				if(contains(projection, "2D")) {
							
					let lights=item.imageConfig._2D.lights;
					if(lights){

						foreach(lights,(l)=>{
							
							// OLD : 
							// let light=cloneObjectShallow(l)
							//		NEW :
							let light=clone(l)
							
							let pos=item.position;
							light.x+=pos.x;
							light.y+=pos.y;
							
							self.allLights.push(light);
							
						});


					}

							
				}
				
			},
			
			
		
			/*private*/partiallyDrawImageZoomedDark(ctx,camera,sizeFactor=1,realImageCoords,darkImageToUse,normalImage=null,ignoreRaysCasting=false){
				
				if(darkImageToUse){
					if(!darkImageToUse.width || !darkImageToUse.height)	return;
				}

				const drawingWidth=darkImageToUse.width*realImageCoords.scaleW;
				const drawingHeight=darkImageToUse.height*realImageCoords.scaleH;
				const zooms=realImageCoords.zooms;
				const zoomedDrawingWidth=drawingWidth * zooms.zx;
				const zoomedDrawingHeight=drawingHeight * zooms.zy;
				const zoomedCenteredCoords=getZoomedCenteredZoneCoords(realImageCoords.x, realImageCoords.y, drawingWidth, drawingHeight, realImageCoords.center, realImageCoords.zooms);
				
				let opacity=Math.roundTo((1-(self.lightPercent*.01)), 2);
				ctx.save();
				ctx.globalAlpha=opacity;
				
				ctx.drawImage(darkImageToUse,
						// Clip source :
						0, 0,
						darkImageToUse.width, darkImageToUse.height,
						// Drawing destination (including scaling) :
						zoomedCenteredCoords.x, zoomedCenteredCoords.y,
						zoomedDrawingWidth, zoomedDrawingHeight,
				);
				ctx.restore();


				// TODO : FIXME : Being able to process lights even if we have no base image !

				
				// We partially draw the normal image on top of the image dark :
				if(!ignoreRaysCasting){

					// We re-draw a chunk in the clip of the regular image,
					// because on today (01/2020), inverted clipping is not supporterd in html5 canvas:
					let hasLightVisible=
						!!foreach(self.allLights,(light)=>{
							return true;
						},(light)=>{
							// CAUTION ! Coordinates for DRAWING are not the same than for CALCULUS !
							return isInZoomedVisibilityZone(self.gameConfig,camera,light);
					});
					if(hasLightVisible){
						self.partiallyDrawImageZoomed(ctx,camera,sizeFactor,realImageCoords,normalImage);
					}
				
				}
				
				
			},
			
			
			/*private*/partiallyDrawImageZoomed(ctx,camera,sizeFactor=1,realImageCoords,imageToPartiallyDraw=null
				// NO : Because on today (01/2020), inverted clipping is not supporterd in html5 canvas:
//						,invertAngles=false
					){
				let width=null;
				let height=null;
				if(imageToPartiallyDraw){
					if(!imageToPartiallyDraw.width || !imageToPartiallyDraw.height)	return;
					width=imageToPartiallyDraw.width;
					height=imageToPartiallyDraw.height;
				}

				const drawingWidth=width*realImageCoords.scaleW;
				const drawingHeight=height*realImageCoords.scaleH;
				const zooms=realImageCoords.zooms;
				const zoomedDrawingWidth=drawingWidth * zooms.zx;
				const zoomedDrawingHeight=drawingHeight * zooms.zy;
				const zoomedCenteredCoords=getZoomedCenteredZoneCoords(realImageCoords.x, realImageCoords.y, drawingWidth, drawingHeight, realImageCoords.center, realImageCoords.zooms);
				
	
				let opacity=Math.roundTo((1-(self.lightPercent*.01)), 2);
//			if(invertAngles)	opacity=1-opacity;
				
	
				// 1) We check which of the lights are visible in screen :
				foreach(self.allLights,(light)=>{
	
					let radius=light.radius;
//				if(invertAngles)	radius=999999;
					
					// We have to invert the angle, because the y axis is inverted !
					let lightAngle=-light.angle;

					// We have an arbitrary zoom factor on the light shape arc, to give a slight deepness 3D effect ! (related to z-index ?)
					let lightArc=light.arc*sizeFactor;
													
//					// DBG
//					if(sizeFactor!=1)
//					console.log("lightArc:"+lightArc);
					
					
					let startAngleRadians=Math.toRadians(Math.coerceAngle(lightAngle-lightArc/2),true);
					let endAngleRadians=Math.toRadians(Math.coerceAngle(lightAngle+lightArc/2),true);
	
					// We swap angles if they are in the wrong order :
					if(			endAngleRadians<startAngleRadians 
//						|| (startAngleRadians<endAngleRadians && invertAngles)
						){
						let tmp=endAngleRadians;
						endAngleRadians=startAngleRadians;
						startAngleRadians=tmp;
					}
	
					let drawableLightZoomedCoordinates=getDrawableZoomedIfNecessaryCoordinates2D(self.gameConfig, light, camera, null, null, zooms);
					let x=drawableLightZoomedCoordinates.x;
					let y=drawableLightZoomedCoordinates.y;

					
					if(imageToPartiallyDraw){
						ctx.save();
		
						// default is ctx.globalCompositeOperation = "source-over";
		//				ctx.globalCompositeOperation = "source-atop";
						
						ctx.globalAlpha=opacity;
										
						ctx.beginPath();
						ctx.lineTo(x, y);
						ctx.arc(x, y, radius, startAngleRadians, endAngleRadians);
						ctx.closePath();
						ctx.clip();
						
						
						// DBG
						if(imageToPartiallyDraw.colorSpace){
							// Case ImageData :
							ctx.putImageData(imageToPartiallyDraw, zoomedCenteredCoords.x, zoomedCenteredCoords.y);
						}else{
							ctx.drawImage(imageToPartiallyDraw,
									// Clip source :
									0, 0,
									imageToPartiallyDraw.width, imageToPartiallyDraw.height,
									// Drawing destination (including scaling) :
									zoomedCenteredCoords.x, zoomedCenteredCoords.y,
									zoomedDrawingWidth, zoomedDrawingHeight,
							);
						}
						
						
						ctx.restore();
					}
					
	
					
				},(light)=>{
					// CAUTION ! Coordinates for DRAWING are not the same than for CALCULUS !
					return isInZoomedVisibilityZone(self.gameConfig,camera,light);
				});
				
			
			},
			
			
			
		/*public*/drawLightsZoomedIfNeeded:function(ctx,camera,realImageCoords,sizeFactor=1,darkImage=null,bumpImage=null,normalImage=null,/*UNUSED*/normalImageToUseSize=null,ignoreRaysCasting=false){
				
				
				const zooms=realImageCoords.zooms;
				
//				let tmpCtx=self.tmpCtx;
//				if(!tmpCtx){
//					// TRACE
//					console.log("ERROR : Could not create temporary graphical context, aborting lights drawing.");
//					return;
//				}
				
			// OLD WAY :
//			let acceptedPixels=null;
				if(darkImage && self.gameConfig.lighting.dark && self.lightPercent<100){
					// OLD WAY :
//				acceptedPixels=self.useImage(ctx,camera,realImageCoords,darkImage,"dark",null);
					// OPTIMIZED WAY :
					self.partiallyDrawImageZoomedDark(ctx,camera,sizeFactor,realImageCoords,darkImage,normalImage,ignoreRaysCasting);
				// NO : Because on today (01/2020), inverted clipping is not supporterd in html5 canvas:
				//self.partiallyDrawImageZoomedDark2(ctx,camera,sizeFactor,realImageCoords,darkImage,ignoreRaysCasting);
				}
				
				if(bumpImage && self.gameConfig.lighting.bump && self.lightPercent<100 && !ignoreRaysCasting){
					// OLD WAY :
//				self.useImage(ctx,camera,realImageCoords,bumpImage,"bump",acceptedPixels);
					// OPTIMIZED WAY :
					self.partiallyDrawImageZoomed(ctx,camera,sizeFactor,realImageCoords,bumpImage);
					
				}
				
				
		},
	
	};
	
	
	return self;

}
