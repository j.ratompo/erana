/* ## Utility gamu methods - motion part
 *
 * This set of methods gathers utility game-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils-gamu» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 *  
 */


// ======================== Motion utility methods ========================




function getMoverMonoThreadedNonStop(concernedObject,startDependsOnParentOnly=false,moverConfig={liberty:"2D",refreshMillis:null,wrapping:null
//	,orientation:"horizontal"
	}){
	return getMoverMonoThreaded(concernedObject,startDependsOnParentOnly,moverConfig);
}

	
const DEFAULT_INTERRUPTABLE=true;
const DEFAULT_LIBERTY="2D";

class MoverMonoThreaded{
	constructor(concernedObject,startDependsOnParentOnly=false,moverConfig={liberty:"2D",refreshMillis:null,wrapping:null}){
		
		this.concernedObject=concernedObject;
		this.bidimensionalSpeed=null;
		this.absoluteSpeeds=nonull(moverConfig.absoluteSpeeds,{x:null,y:null}); // A null speed on an axis means no movement is asked.
		this.angularSpeed=null;
		this.interruptable=nonull(moverConfig.interruptable,DEFAULT_INTERRUPTABLE);
		this.liberty=nonull(moverConfig.liberty,DEFAULT_LIBERTY);
//	this.orientation=nonull(moverConfig.orientation,DEFAULT_ORIENTATION);
		this.wrapping=moverConfig.wrapping;
		this.destination=null;
		this.directionFactors={x:1,y:1};
		this.followers=null;
		this.refreshMillis=moverConfig.refreshMillis/*NULLABLE*/;
		
		
		// Two-states : started, stopped fr now, but could inherit three-states : started, paused, stopped from its underlying routine :	

		this.routine=getMonoThreadedRoutine(this, this.refreshMillis, startDependsOnParentOnly);

	}
	
	// Delegation :
	setDurationTimeFactorHolder(durationTimeFactorHolder){
		this.routine.setDurationTimeFactorHolder(durationTimeFactorHolder);
		return this;
	}
	setWrapping(wrappingParam){
		this.wrapping=wrappingParam;
		return this;
	}
	isStarted(){
		return this.routine.isStarted();
	}
	setDirectionFactorX(directionFactorX){
		this.directionFactors.x=directionFactorX;
		return this;
	}
	setDirectionFactorY(directionFactorY){
		this.directionFactors.y=directionFactorY;
		return this;
	}
	setAbsoluteSpeedX(absoluteSpeedX=null){
		if(!this.absoluteSpeeds)	this.absoluteSpeeds={};
		this.absoluteSpeeds.x=absoluteSpeedX;
		return this;
	}
	setAbsoluteSpeedY(absoluteSpeedY=null){
		if(!this.absoluteSpeeds)	this.absoluteSpeeds={};
		this.absoluteSpeeds.y=absoluteSpeedY;
		return this;
	}
	incrementAbsoluteSpeeds(deltas){
		if(!this.absoluteSpeeds)	return this;
		if(this.absoluteSpeeds.x!=null && deltas.x/*case value==0*/){
			const result=this.directionFactors.x*this.absoluteSpeeds.x+deltas.x;
			this.absoluteSpeeds.x=Math.abs(result);
			this.directionFactors.x=(result<0?-1:1);
		}
		if(this.absoluteSpeeds.y!=null && deltas.y/*case value==0*/){
			const result=this.directionFactors.y*this.absoluteSpeeds.y+deltas.y;
			this.absoluteSpeeds.y=Math.abs(result);
			this.directionFactors.y=(result<0?-1:1);
		}
		return this;
	}
	setBidimensionalSpeed(bidimensionalSpeed){
		this.bidimensionalSpeed=bidimensionalSpeed;
		return this;
	}
	setAngularSpeed(angularSpeed){
		this.angularSpeed=angularSpeed;
		return this;
	}
	incrementAngularSpeed(deltaAngle){
		if(!deltaAngle/*case value==0*/)	return this;
		if(this.angularSpeed==null)	this.angularSpeed=0;
		this.angularSpeed+=deltaAngle;
		return this;
	}
	setDoOnStopSecondaryFunction(doOnStopSecondaryFunction){
		this.doOnStopSecondaryFunction=doOnStopSecondaryFunction;
		return this;
	}
	setDoOnEachSecondaryFunction(doOnEachStepSecondaryFunction){
		this.doOnEachStepSecondaryFunction=doOnEachStepSecondaryFunction;
		return this;
	}
	/*private*/calculateAbsoluteSpeeds(speedsParam=null,destinationParam=null){
		let absoluteSpeedX=null;
		let absoluteSpeedY=null;

		let distanceToDestination=null;
		if(destinationParam){
			
			this.destination=destinationParam;
			
			if(this.destination.x!=null && this.destination.y!=null){
				let deltaX=this.destination.x-this.concernedObject.position.x;
				let deltaY=this.destination.y-this.concernedObject.position.y;
				distanceToDestination=Math.sqrt(Math.pow(deltaX,2)+Math.pow(deltaY,2));
			}
			
		}else if(speedsParam){
		
			if(speedsParam.x!=null)	absoluteSpeedX=Math.abs(speedsParam.x);
			if(speedsParam.y!=null)	absoluteSpeedY=Math.abs(speedsParam.y);
		}
		
		// For linear movement :
		if(this.destination && distanceToDestination!=null && this.bidimensionalSpeed){
			let ratio=this.bidimensionalSpeed/distanceToDestination;
			let deltaX=this.destination.x-this.concernedObject.position.x;
			let deltaY=this.destination.y-this.concernedObject.position.y;
			absoluteSpeedX=Math.abs(ratio*deltaX);
			absoluteSpeedY=Math.abs(ratio*deltaY);
		}else{
			if(this.absoluteSpeeds.x!=null)	absoluteSpeedX=Math.abs(this.absoluteSpeeds.x);
			if(this.absoluteSpeeds.y!=null)	absoluteSpeedY=Math.abs(this.absoluteSpeeds.y);
		}
		
		
		this.absoluteSpeeds.x=absoluteSpeedX;
		this.absoluteSpeeds.y=absoluteSpeedY;
	}
	
	doStartUntilDestinationReach(destinationParam=null, followers=null){
		
		if(this.isStarted() && this.interruptable===false)	return this.directionFactors;
		if(!destinationParam)	return this.directionFactors;
			
		this.calculateAbsoluteSpeeds(null, destinationParam);
		
		if(followers) 	this.followers=followers;
		if(this.liberty==="2D"){

				if(this.destination.x!=null){
					let x=this.concernedObject.position.x;
					if(x==this.destination.x){
						this.directionFactors.x=0;
					}else if(x<this.destination.x){
						this.directionFactors.x=1;
					}else{
						this.directionFactors.x=-1;
					}
				}

				if(this.destination.y!=null){
					let y=this.concernedObject.position.y;
					if(y==this.destination.y){
						this.directionFactors.y=0;
					}else if(y<this.destination.y){
						this.directionFactors.y=1;
					}else{
						this.directionFactors.y=-1;
					}
				}
				
				// Wrapping handling :
				if(this.wrapping){
					if(this.wrapping.x)		this.directionFactors.x=-this.directionFactors.x;
					if(this.wrapping.y)		this.directionFactors.y=-this.directionFactors.y;
				}
				
		}else{
			// TODO : DEVELOP...
		}
		
		this.routine.start();
		
		return this.directionFactors;
	}

	doStartNonStop(speedsParam={x:null,y:null}, followers=null){
		
		if(this.isStarted() && this.interruptable===false)	return;
		
		this.calculateAbsoluteSpeeds(speedsParam);
		
		if(speedsParam.x){
			if(speedsParam.x<0)	this.directionFactors.x=-1;
			else								this.directionFactors.x=1;
		}
		if(speedsParam.y){
			if(speedsParam.y<0)	this.directionFactors.y=-1;
			else								this.directionFactors.y=1;
		}

		if(followers)		this.followers=followers;
	
		this.routine.start();
	}
	
	stop(){
		this.routine.stop();
	}
	doOnStop(){
		if(this.concernedObject.doOnStop)	this.concernedObject.doOnStop();
		// Executed after the stop :
		if(this.doOnStopSecondaryFunction)	this.doOnStopSecondaryFunction(this.concernedObject);
	}
	doStep(args){
		this.routine.doStep(this.concernedObject,args);
	}
	doOnEachStep(){
		if(this.liberty==="2D"){
				
				let absoluteSpeedX=this.absoluteSpeeds.x;
				let absoluteSpeedY=this.absoluteSpeeds.y;
				
				
				// The time factor speeds corrections :
				if(this.routine.durationTimeFactorHolder 
						// In that case it is handled by reducing the refreshMillis time !
						&& !this.refreshMillis){
					let timeFactor=(1/this.routine.durationTimeFactorHolder.getDurationTimeFactor());
					if(absoluteSpeedX!=null)	absoluteSpeedX=absoluteSpeedX*timeFactor;
					if(absoluteSpeedY!=null)	absoluteSpeedY=absoluteSpeedY*timeFactor;
				}
				
				let x=null;
				if(absoluteSpeedX!=null){
						x=this.concernedObject.position.x + this.directionFactors.x*absoluteSpeedX;
				}
				
				let y=null;
				if(absoluteSpeedY!=null){
						y=this.concernedObject.position.y + this.directionFactors.y*absoluteSpeedY;
				}
				
				let a=null;
				if(this.angularSpeed/*case value==0*/){
					// Non-stop case :
					a=Math.coerceAngle(this.concernedObject.position.a + this.angularSpeed,true,true);
				}
				
				if(x!=null || y!=null){
					// Wrapping handling :
					if(this.wrapping){
						if(this.wrapping.x && x!=null)		x=x%this.wrapping.x;
						if(this.wrapping.y && y!=null)		y=y%this.wrapping.y;
					}
					
					x=nonull(x,this.concernedObject.position.x);
					y=nonull(y,this.concernedObject.position.y);
					a=nonull(a,this.concernedObject.position.a);
					this.concernedObject.position.setLocation( {x:x, y:y, a:a} );

					if(this.followers){
						foreach(this.followers,(f)=>{
							f.position.setLocation({x:this.concernedObject.position.x, y:this.concernedObject.position.y, a:this.concernedObject.position.a});
						});
					}
					
					if(this.doOnEachStepSecondaryFunction)	this.doOnEachStepSecondaryFunction(this.concernedObject);
					
				}
		}

	}
	
	terminateFunction(){

		// UNUSED
		// // Executed before the terminate condition is evaluated :
		// if(this.doOnTerminate && this.doOnTerminate())	return true;
		
		if(this.destination){
			if(this.liberty==="2D"){

					let x=this.concernedObject.position.x;
					let y=this.concernedObject.position.y;
					if(this.destination.x!=null && this.destination.y!=null){
						if(this.directionFactors.x==0 && this.directionFactors.y==0)	return true;
						if(		 (0<this.directionFactors.x && this.destination.x<=x || this.directionFactors.x<0 && x<=this.destination.x)
								&& (0<this.directionFactors.y && this.destination.y<=y || this.directionFactors.y<0 && y<=this.destination.y)){
							//DBG
							this.concernedObject.position.setLocation({x:this.destination.x,y:this.destination.y});
							return true;
						}
					}else{
						if(this.destination.x!=null){
							if(this.directionFactors.x==0)	return true;
							if(0<this.directionFactors.x && this.destination.x<=x || this.directionFactors.x<0 && x<=this.destination.x){
								//DBG
								this.concernedObject.position.setLocation({x:this.destination.x});
								return true;
							}
						}else if(this.destination.y!=null){
							if(this.directionFactors.y==0)	return true;
							if(0<this.directionFactors.y && this.destination.y<=y || this.directionFactors.y<0 && y<=this.destination.y){
								//DBG
								this.concernedObject.position.setLocation({y:this.destination.y});
								return true;
							}
						}
					}
			}
			
		}else{ // Case non-stop mover :
			return false;
		}
			
		return false;
	}
	
}

function getMoverMonoThreaded(concernedObject,startDependsOnParentOnly=false,moverConfig={liberty:"2D",refreshMillis:null,wrapping:null}){
	return new MoverMonoThreaded(concernedObject,startDependsOnParentOnly,moverConfig);
}




