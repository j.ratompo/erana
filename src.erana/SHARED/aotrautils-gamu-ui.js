/* ## Utility gamu methods - view part
 *
 * This set of methods gathers utility game-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils-gamu» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 *  
 */


// ======================== UI elements utility methods ========================



// GLOBAL CONSTANTS

const DEFAULT_CSS_DISPLAY = "flex";
const DOM_IDS_SEPARATOR = "_";
const PATH_SEPARATOR="/";
const SUB_PATH_UNIVERSE="universe";
const SUB_PATH_UI="ui";
const SUB_PATH_AUDIO="audio";
const SUB_PATH_CONFIG="config";

// UI ELEMENTS CONSTANTS

const SELECTION_MARK_UI_BASE_COLOR="#EEEEEE";
const SELECTION_MARK_UI_SEGMENTS=[10,20];

const MESSAGES_DELAY_MILLIS=5000;
const MESSAGES_COLORS={"info":"#4488FF","warn":"#FFFF88","error":"#FF8888","dialog":"#555555"};
const MESSAGES_COLORS_ANSWERS={"info":"#7799FF","warn":"#FFFF99","error":"#FF777777","dialog":"#777777"};
const MESSAGES_SHADOW_COLOR="#000000";
const MESSAGES_SHIFT_SHADOW_PX=3;
const MESSAGES_DISPLAY_X=30;
const MESSAGES_DISPLAY_Y=16;
const MESSAGES_DIALOGS_DISPLAY_Y=16;
//const MESSAGES_DISPLAY_Y_STEP=20;
//const MESSAGES_FONT_FAMILY_FAMILY="helvetica";
const MESSAGES_FONT_FAMILY_FAMILY="monospace"; // Necessary to display a progress bar of the right size on choice dialogs !
const MESSAGES_FONT_MODIFYER="bold";
const MESSAGES_FONT_SIZE=17;
const MESSAGES_CHOICES_SPACING_Y=8;

const MESSAGES_FONT_SIZE_ADJUST_FACTOR=2;// TODO : FIXME : STUPID MAGIC NUMBER !

const PORTRAIT_SCALE=0.3;

const CORRECTION_DIALOG_BACKGROUND_X=-10;
const CORRECTION_DIALOG_BACKGROUND_WIDTH=110;
const CORRECTION_DIALOG_BACKGROUND_Y=-12;


const MENU_BUTTON_PERCENT_SIZE=10;
const MENU_BUTTON_SIZE=30;


// COMMON METHODS
function getMainElement(mainId) {

	var mainElement = document.body;
	if(mainId) {
		var foundMainElement = document.getElementById(mainId);
		if(foundMainElement) mainElement = foundMainElement;
	}

	return mainElement;
}

function getConventionalFilePath(basePath,objectToInspect=null,useUniverseBasePath="none"){
	
	let pathUniverseOrUIOrAudio="";
	if(useUniverseBasePath==="universe"){
		pathUniverseOrUIOrAudio=SUB_PATH_UNIVERSE+PATH_SEPARATOR;
	}else if(useUniverseBasePath==="ui"){
		pathUniverseOrUIOrAudio=SUB_PATH_UI+PATH_SEPARATOR;
	}else if(useUniverseBasePath==="audio"){
		pathUniverseOrUIOrAudio=SUB_PATH_AUDIO+PATH_SEPARATOR;
	}else if(useUniverseBasePath==="config"){
		pathUniverseOrUIOrAudio=SUB_PATH_CONFIG+PATH_SEPARATOR;
	}
	
	if(!objectToInspect)
		return basePath+PATH_SEPARATOR+pathUniverseOrUIOrAudio;
	
	if(typeof(objectToInspect)==="string")
		return basePath+PATH_SEPARATOR+pathUniverseOrUIOrAudio+objectToInspect;
		
	// CAUTION : Do not put on several lines of code, or else it WILL CAUSE BUGS AFTER MINIFYING, (because of optional semicolons handling) !
	let className=objectToInspect.constructor.name;
	let prototypeName= (!contains(objectToInspect.usesSuper,"abstract_") && objectToInspect.inheritedPath ) ? 
														(objectToInspect.inheritedPath):(objectToInspect.prototypeName);

	return basePath+PATH_SEPARATOR+pathUniverseOrUIOrAudio+className+PATH_SEPARATOR+prototypeName+PATH_SEPARATOR;
}




// ===================== UI ELEMENTS DISPLAY =====================


/*public*/function drawSolidText(ctx,x,y,messageText,color="#FFFFFF",fontSize=10,font="helvetica",fontModifyer="",zooms={zx:1,zy:1}, 
																	shadowConfig={color:MESSAGES_SHADOW_COLOR, shift:MESSAGES_SHIFT_SHADOW_PX}){
	
	color=nonull(color,"#FFFFFF");
	fontSize=nonull(fontSize,10);
	font=nonull(font,"helvetica");
	fontModifyer=nonull(fontModifyer,"");
	
	// A simple 3D text effect :
	ctx.save();
	ctx.font=fontModifyer+" "+(fontSize * zooms.zx)+"px "+nonull(font,"Arial");
	ctx.fillStyle=nonull(shadowConfig.color,MESSAGES_SHADOW_COLOR);
	let shift=nonull(shadowConfig.shift,MESSAGES_SHIFT_SHADOW_PX);
	ctx.fillText(""+messageText, (x+shift)*zooms.zx, (y+shift)*zooms.zy);
	ctx.restore();
	
	// The actual text :
	ctx.save();
	ctx.font=fontModifyer+" "+(fontSize * zooms.zx)+"px "+nonull(font,"Arial");
	ctx.fillStyle=color;
	ctx.fillText(""+messageText, x*zooms.zx, y*zooms.zy);
	ctx.restore();

}


/*public*/function getUIManager(gameConfig, controller){

	var projection = gameConfig.projection;

	
	if(!controller){
		// !!! CAUTION : THIS NAME IS A CONVENTION TO BE RESPECTED IN CLIENT CODE !!!
		controller=window.eranaScreen;
	}
	
	
	const self={
		
		gameConfig:gameConfig,
		messageText:null,
		messageDisplayTime:null,
		durationMillis:null,
		severity:"info",
		// messageWidth:100,
		isLineTriggered:false,
		
		dialogLinesStack:[],
		choiceLineStack:[],

		uiVisibilities:{selection:true,text:true,dialogs:true},
		setUIVisibility:function(visibilityName,visibility){
			self.uiVisibilities[visibilityName]=visibility
		},
		
		/*public*/displayMessage:function(severity,durationMillis,messageText){
			self.messageDisplayTime=getNow();
			self.durationMillis=durationMillis;
			self.messageText=messageText;
			self.severity=severity;
//			self.messageWidth=nonull(messageWidth, messageText.length*MESSAGES_FONT_SIZE);
		},

		/*public*/displayDialogMessage:function(portraitImage,endCondition,messageText,offsetY=0,side="left",
					parentObject,
					childDialogProtoName=null, // CAUTION : for dialog line only, not for its subsequent choice lines !
																		 // *IE. MANDATORILY null IF THIS DIALOG HAS CHOICE LINES !*
					doOnClickChoiceline=null,
		){
		
			let line={
					portraitImage:portraitImage,
					messageDisplayTime:getNow(),
					endCondition:endCondition,
					messageText:messageText,
					side:side,
		//		messageWidth:nonull(messageWidth, messageText.length*MESSAGES_FONT_SIZE),
					offsetY:offsetY,
					choiceLines:[],
					childDialogProtoName:childDialogProtoName,
					parentObject:parentObject,
			};
			self.dialogLinesStack.push(line);
			
			
			if(endCondition.choiceLines){
				foreach(endCondition.choiceLines,(choiceLine)=>{
					
					let customAttrValueToSet=null;
					let customAttrName=null;
					if(choiceLine.setParentAttribute){
						let splits=choiceLine.setParentAttribute.split(":");
						customAttrName==splits[0];
						customAttrValueToSet=splits[1];
					}
						
					let choiceLineClickable={
							position:null, // set later !
							size:null, // set later with ctx.measureText(...) !
							doOnClick:function(){ }, // set later !
							isClickable:function(){		return true;	},
							text:choiceLine.text,
							parentObject:parentObject,
							customAttrName:customAttrName,
							customAttrValueToSet:customAttrValueToSet,
							childDialogProtoName:choiceLine.childDialog, // CAUTION : Different from the parent dialog childDialogProtoName passed as parameter.
							childrenQuests:choiceLine.childrenQuests,
							childrenActions:choiceLine.childrenActions,
							doOnClickChoiceline:doOnClickChoiceline,
					};
					
					
					let registeredDialogLine=controller.sceneManager.view.controlsManager.registerClickable(choiceLineClickable);
					line.choiceLines.push(registeredDialogLine);
					
					
				});
				
				
			}
			
			
			
			return line;
		},
		
		
		/*private*/removeLine:function(dialogLine, childDialogProtoName=null){
			
			// We unregister the choice lines from the clickables :
			if(!empty(dialogLine.choiceLines)){
				foreach(dialogLine.choiceLines,(choiceLineClickable)=>{
					controller.sceneManager.view.controlsManager.unregisterClickable(choiceLineClickable);
				});
			}

			remove(self.dialogLinesStack,dialogLine);
			
			// The dialogs holder :
			let parentObject=dialogLine.parentObject;
			
			// TODO : FIXME : DUPLICATED CODE !
			// We trigger the child dialog :
			if(parentObject.getDialogs && parentObject.getDialogs()){
				
				// CAUTION : Dialog must been started to appear in this list :
				foreach(parentObject.getDialogs(),(childDialog)=>{
					childDialog.triggerParentCondition();
					return "break";
				},(childDialog)=>{		return childDialog.prototypeName===childDialogProtoName;	});
			}
			
		},

		
		/*private*/drawDialogLinesCardboard:function(ctx, self){
			
			if(empty(self.dialogLinesStack))	return;
			
			
			let maxWidth=0;
			let xOffset=0;
			let maxHeight=0;
			foreach(self.dialogLinesStack,(dialogLine)=>{
				
				if(!xOffset && dialogLine.portraitImage && dialogLine.portraitImage.width)
					xOffset=dialogLine.portraitImage.width*PORTRAIT_SCALE;
				let textWidth=ctx.measureText(dialogLine.messageText).width*MESSAGES_FONT_SIZE_ADJUST_FACTOR;
				if(maxWidth<textWidth)	maxWidth=textWidth;
				
				maxHeight+= MESSAGES_FONT_SIZE;
				let choiceLines=dialogLine.choiceLines;
				if(!empty(choiceLines)){
					maxHeight+= (MESSAGES_FONT_SIZE+MESSAGES_CHOICES_SPACING_Y)*choiceLines.length;
				}
			});
			
			if(gameConfig.dialogs && gameConfig.dialogs.backgroundColor){
					
					ctx.save();
					
					ctx.fillStyle=gameConfig.dialogs.backgroundColor;
					ctx.strokeStyle=changeLuminosity(gameConfig.dialogs.backgroundColor,-32);
					ctx.lineWidth=10;
					
					ctx.beginPath();
					
					let x=xOffset;
					let offsetY=(!empty(self.dialogLinesStack)?nonull(self.dialogLinesStack[0].offsetY,0):0);
					let y=MESSAGES_DIALOGS_DISPLAY_Y+offsetY;
					
					
					const zooms=gameConfig.zooms;
					const xCardboard=(x+CORRECTION_DIALOG_BACKGROUND_X) * zooms.zx;
					const yCardboard=(y+CORRECTION_DIALOG_BACKGROUND_Y) * zooms.zy;
					const widthCardboard=(maxWidth+CORRECTION_DIALOG_BACKGROUND_WIDTH) * zooms.zx;
					const heightCardboard=maxHeight * zooms.zy;
					
					
					ctx.rect(xCardboard, yCardboard, widthCardboard, heightCardboard);
					ctx.stroke();
					ctx.fill();
					ctx.closePath();

					
					ctx.restore();
			}
		},
		
		

		/*private*/drawDialogLines:function(ctx){
		
			// UI dialog lines drawing :
			let gameConfig=self.gameConfig;
			const zooms=gameConfig.zooms;
			
			
			
			// We draw the dialogs cardboard :
			self.drawDialogLinesCardboard(ctx,self);
			
			
			
			let messagesStackIndexY=0;
			foreach(self.dialogLinesStack,(dialogLine)=>{
				
				let durationMillis=dialogLine.endCondition.durationMillis; // calculated previously
				
				
				let choiceLines=dialogLine.choiceLines;
				
				let portraitImage=dialogLine.portraitImage;
				let messageDisplayTime=dialogLine.messageDisplayTime;
				let messageText=dialogLine.messageText;
				let side=dialogLine.side;
//			let messageWidth=dialogLine.messageWidth;
				let offsetY=dialogLine.offsetY;
				
				if(messageText && messageDisplayTime){
					
					let delayHasPassed = (durationMillis && hasDelayPassed(messageDisplayTime, durationMillis));
					if(!delayHasPassed){
						
						let x=0;

						// TODO : FIXME : DEACTIVATED FOR NOW BECAUSE THERE REMAINS A LITTLE POSITIONNING BUG !
//					if(side==="left")
							x=portraitImage.width*PORTRAIT_SCALE;
						
						let y=MESSAGES_DIALOGS_DISPLAY_Y+messagesStackIndexY+offsetY;
						
						messagesStackIndexY+=(MESSAGES_FONT_SIZE);
						
						
						
						
						// (Must remain after text has been drawn to use measureText(...)) !!
						// ONLY WORKS WITH MONOSPACE FONT FAMILY ! :
						let messageWidth=ctx.measureText(messageText).width;
//					OLD : let messageWidth=messageText.length*MESSAGES_FONT_SIZE*MESSAGES_FONT_SIZE_ADJUST_FACTOR;
						
						
						
						if(gameConfig.dialogs){

							let originalMessageText=messageText;
							
							
							if(gameConfig.dialogs.autoScroll==="letters"){

								let speedFactor=nonull(gameConfig.dialogs.speedFactor,1);
								
								let originalMessageTextLength=originalMessageText.length;
								let amountOfTime= (getNow()-messageDisplayTime);
		
								let messageTextProgressing="";
								
								let numberOfCharacters= Math.min(
															originalMessageTextLength, 
															Math.ceil( (amountOfTime*originalMessageTextLength*speedFactor) / durationMillis  )
														);
								
								messageText=originalMessageText.substring(0, numberOfCharacters);
								
								
							}
							
							
						}
						
						// We draw the dialog line text :
						drawSolidText(ctx,x,y,messageText,MESSAGES_COLORS["dialog"],MESSAGES_FONT_SIZE,MESSAGES_FONT_FAMILY_FAMILY,MESSAGES_FONT_MODIFYER,zooms,
															{shift:1});
						
						

						let imageX=0;
						let imageY=offsetY;
						
						// TODO : FIXME : DEACTIVATED FOR NOW BECAUSE THERE REMAINS A LITTLE POSITIONNING BUG !
//					if(side!=="left")
//						imageX=(messageWidth*MESSAGES_FONT_SIZE_ADJUST_FACTOR)+portraitImage.width*PORTRAIT_SCALE;
						
						drawImageAndCenterWithZooms(ctx, portraitImage, imageX, imageY, zooms, {x:"left",y:"top"}, PORTRAIT_SCALE, PORTRAIT_SCALE);
						
						
						if(!empty(choiceLines)){
							
							//let indexYChoices=y+MESSAGES_DISPLAY_Y_STEP;
							let indexYChoices= y+(MESSAGES_FONT_SIZE+MESSAGES_CHOICES_SPACING_Y);

							foreach(choiceLines,(choiceLine)=>{

							
								let xChoice=0;

								// TODO : FIXME : DEACTIVATED FOR NOW BECAUSE THERE REMAINS A LITTLE POSITIONNING BUG !
//							if(side==="left")
								xChoice=portraitImage.width*PORTRAIT_SCALE;
								
								let yChoice=indexYChoices;
								
								let choiceLineText=choiceLine.text;
								
								// We draw the dialog answer line text :
								drawSolidText(ctx,xChoice,yChoice,choiceLineText,MESSAGES_COLORS_ANSWERS["dialog"],MESSAGES_FONT_SIZE,MESSAGES_FONT_FAMILY_FAMILY,MESSAGES_FONT_MODIFYER,zooms,
										{shift:1});

								// (Must remain after text has been drawn to use measureText(...)) !!
								if(!choiceLine.position){
									
									
									// ONLY WORKS WITH MONOSPACE FONT FAMILY ! :
									let choiceSizeMeasuredWidth=ctx.measureText(choiceLineText).width;
//								OLD : let choiceSizeMeasuredWidth=choiceLineText.length*MESSAGES_FONT_SIZE*MESSAGES_FONT_SIZE_ADJUST_FACTOR;
									choiceLine.size={w:choiceSizeMeasuredWidth*MESSAGES_FONT_SIZE_ADJUST_FACTOR, h:MESSAGES_FONT_SIZE}, // set now !
//								OLD : choiceLine.position=new Position(choiceLine,xChoice,yChoice-MESSAGES_FONT_SIZE*MESSAGES_FONT_SIZE_ADJUST_FACTOR), // set now !
									
									choiceLine.position=new Position(choiceLine,xChoice,yChoice), // set now !  // -MESSAGES_FONT_SIZE because we are based on the line dialog y !
									choiceLine.isFixed=true;
									choiceLine.doOnClick=function(){
										
										if(choiceLine.parentObject && choiceLine.customAttrName){
											choiceLine.parentObject[choiceLine.customAttrName]=choiceLine.customAttrValueToSet;
										}

//									self.removeLine(dialogLine,choiceLine.childDialogObject);
										self.removeLine(dialogLine,choiceLine.childDialogProtoName);
										messagesStackIndexY-=(MESSAGES_FONT_SIZE+MESSAGES_CHOICES_SPACING_Y);
										
										let doOnClickChoiceline=choiceLine.doOnClickChoiceline;
										if(doOnClickChoiceline){
											doOnClickChoiceline(choiceLine);
										}
										
									}; // set now !
									
								}else{
									
									choiceLine.position.setLocation({x:xChoice,y:yChoice-MESSAGES_FONT_SIZE}); // -MESSAGES_FONT_SIZE because we are based on the line dialog y !

								}
								
								
								// FOR DEBUG ONLY :
								if(IS_DEBUG){
									
									ctx.save();
									let rectangle={
											x:choiceLine.position.x * zooms.zx, 
											y:choiceLine.position.y * zooms.zy + 5,// TODO : FIXME : MAGIC NUMBER !
											w:choiceLine.size.w * zooms.zx * 1.2,  // TODO : FIXME : MAGIC NUMBER ! 
											h:choiceLine.size.h * zooms.zy * 1.2}; // TODO : FIXME : MAGIC NUMBER !
									
									ctx.globalAlpha = 0.9;
						//				ctx.fillStyle = SELECTION_MARK_UI_BASE_COLOR;
						//				ctx.fillRect(rectangle.x,rectangle.y,rectangle.w,rectangle.h);
									
									ctx.lineWidth = 2;
									ctx.strokeStyle = SELECTION_MARK_UI_BASE_COLOR;
									ctx.strokeRect(rectangle.x,rectangle.y,rectangle.w,rectangle.h); 
									ctx.restore();
								}
								
								
														
								indexYChoices+=(MESSAGES_FONT_SIZE+MESSAGES_CHOICES_SPACING_Y);
								
							});
							
							
							// We draw a little time out time indicator on the first choice line to show how much time is left before it is chosen by default : 
							if(durationMillis){
								
								let firstChoiceLine=choiceLines[0]; // default choice line index
								
								let firstChoiceLineText=firstChoiceLine.text;
								let firstChoiceLineTextLength=firstChoiceLineText.length;
								let amountOfTime=getNow()-messageDisplayTime;

								
								let choiceLineProgressText="";
								let numberOfCharacters=Math.min(firstChoiceLineTextLength, Math.ceil((amountOfTime*firstChoiceLineTextLength)/durationMillis));
								for(let i=0;i<numberOfCharacters;i++) {
									choiceLineProgressText+="_";
								}
								
								let xChoice=0;
								
								// TODO : FIXME : DEACTIVATED FOR NOW BECAUSE THERE REMAINS A LITTLE POSITIONNING BUG !
//							if(side==="left")
								xChoice=portraitImage.width*PORTRAIT_SCALE;

								let yChoice= MESSAGES_DIALOGS_DISPLAY_Y + offsetY + messagesStackIndexY + MESSAGES_CHOICES_SPACING_Y;
								
								drawSolidText(ctx,xChoice,yChoice,choiceLineProgressText,MESSAGES_COLORS_ANSWERS["dialog"],MESSAGES_FONT_SIZE,MESSAGES_FONT_FAMILY_FAMILY,MESSAGES_FONT_MODIFYER,zooms);

							}
							
							
							
						}
						
						
						
					}else{
						
						// If the user is at a multiple answers, but has clicked any after the time out delay has passed :
						if(!empty(choiceLines)){
							
							// Then we do automatically click the first choice line :
							let firstChoiceLine=choiceLines[0]; // default choice line index
							firstChoiceLine.doOnClick();
														
						}

						
						self.removeLine(dialogLine,dialogLine.childDialogProtoName);
						messagesStackIndexY-=(MESSAGES_FONT_SIZE);
						
					}
					
					
				}
				
				
				
			});


		},
	
		
			
			
			
		/*private*/drawSelectionMarkUI:function(ctx,camera,selectionManager){
			
			let gameConfig=self.gameConfig;
			// The selection mark drawing :
			let selection=selectionManager.getSelected();
			if(!selection || !selection.getIsVisible() || gameConfig.hideSelectionMark)	return;
					
			const zooms=gameConfig.zooms;
			
			
			const clickableSize=selection.clickableSize;


			let offsettedPos=selection.position.getParentPositionOffsetted();
			let drawableZoomedCoords=getDrawableZoomedIfNecessaryCoordinates2D(gameConfig, {x:offsettedPos.x,y:offsettedPos.y}, camera, null, null, zooms);
			
			
			ctx.save();

			
//				// Gradient
//				var gradient = ctx.createLinearGradient(0, 0, 0, 100);
//				gradient.addColorStop(0, "transparent");
//				gradient.addColorStop(.5, SELECTION_MARK_UI_BASE_COLOR);
//				gradient.addColorStop(1, "transparent");
//				ctx.fillStyle = gradient;

			
			// NO : BECAUSE THAT WAY THE RECTANGLE OBJECTS REMAIN AND ACCUMULATE !
//				ctx.rect((drawableZoomedCoords.x-Math.floor(selection.size.w/2)), 
//					 			 (drawableZoomedCoords.y-selection.size.h),
//					 			 selection.size.w, 
//					 			 selection.size.h);
			
			
			let w=(clickableSize?clickableSize.w:selection.size.w) * zooms.zx;
			let h=(clickableSize?clickableSize.h:selection.size.h) * zooms.zy;
			
			
			const demiW=Math.floor(w/2);
			const demiH=Math.floor(h/2);
			let rectangle={	x:(drawableZoomedCoords.x-demiW), 
						   				y:(drawableZoomedCoords.y-demiH),
						   				w:w, 
						   				h:h};

			// SPECIAL : We have to compensate for the center configuration of the items !
			const selectedObjectCenter=selection.position.center;
			if(selectedObjectCenter){
				if(selectedObjectCenter.x==="left")		rectangle.x=drawableZoomedCoords.x;
				else if(selectedObjectCenter.x==="right")	rectangle.x=drawableZoomedCoords.x-w;
				if(selectedObjectCenter.y==="top")			rectangle.y=drawableZoomedCoords.y;
				else if(selectedObjectCenter.y==="bottom")	rectangle.y=drawableZoomedCoords.y-h;
			}

			
//				ctx.globalAlpha = 0.5;
//				ctx.fillStyle = SELECTION_MARK_UI_BASE_COLOR;
//				ctx.fillRect(rectangle.x,rectangle.y,rectangle.w,rectangle.h);
			
			ctx.lineWidth = 2;
			ctx.strokeStyle = SELECTION_MARK_UI_BASE_COLOR;
			ctx.setLineDash(SELECTION_MARK_UI_SEGMENTS);
			ctx.strokeRect(rectangle.x,rectangle.y,rectangle.w,rectangle.h); 
			
			
			ctx.restore();
				
		},
			
		/*private*/drawText:function(ctx){
		
			// UI text drawing :
			if(self.durationMillis && self.messageText && self.messageDisplayTime){

				var delayHasPassed =  hasDelayPassed(self.messageDisplayTime, self.durationMillis);
				if(!delayHasPassed){
					
					let x=MESSAGES_DISPLAY_X;
//					let x=Math.floor(resolution.w-(self.messageWidth/2));
					let y=MESSAGES_DISPLAY_Y;
					
					const zooms=self.gameConfig.zooms;
					drawSolidText(ctx,x,y,self.messageText,MESSAGES_COLORS[self.severity],MESSAGES_FONT_SIZE,MESSAGES_FONT_FAMILY_FAMILY,MESSAGES_FONT_MODIFYER,zooms);
					
				}
			}
			
		},
		
		/*public*/drawAll2D:function(ctx,camera,selectionManager){

			// The UI selection mark drawing :
			if(self.uiVisibilities.selection)
				self.drawSelectionMarkUI(ctx,camera,selectionManager);
			
			// UI text drawing :
			if(self.uiVisibilities.text)
				self.drawText(ctx);
			
			// UI portraits dialog lines drawing :
			if(self.uiVisibilities.dialogs)
				self.drawDialogLines(ctx);
			
			
		}
		
	};
	
	return self;
}



