

class FusrodaClientManager{
	
	constructor(controller){
		
		this.controller=controller;
		
		this.hasAlreadyLaunchedConnection=false;
		
		this.vncEndPointsByURL={};
		this.vncEndPointsByURLObjects={};
		this.lastPointerMoveTime=null;

	}
	
	
	// INIT ON START

	initOnStartScreenAtLevelStart(){


		this.setupMousePositionRayCasterOnce();
		this.setupMouseButtonsEventsOnce();
		this.setupKeyboardEventsOnce();
		
	}
	
	/*private*/setupMousePositionRayCasterOnce(){
		
		const self=this;
		
		document.body.addEventListener("pointermove",(event)=>{
			
			// (we have a time restriction for fusroda continuous mouse events sending only.)
			if(hasDelayPassed(self.lastPointerMoveTime,MOUSE_EVENTS_REFRESH_MILLIS)){
				self.lastPointerMoveTime=getNow();
	
				if(!empty(self.vncEndPointsByURL) && !empty(self.vncEndPointsByURLObjects)){
					self.controller.handleRaycasterPointerEvent(event,self.vncEndPointsByURLObjects).then((args)=>{
							if(empty(args.intersecteds))		return;
							const intersected=args.intersecteds[0]; // We only take the first found one !
							if(self.fusrodaClient)	self.fusrodaClient.sendMouseMoveEvent(getPosition2DFromRayVector(intersected));
					});
				}
			}
			
		});
	
	}
	
		
	
	/*private*/setupMouseButtonsEventsOnce(){
		const self=this;
		
		document.body.addEventListener("mousedown",(event)=>{
			const mouseButton=event.button;
			
//			// DBG
//			lognow("!!! mousedown event:["+mouseButton+"]",event);
		
			if(!empty(self.vncEndPointsByURL) && !empty(self.vncEndPointsByURLObjects)){
				self.controller.handleRaycasterPointerEvent(event,self.vncEndPointsByURLObjects).then((args)=>{
						const intersecteds=args.intersecteds;
						if(empty(intersecteds))		return;
						// CAUTION : WE SEND NO CLICKED COORDINATES INFORMATION !
						if(self.fusrodaClient)	self.fusrodaClient.sendMouseClickEvent({mouseButton:mouseButton});
				});
			}

		
			
		
		});
		
		document.body.addEventListener("mouseup",(event)=>{
			const mouseButton=event.button;
			
//		// DBG
//		lognow("!!! mouseup event:["+mouseButton+"]",event);

			if(!empty(self.vncEndPointsByURL) && !empty(self.vncEndPointsByURLObjects)){
				if(self.fusrodaClient)	self.fusrodaClient.sendMouseReleasedEvent({mouseButton:mouseButton});
			}
			
			
		});
		
		
		document.body.addEventListener("wheel",(event)=>{
			const wheelAmount=event.deltaY*WHEEL_EVENT_FACTOR;
//			// DBG
//			lognow("!!! wheel event:["+wheelAmount+"]",event);

			if(!empty(self.vncEndPointsByURL) && !empty(self.vncEndPointsByURLObjects)){
				self.controller.handleRaycasterPointerEvent(event,self.vncEndPointsByURLObjects).then((args)=>{
						const intersecteds=args.intersecteds;
						if(empty(intersecteds))		return;
						if(self.fusrodaClient)	self.fusrodaClient.sendWheelEvent({wheelAmount:wheelAmount});
				});
			}			
			
			
			
		});
		
	
	}
	

	/*private*/setupKeyboardEventsOnce(){
		const self=this;

		if(!empty(self.vncEndPointsByURL) && !empty(self.vncEndPointsByURLObjects)){
			document.body.addEventListener("keydown",(event)=>{
				let keyChar=event.key;
				const keyCode=event.keyCode;
				
				// DBG
				lognow("!!! keydown KEYDOWN  event:[event.key:"+event.key+"][keyCode:"+keyCode+"][keyChar:"+keyChar+"]",event);
				
				// To avoid server-side «A JSONObject text must end with '}'» errors :
				if(keyChar==="{")	keyChar="left_brace";
				if(keyChar==="}")	keyChar="right_brace";

//				// CAUTION : THIS IS A SPECIAL CHARACTER, USED ONLY TO TRIGGER THE CONTENT ASSIST ! (it was the ugliest on keyboard)
//				if(keyChar==="¶")	keyChar="special";
				
				if(self.fusrodaClient)	self.fusrodaClient.sendKeyboardPressedEvent({keyChar:keyChar, keyCodeFallback:keyCode});
			});
			
			document.body.addEventListener("keyup",(event)=>{
				let keyChar=event.key;
				const keyCode=event.keyCode;
				
				// DBG
				lognow("!!! keyup KEYUP event:[code:"+event.code+"][keyCode:"+keyCode+"][key:"+keyChar+"]",event);

				// To avoid server-side «A JSONObject text must end with '}'» errors :
				if(keyChar==="{")	keyChar="left_brace";
				if(keyChar==="}")	keyChar="right_brace";
				
//				// CAUTION : THIS IS A SPECIAL CHARACTER, USED ONLY TO TRIGGER THE CONTENT ASSIST ! (it was the ugliest on keyboard)
//				if(keyChar==="¶")	keyChar="special";
				
				if(self.fusrodaClient)	self.fusrodaClient.sendKeyboardReleasedEvent({keyChar:keyChar, keyCodeFallback:keyCode});
			});
		}
		
	}
	
	
	
	
	/*public*/connectToServer(url, port=6080){

		if(!url)	return;
		if(!port)	return;
		
		// We only connect to server once :
		if(this.hasAlreadyLaunchedConnection)	return;
		this.hasAlreadyLaunchedConnection=true;
		

		const key=url+":"+port;
		
		const self=this;
		this.fusrodaClient=createFusrodaClient(
			(messageConfig)=>{self.doOnClientReady(messageConfig);},
			// doOnDataReception:
			{
				"image":(imageDataStr)=>{
					const vncEndPoint=self.vncEndPointsByURL[key];
					if(!vncEndPoint)	return;
					vncEndPoint.updateTexture(imageDataStr);
				},
				"sound":(soundDataStr)=>{
					const vncEndPoint=self.vncEndPointsByURL[key];
					if(!vncEndPoint)	return;
					const decodedIntsArray=getDecodedArrayFromSoundDataString(soundDataStr);
					vncEndPoint.playSound(decodedIntsArray);
				}
		}, url, port);
		
		this.fusrodaClient.client.start();
		
	}
	
	
	
	/*private*/doOnClientReady(messageConfig){
		
		const self=this;
		
		//3)
		// Now we can configure all the VNC endpoints according to what the server has abilities :
		const vncEndPoint=self.vncEndPointsByURL[key];
		
		vncEndPoint.configure(messageConfig);
		
		self.controller.loadObjectInScene(vncEndPoint, vncEndPoint.overriddenBasePath);
		
		vncEndPoint.addOnFinishedDrawingListener(()=>{
			
			// //DBG
			// lognow("Image doOnFinishedDrawingListener.");
			
			//5)=>2)
			self.fusrodaClient.client.socketToServer.send("screenFrameRequest", {});
		});		
						
		vncEndPoint.addOnFinishedPlayingListener(()=>{
			//5)=>2)
			self.fusrodaClient.client.socketToServer.send("soundSampleRequest", {});
		});	
				
		
	}
	
	
	
	
	
	
	/*public*/setVNCEndPoint(key, vncEndPoint){
		this.vncEndPointsByURL[key]=vncEndPoint;
	}
	
	/*public*/setVNCEndPointObject(key, vncEndPointObject){
		this.vncEndPointsByURLObjects[key]=vncEndPointObject;
	}
	
	
}

