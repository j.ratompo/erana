

class OritaMainClientManager{
	
	constructor(controller){
		
		this.controller=controller;
		
		// CURRENT :
		//this.oritaMainClient;
		
		this.streamEndPointsByMicroClientId={};
		this.streamEndPointsByMicroClientIdObjects={};
		this.ioEndPointsByMicroClientId={};
		this.ioEndPointsByMicroClientIdObjects={};

	}
	
	
	/*public*/initOnStartScreenAtLevelStart(){
		
		
	}
	

	
	
	/*public*/connectToServer(url, port=25000){
		
		if(!url)	return;
		if(!port)	return;
		
		// We only connect to server once :
		if(this.hasAlreadyLaunchedConnection)	return;
		this.hasAlreadyLaunchedConnection=true;
		

		const key=url+":"+port;
		
		const self=this;
		this.oritaMainClient=createOritaMainClient(
			 ()=>self.doOnRegistered(),
			 (microClientId, receivedData)=>self.treatVideoData(microClientId, receivedData),
			 (microClientId, receivedData)=>self.treatAudioData(microClientId, receivedData),
			 (mainClient, microClientId, inputsGPIO)=>self.doOnInputsCreated(mainClient, microClientId, inputsGPIO),
			 (mainClient, microClientId, inputsGPIO)=>self.doOnInputsUpdated(mainClient, microClientId, inputsGPIO),
			 (mainClient, microClientId, outputsGPIO)=>self.doOnOutputsCreated(mainClient, microClientId, outputsGPIO),
			 (mainClient, microClientId, outputsGPIO)=>self.doOnOutputsUpdated(mainClient, microClientId, outputsGPIO),
			 url, port, /*DEBUG*/true);
		
		this.oritaMainClient.start();
		
	}
	
	/*public*/setStreamEndPoint(microClientId, streamEndPoint){
		this.streamEndPointsByMicroClientId[microClientId]=streamEndPoint;
	}
	/*public*/setStreamEndPointObject(microClientId, streamEndPointObject){
		this.streamEndPointsByMicroClientId[microClientId]=streamEndPointObject;
	}	
	
	/*public*/setIOEndPoint(microClientId, ioType, ioEndPoint){
		if(!this.ioEndPointsByMicroClientId[ioType])	this.ioEndPointsByMicroClientId[ioType]={};
		this.ioEndPointsByMicroClientId[ioType][microClientId]=ioEndPoint;
	}
	/*public*/setIOEndPointObject(microClientId, ioType, ioEndPointObject){
		if(!this.ioEndPointsByMicroClientIdObjects[ioType])	this.ioEndPointsByMicroClientIdObjects[ioType]={};
		this.ioEndPointsByMicroClientIdObjects[ioType][microClientId]=ioEndPointObject;
	}	
	
	

	/*private*/doOnRegistered(){
		
		// DBG
		lognow("Orita client registered to server !");
		
		
	}
	
	
	/*private*/treatVideoData(microClientId, receivedData){
		
		const videoData=receivedData.video;
		
		const streamEndPoint=this.streamEndPointsByMicroClientId[microClientId];
		if(!streamEndPoint)	return;
		
		// DBG
		lognow("videoData:",videoData);

//		// DOES NOT SEEM TO WORK :
//		const imageDataStr=intArrayToBase64String(videoData.data, true);
//		// DBG
//		lognow(imageDataStr);
//		streamEndPoint.updateTexture(imageDataStr);

// DBG
//		const imageDataBlob=intArrayToBlob(videoData.data);
//		// DBG
//		lognow(imageDataBlob);
//
//		streamEndPoint.updateTexture(imageDataBlob);
		
		// DBG
		
		streamEndPoint.updateTexture(videoData);
		
	}
	
	/*private*/treatAudioData(microClientId, receivedData){
		const audioData=receivedData.audio;
		
		const ioEndPoint=this.streamEndPointsByMicroClientId[microClientId];
		if(!ioEndPoint)	return;
		
		
		// DBG
		lognow("audioData:",audioData);

		// TODO : Develop...
	}
	
	/*private*/doOnInputsCreated(mainClient, microClientId, inputsGPIO){
		
	}
	/*private*/doOnInputsUpdated(mainClient, microClientId, inputsGPIO){
		
	}
	/*private*/doOnOutputsCreated(mainClient, microClientId, outputsGPIO){
		
	}
	/*private*/doOnOutputsUpdated(mainClient, microClientId, outputsGPIO){
		
	}

	
}