

class OritaIOFrame3D extends GameIOFrame3D{


	constructor(){
		super();
		
		this.targetMicroClientId=null;
	}
		


	/*OVERRIDES*/
	initAfterLevelLoading(){
		// NO : Because we have to start the orita client first :
//	window.eranaScreen.loadObjectInScene(this, this.overriddenBasePath);
		super.initAfterLevelLoading();


		const controller=window.eranaScreen;
		
		const key=this.targetMicroClientId;
		controller.setStreamEndPoint(key, this);
		
		controller.connectToIOServerOnce(this.url, this.port);
		
	}
	
	
	// DOESN'T uses loadModel function set by default :
	/*OVERRIDES*/
	/*protected*/doOnLoaded(controller){
		super.doOnLoaded(controller);
		
		const key=this.targetMicroClientId;
		controller.setStreamEndPointObject(key, this.object3D);
	}
	
//	/*OVERRIDES*/
//	/*public*/updateTexture(imageDataBlob=null){
//		
//		if(!this.texture)	return;
//		
//		if(this.isImageLoading)		return;
//		
//		
//		if(this.image.complete){
//			if(!imageDataBlob){
//				this.image.src="";
//			}else{
//				this.image.src=imageDataBlob;
//				this.isImageLoading=true;
//			}
//			
//		}
//	}

	/*OVERRIDES*/
	/*public*/updateTexture(videoData=null){
		
		if(!videoData)	return;
		
		// DBG
		const newVideoDataData=[];
		for(let i=0;i<videoData.data.length;i+=3){
			newVideoDataData.push(videoData.data[i]);
			newVideoDataData.push(videoData.data[i+1]);
			newVideoDataData.push(videoData.data[i+2]);
			newVideoDataData.push(255);
		}
		
		const imgDataRaw=new Uint8ClampedArray(newVideoDataData);
		
		const imageData=new ImageData(imgDataRaw, videoData.width, videoData.height);
		this.ctx.putImageData(imageData,0,0);
		
		this.texture.needsUpdate=true;
		
		//4)
		this.onFinishedDrawingListeners.forEach((listener)=>{listener();});
		
	}
	
}

