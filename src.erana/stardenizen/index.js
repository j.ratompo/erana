

require "erana.commons.build.js"
require "erana.stardenizen.build.js"


window.eranaScreen = new StardenizenScreenServer({gameConfig:GAME_CONFIG_SERVER, prototypesConfig:PROTOTYPES_CONFIG});

// TRACE
lognow("window.eranaScreen",window.eranaScreen);			



const readline = require("readline");

const consoleInterface = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

consoleInterface.setPrompt("> "); // the prompt symbol
consoleInterface.prompt();

consoleInterface.on("line", (input) => {
  if (input.toLowerCase() === "exit") {
    console.log("Bye.");
    consoleInterface.close();
  } else if (input.toLowerCase() === "connect") {
    console.log("Connecting node...");
    window.eranaScreen.connectAORTACNode();
    consoleInterface.close();
  } else {
    consoleInterface.prompt();
  }
});


