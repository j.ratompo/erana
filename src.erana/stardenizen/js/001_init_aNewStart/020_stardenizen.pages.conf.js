// DEPENDENCY:init.js


// ===============================================================================
// FLOW
// ===============================================================================



FLOW_CONFIG = {
	
	
	splashPage_erana:{

		delayMillis:(IS_DEBUG?200:3000),
		
		image: "splashs/aotraLogo_300.png",
		background: "#FFFFFF",
		text:{
			font:"helvetica",
			position:{yBottom:20},
			message:i18n("propulséParEranaBrSpanStyleFontSize7emUnMoteurOuv")
		},
		goTo:"splashPage_alqemia"

	},
	
	splashPage_alqemia:{

		delayMillis:(IS_DEBUG?200:3000),
		
		image: "splashs/alqemia_400.png",
		background: "#000000",
		text:{
			color:"#000000",
			font:"helvetica",
			position:{yTop:70},
			message:i18n("leStudioAlqemiaPrésente"),
		},
		
//	goTo:(IS_DEMO?"alphaPage":(IS_DEBUG?"homePage":"audioPage")),

		// FOR NOW ONLY (alpha version) :
//		goTo:(IS_DEBUG?"homePage":"alphaPage"),
		goTo:("homePage"),

		
//		goTo:"audioPage"
//		goTo:"alphaPage",

	},

	
		
	alphaPage:{
		
		image: "splashs/icon_bug.png",


		// WORKAROUND : To force the user to interact to bypass the «user must interact with the page first before autoplay» user annoyance protection politics :

		background: "#000000",
		text:{
			color:"#FFFFFF",
			font:"helvetica",
			position:{yTop:80},
			message:i18n("attentionCeciEstUneVersionAlphaBrDesBugsInacceptable"),
												
		},
		
		
		menus:{
			mainMenu:{
				items:[
					{
						label:i18n("ok"),
						goTo:"audioPage"
					},
				]
			},
		},
	},
	
	
	
	
	
	audioPage:{
		
		image: "splashs/icon_headset.png",


		// WORKAROUND : To force the user to interact to bypass the «user must interact with the page first before autoplay» user annoyance protection politics :

		background: "#000000",
		text:{
			color:"#FFFFFF",
			font:"helvetica",
			position:{yTop:80},
			message:i18n("unDispositifDÉcouteEstConseilléBrPourUneMeilleureExpé"),
		},
		
		
		menus:{
			mainMenu:{
				items:[
					{
						label:i18n({
							"fr":"OK",
							"en":"OK"
						}),
						goTo:"homePage"
					},
				]
			},
		},
	},
	
	
		
	
	
	
	homePage:{
		
		//image: "splashs/diafanahe-title.png",
		
		background: "#333399",
		text:{
			font:"helvetica",
			position:{yTop:0},
			message:i18n({"fr":"<h1 class='gameTitle'>Les Stellicoles</h1>","en":"<h1 class='gameTitle'>Star denizen</h1>",})
		},
		
//		startLevelName:"proto_level_0",

		// WORKAROUND : To bypass the «user must interact with the page first before autoplay» user annoyance protection politics :


//		music:{
//			pauseBetweenMillis:4000,
//			volumePercent:50,
//			tracks: ["musics/Mercy.mp3","musics/Evermore.mp3"],
//		},
	
		
		menus:{
			mainMenu:{
				items:[
					{
						label:i18n("commencer"),
//						actionAfter:(IS_DEBUG?"startNewGame":"startCinematic"),
//						goTo:(IS_DEBUG?"gamePage":"cinematic1Page"),
						actionAfter:("startNewGame"),
						goTo:("gamePage"),
						
					},
					{
						label:i18n("continuer"),
						actionAfter:"continueGame",
						goTo:"gamePage",
						activeOnlyIf:"isPreviousGameDetected()",
						restoreLevel:true,
					}, 
					{
						label:i18n("crédits"),
						actionAfter:"startCredits",
						goTo:"creditsPage"
					},
					{
						label:i18n("soutenir"),
						goTo:"supportPage",
					},
					{
						label:i18n("quitter"),
						actionAfter:"exitGame",
						//goTo:"homePage",
					},
				]
			},
		},
	},


//	cinematic1Page:{
//
//		startLevelName:"proto_level1_cinematic1",
//		visibleControls:true,
//		hiddenControls:true,
//
//		music:{
//			pauseBetweenMillis:4000,
//			volumePercent:100,
//			tracks: ["musics/Seeker.mp3"],
//			loop:false,
//		},
//		
//		onLeave:"stopCinematic",
//		
//		menus:{
//			mainMenu:{
//				config:{
//					label:i18n("menu"),
//					actionAfter:"showMenu",
//					position:"top-right" // default position is "top-left"
//				},
//				items:[
//					{
//						label:i18n("passer"),
//						actionAfter:"startNewGame",
//						goTo:"gamePage"
//					}, {
//						label:i18n("retourALAccueil"),
//						actionAfter:"exitGame",
//						goTo:"homePage"
//					},
//				]
//			},
//		},
//	},

	
	
//	endOfDemoCinematicPage:{
//
//		startLevelName:"proto_level1_cinematicEndDemo",
//		visibleControls:true,
//		hiddenControls:true,
//
//// 	UNUSEFUL :
////	onEnter:"startCinematic", // Special case, since this cinematic is not triggered by a button click !
////	onLeave:"stopCinematic",
//		
//		music:{
//			pauseBetweenMillis:4000,
//			volumePercent:100,
//			tracks: ["musics/Realness.mp3"],
//			loop:false,
//		},
//		
//		menus:{
//			mainMenu:{
//				config:{
//					label:i18n("menu"),
//					actionAfter:"showMenu",
//					position:"top-right" // default position is "top-left"
//				},
//				items:[
//					{
//						label:i18n("passer"),
//						actionAfter:"startNewGame",
//						goTo:"homePage"
//					},
//				]
//			},
//		},
//		
//	},
	
	
	
	
	gamePage:{

		startLevelName:"proto_level1",
		visibleControls:true,

//		music:{
//			pauseBetweenMillis:4000,
//			volumePercent:50,
//			tracks: ["musics/Brooks.mp3","musics/Denouement.mp3","musics/Snowmen.mp3"],
//		},
		
		menus:{
			mainMenu:{
				config:{
					label:i18n("menu"),
					actionAfter:"showMenu",
					position:"top-right" // default position is "top-left"
				},
				items:[
					{
						label:i18n("reprendre"),
						actionAfter:"resumeGame",
						resume:true
					},
					{
						label:i18n("sauverEtRevenirÀLAccueil"),
						actionBefore:"interruptGame",
						goTo:"homePage"
					},
					{
						label:i18n("retourALAccueil"),
						actionAfter:"abandonGame",
						goTo:"homePage"
					}
				]
			},
		},
	},


	
	
	
	creditsPage:{

		startLevelName:"proto_level_credits",
		
		
		menus:{
			mainMenu:{
				items:[
					
					{ label:i18n("programmationJeremieR"), inactive:true,},
					{ label:i18n("graphiquesJeremieR"), inactive:true,},
					{ label:i18n("écritureJeremieR"), inactive:true,},
					{ label:i18n("sonsJeremieR"), inactive:true,},
					{ label:i18n("communicationJeremieR"), inactive:true,},
					{ label:i18n("musiqueKaiEngel"), actionAfter:"goToURLOther" },
					{ label:i18n("remerciementsSupportMoralDominiqueB"), inactive:true,},
					{ label:i18n("remerciementsMusiqueÉcoutéeKaiEngelLindseyStirlingOh"), inactive:true,},
					{ label:i18n("remerciementsPodcastsÉcoutésTalesOfPiFranceInterFib"), inactive:true,},
					{ label:i18n("retourALAccueil"), goTo:"homePage"},
										
				]
			},
		},
	},
	
	
	

	
	
	supportPage:{

		startLevelName:"proto_level_credits",
		
		menus:{
			mainMenu:{
				items:[
					
			
					{
						label:i18n({
							"fr":"<strong>Comment nous soutenir ?</strong>",
							"en":"<strong>How to support the game ?</strong>",
						}),
						inactive:true,
					},

					{
						label:i18n({
							"fr":"Faites-nous parvenir vos commentaires / demandez à être informé des prochains développements !",
							"en":"Let us know your comments / ask for updates from the game development !",
						}),
						inactive:true,
					},

					{
						label:i18n({
							"fr":"Envoyer un courriel à l´équipe : info@alqemia.com",
							"en":"Send an email to the team : info@alqemia.com",
						}),
						actionAfter:"goToURLMailTo"

					},
					
//					{
//						label:i18n({
//							"fr":"Faites-nous un petit don qui fera une grande différence ! 😉",
//							"en":"Give a us a little tip that will make a big difference ! 😉",
//						}),
//						actionAfter:"goToURLSupport"
//					},
					
					{
						label:i18n("retourALAccueil"),
						goTo:"homePage"
					},
					
					
				]
			},
		},
	},
	
	
	
	
	
	

};



