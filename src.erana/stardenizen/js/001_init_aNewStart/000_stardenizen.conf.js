// DEPENDENCY:init.js


window.lang = "fr";


// ===============================================================================
// CONFIG
// ===============================================================================


const PROTO_PREFIX="proto_";


const GAME_CONFIG = {
	
	gameURLs:{
//		"support":"https://paypal.me/pools/c/8szm55N1iC", 
		"other":"https://www.kai-engel.com", "email":"mailto:info@alqemia.com"},
	basePath:"stardenizen.medias",
	
	spinnerImage:"aotraLogo_128.gif", 
	
	// view config :
	X_OFFSET:0,
	Y_OFFSET:0,
	// TODO:FIXME:ZOOM DOES NOT WORK AT ALL :
	zooms:{zx:1,zy:1}, // Zoom is the most outer function. Camera and Offsets coordinates
			// obeys it!

	// resolution :
	chosenResolutionIndex:1,
	getResolution:function(){
		// This syntax is quite cool :
		return [{w:getWindowSize("width"),h:getWindowSize("height")},{w:600,h:400},][this.chosenResolutionIndex];
	},
	getViewCenterOffset:function(ignoreOffset=false){
		var result={
				x:Math.floor(this.getResolution().w*.5) + (ignoreOffset?0:this.X_OFFSET),
				y:Math.floor(this.getResolution().h*.5) + (ignoreOffset?0:this.Y_OFFSET)
		};
		return result;
	},
	
	canChangeDurationTimeFactor:false,

	// lighting config :
	lighting:{ dark:true, darkRaycast:true, bump:true },
	
	// scroll config :
	scale:0.025,// = 1/40 (40 px = 1m)
	scroll:"bidimensional",
	
	projection:"2D",
	
	// camera config :
	cameraFixed:"selection",
	
	// selection config :
	selectionCanChange:false,
	
	// controls config :
	controlsMode: "screen-click object",
	
	popupConfig:{
		backgroundColor1:"#222222",
		backgroundColor2:"#111111",
		textColor:"#FFFFFF",
	},
	
	// images config :
	backgroundClipSize:{w:400,h:600},
	// controller config :
	// ALTERNATIVE PARAMETER : refreshRateMillis:33, // :(1000/33)=30 FPS max

//	fps:40, // CAUTION : changes speeds de facto !
	// DBG
	fps:20,
	
	// ui
	dialogs:{	
		autoScroll:"letters", 
		speedFactor:8, /*UNUSEFUL (Since there will always be a «linger» time if you set a speedFactor>1 !) : minimumTimeMillis:1000*/
		backgroundColor:"#D1B69C",
	},
	
	// model config :
	allowParentLinkForChildren:true,
	gameCentralServer:null,
	
	ignoreSpinner:false,
	hideSelectionMark:true,


	aortac:{

		nodesURLs:["ws://127.0.0.1:40001","ws://127.0.0.1:40002","ws://127.0.0.1:40003"],

	},
	

};






