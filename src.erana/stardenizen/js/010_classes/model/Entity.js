		
class Entity extends GameCharacter{

	constructor() {
		super();
	
		
	}

		
	// INIT
	
//	init(gameConfig){
//		super.init(gameConfig);
//	}
	
	initWithParent(){
		if(this.isAIControlled)		this.gameAutomaton.start(this);
	}
	
	
	/*public*/doOnSelectionChange(isSelected){
		if(!isSelected)	this.gameAutomaton.resume();
		else						this.gameAutomaton.pause();
	}

	
	/*public*//*OVERRIDES*/startMovingTo(destination,followers){
		super.startMovingTo(destination,followers,null,this.getSpeed());
	}
	
	/*private*/getSpeed(){
		return nonull(this.speed,10);
	}
	
	
	/*public*/goToRandomPoint(){
		let mover=this.getMover();
		if(!mover || (mover.isStarted() && mover.interruptable==false)) return; 
		let positionZone={x:this.position.x, y:this.position.y, w:this.movingZone.w, h:this.movingZone.h};
		let destination=getPositionFor2DObject(positionZone);
		
		// CAUTION : Here this method is intended to be used for AI-controlled entities ! (So we always have no followers !)
		let followers=this.isSelected?window.eranaScreen.followers:[];
		super.startMovingTo(destination,followers,null,this.getSpeed());
		this.gameAutomaton.next();
	}
	/*public*/isMoveTriggered(){
		return Math.getRandomInt(100)<=this.triggerMovingPercent;
	}
	/*public*/hasDisappeared(){
		return !this.isVisible;
	}
	
	
	
	
	// ------------------------------------------------------------------------------
	
	draw(ctx,camera,lightsManager){
		super.draw(ctx,camera,lightsManager);
		
		
		// DBG
		ctx.save();
		const drawableUnzoomedCoordinates=getDrawableZoomedIfNecessaryCoordinates2D(this.gameConfig,this.position.getParentPositionOffsetted(),camera,{w:this.size.w, h:this.size.h},{x:"center",y:"center"});
		const x=drawableUnzoomedCoordinates.x;
		const y=drawableUnzoomedCoordinates.y;
		ctx.fillStyle="#FFFFFF";
		ctx.fillText(this.gameAutomaton.currentStateName+"("+this.gameAutomaton.isStarted()+")|"+Math.floor(this.position.x)+";"+Math.floor(this.position.y), x, y);		
		ctx.restore();
		
		
		this.gameAutomaton.doStep();

	}



}
