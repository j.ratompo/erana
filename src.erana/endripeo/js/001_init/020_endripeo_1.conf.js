// DEPENDENCY:init.js


window.lang = "fr";


// ===============================================================================
// CONFIG
// ===============================================================================


const PROTO_PREFIX="proto_";


const GAME_CONFIG = {
	
	gameURLs:{
//		"support":"https://paypal.me/pools/c/8szm55N1iC", 
		"other":"https://www.kai-engel.com", "email":"mailto:info@alqemia.com"},
	basePath:"endripeo.medias",
	
	spinnerImage:"logoErana.gif", 
	
	// view config :
	X_OFFSET:0,
	Y_OFFSET:0,
	// TODO:FIXME:ZOOM DOES NOT WORK AT ALL :
	zoom:1, // Zoom is the most outer function. Camera and Offsets coordinates obeys it!

	// resolution :
	chosenResolutionIndex:1,
	getResolution:function(){
		// This syntax is quite cool :
		return [{w:getWindowSize("width"),h:getWindowSize("height")},{w:400,h:600},][this.chosenResolutionIndex];
	},
	getViewCenterOffset:function(ignoreOffset=false){
		var result={
				x:Math.floor(this.getResolution().w*.5) + (ignoreOffset?0:this.X_OFFSET),
				y:Math.floor(this.getResolution().h*.5) + (ignoreOffset?0:this.Y_OFFSET)
		};
		return result;
	},
	
	canChangeDurationTimeFactor:false,

	// lighting config :
	lighting:{ dark:true, darkRaycast:true, bump:true },
	
	// scroll config :
	scale:0.025,// = 1/40 (40 px = 1m)
	scroll:"bidimensional",
	
	projection:"2D flat",
	
	// camera config :
	cameraFixed:"selection",
	
	// selection config :
	selectionCanChange:true,
	
	// controls config :
	controlsMode: "screen-click object",
	
	popupConfig:{
		backgroundColor1:"#222222",
		backgroundColor2:"#111111",
		textColor:"#FFFFFF",
	},
	
	// images config :
	backgroundClipSize:{w:400,h:600},
	// controller config :
	// ALTERNATIVE PARAMETER : refreshRateMillis:33, // :(1000/33)=30 FPS max

//	fps:40, // CAUTION : changes speeds de facto !
	// DBG
	fps:20,
	
	// ui
	dialogs:{	
		autoScroll:"letters", 
		speedFactor:8, /*UNUSEFUL (Since there will always be a «linger» time if you set a speedFactor>1 !) : minimumTimeMillis:1000*/
		backgroundColor:"#D1B69C",
	},
	
	// model config :
	allowParentLinkForChildren:true,
	gameCentralServer:null,
	
	ignoreSpinner:false,
	hideSelectionMark:true,

};





// ===============================================================================
// FLOW
// ===============================================================================



FLOW_CONFIG = {
	
	
	splashPage_erana:{

		delayMillis:(IS_DEBUG?200:3000),
		
		image: "splashs/aotraLogo_300.png",
		background: "#FFFFFF",
		text:{
			font:"helvetica",
			position:{yBottom:20},
			message:i18n("propulséParEranaBrSpanStyleFontSize7emUnMoteurOuv")
		},
		goTo:"splashPage_alqemia"

	},
	
	splashPage_alqemia:{

		delayMillis:(IS_DEBUG?200:3000),
		
		image: "splashs/alqemia_400.png",
		background: "#000000",
		text:{
			color:"#000000",
			font:"helvetica",
			position:{yTop:70},
			message:i18n("leStudioAlqemiaPrésente"),
		},
		
//	goTo:(IS_DEMO?"alphaPage":(IS_DEBUG?"homePage":"audioPage")),

		// FOR NOW ONLY (alpha version) :
//		goTo:(IS_DEBUG?"homePage":"alphaPage"),
		goTo:("homePage"),

		
//		goTo:"audioPage"
//		goTo:"alphaPage",

	},

	
		
	alphaPage:{
		
		image: "splashs/icon_bug.png",


		// WORKAROUND : To force the user to interact to bypass the «user must interact with the page first before autoplay» user annoyance protection politics :

		background: "#000000",
		text:{
			color:"#FFFFFF",
			font:"helvetica",
			position:{yTop:80},
			message:i18n("attentionCeciEstUneVersionAlphaBrDesBugsInacceptable"),
												
		},
		
		
		menus:{
			mainMenu:{
				items:[
					{
						label:i18n("ok"),
						goTo:"audioPage"
					},
				]
			},
		},
	},
	
	
	
	
	
	audioPage:{
		
		image: "splashs/icon_headset.png",


		// WORKAROUND : To force the user to interact to bypass the «user must interact with the page first before autoplay» user annoyance protection politics :

		background: "#000000",
		text:{
			color:"#FFFFFF",
			font:"helvetica",
			position:{yTop:80},
			message:i18n("unDispositifDÉcouteEstConseilléBrPourUneMeilleureExpé"),
		},
		
		
		menus:{
			mainMenu:{
				items:[
					{
						label:i18n({
							"fr":"OK",
							"en":"OK"
						}),
						goTo:"homePage"
					},
				]
			},
		},
	},
	
	
		
	
	
	
	homePage:{
		
		//image: "splashs/diafanahe-title.png",
		
		background: "#000000",
		text:{
			font:"helvetica",
			position:{yTop:0},
			message:i18n({"fr":"<h1 class='gameTitle'>Le voyage d'une conscience</h1>","en":"<h1 class='gameTitle'>The journey of a consciousness</h1>",})
					+"<br /><small>diafanahe</small>"
		},
		
//		startLevelName:"proto_level_0",

		// WORKAROUND : To bypass the «user must interact with the page first before autoplay» user annoyance protection politics :


//		music:{
//			pauseBetweenMillis:4000,
//			volumePercent:50,
//			tracks: ["musics/Mercy.mp3","musics/Evermore.mp3"],
//		},
	
		
		menus:{
			mainMenu:{
				items:[
					{
						label:i18n("commencer"),
//						actionAfter:(IS_DEBUG?"startNewGame":"startCinematic"),
//						goTo:(IS_DEBUG?"gamePage":"cinematic1Page"),
						actionAfter:("startNewGame"),
						goTo:("gamePage"),
						
					},
					{
						label:i18n("continuer"),
						actionAfter:"continueGame",
						goTo:"gamePage",
						activeOnlyIf:"isPreviousGameDetected()",
						restoreLevel:true,
					}, 
					{
						label:i18n("crédits"),
						actionAfter:"startCredits",
						goTo:"creditsPage"
					},
					{
						label:i18n("soutenir"),
						goTo:"supportPage",
					},
					{
						label:i18n("quitter"),
						actionAfter:"exitGame",
						//goTo:"homePage",
					},
				]
			},
		},
	},


	
	
	
	
	gamePage:{

		startLevelName:"proto_level1",
		visibleControls:true,

//		music:{
//			pauseBetweenMillis:4000,
//			volumePercent:50,
//			tracks: ["musics/Brooks.mp3","musics/Denouement.mp3","musics/Snowmen.mp3"],
//		},
		
		menus:{
			mainMenu:{
				config:{
					label:i18n("menu"),
					actionAfter:"showMenu",
					position:"top-right" // default position is "top-left"
				},
				items:[
					{
						label:i18n("reprendre"),
						actionAfter:"resumeGame",
						resume:true
					},
					{
						label:i18n("sauverEtRevenirÀLAccueil"),
						actionBefore:"interruptGame",
						goTo:"homePage"
					},
					{
						label:i18n("retourALAccueil"),
						actionAfter:"abandonGame",
						goTo:"homePage"
					}
				]
			},
		},
	},


	
	
	
	creditsPage:{

		startLevelName:"proto_level_credits",
		
		
		menus:{
			mainMenu:{
				items:[
					
					{ label:i18n("programmationJeremieR"), inactive:true,},
					{ label:i18n("graphiquesJeremieR"), inactive:true,},
					{ label:i18n("écritureJeremieR"), inactive:true,},
					{ label:i18n("sonsJeremieR"), inactive:true,},
					{ label:i18n("communicationJeremieR"), inactive:true,},
					{ label:i18n("musiqueKaiEngel"), actionAfter:"goToURLOther" },
					{ label:i18n("remerciementsSupportMoralDominiqueB"), inactive:true,},
					{ label:i18n("remerciementsMusiqueÉcoutéeKaiEngelLindseyStirlingOh"), inactive:true,},
					{ label:i18n("remerciementsPodcastsÉcoutésTalesOfPiFranceInterFib"), inactive:true,},
					{ label:i18n("retourALAccueil"), goTo:"homePage"},
										
				]
			},
		},
	},
	
	
	

	
	
	supportPage:{

		startLevelName:"proto_level_credits",
		
		menus:{
			mainMenu:{
				items:[
					
			
					{
						label:i18n({
							"fr":"<strong>Comment nous soutenir ?</strong>",
							"en":"<strong>How to support the game ?</strong>",
						}),
						inactive:true,
					},

					{
						label:i18n({
							"fr":"Faites-nous parvenir vos commentaires / demandez à être informé des prochains développements !",
							"en":"Let us know your comments / ask for updates from the game development !",
						}),
						inactive:true,
					},

					{
						label:i18n({
							"fr":"Envoyer un courriel à l´équipe : info@alqemia.com",
							"en":"Send an email to the team : info@alqemia.com",
						}),
						actionAfter:"goToURLMailTo"

					},
					
//					{
//						label:i18n({
//							"fr":"Faites-nous un petit don qui fera une grande différence ! 😉",
//							"en":"Give a us a little tip that will make a big difference ! 😉",
//						}),
//						actionAfter:"goToURLSupport"
//					},
					
					{
						label:i18n("retourALAccueil"),
						goTo:"homePage"
					},
					
					
				]
			},
		},
	},
	
	
	
	
	
	

};



