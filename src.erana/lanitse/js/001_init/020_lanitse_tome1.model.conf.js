// DEPENDENCY:init.js




// ===============================================================================
// MODEL
// ===============================================================================


// Prototype can define as a well a group of things as particular
// individuals:only by not setting a random variance on them.
// ***
// CAUTION : The attribute «usesSuper» must always be the first attribute ! (All attribute put before will be ignored).
// ***


PROTOTYPES_CONFIG = {

	allPrototypes:{
		
		
		// ------------------ Levels objects ------------------
		
		GameLevel:{
			
			proto_level1:{

				selectedItem:"getDefaultSelectedItem()",
				
				Universe:"proto_universe1",
				
			},
			
		},
		
		
		
		// ------------------ Game objects ------------------
		
		
		Universe:{
			
			proto_universe1:{
				

				containerConfig:"starter", // To indicate where the player starts in the tree objects hierarchy
				
				
//				backgroundColor:"#FEE7A2",

//				backgroundColor:"#FFFFFF",
				
				backgroundZIndex:1,
				backgroundParallax:1,
				backgroundOffsetY:0,
				backgrounds:[
 					{images:["background1.png","background2.png"]},
				],
				


				
//				backgroundZIndex:1,
//				backgroundParallax:10,
//				backgroundOffsetY:0,
//				backgrounds:[
//// {images:["backgroundSky1.png","backgroundSky2.png","backgroundSky3.png","backgroundSky4.png"],parallaxAdd:100},
//// //
//// {images:["backgroundFore1.png","backgroundFore2.png","backgroundFore3.png","backgroundFore4.png"],parallaxAdd:10},
//// {images:["background1.png","background2.png","background3.png","background4.png"]},
//
//// {images:["backgroundSky1.png"],parallaxAdd:100},
//// {images:["background1.png"],parallaxAdd:10},
//// {images:["backgroundFore1.png"]},
//
//					{images:["backgroundSky1_v1.png"],parallaxAdd:400},
//					{images:["background1_v1.png"],parallaxAdd:200},
//					{images:["backgroundFore1_v1.png"],parallaxAdd:20},
//					{images:[{normal:"foregroundGround1.png"}],offsetY:-230,clipSize:{w:1600,h:100}},
//
//				],
				
				
//				foregroundParallax:1,
//// foregroundOffsetY:10,
//				foregrounds:[
//// {images:["foreground1.png"],offsetY:-20},
//					{images:[ {normal:"foreground1_v1.png",dark:"foreground1.dark.png"} ],offsetY:-20},
//				],
				
				
			
				GamePhysicsResolver:{
//					inertia:20000 // (in newtons)
					maxSpeeds:{
						linear:100, // At 45° angle
						angularDegrees:10, // In degrees
					},
					
					linearFriction:5,
					angularFriction:1,
					
				},
				
				
				SpacePlace:{
					cardinality:{
						value:"staticGroups",
						prototypesInstantiation:{
							staticInstantiations:[
//								{name:"proto_pingoo",spawningZone:{x:100,y:100,a:Math.PI*.25},preinstanciate:true,subClass:"Ship",},
								{name:"proto_pingoo",spawningZone:{x:0,y:0,a:0},preinstanciate:true,subClass:"Ship",},
								{name:"proto_pingoo",spawningZone:{x:100,y:100,a:Math.PI*.25},preinstanciate:true,subClass:"Ship",},
							]
						},
					},
				},
				
				
			},

			
		},
		
		
		
		Ship:{

//			proto_testDeck:{
//				
//				overridingPosition:{
//					x:0,y:0
//				},
//				
////				Card:["proto_card1","proto_card2","proto_card3","proto_card4","proto_card5","proto_card6"],
////				Card:["*"],
//					Card:["proto_card1","proto_card2",
//								"proto_firstCard","proto_cardFail1", "proto_cardFail2", "proto_cardFail3", "proto_cardFail4", "proto_cardSuccess","proto_cardNoIssue"],
//				
//			},
			
			
			proto_pingoo:{
			
			
				maxGoalSpeed:10000, // in meters/second
			
				inertia:30000, // In N
				
//				overridingPosition:{
//					x:0,y:0
//				},
				
				
				
				CollisionZone:{
				
					scaleX:.5,
					scaleY:.5,
					
					src:"exterior.svg",
				
				},
				
				
				
				size:{w:200,h:100},
				
				imageConfigs:{
					outside:{
						_2D:{
							
							
							zIndex:2,						
							center:{x:"center",y:"center"},
							
							sourcePath:"pingoo.png",
							sourcePathDark:"pingoo.dark.png",
							sourcePathBump:"pingoo.bump.png",
						},
					},
					inside:{
						_2D:{
							
							
							zIndex:2,						
							center:{x:"center",y:"center"},
							
							sourcePath:"pingoo_inside.png",
							sourcePathDark:"pingoo_inside.dark.png",
							sourcePathBump:"pingoo_inside.bump.png",
						},
					},
				},				
				
				Device:{
					cardinality:{
						value:"staticGroups",
						prototypesInstantiation:{
							staticInstantiations:[
//								{name:"proto_engineSubachu1",spawningZone:{x:60,y:20,a:null},preinstanciate:true,subClass:"Engine",},
//								{name:"proto_engineSubachu1",spawningZone:{x:60,y:-20,a:0},preinstanciate:true,subClass:"Engine",},
//								{name:"proto_engineSubachu1",spawningZone:{x:60,y:-20,a:Math.PI*.25},preinstanciate:true,subClass:"Engine",},
//								{name:"proto_engineSubachu1",spawningZone:{x:-60,y:20,a:Math.PI*.5},preinstanciate:true,subClass:"Engine",},
//								{name:"proto_engineSubachu1",spawningZone:{x:-60,y:-20,a:Math.PI},preinstanciate:true,subClass:"Engine",},
//								{name:"proto_engineSubachu1",spawningZone:{x:-100,y:50,a:Math.PI*1.5},preinstanciate:true,subClass:"Engine",},
//								{name:"proto_engineSubachu1",spawningZone:{x:-100,y:-50,a:Math.PI*1.75},preinstanciate:true,subClass:"Engine",},
							]
						},
					},
				},
				
				
				Engine:{
					cardinality:{
						value:"staticGroups",
						prototypesInstantiation:{
							staticInstantiations:[
								
//							// DBG
//								{name:"proto_engineSubachu1",spawningZone:{x:-100,y:-50,a:Math.PI},preinstanciate:true,customAttributes:{id:"MAINrectoR"}},
//								// DBG
//								{name:"proto_engineSubachu1",spawningZone:{x:-100,y:50,a:Math.PI},preinstanciate:true,customAttributes:{id:"MAINrectoL"}},
								
								
								
								{name:"proto_engineSubachu1",spawningZone:{x:40,y:20,a:null},preinstanciate:true,customAttributes:{id:"MAINretroR"}},
								{name:"proto_engineSubachu1",spawningZone:{x:40,y:-20,a:0},preinstanciate:true,customAttributes:{id:"MAINretroL"}},
								{name:"proto_engineSubachu1",spawningZone:{x:-95,y:-10,a:Math.PI},preinstanciate:true,customAttributes:{id:"MAINrectoR"}},
								{name:"proto_engineSubachu1",spawningZone:{x:-95,y:10,a:Math.PI},preinstanciate:true,customAttributes:{id:"MAINrectoL"}},
								{name:"proto_engineSubachuRCS1",spawningZone:{x:65,y:15,a:Math.PI*.5},preinstanciate:true,customAttributes:{id:"RCStopFrontR"}},
								{name:"proto_engineSubachuRCS1",spawningZone:{x:65,y:-15,a:-Math.PI*.5},preinstanciate:true,customAttributes:{id:"RCStopFrontL"}},
								{name:"proto_engineSubachuRCS1",spawningZone:{x:-65,y:20,a:Math.PI*.5},preinstanciate:true,customAttributes:{id:"RCStopRearR"}},
								{name:"proto_engineSubachuRCS1",spawningZone:{x:-65,y:-20,a:-Math.PI*.5},preinstanciate:true,customAttributes:{id:"RCStopRearL"}},
								
								
								
								// DBG
//								{name:"proto_engineSubachu1",spawningZone:{x:-60,y:-20,a:Math.PI},preinstanciate:true,},
//								{name:"proto_engineSubachu1",spawningZone:{x:-100,y:50,a:Math.PI*1.5},preinstanciate:true,},
//								{name:"proto_engineSubachu1",spawningZone:{x:-100,y:-50,a:Math.PI*1.75},preinstanciate:true,},
//								// DBG
//								{name:"proto_engineSubachu1",spawningZone:{x:40,y:20,a:Math.PI*.25},preinstanciate:true,customAttributes:{id:"MAINretroL"}},

							]
						},
					},
				},
				
				
		
		
				EnginesControlsSchema:{
				
					commands:{
					
						angleChange:{
							"left":["RCStopFrontL","RCStopRearR"],
							"right":["RCStopFrontR","RCStopRearL"],
						},
						thrustChange:{
							"forward":["MAINrectoR","MAINrectoL"],
							"backward":["MAINretroR","MAINretroL"],
							"strifeLeft":["RCStopFrontL","RCStopRearL"],
							"strifeRight":["RCStopFrontR","RCStopRearR"],
							
						},
						
					},
				
				},
				
				
				
				
				// FOR NOW : 
//				GameCharacter:{
//					cardinality:{
//						value:"staticGroups",
//						prototypesInstantiation:{
//							staticInstantiations:[
//								{name:"proto_humanii1",spawningZone:{x:0,y:0},preinstanciate:true,},
//								
//							]
//						},
//					},
//				
//				},
				
				
				
				
				
								
			},
		
		},
		
		
		Engine:{
			proto_engineSubachu1:{
			
				power:10000,// (in newtons)
				consumption:2,// (in liters of fuel per second)
			
			
				size:{w:40,h:16},
			
				
			
				imageConfig:{
					_2D:{
						
						zIndex:2,						
						center:{x:"center",y:"center"},
//						center:{x:"left",y:"center"},
						
						chiralities:{
							right:{
								sourcePath:"engine.right.png",
								// sourcePathDark:"engine.right.dark.png",
								// sourcePathBump:"engine.right.bump.png",
							},
							left:{
								sourcePath:"engine.left.png",
								// sourcePathDark:"engine.left.dark.png",
								// sourcePathBump:"engine.left.bump.png",
							},
						},
						
//						// Animations :
//						refreshMillis:200,
//						clipSize:{w:200,h:40},
//						animations:{
//							thrust:"thrust.png",
//						},
							
					},
				},
				
								
				thrustFlameImageConfig1:{
					_2D:{
						center:{x:"left",y:"center"},
						
						// Animations :
						refreshMillis:200,
						clipSize:{w:200,h:40},
						size:{w:100,h:20},
//						offsets:{x:-10,y:10},
						animations:{
							"default":"thrust.png",
						},
					},
				},
				
				
				
				
				
			},
			
			proto_engineSubachuRCS1:{
				power:4000,
				consumption:.01,
				
				size:{w:20,h:8},
				imageConfig:{
					_2D:{
						
						zIndex:2,						
						center:{x:"center",y:"center"},
//						center:{x:"left",y:"center"},
						
						chiralities:{
							right:{
								sourcePath:"engine.right.png",
								// sourcePathDark:"engine.right.dark.png",
								// sourcePathBump:"engine.right.bump.png",
							},
							left:{
								sourcePath:"engine.left.png",
								// sourcePathDark:"engine.left.dark.png",
								// sourcePathBump:"engine.left.bump.png",
							},
						},
						
//						// Animations :
//						refreshMillis:200,
//						clipSize:{w:50,h:10},
//						animations:{
//							thrust:"thrust.png",
//						},
							
					},
				},
				
				
				thrustFlameImageConfig1:{
					_2D:{
						center:{x:"left",y:"center"},
						
						// Animations :
						refreshMillis:200,
						clipSize:{w:50,h:10},
						size:{w:25,h:5},
//						offsets:{x:-10,y:10},
						animations:{
							"default":"thrust.png",
						},
					},
				},
				
				
				
				
			
			},
		
		},
		
		
		
		
//		GameCharacter:{
//			
//			proto_humanii1:{
//				
//				imageConfig : {
//					_2D : {
//						center:{x:"center",y:"center"},
//						refreshMillis:200,
//						clipSize:{w:87,h:200},
////						idles : {
////							triggerAfterMillis : 4000,
////							sourcesPaths : [
////								"idle1.png", "idle2.png", "idle3.png"
////							],
////						},
//						isometric : {
//							walkNE : "walkNE.gif",
//							walkNW : "walkNW.gif",
//							walkSE : "walkSE.gif",
//							walkSW : "walkSW.gif",
//							standby : "standby.gif",
////						standbyRight : "standR.png",
//						}
//					}
//				},
//				
//				
//			},
//		},

		
		
		
	},
		
		

};
