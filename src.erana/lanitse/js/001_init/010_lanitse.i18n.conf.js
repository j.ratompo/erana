// DEPENDENCY:init.js




// ===============================================================================
// CONFIG
// ===============================================================================

i18n.declare({

	"commencer":{"capitalize":true,"fr":"commencer","en":"start"},
	"prochainArrêt":{"capitalize":true,"append":": ","fr":"prochain arrêt","en":"next stop"},
	"propulséParEranaBrSpanStyleFontSize7emUnMoteurOuv":{"fr":"Propulsé par erana<br><span style=´font-size:.7em´>Un moteur ouvert de jeu (licence HGPL)</span>","en":"Powered by erana<br><span style=´font-size:.7em´>Open-source javascript game engine (HGPL license)</span>"},
	"leStudioAlqemiaPrésente":{"fr":"Le studio Alqemia présente","en":"Alqemia studio presents"},
	"soutenir":{"capitalize":true,"fr":"soutenir!","en":"support!"},
	"attentionCeciEstUneVersionAlphaBrDesBugsInacceptable":
	{"fr":"ATTENTION ! Ceci est une version démo alpha <strong>NON OPTIMISÉE</strong> (±15 min de jeu)<br/>" +
			"Des bugs inacceptables sont encore présents<br/>" +
			"et des fonctionnalités indispensables sont encore manquantes." +
			"<br/><br/><br/><br/><br/><br/><br/><br/>" +
			"Nous nous en excusons et nous vous remercions de votre patience !<br/>" +
			"Bonne aventure !<br/>" +
			"<br/><br/><br/>" +
			"<small>(icône : Bug par Nociconist du Noun Project)</small>",
		"en":"CAUTION ! This is a <strong>NOT OPTIMIZED</strong> demo alpha version (±15 min of play)<br/>" +
			"Unacceptable bugs are still present<br/>" +
			"and necessary features are not here yet." +
			"<br/><br/><br/><br/><br/><br/><br/><br/>" +
			"We apologize for this and we thank you for your patience!<br/>" +
			"Good adventure !<br/>" +
			"<br/><br/><br/>" +
			"<small>(icon : Bug by Nociconist from the Noun Project)</small>"},
	"ok":{"fr":"OK","en":"OK"},
	"unDispositifDÉcouteEstConseilléBrPourUneMeilleureExpé":{
		"fr":"Un dispositif d´écoute est conseillé<br/>" +
				"pour une meilleure expérience" +
				"<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>" +
				"<small>(icône : Headset par mahdalenyy du Noun Project)</small>",
		"en":"Audio set advised <br/>" +
				"for a better experience" +
				"<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>" +
				"<small>(icon : Headset by mahdalenyy from the Noun Project)</small>"},
	"continuer":{"fr":"Continuer","en":"Continue"},
	"crédits":{"fr":"Crédits","en":"Credits"},
	"quitter":{"fr":"Quitter","en":"Exit"},
	"retourALAccueil":{"fr":"Retour à l´accueil", "en":"Back to Home",},
	"menu":{"fr":"Menu","en":"Menu"},
	"passer":{"fr":"Passer","en":"Skip"},
	"reprendre":{"fr":"Reprendre","en":"Resume"},
	"sauverEtRevenirÀLAccueil":{"fr":"Sauver et revenir à l´accueil","en":"Home"},
	
	// =================================================================================================================

	"programmationJeremieR":{"fr":"Programmation : Jérémie R.","en":"Programming : Jeremie R."},
	"graphiquesJérémieR":{"fr":"Graphiques : Jérémie R.","en":"Graphics : Jeremie R."},
	"écritureJeremieR":{"fr":"Écriture : Jeremie R.","en":"Writing : Jeremie R."},
	"sonsJeremieR":{"fr":"Sons : Jérémie R.","en":"Sounds : Jeremie R."},
	"communicationJeremieR":{"fr":"Communication : Jérémie R.","en":"PR : Jeremie R."},
	"musiquePar":{"fr":"Musique : Jérémie R.","en":"Music : Jeremie R."},
	"remerciementsSupportMoralDominiqueB":{"fr":"Remerciements (support moral): Dominique B.","en":"Thanks to (moral support): Dominique B."},

	
	
});


//i18n.declareSplitted({
//"fr":{ "commencer":"Commencer", },
//"en":{ "commencer":"Start",},
//});

