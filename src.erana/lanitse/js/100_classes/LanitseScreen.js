
class LanitseScreen extends GameScreen2D {

	constructor(mainId,config) {
		super(mainId,config,"lanitse-saved");

	}
	
	// INIT ON START

	initOnStartScreenAtLevelStart(){

		super.initOnStartScreenAtLevelStart();
		
		const rootContainer=this.getSubclassRootContainer();
		
		// DBG
		console.log("initOnStartScreenAtLevelStart Screen",rootContainer);
		
		
//		// DBG
//		foreach(rootContainer.stations,(station)=>{
//			lognow("station.position:",station.position);
//		});
		

	}

	
	//=============== MODEL METHODS ===============
	
	/*protected*/getSubclassRootContainer(){
		if(this.currentLevel.isCinematic)
			return this.currentLevel.gameCinematic;
		return this.currentLevel.universe;
	}
	
	


	// METHODS
	

	// =============== CONTROLLER METHODS ===============
	
	
	
	
	// ACTIONS (for each page)

	startCinematic(){
		
		// We stop the current game, if existing : 
//		super.stopGame();
		
		if(!this.currentLevel.isCinematic)	return;
		
		
		// We start moving the cinematic scroll :
		// WORKAROUND : after some time, to allow all model to be loaded ! :
		let self=this;
		setTimeout(function(){
			
			let cinematic=self.getSubclassRootContainer();
			
			
			if(cinematic){
				
				let camera=self.getCamera();
				cinematic.start(camera);
				
			}
			
			
		},1000);
		
	}
	
	stopCinematic(){
		
		let cinematic=this.getSubclassRootContainer();
		if(cinematic){
			cinematic.stop();
		}
		
	}
	
	
	
	//homePage
	
	startNewGame(){
	
		
	}
	
	continueGame(){	
	

	}
	
	startCredits(){	}


	//gamePage
	resumeGame(){	}
	
	interruptGame(){ 

		// We save the current game :
		this.saveGame();
		this.abandonGame();
		
	}
	
	
	
	abandonGame(){
		super.abandonGame();
//	super.exitGame();
		this.pagesManager.goToPage("homePage");
	}
	
	
//	/*public*/endDemo(){
//		this.pagesManager.goToPage("endOfDemoCinematicPage");
//		this.startCinematic();
//		
//	}
	
	
	//creditsPage
//	exitCredits(){	/*DO NOTHING*/ }
	

	
	
	
	// METHODS
	
	
	
	
	
	// FACULTATIVE PUBLIC METHODS
	
	getDefaultSelectedItem(){
		return this.getSubclassRootContainer().getSelectedItem();
	}
	

	
	

	// MANDATORY METHODS
	
	
//	// DBG ONLY
//	/*protected*/getCurrentBackgroundsToOverride(){
//		let results=[];
//		return results;
//	}
	
	
}


