
class EnginesControlsSchema{

	constructor(){
	

		this.enginesIdsToTrigger={};
		

	}
	
	
	init(gameConfig){
		this.gameConfig=gameConfig;
		
	}


	doOnAngleChange(direction, thrustFactor=1){
		
		this.enginesIdsToTrigger["angleChange"]=this.getEnginesIdsForAngleChange(direction);
	
		this.triggerEngines("angleChange", thrustFactor);
	}
	
	
	/*private*/getEnginesIdsForAngleChange(direction){
		if(direction=="left"){	// left circle
			return this.commands.angleChange.left;
		}												// right circle
		return this.commands.angleChange.right;
	}
	
	/*public*/getEnginesForAngleChange(direction){
		const results=[];
		
		const self=this;
		foreach(this.getEnginesIdsForAngleChange(direction),engineId=>{
			const engine=self.parent.findEngineById(engineId);
			if(!engine)	return "continue";
			results.push(engine);
		});
		return results
	}


// 	TODO : FIXME :
//	doOnSetGoalAngle(clientX,clientY){
//			...
//	}

//	doOnAngleChange(deltaAngle, destinationAngle){
//	
//		const DELTA_ANGLE_MINIMUM_TRIGGER=Math.TAU*.001;
//		
//		// At this point, we know we have to change the object's angle, to reach the destination angle, and reduce the angle delta !
//		let positiveDelta=Math.coerceAngle(deltaAngle,true,true);
//		if(positiveDelta<Math.PI){	// left circle
//			this.enginesIdsToTrigger["angleChange"]=this.commands.angleChange.clockwise;
//		}else{						// right circle
//			this.enginesIdsToTrigger["angleChange"]=this.commands.angleChange.antiClockwise;
//		}
//	
//		this.triggerEngines("angleChange");
//	}
	
	
	doOnThrustChange(direction, thrustFactor=1){
	
		// At this point, we know we have to change the object's position, to reach the destination, and reduce the position delta !
		const engines=this.getEnginesIdsForThrustChange(direction);
		if(!engines)	return;	
		this.enginesIdsToTrigger["thrustChange"]=engines;
	
		this.triggerEngines("thrustChange", thrustFactor);
	}
	
	/*private*/getEnginesIdsForThrustChange(direction){
		
		// At this point, we know we have to change the object's position, to reach the destination, and reduce the position delta !
		return this.commands.thrustChange[direction];
		
	}

	
	
	
//	doOnThrustChange(deltaAngle, destinationPosition){
//	
//		const DELTA_POSITION_MINIMUM_TRIGGER=1;
//	
//		// At this point, we know we have to change the object's position, to reach the destination, and reduce the position delta !
//		if(this.isDeltaAngleInTowardsCone(deltaAngle)){ // In the «towards cone» :
//			this.enginesIdsToTrigger["thrustChange"]=this.commands.thrustChange.towards;
//		}else{	// In the «away cone» :
//			this.enginesIdsToTrigger["thrustChange"]=this.commands.thrustChange.away;
//		}
//	
//	
//		this.triggerEngines("thrustChange");
//	}
	
	/*private static*/isDeltaAngleInTowardsCone(deltaAngle){
		const positiveDelta=Math.coerceAngle(deltaAngle,true,true);
		const result=(positiveDelta<Math.PSI || Math.PI+Math.PSI<positiveDelta); // In the front cone, in the rear cone otherwise.
		return result;
	}


	/*private*/triggerEngines(commandType, thrustFactor=1){
		
		const self=this;
		foreach(this.enginesIdsToTrigger[commandType],(engineId)=>{
			const engine=self.parent.findEngineById(engineId);
			if(!engine)	return "continue";
			engine.trigger(thrustFactor);
		});
	
	}


	//----------------------------------------------------------------
	/*public*/doStep(gamePhysicsResolver){
	
		const concernedObject=this.parent;
		
//		const angle=concernedObject.getPosition().getAngle2D();
		
		// TODO : ...
		
		
		if(gamePhysicsResolver.getMaxSpeeds()){
			
		}
		
	
	}
	


}
