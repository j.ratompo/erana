
class Device{

	constructor(){
	
		this.isVisible=true;
		
		this.position=new Position(this);
		
	
		this.img=new GameImage(this);
		
		this.chirality="right";
	
	}

	// INIT
	init(gameConfig){
		
		this.gameConfig=gameConfig;
	
	
	}
	
	initWithParent(){

		this.position.center=this.imageConfig._2D.center;
		
	}
	
	initAfterSetPositionFirstTime(){
		this.chirality=((this.parent.position.getParentPositionOffsetted().y-this.position.getParentPositionOffsetted().y)<0?"left":"right");
		this.img.init(this.gameConfig);
	}
	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	

	getGameImage(){
		return this.img;
	}
	
	getChirality(){
		return this.chirality;
	}
	
	// --------------------------------------------------------------------------------------------
	
	draw(ctx,camera,lightsManager){
		return this.getGameImage().draw(ctx,camera,lightsManager);
	}

}
