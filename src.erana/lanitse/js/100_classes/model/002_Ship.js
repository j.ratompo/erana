class Ship extends SpacePlace{
	
	constructor(){
		super();
	
	
		this.isVisible=true;
		this.isMovable=true;

		this.position=new Position(this);
		this.position.setAngle2D(0);
		
		this.imgs={inside:new GameImage(this), outside:new GameImage(this)};
	
		this.viewMode="outside";
		
		this.speed=0;
		this.goalSpeed=0;
		
		
		this.swipingCommand={x:null, y:null, routine:null};
		
		this.isAutoAngularPiloting=true;
		
		
	}
	
	
	// INIT
	init(gameConfig){
		
		this.gameConfig=gameConfig;

		this.position.center=this.imageConfigs.outside._2D.center;
		
		this.imgs.inside.init(gameConfig,this.imageConfigs.inside);
		this.imgs.outside.init(gameConfig,this.imageConfigs.outside);
		
		
		
	}



	/*public*/doOnStartSwipe(mouseX, mouseY){
	
		this.swipingCommand.x=mouseX;
		this.swipingCommand.y=mouseY;
		
		/*
		if(!this.swipingCommand.routine){
			
			const self=this;
			self.swipingCommand.routine=setInterval(()=>{
				self.doAngularThrusting();
			},300);
			
			//const mouseX=this.swipingCommand.x;
			//const mouseY=this.swipingCommand.y;
			//const camera=window.eranaScreen.getCamera();
			
			//const shipDrawableCoords=getDrawableZoomedIfNecessaryCoordinates2D(this.gameConfig, this.position, camera);
			//const relativePointer={x:(mouseX-shipDrawableCoords.x),y:-(mouseY/ *CAUTION : Y AXIS IS INVERTED* /-shipDrawableCoords.y)};
			
			//let pointerAngle=calculateAngleRadians2D({x:0,y:0},relativePointer); 
			//let shipAngle=this.position.getAngle2D();
			
			//this.originalAngleDiff=Math.roundTo(Math.getAnglesDiffOnDemiCircle(shipAngle,pointerAngle), 4);
			//this.hasStartedAutopilotAngularThrusting=true;
		}
		*/
		
		
		if(this.isAutoAngularPiloting){
			
			const camera=window.eranaScreen.getCamera();
			const shipDrawableCoords=getDrawableZoomedIfNecessaryCoordinates2D(this.gameConfig, this.position, camera);
			const relativePointer={x:(mouseX-shipDrawableCoords.x),y:-(mouseY/*CAUTION : Y AXIS IS INVERTED*/-shipDrawableCoords.y)};
//		const pointerNorm=Math.getDistance({x:0,y:0},relativePointer);
			let pointerAngle=calculateAngleRadians2D({x:0,y:0},relativePointer); 
			
			this.angularDestinationAuto=pointerAngle;


			// DBG
			lognow("this.angularDestinationAuto:"+Math.toDegrees(this.angularDestinationAuto));

			
			// TODO : Put this in a monothreaded routine :
	
			// CURRENT
			const shipAngle=this.position.getAngle2D();
			const angleDiff=Math.roundTo(Math.getAnglesDiffOnDemiCircle(shipAngle, pointerAngle), 4);

			const momentum=this.getGamePhysicsResolver().getComposedForceAndMomentum().momentumm;



		}
		
	}
	
	/*private*/getGamePhysicsResolver(){
		return this.parent.gamePhysicsResolver;
	}


	/*public*/doOnSwiping(deltaMouseX, deltaMouseY, mouseX, mouseY, followers){
		this.swipingCommand.x=mouseX;
		this.swipingCommand.y=mouseY;
		
		if(!this.isAutoAngularPiloting){
			this.doAngularThrustingManual();
		}
		
	}
	
	/*public*/doOnEndSwipe(swipeInputDirectionAngle){
		
//		if(this.swipingCommand.routine){
//			clearInterval(this.swipingCommand.routine);
//			this.swipingCommand.routine=null;
//		}
		
		
	}
	
//
//	/*private*/doAngularThrusting(){
//		
//		const mouseX=this.swipingCommand.x;
//		const mouseY=this.swipingCommand.y;
//		
//		const camera=window.eranaScreen.getCamera();
//		const shipDrawableCoords=getDrawableZoomedIfNecessaryCoordinates2D(this.gameConfig, this.position, camera);
//		const relativePointer={x:(mouseX-shipDrawableCoords.x),y:-(mouseY/*CAUTION : Y AXIS IS INVERTED*/-shipDrawableCoords.y)};
//		const pointerNorm=Math.getDistance({x:0,y:0},relativePointer);
//		let pointerAngle=calculateAngleRadians2D({x:0,y:0},relativePointer); 
//		
////		// We set the angle on two right-left hemispheres only, not the whole circle !
////		if(Math.PI<pointerAngle)	pointerAngle=-(Math.TAU-pointerAngle);
//
//	
//		const angleDiff=Math.roundTo(Math.getAnglesDiffOnDemiCircle(this.position.getAngle2D(),pointerAngle), 4);
//			
//		// DBG
//		lognow("pointerNorm:"+pointerNorm+" angleDiff:"+angleDiff);
//		
////		const POINTER_NORM_TRIGGER_MARGIN=1;
//		const POINTER_NORM_MAX_LENGTH=100;
////		if(POINTER_NORM_TRIGGER_MARGIN<pointerNorm){
//		
//			
//			const gamePhysicsResolver=this.getGamePhysicsResolver();
//			const maxSpeeds=gamePhysicsResolver.getMaxSpeeds();
//			
////			let angularThrustFactor=1;
////			if(maxSpeeds && maxSpeeds.a){
////				const currentAngularSpeed=this.physicableMover.angularSpeed;
////				angularThrustFactor=(currentAngularSpeed/maxSpeeds.a);
////			}
//
//
//			const absoluteAngleDiff=Math.abs(angleDiff);
//			if(angleDiff<0){
//			
////				angularThrustFactor=((absoluteAngleDiff%Math.PSI)/Math.PSI);
//				
//				if(absoluteAngleDiff<Math.PSI){
//					
//					this.enginesControlsSchema.doOnAngleChange("right");
////					this.enginesControlsSchema.doOnAngleChange("left");
//					
//				}else{
//					// INVERTED IF WE GO BACKWARD :
////					this.enginesControlsSchema.doOnAngleChange("left");
//					this.enginesControlsSchema.doOnAngleChange("right");
//					
//				}
//			}else{
//				if(absoluteAngleDiff<Math.PSI){
//					this.enginesControlsSchema.doOnAngleChange("left");
////					this.enginesControlsSchema.doOnAngleChange("right");
//					
//				}else{
//					// INVERTED IF WE GO BACKWARD :
////					this.enginesControlsSchema.doOnAngleChange("right");
//					this.enginesControlsSchema.doOnAngleChange("left");
//					
//				}
//				
////			}
//			
//		
//		}
//		
//		
//	}



	/*private*/doAngularThrustingManual(){
		
		
		//if(!this.hasStartedAutopilotAngularThrusting)	return;
		
		
		const mouseX=this.swipingCommand.x;
		const mouseY=this.swipingCommand.y;
		const camera=window.eranaScreen.getCamera();
		
		const shipDrawableCoords=getDrawableZoomedIfNecessaryCoordinates2D(this.gameConfig, this.position, camera);
		const relativePointer={x:(mouseX-shipDrawableCoords.x),y:-(mouseY/*CAUTION : Y AXIS IS INVERTED*/-shipDrawableCoords.y)};
		
		let pointerAngle=calculateAngleRadians2D({x:0,y:0},relativePointer); 
		let shipAngle=this.position.getAngle2D();
		
		
		
		
		// We set the angle on two right-left hemispheres only, not the whole circle !
		if(Math.PI<pointerAngle)	pointerAngle=-(Math.TAU-pointerAngle);
		if(Math.PI<shipAngle)			shipAngle=-(Math.TAU-shipAngle);

		const angleDiff=Math.roundTo(Math.getAnglesDiffOnDemiCircle(shipAngle,pointerAngle), 4);
		
		
		if(angleDiff<0){
//			const factor=this.getAngularThrustFactor("right",angleDiff);
//			this.enginesControlsSchema.doOnAngleChange("right",factor);
			this.enginesControlsSchema.doOnAngleChange("right");
		}else{
//			const factor=this.getAngularThrustFactor("left",angleDiff);
//			this.enginesControlsSchema.doOnAngleChange("left",factor);
			this.enginesControlsSchema.doOnAngleChange("left");
		}
		
		
//		if(angleDiff<0){
//			if(angleDiff<this.originalAngleDiff*.5){
//				this.enginesControlsSchema.doOnAngleChange("right");
//			}else{
//				this.enginesControlsSchema.doOnAngleChange("left");
//			}
//		}else{
//			if(this.originalAngleDiff*.5<angleDiff){
//				this.enginesControlsSchema.doOnAngleChange("left");
//			}else{
//				this.enginesControlsSchema.doOnAngleChange("right");
//			}
//		}
//		if(Math.abs(angleDiff)<Math.PI*.1){
//			this.hasStartedAutopilotAngularThrusting=false;
//		}

		
		
			
	}

	/*private*/getAngularThrustFactor(direction, angleDiff){
		
		const refPosition=this.getPosition().getGlued2D();
		const enginesMaxForces=[];
		const currentComposedForceAndMomentum=this.getGamePhysicsResolver().getComposedForceAndMomentum();
		if(!currentComposedForceAndMomentum || currentComposedForceAndMomentum.momentum==0)	return 1;
		const angularFactor=Math.abs(angleDiff)/Math.PI;

		const concernedEngines=this.enginesControlsSchema.getEnginesForAngleChange(direction);
		foreach(concernedEngines,engine=>{enginesMaxForces.push(engine.getMaxForce());});
		const maxComposedForceAndMomentum=this.getGamePhysicsResolver().calculateComposedForceAndMomentum(refPosition, enginesMaxForces);
		let factor=Math.min(1, (Math.abs(currentComposedForceAndMomentum.momentum)/Math.abs(maxComposedForceAndMomentum.momentum)) );
		
		return factor;
	}

	
//	/*public*/startMovingTo(destination,followers){
//		const DELTA_ANGLE_MINIMUM_TRIGGER=Math.TAU*.001;
//		const DELTA_POSITION_MINIMUM_TRIGGER=1;
//	
//		// DBG
//		lognow("Ship startMovingTo");
//		
//		let offsettedPos=this.position.getParentPositionOffsetted();
//		
//		let angleToDestination=calculateAngleRadians2D(offsettedPos, destination);
//		let deltaAngle=Math.coerceAngle(angleToDestination-offsettedPos.getAngle2D(),true);
//		if(DELTA_ANGLE_MINIMUM_TRIGGER<=Math.abs(deltaAngle)){
//			this.enginesControlsSchema.doOnAngleChange(deltaAngle, angleToDestination);
//		}
//
//		let deltaPosition={x:destination.x-offsettedPos.x, y:destination.y-offsettedPos.y};
//		if(DELTA_POSITION_MINIMUM_TRIGGER<=Math.abs(deltaPosition.x) || DELTA_POSITION_MINIMUM_TRIGGER<=Math.abs(deltaPosition.y)){
//			this.enginesControlsSchema.doOnThrustChange(deltaAngle, destination);
//		}
//	}
	
	/*public*/startCommand(commandName, event=null){
	
		const STEP_GOAL_SPEED=5;
	
		const enginesControlsSchema=this.enginesControlsSchema;
		
		// CAUTION : ONLY CHANGES THE GOAL SPEED !!!
		if(commandName=="accelerate"){
			this.goalSpeed=Math.min(this.maxGoalSpeed,this.goalSpeed+STEP_GOAL_SPEED);
			
		}else if(commandName=="decelerate"){
			this.goalSpeed=Math.max(0,this.goalSpeed-STEP_GOAL_SPEED);
					
		}else if(commandName=="move"){
			enginesControlsSchema.doOnThrustChange("forward");

		}else if(commandName=="stop"){
			enginesControlsSchema.doOnThrustChange("backward");
		
		}else if(commandName=="rotateLeft"){
			enginesControlsSchema.doOnAngleChange("left");
		}else if(commandName=="rotateRight"){
			enginesControlsSchema.doOnAngleChange("right");
		
		}else if(commandName=="strifeLeft"){

			enginesControlsSchema.doOnThrustChange("strifeLeft");

		}else if(commandName=="strifeRight"){

			enginesControlsSchema.doOnThrustChange("strifeRight");
		
		
		
		}else if(commandName=="rotateLeftDEBUG"){
			// DBG
			this.position.setAngle2D(Math.coerceAngle(this.position.getAngle2D()-Math.PI*.05,true,true));
		}else if(commandName=="rotateRightDEBUG"){
			// DBG
			this.position.setAngle2D(Math.coerceAngle(this.position.getAngle2D()+Math.PI*.05,true,true));
		}else if(commandName=="moveUpDEBUG"){
			// DBG
			const destination=calculateLinearlyMovedPoint2DPolar(this.position,this.position.getAngle2D(),5);
			this.position.setLocation(destination);
		}else if(commandName=="moveDownDEBUG"){
			// DBG
			const destination=calculateLinearlyMovedPoint2DPolar(this.position,this.position.getAngle2D(),-5);
			this.position.setLocation(destination);
		
		
		// TODO : FIXME :		
//		}else if(commandName=="setGoalAngle"){
//			enginesControlsSchema.doOnSetGoalAngle(event.clientX,event.clientY);

		
		}else{
			// TRACE
			lognow("WARN : Unsupported recognized command : «"+commandName+"». Doing nothing.");
			return;
		}
		
	}
	
	/*public*/findDeviceById(deviceId){
		return foreach(this.devices,(device)=>{
			if(device.id===deviceId)	return device;
		});
	}
	
	/*public*/findEngineById(engineId){
		return foreach(this.engines,(engine)=>{
			if(engine.id===engineId)	return engine;
		});
	}
	
	/*private*/getSpeed(){
		return nonull(this.speed,10);
	}


	getMover(){
		return this.mover;
	}
	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}

	getIsMovable(){
		return this.isMovable;
	}
	setIsMovable(isMovable){
		this.isMovable=isMovable;
	}
	
	getGameImages(){
		return this.imgs;
	}
	
	
	getDrawables(){
		return [];// OVERRIDEN
	}
	getPlaceables(){
		return [this.engines,this.devices];
	}
	
	getPosition(){
		return this.position;
	}
	
	
	getAllForces(){
		const results=[];
		
		foreach(this.engines,(engine)=>{
			const force=engine.updateAndGetForce();
			if(force)	results.push(force);
		},(e)=>{	return e.getIsFunctionning();	});
		
		return results;
	}
	
	
	// --------------------------------------------------------------------------------------------
	
	
	drawOutside(ctx,camera,lightsManager){
	
		const self=this;
		
		foreach(this.devices,device=>{
			device.draw(ctx,camera,lightsManager);
		});
		foreach(this.engines,engine=>{
			engine.draw(ctx,camera,lightsManager);
		});
		
		
		if(IS_DEBUG){
			this.collisionZone.draw(ctx,camera,lightsManager);
		}		
		
		
	
	}
	
	
	
	
	draw(ctx,camera,lightsManager){
	
		let viewMode=this.viewMode;
		
		
		let result=this.getGameImages()[viewMode].draw(ctx,camera,lightsManager);
		
		// SPECIAL CASE :
		if(viewMode=="outside"){
		
			this.drawOutside(ctx,camera,lightsManager);
			
		}
		
		
		this.enginesControlsSchema.doStep(this.getGamePhysicsResolver());
		

		return result;
	}
	
	
}