
const DEFAULT_ACTIVATION_TIME_MILLIS=300;

class Engine extends Device{

	constructor(){
		super();
		
		
		this.isFunctionning=false;
		
		this.thrustFlameAnimation=new GameAnimation(this);
		
		
		this.activationTime=null;
		this.activationTimeMillis=null;
		
		this.force=null;
		
		this.thrustFactor=1;
		
	}



	init(gameConfig){
		super.init(gameConfig);
		
		this.activationTimeMillis=nonull(this.activationTimeMillis, DEFAULT_ACTIVATION_TIME_MILLIS);
		
		this.thrustFlameAnimation.init(this.gameConfig).loadSprite2DAndSet("default", "thrustFlameImageConfig1", this.thrustFlameImageConfig1._2D.size).start();
	}
	
	
	
	
	/*public*/trigger(thrustFactorParam=1){
		const thrustFactor=Math.abs(thrustFactorParam);
		if(this.thrustFactor!=thrustFactor)	this.thrustFactor=thrustFactor;
		this.activationTime=getNow();
		if(this.getIsFunctionning())	return;
		this.isFunctionning=true;
	}

	/*private*/untrigger(){
		if(!this.getIsFunctionning())	return;
		this.isFunctionning=false;
	}
	
	/*public*/getIsFunctionning(){
		return this.isFunctionning;
	}
	
	
	/*private*/getForce(){
		return this.force;
	}	
	
	
	/*public*/updateAndGetForce(){
		if(!this.getIsFunctionning())	return null;

		const gluedPosition=this.getPosition().getGlued2D();
		
		if(!this.force){
			this.force=new GameForce(gluedPosition.x, gluedPosition.y,
				null,
				// Inverted because engines produce a REPULSIVE force, so at the opposite of their thrust direction !
				-this.power*.001,// We use kN here, not just N
				null,null,gluedPosition.getAngle2D());
		}else{
			const gluedPosition=this.getPosition().getGlued2D();
			this.force.setLocation(gluedPosition);
		}
		
		this.force.setNormByFactor(this.thrustFactor);
		
		return this.force;
	}
	
	
	/*public*/getMaxForce(){

		const gluedPosition=this.getPosition().getGlued2D();
		
		const resultForce=new GameForce(gluedPosition.x, gluedPosition.y,
			null,
			// Inverted because engines produce a REPULSIVE force, so at the opposite of their thrust direction !
			-this.power*.001,// We use kN here, not just N
			null,null,gluedPosition.getAngle2D());
		
		resultForce.setNormByFactor(1);
		
		return resultForce;
	}
	

	getPosition(){
		return this.position;
	}


	// --------------------------------------------------------------------------------------------


	draw(ctx,camera,lightsManager){
	
		if(!super.draw(ctx,camera,lightsManager))	return false;
		
		if(this.getIsFunctionning()){
		
			if(this.activationTime && this.activationTimeMillis && hasDelayPassed(this.activationTime,this.activationTimeMillis)){
				this.untrigger();
			}
		
			this.thrustFlameAnimation.draw(ctx,camera,lightsManager);

			
			const force=this.getForce();
			if(force){
				const gluedPosition=this.getPosition().getGlued2D();
				force.setLocation(gluedPosition);
				
				if(IS_DEBUG){
					force.draw(this.gameConfig,ctx,camera);
				}
			}
			
		}
		
		
		
		
		return true;
	}


	

}