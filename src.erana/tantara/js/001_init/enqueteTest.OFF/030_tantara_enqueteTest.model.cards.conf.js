// DEPENDENCY:init.js

// ===============================================================================
// MODEL
// ===============================================================================

// Prototype can define as a well a group of things as particular
// individuals:only by not setting a random variance on them.
// ***
// CAUTION : The attribute «usesSuper» must always be the first attribute ! (All attribute put before will be ignored).
// ***

/*OVERRIDES*/PROTOTYPES_CONFIG.allPrototypes.Card={
			
	
	proto_abstract_card:{
		angleDegrees:"-2->2",
		fontSizePercent:5,
		
		cardsSetPath:"enqueteTest/",
//		imageSrc:"defaultCard.png",
	
		size:{w:GAME_CONFIG.getResolution().w*.85, h:GAME_CONFIG.getResolution().h*.8},
	
		hintSize:{w:GAME_CONFIG.getResolution().w*.5, h:250},
		
		hintRelativePosY:GAME_CONFIG.getResolution().h*.15,

		overridingPosition:{
			x:GAME_CONFIG.getResolution().w*.5 - 20, y:GAME_CONFIG.getResolution().h*.5
		},
		
		wheelChoiceConfig:{
//			timeoutConfig:{delayMillis:20000,defaultIndex:0},
			hideChoiceLabels:true,
		},
		
//		// DBG			
//		debugAttr:getUUID("short"),
		
		
	},
	
	// -----------------------------------------------------------------------------------
	// Choice of character :
	// -----------------------------------------------------------------------------------
	
	
	//proto_cardWaitForPlayers:{
	//	usesSuper:"proto_abstract_card",
	//	imageSrc:null,
	//	title:"L'enquête test",
	//	description:"En attente de joueurs...",	
	//	first:true,
	//	choicesLabels:["Start!"],
	//	Card:["proto_cardWelcome"],
	//	effect:{
	//		action:{"startWaitingForPlayers":[]}
	//	},
	//},
	
	// -----------------------------------------------------------------------------------
	// Choice of character :
	// -----------------------------------------------------------------------------------
	
	// TODO : Develop...
	
	proto_cardWelcome:{ 
		usesSuper:"proto_abstract_card",
		cardIndex:1,
		first:true,
//		imageSrc:null,
		imageSrc:"iconSwipeHands.png",
		isImageDeviceDependent:true,
		title:"L'enquête test",
		description:"Bonjour et bienvenue dans l'enquête test. Comment ça va ? <br /><small>(faites glisser la carte selon votre choix)</small>",
//		choicesLabels:["Bien!","Moyen...","Ne souhaite pas répondre."],

// TODO : Faire une carte «consulter les trigger warning» à la place !

		choicesLabels:["Pas pire!","Ne souhaite pas répondre.","Moyen..."],
		
		
//		Card:["proto_firstCard_1","proto_firstCard_1_2","proto_card2"],
//		Card:["proto_firstCard_1","proto_firstCard_1_2","proto_card2","proto_card3"],
		Card:["proto_cardWelcome1","proto_cardWelcome1","proto_cardWelcome1"],

	},
		
		proto_cardWelcome1:{ usesSuper:"proto_abstract_card",
			cardIndex:2,
			description:"OK... tout d'abord qui êtes-vous ?",
	
			choices:[
				{
 				 label:"Michel Tremblay",
// 				 imageSrc:"MichelTremblay.png",
 				 constraints:{max:1},
 				 effect:{ isOnlyChooserClientAffected:true, action:{"setRoleCharacterByPrototypeName":["proto_roleCharacterMichelTremblay"]} },
 				},
				{
				 label:"Marie Gagnon",
//				 imageSrc:"MarieGagnon.png",
				 constraints:{max:1},
				 effect:{ isOnlyChooserClientAffected:true, action:{"setRoleCharacterByPrototypeName":["proto_roleCharacterMarieGagnon"]} },	
				}
			],
			Card:["proto_cardChooseMT","proto_cardChooseMG"],
		},
		
		
	
	proto_cardChooseMT:{ usesSuper:"proto_abstract_card",
		cardIndex:3,
		title:"Michel Tremblay",
		description:"Vous êtes un gars ben normal.<br/>Prêt ?",

//		choices:[{label:"Michel Tremblay",imageSrc:"MichelTremblay.png",constraints:{max:1}},{label:"Marie Gagnon",imageSrc:"MarieGagnon.png",constraints:{max:1}}],
//		Card:["proto_cardChooseMT","proto_cardChooseMG"],
		choicesLabels:["On y va !"],
		Card:["proto_cardStartMT"],

	},
		
//		proto_cardChooseMT1:{ usesSuper:"proto_abstract_card",
//			title:"Prêt ?",
//			description:"On y va !",
//			Card:["proto_cardStartMT"],
//		},
		
	
	
	proto_cardChooseMG:{ usesSuper:"proto_abstract_card",
		cardIndex:3,
		title:"Marie Gagnon",
		description:"Vous êtes une fille ben sympathique.<br/>Prête ?",
//		choices:[{label:"Michel Tremblay",imageSrc:"MichelTremblay.png",constraints:{max:1}},{label:"Marie Gagnon",imageSrc:"MarieGagnon.png",constraints:{max:1}}],
//		Card:["proto_cardChooseMT","proto_cardChooseMG"],
		choicesLabels:["C'est parti !"],
		Card:["proto_cardStartMG"],
	},
	
//		proto_cardChooseMG1:{ usesSuper:"proto_abstract_card",
//			title:"Prête ?",
//			description:"C'est parti !",
//			Card:["proto_cardStartMG"],
//		},
		
		
		
	// -----------------------------------------------------------------------------------
	// Scenario :
	// -----------------------------------------------------------------------------------
	
	
	// INTRODUCTION
	
	proto_cardStartMT:{ usesSuper:"proto_abstract_card",
		cardIndex:4,
		title:"Samedi matin",
		description:"À une terrasse, je lis un bon livre avec un café. Il y a un beau soleil.",
		Card:["proto_cardMGApprocheMT"],
	},
		
	proto_cardStartMG:{ usesSuper:"proto_abstract_card",
		cardIndex:4,
		title:"Samedi matin",
		description:"Assise à une terrasse, je bois mon café avec un bon livre. Il fait beau.",
		Card:["proto_cardMTApprocheMG"],
	},
	
		
	// ----
	proto_cardMGApprocheMT:{ usesSuper:"proto_abstract_card",
		cardIndex:5,
		title:"Une inconnue sympathique",
		description:"Je remarque une fille sympathique à la table juste à côté, et elle lit le même livre que moi !",
		choices:[
			{label:"Lui parler",effect:{ action:{"setVariable":["3MT","parler"]} },},
			{label:"L'ignorer", effect:{ action:{"setVariable":["3MT","ignorer"]} },}
		],
//		choicesLabels:["Lui parler","L'ignorer"],
		Card:["proto_cardParler1MT","proto_cardIgnorer1MT"],
	},
		
	proto_cardMTApprocheMG:{ usesSuper:"proto_abstract_card",
		cardIndex:5,
		title:"Un inconnu charmant",
		description:"Il y a un gars mignon à la table d'à côté, qui lit le même livre.",
		choices:[
			{label:"Lui dire bonjour",effect:{ action:{"setVariable":["3MG","parler"]} },},
			{label:"Continuer à lire",effect:{ action:{"setVariable":["3MG","ignorer"]} },}
		],
//		choicesLabels:["Lui dire bonjour","Continuer à lire"],
		Card:["proto_cardParler1MG","proto_cardIgnorer1MG"],
	},
		
		
		// ----
		proto_cardParler1MT:{ usesSuper:"proto_abstract_card",
			title:"Lui parler",
			description:"Je lui fais remarquer nonchalamment que je lis le même livre.",
			Card:["proto_cardParler2MT"],
		},
		proto_cardIgnorer1MT:{ usesSuper:"proto_abstract_card",
			title:"L'ignorer",
			description:"Je continue à lire sans la regarder.",
			Card:["proto_cardIgnorer2MT"],
		},
		
		proto_cardParler1MG:{ usesSuper:"proto_abstract_card",
			title:"Lui dire bonjour",
			description:"Je lui dis bonjour, tout en lui faisant remarquer que je lis le même livre.",
			Card:["proto_cardParler2MG"],
		},
		proto_cardIgnorer1MG:{ usesSuper:"proto_abstract_card",
			title:"Continuer à lire",
			description:"Je poursuis ma lecture, sans faire attention à lui.",
			Card:["proto_cardIgnorer2MG"],
		},
	
	
		// ----
		proto_cardParler2MT:{ usesSuper:"proto_abstract_card",
			description:("#3MG=parler::La jeune femme me parle du livre. Nous en parlons et passons un bon moment."
					    +"#3MG=ignorer::La jeune femme semble ne pas m'avoir entendu. Je n'insiste pas."
//					    +"#else::ELSE"
					    ),
			Card:["proto_card3MT"],
		},
		proto_cardIgnorer2MT:{ usesSuper:"proto_abstract_card",
			description:("#3MG=parler::La jeune femme semble vouloir engager la conversation sur le livre que nous lisons. Je lui réponds poliment mais sans continuer plus loin. Elle comprend et n'insiste pas."
					    +"#3MG=ignorer::Nous continuons notre lecture sans dire un mot. Je finis mon café."),
			Card:["proto_card3MT"],
		},
		
		proto_cardParler2MG:{ usesSuper:"proto_abstract_card",
			description:("#3MT=parler::Le jeune homme me parle du livre. Nous en parlons et passons un moment joyeux entre fans de la même autrice."
					    +"#3MT=ignorer::Le jeune homme ne semble pas vouloir parler avec moi. Un peu vexée, je retourne à ma lecture en finissant rapidement mon café."),
			Card:["proto_card3MG"],
		},
		proto_cardIgnorer2MG:{ usesSuper:"proto_abstract_card",
			description:("#3MT=parler::J'entends le jeune homme me parler du livre. Je lui souris tout en lui faisant comprendre qu'il me dérange dans ma lecture. Il n'insiste pas."
					    +"#3MT=ignorer::Nous continuons notre lecture sans nous parler. Je vérifie qu'il ne me regarde plus puis je finis mon café."),
			Card:["proto_card3MG"],
		},	



		// ----
		proto_card3MT:{ usesSuper:"proto_abstract_card",
			title:"Une impression de déjà vu",
			description:"C'est étrange j'ai l'impression d'avoir déjà vu cette jeune fille.",
			choices:[
				{condition:"3MG=parler",label:"Prendre congé avec un sourire"},
				{condition:"3MG=ignorer",label:"Prendre congé en silence"},
			],
			Card:["proto_cardSourire3MT","proto_cardSilence3MT"],
		},
		proto_card3MG:{ usesSuper:"proto_abstract_card",
			title:"Un air familier",
			description:"J'ai comme l'impression de connaître ce garçon.",
			choices:[
				{condition:"3MT=parler",label:"Le saluer avec un sourire"},
				{condition:"3MT=ignorer",label:"Le saluer silencieusement"},
			],
			Card:["proto_cardSourire3MG","proto_cardSilence3MG"],
		},
		
		
			// ----
			proto_cardSourire3MT:{ usesSuper:"proto_abstract_card",
				description:"Je fais un sourire à la jeune femme, puis je la salue poliment, avant de quitter les lieux.",
				Card:["proto_cardPlusTard5MT"],
			},
			proto_cardSilence3MT:{ usesSuper:"proto_abstract_card",
				description:"Je jette un dernier regard à la jeune fille pour prendre congé, puis je m'en vais.",
				Card:["proto_cardPlusTard5MT"],
			},
			
			proto_cardSourire3MG:{ usesSuper:"proto_abstract_card",
				description:"Je fais un dernier sourire charmeur à l'inconnu, puis je lui dis poliment au revoir.",
				Card:["proto_cardPlusTard5MG"],
			},
			proto_cardSilence3MG:{ usesSuper:"proto_abstract_card",
				description:"Je lance un regard poli à l'homme, puis je m'éloigne sans traîner.",
				Card:["proto_cardPlusTard5MG"],
			},	
			
	
		// ----
		

		// SCENARIO
		proto_cardPlusTard5MT:{ usesSuper:"proto_abstract_card",
			title:"6 mois plus tard",
			description:"Je suis dans une pièce inconnue. Il y a une femme assise sur le sol. Elle semble aussi surprise que moi de se retrouver là.",
//			choicesLabels:["Parler à la femme","Examiner la pièce"],
			choices:[
				{label:"Parler à la femme",effect:{ action:{"setVariable":["5MT","parler"]} },},
				{label:"Examiner la pièce",effect:{ action:{"setVariable":["5MT","examiner"]} },}
			],
			Card:["proto_cardParler6MT","proto_cardExaminer6MT"],
		},

		proto_cardPlusTard5MG:{ usesSuper:"proto_abstract_card",
			title:"6 mois plus tard",
			description:"Je me retrouve dans une pièce inconnue. Il y a un homme avec moi, assis sur le sol. Il a l'air aussi étonné que moi d'être ici.",
//			choicesLabels:["Parler à l'homme","Observer la pièce"],
			choices:[
				{label:"Parler à l'homme",effect:{ action:{"setVariable":["5MG","parler"]} },},
				{label:"Observer la pièce",effect:{ action:{"setVariable":["5MG","examiner"]} },}
			],
			Card:["proto_cardParler6MG","proto_cardExaminer6MG"],
		},




		// ----
		proto_cardParler6MT:{ usesSuper:"proto_abstract_card",
			title:"La questionner",
			description:"Je lui demande : «Euh...qui êtes-vous ? Est-ce que vous savez pourquoi nous sommes ici ?».",
			Card:["proto_cardParler7MT"],
		},
		proto_cardExaminer6MT:{ usesSuper:"proto_abstract_card",
			title:"Examiner l'endroit",
			description:"Je regarde autour de moi à la recherche d'informations.",
			Card:["proto_cardExaminer7MT"],
		},
		
		proto_cardParler6MG:{ usesSuper:"proto_abstract_card",
			title:"Lui poser des questions",
			description:"Je lui demande : «Qui êtes-vous ? Que faisons-nous ici ?».",
			Card:["proto_cardParler7MG"],
		},
		proto_cardExaminer6MG:{ usesSuper:"proto_abstract_card",
			title:"Continuer à lire",
			description:"Je jette un oeil tout autour de moi pour tenter d'en savoir plus.",
			Card:["proto_cardExaminer7MG"],
		},


			// ----
			proto_cardParler7MT:{ usesSuper:"proto_abstract_card",
				description:("#5MG=parler::Alors que je m'apprétais à lui parler, la jeune femme me pose des questions avec un ton paniqué. Nous finissions par nous présenter. Son visage m'est vaguement familier. Elle ne semble cependant pas en savoir davantage que moi."
						    +"#5MG=examiner::La femme ne me répond pas. Elle semble examiner la pièce avec attention."),
//				choicesLabels:["Parler à la femme","Examiner la pièce"],
				Card:["proto_cardAttendre8MT"],
			},
			proto_cardExaminer7MT:{ usesSuper:"proto_abstract_card",
				description:("#5MG=parler::Les murs et le sol sont en béton. L'espace de la pièce est assez petit. La seule ouverture est une porte en fer fermée. J'entends la femme me questionner mais je n'y prête pas attention, consacré tout entier à observer la pièce à la recherche d'informations supplémentaires."
						    +"#5MG=examiner::Nous examinons la pièce tous les deux en silence. Les murs et le sol sont en béton. L'espace de la pièce est assez petit. La seule ouverture est une porte en fer fermée."),
//				choicesLabels:["Parler à la femme","Examiner la pièce"],
				Card:["proto_cardAttendre8MT"],
			},
			
			proto_cardParler7MG:{ usesSuper:"proto_abstract_card",
//				description:"«Qui êtes-vous ? Que faisons-nous ici ?»",
				description:("#5MT=parler::Alors que je m'apprétais à parler, l'homme m'assaille de questions. Nous nous présentons. Il me dit vaguement quelque chose. À part ça il ne semble pas avoir plus d'informations que moi."
						    +"#5MT=examiner::L'homme ne me répond pas et semble examiner la pièce attentivement."),
//				choicesLabels:["Parler à la femme","Examiner la pièce"],
				Card:["proto_cardAttendre8MG"],
			},
			proto_cardExaminer7MG:{ usesSuper:"proto_abstract_card",
				description:"",
				description:("#5MT=parler::La salle est en béton et n'est pas très grande. Il n'y a aucune autre ouverture autre qu'une porte en fer fermée. J'entends l'homme me parler mais je n'y prête pas attention, absorbée par mon examen minutieux de la pièce."
						    +"#5MT=examiner::Nous examinons la pièce tous les deux sans nous parler. La salle est en béton et n'est pas très grande. Il n'y a aucune autre ouverture autre qu'une porte en fer fermée."),
//				choicesLabels:["Parler à la femme","Examiner la pièce"],
				Card:["proto_cardAttendre8MG"],
			},
		
			

		// ----
		proto_cardAttendre8MT:{ usesSuper:"proto_abstract_card",
			title:"1h plus tard",
			description:"Un certain temps passe. Rien n'a changé. L'ennui et la frustration me gagnent. Je suis pas mal sûr d'avoir déjà vu cette fille.",
			choices:[
				{label:"Discuter",effect:{ action:{"setVariable":["8MT","parler"]} },},
				{label:"Essayer de me souvenir où je l'ai vue",effect:{ action:{"setVariable":["8MT","seSouvenir"]} },}
			],
			Card:["proto_cardParler9MT","proto_cardSeSouvenir9MT"],
		},
		proto_cardAttendre8MG:{ usesSuper:"proto_abstract_card",
			title:"1h plus tard",
			description:"Le temps passe. Des souvenirs commencent à me revenir. Je me souviens de cet homme, et ce qu'on m'a demandé de faire à son sujet",
			choices:[
				{label:"Ne rien faire en silence.",effect:{ action:{"setVariable":["8MG","neRienFaire"]} },},
				{label:"Faire le signal",effect:{ action:{"setVariable":["8MG","faireSignal"]} },},
			],
			Card:["proto_cardNeRienFaire9MG","proto_cardFaireSignal9MG"],
		},
		
		
		// ----
		proto_cardParler9MT:{ usesSuper:"proto_abstract_card",
			description:"Je lui demande : «Excusez-moi, mais est-ce que je ne voua ai pas déjà à un café il y a quelques temps ? Vous lisiez le même livre que moi, il me semble.»",
			Card:["proto_cardParler10MT"],
		},		
		proto_cardSeSouvenir9MT:{ usesSuper:"proto_abstract_card",
			description:"Je fais un effort de mémoire et je me souviens où je l'ai déjà vue : à la table d'un café, où elle lisait le même livre que moi.",
			Card:["proto_cardSeSouvenir10MT"],
		},
		
		proto_cardNeRienFaire9MG:{ usesSuper:"proto_abstract_card",
			description:"Je ne bouge pas et je ne dis rien. Je ne suis plus certaine de vouloir faire ce que je me suis engagée de faire.",
			Card:["proto_cardNeRienFaire10MG"],
		},		
		proto_cardFaireSignal9MG:{ usesSuper:"proto_abstract_card",
			description:"Je regarde en direction de la porte, et j'opine 3 fois discrètement.",
			Card:["proto_cardFaireSignal10MG"],
		},

		
		// ----
		proto_cardParler10MT:{ usesSuper:"proto_abstract_card",
			description:("#8MG=neRienFaire::Elle me regarde en silence. Avec un étrange air prostré. Je me demande ce qu'il y a dans ma question qui la fasse réagir comme ça."
					    +"#8MG=faireSignal::On dirait que je la sors de sa rêverie, elle semblait fixer la porte."),
			Card:["proto_cardUnMomentPlusTard11MT"],
		},
		proto_cardSeSouvenir10MT:{ usesSuper:"proto_abstract_card",
			description:("#8MG=neRienFaire::Tandis que je l'observe pour me souvenir, je constate qu'elle n'a pas bougé. Elle semble très mal à l'aise."
					    +"#8MG=faireSignal::J'ai comme l'impression de voir sa tête agitée de tics brefs, mais c'est probablement juste le stress. Je tente de la rassurer par quelques banalités peu convaincantes."),
			Card:["proto_cardUnMomentPlusTard11MT"],
		},
		
		proto_cardNeRienFaire10MG:{ usesSuper:"proto_abstract_card",
			description:("#8MT=parler::Il me demande si on s'est pas déjà vus. Je ne réponds rien, paralysée par le doute."
					    +"#8MT=seSouvenir::L'homme semble songeur. Il se demande sûrment ce qui va nous arriver."),
			Card:["proto_cardUnMomentPlusTard11MG"],
		},
		proto_cardFaireSignal10MG:{ usesSuper:"proto_abstract_card",
			description:"",
			description:("#8MT=parler::Sa question me surprend. J'espère qu'il ne m'a pas vue faire le signal. J'élude rapidement sa question."
					    +"#8MT=seSouvenir::À peine ai-je fini de faire mon signal que je l'entends essayer maladroitement de me rassurer. Je pense qu'il ne m'a pas vue ou bien il pense que je tremble de stress. Il est vrai que la situation n'est pas des plus sereines."),
			Card:["proto_cardUnMomentPlusTard11MG"],
		},
	

		
		// ----
		proto_cardUnMomentPlusTard11MT:{ usesSuper:"proto_abstract_card",
			description:"Après un moment, la porte s'ouvre. Un vieil homme est jeté dans notre cellule, accompagné d'une enveloppe qui contient un objet tubulaire.",
			choices:[
				{ label:"Je me saisis de l'enveloppe.",
 				 constraints:{max:1},
 				 effect:{ action:{"setVariable":["11MT","enveloppe"]} } },
				{ label:"Je reste où je suis.",
 				 constraints:{max:1},
 				 effect:{ action:{"setVariable":["11MT","nePasBouger"]} } },
			],
			Card:["proto_cardEnveloppe12MT","proto_cardNePasBouger12MT"],
		},
		
		proto_cardUnMomentPlusTard11MG:{ usesSuper:"proto_abstract_card",
			description:"Après un certain temps, la porte s'ouvre. Je me souviens du plan. Un homme est jeté dans notre pièce, avec une enveloppe dont je connais le contenu : une seringue de poison et un message.",
			choices:[
				{ label:"Je m'empare de l'enveloppe.",
 				 constraints:{max:1},
 				 effect:{ action:{"setVariable":["11MG","enveloppe"]} } },
				{ label:"Je ne bouge pas et j'observe.",
 				  constraints:{max:1},
 				  effect:{ action:{"setVariable":["11MT","nePasBouger"]} } },
			],
			Card:["proto_cardEnveloppe12MG","proto_cardNePasBouger12MG"],
		},
		
		// ----
		// 11MT=enveloppe
		proto_cardEnveloppe12MT:{ usesSuper:"proto_abstract_card",
			description:("#11MG=enveloppe::Elle s'avance vers l'enveloppe, mais je suis plus rapide et m'en saisis avant elle. Je l'ouvre. Elle contient une seringue avec écrit «poison» dessus. Sur le mot je lis «Tuez le dernier venu ou vous mourrez tous les deux»."
					    +"#11MG=nePasBouger::J'ouvre. Elle contient une seringue avec écrit «poison» dessus. Sur le mot je lis «Tuez le dernier venu ou vous mourrez tous les deux»."),
			choices:[
				{label:"Je montre le contenu"},
				{label:"J'injecte le poison au dernier venu"},
			],
			// TODO
//			Card:["proto_cardMontrer13MT","proto_cardInjecter13MT"],
		},
		// 11MT=nePasBouger
		proto_cardNePasBouger12MT:{ usesSuper:"proto_abstract_card",
			description:("#11MG=enveloppe::La jeune femme se saisit de l'enveloppe puis elle l'ouvre. Elle fixe le dernier venu."
					    +"#11MG=nePasBouger::Personne ne bouge. L'autre homme nous regarde d'un air appeuré sans rien faire ni dire."),
			choices:[
				{condition:"11MG=enveloppe",label:"Je lui demande ce qu'elle contient."},
				{condition:"11MG=enveloppe",label:"Je lui demande de me la donner"},
				{condition:"11MG=nePasBouger",label:"Je prends finalement l'enveloppe"},
				{condition:"11MG=nePasBouger",label:"Je ne fais toujours rien"},
			],
			// TODO
//			Card:["proto_cardDemanderOuvrir13MT","proto_cardDemanderDonner13MT","proto_cardPrendre13MT","proto_cardAttendre13MT",],
		},
		
		// 11MG=enveloppe
		proto_cardEnveloppe12MG:{ usesSuper:"proto_abstract_card",
			description:("#11MT=enveloppe::Je fais mine de prendre l'enveloppe, mais je laisse l'homme la prendre. Il l'ouvre. Il fixe le dernier entré d'un air confus. Je me rappelle de ce qu'elle devait contenir : une seringue de poison et un mot avec inscrit «Tuez le dernier venu ou vous mourrez tous les deux»."
					    +"#11MT=nePasBouger::Je prends puis ouvre l'enveloppe. Comme prévu elle contient une seringue avec écrit «poison» dessus, et un mot avec inscrit «Tuez le dernier venu ou vous mourrez tous les deux»."),
			choices:[
				{label:"Je montre le contenu"},
				{label:"Je lui donne l'enveloppe"},
			],
			// TODO
//			Card:["proto_cardMontrer13MG","proto_cardDonner13MG"],
		},
		// 11MG=nePasBouger
		proto_cardNePasBouger12MG:{ usesSuper:"proto_abstract_card",
			description:("#11MT=enveloppe::Je laisse ma cible ramasser l'enveloppe, puis l'ouvrir. Il fixe le dernier entré d'un air confus. Je me rappelle de ce qu'elle devait contenir : une seringue de poison et un mot avec inscrit «Tuez le dernier venu ou vous mourrez tous les deux»."
					    +"#11MT=nePasBouger::Personne ne fait ni ne dit rien. L'autre homme nous regarde d'un semblant d'air apeuré en silence."),
			choices:[
				{condition:"11MT=enveloppe",label:"Je lui demande d'ouvrir."},
				{condition:"11MT=enveloppe",label:"J'attends de voir sa réaction"},
				{condition:"11MT=nePasBouger",label:"Je ramasse l'enveloppe et montre le contenu"},
				{condition:"11MT=nePasBouger",label:"Je ramasse l'enveloppe et lui donne"},
			],
			// TODO
//			Card:["proto_cardDemanderOuvrir13MG","proto_cardAttendre13MG","proto_cardMontrer13MG","proto_cardDonner13MG",],
		},
		
		
		// ----
//		effect:{ action:{"setVariable":["8MG","neRienFaire"]} },
		// TODO
//		"proto_cardMontrer13MT","proto_cardInjecter13MT","proto_cardDemanderOuvrir13MT","proto_cardDemanderDonner13MT","proto_cardPrendre13MT","proto_cardAttendre13MT"
//		"proto_cardDemanderOuvrir13MG","proto_cardAttendre13MG","proto_cardDonner13MG","proto_cardMontrer13MG"
		
		
		
		
		
		
		

};
