// DEPENDENCY:init.js




// ===============================================================================
// MODEL
// ===============================================================================





// Prototype can define as a well a group of things as particular
// individuals:only by not setting a random variance on them.



PROTOTYPES_CONFIG = {

	allPrototypes:{
		
		
		// ------------------ Levels objects ------------------
		GameLevel:{
		
			
		
			
			
			proto_level_1:{
				
				selectedItem:null,
				
				// TODO : Pouvoir sélectionner parmis une liste de scenarii
				Scenario:"proto_scenario1",
				
			},
			
			
			
		},
		
		
		
		// ------------------ Game objects ------------------
		
		Scenario:{
			
			proto_scenario1:{

				containerConfig:"starter", // To indicate where the player starts in the tree objects hierarchy
				
//				backgroundColor:"#FEE7A2",
				backgroundColor:"#444444",

//				backgroundZIndex:1,
//				backgroundParallax:10,
//				backgroundOffsetY:0,
//				backgrounds:[
//// {images:["backgroundSky1.png","backgroundSky2.png","backgroundSky3.png","backgroundSky4.png"],parallaxAdd:100},
//// //
//// {images:["backgroundFore1.png","backgroundFore2.png","backgroundFore3.png","backgroundFore4.png"],parallaxAdd:10},
//// {images:["background1.png","background2.png","background3.png","background4.png"]},
//
//// {images:["backgroundSky1.png"],parallaxAdd:100},
//// {images:["background1.png"],parallaxAdd:10},
//// {images:["backgroundFore1.png"]},
//
//					{images:["backgroundSky1_v1.png"],parallaxAdd:400},
//					{images:["background1_v1.png"],parallaxAdd:200},
//					{images:["backgroundFore1_v1.png"],parallaxAdd:20},
//					{images:[{normal:"foregroundGround1.png"}],offsetY:-230,clipSize:{w:1600,h:100}},
//
//				],
				
//				foregroundParallax:1,
//// foregroundOffsetY:10,
//				foregrounds:[
//// {images:["foreground1.png"],offsetY:-20},
//					{images:[ {normal:"foreground1_v1.png",dark:"foreground1.dark.png"} ],offsetY:-20},
//				],
				
				TurnsManager:"proto_turnsManager1",
				
//				RoleCharacter:["proto_roleCharacterMichelTremblay"],
				RoleCharacter:["*"],
				
			}
			
		},

		TurnsManager:{
			proto_turnsManager1:{
			
			},
		},
		
		RoleCharacter:{
		
		
			proto_roleCharacterUnknown:{
				name:"Inconnu",
				Deck:["proto_deckChooseCharacter"],
			},
		
			proto_roleCharacterMichelTremblay:{
				name:"Michel Tremblay",
				Deck:["proto_deckMichelTremblay"],
				GameGauge:["*"],
			},
			
			proto_roleCharacterMarieGagnon:{
				name:"Marie Gagnon",
				Deck:["proto_deckMarieGagnon"],
				GameGauge:["*"],
			},
			
		},
		
		
		GameGauge:{
			
			proto_abstractGauge:{
				
//				gaugeValue:1000,
//				gaugeMax:2000,

				// DBG
				gaugeValue:150,
				gaugeMax:300,

				displayNumericValue:true,
				
				
//				// DBG
//				gaugeValue:50,
//				gaugeMax:100,
				
//				overridingPosition:{ x:0,y:0 },
				overridingPosition:{ x: -GAME_CONFIG.getResolution().w*.1 -25, y:10 },
//				overridingPosition:{ x: -25  ,y:10 },

			},
			
			
			proto_health:{
				usesSuper:"proto_abstractGauge",
				GameThumb:"proto_health",
			},
			proto_mind:{
				usesSuper:"proto_abstractGauge",
				GameThumb:"proto_mind",
			},
			proto_feelling:{
				usesSuper:"proto_abstractGauge",
				GameThumb:"proto_feelling",
			},
			proto_money:{
				usesSuper:"proto_abstractGauge",
				noMax:true,
				GameThumb:"proto_money",
			},
			
			
		},
		
		
		GameThumb:{

			proto_abstract_thumb:{
				sizeFactor:1,
				scaleFactor:1,
				
//				overridingPosition:{ x: -GAME_CONFIG.getResolution().w*.2 -25  ,y:10 },

			},
			proto_health:{
				usesSuper:"proto_abstract_thumb",
				offsetsUI:{x:GAME_CONFIG.getResolution().w*.2},
				thumbSrc:"health.thumb.png",
			},
			proto_mind:{
				usesSuper:"proto_abstract_thumb",
				offsetsUI:{x:GAME_CONFIG.getResolution().w*.4},
				thumbSrc:"mind.thumb.png",
			},
			proto_feelling:{
				usesSuper:"proto_abstract_thumb",
				offsetsUI:{x:GAME_CONFIG.getResolution().w*.6},
				thumbSrc:"feelling.thumb.png",
			},
			proto_money:{
				usesSuper:"proto_abstract_thumb",
				offsetsUI:{x:GAME_CONFIG.getResolution().w*.8},
				thumbSrc:"money.thumb.png",
			},
			
		},

		
		Deck:{
		
			proto_abstract_deck:{
				overridingPosition:{ x:0,y:0 },
				
			},
		

//			proto_testDeck:{
//				
//				overridingPosition:{ x:0,y:0 },
//				
////				Card:["proto_card1","proto_card2","proto_card3","proto_card4","proto_card5","proto_card6"],
////				Card:["*"],
//					Card:["proto_card1","proto_card2",
//								"proto_firstCard","proto_cardFail1", "proto_cardFail2", "proto_cardFail3", "proto_cardFail4", "proto_cardSuccess","proto_cardNoIssue"],
//			},
			
			// proto_deckWaitForPlayers:{
			//	 usesSuper:"proto_abstract_deck",
			//	 Card:["proto_cardWaitForPlayers",],
			// },
			
			
			
			proto_deckChooseCharacter:{
				usesSuper:"proto_abstract_deck",
				
//				Card:["proto_card1","proto_card2","proto_card3","proto_card4","proto_card5","proto_card6"],
//				Card:["*"],
				
				Card:["proto_cardWelcome",],
			},
			
			
			
			
			proto_deckMichelTremblay:{
				
				usesSuper:"proto_abstract_deck",
				
//				Card:["proto_card1","proto_card2","proto_card3","proto_card4","proto_card5","proto_card6"],
//				Card:["*"],
				
				Card:["proto_cardChooseMT"],
				
			},
			
			
			
			proto_deckMarieGagnon:{
				
				usesSuper:"proto_abstract_deck",
				
//				Card:["proto_card1","proto_card2","proto_card3","proto_card4","proto_card5","proto_card6"],
//				Card:["*"],
				
				Card:["proto_cardChooseMG"],
				
			},
		
		},
		
		
	},
		

};
