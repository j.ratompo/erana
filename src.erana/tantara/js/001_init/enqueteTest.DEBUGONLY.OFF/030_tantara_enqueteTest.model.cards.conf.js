// DEPENDENCY:init.js

// ===============================================================================
// MODEL
// ===============================================================================

// Prototype can define as a well a group of things as particular
// individuals:only by not setting a random variance on them.
// ***
// CAUTION : The attribute «usesSuper» must always be the first attribute ! (All attribute put before will be ignored).
// ***

/*OVERRIDES*/PROTOTYPES_CONFIG.allPrototypes.Card={
			
	
	proto_abstract_card:{
		angleDegrees:"-2->2",
		fontSizePercent:5,
		
		cardsSetPath:"enqueteTest/",
		imageSrc:"defaultCard.png",
	
		size:{w:GAME_CONFIG.getResolution().w*.85, h:GAME_CONFIG.getResolution().h*.8},
	
		hintSize:{w:GAME_CONFIG.getResolution().w*.5, h:250},
		
		hintRelativePosY:GAME_CONFIG.getResolution().h*.15,

		overridingPosition:{
			x:GAME_CONFIG.getResolution().w*.5 - 20, y:GAME_CONFIG.getResolution().h*.5
		},
		
		wheelChoiceConfig:{
//			timeoutConfig:{delayMillis:20000,defaultIndex:0},
			hideChoiceLabels:true,
		},
		
//		// DBG			
//		debugAttr:getUUID("short"),
		
		
	},
	
	// -----------------------------------------------------------------------------------
	// Choice of character :
	// -----------------------------------------------------------------------------------
	
	
	//proto_cardWaitForPlayers:{
	//	usesSuper:"proto_abstract_card",
	//	imageSrc:null,
	//	title:"L'enquête test",
	//	description:"En attente de joueurs...",	
	//	first:true,
	//	choicesLabels:["Start!"],
	//	Card:["proto_cardWelcome"],
	//	effect:{
	//		action:{methodName:"startWaitingForPlayers"}
	//	},
	//},
	
	// -----------------------------------------------------------------------------------
	// Choice of character :
	// -----------------------------------------------------------------------------------
	
	
	proto_cardWelcome:{
		usesSuper:"proto_abstract_card",
		
		cardIndex:1,
		first:true,
	
//		imageSrc:null,
		imageSrc:"iconSwipeHands.png",
		isImageDeviceDependent:true,
		title:"L'enquête test",
		description:"Bonjour et bienvenue dans l'enquête test. Comment allez-vous ? <small>(faites glisser la carte selon votre choix)</small>",
		
		first:true,
//		choicesLabels:["Bien!","Moyen...","Ne souhaite pas répondre."],
		choices:[{label:"Pas pire!",constraints:{max:1}},{label:"Moyen...",constraints:{max:1}},{label:"Ne souhaite pas répondre.",constraints:{max:1}}],
		
		
//		Card:["proto_firstCard_1","proto_firstCard_1_2","proto_card2"],
//		Card:["proto_firstCard_1","proto_firstCard_1_2","proto_card2","proto_card3"],
		Card:["proto_cardWelcome1","proto_cardWelcome1","proto_cardWelcome1"],

	},
		
		proto_cardWelcome1:{
			usesSuper:"proto_abstract_card",
			
			cardIndex:2,
		
			title:"L'aventure commence",
			description:"OK... tout d'abord qui êtes-vous ?",
	
			choices:[
				{
 				 label:"Michel Tremblay",
 				 imageSrc:"MichelTremblay.png",
 				 constraints:{max:1},
 				 effect:{ isOnlyChooserClientAffected:true, action:{methodName:"setRoleCharacterByPrototypeName",args:["proto_roleCharacterMichelTremblay"]} },
 				},
				{
				 label:"Marie Gagnon",
				 imageSrc:"MarieGagnon.png",
				 constraints:{max:1},
				 effect:{ isOnlyChooserClientAffected:true, action:{methodName:"setRoleCharacterByPrototypeName",args:["proto_roleCharacterMarieGagnon"]} },	
				}
			],
			Card:["proto_cardChooseMT","proto_cardChooseMG"],
		},
		
		
			
	
	proto_cardChooseMT:{
		usesSuper:"proto_abstract_card",
	
		cardIndex:3,
		
	
		title:"Michel Tremblay",
		description:"Vous êtes un gars ben normal.",

//		choices:[{label:"Michel Tremblay",imageSrc:"MichelTremblay.png",constraints:{max:1}},{label:"Marie Gagnon",imageSrc:"MarieGagnon.png",constraints:{max:1}}],
//		Card:["proto_cardChooseMT","proto_cardChooseMG"],
		Card:["proto_cardChooseMT1"],

	},
		
		proto_cardChooseMT1:{
			usesSuper:"proto_abstract_card",
		
			cardIndex:4,
		
			title:"Prêt ?",
			description:"Vous êtes prêt à commencer l'aventure !",
//			Card:["proto_cardChooseMTEffect"],
		},
		
//		proto_cardChooseMTEffect:{
//			usesSuper:"proto_abstract_card",
//
//			cardIndex:5,
//		
//			effect:{
//				action:{methodName:"setRoleCharacterByPrototypeName",args:["proto_roleCharacterMichelTremblay"]}
//			},
//		},

	
	
	proto_cardChooseMG:{
		usesSuper:"proto_abstract_card",
	
		cardIndex:3,
	
		title:"Marie Gagnon",
		description:"Vous êtes une fille ben sympathique.",

//		choices:[{label:"Michel Tremblay",imageSrc:"MichelTremblay.png",constraints:{max:1}},{label:"Marie Gagnon",imageSrc:"MarieGagnon.png",constraints:{max:1}}],
//		Card:["proto_cardChooseMT","proto_cardChooseMG"],
		Card:["proto_cardChooseMG1"],
	},
	
		proto_cardChooseMG1:{
			usesSuper:"proto_abstract_card",
		
			cardIndex:4,
		
			title:"Prête ?",
			description:"Vous êtes prête à commencer l'aventure !",
//			Card:["proto_cardChooseMGEffect"],
		},
		
//		proto_cardChooseMGEffect:{
//		
//			cardIndex:5,
//		
//			usesSuper:"proto_abstract_card",
//
//			effect:{
//				action:{methodName:"setRoleCharacterByPrototypeName",args:["proto_roleCharacterMarieGagnon"]}
//			},
//		},
		
		
	// -----------------------------------------------------------------------------------
	// Scenario :
	// -----------------------------------------------------------------------------------
	

		
	
	
//	
//	
//	proto_card1_MT:{
//		usesSuper:"proto_abstract_card",
//		imageSrc:null,
//
//		// DBG			
//		debugAttr:getUUID("short"),
//		
//		title:"Carte 1!",
//		description:"Bienvenue",
//		
//		choicesLabels:["Bonjour!","Suite"],
////		Card:["proto_firstCard_1","proto_firstCard_1_2","proto_card2"],
////		Card:["proto_firstCard_1","proto_firstCard_1_2","proto_card2","proto_card3"],
//		Card:["proto_firstCard_1","proto_firstCard_1_2"],
//
//	},
//	
//		proto_firstCard_1:{
//			usesSuper:"proto_abstract_card",
//			title:"Carte 2",
//			description:"Suite...",
//			choicesLabels:["OK...","Jouer !",],
//			Card:["proto_firstCard_1_1","proto_firstCard_1_1"],
//		},
//			proto_firstCard_1_1:{
//				usesSuper:"proto_abstract_card",
//				imageSrc:"none",
//				title:"Attention à tes indicateurs",
//				description:"Si un seul des indicateurs tombe à 0, tu as perdu, s`il arrivent tous à 100%, tu as gagné la partie!.",
//				choicesLabels:["C`est bon je veux jouer, maintenant!",],
//				Card:["proto_firstCard_1_2"],
//			},
//			proto_firstCard_1_2:{
//				usesSuper:"proto_abstract_card",
//				imageSrc:"none",
//				title:"C`est parti!",
//				description:"Swipe simplement cette carte de n`importe quel côté pour démarrer une partie !",
//			},
//		
//	proto_cardFail1:{
//		usesSuper:"proto_abstract_card",
//		imageSrc:"pileOrdures.png",
//		title:"L`environnement est détruit",
//		description:"La biosphère ne peut plus supporter les activités humaines et la planète n`est plus habitable. Nous avons échoué." +
//								"<br /><small>(Heureusement ce n`est qu`un jeu! Tu peux recommencer pour essayer une nouvelle approche!)</small>",
//		fail:"proto_environment",
//		isTerminal:true,
//	},
//	proto_cardFail2:{
//		usesSuper:"proto_abstract_card",
//		imageSrc:"madameDansRue.png",
//		title:"Plus aucune solidarité",
//		description:"La société est devenue si indivualiste, que seul le chacun pour soi règne. Le tissu social n`existe plus et tout le monde est en compétition contre tout le monde."+
//								"<br /><small>(Heureusement ce n`est qu`un jeu! Tu peux recommencer pour essayer une nouvelle approche!)</small>",
//		fail:"proto_solidarity",
//		isTerminal:true,
//	},
//	proto_cardFail3:{
//		usesSuper:"proto_abstract_card",
//		imageSrc:"politicienAnxieux.png",
//		title:"On vit dans une dictature",
//		description:"Les élites ont accaparé le pouvoir pour de bon et ont cessé de faire semblant. Le peuple n`a plus son mot à dire et vit dans la peur de la brutalité des puissants."+
//								"<br /><small>(Heureusement ce n`est qu`un jeu! Tu peux recommencer pour essayer une nouvelle approche!)</small>",
//		fail:"proto_democracy",
//		isTerminal:true,
//	},
//	proto_cardFail4:{
//		usesSuper:"proto_abstract_card",
//		imageSrc:"surveillants.png",
//		title:"Nous avons perdu la paix",
//		description:"Le chaos s`est répandu. Ne reste que la loi du plus fort et les luttes fratricides absurdes. Il ne fait plus bon vivre et un climat de terreur s`est installé.",
//		fail:"proto_peace",
//		isTerminal:true,
//	},
//	
//	proto_cardSuccess:{
//		usesSuper:"proto_abstract_card",
//		imageSrc:"heureux.png",
//		title:"Bravo!",
//		description:"Nous avons atteint un âge de paix, de respect mutuel et d`égalité, où tous les humains sont libres de s`épanouir dans un environnement sain."+
//								"<br /><small>(Malheureusement ce n`est qu`un jeu! Mais libre à toi d`essayer d`aappliquer ce que tu y as appris pour rendre meilleur le monde réel!)</small>",
//		success:true,
//		isTerminal:true,
//	},
//	
//	proto_cardNoIssue:{
//		usesSuper:"proto_abstract_card",
//		imageSrc:"meh.png",
//		title:"Statut quo",
//		description:"Les choses ont un peu bougé, mais pas assez. Le bilan est mitigé et insuffisant. Mais au moins on aura essayé!"+
//								"<br /><small>(Heureusement ce n`est qu`un jeu! Tu peux recommencer pour essayer une nouvelle approche!)</small>",
//		noIssue:true,
//		isTerminal:true,
//	},
//	
//	
//	
//	// Scenario cards :
//	
//			
//	proto_card1:{
//		usesSuper:"proto_abstract_card",
//		imageSrc:"iphone24.png",
//		title:"L`iphone 24 est enfin sorti!",
//		description:"Avec ses 40 caméras, prenez des selfies de haute qualité! Seul problème : son prix, 7999$ et les mines d`où sont extraits ses matériaux. Mais qui peut y resister?",
//		choicesLabels:[ "Si je peux me le payer, pourquoi m`en priver?", "Je n`en ai pas besoin!" ],
//		Card:["proto_card1_a","proto_card1_b"],
//	},
//		proto_card1_a:{
//			usesSuper:"proto_abstract_card",
//			imageSrc:"iphoneAchete.png",
//			effect:{
//				proto_environment:-200,
//				proto_solidarity:0,
//				proto_democracy:0,
//				proto_peace:0,
//			},
//			title:"C`était cher mais ça en valait le coup!",
//			description:"Bon je sais que ce n`est pas très bon pour la planète, mais j`ai hâte de le montrer à mes amis!",
//		},
//		proto_card1_b:{
//			usesSuper:"proto_abstract_card",
//			imageSrc:"cultiverAvecArrosoir.png",
//			effect:{
//				proto_environment:0,
//				proto_solidarity:200,
//				proto_democracy:0,
//				proto_peace:0,
//			},
//			title:"Mon téléphone actuel me suffit amplement",
//			description:"Pourquoi toujours se ruer sur le dernier modèle? Je me suis inscrit à un jardin communautaire. Au lieu de niaiser des heures sur instabookeeter, je cultive mes propres carottes!",
//		},





};
