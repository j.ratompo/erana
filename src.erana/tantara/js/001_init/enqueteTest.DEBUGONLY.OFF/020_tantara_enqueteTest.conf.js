// DEPENDENCY:init.js


window.lang = "fr";


// ===============================================================================
// CONFIG
// ===============================================================================


const PROTO_PREFIX="proto_";


const GAME_CONFIG = {
	
	gameURLs:{
//		"support":"https://paypal.me/pools/c/8szm55N1iC", 
		"other":"https://alqemia.com", "email":"mailto:info@alqemia.com"},
	basePath:"tantara.medias",
	
	spinnerImage:"aotraLogo_128.gif",
	
	// view config :
	X_OFFSET:0,
	Y_OFFSET:0,
	// TODO:FIXME:ZOOM DOES NOT WORK AT ALL :
	zooms:{zx:1,zy:1}, // Zoom is the most outer function. Camera and Offsets coordinates
					// obeys it!

	// resoiution :
	
	// PROD
	isMobile:isDeviceMobile(),
	
//	// DBG
//	chosenResolutionIndex:1,
	// PROD
	chosenResolutionIndex:0,
	getResolution:function(){
		// This syntax is quite cool :
		return [{w:getWindowSize("width"),h:getWindowSize("height")},{w:360,h:640},{w:400,h:600},][this.chosenResolutionIndex];
//		return [{w:window.innerWidth,h:window.innerHeight},{w:400,h:600},][this.chosenResolutionIndex];
//		return [{w:400,h:600},{w:400,h:600},][this.chosenResolutionIndex];
	},
	getViewCenterOffset:function(ignoreOffset=false){
		var result={
				x:Math.floor(this.getResolution().w*.5) + (ignoreOffset?0:this.X_OFFSET),
				y:Math.floor(this.getResolution().h*.5) + (ignoreOffset?0:this.Y_OFFSET)
		};
		return result;
	},


	// lighting config :
	lighting:{ dark:true, darkRaycast:true, bump:true },
	// scroll config :
	scale:0.025,// = 1/40 (40 px = 1m)
	

	scroll:"bidirectionnal",
	
	projection:"2D flat",
	
	
	// camera config :
	cameraFixed:null,
	
	// selection config :
	selectionCanChange:false,
	selectionMaxSize:1,
	
	// controls config :
	controlsMode: "screen-drag camera",
	controlsPosition:"direction action",
	controlsPanelHeightRatio:0.3,
	controlsPanelStyle:{
		backgroundGradient:"90deg, #222222, #111111",
		text:"#EEEEEE",
		gaugeBackColor:"transparent",
		gaugeForeColor:"#444444",
	},
	popupConfig:{
		backgroundColor1:"#222222",
		backgroundColor2:"#111111",
		textColor:"#FFFFFF",
	},
	// images config :
	backgroundClipSize:{w:400,h:400},
	
	// controller config :
	// ALTERNATIVE PARAMETER : refreshRateMillis:33, // :(1000/33)=30 FPS max

//	fps:40, // CAUTION : changes speeds de facto !
	// DBG
	fps:20,
	
	// ui
	dialogs:{	
		autoScroll:"letters", 
		speedFactor:8, 
		/*UNUSEFUL (Since there will always be a «linger» time if you set a speedFactor>1 !) :
		 minimumTimeMillis:1000*/
		backgroundColor:"#D1B69C",
	},
	// model config :
	allowParentLinkForChildren:true,
	
	
	gameCentralServer:{
		url:"ws://192.168.0.111",
		port:25000,
		timeout:5000,
		modelStrategy:"polyverse",
	},
	
	
};





// ===============================================================================
// FLOW
// ===============================================================================



FLOW_CONFIG = {
	
	
	splashPage_erana:{

		//DBG
//		delayMillis:(IS_DEBUG?200:3000),
		delayMillis:(true?200:3000),
		
		actionAfter:(IS_DEBUG?"startNewGame":null),
		
		image: "splashs/aotraLogo_300.png",
		background: "#FFFFFF",
		text:{
			font:"helvetica",
			position:{yBottom:20},
			message:i18n("propulséParEranaBrSpanStyleFontSize7emUnMoteurOuv")
		},
		goTo:(IS_DEBUG?"gamePage":"splashPage_alqemia"),

	},
	
	splashPage_alqemia:{

		//DBG
//		delayMillis:(IS_DEBUG?200:3000),
		delayMillis:(true?200:3000),
		
		image: "splashs/alqemia_400.png",
		background: "#000000",
		text:{
			color:"#000000",
			font:"helvetica",
			position:{yTop:70},
			message:i18n("leStudioAlqemiaPrésente"),
		},
		
//		goTo:(IS_DEMO?"alphaPage":(IS_DEBUG?"homePage":"audioPage")),

		// FOR NOW ONLY (alpha version) :
		goTo:("alphaPage"),
		
//		goTo:"audioPage"
//		goTo:"alphaPage",

	},

	
	
	alphaPage:{
		
		image: "splashs/icon_bug.png",

		// WORKAROUND : To force the user to interact to bypass the «user must interact with the page first before autoplay» user annoyance protection politics :

		background: "#000000",
		text:{
			color:"#FFFFFF",
			font:"helvetica",
			position:{yTop:30},
			message:i18n("attentionCeciEstUneVersionAlphaBrDesBugsInacceptable"),
												
		},
		
		
		menus:{
			mainMenu:{
				items:[
					{
						label:i18n("ok"),
						goTo:"homePage"
					},
				]
			},
		},
	},
	
	
	
	
	
//	audioPage:{
//		
//		image: "splashs/icon_headset.png",
//
//
//		// WORKAROUND : To force the user to interact to bypass the «user must interact with the page first before autoplay» user annoyance protection politics :
//
//		background: "#000000",
//		text:{
//			color:"#FFFFFF",
//			font:"helvetica",
//			position:{yTop:80},
//			message:i18n("unDispositifDÉcouteEstConseilléBrPourUneMeilleureExpé"),
//		},
//		
//		
//		menus:{
//			mainMenu:{
//				items:[
//					{
//						label:i18n({
//							"fr":"OK",
//							"en":"OK"
//						}),
//						goTo:"homePage"
//					},
//				]
//			},
//		},
//	},
	
	
	
	
	homePage:{
		
//		image: "splashs/lagrandeligne-title.png",
		
		background: "#FFFFFF",
		text:{
			font:"helvetica",
			position:{yTop:0},
			message:i18n({"fr":"<h1 class='gameTitle'>Enquête test</h1>","en":"<h1 class='gameTitle'>Test investigation</h1>",})
		},
		
		

		startLevelName:null,

		// WORKAROUND : To bypass the «user must interact with the page first before autoplay» user annoyance protection politics :

//		music:{
//			pauseBetweenMillis:4000,
//			volumePercent:50,
//			tracks: ["musics/Mercy.mp3","musics/Evermore.mp3"
//				],
//		},
	
		
		menus:{
			mainMenu:{
				items:[
					{
						label:i18n("commencer"),
						goTo:("gamePage"),
					},
					{
						label:i18n("joindre"),
						actionAfter:"joinGame",
						goTo:("gamePage"),
					},
					{
						label:i18n("continuer"),
						actionAfter:"continueGame",
						goTo:"gamePage",
						activeOnlyIf:"isPreviousGameDetected()",
						restoreLevel:true,
					}, 
					{
						label:i18n("crédits"),
						actionAfter:"startCredits",
						goTo:"creditsPage"
					},
					{
						label:i18n("soutenir"),
						goTo:"supportPage",
					},
					{
						label:i18n("quitter"),
						actionAfter:"exitGame",
						//goTo:"homePage",
					},
				]
			},
		},
	},

	
	
	
	gamePage:{

		startLevelName:"proto_level_1",


//		music:{
//			pauseBetweenMillis:4000,
//			volumePercent:50,
//			tracks: ["musics/Brooks.mp3","musics/Denouement.mp3","musics/Snowmen.mp3"],
//		},
		
		menus:{
			mainMenu:{
				config:{
					label:i18n("menu"),
					actionAfter:"showMenu",
					position:"top-right" // default position is "top-left"
				},
				items:[
					{
						label:i18n("clientID: %value%"),
//						getLabel:"getClientID",
						type:"label",
						clientIDHolder:true,
					},
					{
						label:i18n("reprendre"),
						actionAfter:"resumeGame",
						resume:true
					},
//					{
//						label:i18n("sauverEtRevenirÀLAccueil"),
//						actionAfter:"interruptGame",
//						goTo:"homePage"
//					},
					{
						label:i18n("retourALAccueil"),
						actionAfter:"abandonGame",
						goTo:"homePage"
					}
				]
			},
		},
	},


	
	
	creditsPage:{

		startLevelName:null,
		
		
		menus:{
			mainMenu:{
				items:[
					
					{ label:i18n("programmationJeremieR"), type:"label",},
					{ label:i18n("graphiquesJeremieR"), type:"label",},
					{ label:i18n("écritureJeremieR"), type:"label",},
					{ label:i18n("sonsJeremieR"), type:"label",},
					{ label:i18n("communicationJeremieR"), type:"label",},
					{ label:i18n("musiquePar"), actionAfter:"goToURLOther" },
					{ label:i18n("remerciementsSupportMoralDominiqueB"), type:"label",},
//					{ label:i18n("remerciementsMusiqueÉcoutéeKaiEngelLindseyStirlingOh"), type:"label",},
//					{ label:i18n("remerciementsPodcastsÉcoutésTalesOfPiFranceInterFib"), type:"label",},
					{ label:i18n("retourALAccueil"), goTo:"homePage"},
										
				]
			},
		},
	},
	
	
	
	
	supportPage:{

		startLevelName:null,
		
		menus:{
			mainMenu:{
				items:[
					
			
					{
						label:i18n({
							"fr":"<strong>Comment nous soutenir ?</strong>",
							"en":"<strong>How to support the game ?</strong>",
						}),
						type:"label",
					},

					{
						label:i18n({
							"fr":"Faites-nous parvenir vos commentaires / demandez à être informé des prochains développements !",
							"en":"Let us know your comments / ask for updates from the game development !",
						}),
						type:"label",
					},

					{
						label:i18n({
							"fr":"Envoyer un courriel à l´équipe : info@alqemia.com",
							"en":"Send an email to the team : info@alqemia.com",
						}),
						actionAfter:"goToURLMailTo"

					},
					
//					{
//						label:i18n({
//							"fr":"Faites-nous un petit don qui fera une grande différence ! 😉",
//							"en":"Give a us a little tip that will make a big difference ! 😉",
//						}),
//						actionAfter:"goToURLSupport"
//					},
					
					{
						label:i18n("retourALAccueil"),
						goTo:"homePage"
					},
					
					
					
				]
			},
		},
	},
	
	


};






