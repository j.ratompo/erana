// DEPENDENCY:init.js

// ===============================================================================
// MODEL
// ===============================================================================

// Prototype can define as a well a group of things as particular
// individuals:only by not setting a random variance on them.


/*OVERRIDES*/PROTOTYPES_CONFIG.allPrototypes.Card={
			
	
	proto_abstract_card:{
		angleDegrees:"-2->2",
		fontSizePercent:5,
		
		imageSrc:"defaultCard.png",
	
		size:{w:GAME_CONFIG.getResolution().w*.85, h:GAME_CONFIG.getResolution().h*.8},
	
		hintSize:{w:GAME_CONFIG.getResolution().w*.5, h:250},
		
		hintRelativePosY:GAME_CONFIG.getResolution().h*.15,

		overridingPosition:{
			x:GAME_CONFIG.getResolution().w*.5 - 20, y:GAME_CONFIG.getResolution().h*.5
		},
		
	},
	
	
	proto_firstCard:{
		usesSuper:"proto_abstract_card",
		imageSrc:"billetJardinier.png",
		title:"Bonjour ! Bienvenue dans l`envers du billet!",
		description:"Dans ce petit jeu tu incarnes toute l`Humanité, qui doit faire des choix pour survivre à la crise actuelle...<br />" +
								"Est-ce que ça te dirait un petit mode d`emploi?<br />" +
								"<small>(swipe à gauche ou à droite pour répondre)</small>",
		first:true,
		choicesLabels:["Ça serait pas de refus!","Non ça ira, merci.",],
		Card:["proto_firstCard_1","proto_firstCard_1_2"],
	},
		proto_firstCard_1:{
			usesSuper:"proto_abstract_card",
			imageSrc:"iconSwipeHands.png",
			isImageDeviceDependent:true,
			title:"C`est très simple",
			description:"Swipe à gauche ou à droite pour faire un choix. Tu verras les conséquences immédiatement sur les 4 petits indicateur en haut de l`écran.",
			choicesLabels:["OK...","C`est bon je veux jouer, maintenant!",],
			Card:["proto_firstCard_1_1","proto_firstCard_1_2"],
		},
			proto_firstCard_1_1:{
				usesSuper:"proto_abstract_card",
				imageSrc:"none",
				title:"Attention à tes indicateurs",
				description:"Si un seul des indicateurs tombe à 0, tu as perdu, s`il arrivent tous à 100%, tu as gagné la partie!.",
				choicesLabels:["C`est bon je veux jouer, maintenant!",],
				Card:["proto_firstCard_1_2"],
			},
			proto_firstCard_1_2:{
				usesSuper:"proto_abstract_card",
				imageSrc:"none",
				title:"C`est parti!",
				description:"Swipe simplement cette carte de n`importe quel côté pour démarrer une partie !",
			},
		
	proto_cardFail1:{
		usesSuper:"proto_abstract_card",
		imageSrc:"pileOrdures.png",
		title:"L`environnement est détruit",
		description:"La biosphère ne peut plus supporter les activités humaines et la planète n`est plus habitable. Nous avons échoué." +
								"<br /><small>(Heureusement ce n`est qu`un jeu! Tu peux recommencer pour essayer une nouvelle approche!)</small>",
		fail:"proto_environment",
		isTerminal:true,
	},
	proto_cardFail2:{
		usesSuper:"proto_abstract_card",
		imageSrc:"madameDansRue.png",
		title:"Plus aucune solidarité",
		description:"La société est devenue si indivualiste, que seul le chacun pour soi règne. Le tissu social n`existe plus et tout le monde est en compétition contre tout le monde."+
								"<br /><small>(Heureusement ce n`est qu`un jeu! Tu peux recommencer pour essayer une nouvelle approche!)</small>",
		fail:"proto_solidarity",
		isTerminal:true,
	},
	proto_cardFail3:{
		usesSuper:"proto_abstract_card",
		imageSrc:"politicienAnxieux.png",
		title:"On vit dans une dictature",
		description:"Les élites ont accaparé le pouvoir pour de bon et ont cessé de faire semblant. Le peuple n`a plus son mot à dire et vit dans la peur de la brutalité des puissants."+
								"<br /><small>(Heureusement ce n`est qu`un jeu! Tu peux recommencer pour essayer une nouvelle approche!)</small>",
		fail:"proto_democracy",
		isTerminal:true,
	},
	proto_cardFail4:{
		usesSuper:"proto_abstract_card",
		imageSrc:"surveillants.png",
		title:"Nous avons perdu la paix",
		description:"Le chaos s`est répandu. Ne reste que la loi du plus fort et les luttes fratricides absurdes. Il ne fait plus bon vivre et un climat de terreur s`est installé.",
		fail:"proto_peace",
		isTerminal:true,
	},
	
	proto_cardSuccess:{
		usesSuper:"proto_abstract_card",
		imageSrc:"heureux.png",
		title:"Bravo!",
		description:"Nous avons atteint un âge de paix, de respect mutuel et d`égalité, où tous les humains sont libres de s`épanouir dans un environnement sain."+
								"<br /><small>(Malheureusement ce n`est qu`un jeu! Mais libre à toi d`essayer d`aappliquer ce que tu y as appris pour rendre meilleur le monde réel!)</small>",
		success:true,
		isTerminal:true,
	},
	
	proto_cardNoIssue:{
		usesSuper:"proto_abstract_card",
		imageSrc:"meh.png",
		title:"Statut quo",
		description:"Les choses ont un peu bougé, mais pas assez. Le bilan est mitigé et insuffisant. Mais au moins on aura essayé!"+
								"<br /><small>(Heureusement ce n`est qu`un jeu! Tu peux recommencer pour essayer une nouvelle approche!)</small>",
		noIssue:true,
		isTerminal:true,
	},
	
	
	
	// Scenario cards :
	
			
	proto_card1:{
		usesSuper:"proto_abstract_card",
		imageSrc:"iphone24.png",
		title:"L`iphone 24 est enfin sorti!",
		description:"Avec ses 40 caméras, prenez des selfies de haute qualité! Seul problème : son prix, 7999$ et les mines d`où sont extraits ses matériaux. Mais qui peut y resister?",
		choicesLabels:[ "Si je peux me le payer, pourquoi m`en priver?", "Je n`en ai pas besoin!" ],
		Card:["proto_card1_a","proto_card1_b"],
	},
		proto_card1_a:{
			usesSuper:"proto_abstract_card",
			imageSrc:"iphoneAchete.png",
			effect:{
				proto_environment:-200,
				proto_solidarity:0,
				proto_democracy:0,
				proto_peace:0,
			},
			title:"C`était cher mais ça en valait le coup!",
			description:"Bon je sais que ce n`est pas très bon pour la planète, mais j`ai hâte de le montrer à mes amis!",
		},
		proto_card1_b:{
			usesSuper:"proto_abstract_card",
			imageSrc:"cultiverAvecArrosoir.png",
			effect:{
				proto_environment:0,
				proto_solidarity:200,
				proto_democracy:0,
				proto_peace:0,
			},
			title:"Mon téléphone actuel me suffit amplement",
			description:"Pourquoi toujours se ruer sur le dernier modèle? Je me suis inscrit à un jardin communautaire. Au lieu de niaiser des heures sur instabookeeter, je cultive mes propres carottes!",
		},


		
	proto_card2:{
		usesSuper:"proto_abstract_card",
		imageSrc:"boiteAliments.png",
		title:"Gros rabais au Costco!",
		description:"Pour 20 boîtes de prêt-à-manger achetées, 2 gratuites!",
		choicesLabels:[ "Ça tombe bien j`ai pas le temps de cuisiner!", "Je préfère les aliments frais" ],
		Card:["proto_card2_a","proto_card2_b"],
	},
		proto_card2_a:{
			usesSuper:"proto_abstract_card",
			imageSrc:"panierPlein1.png",
			effect:{
				proto_environment:-100,
				proto_solidarity:-100,
				proto_democracy:0,
				proto_peace:0,
			},
			title:"Une bonne affaire?",
			description:"Beaucoup d`emballage à gérer, et pas mal de produits chimiques à digérer! Espérons que ce ne sera pas à toutes les semaines.",
		},
		proto_card2_b:{
			usesSuper:"proto_abstract_card",
			imageSrc:"cuisinier.png",
			effect:{
				proto_environment:0,
				proto_solidarity:100,
				proto_democracy:0,
				proto_peace:100,
			},
			title:"Le plaisir de bien manger!",
			description:"J`aime préparer des plats simples, avec de bons ingrédients. C`est rare que les invités s`en plaignent!",
		},
	
			
			

	proto_card3:{
		usesSuper:"proto_abstract_card",
		imageSrc:"lampe.png",
		title:"Quelles énergies pour le futur?",
		description:"Nos activités humaines requièrent de l`énergie, mais notre environnement a ses limites. Devons-nous nous lancer à corps perdu dans le renouvellable ou utiliser le fossile pour faire notre transition?",
		choicesLabels:[ "Le pétrole nous sortira de l`impasse!", "Autant aller tout de suite dans le renouvellable" ],
		Card:["proto_card3_a","proto_card3_b"],
	},
		proto_card3_a:{
			usesSuper:"proto_abstract_card",
			imageSrc:"stationPetroliere.png",
			effect:{
				proto_environment:-100,
				proto_solidarity:0,
				proto_democracy:0,
				proto_peace:-100,
			},
			title:"Difficile de briser notre addiction au pétrole",
			description:"Les effets de rebond ont empiré notre addiction au pétrole. Cette énergie qui devait nous sortir de la dépendance aux ressources fossiles nous y enferme un peu plus. La solution est ailleurs.",
		},
		proto_card3_b:{
			usesSuper:"proto_abstract_card",
			imageSrc:"fleursEtOiseaux.png",
			effect:{
				proto_environment:0,
				proto_solidarity:100,
				proto_democracy:50,
				proto_peace:50,
			},
			title:"Une solution simple n`existe pas",
			description:"Les technologies actuelles pour fournir de l`énergie renouvellable sont très gourmandes en ressources et ler adoption massive précipite l`effondrement environnemental. La solution est ailleurs.",
		},


		
		
	proto_card4:{
		usesSuper:"proto_abstract_card",
		imageSrc:"transports.png",
		title:"Le transport : contrainte, ou liberté ?",
		description:"Se déplacer est à la fois une nécessité et un plaisir. Mais comment concilier notre besoin de liberté et la limitation de l`impact environnemental de nos déplacements ?",
		choicesLabels:[ "L`auto électrique est la solution!", "Développons les transports en commun." ],
		Card:["proto_card4_a","proto_card4_b"],
	},
		proto_card4_a:{
			usesSuper:"proto_abstract_card",
			imageSrc:"voiture.png",
			effect:{
				proto_environment:-50,
				proto_solidarity:-150,
				proto_democracy:0,
				proto_peace:0,
			},
			title:"Le transport individuel doit être repensé",
			description:"En renouvellant le parc d`automobiles thermiques vers de l`électrique, nous ons réussi à réduire les impacts d`émissions de gaz à effet de serre, mais au prix de beaucoup de ressources, et sans régler le probleme de fond.",
		},
		proto_card4_b:{
			usesSuper:"proto_abstract_card",
			imageSrc:"busStop.png",
			effect:{
				proto_environment:100,
				proto_solidarity:100,
				proto_democracy:0,
				proto_peace:0,
			},
			title:"Nous avons fait un pas, mais nous ne sommes pas sortis d`affaire.",
			description:"Malgré un faible impact environnemental, développer les transports en commun ne s`est pas fait sans heurts et a mis en évidence les problèmes d`organisation de l`espace, que nous devons à présent régler.",
		},
		
		


	proto_card5:{
		usesSuper:"proto_abstract_card",
		imageSrc:"bacAFleurs.png",
		title:"L`agriculture urbaine, solution ou simple mode?",
		description:"Les produits issus de l`agriculture urbaine ont le vent en poupe. Mais est-ce une fausse bonne idée ou une tendance passagère et superficielle ?",
		choicesLabels:[ "Allons jusqu`au bout!", "C`est contre-productif." ],
		Card:["proto_card5_a","proto_card5_b"],
	},
		proto_card5_a:{
			usesSuper:"proto_abstract_card",
			imageSrc:"planteEnPotSuspendu.png",
			effect:{
				proto_environment:150,
				proto_solidarity:50,
				proto_democracy:50,
				proto_peace:-50,
			},
			title:"La majorité des produits frais consommés en ville sont produits sur place",
			description:"La production locale urbaine a rendu les villes beaucoup plus résilientes. Par contre le monde rural, déjà au bord du gouffre, en a souffert et la fracture villes/campagnes s`est accentuée.",
		},
		proto_card5_b:{
			usesSuper:"proto_abstract_card",
			imageSrc:"frigoVide.png",
			effect:{
				proto_environment:-50,
				proto_solidarity:-50,
				proto_democracy:-50,
				proto_peace:-50,
			},
			title:"L`agriculture urbaine reste un simple agrément.",
			description:"Les espace d`agriculture urbaine emnbelissent nos villes sans empiéter sur les stationnements et les habitations, ce qui garantit des revenus importants aux municipalités, mais aggrave la dépendance des villes aux importations de nourriture et des fermes industrielles.",
		},
		
	
	
	proto_card6:{
		usesSuper:"proto_abstract_card",
		imageSrc:"cielEtOiseaux.png",
		title:"Une proposition audacieuse",
		description:"Un think tank propose de lancer des millions de tonnes de sulfure dans la haute atmosphère, pour contrer le réchauffement climatique, et a les moyens de le faire. Franchement, au point où on est rendus, pourquoi on essayerait pas?",
		choicesLabels:[ "Allez! Qui ne tente rien...", "Mais vous êtes dingues ?" ],
		Card:["proto_card6_a","proto_card6_b"],
	},
		proto_card6_a:{
			usesSuper:"proto_abstract_card",
			imageSrc:"merNappePetrole.png",
			effect:{
				proto_environment:-100,
				proto_solidarity:0,
				proto_democracy:0,
				proto_peace:0,
			},
			title:"Un lourd fiasco.",
			description:"Après plusieurs années sans nouvelles, l`information a fuité que les conséquence de cette opération qui a coûté des milliards, sont la mort de la majorité du phytoplancton.... Heureusement que l`amnistie totale avait été accordée en avance aux instigateurs du projet, sinon ils seraient actuellement derrière les barreaux!",
		},
		proto_card6_b:{
			usesSuper:"proto_abstract_card",
			imageSrc:"kayaksEtVelos.png",
			imageSrc:"none",
			effect:{
				proto_environment:0,
				proto_solidarity:0,
				proto_democracy:0,
				proto_peace:0,
			},
			title:"On l`a echappé belle.",
			description:"Quelques temps après avoir retoqué cette expérience, des simulations ont montré que ça aurait eu des effets catastropiques sur l`environnement...Mais attends! Un autre think tank propose une autre solution à base de milliards de balles de golf relachées dans l`océan pour faire baisser la température!",
			forceACardChoice:true,
			choicesLabels:[ "Okay peut-être que ça va marcher...", "Quoi j`ai même pas le choix de refuser?!" ],
			Card:["proto_card6_a"],
		},
		
			
			
	
	proto_card7:{
		usesSuper:"proto_abstract_card",
		imageSrc:"chienVaniteux.png",
		title:"Le bien-être animal, c`est important.",
		description:"Kiki, ton caniche nain a la peau très délicate et aurait besoin d`une petite laine pour garder son pelage soyeux pendant les longs mois d`hiver, Problème ce petit manteau en poil de vison est un peu cher.",
		choicesLabels:[ "La carte de crédit sert exactement à ça", "Il est temps pour moi d`apprendre le tricot!" ],
		Card:["proto_card7_a","proto_card7_b"],
	},
		proto_card7_a:{
			usesSuper:"proto_abstract_card",
			imageSrc:"chienHeureux.png",
			effect:{
				proto_environment:-50,
				proto_solidarity:-50,
				proto_democracy:0,
				proto_peace:0,
			},
			title:"Certains animaux sont plus égaux que les autres",
			description:"Kiki semble détester son nouveau manteau, sans doute parce qu`il sent l`odeur d`un autre animal. En plus ça va me prendre des années à le rembourser. Mais quel ingrat !",
		},
		proto_card7_b:{
			usesSuper:"proto_abstract_card",
			imageSrc:"manteauPourChien.png",
			effect:{
				proto_environment:50,
				proto_solidarity:0,
				proto_democracy:0,
				proto_peace:50,
			},
			title:"Des tricots pour tout le monde!",
			description:"Depuis que j`ai commencé à tricoter je ne m`arrête plus. Tout le monde commence à en avoir marre que je leur en offre, j`en ai même fait pour habiller les arbres de mon quartier.",
		},
		
	
	
	proto_card8:{
		usesSuper:"proto_abstract_card",
		imageSrc:"ecranFinances.png",
		title:"Tout le monde fait sa part.",
		description:"Faire la transition écologique, ça coûte cher. Heureusement le monde entier semble prêt à faire des sacrifices. Mais à qui doit-on les demander en premier ? Aux grosses entreprises quite à faire ralentir l`économie ou aux petites gens quite à appauvrir le peuple?",
		choicesLabels:[ "Les entreprises participent déjà suffisement", "Le peuple n`a pas à payer pour les pollueurs!" ],
		Card:["proto_card8_a","proto_card8_b"],
	},
		proto_card8_a:{
			usesSuper:"proto_abstract_card",
			imageSrc:"personneProblemesFinanciers.png",
			effect:{
				proto_environment:0,
				proto_solidarity:-50,
				proto_democracy:-50,
				proto_peace:-100,
			},
			title:"Le peuple se sent trahi",
			description:"Les efforts de transition reposent désormais sur les plus pauvres, qui sont déjà le plus durement touchés par la crise écologique. La société est au bord de l`implosion sociale sous les inégalités qui se creusent.",
		},
		proto_card8_b:{
			usesSuper:"proto_abstract_card",
			imageSrc:"graphiqueHaussePrix.png",
			effect:{
				proto_environment:50,
				proto_solidarity:50,
				proto_democracy:50,
				proto_peace:50,
			},
			title:"Principe du pollueur-payeur",
			description:"Les grosses compagnies polluantes n`en reviennent pas d`avoir perdu leur privilège d`exploiter l`environnement et de facturer au public la réparation les dégâts. Des menaces ont été proférées mais se sont avérées être du bluff.",
		},

	
			
	proto_card9:{
		usesSuper:"proto_abstract_card",
		imageSrc:"bus.png",
		title:"La question des services publics.",
		description:"Il est primordial que les services soient performants, pour faire face à la crise. Comment les rendre encore plus efficaces?",
		choicesLabels:[ "Privatisons le plus possible", "Réinvestissons massivement" ],
		Card:["proto_card9_a","proto_card9_b"],
	},
		proto_card9_a:{
			usesSuper:"proto_abstract_card",
			imageSrc:"enseignes.png",
			effect:{
				proto_environment:0,
				proto_solidarity:-100,
				proto_democracy:-100,
				proto_peace:0,
			},
			title:"Désastre prévisible",
			description:"Donner tous les services publics au privé a fait exploser les coûts et réduit l`efficacité et l`accessibilités des services de base au strict minimum. Seuls les plus riches y ont accès, mais par contre, les actionnaires sont ravis!",
		},
		proto_card9_b:{
			usesSuper:"proto_abstract_card",
			imageSrc:"personnesQuiAttendentLeBus.png",
			effect:{
				proto_environment:0,
				proto_solidarity:100,
				proto_democracy:0,
				proto_peace:100,
			},
			title:"Des services publics pour tout le monde",
			description:"Tout le monde peut enfin avoir accès à des services publics complets et de qualité, ce qui a grandement réduit les problèmes sociaux. Les citoyens, moins préoccupés par leur survie, peuvent désormais consacrer leur énergie à résoudre la crise chacun à son échelle.",
		},



	proto_card10:{
		usesSuper:"proto_abstract_card",
		imageSrc:"lunettesSoleil.png",
		title:"Human++.",
		description:"Le CEO du groupe techno-religieux Human++ pense que l`on ne résoudra la crise écologique qu`en transcendant notre condition d`humain, grâce aux hautes technologies cybernétiques.",
		choicesLabels:[ "Ce type est un génie! Mon chéquier, vite!", "On peut-tu être sérieux 2 minutes?" ],
		Card:["proto_card10_a","proto_card10_b"],
	},
		proto_card10_a:{
			usesSuper:"proto_abstract_card",
			imageSrc:"trader.png",
			effect:{
				proto_environment:-100,
				proto_solidarity:-100,
				proto_democracy:0,
				proto_peace:0,
			},
			title:"Money--",
			description:"Il se trouve que c`était juste une arnaque et on a plus aucune nouvelle de ce leader, disparu juste après sa levée de fonds pour des implants qui se sont avérés n`exister que dans des vidéos promotionnelles.",
		},
		proto_card10_b:{
			usesSuper:"proto_abstract_card",
			imageSrc:"filleQuiLit.png",
			effect:{
				proto_environment:100,
				proto_solidarity:100,
				proto_democracy:0,
				proto_peace:0,
			},
			title:"Et pourtant, de vraies solutions existent",
			description:"Des années plus tard, les historiens ont encore du mal à croire que ce mouvement n`était pas une simple parodie destinée à alerter des dangers du techno-scientisme de notre époque. On l`a échappé belle.",
		},



	proto_card11:{
		usesSuper:"proto_abstract_card",
		imageSrc:"parentEtEnfant.png",
		title:"Les écoles ont besoin d`un coup de pouce",
		description:"Les élèves et les professeurs sont de plus en plus conscientisés aux enjeux écologiques, mais manquent grandement de moyens.",
		choicesLabels:[ "Soutenons-les, bien sûr!", "Ce n`est pas aux écoles de faire ça!" ],
		Card:["proto_card11_a","proto_card11_b"],
	},
		proto_card11_a:{
			usesSuper:"proto_abstract_card",
			imageSrc:"femmeEtOiseau.png",
			effect:{
				proto_environment:500,
				proto_solidarity:50,
				proto_democracy:50,
				proto_peace:50,
			},
			title:"Un apprentissage complet",
			description:"Les écoles et les élèves rafolent des activités à thématiques écologiques et sociales, et beaucoup d`enfants en sortent avec une conscience politique et une réflexion de grande qualité sur les enjeux actuels.",
		},
		proto_card11_b:{
			usesSuper:"proto_abstract_card",
			imageSrc:"filleSurSonCellulaire.png",
			effect:{
				proto_environment:-50,
				proto_solidarity:-100,
				proto_democracy:0,
				proto_peace:0,
			},
			title:"Les écoles restent des creusets à inégalités",
			description:"Les écoles publiques continuent de tomber en ruine et les enjeux écologiques et sociaux passent complètement à la trappe. Les enseignants ont pour consigne de ne former que des ouvriers, ou des cadres dans les écoles privées. Les inégalités se creusent de plus en plus tôt dans la vie.",
		},


};
