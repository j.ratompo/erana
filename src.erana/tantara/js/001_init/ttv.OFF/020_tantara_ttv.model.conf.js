// DEPENDENCY:init.js




// ===============================================================================
// MODEL
// ===============================================================================





// Prototype can define as a well a group of things as particular
// individuals:only by not setting a random variance on them.



PROTOTYPES_CONFIG = {

	allPrototypes:{
		
		
		// ------------------ Levels objects ------------------
		
		GameLevel:{
			
			
			proto_level_1:{


				
				selectedItem:null,
				
				Scenario:"proto_scenario1",
			},
			
		},
		
		
		
		// ------------------ Game objects ------------------
		
		Scenario:{
			
			proto_scenario1:{		


				containerConfig:"starter", // To indicate where the player starts in the tree objects hierarchy
				
				
//				backgroundColor:"#FEE7A2",
				backgroundColor:"#FFFFFF",


				
//				backgroundZIndex:1,
//				backgroundParallax:10,
//				backgroundOffsetY:0,
//				backgrounds:[
//// {images:["backgroundSky1.png","backgroundSky2.png","backgroundSky3.png","backgroundSky4.png"],parallaxAdd:100},
//// //
//// {images:["backgroundFore1.png","backgroundFore2.png","backgroundFore3.png","backgroundFore4.png"],parallaxAdd:10},
//// {images:["background1.png","background2.png","background3.png","background4.png"]},
//
//// {images:["backgroundSky1.png"],parallaxAdd:100},
//// {images:["background1.png"],parallaxAdd:10},
//// {images:["backgroundFore1.png"]},
//
//					{images:["backgroundSky1_v1.png"],parallaxAdd:400},
//					{images:["background1_v1.png"],parallaxAdd:200},
//					{images:["backgroundFore1_v1.png"],parallaxAdd:20},
//					{images:[{normal:"foregroundGround1.png"}],offsetY:-230,clipSize:{w:1600,h:100}},
//
//				],
				
				
//				foregroundParallax:1,
//// foregroundOffsetY:10,
//				foregrounds:[
//// {images:["foreground1.png"],offsetY:-20},
//					{images:[ {normal:"foreground1_v1.png",dark:"foreground1.dark.png"} ],offsetY:-20},
//				],
				
				
				
				
//				RoleCharacter:["proto_roleCharacter1","proto_roleCharacter1"],
				RoleCharacter:["proto_roleCharacter1"],
				
				
				
			}
			
		},

		
		
		RoleCharacter:{
			
			proto_roleCharacter1:{
				

				Deck:["proto_deck1"],
				
				
//				GameGauge:{
//				
//					cardinality:{
//						value:"staticGroups",
//						prototypesInstantiation:{
//							staticInstantiations:[
////								{name:"proto_health",spawningZone:{x:0,y:0},preinstanciate:true},
////								{name:"proto_thriving",spawningZone:{x:0,y:0},preinstanciate:true},
////								{name:"proto_happiness",spawningZone:{x:0,y:0},preinstanciate:true},
////								{name:"proto_security",spawningZone:{x:0,y:0},preinstanciate:true},
//
//								{name:"proto_environment",spawningZone:{x:0,y:0},preinstanciate:true},
//								{name:"proto_solidarity",spawningZone:{x:0,y:0},preinstanciate:true},
//								{name:"proto_democracy",spawningZone:{x:0,y:0},preinstanciate:true},
//								{name:"proto_peace",spawningZone:{x:0,y:0},preinstanciate:true},
//								
//							]
//						},
//					},
//				
//				},

				
				GameGauge:["proto_environment", "proto_solidarity", "proto_democracy", "proto_peace",],

				
				
			},

			
		},
		
		
		GameGauge:{
			
			proto_abstractGauge:{
				
//				gaugeValue:1000,
//				gaugeMax:2000,

				// DBG
				gaugeValue:150,
				gaugeMax:300,

				displayNumericValue:true,
				
				
//				// DBG
//				gaugeValue:50,
//				gaugeMax:100,
				
//				overridingPosition:{ x:0,y:0 },
				overridingPosition:{ x: -GAME_CONFIG.getResolution().w*.1 -25  ,y:10 },
//				overridingPosition:{ x: -25  ,y:10 },

			},
			
			
			proto_environment:{
				usesSuper:"proto_abstractGauge",
				GameThumb:"proto_environmentThumb",
			},
			proto_solidarity:{
				usesSuper:"proto_abstractGauge",
				GameThumb:"proto_solidarityThumb",
			},
			proto_democracy:{
				usesSuper:"proto_abstractGauge",
				GameThumb:"proto_democracyThumb",
			},
			proto_peace:{
				usesSuper:"proto_abstractGauge",
				GameThumb:"proto_peaceThumb",
			},
			
			
		},
		
		
		GameThumb:{

			proto_abstract_thumb:{
				sizeFactor:1,
				scaleFactor:1,
				
//				overridingPosition:{ x: -GAME_CONFIG.getResolution().w*.2 -25  ,y:10 },

			},
			proto_environmentThumb:{
				usesSuper:"proto_abstract_thumb",
				offsetsUI:{x:GAME_CONFIG.getResolution().w*.2},
				thumbSrc:"environment.thumb.png",
			},
			proto_solidarityThumb:{
				usesSuper:"proto_abstract_thumb",
				offsetsUI:{x:GAME_CONFIG.getResolution().w*.4},
				thumbSrc:"solidarity.thumb.png",
			},
			proto_democracyThumb:{
				usesSuper:"proto_abstract_thumb",
				offsetsUI:{x:GAME_CONFIG.getResolution().w*.6},
				thumbSrc:"democracy.thumb.png",
			},
			proto_peaceThumb:{
				usesSuper:"proto_abstract_thumb",
				offsetsUI:{x:GAME_CONFIG.getResolution().w*.8},
				thumbSrc:"peace.thumb.png",
			},
			
		},

		
		Deck:{

//			proto_testDeck:{
//				
//				overridingPosition:{
//					x:0,y:0
//				},
//				
////				Card:["proto_card1","proto_card2","proto_card3","proto_card4","proto_card5","proto_card6"],
////				Card:["*"],
//					Card:["proto_card1","proto_card2",
//								"proto_firstCard","proto_cardFail1", "proto_cardFail2", "proto_cardFail3", "proto_cardFail4", "proto_cardSuccess","proto_cardNoIssue"],
//				
//			},
			
			
			proto_deck1:{
				
				overridingPosition:{
					x:0,y:0
				},
				
//				Card:["proto_card1","proto_card2","proto_card3","proto_card4","proto_card5","proto_card6"],
//				Card:["*"],
				
					// TODO
					Card:["proto_firstCard","proto_cardFail1", "proto_cardFail2", "proto_cardFail3", "proto_cardFail4", "proto_cardSuccess","proto_cardNoIssue",
								"proto_card1","proto_card2","proto_card3","proto_card4","proto_card5",
								"proto_card6","proto_card7","proto_card8","proto_card9","proto_card10","proto_card11",
								],
				
								
			},
		
		},
		
		
		
		
		
		
		
	},
		
		

};
