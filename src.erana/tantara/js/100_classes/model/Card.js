
const DEFAULT_CARDS_SET_NAME="default";
const CARD_POSITION_MOUSE_FACTOR=1;
const CARD_ANGLE_MOUSE_FACTOR=.01;

const DEFAULT_MAX_FACTOR_X_TO_HINT_SWIPE=.1;
//const DEFAULT_MAX_FACTOR_X_TO_TRIGGER_SWIPE=.4;
const DEFAULT_MAX_FACTOR_X_TO_TRIGGER_SWIPE=.25;

const CARD_HINT_OFFSET_Y=300;


class Card{

	constructor(){

		this.isVisible=true;
		this.isMovable=true;

//	IMPLICIT:	this.position=null;
		
		this.angleDegrees=0;
		
		this.choiceSideIndex=null;

		this.descriptionsInfos=[];
		this.divImageCanvas=[]; // An array of canvas images
		this.divImageCanvasBack=null; // A single canvas image
		
		this.hintDivs=[];
		this.divHintImagesCanvas=[]; // An array of canvas images
		
		this.isSeen=false;

		

		this.pointerX=null;
		this.pointerY=null; // CAUTION : is null if we have an «horizontal» scroill !
		
		this.mouseXHintFactor=null;
		this.mouseXTriggerFactor=null;
		
		
		this.gameChoiceWheel=null;
		
		
	}

	// INIT
	init(gameConfig){
		this.gameConfig=gameConfig;
		
		
		this.mouseXHintFactor=nonull(this.mouseXHintFactor,DEFAULT_MAX_FACTOR_X_TO_HINT_SWIPE);
		this.mouseXTriggerFactor=nonull(this.mouseXTriggerFactor,DEFAULT_MAX_FACTOR_X_TO_TRIGGER_SWIPE);
		
		
		// We init choices availabilities to true :
		if(this.choicesLabels){
			this.choices=[];
			const self=this;
			foreach(this.choicesLabels,(choiceLabel)=>{
				self.choices.push( {label:choiceLabel, isAvailable:true} );
			});
		}else{
			foreach(this.choices,(choice)=>{
				choice.isAvailable=true;
			});
		}
		
	}
	
	
	
	initWithParent(){
	
	
		let self=this;
	
		
		// Fronts of the card :
		
		
		if(this.description){
	
			const splitsDescription=this.description.split("#");
			if(2<=splitsDescription.length){
				for(let i=1;i<splitsDescription.length;i++){// (we ignore the first one)
					let splitDescription=splitsDescription[i];
					let splitsConditionAndValue=splitDescription.split("::");
					if(splitsConditionAndValue.length==2){
						let condition=splitsConditionAndValue[0];
						let stringToDisplay=splitsConditionAndValue[1];
						let splitsVariableNameAndValue=condition.split("=");
						if(splitsVariableNameAndValue.length==2){
							let variableName=splitsVariableNameAndValue[0];
							let variableValue=splitsVariableNameAndValue[1];
							
							this.descriptionsInfos.push({variableName:variableName,variableValue:variableValue,stringToDisplay:stringToDisplay});
				
						}else{
						
							this.descriptionsInfos.push({stringToDisplay:stringToDisplay}); // CAUTION : the #else condition MUST BE PLACED AT THE END !
						}
					}
				}
			}else{
				this.descriptionsInfos.push({stringToDisplay:this.description});
			}
		}
		
		
		foreach(this.descriptionsInfos,(description,index)=>{
			self.createSingleCardFrontDiv(index,description.stringToDisplay);
		});
		
		

		// Back of the card :
		this.createSingleCardDiv(null,"","divImageCanvasBack","cardBack",true);



		// Hints :
		foreach(self.choices,(choice,index)=>{
			self.createSingleHintDiv(index, choice);
		});
		
		
		// Sides flipping and choiceswheel:
		const gameConfig=this.gameConfig;
		// If we do want to randomize answers sides :
		let projection = gameConfig.projection;
		if(contains(projection, "2D")){
			// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
			let scroll = gameConfig.scroll;
			if(contains(scroll, "horizontal")){
				
				// Sides flipping :
				if(!this.fixedSides){
					if(Math.getRandomInt(2)==2){
						// We flip them at random :
						this.sidesHaveBeenFlipped=true;
					}
				}
					
			}else{
			
				// Choiceswheel :
				this.gameChoiceWheel=new GameChoiceWheel(this,this.choices,this.wheelChoiceConfig,this.fixedSides)
											.init(this.gameConfig);
											

			}
		}
		
		
	}

	// METHODS
	
	
	
	
	/*public*/getVariable(variableName){
	
		if(!this.parent){
			lognow("CARD PAS DE PARENT:",this);
		}
	
		return this.parent.getVariable(variableName);
	}
	
	
	/*private*/hasVariableValue(variableName, variableValueExpected){
		const variable=this.getVariable(variableName);
		if(!variable)	return null;
		return (variable==variableValueExpected);
	}
	
	
	/*private*/createSingleCardFrontDiv(index, description){
	
		
		let html="";

		// Front of the card :
		if(this.title){
			html+="<h1>"+this.title+"</h1>";
		}
		
		
		html+="<div class='text'>"+description+"</div>";
		

		let imageSrc=this.imageSrc;
		if(imageSrc && imageSrc!=="none"){
			let suffix="";
			if(this.isImageDeviceDependent){
				if(this.gameConfig.isMobile)	imageSrc=imageSrc.replace(".","_mobile.");
				else													imageSrc=imageSrc.replace(".","_desktop.");
			}
			const imgSrc=getConventionalFilePath(this.gameConfig.basePath, nonull(this.cardsSetPath,DEFAULT_CARDS_SET_NAME+"/")+"cardImages/", "universe") + imageSrc;
			// ALTERNATIVE : const imageSrc=getConventionalFilePath(this.gameConfig.basePath,this,"universe")+"card.png";
			html+="<div class='image'><img src='"+imgSrc+"' /></div>";
		}
		
		
		this.createSingleCardDiv(index,html);
		
	}
	
	
		
	/*private*/getDivFrontImageCanvas(){
	
		const self=this;
		let indexToChoose=nonull(foreach(self.descriptionsInfos,(description,index)=>{
			let variableName=description.variableName;
			let variableValue=description.variableValue;
			if((!variableName && !variableValue) || self.hasVariableValue(variableName, variableValue)){
				return index;
			}
		}),0);
	
		let divImageCanvas=this.divImageCanvas[indexToChoose];
	
		return divImageCanvas;
	}
	
	
	/*private*/createSingleCardDiv(index, html, divImageCanvasAttrName="divImageCanvas", cssClassName="card", isStoredInSingleImageAttribute=false){

		if(!this.size){
			// TRACE
			lognow("ERROR : this card is not well defined !",this);
			return;
		}


		let div=document.createElement("div");
		
		//const zooms=this.gameConfig.zooms;
		
		div.innerHTML=html;
		//div.style.width=(this.size.w * zooms.zx)+"px";
		//div.style.height=(this.size.h * zooms.zy)+"px";
		div.style.width=this.size.w+"px";
		div.style.height=this.size.h+"px";
		
//		div.style.fontSize=this.fontSizePercent+"vw";
//		div.style["background-color"]=this.backgroundColor;
//		div.style.color=this.color;
//		div.style.border=this.borderStyle;
		div.className=cssClassName;
		document.body.appendChild(div);
		
		
		const self=this;
		this.toImage(div, (canvas)=>{
				// NO : SLOWER AND NOT AFFECTED BY ROTATION !: self.imageData=canvas.getContext("2d").getImageData(0,0,200,200);
				if(isStoredInSingleImageAttribute){
					// A single image :
					self[divImageCanvasAttrName]=canvas;
				}else{
					// An array of images :
					if(index==null)	self[divImageCanvasAttrName].push(canvas);
					else			self[divImageCanvasAttrName].splice(index,0,canvas); // To keep the right order, despite the callback arbitrary order !
				}
				document.body.removeChild(div);
			}		
		);
		
	}
	
	/*private*/createSingleHintDiv(index, choice, divHintImagesCanvasAttrName="divHintImagesCanvas"){
		
		const choiceText=nonull(choice.label, choice);
		let imageSrc=nonull(choice.imageSrc,null);
		
		let divHint=document.createElement("div");
//		DOES NOT WORK : Currently used renderer glitches with cssed sub-containers ! divHint.innerHTML="<div class='text'>"+choiceText+"</div>";
		// DEBUG
		let html=choiceText;
		
		//const zooms=this.gameConfig.zooms;

		//divHint.style.width=(this.hintSize.w * zooms.zx)+"px";
		divHint.style.width=this.hintSize.w+"px";
		
		if(imageSrc && imageSrc!=="none"){
			const imgSrc=getConventionalFilePath(this.gameConfig.basePath, nonull(this.cardsSetPath,DEFAULT_CARDS_SET_NAME+"/")+"cardImages/", "universe") + imageSrc;
			// ALTERNATIVE : const imageSrc=getConventionalFilePath(this.gameConfig.basePath,this,"universe")+"card.png";
			html+="<div class='image'><img src='"+imgSrc+"' /></div>";
			
			//divHint.style.height=(60 * zooms.zy)+"vw";
			divHint.style.height="60vw";
		}else{
		
			//divHint.style.height=(this.hintSize.h * zooms.zy)+"px";
			divHint.style.height=this.hintSize.h+"px";
		}
		
		divHint.innerHTML=html;
		


//		divHint.style["background-color"]=this.hintBackgroundColor;
//		divHint.style.color=this.hintColor;
//		divHint.style.border=this.borderStyleHint;
//		divHint.style.fontSize=this.fontSizePercent+"vw";
		divHint.className="hint";
		
		document.body.appendChild(divHint);
		this.hintDivs.push(divHint);
		
		
		const self=this;
		this.toImage(divHint, (canvas)=>{
			// An array of images only :
			if(index==null)	self[divHintImagesCanvasAttrName].push(canvas);
			// To keep the right order, despite the callback arbitrary order ! :
			else						self[divHintImagesCanvasAttrName].splice(index,0,canvas);
			document.body.removeChild(divHint);
		});
		
		
	}
	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	
	getIsMovable(){
		return this.isMovable;
	}
	
	getPlaceables(){
		return [ 
			// WE NEED ALL CHILDREN CARDS TO BE PLACED ! (BUT NOT NCESSARILY DRAWN)
			this.cards
		];
	}
	

	
	/*public*/swiping(deltaMouseX,deltaMouseY,pointerXParam,pointerYParam){
		
		if(!this.basePosition)	return;// Can happen if we swipe before the card is fully loaded ! :
		
		let newX=(this.basePosition.x + deltaMouseX*CARD_POSITION_MOUSE_FACTOR);
		let newY=((deltaMouseY!=null?(this.basePosition.y + deltaMouseY*CARD_POSITION_MOUSE_FACTOR):null));
		
		this.position.setLocation({x:newX,y:newY});
		this.angleDegrees=this.baseAngle + deltaMouseX*CARD_ANGLE_MOUSE_FACTOR;

//		// DBG
//		lognow("SWIPING ! deltas:"+deltaMouseX+";"+deltaMouseY);
		
		this.deltaMouseX=deltaMouseX;
		this.deltaMouseY=deltaMouseY;
		
		
		if(!this.isInHintZone()){
			this.choiceSideIndex=null;
			// NO (or else it will mess with the location !) this.resetLocationToBase();
			
			if(this.gameChoiceWheel)	this.gameChoiceWheel.reset();
			
			return;
		}
		
		const gameConfig=this.gameConfig;
		let projection = gameConfig.projection;
		if(contains(projection, "2D")){
		
			// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
			let scroll = gameConfig.scroll;
			if(contains(scroll, "horizontal")){
		
				if(deltaMouseX!=0){
					if(deltaMouseX<0){
						this.choiceSideIndex=0;
		
		//				// DBG
		//				lognow("SWIPING LEFT this.choiceSideIndex:"+this.choiceSideIndex);
		
					}else{
						this.choiceSideIndex=1;
		
		//				// DBG
		//				lognow("SWIPING RIGHT this.choiceSideIndex:"+this.choiceSideIndex);
		
					}
					
					
					// If we do want to randomize answers sides :
					if(this.sidesHaveBeenFlipped){
						// We flip them at random :
						this.choiceSideIndex=(this.choiceSideIndex==0?1:0);
					}
					
					
					this.pointerX=pointerXParam;
					this.pointerY=pointerYParam;
					
				}
			
			}else{
			
				if(deltaMouseX!=0 || deltaMouseY!=0){
				
			
					this.choiceSideIndex=this.gameChoiceWheel.getChoiceIndex(deltaMouseX,deltaMouseY);
					
					this.pointerX=pointerXParam;
					this.pointerY=pointerYParam;
					
				}
			
			
			}
		}
		
		
	}
	
	// For child cards only :
	/*public*/getTurnsManager(){
		return this.parent.getTurnsManager();
	}
	
	
	/*public*/getDeactivationReason(){
		const turnsManager=this.parent.getTurnsManager()
		if(turnsManager && !turnsManager.isMyTurn()){
			return "Not your turn !";
		}
		return null;
	}
	
	/*public*/swipe(directionAngle/*UNUSED PARAM HERE*/){
	
		this.finalizeChoice(this.choiceSideIndex);
	}
	
	/*public*/finalizeChoice(choiceIndexParam, forceChoice=false){
		
		
		if(this.gameChoiceWheel)	this.gameChoiceWheel.reset();
		
		
		if(this.isTerminal || (!forceChoice && !this.isInTriggerZone())){
			this.resetLocationToBase();
			return;
		}else if(this.isFinal()){
			// In this case, we just reset the card position after a non-possible choice :
			this.resetLocationToBase();
		}
		
		
//		// DBG
//		lognow("SWIPE ! directionAngle:"+directionAngle);

		const deactivationReason=this.getDeactivationReason();
		if(deactivationReason){
			// TRACE
			lognow("WARN : "+deactivationReason);
			this.resetLocationToBase();
			return;
		}
		
		
		if(!nothing(this.cards) && !empty(this.cards)){
				
			const lastChildCardIndex=this.cards.length-1;
			if(choiceIndexParam<=lastChildCardIndex){
				
				// If the user's choice is within the choices possibilities :
				this.finalizeChoiceChildCard(choiceIndexParam);
				
			}else{
				
				// If the user cannot make this choice because card is terminal, then we stall the user here :
				if(this.isTerminal){
					this.resetLocationToBase();
					return;
				}else{

					// ...else, if the user's choice is outside the choices possibilities :
					if(this.forceACardChoice){
						// If we force the user to choice within the cards anyway :
						if(!empty(this.cards)){
						
							// We chose the last possible card !
							this.finalizeChoiceChildCard(lastChildCardIndex);
							
						}else{
							// The user may proceed to next card :
							
							this.finalizeChoiceNextCard();
							
						}
					}else{
						// ...else if we don't force the user to choice within the cards anyway, then
						// the user may proceed to next card :
						
						
						this.finalizeChoiceNextCard();
						
					}

				}
			}
			
			
		}else{ // ...if there's no cards left in the deck :
		
			if(this.isFinal()){
				// The user may proceed to next card :
				
				this.finalizeChoiceNextCard();

				
			}else{
				// If the user cannot make this choice, then we stall the user here :
				if(this.isTerminal){
					this.resetLocationToBase();
					return;
				}else{
					// Else the user may proceed to next card :
					
					this.finalizeChoiceNextCard();

					
				}
			}
		}
		
	}
	
	/*private*/finalizeChoiceChildCard(choiceIndex){
	
		if(empty(this.cards) || choiceIndex==null){
			this.resetLocationToBase();
			return;
		}
		
		const childCard=this.cards[choiceIndex];
		if(!childCard){
			this.resetLocationToBase();
			return;
		}
							
		const choice=(this.choices?this.choices[choiceIndex]:null);
		
		
		if(choice // CAUTION : It is possible to play a child card but there are no choices proposed !
			&& choice.isAvailable==false){
			this.resetLocationToBase();
			return;
		}

		const networkResultIsOnLine=window.eranaScreen.dispatchProcessing("playCard",
//							{cardPrototypeName:this.prototypeName, nextCardPrototypeName:childCard.prototypeName, choiceIndex:choiceIndex, choiceConstraints:choice.constraints, broadcastTo:"all"});
							{cardPrototypeName:this.prototypeName,
							 nextCardPrototypeIndex:(childCard?childCard.cardIndex:null),
							 nextCardPrototypeName:(!childCard || childCard.cardIndex!=null?null:childCard.prototypeName),// We set this attribute only if the child card has no card index !
							 choiceIndex:choiceIndex, choiceConstraints:(choice?choice.constraints:null),choiceEffect:(choice?choice.effect:null),
							 broadcastTo:"all"});
		if(!networkResultIsOnLine){ // (if we're offline)
			this.setCurrentCard(childCard);
		}
							
		this.resetLocationToBase();
	}
	
	/*private*/finalizeChoiceNextCard(){
	
//	NO : A CARD MUST BE ABLE TO HAVE NO CHILDREN ! (in this case we proceeed to next cards in deck):	if(empty(this.cards))	return;
	
		const nextCard=this.getNextCard();
		if(!nextCard)	return;
		
					
		const networkResultIsOnLine=window.eranaScreen.dispatchProcessing("playCard",
//							{cardPrototypeName:this.prototypeName, nextCardPrototypeName:nextCard.prototypeName, broadcastTo:"all"});
							{cardPrototypeName:this.prototypeName,
							 nextCardPrototypeIndex:nextCard.cardIndex,
							 nextCardPrototypeName:(!nextCard || nextCard.cardIndex!=null?null:nextCard.prototypeName),// We set this attribute only if the next card has no card index !
							 broadcastTo:"all"});
							 
		if(!networkResultIsOnLine){ // (if we're offline)
			this.setCurrentCard(nextCard);
		}
										
		this.resetLocationToBase();
		
		
		
	}
	
	
	/*private*/resetLocationToBase(){
		this.choiceSideIndex=null;
		if(this.basePosition)		this.position.setLocation({x:this.basePosition.x, y:this.basePosition.y});
		if(this.baseAngle!=null)	this.angleDegrees=this.baseAngle;
	}

	
	/*public*/getNextCard(){
		// To recursively go up to the deck :
		return this.parent.getNextCard();
	}
	
	/*public*/setCurrentCard(card){
		// To recursively go up to the deck :
		this.parent.setCurrentCard(card);
	}
	
	/*public*/applyEffect(effect){
		// To recursively go up to the deck :
		this.parent.applyEffect(effect);
	}
	
	
	/*public*/setChoicesAvailabilities(choicesAvailabilities){
		const self=this;
		foreach(choicesAvailabilities,(choiceAvailability,index)=>{
			const choice=self.choices[index];
			choice.isAvailable=choiceAvailability;
		});
	}
		
	
	/*public*/getIsSeen(isSeen){
		return this.isSeen;
	}
	
	/*public*/setIsSeen(isSeen){
		this.isSeen=isSeen;
	}
	
	
	/*public*/doEffect(){
		if(this.gameChoiceWheel)	this.gameChoiceWheel.start();
		
		if(this.effect){
			this.applyEffect(this.effect);
		}
	}
	
	/*private*/isFinal(){
		// CAUTION : A card which has only deactivated choices is still NOT FINAL ! (ie. for instance, the choices wheel should still show up !)
		return empty(this.choices);
	}
	
	
	/*private*/toImage(element,callBack){

		let asyncId=window.eranaScreen.addAsyncStart();
		convertHTMLToImage(element,(img)=>{
			callBack(img);
			window.eranaScreen.addAsyncEnd(asyncId);
		});
		
	}
	
	
	/*private*/isInHintZone(){
		return this.isInInteractionZone(this.mouseXHintFactor);
	}
	
	/*public*/isInTriggerZone(){
		return this.isInInteractionZone(this.mouseXTriggerFactor);
	}
	
	
	/*private*/isInInteractionZone(factor){
		let margin=this.gameConfig.getResolution().w*factor;
		// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
		let scroll = this.gameConfig.scroll;
		if(contains(scroll, "horizontal")){
			return margin<=Math.abs(this.deltaMouseX);
		}
		let distance=Math.sqrt(Math.pow(this.deltaMouseX,2)+Math.pow(this.deltaMouseY,2));
		return margin<=distance;
	}
	
	
	
	// ============================================================================
	
	

	/*private*/drawTriggerMarkIfNecessary(ctx, x, y, width, height, forceDrawingTriggerMark){
		
	
		const zooms=this.gameConfig.zooms;
	
		if(forceDrawingTriggerMark || this.isInTriggerZone()){
			
			ctx.lineWidth = 20 * zooms.zx;
			ctx.strokeStyle = "#FFFFFF";
			ctx.strokeRect(x,y,width,height);
			
			
//			let radialGradient = ctx.createRadialGradient(60,60,0,60,60,60);
//			radialGradient.addColorStop(0, 'rgba(255,0,0,1)');
//		  radialGradient.addColorStop(0.8, 'rgba(228,0,0,.9)');
//		  radialGradient.addColorStop(1, 'rgba(228,0,0,0)');
//	
//		  // draw shape
//		  ctx.fillStyle = radialGradient;
// //	  ctx.fillRect(x,y,this.size.w,this.size.h);
//		  ctx.fillRect(x,y,width,height);
			
		  
		}
	
	}
	
	/*private*/getHintDivImageCanvas(choiceSideIndex){
		let divHintImageCanvasIndex=( choiceSideIndex<=this.choices.length-1 ? choiceSideIndex : null );
		if(divHintImageCanvasIndex==null || this.choices[choiceSideIndex].isAvailable==false)	return null;
		return this.divHintImagesCanvas[divHintImageCanvasIndex];
	}
	
	/*private*/drawHintZoomed(ctx, camera, lightsManager, isBack, zooms={zx:1,zy:1}){
		
	
		if(this.isFinal() || this.isTerminal || isBack)	return null;
		
		let hintDivImageCanvas=null;
		let forceDrawingTriggerMark=false;
		let hintOpacity=1;
		
		const pointerXLocal=this.pointerX;
		const pointerYLocal=this.pointerY;

		const resolution=this.gameConfig.getResolution();
		
		// Timeout displays behaviors :
		if(this.gameChoiceWheel){
			if(this.choiceSideIndex==null){
				const defaultChoiceIndex=this.gameChoiceWheel.getDefaultChoiceIndex();
				if(defaultChoiceIndex==null)	return null;
				
				
				hintDivImageCanvas=this.getHintDivImageCanvas(defaultChoiceIndex);
				
				pointerXLocal=((resolution.w)*.5) * zooms.zx;
				pointerYLocal=((resolution.h)-(this.hintSize.h)) * zooms.zy; // TODO : FIXME : MAGIC NUMBER
				
				hintOpacity=.8; // TODO : FIXME : MAGIC NUMBER
				
				forceDrawingTriggerMark=true;
			}else{
				hintDivImageCanvas=this.getHintDivImageCanvas(this.choiceSideIndex);
			}
		}else{
			if(this.choiceSideIndex==null)	return null;
			hintDivImageCanvas=this.getHintDivImageCanvas(this.choiceSideIndex);
			
		}
		
		if(!hintDivImageCanvas)	return null;
		
	
		// NO : SPECIAL CASE : WE HAVE TO IGNORE PARENT OFFSETS HERE !
//		let offset=this.position.centerOffsets;
//		let x=this.basePosition.x+offset.x-this.hintSize.w*.5;
//		let y=this.basePosition.y+offset.y-this.hintSize.h*.5+this.hintRelativePosY;

		let x,y;
		if(this.hintFixed){
			x=this.basePosition.x-this.hintSize.w*.5;
			y=this.basePosition.y-this.hintSize.h*.5;
		}else{
			// Case floating hint :
			x=(pointerXLocal / zooms.zx)-this.hintSize.w*.5;
			y=nonull(pointerYLocal?(pointerYLocal / zooms.zy):null, resolution.h*.5)+this.hintSize.h*.5;
		}


		// Deactivation reason drawing : 
		let deactivationReason=this.getDeactivationReason();
		if(deactivationReason){
		
			return null;
		}else{
		
			y-=nonull(this.hintRelativePosY,0);

			// Hint drawing :		
			ctx.save();
			ctx.globalAlpha=hintOpacity;
			drawImageAndCenterWithZooms(ctx, hintDivImageCanvas, x, y, zooms);
			ctx.restore();
		
		}
		
		
		return {x:x * zooms.zx, y:y * zooms.zy,
						w:hintDivImageCanvas.width * zooms.zx, h:hintDivImageCanvas.height * zooms.zy,
						forceDrawingTriggerMark:forceDrawingTriggerMark}
	}
	
	
	drawInUI(ctx, camera){
		
		if(this.gameChoiceWheel){
			this.gameChoiceWheel.drawInUI(ctx,camera);
		}
		
	}
	


	draw(ctx,camera,lightsManager,isBack=false){

		
		let divImageCanvas=(isBack?this.divImageCanvasBack:this.getDivFrontImageCanvas());
		if(!divImageCanvas)	return false;

		
		if(this.basePosition==null)	this.basePosition={x:this.position.x, y:this.position.y};
		if(this.baseAngle==null)	this.baseAngle=this.angleDegrees;
		
		
		// NO : SLOWER AND NOT AFFECTED BY ROTATION !:
//	ctx.putImageData(this.imageData, 200, 200);
		// NO : SPECIAL CASE : WE HAVE TO IGNORE PARENT OFFSETS HERE !
//	let pos=this.position.getParentPositionOffsetted();
		const pos=this.position;
		const size=this.size;
		
		
		const zooms=this.gameConfig.zooms;
		
		const angleRadians=Math.toRadians(this.angleDegrees);
		
		// To center the rotation on the center of the object :
		
		drawImageAndCenterWithZooms(ctx, divImageCanvas, pos.x, pos.y , zooms, {x:"center",y:"center"},
			1,1,
			size.w,size.h,
			1,
			false,
			null,
			-angleRadians);// Special, because we want a «swinging effect» to draw !
		
		
		// Choices availabilities updates :
		const self=this;
		foreach(this.choices,(choice)=>{
			
			let condition=choice.condition;

			let variableName;
			let variableValueExpected;
			
			if(contains(condition,"=")){
				let splits=condition.split("=");
				variableName=splits[0];
				variableValueExpected=splits[1];
			}
			
			if(!variableName && !variableValueExpected){
				choice.isAvailable=true;
			}else{
				if(!self.hasVariableValue(variableName, variableValueExpected)){
					choice.isAvailable=false;
				}else{
					choice.isAvailable=true;
				}
			}
		},(choice)=>{	return choice.isAvailable!=false && choice.condition;	});

	
		// Hints :
		let hintFrameZoomed=this.drawHintZoomed(ctx,camera,lightsManager,isBack,zooms);
		if(hintFrameZoomed){
			this.drawTriggerMarkIfNecessary(ctx, hintFrameZoomed.x, hintFrameZoomed.y, hintFrameZoomed.w, hintFrameZoomed.h, hintFrameZoomed.forceDrawingTriggerMark);
		}
			
		
		// Choices wheel :
		if(this.gameChoiceWheel){
			this.gameChoiceWheel.draw(ctx, camera, lightsManager);
		}
		
		
		
		return true;
	}
	
}
