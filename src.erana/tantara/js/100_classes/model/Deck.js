class Deck{

	constructor() {

		this.isVisible=true;
		this.isMovable=true;
		
//	test for portrait variables :	this.gru="ijtaj";
		
//	IMPLICIT:	this.position=null;

		this.currentCard=null;
		
	}

	// INIT
	
	initWithParent(){
		
		
		// We init the current card :
		let firstCard=foreach(this.getRootCardsOnly(false),(c)=>{
			if(c.first){
				return c;
			}
		});
		
		if(firstCard){
			this.setCurrentCard(firstCard);
		}else{
			const nextCard=this.getNextCard();
			this.setCurrentCard(nextCard);
		}
		
		
	}
	

	// METHODS
	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	
	getIsMovable(){
		return this.isMovable;
	}
	
	getDrawables(){
		return [ 
			// // NO : overridden in the draw(...) and drawInUI(...) methods :
//		this.cards
		];
	}
	
	getPlaceables(){
		return [ 
			// WE NEED ALL CARDS TO BE PLACED !
			this.cards
		];
	}
	
	
	swiping(deltaMouseX,deltaMouseY,pointerX,pointerY){
		let currentCard=this.getCurrentCard();
		if(!currentCard)	return;
		currentCard.swiping(deltaMouseX,deltaMouseY,pointerX,pointerY);
	}
	
	swipe(directionAngle){
		let currentCard=this.getCurrentCard();
		if(!currentCard)	return;
		currentCard.swipe(directionAngle);
	}
	
	/*public*/findCardByPrototypeName(cardPrototypeName, cardsHolder=null, visitedCards=[]){
		if(!cardsHolder)	cardsHolder=this;
		const self=this;
		return foreach(cardsHolder.cards,(card)=>{
			if(card.prototypeName===cardPrototypeName)
				return card;
			if(!contains(visitedCards,card)){
				visitedCards.push(card);
				return self.findCardByPrototypeName(cardPrototypeName, card, visitedCards);
			}
			return null;
		});
	}
	
	/*public*/findCardByIndex(cardPrototypeIndex, cardsHolder=null, visitedCards=[]){
		if(!cardsHolder)	cardsHolder=this;
		const self=this;
		return foreach(cardsHolder.cards,(card)=>{
			if(card.cardIndex===cardPrototypeIndex)
				return card;
			if(!contains(visitedCards,card)){
				visitedCards.push(card);
				return self.findCardByIndex(cardPrototypeIndex, card, visitedCards);
			}
			return null;
		});
	}
	
	
	/*private*/getCurrentCard(){
		return this.currentCard;
	}
	
	/*public*/setCurrentCard(currentCard){
		this.currentCard=currentCard;
		
		if(!this.currentCard
				&& !this.hasRemainingRegularUnseenCard()
				) {
			let noIssueCard=foreach(this.getRootCardsOnly(true),(c)=>{
				if(c.noIssue){
					return c;
				}
			});
			if(!noIssueCard)	return;
			this.currentCard=noIssueCard;
		}
		
		
		
		if(!this.currentCard) return;
		
		this.currentCard.doEffect();
		this.currentCard.setIsSeen(true);
	}
	
	
	
	/*public*/getNextCard(){
		let unseenCards=this.getRemainingRegularUnseenCard();
		
		if(empty(unseenCards)){
			
//			// DBG
//			lognow("PLUS DE unseenCards !!!",this);

			this.setCurrentCard(null);	
			return null;
		}
		
		let randomlyChosenCard=Math.getRandomInArray(unseenCards);

//		// DBG
//		lognow("randomlyChosenCard !!!",randomlyChosenCard);
		
		return randomlyChosenCard;
	}
	
	/*public*/applyEffect(effect){
		// To recursively go up to the space :
		let issue=this.parent.applyEffect(effect);
		
		// handle success/fail/do nothing effects issues :
		if(issue){
			let issueCard=null;
			if(issue==="success"){
				issueCard=foreach(this.getRootCardsOnly(true),(c)=>{
					if(c.success){
						return c;
					}
				});
			}else{
				issueCard=foreach(this.getRootCardsOnly(true),(c)=>{
					if(c.fail && c.fail===issue){
						return c;
					}
				});
			}
			if(issueCard){
				this.setCurrentCard(issueCard);
			}
		}
		
	}
	
	/*private*/getRootCardsOnly(terminalCardsOnly=false){
		let results=[];
		foreach(this.cards,(c)=>{
			results.push(c);
		},(c)=>{	return (terminalCardsOnly?c.isTerminal:!c.isTerminal);	});
		return results;
	}
	
	

	
	/*private*/getRemainingRegularUnseenCard(){
		let unseenCards=[];
		let self=this;
		foreach(this.getRootCardsOnly(false),(c)=>{	
			unseenCards.push(c)
		},(c)=>{	return self.isRegularUnseenCard(c);	});
		return unseenCards;
	}
	
	/*private*/hasRemainingRegularUnseenCard(){
		let unseenCards=this.getRemainingRegularUnseenCard();
		if(empty(unseenCards))	return false;
		return true;
	}
	
	/*private*/isRegularUnseenCard(c){
		let currentCard=this.getCurrentCard();
		if(!currentCard)	return true;
		return (!c.getIsSeen() && !c.first && !c.fail && !c.success && !c.noIssue);
	}
	
	
	/*public*/getTurnsManager(){
		return this.parent.getTurnsManager();
	}
	
	
	
	/*public*/getCharacterName(){
		return this.parent.name;	
	}
	

	/*public*/getVariable(variableName){
		return this.parent.getVariable(variableName);
	}
	
	
	// =============================================
	
	drawInUI(ctx, camera){


		let currentCard=this.getCurrentCard();
		if(!currentCard)	return;

		// And the current card in last, so that it stays on the top of the pile :
		currentCard.drawInUI(ctx,camera);
		
	}

	draw(ctx,camera,lightsManager){

		let currentCard=this.getCurrentCard();
		if(!currentCard)	return;
		
		// We draw all the non-current cards :
		// (if we are not in a success or fail state)
		if(!currentCard.fail && !currentCard.success){
			let self=this;
			foreach(this.getRootCardsOnly(),(card)=>{
				card.draw(ctx,camera,lightsManager,true);
			},(c)=>{	return c!==currentCard && self.isRegularUnseenCard(c);	});
		}

		// And the current card in last, so that it stays on the top of the pile :
		currentCard.draw(ctx,camera,lightsManager);
		
	}
	
}
