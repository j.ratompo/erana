class RoleCharacter{

	constructor() {

		this.isVisible=true;
		
		
		
		this.currentDeckIndex=0;
		
		this.variables={};
		
	}

	// INIT
	
	init(gameConfig){
		this.gameConfig=gameConfig;


		
		
	}

	
	/*public*/swiping(deltaMouseX,deltaMouseY,pointerX,pointerY){
		this.getCurrentDeck().swiping(deltaMouseX,deltaMouseY,pointerX,pointerY);
	}
	
	/*public*/swipe(directionAngle){
		this.getCurrentDeck().swipe(directionAngle);
	}
	
	
	
	/*private*/getCurrentDeck(){
		return nonull(this.decks[nonull(this.currentDeckIndex,0)], this.decks[0]);
	}
	

	// METHODS
	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	
	getDrawables(){
		return [ this.gameGauges, this.decks ];
	}
	
	
	
	/*public*/setRoleCharacterByPrototypeName(characterPrototypeName){
		this.parent.setRoleCharacterByPrototypeName(characterPrototypeName);
	}
	
		
	/*public*/getTurnsManager(){
		return this.parent.getTurnsManager();
	}
	
	
	/*public*/applyEffect(effect){
		let self=this;
		
		foreach(effect, (e,effectName)=>{
			
			// If we have prototypes names, we assume it's just gauges values affecting effects : 
			if(contains(effectName,"proto_")){
				let g=self.getGaugeByPrototypeName(effectName)
				if(!g)	return "continue";
				g.affectCondition(e);
			}else if(effectName=="action"){
				
				const action=e;
				if(!action)	return "continue";
				let methodName=getAt(action,0,true);
				if(!methodName)	return "continue";
				let args=getAt(action,0);
				self[methodName].apply(self,nonull(args,[]));
			
			}
			
			
		});
		
		// If all of the gauges reaches max => success
		let success=true;
		foreach(this.gameGauges,(g)=>{
			if(!g.isAtMaxCondition())	success=false;
		});
		if(success)	return "success";
		
		// If one of the gauges reaches zero => fail
		let failedPrototypeName=foreach(this.gameGauges,(g)=>{
			if(!g.isAlive())	return g.prototypeName;
		});
		
		if(failedPrototypeName)	return failedPrototypeName;
		
		return null;
	}
	
	/*private*/getGaugeByPrototypeName(prototypeName){
		return foreach(this.gameGauges,(g)=>{
			return g;
		},(g)=>{	return g.prototypeName===prototypeName;	});
	}
	
	
	/*public*/setVariable(variableName, variableValue){
		this.variables[variableName]=variableValue;
		
		// DBG
		lognow("(DEBUG) SET VARIABLE «"+variableName+"» TO : "+variableValue, this.variables);
		
	}
	
	/*public*/setVariables(variablesAssociativeArray){
	
		const self=this;
		foreach(variablesAssociativeArray, (variableValue, variableName)=>{
			self.setVariable(variableName, variableValue);
		});
		
	}
	
	/*public*/getVariable(variableName){
		return this.variables[variableName];
	}

	
	// =============================================
	

	drawInUI(ctx,camera){
		/*DO NOTHING*/
		// (Only to allow its children to draw their UIs)
	}
	
	//	draw(ctx,camera,lightsManager){
//		/*DO NOTHING*/
//	}
	
	
	
	
}
