class Scenario{
	
	constructor(){
		
		// Used for multiplayer only :
		this.protagonist=null;
		
	}
	
	
	init(gameConfig){
		this.gameConfig=gameConfig;
		
		if(!empty(this.roleCharacters))		this.protagonist=this.roleCharacters[0]; // default protagonist is the firstr role character declared.
		
		
				
		let self=this;
		
		
		// CAUTION : We kinda  hijack the swipe methods here :
		window.eranaScreen.controlsManager.doOnSwiping=function(deltaMouseX,deltaMouseY,pointerX,pointerY){
			self.doOnSwiping(deltaMouseX,deltaMouseY,pointerX,pointerY);
		};
		// CAUTION : We kinda  hijack the swipe methods here :
		window.eranaScreen.controlsManager.doOnEndSwipe=function(directionAngle){
			self.doOnEndSwipe(directionAngle);
		};
		
		
	}
	
	
	/*public*/doOnSwiping(deltaMouseX,deltaMouseY,pointerX,pointerY){
		const protagonist=this.getProtagonist();
		protagonist.getCurrentDeck().swiping(deltaMouseX,deltaMouseY,pointerX,pointerY);
		
//		// DBG
//		lognow("DO ON SWIPING !");
	}
	
	/*public*/doOnEndSwipe(directionAngle){
		const protagonist=this.getProtagonist();
		protagonist.getCurrentDeck().swipe(directionAngle);
	}
	
	
	getDrawables(){
		return [ 
//			this.roleCharacters,
// //			// SPECIAL CASE :
			this.protagonist, 
			this.turnsManager ];
	}
	
	getPlaceables(){
		return [
			// WE NEED ALL CHILDREN CARDS TO BE PLACED ! (BUT NOT NCESSARILY DRAWN)
			this.roleCharacters
		];
	}
	
		
	executeProcessing(processing, isFollower){
		if(this.turnsManager)	this.turnsManager.executeProcessing(processing, isFollower);
	}
	
	/*public*/getTurnsManager(){
		return this.turnsManager;
	}
	
	/*public*/getProtagonist(){
		return this.protagonist;
	}
	/*public*/setProtagonist(protagonist){
		this.protagonist=protagonist;
	}
	
	/*private*/getRoleCharacterByPrototypeName(characterPrototypeName){
		return foreach(this.roleCharacters,(c)=>{
			return c;
		},(c)=>{	return c.prototypeName==characterPrototypeName;	});
	}
	
	/*public*/setRoleCharacterByPrototypeName(characterPrototypeName){
		let foundCharacter=this.getRoleCharacterByPrototypeName(characterPrototypeName);

//		// DBG
//		lognow("SCENARIO SEARCHS PROTAGONIST : characterPrototypeName:",characterPrototypeName);
//		// DBG
//		lognow("SCENARIO SETS PROTAGONIST : foundCharacter:",foundCharacter);


		if(!foundCharacter)	return;
		
		
		this.setProtagonist(foundCharacter);
	}	
	

	
	// ======================================================
	
//	draw(ctx,camera,lightsManager){
//		/*DO NOTHING*/
//	}
	
}
