

// TODO : FIXME : Générifier les comportements communs !!

class TurnsManager{

	
	constructor(){
		
		
		this.currentPageNumber=0;
		this.selfClientID=null;

		
		// Turns management :
		this.sharedInfo={};
		this.sharedInfo.allPlayersIDs=[];
		
		this.sharedInfo.currentTurnPlayerID=null;
		
		
		this.leaderOnlyInfo={};
		this.leaderOnlyInfo.currentTurnHavePlayedPlayersIDs=[];
		this.leaderOnlyInfo.currentTurnHaveNotPlayedPlayersIDs=[];
		
		// Constraints managements :
		this.leaderOnlyInfo.choicesMade={};
		
		
	}
	
	
	
	
	executeProcessing(processing, isFollower){
	
		const type=processing.type;
		
		// DBG
		lognow("START (isFollower:"+isFollower+") EXECUTING RECEIVED PROCESSING :",processing);
		
		
		if(type==="setAllPlayers"){
		
			const newClientID=processing.newClientID;
			const participantsClientsIDs=processing.participantsClientsIDs;

		
			this.sharedInfo.allPlayersIDs=participantsClientsIDs;
		
			// DBG
			lognow("setAllPlayers :",processing);
			lognow("this.sharedInfo.allPlayersIDs:",this.sharedInfo.allPlayersIDs);
			
			if(!isFollower){
				this.leaderOnlyInfo.currentTurnHaveNotPlayedPlayersIDs.push(newClientID);
			}
		
		}else if(type==="gameStarted"){

			this.selfClientID=processing.starterClientID;

			// Only leader sends events to manage turns :
			if(!isFollower){
			
				// Leader must include itself in the players list of its game :
				this.sharedInfo.allPlayersIDs.push(this.selfClientID);
				
			
				// DBG
				lognow("(isFollower:"+isFollower+") EXECUTING startNewTurn PROCESSING :",processing);
			
				// Only leader can start a turn and always play first, because at this point, the game only contains this player :
				window.eranaScreen.dispatchProcessing("startNewTurn", {clientIDToPlay:this.selfClientID, broadcastTo:"all"});
			}
		
		
		}else if(type==="playCard"){
		
			
			const choiceIndex=processing.choiceIndex;
		
			
			const nextCardPrototypeIndex=processing.nextCardPrototypeIndex;
//			const nextCard=this.getCardForProtagonistByPrototypeName(nextCardPrototypeName);



			// At this point, the player has played :

			// Case leader only :
			if(!isFollower){
			
				// We mark this player as having played :
				const havePlayedClientID=processing.originatingClientID;
				this.leaderOnlyInfo.currentTurnHavePlayedPlayersIDs.push(havePlayedClientID);
				remove(this.leaderOnlyInfo.currentTurnHaveNotPlayedPlayersIDs, havePlayedClientID);			
				
				// DBG
				lognow("LEADER has received playcard instruction:choiceIndex:",choiceIndex);
			
				if(!empty(this.leaderOnlyInfo.currentTurnHaveNotPlayedPlayersIDs)){
				
					// The leader now chooses the next player to play :
					const randomlyChoosenClientID=Math.getRandomInArray(this.leaderOnlyInfo.currentTurnHaveNotPlayedPlayersIDs);
					
					
					// DBG
					lognow("LEADER sets new player to play in this turn:randomlyChoosenClientID",randomlyChoosenClientID);
					
					
					// We tell the concerned client to play :
					window.eranaScreen.dispatchProcessing("pleasePlay", {clientIDToPlay:randomlyChoosenClientID, broadcastTo:"all"});
			
				}else{// OLD :  if(nextCardPrototypeIndex!=null){
				
					// DBG
					lognow("LEADER tells all to set the new card and start a new turn :nextCardPrototypeIndex",nextCardPrototypeIndex);
					
					// Leader choses anybody among all the players to play as first on this new turn :
					const randomlyChoosenClientID=Math.getRandomInArray(this.sharedInfo.allPlayersIDs);
				
					// If all players have played :
					
					// We set the card according to each client's choice !
					window.eranaScreen.dispatchProcessing("startNewTurn", {clientIDToPlay:randomlyChoosenClientID, 
											targetCardPrototypeIndex:nextCardPrototypeIndex, 
											broadcastTo:"all"});
				}
				
				
				if(choiceIndex!=null){
					this.handleChoiceConstraints(processing, processing.cardPrototypeName, choiceIndex);
				}
				
				
			}
			
			
			
			// Case leader and followers :
			if(choiceIndex!=null){
				this.handleChoiceEffect(processing);
			}
			
			// Local card :
			const nextCardPrototypeName=processing.nextCardPrototypeName;
			if(nextCardPrototypeName){
			
			 	// Local card only :
				// (For the very first turn, the first card is the one with the attribute first==true, so it's OK if we don't set it here)
				
				const c=this.setCurrentCardForProtagonistByPrototypeName(nextCardPrototypeName);
				// DBG
				lognow("SET LOCAL current card on deck:c:",c);
			}



		}else if(type==="pleasePlay"){

			const clientIDToPlay=processing.clientIDToPlay;
			
			this.sharedInfo.currentTurnPlayerID=clientIDToPlay;
		
			
		}else if(type==="startNewTurn"){
		
			const clientIDToPlay=processing.clientIDToPlay;
			let targetCardPrototypeIndex=processing.targetCardPrototypeIndex;
			
			this.sharedInfo.currentTurnPlayerID=clientIDToPlay;
			
			if(!isFollower){
				this.leaderOnlyInfo.currentTurnHavePlayedPlayersIDs=[];
				this.leaderOnlyInfo.currentTurnHaveNotPlayedPlayersIDs=clone(this.sharedInfo.allPlayersIDs);
			}
			
			// DBG
			lognow("START new turn with card index : ",targetCardPrototypeIndex);
			
			
			// We set the new card :
			if(targetCardPrototypeIndex!=null){
				const c=this.setCurrentCardForProtagonistByIndex(targetCardPrototypeIndex);
				// DBG
				lognow("SET current card on deck:c:",c);
			}
			

		}else if(type==="setChoicesAvailabilities"){
		
			const cardPrototypeName=processing.cardPrototypeName;
			const currentCard=this.getCardForProtagonistByPrototypeName(cardPrototypeName);
			if(!currentCard)	return;
			
			currentCard.setChoicesAvailabilities(processing.choicesAvailabilities);
		}
		
		
		// DBG
		lognow("END EXECUTING PROCESSING (isFollower:"+isFollower+"): ",processing);
	}
	
	
	/*private*/handleChoiceConstraints(processing, cardPrototypeName, choiceIndex){
		

		const choiceConstraints=processing.choiceConstraints;
		if(!choiceConstraints) return;

		const currentCard=this.getCardForProtagonistByPrototypeName(cardPrototypeName);
		if(!currentCard)	return;


		// Only leader sends events to manage choices availabilities :
	
		// Leader calculates the new choices availabilities :
			
		if(!this.leaderOnlyInfo.choicesMade[cardPrototypeName])	this.leaderOnlyInfo.choicesMade[cardPrototypeName]=[];
		this.leaderOnlyInfo.choicesMade[cardPrototypeName].push(processing.originatingClientID); // (here, the clientID represents the originating choice client)
		
		// The leader sends choices availabilities updates, if necessary :
		if(choiceConstraints.max && choiceConstraints.max<=this.leaderOnlyInfo.choicesMade[cardPrototypeName].length){
			
			const choicesAvailabilities=[];
			foreach(currentCard.choices,(choice, index)=>{
				if(index===choiceIndex)	choicesAvailabilities.push(false);
				else					choicesAvailabilities.push(!!choice.isAvailable);
			});
			
			
			// DBG
			lognow("LEADER tells all about choices availabilities :choicesAvailabilities",choicesAvailabilities);
			lognow("LEADER tells all about choices availabilities :currentCard",currentCard);
		
			window.eranaScreen.dispatchProcessing("setChoicesAvailabilities",
						{cardPrototypeName:cardPrototypeName, choicesAvailabilities:choicesAvailabilities, broadcastTo:"all"});
		}
	
	
	}
	
	/*private*/handleChoiceEffect(processing){
				
		
		const effect=processing.choiceEffect;
		if(!effect)		return;
	
		const deck=this.getProtagonistDeck();

		const originatingClientID=processing.originatingClientID;
		const selfClientID=window.eranaScreen.getClientID();

		// DBG
		lognow("origin originatingClientID :",originatingClientID);
		lognow("origin selfClientID :",selfClientID);

	
		let executeEffect=false;
		if(effect.isOnlyChooserClientAffected){
			
			// DBG
			lognow("CHOICE EFFECT ! deck:",deck);
			
			if(originatingClientID==selfClientID)	executeEffect=true;
			
		}else{
		
			// DBG
			lognow("ALL CHOICE EFFECT ! deck:",deck);
		
			executeEffect=true;
		
		}
		
		// We apply the effect to the deck :
		if(deck && executeEffect){
			deck.applyEffect(effect);
		}
		
				
	}	
	
	
	/*private*/getProtagonistDeck(){
	
		// We set the next card in the local model with the prototype name :
		const scenario=this.parent;
		
		// We find the role character corresponding to the playing client :
		let roleCharacter=scenario.getProtagonist();
		if(!roleCharacter){
			// TRACE
			lognow("ERROR : Could not find protagonist roleCharacter.");
			return null;
		}

		return roleCharacter.getCurrentDeck();
	}
	
	
	/*private*/getCardForProtagonistByPrototypeName(cardPrototypeName){
		
		const deck=this.getProtagonistDeck();
		const card=deck.findCardByPrototypeName(cardPrototypeName);
		if(!card){
			// TRACE
			lognow("ERROR : Could not find card with prototype name «"+cardPrototypeName+"» in deck :",deck);
			return null;
		}

		return card;
	}
	
	
	
	/*private*/getCardForProtagonistByIndex(cardPrototypeIndex){
		// We set the next card in the local model with the prototype name :
		const scenario=this.parent;
		
		// We find the role character corresponding to the playing client :
		let roleCharacter=scenario.getProtagonist();
		if(!roleCharacter){
			// TRACE
			lognow("ERROR : Could not find protagonist roleCharacter.");
			return null;
		}
		
		const deck=roleCharacter.getCurrentDeck();
		const card=deck.findCardByIndex(cardPrototypeIndex);
		if(!card){
			// TRACE
			lognow("ERROR : Could not find card with index «"+cardPrototypeIndex+"» in deck :",deck);
			return null;
		}

		return card;
	}
	
	
	
	/*private*/setCurrentCardForProtagonistByIndex(cardPrototypeIndex){
		const card=this.getCardForProtagonistByIndex(cardPrototypeIndex);
		if(!card)	return null;
		
		const deck=card.parent;
		deck.setCurrentCard(card);
		
		return card;
	}
	
	/*private*/setCurrentCardForProtagonistByPrototypeName(cardPrototypeName){
		const card=this.getCardForProtagonistByPrototypeName(cardPrototypeName);
		if(!card)	return null;
		
		const deck=card.parent;
		deck.setCurrentCard(card);
		
		return card;
	}

	
	
	/*public*/isMyTurn(){
		return (this.sharedInfo.currentTurnPlayerID==this.selfClientID);
	}
	
	// ======================================================
	
	drawInUI(ctx,camera){

		ctx.save();
		
		// DBG
		ctx.fillText(20,20,"Au tour de : "+nonull(this.sharedInfo.currentTurnPlayerID,"PERSONNE"));
		
		ctx.restore();

	}
	
//	draw(ctx,camera,lightsManager){
//		/*DO NOTHING*/
//	}
	
}
