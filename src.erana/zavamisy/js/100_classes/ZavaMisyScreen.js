

const MOUSE_EVENTS_REFRESH_MILLIS=40;
const WHEEL_EVENT_FACTOR=.01;

class ZavaMisyScreen extends GameScreenXR {

	constructor(mainId,config) {
		super(mainId,config,"zavamisy-saved");

		this.clickableObjects3D=[];
		
		this.fusrodaClientManager=new FusrodaClientManager(this);
		this.oritaMainClientManager=new OritaMainClientManager(this);
	}
	
	// INIT ON START

	initOnStartScreenAtLevelStart(){
		super.initOnStartScreenAtLevelStart();
		
		this.currentContainer=this.getSubclassRootContainer();
		
		this.setupMousePositionRayCasterOnce();
		this.setupMouseButtonsEventsOnce();
		this.setupKeyboardEventsOnce();
		
		this.fusrodaClientManager.initOnStartScreenAtLevelStart();
		this.oritaMainClientManager.initOnStartScreenAtLevelStart();
	}

	
	//=============== MODEL METHODS ===============
	
	/*protected*/getSubclassRootContainer(){
		if(this.currentLevel.isCinematic)	return this.currentLevel.gameCinematic;
		return this.currentLevel.space;
	}
	
	


	// METHODS
	

	// =============== CONTROLLER METHODS ===============
	
	
	
	
	// ACTIONS (for each page)

	
	
	//homePage
	
	startSpace(){

		// if(this.gameConfig.triggerFullscreenOnStart)	openFullscreen(document.body);

	}
	
	continueGame(){	
	

	}
	
	startCredits(){	}


	//gamePage
	resumeGame(){	}
	
	interruptGame(){ 

		// We save the current game :
		this.saveGame();
		this.abandonGame();
		
	}
	
	
	
	abandonGame(){
		super.abandonGame();
//	super.exitGame();
		this.pagesManager.goToPage("homePage");
	}
	
	
	// METHODS
	

	
	// FACULTATIVE PUBLIC METHODS
	
	getDefaultSelectedItem(){
		return this.getSubclassRootContainer().getSelectedItem();
	}
	
	
	// MANDATORY METHODS
	
	
	/*private*/setupMousePositionRayCasterOnce(){
		
		const self=this;
	
		
		this.raycaster=new THREE.Raycaster(); // create once
		this.mouseCoords2D=new THREE.Vector2(); // create once
		
		
		document.body.addEventListener("pointermove",(event)=>{
			
			self.handleRaycasterPointerEvent(event,self.clickableObjects3D).then((args)=>{
					
					const intersecteds=args.intersecteds;
					if(!empty(intersecteds)){
						foreach(intersecteds,(intersected)=>{
							const interactableEntity=intersected.object.interactableEntity;
							if(interactableEntity){
								if(interactableEntity.doOnHover)		interactableEntity.doOnHover(self.getPosition3DFromRayVectorAtRadius(args.coordinates.rayObject, intersected.point), args.camera);
								if(interactableEntity.doOnHover2D)	interactableEntity.doOnHover2D(getPosition2DFromRayVector(intersected));
								
							}
						});
					}
					const notIntersectedObjects=args.notIntersectedObjects;
					if(!empty(notIntersectedObjects)){
						foreach(notIntersectedObjects,(notIntersectedObject)=>{
							const interactableEntity=notIntersectedObject.interactableEntity;
							if(interactableEntity){
								if(interactableEntity.doOnNotHover){
									const object3DPosition=self.getAbsolutePosition(notIntersectedObject);
									interactableEntity.doOnNotHover(self.getPosition3DFromRayVectorAtRadius(args.coordinates.rayObject, object3DPosition), args.camera);
								}
							}
						});
					}

			});
			
			
		});
	
	}
	
	
	/*private*/getPosition3DFromRayVectorAtRadius(rayObject,object3DPosition){
		
		let x=0;
		let y=0;
		
		const originPoint=rayObject.origin;
		
		let radius=Math.getDistance(originPoint, object3DPosition);
		
//	const result=Math.getArcCoordinatesOnSphereFromCartesianVector(rayObject.direction,radius);
//	result.y=-result.y;// Inverted y axis !

		const result=new Vector3();
		rayObject.at(radius, result);

		return result;
	}
	
	
	
	/*private*/handleRaycasterPointerEvent(event, clickableObjects3DParam){
		
		let resultPromise=new Promise((resolve,reject)=>{/*DO NOTHING*/});
		if(empty(clickableObjects3DParam))	return resultPromise;

		const recursiveFlag=false;

		const self=this;
		resultPromise=new Promise((resolve)=>{
			
			let renderer=null;
			let camera=null;
			const isStereo=self.sceneManager.isStereo();
			if(!isStereo){
				camera=self.getCamera().camera3D;
				renderer=self.sceneManager.renderers3D["webgl"];
			}else{
				camera=self.getCamera().camera3Ds["left"];
				renderer=self.sceneManager.renderers3D["left"]["webgl"];
			}
	
			self.raycaster.setFromCamera(self.mouseCoords2D, camera);
	
			const containerElement=renderer.domElement;
			self.mouseCoords2D.x= (event.clientX/containerElement.clientWidth) * 2 - 1;
			self.mouseCoords2D.y=-(event.clientY/containerElement.clientHeight)* 2 + 1;
			

	
			// We need to convert the associative array into a simle array :
			const clickableObject3DArrayLocal=[];
			foreach(clickableObjects3DParam,(clickableObject3D)=>{clickableObject3DArrayLocal.push(clickableObject3D);});
			
			const notIntersectedObjects=copy(clickableObject3DArrayLocal);
			const intersecteds=self.raycaster.intersectObjects(clickableObject3DArrayLocal, recursiveFlag);
			if(!empty(intersecteds)){
//				foreach(intersecteds,intersected=>{
//					// We delegate the mouseCoords2D event treatment for each of the  found intersected objects : they will handle if they do something with it or not
//					resolve({intersected:intersected});
////				NO : Because We delegate the mouseCoords2D event treatment for each of the  found intersected objects : they will handle if they do something with it or not 
//					// return "break";
//				});
				
//		// DBG ONLY :
//		}else{
//			foreach(clickableObject3DArrayLocal,o=>{
//				// DBG
//				o.material.color.set(0x00ff00);
//			});

				foreach(intersecteds,intersected=>{
					remove(notIntersectedObjects,intersected.object);
				});

			}
			

			// We delegate the mouseCoords2D event treatment for each of the  found intersected objects : they will handle if they do something with it or not
			resolve({intersecteds:intersecteds,notIntersectedObjects:notIntersectedObjects,coordinates:{rayObject:self.raycaster.ray},camera:camera});
		});
		
		return resultPromise;
	}
	
	
	
	/*private*/setupMouseButtonsEventsOnce(){
		const self=this;
		
		document.body.addEventListener("mousedown",(event)=>{
			const mouseButton=event.button;
			
//			// DBG
//			lognow("!!! mousedown event:["+mouseButton+"]",event);
		
			self.handleRaycasterPointerEvent(event,self.clickableObjects3D).then((args)=>{

					const intersecteds=args.intersecteds;
					if(!empty(intersecteds)){
						const intersected=intersecteds[0]; // We only take the first found one !
						const interactableEntity=intersected.object.interactableEntity;
						if(!interactableEntity)	return;
						if(interactableEntity.doOnClick)		interactableEntity.doOnClick(self.getPosition3DFromRayVectorAtRadius(args.coordinates.rayObject, intersected.point), args.camera);
						if(interactableEntity.doOnClick2D)	interactableEntity.doOnClick2D(getPosition2DFromRayVector(intersected));
					}
					
					const notIntersectedObjects=args.notIntersectedObjects;
					if(!empty(notIntersectedObjects)){
						foreach(notIntersectedObjects,(notIntersectedObject)=>{
							const interactableEntity=notIntersectedObject.interactableEntity;
							if(interactableEntity){
								if(interactableEntity.doOnNotClick){
									const object3DPosition=self.getAbsolutePosition(notIntersectedObject);
									interactableEntity.doOnNotClick(self.getPosition3DFromRayVectorAtRadius(args.coordinates.rayObject, object3DPosition), args.camera);
								}
							}
						});
					}
			
			});
			
			
		
		});
		
		document.body.addEventListener("mouseup",(event)=>{
			const mouseButton=event.button;
			
//		// DBG
//		lognow("!!! mouseup event:["+mouseButton+"]",event);


			self.handleRaycasterPointerEvent(event,self.clickableObjects3D).then((args)=>{
			
				const intersecteds=args.intersecteds;
				if(!empty(intersecteds)){
					const intersected=intersecteds[0]; // We only take the first found one !
					const interactableEntity=intersected.object.interactableEntity;
					if(!interactableEntity)	return;
					if(interactableEntity.doOnStopClick)		interactableEntity.doOnStopClick(self.getPosition3DFromRayVectorAtRadius(args.coordinates.rayObject, intersected.point), args.camera);
					if(interactableEntity.doOnStopClick2D)	interactableEntity.doOnStopClick2D(getPosition2DFromRayVector(intersected));
				}
			});
			
			
		});
		
		
		document.body.addEventListener("wheel",(event)=>{
			const wheelAmount=event.deltaY*WHEEL_EVENT_FACTOR;
//			// DBG
//			lognow("!!! wheel event:["+wheelAmount+"]",event);

			
			self.handleRaycasterPointerEvent(event,self.clickableObjects3D).then((args)=>{

					const intersecteds=args.intersecteds;
					if(!empty(intersecteds)){
						const intersected=intersecteds[0]; // We only take the first found one !
						const interactableEntity=intersected.object.interactableEntity;
						if(!interactableEntity)	return;
						
						if(interactableEntity.doOnWheel)	interactableEntity.doOnWheel(wheelAmount, args.camera);
					}
					
					const notIntersectedObjects=args.notIntersectedObjects;
					if(!empty(notIntersectedObjects)){
						foreach(notIntersectedObjects,(notIntersectedObject)=>{
							const interactableEntity=notIntersectedObject.interactableEntity;
							if(interactableEntity){
								if(interactableEntity.doOnNotWheel)		interactableEntity.doOnNotWheel(wheelAmount, args.camera);
							}
						});
					}
			
			});

			
			
			
			
		});
		
		// To remove the local contextual menu :
		document.body.addEventListener("contextmenu",(event)=>{event.preventDefault();});
	
	}
	

	
	/*private*/setupKeyboardEventsOnce(){
		const self=this;

		/*DO NOTHING*/
	}
	
	
	
	// DELEGATION METHODS :
	
	// Fusroda (VNC) server connection :
	/*DELEGATED*/
	/*public*/connectToVNCServerOnce(url, port){
		this.fusrodaClientManager.connectToServer(url, port);
	}
	
	/*DELEGATED*/
	/*public*/setVNCEndPoint(key, vncEndPoint){
		this.fusrodaClientManager.setVNCEndPoint(key, vncEndPoint);
	}
	/*DELEGATED*/
	/*public*/setVNCEndPointObject(key, vncEndPointObject){
		this.fusrodaClientManager.setVNCEndPointObject(key, vncEndPointObject);
	}
	
	// Orita (Stream / IO) server connection :
	/*DELEGATED*/
	/*public*/connectToIOServerOnce(url, port){
		this.oritaMainClientManager.connectToServer(url, port);
	}
	
	/*DELEGATED*/
	/*public*/setStreamEndPoint(microClientId, streamEndPoint){
		this.oritaMainClientManager.setStreamEndPoint(microClientId, streamEndPoint);
	}
	/*DELEGATED*/
	/*public*/setStreamEndPointObject(microClientId, streamEndPointObject){
		this.oritaMainClientManager.setStreamEndPointObject(microClientId, streamEndPointObject);
	}
	
	/*public*/setIOEndPoint(microClientId, ioType, ioEndPoint){
		this.oritaMainClientManager.setIOEndPoint(microClientId, ioType, ioEndPoint);
	}
	/*DELEGATED*/
	/*public*/setIOEndPointObject(microClientId, ioType, ioEndPointObject){
		this.oritaMainClientManager.setIOEndPointObject(microClientId, ioType, ioEndPointObject);
	}
	
	
	
	/*public*/addClickableObject3D(object3D){
		this.clickableObjects3D.push(object3D);
	}
	
	
	/*private*/getAbsolutePosition(object3D){
		const interactableEntity=object3D.interactableEntity;

		// CAUTION : MUST BE THE ABSOLUTE POSITION !
		const object3DPosition=new THREE.Vector3();
		if(interactableEntity && interactableEntity.getRefObject3D && interactableEntity.getRefObject3D())
					interactableEntity.getRefObject3D().getWorldPosition(object3DPosition);
		else	object3D.getWorldPosition(object3DPosition);
		
		return object3DPosition;
	}
	
}


