
class LateralMoveButton3D extends GameButton3D{
	
	constructor(parent){
		super(parent);
		
		// TODO : FIXME : DUPLICATED CODE !
		this.hasPendingAction=false;
		this.color=0x00ffff;
		
		this.iconSrc="move_32.png";
	}
	
	// TODO : FIXME : DUPLICATED CODE !
	/*OVERRIDES*//*public*/doOnClick(){
		this.hasPendingAction=!this.hasPendingAction;
		this.updateColor();

	}

	/*public*/doOnNotClick(coordinates,camera){

		// TODO : FIXME : DUPLICATED CODE !
		if(!this.hasPendingAction)	return;
		this.hasPendingAction=false;
		this.updateColor();
		
		
		const buttonOffsets=this.parent.getButtonPositionOffsetsAndCommandedObjectSize(this);
		
		
		let x=coordinates.x-buttonOffsets.x;
		let y=coordinates.y-buttonOffsets.y+(this.parent.toolbarLayout=="top"?(-buttonOffsets.h*.5):0);
		let z=coordinates.z;
		
		
		const refObject3D=this.getRefObject3D();
		refObject3D.position.set(x,y,z);
		refObject3D.lookAt(camera.position);
	}


	// TODO : FIXME : DUPLICATED CODE !
	/*public*/getRefObject3D(){
		return this.parent.parentObject3D;
	}
	
	// TODO : FIXME : DUPLICATED CODE !
	/*private*/updateColor(){
		this.color=(!this.hasPendingAction?0xffffff:0xffff00);
		this.object3D.material.color.setHex(this.color); // there is also setHSV and setRGB
	}
	
}