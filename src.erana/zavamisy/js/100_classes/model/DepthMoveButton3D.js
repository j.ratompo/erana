
class DepthMoveButton3D extends GameButton3D{
	
	constructor(parent){
		super(parent);
		
		
		// TODO : FIXME : DUPLICATED CODE !
		this.hasPendingAction=false;
		this.color=0x00ff00;

		this.iconSrc="depth_32.png";
		
	}
	
	// TODO : FIXME : DUPLICATED CODE !
	/*OVERRIDES*//*public*/doOnClick(){
		this.hasPendingAction=!this.hasPendingAction;
		this.updateColor();
	}
	
	/*public*/doOnNotWheel(wheelAmount, camera){

		if(!this.hasPendingAction)	return;
		
		
		const refObject3D=this.getRefObject3D();
		
		
		// CAUTION : MUST BE THE ABSOLUTE POSITION !
		const refPosition=new THREE.Vector3();
		refObject3D.getWorldPosition(refPosition);
		
		
		const originPoint=camera.position;

		const angles=calculateAngleRadians3D(originPoint, refPosition);
		const coordinates=calculateLinearlyMovedPoint3DPolar(refPosition, angles, wheelAmount);
		

//  DOES NOT WORK :
//	const coordinates=new Vector3();
//	const newRadius=Math.getDistance(originPoint, refPosition)+wheelAmount;
//	const rayObject=new THREE.Ray(originPoint, refPosition.normalize());
//	const coordinates=new Vector3();
//	rayObject.at(newRadius, coordinates);
		
		
//	// DBG
//	lognow("BUTTON WHEEL !",coordinates);

		// (Here, no need to care about the button offsets,
		// since we want to place the object group itself directly:)
		let x=coordinates.x;
		let y=coordinates.y;
		let z=coordinates.z;
		
		
		refObject3D.position.set(x,y,z);
		refObject3D.lookAt(camera.position);
		
	}
	

	/*public*/doOnNotClick(coordinates,camera){
		// TODO : FIXME : DUPLICATED CODE !
		if(!this.hasPendingAction)	return;
		this.hasPendingAction=false;
		this.updateColor();
	}

	// TODO : FIXME : DUPLICATED CODE !
	/*public*/getRefObject3D(){
		return this.parent.parentObject3D;
	}
	
	// TODO : FIXME : DUPLICATED CODE !
	/*private*/updateColor(){
		this.color=(!this.hasPendingAction?0xffffff:0xffff00);
		this.object3D.material.color.setHex(this.color); // there is also setHSV and setRGB
	}
	
	
}