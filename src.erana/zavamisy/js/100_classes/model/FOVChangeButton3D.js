
class FOVChangeButton3D extends GameButton3D{
	
	constructor(parent,orientation="vertical"){
		super(parent);
		
		
		// TODO : FIXME : DUPLICATED CODE !
		this.hasPendingAction=false;
		this.color=0x0000ff;

		this.iconSrc=null;
		
		this.orientation=orientation;
		
		this.angularFactorStep=.02;
		
	}
	
	// TODO : FIXME : DUPLICATED CODE !
	/*OVERRIDES*//*public*/doOnClick(){
		this.hasPendingAction=!this.hasPendingAction;
		this.updateColor();
	}
	
	/*public*/doOnNotWheel(wheelAmount, camera){

		if(!this.hasPendingAction)	return;
		
		const azimutalFactor=window.eranaScreen.setAzimutalAnglesFactor(this.orientation,this.angularFactorStep*(wheelAmount<0?-1:1));
		
		this.updateLabel(azimutalFactor);
	}
	
	
	/*public*/updateLabel(azimutalFactor){
		
		this.ctx.save();
		this.ctx.rect(0,0,32,32);
		this.ctx.fillStyle="#FFFFFF"; // (default color)
		this.ctx.fill();
		this.ctx.font="12px helvetica";
		this.ctx.fillStyle="#000000"; // (default color)
		this.ctx.fillText(azimutalFactor.toFixed(2),5,20);
		this.ctx.fillText((this.orientation=="horizontal"?"--":"|"),10,30);
		this.ctx.restore();
		
		super.updateTexture();
		
	}
	
	
	
	

	/*public*/doOnNotClick(coordinates,camera){
		// TODO : FIXME : DUPLICATED CODE !
		if(!this.hasPendingAction)	return;
		this.hasPendingAction=false;
		this.updateColor();
	}

	// TODO : FIXME : DUPLICATED CODE !
	/*public*/getRefObject3D(){
		return this.parent.parentObject3D;
	}
	
	// TODO : FIXME : DUPLICATED CODE !
	/*private*/updateColor(){
		this.color=(!this.hasPendingAction?0xffffff:0xffff00);
		this.object3D.material.color.setHex(this.color); // there is also setHSV and setRGB
	}
	
	
}