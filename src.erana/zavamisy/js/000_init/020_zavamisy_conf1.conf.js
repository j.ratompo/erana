// DEPENDENCY:init.js


window.lang = "fr";


// ===============================================================================
// CONFIG
// ===============================================================================


const PROTO_PREFIX="proto_";


const GAME_CONFIG = {
	
	gameURLs:{
//	"support":"https://paypal.me/pools/c/8szm55N1iC", 
		"other":"https://www.kai-engel.com", "email":"mailto:info@alqemia.com"},
	basePath:"zavamisy.medias",
	
	spinnerImage:"aotraLogo_128.gif",
	
	// view config :
	X_OFFSET:0,
	Y_OFFSET:0,
	// TODO:FIXME:ZOOM DOES NOT WORK AT ALL :
//	zoom:1, // Zoom is the most outer function. Camera and Offsets coordinates
			// obeys it!

	// resolution :
//	chosenResolutionIndex:1,
//	getResolution:function(){
//		// This syntax is quite cool :
//		return [{w:getWindowSize("width"),h:getWindowSize("height")},{w:400,h:600},][this.chosenResolutionIndex];
//	},
//	
//	getViewCenterOffset:function(ignoreOffset=false){
//		var result={
//				x:Math.floor(this.getResolution().w*.5) + (ignoreOffset?0:this.X_OFFSET),
//				y:Math.floor(this.getResolution().h*.5) + (ignoreOffset?0:this.Y_OFFSET)
//		};
//		return result;
//	},
	
	
	// Example URL : https://74.58.12.27/erana/?debug=true&src=0&stereo=1
	
	
	
	// lighting config :
//	lighting:{ dark:false, darkRaycast:false, bump:false },
	
	// scroll config :
//	scale:0.025,// = 1/40 (40 px = 1m)
//	scroll:null,

	projection:"3D webgl css3",	// Threejs : if contains "html" => CSS3 renderer, else WebGL renderer
//	projection:"3D css3",	// Threejs : if contains "html" => CSS3 renderer, else WebGL renderer
//	projection:"3D webgl",	// Threejs : if contains "html" => CSS3 renderer, else WebGL renderer
	configXR:{
		isStereo:(getURLParameter("stereo")==="1"?true:false),
		isAR:true,		// WebXR
		hasAnchors:true,
//	ARFeed:{sources:{default:"webcam+1",left:"webcam+1",right:"webcam+1"}},
		ARFeed:{sources:{default:"webcam+"+(nonull(getURLParameter("default"),0)),left:"webcam+"+(nonull(getURLParameter("left"),1)),right:"webcam+"+(nonull(getURLParameter("right"),1))}},
		forceFallbackAR:true,

		// TODO : Develop... : isPhysics:true, // (lib : physijs)
		// TODO : FIXME : AR CONTROLS DOES NOT WORK ON MOBILE DEVICES ! :
		forceFallbackControls:true,
		
//		DOES NOT WORK :	controls3D:"orbital",// "orbital"(non-stereo only), "hands"
//		TOO SLOW : controls3D:"hands",
// 		TODO : FIXME : DEVELOP... (NOT WORKING FOR NOW)
//		controls3D:"colorFingers",// "orbital"(non-stereo only), "hands"

//		videoTagZoom:1.48,
	},
	
	triggerFullscreenOnStart:!IS_DEBUG,
	
	// camera config :
//	cameraFixed:null,
		
	// controls config :
	controlsMode: "hands",// "hands"/"stare"
	
	
	
	
	// selection config :
	selectionCanChange:false,
	selectionMaxSize:1,
	hideSelectionMark:true,
	

	
	
//	customKeyboardControls: {"ArrowUp":"accelerate","ArrowDown":"decelerate","Space":"secondaryAction"},
//	customKeyboardControls: {"KeyR":"accelerate","KeyF":"decelerate",
//					 "KeyW":"move","KeyS":"stop",
//					 "KeyQ":"rotateLeft","KeyE":"rotateRight",
//					 "KeyA":"strifeLeft","KeyD":"strifeRight",
//					 "Space":"secondaryAction",
//					 
//					 // DBG
//					 "ArrowLeft":"rotateLeftDEBUG","ArrowRight":"rotateRightDEBUG",
//					 // DBG
//					 "ArrowUp":"moveUpDEBUG","ArrowDown":"moveDownDEBUG",
//					 
//					 },

	
	popupConfig:{
		backgroundColor1:"#222222",
		backgroundColor2:"#111111",
		textColor:"#FFFFFF",
	},
	
	
	// images config :
//	backgroundClipSize:{w:600,h:600},
	
	// controller config :
	// ALTERNATIVE PARAMETER : refreshRateMillis:33, // :(1000/33)=30 FPS max

//	fps:40, // CAUTION : changes speeds de facto !
	// DBG
	fps:20,
	
	// ui
	dialogs:{	
		autoScroll:"letters", 
		speedFactor:8, /*UNUSEFUL (Since there will always be a «linger» time if you set a speedFactor>1 !) : minimumTimeMillis:1000*/
		backgroundColor:"#D1B69C",
	},
	
	// model config :
	allowParentLinkForChildren:true,
	
	
	gameCentralServer:{
		url:"wss://192.168.0.111",
		port:25000,
		modelStrategy:null,
	},
	
	
	
};





// ===============================================================================
// FLOW
// ===============================================================================



FLOW_CONFIG = {
	
	
	splashPage_erana:{

		delayMillis:(IS_DEBUG?200:3000),
		
		image: "splashs/aotraLogo_300.png",
		background: "#FFFFFF",
		text:{
			font:"helvetica",
			position:{yBottom:20},
			message:i18n("propulséParEranaBrSpanStyleFontSize7emUnMoteurOuv")
		},
		goTo:"splashPage_alqemia"

	},
	
	splashPage_alqemia:{

		delayMillis:(IS_DEBUG?200:3000),
		
		image: "splashs/alqemia_400.png",
		background: "#000000",
		text:{
			color:"#000000",
			font:"helvetica",
			position:{yTop:70},
			message:i18n("leStudioAlqemiaPrésente"),
		},
		
//	goTo:(IS_DEMO?"alphaPage":(IS_DEBUG?"homePage":"audioPage")),

		// FOR NOW ONLY (alpha version) :
//		goTo:(IS_DEBUG?"homePage":"alphaPage"),
		goTo:("homePage"),

		
//		goTo:"audioPage"
//		goTo:"alphaPage",

	},


	
	
	homePage:{
		
//	image: "splashs/lagrandeligne-title.png",

		background: "#888888",
		text:{
			font:"helvetica",
			position:{yTop:0},
			message:i18n({"fr":"<h1 class='gameTitle'>The Open</h1><br /><small>Open Plateform for Extended perceptioN</small>",
										"en":"<h1 class='gameTitle'>The Open</h1><br /><small>Open Plateforme Englobante de perceptioN</small>"})
		},

		// startLevelName:"proto_level_0",

		// WORKAROUND : To bypass the «user must interact with the page first before autoplay» user annoyance protection politics :

//		music:{
//			pauseBetweenMillis:4000,
//			volumePercent:50,
//			tracks: ["musics/Mercy.mp3","musics/Evermore.mp3"
//				],
//		},
	
		
		menus:{
			mainMenu:{
				items:[
					{
						id:"startButton",
						label:i18n("commencer"),
						actionAfter:"startSpace",
						goTo:"gamePage",
					},
					{
						label:i18n("continuer"),
						actionAfter:"continueGame",
						goTo:"gamePage",
						activeOnlyIf:"isPreviousGameDetected()",
						restoreLevel:true,
					}, 
					{
						label:i18n("crédits"),
						actionAfter:"startCredits",
						goTo:"creditsPage"
					},
//					{
//						label:i18n("soutenir"),
//						goTo:"supportPage",
//					},
					{
						label:i18n("quitter"),
						actionAfter:"exitGame",
						//goTo:"homePage",
					},
				]
			},
		},
	},

	
	
	gamePage:{

		startLevelName:"proto_level1",
		visibleControls:true,

		//music:{
		//	pauseBetweenMillis:4000,
		//	volumePercent:50,
		//	tracks: ["musics/Brooks.mp3","musics/Denouement.mp3","musics/Snowmen.mp3"],
		//},
		
		menus:{
			mainMenu:{
				config:{
					label:i18n("menu"),
					actionAfter:"showMenu",
					position:"top-right" // default position is "top-left"
				},
				items:[
					{
						label:i18n("reprendre"),
						actionAfter:"resumeGame",
						resume:true
					},
					{
						label:i18n("sauverEtRevenirÀLAccueil"),
						actionAfter:"interruptGame",
						goTo:"homePage"
					},
					{
						label:i18n("retourALAccueil"),
						actionAfter:"abandonGame",
						goTo:"homePage"
					}
				]
			},
		},
	},


	
	
	
	creditsPage:{
		
		menus:{
			mainMenu:{
				items:[
					
					{ label:i18n("programmationJeremieR"), inactive:true,},
					{ label:i18n("graphiquesJeremieR"), inactive:true,},
					{ label:i18n("écritureJeremieR"), inactive:true,},
					{ label:i18n("sonsJeremieR"), inactive:true,},
					{ label:i18n("communicationJeremieR"), inactive:true,},
					{ label:i18n("musiqueKaiEngel"), actionAfter:"goToURLOther" },
					{ label:i18n("remerciementsSupportMoralDominiqueB"), inactive:true,},
					{ label:i18n("remerciementsMusiqueÉcoutéeKaiEngelLindseyStirlingOh"), inactive:true,},
					{ label:i18n("remerciementsPodcastsÉcoutésTalesOfPiFranceInterFib"), inactive:true,},
					{ label:i18n("retourALAccueil"), goTo:"homePage"},
										
				]
			},
		},
	},
	
	
	

	
//	supportPage:{
//
//		startLevelName:"proto_level_credits",
//		
//		menus:{
//			mainMenu:{
//				items:[
//					
//			
//					{
//						label:i18n({
//							"fr":"<strong>Comment nous soutenir ?</strong>",
//							"en":"<strong>How to support the game ?</strong>",
//						}),
//						inactive:true,
//					},
//
//					{
//						label:i18n({
//							"fr":"Faites-nous parvenir vos commentaires / demandez à être informé des prochains développements !",
//							"en":"Let us know your comments / ask for updates from the game development !",
//						}),
//						inactive:true,
//					},
//
//					{
//						label:i18n({
//							"fr":"Envoyer un courriel à l´équipe : info@alqemia.com",
//							"en":"Send an email to the team : info@alqemia.com",
//						}),
//						actionAfter:"goToURLMailTo"
//
//					},
//					
////					{
////						label:i18n({
////							"fr":"Faites-nous un petit don qui fera une grande différence ! 😉",
////							"en":"Give a us a little tip that will make a big difference ! 😉",
////						}),
////						actionAfter:"goToURLSupport"
////					},
//					
//					{
//						label:i18n("retourALAccueil"),
//						goTo:"homePage"
//					},
//					
//					
//					
//				]
//			},
//		},
//	},
	
	
	
	
	
	

};



