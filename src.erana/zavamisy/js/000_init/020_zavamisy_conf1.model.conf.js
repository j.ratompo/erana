// DEPENDENCY:init.js




// ===============================================================================
// MODEL
// ===============================================================================


// Prototype can define as a well a group of things as particular
// individuals:only by not setting a random variance on them.
// ***
// CAUTION : The attribute «usesSuper» must always be the first attribute ! (All attribute put before will be ignored).
// ***


PROTOTYPES_CONFIG = {

	allPrototypes:{
		
		
		// ------------------ Levels objects ------------------
		
		GameLevel:{
			
			proto_level1:{

				selectedItem:"getDefaultSelectedItem()",
				
				Space:"proto_space1",
				
			},
			
		},
		
		
		
		// ------------------ Game objects ------------------
		
		
		Space:{
			
			proto_space1:{
				

				containerConfig:"starter", // To indicate where the player starts in the tree objects hierarchy
				
				
//			backgroundColor:"#FEE7A2",

//			backgroundColor:"#FFFFFF",
				
				backgroundZIndex:1,
				backgroundParallax:1,
				backgroundOffsetY:0,
				backgrounds:[
 					{images:["background1.png","background2.png"]},
				],


				
//				backgroundZIndex:1,
//				backgroundParallax:10,
//				backgroundOffsetY:0,
//				backgrounds:[
//// {images:["backgroundSky1.png","backgroundSky2.png","backgroundSky3.png","backgroundSky4.png"],parallaxAdd:100},
//// //
//// {images:["backgroundFore1.png","backgroundFore2.png","backgroundFore3.png","backgroundFore4.png"],parallaxAdd:10},
//// {images:["background1.png","background2.png","background3.png","background4.png"]},
//
//// {images:["backgroundSky1.png"],parallaxAdd:100},
//// {images:["background1.png"],parallaxAdd:10},
//// {images:["backgroundFore1.png"]},
//
//					{images:["backgroundSky1_v1.png"],parallaxAdd:400},
//					{images:["background1_v1.png"],parallaxAdd:200},
//					{images:["backgroundFore1_v1.png"],parallaxAdd:20},
//					{images:[{normal:"foregroundGround1.png"}],offsetY:-230,clipSize:{w:1600,h:100}},
//
//				],
				
				
//				foregroundParallax:1,
//// foregroundOffsetY:10,
//				foregrounds:[
//// {images:["foreground1.png"],offsetY:-20},
//					{images:[ {normal:"foreground1_v1.png",dark:"foreground1.dark.png"} ],offsetY:-20},
//				],
				

				// DBG
//				GameObject3D:{
//					cardinality:{
//						value:"staticGroups",
//						prototypesInstantiation:{
//							staticInstantiations:[
////								{name:"proto_pingoo",spawningZone:{x:0,y:0,z:0,a:0,b:0,g:0},preinstanciate:true,subClass:"Ship",},
//								{name:"proto_mug",spawningZone:{x:0,y:15,z:-20,a:0,b:0,g:0},preinstanciate:true},
//							
//								{name:"proto_mug",spawningZone:{x:10,y:5,z:20,a:0,b:0,g:0},preinstanciate:true},
//								{name:"proto_mug",spawningZone:{x:10,y:5,z:20,a:0,b:0,g:0},preinstanciate:true},
//								{name:"proto_mug",spawningZone:{x:5,y:4,z:-20,a:0,b:0,g:0},preinstanciate:true},
//								{name:"proto_mug",spawningZone:{x:-5,y:1,z:-40,a:0,b:0,g:0},preinstanciate:true},
//								{name:"proto_mug",spawningZone:{x:5,y:-2,z:-20,a:0,b:0,g:0},preinstanciate:true},
//
//							]
//							
//						},
//					},
//				},


//				GameObject3D:{
//					cardinality:{
//						value:10,
////						value:"staticGroups",
//							prototypesInstantiation:{
//								staticInstantiations:[
//									{name:"proto_mug",spawningZone:{x:0,y:0,z:0,a:0,b:0,g:0,w:50,h:50,d:50},preinstanciate:true},
//	//							{name:"proto_mug",spawningZone:{x:0,y:0,z:-20,a:0,b:0,g:0},customAttributes:{anchorId:"alqemia_1"},preinstanciate:true},
//	//							{name:"proto_mug",spawningZone:{x:0,y:0,z:-20,a:0,b:0,g:0},preinstanciate:true},
//								]
//						},
//					},
//				},



//				GameElement3DCSS:{
//					cardinality:{
//						value:"staticGroups",
//						prototypesInstantiation:{
//							staticInstantiations:[
////								{name:"proto_divTest",spawningZone:{x:2,y:1,z:-15,a:0.1,b:-0.2,g:0},preinstanciate:true},
////								{name:"proto_divTest2",spawningZone:{x:2,y:1,z:-20,a:0.1,b:-0.2,g:0},preinstanciate:true},
////									{name:"proto_iframeTest1",spawningZone:{x:2,y:1,z:-80,a:0.1,b:-0.2,g:0},preinstanciate:true,subClass:"GameIFrame3D"},
//
//									{name:"proto_iframeVNC",spawningZone:{x:2,y:1,z:-80,a:0.1,b:-0.2,g:0},preinstanciate:true,subClass:"GameVNCFrame3D"},
//									
////								 ANCHORED:								
////								{name:"proto_divTest",customAttributes:{anchorId:"alqemia_1",size:{w:5,h:5}},preinstanciate:true},
//
////					IF IS_MODE_GROUPS_AUTO:			{name:"proto_divTest",spawningZone:{x:0,y:0,z:0,a:0,b:0,g:-Math.PSI},customAttributes:{size:{w:4,h:4},anchorId:"alqemia_1"},preinstanciate:true},
////							{name:"proto_divTest",spawningZone:{x:0,y:0,z:-50,a:0,b:0,g:0},customAttributes:{size:{w:20,h:20}},preinstanciate:true},
////							{name:"proto_divTest",spawningZone:{x:0,y:0,z:10,a:0.1,b:0,g:0},preinstanciate:true},
//							]
//						},
//					},
//				},



// PROD :
/*
				GameVNCFrame3D:{
					cardinality:{
						value:"staticGroups",
						prototypesInstantiation:{
							staticInstantiations:[
//									{name:"proto_iframeVNC",spawningZone:{x:2,y:1,z:-80,a:0.1,b:-0.2,g:0},preinstanciate:true},
									{name:"proto_iframeVNC",spawningZone:{x:0,y:0,z:-80,a:0,b:0,g:0},preinstanciate:true},
							]
						},
					},
				},
*/

				OritaIOFrame3D:{
					cardinality:{
						value:"staticGroups",
						prototypesInstantiation:{
							staticInstantiations:[
									{name:"proto_iframeOrita",spawningZone:{x:0,y:0,z:-80,a:0,b:0,g:0},preinstanciate:true},
							]
						},
					},
				},
				
				

//				GameDrawingFrame3D:{
//					cardinality:{
//						value:"staticGroups",
//						prototypesInstantiation:{
//							staticInstantiations:[
//// TOO SLOW :			{name:"proto_iframeDrawing",spawningZone:{x:0,y:0,z:-80,a:0,b:0,g:0},customAttributes:{anchorId:"colors_1"},preinstanciate:true},
//									{name:"proto_iframeDrawing",spawningZone:{x:0,y:0,z:-100,a:0,b:0,g:0},preinstanciate:true},
//							]
//						},
//					},
//				},

				
//				GameARStickerObject2D:{
//						cardinality:{
//						value:"staticGroups",
//						prototypesInstantiation:{
//							staticInstantiations:[
//// works:					{name:"proto_stickerTest",spawningZone:{x:0,y:0,z:-30,a:0,b:0,g:0},customAttributes:{anchorId:"alqemia_1",size:{w:5,h:5}},preinstanciate:true},
//
//									{name:"proto_stickerTest",spawningZone:{x:0,y:0,z:-30,a:0,b:0,g:0},customAttributes:{anchorId:"alqemia_2",size:{w:5,h:5}},preinstanciate:true},
////								{name:"proto_stickerTest",spawningZone:{x:0,y:0,z:-30,a:0,b:0,g:0},customAttributes:{anchorId:"alqemia_3",size:{w:5,h:5}},preinstanciate:true},
//
//							]
//						},
//					},
//					
//				},
				

			
//				GameARMultiMarkersAnchor:{
//					cardinality:{
//						value:"staticGroups",
//						prototypesInstantiation:{
//							staticInstantiations:[
//								{name:"proto_quadriAnchor1",preinstanciate:true,customAttributes:{id:"alqemia_2"}},
//							]
//						},
//					},
//				},
//
//				
//				GameARSingleMarkerAnchor:{
//					cardinality:{
//						value:"staticGroups",
//						prototypesInstantiation:{
//							staticInstantiations:[
//									{name:"proto_ARPattern",preinstanciate:true,customAttributes:{id:"alqemia_1"}},
////									{name:"proto_emptyARPattern",preinstanciate:true,customAttributes:{id:"alqemia_3"}},
//							]
//						},
//					},
//				},
				
				// TOO SLOW :
//				GameColorsMultiPointsAnchor:{
//					cardinality:{
//						value:"staticGroups",
//						prototypesInstantiation:{
//							staticInstantiations:[
//									{name:"proto_colorsPattern",preinstanciate:true,customAttributes:{id:"colors_1"}},
//							]
//						},
//					},
//				},
				

				
				
				
				
			},

			
		},
		
		
		
		GameObject3D:{

			proto_mug:{
				
//				overridingPosition:{
//					x:0,y:0,z:10,
//				},
//
				size:{w:200,h:200},
				
				modelConfig:{
					
					stl:"tasse1.stl",
					color:"#FF8800",
					
				},
								
			},
			
		
		},
		
		
		GameElement3DCSS:{
				
			proto_divTest:{
			
				size:{w:10,h:10},
			
				modelConfig:{
					
					htmlElementType:"div",
					innerHTML:"TESTTESTTESTTESTTESTTESTTESTTEST<br/>TESTTESTTESTTESTTESTTESTTESTTEST<br/>TESTTESTTESTTESTTESTTESTTESTTEST<br/>TESTTESTTESTTESTTESTTESTTESTTEST<br/>TESTTESTTESTTESTTESTTESTTESTTEST<br/>",
	//			htmlElementType:"img",
	//			src:"test1.png",
					
				},
			},
			
			
			proto_divTest2:{
			
				size:{w:10,h:10},
			
				isMirrored:true,
				
				modelConfig:{
					
					htmlElementType:"div",
					innerHTML:"<div style='background-color:red;color:white'>TESTTESTTEST<br />TESTTESTTEST<br />TESTTESTTEST<br />TESTTESTTEST<br />TESTTESTTEST<br />TESTTESTTEST<br /></div>",
//					htmlElementType:"img",
//					src:"test1.png",
					
				},
			},
			
			
		},
		
		
//		GameIFrame3D:{
//			proto_iframeTest1:{
//				
//				size:{w:20,h:20},
//
//				isMirrored:true,
//				isRefreshedContinuously:false,
//				
//				url:"ws://192.168.0.111",
//				
//			},
//		},


		OritaIOFrame3D:{
			
			proto_iframeOrita:{
				
				rooted:true,
				
				targetMicroClientId:"cam1",
					
				size:{w:50,h:50},
	
				url:"wss://192.168.0.111",
				port:25000,
				
			},
		},
		
		

		GameVNCFrame3D:{
			proto_iframeVNC:{
					
				rooted:true,
					
				size:{w:50,h:50},
	
//			url:"wss://192.168.0.111",
				// WORKS : 
				url:"ws://192.168.0.111",
//			url:"https://192.168.0.111",
				port:6080,
				
			},
		},
		
		
		GameDrawingFrame3D:{
			proto_iframeDrawing:{
				
				rooted:true,
				
				size:{w:50,h:50},
				canvasSize:{w:300,h:300},

				opacity:0.4,
				thickness:1.5,
				
			},
		},
		
		
		
		
		GameARStickerObject2D:{
			proto_stickerTest:{
				
				modelConfig:{
					
//				htmlElementType:"div",
//				innerHTML:"TESTTESTTESTTESTTESTTESTTESTTEST<br/>TESTTESTTESTTESTTESTTESTTESTTEST<br/>TESTTESTTESTTESTTESTTESTTESTTEST<br/>TESTTESTTESTTESTTESTTESTTESTTEST<br/>TESTTESTTESTTESTTESTTESTTESTTEST<br/>",
					htmlElementType:"img",
					src:"test0.png",
					
				},
				
			},
		},
		
		
		
		
		
		
		GameARSingleMarkerAnchor:{
			proto_ARPattern:{
				patternFile:"pattern-aotracodeLogo_64.patt",
			},
			proto_emptyARPattern:{
				patternFile:"empty.patt",
			},
		},
		
		
		
		GameARMultiMarkersAnchor:{
			proto_quadriAnchor1:{
				patternFiles:["1.patt","2.patt","3.patt","4.patt"],
			},
		},
		
		
		
		GameColorsMultiPointsAnchor:{
			proto_colorsPattern:{
				colors:["#FF0000","#00FF00","#0000FF"],
				colorValueMargin:80,
			},
		},
	
		
		
		
		
	},
		
		

};
