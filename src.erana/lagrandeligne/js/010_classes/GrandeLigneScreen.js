
class GrandeLigneScreen extends GameScreen2D {

	constructor(mainId,config) {
		super(mainId,config,"lagrandeligne-saved");

	}
	
	// INIT ON START

	initOnStartScreenAtLevelStart(){
		super.initOnStartScreenAtLevelStart();
		
		
		const rootContainer=this.getSubclassRootContainer();
		
		// DBG
		console.log("initOnStartScreenAtLevelStart Screen",rootContainer);
		
		
//		// DBG
//		foreach(rootContainer.stations,(station)=>{
//			lognow("station.position:",station.position);
//		});
		

	}

	
	//=============== MODEL METHODS ===============
	
	/*protected*/getSubclassRootContainer(){
		if(this.currentLevel.isCinematic)
			return this.currentLevel.gameCinematic;
		return this.currentLevel.region;
	}
	
	/*protected*/getTrain(){
		return this.getSubclassRootContainer().getMainTrain();
	}
	
	


	// METHODS
	
	/*private*/getCurrentBackgroundsToOverride(){
		let results=[];
		
		let rootContainer=this.getSubclassRootContainer();
		const gameConfig=this.gameConfig;
		this.addToBackgrounds(gameConfig,results,rootContainer,0);
		if(!this.currentLevel.isCinematic){
			this.addToBackgrounds(gameConfig,results,rootContainer.weather,-1);
		}
		
		return results;
	}

	
	
	// =============== CONTROLLER METHODS ===============
	
	// ACTIONS (for each page)

	startCinematic(){
		
		// We stop the current game, if existing : 
//		super.stopGame();
		
		if(!this.currentLevel.isCinematic)	return;
		
		
		// We start moving the cinematic scroll :
		// WORKAROUND : after some time, to allow all model to be loaded ! :
		let self=this;
		setTimeout(function(){
			
			let cinematic=self.getSubclassRootContainer();
			
			
			if(cinematic){
				
				let camera=self.getCamera();
				cinematic.start(camera);
				
			}
			
			
		},1000);
		
	}
	
	stopCinematic(){
		
		let cinematic=this.getSubclassRootContainer();
		if(cinematic){
			cinematic.stop();
		}
		
	}
	
	
	
	//homePage
	startNewGame(){
		
		// We start moving the train :
		// WORKAROUND : after some time, to allow all model to be loaded ! :
		let self=this;
		setTimeout(function(){
			
			self.startMovingTrainNonStop();
			
			let region=self.getSubclassRootContainer();
			region.startScenario();
			
		},1000);
		
		
	}
	
	continueGame(){	
		// We start moving the train
		// WORKAROUND : after some time, to allow all model to be loaded ! :
		let self=this;
		setTimeout(function(){
			
			self.startMovingTrainNonStop();
			
		},1000);
	}
	
	startCredits(){	}


	//gamePage
	resumeGame(){	}
	
	interruptGame(){ 

		// We save the current game :
		this.saveGame();
		this.abandonGame();
		
	}
	
	
	
	abandonGame(){
		super.abandonGame() ;
//	super.exitGame();
		this.pagesManager.goToPage("homePage");
	}
	
	
	/*public*/endDemo(){
		this.pagesManager.goToPage("endOfDemoCinematicPage");
		this.startCinematic();
		
	}
	
	
	//creditsPage
//	exitCredits(){	/*DO NOTHING*/ }
	

	
	
	
	// METHODS
	
	// FACULTATIVE PUBLIC METHODS
	
	
	getDefaultRollingPlatform(){
		// We have only one selection in this game :
//	return this.getTrain().rollingPlatforms[0].dockableEquipments[0];
		return this.getTrain().rollingPlatforms[0].getDockableEquipment();
	}
	
	
	

	// MANDATORY METHODS
	
	

	
	/*private*/startMovingTrainNonStop(){
		
		let camera=this.getCamera();
		
		
		let childrenNameOnWhichCameraIsFixed=this.gameConfig.cameraFixed.name;
		let childrenIndexOnWhichCameraIsFixed=this.gameConfig.cameraFixed.index;
		
		// We move all the trains :
		let trains=this.getSubclassRootContainer()[childrenNameOnWhichCameraIsFixed];
		let self=this;
		foreach(trains,(train,index)=>{
			
			// We move the camera too, attached to the train that we want to follow :
			let followers=[];
			if(self.gameConfig.cameraFixed.index===index){
				followers.push(camera);
			}
			
			train.startMovingNonStop(followers);
			
		});

		// We move the daylight manager :
		let region=this.getSubclassRootContainer();
		region.daylightManager.startMovingNonStop();
		
	}
	
}


