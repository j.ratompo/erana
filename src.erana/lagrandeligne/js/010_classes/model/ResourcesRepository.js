const RESOURCES_X_SHIFT=20;
const RESOURCES_Y_SHIFT=0;

const RESOURCES_WIDTH_FACTOR=.75;

class ResourcesRepository{

	constructor() {

		this.isVisible=true;

		this.resources=[];
		this.resourcesImages={};
		
	}

	// INIT
	init(gameConfig){
		this.gameConfig=gameConfig;
		
	
	}

	initWithParent(){

		this.initResources();
		
	}
	
	
	initResources(){
		
		let repositoryParent=this.parent;
		let totalWidth=repositoryParent.size.w;
		let numberOfSlots=getArraySize(this.resources);
		if(numberOfSlots==0)	return;
		let step=Math.round(totalWidth/numberOfSlots);
		let index=0;
		let self=this;
		foreach(this.resources,(resource,resourceName)=>{
			
			//resource.positionX=Math.floor(step*index-step/2 - repositoryParent.size.w/2); // (Because all objects are in a «center x bottom» center mode)
			let demiTotalWidth=totalWidth*.5;
			resource.positionX=Math.floor((step*index-demiTotalWidth)*RESOURCES_WIDTH_FACTOR); // (Because all objects are in a «center x bottom» center mode)
			resource.name=resourceName;
			
			// NO : Only to be able to use repository.load/unload methods : resource.repository=self;
			resource.parentPlace=repositoryParent;
			
			self.resourcesImages[resource.name]=self.loadResourceImage(resource.name);
			
			let amount=resource.amount;
			if(isString(amount)){
				resource.amount=Math.getRandomInRange(amount);
			}
			
			index++;
		},(resource,resourceName)=>{	return self.isResourceNameDisplayable(resourceName); });
		

	}
	
	getImageForResourceName(resourceName){
		return this.resourcesImages[resourceName];
	}

	
	// METHODS
	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}

	
	isResourceNameDisplayable(resourceName){
		return !contains(this.excludedResourcesImages,resourceName);
	}
	
	/*private*/loadResourceImage(resourceName){
		let result=new Image();
		result.src=getConventionalFilePath(this.gameConfig.basePath,"resources/","universe")+resourceName+".png";
		return result;
	}
	
	
	
	
	getInsufficientResourcesList(costsStepsByNames){

		let insufficientResourcesList=[];
		
		foreach(costsStepsByNames,(costAmount,costName)=>{
			let r=this.resources[costName];
			if(r==null){
				// TRACE
				console.log("WARN : Resource repository for resource named «"+costName+"» do not exist in this resources repository.");
				return "continue";
			}
			
			if(r.amount<costAmount){
				insufficientResourcesList.push(costName);
			}

		});

		return insufficientResourcesList;
	}
	
	getWastesFullResourcesList(wastesByNames){

		let wastesFullResourcesList=[];
		
		foreach(wastesByNames,(wasteAmount,wasteName)=>{
			let w=this.resources[wasteName];
			if(w==null){
				// TRACE
				console.log("WARN : Resource repository for waste named «"+wasteName+"» do not exist in this resources repository.");
				return "continue";
			}
			
			if(w.capacity<w.amount+wasteAmount){
				wastesFullResourcesList.push(wasteName);
			}

		});

		return wastesFullResourcesList;
	}
	
	
	
	hasInsufficientResources(costsStepsByNames){
		return !!foreach(costsStepsByNames,(costAmount,costName)=>{
			
			if(this.resources[costName]==null){
				// TRACE
				console.log("WARN : Resource repository for resource named «"+costName+"» do not exist in this resources repository.");
				return "continue";
			}
			
			if(this.resources[costName].amount<costAmount){
				return true;
			}

		});
	}
	
	
	/*public*/consumeStocks(resourcesStepsByNames){

		let self=this;
		foreach(resourcesStepsByNames,(resourceStep,resourceName)=>{
			self.incrementStock(resourceName, -resourceStep);		
		});
	}

	/*public*/increaseStocks(resourcesStepsByNames){

		let self=this;
		foreach(resourcesStepsByNames,(resourceStep,resourceName)=>{
			self.incrementStock(resourceName, resourceStep);		
		});
	}

	/*public*/incrementStock(stockName, amount){
		
		let resource=this.resources[stockName];
		
		if(!resource){
			// TRACE
			console.log("WARN : Resource repository for resource named «"+stockName+"» do not exist in this resources repository.");
			return false;
		}
		
		if(resource.amount<amount && amount<0){ // case consumption of the stock
//			// TRACE
//			console.log("WARN : Resource repository named «"+stockName+"» is empty. Cannot consume this stock.");
			return false;
		}
		
		if((resource.capacity<=resource.amount && 0<amount) || resource.capacity<resource.amount+amount){ // case filling of the stock
//			// TRACE
//			console.log("WARN : Resource repository named «"+stockName+"» is full. Cannot increase this stock.");
			return false;
		}
		

		resource.amount = resource.amount+amount;
		
		
		window.eranaScreen.controlsManager.refreshInfoPad(resource.parentPlace);

		
		return true;
	}

	
	isStockFull(resourceName){
		if(typeof(this.resources[resourceName])=="undefined" || this.resources[resourceName]==null){
			// TRACE
			console.log("WARN : Resource repository for resource named «"+resourceName+"» do not exist in this resources repository.");
			return true;
		}
		
		return this.resources[resourceName].capacity <= this.resources[resourceName].amount; 
	}

	
	isStockEmpty(resourceName){
		if(typeof(this.resources[resourceName])==="undefined" || this.resources[resourceName]==null){
			// TRACE
			console.log("WARN : Resource repository for resource named «"+resourceName+"» do not exist in this resources repository.");
			return true;
		}
		
		return this.resources[resourceName].amount<=0; 
	}
	
	
//	setStockRetrieving(resourceName,retrieve=false){
//		if(typeof(this.resources[resourceName])=="undefined" || this.resources[resourceName]==null){
//			// TRACE
//			console.log("WARN : Resource repository for resource named «"+resourceName+"» do not exist in this resources repository.");
//			return;
//		}
//		
//		this.resources[resourceName].retrieve=retrieve; 
//	}
	

	/*public*/isStockRetrieving(resourceName){
//		if(typeof(this.resources[resourceName])=="undefined" || this.resources[resourceName]==null){
//			// TRACE
//			console.log("WARN : Resource repository for resource named «"+resourceName+"» do not exist in this resources repository.");
//			return false;
//		}
//		
//		return this.resources[resourceName].retrieve; 
		
		let costsNames=this.parent.getNeededCostsNames();
		let result=contains(costsNames,resourceName);
		return result;
	}
	
	
	/*public*/getGivingResources(resourceInNeedNameFilter=null){
		let givingResources=[];
		
		foreach(this.resources,(resource,resourceName)=>{
			givingResources.push(resource);
		},(resource,resourceName)=>{
			return   !this.isStockRetrieving(resourceName)
						&& 0<resource.amount 
						&& (resourceInNeedNameFilter==null || resourceName===resourceInNeedNameFilter);
		});
		
		
		return givingResources;
	}
	
	
	/*public*/getNeedingResources(resourceInNeedNameFilter=null,ignoreRetrievingDirective=false){
		let needingResources=[];
		
		const self=this;
		foreach(this.resources,(resource,resourceName)=>{
			needingResources.push(resource);
		},(resource,resourceName)=>{	
			return	 (ignoreRetrievingDirective || self.isStockRetrieving(resourceName))
						&& resource.amount<resource.capacity 
					 	&& (!resourceInNeedNameFilter || resource.name===resourceInNeedNameFilter);
		});
		
		return needingResources;
	}
	
	
	
	getAmountAndTotal(resourceName){
		let resource=this.resources[resourceName];
		let amount=Math.floor(resource.amount);
		return {amount:amount, total:resource.capacity};
	}
	
	
	
	// -----------------------------------------------------



	draw(ctx,camera,lightsManager){

		
		let resourcesRepositoryOwner=this.parent;
		
		if(!resourcesRepositoryOwner.getGameImage().canDraw(camera))	return;

		const zooms=this.gameConfig.zooms;
		
		const drawableUnzoomedCoordinates=getDrawableZoomedIfNecessaryCoordinates2D(this.gameConfig,resourcesRepositoryOwner.position.getParentPositionOffsetted(),camera);

		let self=this;
		foreach(this.resources,(resource,resourceName)=>{
			let image=self.resourcesImages[resourceName];
			let amount=resource.amount;
			
			let xOffset=0;
			let index=0;
			let centeringOffset=Math.floor((amount/self.maxLotHeightAmount)/2);//(to allow the people to load / unload in he middle of the resource, and not at the left side only)

			for(let i=0;i<amount;i++){
				
				let x=resource.positionX + drawableUnzoomedCoordinates.x + (xOffset-centeringOffset)*self.spacingPixelsNumber + RESOURCES_X_SHIFT  ;
				// CAUTION : y axis is inverted !
				let y=-index*self.spacingPixelsNumber + drawableUnzoomedCoordinates.y - self.yOffsetRepository + RESOURCES_Y_SHIFT;
				
				drawImageAndCenterWithZooms(ctx,image,x,y,zooms,{x:"center",y:"bottom"});
				
				if((i+1)%self.maxLotHeightAmount===0){
					index=0;
					xOffset++;
				}else{
					index++;
				}
			}
			
		},(resource,resourceName)=>{	return self.isResourceNameDisplayable(resourceName) && 0<resource.amount; });
		
	}

}
