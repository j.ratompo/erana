const IS_DEBUG_TRAIN=false;


const UI_ELEMENTS_TRAIN_IS_FIXED=true;

const UI_ELEMENTS_TRAIN_FIXED_RIGHT=-60;
const UI_ELEMENTS_TRAIN_FIXED_TOP=40;
const UI_ELEMENTS_TRAIN_X=-20;
const UI_ELEMENTS_TRAIN_Y=20;
const UI_ELEMENTS_TRAIN_TEXT_FONT="helvetica";
const UI_ELEMENTS_TRAIN_TEXT_SIZE=18;

const TRAIN_CHANGE_SPEED_REFRESH_RATE_MILLIS=100;

class Train{

	constructor() {
		
		this.isVisible=true;
		this.isMovable=true;
		
		this.position=new Position(this,0,0,0,{x:"right",y:"bottom"});

		this.mover=getMoverMonoThreadedNonStop(this);
		
		this.speed=0;

		this.changeSpeedRoutine=null;
		
	}

	// INIT
	init(gameConfig){
		this.gameConfig=gameConfig;
		
		this.goalSpeed=this.nonStopSpeed;
		
		// Wrapping handling :
		this.getMover().setWrapping(this.gameConfig.wrapping).setDurationTimeFactorHolder(window.eranaScreen);
		
	}

	
	// METHODS
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	
	getIsMovable(){
		return this.isMovable;
	}
	setIsMovable(isMovable){
		this.isMovable=isMovable;
	}
	
	
	getMover(){
		return this.mover;
	}
	
	startMovingNonStop(followers){
		
		this.speed=this.nonStopSpeed;
		this.getMover().doStartNonStop({x:this.speed},followers);
		
		// We start the train's scripts, if needed :
//	let self=this;
		foreach(this.gameScripts,(script)=>{
//		script.start(self);
			script.startStartable();
		});
		
	}
	

	// Most left platform :
	getLastPlatform(includePending=false){
		
		let extremityX=0;
		let extremityPlatform=this.getFirstPlatform();
		foreach(this.rollingPlatforms,(platform)=>{
			if(platform.position.x<extremityX) {
				extremityX=platform.position.x;
				extremityPlatform=platform;
			}
		},(platform)=>(includePending?true:!platform.isPending()));
		
		return extremityPlatform;
	}
	
	// Most right platform :
	getFirstPlatform(){
		return this.rollingPlatforms[0];
	}
	
	
	getFirstLocomotive(){
		return this.getFirstPlatform().getDockableEquipment();
	}
	
	getExtremities(){
		let leftPlatform=this.getLastPlatform();
		let rightPlatform=this.getFirstPlatform();
		return {
			min:{x:leftPlatform.position.getParentPositionOffsetted(). x-Math.floor(leftPlatform.size.w/2)},
			max:{x:rightPlatform.position.getParentPositionOffsetted().x+Math.floor(rightPlatform.size.w/2)}
  		};
	}
	
	getSize(){
		
		let w=0;
		foreach(this.rollingPlatforms,(platform)=>{
			w+=platform.size.w;
		});
		
		// We add the eventual hanging ergostasio width :
		let self=this;
		foreach(this.ergostasios,(ergostasio)=>{
			if(ergostasio.position.x<self.getLastPlatform().position.x) {
				w+=ergostasio.size.w;
			}
		});
		
		let p=this.getFirstPlatform();
		let h=p.size.h + p.getDockableEquipment().size.h;
		
		return {w:w,h:h};
	}
	
	/*public*/getSpeed(){
		return this.speed;
	}
	
	
//	getAdjacentPlatform(platform,index){
//		
//		return foreach(this.rollingPlatforms,(p,i)=>{
//			if(platform===p){
//				if(i===0 && index===-1)	return null;
//				if(i===this.rollingPlatforms.length-1 && index===1)	return null;
//				return this.rollingPlatforms[i+index];
//			}
//		});
//		
//	}
	
	getClosestNonBusyErgostasio(askingPosition){
		
		let result=null;
		let minDistance=null;
		foreach(this.ergostasios,(equipment)=>{
			let deltaX=Math.abs(askingPosition.x-equipment.position.x);
			if(minDistance==null || deltaX<minDistance) {
				minDistance=deltaX;
				result=equipment;
			}
			
		},(e)=>{	return containsIgnoreCase(e.prototypeName,"ergostasio") && !e.busy;	});
		
		return result;
	}
	
	getClosestPlatform(askingPosition){

		let result=null;
		let minDistance=null;
		foreach(this.rollingPlatforms,(platform)=>{
			let deltaX=Math.abs(askingPosition.x-platform.position.x);
			if(minDistance==null || deltaX<minDistance) {
				minDistance=deltaX;
				result=platform;
			}
			
		},(platform)=>{	return containsIgnoreCase(platform.prototypeName,"platform") && !platform.isPending();	});
		
		return result;
	}
	
	
	/*public*/getPlatformAtPosition(askingPosition){
		let result=foreach(this.rollingPlatforms,(platform)=>{
			return platform;
		},(platform)=>{	
			return containsIgnoreCase(platform.prototypeName,"platform")  && !platform.isPending() && platform.isOnPosition(askingPosition);
		});
		
		return result;
	}

	/*public*/getErgostasioAtPosition(askingPosition){
		return foreach(this.ergostasios,(ergostasio)=>{
			return ergostasio;
		},(ergostasio)=>{	
			return containsIgnoreCase(ergostasio.prototypeName,"ergostasio") && ergostasio.isOnPosition(askingPosition) ;
		});
	}
	
	/*public*/getErgostasioPastLastPosition(){
		let lastPlatform=this.getLastPlatform();
		let askingPosition={x:lastPlatform.position.x - lastPlatform.size.w};
		return this.getErgostasioAtPosition(askingPosition);
	}
	
	/*public*/getErgostasios(){
		return this.ergostasios;
	}
	
	// For ergostasios building :
	/*public*/getErgostasiosNumberAtPosition(askingPosition){
		
		let result=0;
		foreach(this.ergostasios,()=>{
			result++;
		},(ergostasio)=>{	
			return containsIgnoreCase(ergostasio.prototypeName,"ergostasio") && ergostasio.isOnPosition(askingPosition);
		});
		
		return result;
	}
	
	
	isErgostasioMovementBlockedTillPosition(ergostasio, askingPosition){
		
		// Returns true if there is at least one ergostasio on the way :

		return true && foreach(this.ergostasios,(e)=>{
			if( ergostasio.position.x < e.position.x && e.position.x<askingPosition.x
			|| 	askingPosition.x< e.position.x && e.position.x<ergostasio.position.x
			)
				return true;
		},(platform)=>{	return containsIgnoreCase(platform.prototypeName,"ergostasio") ; });
		
	}
	
	
//	giveRetrievingOrders(askingPlatform, resourceName){
//		
//		let giverPlatforms=[];
//		foreach(this.rollingPlatforms,(giverPlatform)=>{
//			giverPlatforms.push(giverPlatform);
//		},(p)=>{	return !p.resourcesRepository.isStockEmpty(resourceName) && !p.resourcesRepository.isStockRetrieving(resourceName); 	});
//		
//		if(empty(giverPlatforms)){
//			// TRACE
//			console.log("WARN : No resource available for resource «"+resourceName+"».");
//			return;
//		}
//		
//		
//		foreach(this.peoples,(transporter)=>{
//				// TODO
//		},(p)=>{	return p.getJobType()==="transporter" && !p.busy		});
//		
//	}
	
	
	getResourcesGivingPlatforms(){
		let list=[];
		foreach(this.rollingPlatforms,(platform)=>{;
			list.push(platform);
		},(platform)=>{	
			return containsIgnoreCase(platform.prototypeName,"platform") && !platform.isPending() && !empty(platform.getGivingResources());	
		});

		return list;
	}

	
	
	/*public*/getResourcesNeedingPlatforms(resourceInNeedNameFilter=null, ignoreRetrievingDirective=false){
		let list=[];

		foreach(this.rollingPlatforms,(platform)=>{
			const needingResourcesOnPlatform=platform.getNeedingResources(resourceInNeedNameFilter, ignoreRetrievingDirective);
			if(!empty(needingResourcesOnPlatform)
			&& !this.hasEnoughIncomingResourcesToPlatform(platform,needingResourcesOnPlatform)
			 ){
				list.push(platform);
			}
		},(platform)=>containsIgnoreCase(platform.prototypeName,"platform") && !platform.isPending());
		
		return list;
	}
	
	/*private*/hasEnoughIncomingResourcesToPlatform(platform,needingResourcesOnPlatform){
		
		const incomingResources={};
		
		foreach(this.getPeoples(),(people)=>{
			const loadedResourceName=people.loadedResourceName;
			if(incomingResources[loadedResourceName]==null)	incomingResources[loadedResourceName]=0;
			incomingResources[loadedResourceName]+=people.loadedAmount;
		},(people)=>(people.getDestinationPlatform() && people.getDestinationPlatform()===platform && !empty(people.loadedResourceName)));
		
		if(empty(incomingResources)){
			// We have here a needing resources but no incoming amount :
			return false;
		}
		
		let hasEnoughIncomingResourcesToPlatformResult=true;
		foreach(needingResourcesOnPlatform,(needingResource)=>{
			const incomingResourceSum=incomingResources[needingResource.name];
			if(incomingResourceSum==null){
				// We have here a needing resource but no incoming amount :
				hasEnoughIncomingResourcesToPlatformResult=false;
				return "break";
			}
			const projectedAmount=needingResource.amount+incomingResourceSum;
			if(projectedAmount<needingResource.capacity){
				// We have here a needing resource but not enough incoming amount :
				hasEnoughIncomingResourcesToPlatformResult=false;
				return "break";
			}
		});
		
		return hasEnoughIncomingResourcesToPlatformResult;
	}
	
	changeSpeed(goalSpeed){
		
		let realDeltaTMillis=window.eranaScreen.getLastMesuredRefreshedRateMillis();
		
		this.goalSpeed=Math.round(goalSpeed);
		if(!this.changeSpeedRoutine || !this.changeSpeedRoutine.isStarted()){
			this.changeSpeedRoutine = 
				getMonoThreadedGoToGoal(this, this.speed, this.goalSpeed,
						Math.max(realDeltaTMillis,TRAIN_CHANGE_SPEED_REFRESH_RATE_MILLIS),
						this.inertiaTimeMillis
//						,"linear",null,function(selfParam){
//					// DBG
//					console.log("SPEED ATTEINTE !",selfParam);
////					selfParam.speed=selfParam.goalSpeeds;
//					}
				).setDurationTimeFactorHolder(window.eranaScreen);

			if(!this.changeSpeedRoutine.isStarted())
					this.changeSpeedRoutine.start();
		}else{
			this.changeSpeedRoutine.setNewGoal(this.goalSpeed);
		}
		
	}
	
	getGoalSpeed(){
		return this.goalSpeed;
	}
	
	
	/*public*/doSetSpeed(newSpeed, forceChangeGoalSpeed=false){
		this.getMover().setAbsoluteSpeedX(newSpeed);
		this.speed=newSpeed;
		if(forceChangeGoalSpeed) {
			this.goalSpeed=newSpeed;
			if(this.changeSpeedRoutine  && this.changeSpeedRoutine.isStarted()){ // To be protected against the possible case if we enter a station without having ever changed the trains's speed ! 
																																					 // (so the change speed routine has not been created nor started !)
				this.changeSpeedRoutine.resetValueAndGoalTo(newSpeed);
				this.changeSpeedRoutine.stop();
			}
		}
	}
	
	getCollidedStation(){
		let gameConfig=this.gameConfig;
		let projection = gameConfig.projection;
		// TODO : FIXME : «scroll» ATTRIBUTE MUST BE IN THE ROOT CONTAINER, NOT IN THE GLOBAL GAME CONFIG !
		let scroll = gameConfig.scroll;
		return getCollidedObject(this, this.parent.stations, projection, scroll);
	}
	
	/*public*/move(){
		// Mover stepping :
		// Only for monothreaded movers :
		this.getMover().doStep();
	}
	
	startGoingOut(selfParam, params){
		
		let self=selfParam;
	
		let side=params.side;
		let speed=params.speed;
		// If we want a vertical out movment too :
		if(side && speed){// includes case value===0
			let mover=self.getMover();
			let currentAbsoluteSpeeds=self.getMover().absoluteSpeeds;
			let verticalSpeed=(side==="up"?speed:-speed);
			self.getMover().setAbsoluteSpeedX(currentAbsoluteSpeeds.x).setAbsoluteSpeedY(verticalSpeed);
		}
		
		self.changeSpeed(params.escapeHorizontalSpeed);
		
	}

	stopGoingOut(selfParam){
		let self=selfParam;
		self.parent.removeTrain(self);
	}
	
	isOutOfScreen(selfParam,camera){
		let self=selfParam;
		let result=!isInZoomedVisibilityZone(self.gameConfig,camera,self.position,self.getSize());
		return result;
	}
	
	/*public*/hasReachedMaxPlatforms(){
		if(!this.maxPlatforms)	return false;
		return (this.maxPlatforms<=this.rollingPlatforms.length);
	}
	
	/*public*/hasReachedPosition(positionX){
		const REACHED_POSITION_MARGIN=400;
		
		let zone={x:positionX,w:REACHED_POSITION_MARGIN};
		let result=this.position.hasReached(zone);
		
//	// DBG
//	if(result)	lognow("hasReachedPosition("+result+")",this.position);
		
		return result;
	}
	
//	/*private*/initIcons(){
//		
//		
//		let buttonsContainer=document.createElement("span");
////		buttonsContainer.style.position
//		
//		window.eranaScreen.controlsManager.appendChild(buttonsContainer); 
//		
//		
//		
//	}
	
	
	// CAUTION : We do not have a doOnClick() method, because train has no real click or selection purpose, it is only to allow its children to have this behavior !
	
	isSelectable(){
		return this.selectable!==false; // CAUTION : By default, it is selectable !
	}

	isClickable(){
		return this.clickable!==false; // CAUTION : By default, it is clickable !
	}

	
	/*public*/isStopped(){
//	NO : (UNSAFE) :	return Math.round(this.getSpeed())===0;
		return (this.getSpeed()<=0);
	}
	
	/*public*/setIsAutoBraking(autoBraking){
		this.autoBraking=autoBraking;
	}
	
	/*public*/isAutoBraking(){
		return this.autoBraking && this.getSpeed()<this.getMainLocomotiveMaxSpeed();
	}

	
	/*private*/getMainLocomotive(){
		return this.getFirstPlatform().getDockableEquipment();
	}
	
	/*private*/getMainLocomotiveMaxSpeed(){
		return this.getMainLocomotive().getMaxSpeed();
	}
	
	/*private*/getRealSpeedPixels(deltaTMillisParam=null){
		
		let deltaTMillis=nonull(deltaTMillisParam, window.eranaScreen.refreshRateMillis);
		let speed=Math.round(this.getSpeed());
		let realSpeed=(speed/deltaTMillis);
		return realSpeed; // The number of pixels per millisecond
	}
	
	/*private*/getRealSpeedMetersBySecond(deltaTMillisParam=null){
		let speed=this.getRealSpeedPixels(deltaTMillisParam);
		let realSpeed=speed*this.gameConfig.scale*1000;
		return realSpeed; // The number of meters per second
	}
	
	/*public*/getDistanceToNextStationMeters(){
		return this.distanceToNextStationMeters;
	}
	
	/*public*/getDistanceToNextStationPixels(){
		return this.distanceToNextStationPixels;
	}
	
	/*public*/affectHabitation(people){
		
		let habitation=foreach(this.rollingPlatforms,(platform)=>{
			let h=platform.getDockableEquipment();
			h.affect(people);
			return h;
		},(p)=>{	
			let dockableEquipmentHabitation=p.getDockableEquipment();
			return dockableEquipmentHabitation
				  && containsIgnoreCase(dockableEquipmentHabitation.prototypeName,"habitation") 
				  && dockableEquipmentHabitation.canAffect(people);
		});

		return habitation;
	}
	
	/*public*/overrideThumbPosition(gameThumbParam,thumbPosition){
		let result=null;
		
		if(!this.thumbsPlatformsPerLine) return null;
		
		let refThumb=this.getFirstLocomotive().gameThumb;
		
		// DBG
		if(!refThumb.size)
			lognow("SHOULD NOT HAPPEN !");
		
		let typicalThumbWidth=refThumb.size.w;
		let typicalThumbDemiWidth=typicalThumbWidth*.5;
		let typicalThumbHeight=refThumb.size.h;
		let pos=thumbPosition;
		
		let lastPlatform=this.getLastPlatform();
		let widthPlatform=lastPlatform.size.w;

		let lineWidth=typicalThumbWidth*this.thumbsPlatformsPerLine;
		
		
		let thumbPositionX=thumbPosition.x-typicalThumbDemiWidth;
		
		// x
		let x=thumbPositionX%lineWidth+typicalThumbDemiWidth;
		// y
		let stepsNumberY=Math.floor(thumbPositionX/lineWidth);
		
		const lastThumbPositionX=(lastPlatform.position.x*typicalThumbWidth)/widthPlatform;
		const totalStepsNumberY=Math.ceil(lastThumbPositionX/lineWidth);

		// We have an inversion on the y stack here :
		let y=thumbPosition.y+(totalStepsNumberY-stepsNumberY)*typicalThumbHeight;
		
		
		let resolution=this.gameConfig.getResolution(); 
		x+=(resolution.w-gameThumbParam.right);
		y+=(resolution.h-gameThumbParam.bottom);
		
		
		result={x:x,y:y};
		return result;
	}


	/*public*/removePlatform(platform){
		remove(this.rollingPlatforms,platform);
	}
	
	/*public*/removeErgostasio(ergostasio){
		remove(this.ergostasios,ergostasio);
	}
	
	
	getDrawables(){
		// CAUTION ! order in this array has an effect on the drawing index, and thus the apparent zIndex !
		return [
			this.rollingPlatforms,
			this.ergostasios,
			this.peoples,
		];
	}
	
	/*public*/getPeoples(){
		return this.peoples;
	}
	
	/*public*/getAvailablePeoplesForWork(){
		const results=[];
		foreach(this.peoples,(p)=>{
			results.push(p);
		},(p)=>p.isAvailableForWork());
		return results;
	}
	
	
	/*public*/getRollingPlatforms(){
		return this.rollingPlatforms;
	}
	
	/*public*/getEquipmentsOfType(partialPrototypeName=null, equipmentType=null){
		let resultList=[];
		
		foreach(this.getRollingPlatforms(),(platform)=>{
			
			
			let dockableEquipment=platform.getDockableEquipment();
			let overEquipment=platform.getOverEquipment();

			if(equipmentType==="dockableEquipment"){
				let equipment=dockableEquipment;
				if(equipment && (!partialPrototypeName || containsIgnoreCase(equipment.prototypeName, partialPrototypeName))){
					resultList.push(equipment);
				}
			}else if(equipmentType==="overEquipment"){
				let equipment=overEquipment;
				if(equipment && (!partialPrototypeName || containsIgnoreCase(equipment.prototypeName,partialPrototypeName))){
					resultList.push(equipment);
				}
			}else{
				if(dockableEquipment && (!partialPrototypeName || containsIgnoreCase(dockableEquipment.prototypeName,partialPrototypeName))){
					resultList.push(dockableEquipment);
				}
				if(overEquipment && (!partialPrototypeName || containsIgnoreCase(overEquipment.prototypeName,partialPrototypeName))){
					resultList.push(overEquipment);
				}
			}
			
			
		});
		
		return resultList;
	}
	
	/*public*/addRollingPlatform(platform){
		this.getRollingPlatforms().push(platform);
		platform.parent=this;
	}
	
	
	

	/*public*/hasAvailableWorkPositions(){
		return !!foreach(this.getRollingPlatforms(),(platform)=>{
			return true;
		},(platform)=>platform.hasAvailableWorkPositions());
	}

	/*public*/getPlatformsWithAvailableWorkPositions(){
		const results=[];
		
		foreach(this.getRollingPlatforms(),(platform)=>{
			results.push(platform);
		},(platform)=>platform.hasAvailableWorkPositions());
		
		return results;
	}


	/*public*/getPosition(){
		return this.position;
	}
	
	
	// -----------------------------------------------------------

	
	/*private*/drawSpeedUI(ctx,camera){
		
		if(this.ui && this.ui.drawSpeed==false)	return false;

		const gameConfig=this.gameConfig;
		const zooms=gameConfig.zooms;
					
		const drawableUnzoomedCoordinates=getDrawableZoomedIfNecessaryCoordinates2D(this.gameConfig, this.position.getParentPositionOffsetted(), camera);
		const x=drawableUnzoomedCoordinates.x;
		const y=drawableUnzoomedCoordinates.y;
	
		let message=Math.round(this.getRealSpeedMetersBySecond())+"m/s";
		let resolution=this.gameConfig.getResolution();
		
		if(UI_ELEMENTS_TRAIN_IS_FIXED)	drawSolidText(ctx,resolution.w+UI_ELEMENTS_TRAIN_FIXED_RIGHT,UI_ELEMENTS_TRAIN_FIXED_TOP,message,"#AAAAAA",UI_ELEMENTS_TRAIN_TEXT_SIZE,UI_ELEMENTS_TRAIN_TEXT_FONT,"bold",zooms);
		else														drawSolidText(ctx,x+UI_ELEMENTS_TRAIN_X,y+UI_ELEMENTS_TRAIN_Y,message,"#AAAAAA",UI_ELEMENTS_TRAIN_TEXT_SIZE,UI_ELEMENTS_TRAIN_TEXT_FONT,"bold",zooms);

		if(IS_DEBUG_TRAIN)		drawSolidText(ctx,resolution.w-114,UI_ELEMENTS_TRAIN_FIXED_TOP+60,this.position.x,"#AAAAAA",UI_ELEMENTS_TRAIN_TEXT_SIZE,UI_ELEMENTS_TRAIN_TEXT_FONT,"bold",zooms);
		
		return true;
	}
	
	/*private*/drawNextStationUIAndSetDistanceToNextStation(ctx, camera){
		let region=this.parent;
		let objectWidth=empty(this.rollingPlatforms)?200:this.rollingPlatforms[0].size.w;
		let closestStation=region.getClosestNextStationDeltaXInformation(this.position, objectWidth);
		
		if(!closestStation)	return;
		
		this.distanceToNextStationPixels=closestStation.deltaX;
		this.distanceToNextStationMeters=Math.round(this.distanceToNextStationPixels*this.gameConfig.scale);
		
		let distance=(9999<this.distanceToNextStationMeters?Math.round(this.distanceToNextStationMeters*.001):this.distanceToNextStationMeters);
		let mesureUnit=(9999<this.distanceToNextStationMeters?"km":"m");
		let message=i18n("prochainArrêt")+distance+mesureUnit;
		
		const gameConfig=this.gameConfig;
		const zooms=gameConfig.zooms;
		const resolution=gameConfig.getResolution();
		
		drawSolidText(ctx, Math.floor(resolution.w/2)+90,15, message,"#CCCCCC",15,UI_ELEMENTS_TRAIN_TEXT_FONT,"bold",zooms);
	}

	drawInBack(ctx, camera, lightsManager){
		/*DO NOTHING*/
		// (Only to allow to drawInBack children elements...)
	}
	
	draw(ctx, camera, lightsManager){
		/*DO NOTHING*/
		// (Only to allow to draw children elements...)

		//...and to allow the mono-threaded change speed routine to do its treatment :
		if(this.changeSpeedRoutine && this.changeSpeedRoutine.isStarted()){
			let newSpeed=this.changeSpeedRoutine.doStep(this);
			this.doSetSpeed(newSpeed);
		}
		
		
		// TODO : FIXME : CENTRALIZE STEPPING ! (in gameScreen in the main loop)
		// Scripts stepping :
		let self=this;
		foreach(this.gameScripts,(script)=>{
			script.doStep(camera);
		});
	}
	
	drawInFront(ctx, camera){
		// !!! CAUTION : not handled in WheelsSet getDrawables() only to override the usual children drawing sequence !!!
		foreach(this.rollingPlatforms,(p)=>{
			foreach(p.wheelsSets,(w)=>{
				w.drawSmokePit(ctx,camera);
			});
			
			let equipment=p.getDockableEquipment();
			if(equipment && equipment.drawSmokePits){
				equipment.drawSmokePits(ctx,camera);
			}

			let overEquipment=p.getOverEquipment();
			if(overEquipment && overEquipment.drawSmokePits){
				overEquipment.drawSmokePits(ctx,camera);
			}
		});
		
		return true;
	}
	
	drawInUI(ctx, camera){
		
		// Speed display :
		// Only for selectable trains : (ie. game is effectively started)
		if(!this.isSelectable())	return;
		this.drawSpeedUI(ctx,camera);
		this.drawNextStationUIAndSetDistanceToNextStation(ctx,camera);
		
	}

}
