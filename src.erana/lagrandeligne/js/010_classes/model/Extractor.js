
class Extractor{

	constructor() {

		this.isVisible=true;
		this.position=new Position(this);

		this.isMovable=false;
		
		
		this.img=new GameImage(this);
		this.anim=new GameAnimation(this);
		
	}
	
	
	// INIT

	init(gameConfig){
		
		this.gameConfig=gameConfig;

		this.position.center=this.imageConfig._2D.center;
		
		this.getGameImage().init(gameConfig);
		
		// First-time init :
		this.getGameAnimation().init(gameConfig).loadSprite2DAndSet().start();
		

		
	}
	
	// METHODS
	
	getGameImage(){
		return this.img;
	}
	
	getGameAnimation(){
		return this.anim;
	}

	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	
	
	getIsMovable(){
		return this.isMovable;
	}
	setIsMovable(isMovable){
		this.isMovable=isMovable;
	}
	
	
	// TO OVERRIDE NORMAL DRAWING ORDER :
//	getDrawables(){
//		return [this.gameSmokePit];
//	}
	// TO OVERRIDE NORMAL DRAWING ORDER :
	/*public*/drawSmokePit(ctx,camera){
		
		let s=this.gameSmokePit;
		s.draw(ctx,camera);

	}

	
	draw(ctx,camera,lightsManager){
		
		if(!this.getGameImage().canDraw(camera))	return false;
		
		let anim=this.getGameAnimation();
		anim.draw(ctx,camera,lightsManager);
		
		if(!this.getGameImage().draw(ctx,camera,lightsManager))	return false;
		
		// TO OVERRIDE NORMAL DRAWING ORDER :
		this.drawSmokePit(ctx,camera);
		
		return true;
	}


}
