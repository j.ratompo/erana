

class Producer extends FactoryEquipment{
	
	
	constructor() {
		super();


		this.productionRoutine=getMonoThreadedRoutine(this,BUILD_REFRESH_RATE_MILLIS);

	}
	
	// INIT

	initWithParent(){
		
		this.productionRoutine.setDurationTimeFactorHolder(window.eranaScreen);


		// All productions are active by default :
		foreach(this.productions,(production)=>{
			production.active=true;
		});
		
		// Productions or producer are always running (but they can be active or inactivated) !
		this.startProductions();
		
		
//		if(this.gameSmokePits) {
//			let self=this;
//			foreach(this.gameSmokePits,(s)=>{
//				s.init(self.gameConfig);
//			});
//		}

		
	}
	
	
	// METHODS
	
	// =================== GAME RULES METHODS ===================
	
	
	
	// Producer :

	activateProduction(event, productionName){

		let production=this.productions[productionName];
		if(!production){
			// TRACE
			console.log("WARN : No production «"+productionName+"» possible for this producer.");
			return;
		}
		
		let isChecked=!!event.target.checked; /*(forced to boolean)*/
		
		production.active=isChecked;
	}
	
	isProductionActive(productionName){

		let production=this.productions[productionName];
		if(!production){
			// TRACE
			console.log("WARN : No production «"+productionName+"» possible for this producer.");
			return;
		}
		
		return production.active;
	}
	
	
	/*private*/startProductions(){
		
		super.startProductionAnimation();
		
		this.productionRoutine.start();
		
	}
	

	/*protected*/doOnEachStep(args){

		const selfParam=this;
		
		let platform=selfParam.parent;
		let resourcesRepository=platform.resourcesRepository;
		
		
		foreach(selfParam.productions,(production,productionName)=>{
			
			let timeMillis=production.timeSeconds*1000;
			
			let durationTimeFactor=window.eranaScreen.getDurationTimeFactor();
			if(!production.lastTime || hasDelayPassed(production.lastTime, timeMillis*durationTimeFactor)){
				production.lastTime=getNow();

				// Costs :
				let costs=production.costs;
				let insufficientResourcesList=resourcesRepository.getInsufficientResourcesList(costs);
				if(empty(insufficientResourcesList)){

					resourcesRepository.consumeStocks(costs);
					
					let efficacityFactor=selfParam.getEfficacityFactor();
					let productionValue=production.value * efficacityFactor;
					resourcesRepository.incrementStock(productionName, productionValue);
					
				}
				
				// Wastes :
				let wastes=production.wastes;
				let wastesFullResourcesList=resourcesRepository.getWastesFullResourcesList(wastes);
				if(empty(wastesFullResourcesList)){

					// Efficacity does not affect wastes production.
					resourcesRepository.increaseStocks(wastes);
					
				}
				
				
			}
			
			
		},(production)=>{	return production.active; });
		
		
	}
	

	/*public*/getNeededCostsNames(){
		
		let costsNames=[];
		
		let productions=this.productions;
		if(productions){
			foreach(productions,(production)=>{
				foreach(production.costs,(cost,costName)=>{
					costsNames.push(costName);
				},(costValue)=>{	return	0<costValue; /*we exclude the possible wastes*/	});
			},(production)=>{	return production.active;	});
		}
		
		return costsNames;
	}
	

	
	
	// OLD
//	/*private*/hasProductionHappening(selfParam){
//		
//
//		let platform=selfParam.parent;
//		let resourcesRepository=platform.resourcesRepository;
//		
//		let hasActiveProductions=false;
//		
//		let hasInsufficientResource=true;
//		let hasWasteFull=true;
//		foreach(selfParam.productions,(production,productionName)=>{
//
//			hasActiveProductions=true;
//			
//			// Costs :
//			let costs=production.costs;
//			let insufficientResourcesList=resourcesRepository.getInsufficientResourcesList(costs);
//			if(empty(insufficientResourcesList)){
//				hasInsufficientResource=false;
//				return "break";
//			}
//
//			// Wastes :
//			let wastes=production.costs;
//			let wastesFullResourcesList=resourcesRepository.getWastesFullResourcesList(wastes);
//			if(empty(wastesFullResourcesList)){
//				hasWasteFull=false;
//				return "break";
//			}
//			
//		},(production)=>{	return production.active; });
//		
//		
//		return hasActiveProductions && !hasInsufficientResource && !hasWasteFull;
//	}

	/*private*/getActiveProductions(){
		let results=[];
		
		foreach(this.productions,(production,productionName)=>{
			results.push(production);
		},(production)=>{	return production.active; });

		return results;
	}
	
	
	/*private*/getProductionsBlockages(activeProductions){
		let results=[];

		
		let platform=this.parent;
		let resourcesRepository=platform.resourcesRepository;

		let hasInsufficientResource=true;
		let hasWasteFull=true;
		
		foreach(activeProductions,(production,productionName)=>{
			
			// Costs :
			let costs=production.costs;
			let insufficientResourcesList=resourcesRepository.getInsufficientResourcesList(costs);
			if(empty(insufficientResourcesList)){
				hasInsufficientResource=false;
			}

			// Wastes :
			let wastes=production.costs;
			let wastesFullResourcesList=resourcesRepository.getWastesFullResourcesList(wastes);
			if(empty(wastesFullResourcesList)){
				hasWasteFull=false;
			}
			
		});
		
		
		if(hasInsufficientResource)	results.push("insufficientResource");
		if(hasWasteFull)						results.push("wasteFull");
		
		
		return results;
	}
	
	// NEW
	/*private*/hasProductionHappening(){
		
		let activeProductions=this.getActiveProductions();
		if(empty(activeProductions))	return false;
		
		let productionsBlockages=this.getProductionsBlockages(activeProductions);
		if(!empty(productionsBlockages))	return false;

		return true;
	}
	
	/*private*/getEfficacityFactor(){
		
		let selfParam=this;
		if(!selfParam.productionsEfficacity)	return 1;
		if(!this.isBusy())		return 0;

		let platform=selfParam.parent;
		let train=platform.parent;
		let region=train.parent;
		let daylightManager=region.daylightManager;
		
		let workingTemperatures=selfParam.productionsEfficacity.workingTemperatures;
		if(		  workingTemperatures
				&& !Math.isInMinMax(workingTemperatures, daylightManager.getTemperature(),"[]","=>")){
			return 0;
		}

		let daylightEfficacityFactor=selfParam.productionsEfficacity.daylightEfficacityFactor;
		if(!daylightEfficacityFactor)	return 1;
		
		if(0<daylightEfficacityFactor)	
			return (daylightManager.getLightPercent()*.01) * daylightEfficacityFactor;
		else
			return 1 + ((daylightManager.getLightPercent()*.01) * daylightEfficacityFactor);
//		EQUIVALENT TO :	return 1-((daylightManager.getLightPercent()*.01) * -daylightEfficacityFactor);
	}
	
	

	
	
	
	/*OVERRIDES*//*public*/isBusy(){
		return this.hasProductionHappening(this);
	}
	
	
	// -----------------------------------------------------

	
	/*public*/canDrawSmokePits(){
		return this.isBusy();
	}

	
	
	
	/*private*/drawEfficacityUI(ctx,camera){
		
		const gameConfig=this.gameConfig;
		if(contains(gameConfig.projection,"2D")){

			let productionsEfficacity=this.productionsEfficacity;
			if(			!productionsEfficacity 
					|| (!productionsEfficacity.workingTemperatures && !productionsEfficacity.daylightEfficacityFactor) ) 	return;
			
			
			let efficacityFactor=this.getEfficacityFactor();

			const zooms=gameConfig.zooms;

			// CAUTION ! Coordinates for DRAWING are not the same than for CALCULUS !
			const drawableUnzoomedCoordinates=getDrawableZoomedIfNecessaryCoordinates2D(gameConfig,this.position.getParentPositionOffsetted(),camera);
			const x=drawableUnzoomedCoordinates.x+38;
			const y=drawableUnzoomedCoordinates.y-60;
			
	
			let percentageStr=i18n("RDT")+":"+Math.round(efficacityFactor*100)+" % "
											 +(this.isBusy()?PLAY_GLYPH:PAUSE_GLYPH); // ⚙ ⭮ ⏸
			
			drawSolidText(ctx,x,y,percentageStr,"#FFFFFF",14,"helvetica","bold",zooms);
			
		}
	
	}
	
	/*overrides*/draw(ctx,camera,lightsManager){
	
//		let platform=selfParam.parent;
//		let train=platform.parent;
		if(		 this.isBusy()
//				// Only for selectable trains : (ie. game is effectively started)
//				&& train.isSelectable()
				){
			this.buildingAnimation.draw(ctx,camera,lightsManager);	
		}
		
		super.draw(ctx,camera,lightsManager);


	}
	
	
	drawInUI(ctx,camera){

		let thumbImagePosition=super.drawInUI(ctx,camera);

		// Efficacity displaying :
		this.drawEfficacityUI(ctx,camera);
		
		
		// Indicators under the thumbnail, 
		// if there are no resources, or if wastes are full
		// for an active production :
		if(thumbImagePosition){
			let activeProductions=this.getActiveProductions();
			if(!empty(activeProductions)){
				let productionsBlockages=this.getProductionsBlockages(activeProductions);
				if(!empty(productionsBlockages)){
					
					const gameConfig=this.gameConfig;
					const zooms=gameConfig.zooms;
					
					foreach(productionsBlockages,(productionBlockage,index)=>{
						let x=thumbImagePosition.x+this.gameThumb.size.w*.5+index*20;
						let y=thumbImagePosition.y+this.gameThumb.size.h+5;
						if(productionBlockage=="insufficientResource"){
							drawSolidText(ctx,x,y,"!R!",null,null,null,null,zooms);
						}else if(productionBlockage=="wasteFull"){
							drawSolidText(ctx,x,y,"!W!",null,null,null,null,zooms);
						}
					});
				}				
			}
			
		}
		
		
	}
	
	
	
}
