class Region{

	constructor() {

		this.isVisible=true;
		this.weather=null;
		
//	test for portrait variables :	this.gru="ijtaj";
		
//	IMPLICIT:	this.position=null;

	}

	// INIT

	// METHODS
	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	
	getDrawables(){
		return [ this.grandeLigneQuestsManager,this.daylightManager,
						 this.trains, this.doodles, this.stations];
	}
		
	removeTrain(trainToRemove){
		remove(this.trains,trainToRemove);
	}
	
	
	/*private*/getMainTrain(){
		return this.trains[0];
	}

	
	/*private*/getClosestNextStationDeltaXInformation(position, objectWidth){
		
		let closestStationAndDelta={delta:null, station:null};
		foreach(this.stations,(station)=>{
//		let stationX=Math.floor(station.position.x+station.size.w/2); // SHIFTED TO RIGHT
			let stationX=Math.floor(station.position.x);
//		let objectX=Math.floor(position.x+objectWidth/2); // SHIFTED TO RIGHT
			let objectX=Math.floor(position.x);
			let deltaX=stationX-objectX;
			if(deltaX<=0)		return "continue";
			let minDeltaX=closestStationAndDelta.deltaX;
			if(minDeltaX==null || deltaX<minDeltaX) {
				closestStationAndDelta.deltaX=deltaX;
				closestStationAndDelta.station=station;
			}
		});
		if(!closestStationAndDelta.station)	return null;
		return closestStationAndDelta;
	}
	
	
	
	
	// For dialogs and quests :
	
	// A MANDATORY METHOD :
	/*public*/getDialogs(){
		return this.gameDialogs;
	}
	
	
	/*public*/startScenario(){
		
//		// We start the region's dialogs, if needed :
////	let self=this;
//		foreach(this.getDialogs(),(dialog)=>{
////		dialog.start(self);
//			dialog.startStartable();
//		});

		
//		// DBG
//		window.eranaScreen.startAllStartables();

		
		
		// IMPORTANT : On today, quests are only started from dialogs !!
//	this.grandeLigneQuestsManager.startAllStartables();
		
		
	}
	
	/*public*/getGameQuestsManager(){
		return this.grandeLigneQuestsManager;
	}
	
	
	
	
	
	
	
	// FOR DIALOGS :

	
	/*public*/isElementSelected(selfParam, camera, prototypeNameRadical){
		const selectedObject=window.eranaScreen.selectionManager.getSelected();
		if(!selectedObject || !selectedObject.prototypeName)	return false;
		return containsIgnoreCase(selectedObject.prototypeName,prototypeNameRadical);
	}
	
	/*public*/trainHasReachedPosition(selfParam, camera, positionX){
		let train=selfParam.getMainTrain();
		let result=train.hasReachedPosition(positionX);
		return result;
	}
	
	
	/*public*/getErgostasioGaugeValue(selfParam, camera, args){
		let train=selfParam.getMainTrain();
		let ergostasio=train.getErgostasios()[0];
		return ergostasio.grandeLigneGauge.gaugeValue;
	}
	
	
	
//	// UNUSED (FOR NOW)
//	/*public*/isAttributeEqualTo(selfParam, camera, args){
//		let attr=selfParam[args.attrName];
//		if(typeof(attr)=="undefined")	return false;
//		return attr===args.value;
//	}

	
	
	// FOR QUESTS :

	
	
	
	/*public*/getAlivePeoplesNumber(selfParam, camera, args){
		let train=selfParam.getMainTrain();
		
		let result=0;
		
		foreach(train.getPeoples(),(people)=>{
			result++;
		},(p)=>{	return p.isAlive();	});
		
		return result;
	}
	
	// TODO : FIXME : Use the «REACHES» condition in scripts for dialogs instead :
	/*public*/hasAtLeastAmountEquipmentsInMainTrain(selfParam, camera, args){
		return args.amount<selfParam.getEquipmentsNumberInMainTrain(selfParam, camera, args);
	}
	
	/*public*/getEquipmentsNumberInMainTrain(selfParam, camera, args){
		let train=selfParam.getMainTrain();
		return train.getEquipmentsOfType(args.partialPrototypeName, args.equipmentType).length;		
	}

	
	// TODO : FIXME : Use the «REACHES» condition in scripts for dialogs instead :
	// UNUSED (for now)
	/*public*/hasAtLeastAmountAvailableResourceInCurrentStation(selfParam, camera, args){
		return args.amount<selfParam.getAvailableResourceInCurrentStation(selfParam, camera, args.resourceName);
	}
	
	/*public*/getAvailableResourceInCurrentStation(selfParam, camera, args){
		let train=selfParam.getMainTrain();
		if(!train)	return null;
		let collidedStation=train.getCollidedStation();
		if(!collidedStation)	return null;
		
		let resourceName=args.resourceName;
		return collidedStation.getResourceAmount(resourceName);
	}
	
	
	/*public*/isTrainInStation(selfParam, camera, args){
		
		let mustBeInStation=args.mustBeInStation;
		let train=selfParam.getMainTrain();
		let result= (!!train.getCollidedStation());
		return (mustBeInStation?result:!result);
	}
	

	

	
	// =============================================
	

	draw(ctx,camera,lightsManager){
		/*DO NOTHING*/
		
		
		// TODO : FIXME : CENTRALIZE STEPPING ! (in gameScreen in the main loop)
		// Scripts stepping :
		let self=this;
		foreach(this.getDialogs(),(dialog)=>{
			dialog.doStep(camera);
		});
		
	}
	
}
