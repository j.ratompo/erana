
class RollingPlatform{

	constructor() {
		
		this.isVisible=true;
		this.isMovable=true;

		this.position=new Position(this);
		
		this.img=new GameImage(this).addExtensionImage("pending");
		
//		this.buildStarted=false;
		

		this.constructionManager=null;
		
		this.activeJobManager=null;
		
		this.pending=false;		

	}
	
	
	// INIT

	init(gameConfig){
		
		this.gameConfig=gameConfig;
		
		this.position.center=this.imageConfig._2D.center;
		
		this.getGameImage().init(gameConfig);
		
		
		this.passageWayImg=loadImageInUniverse(this.gameConfig,this.passagewaySrc,this);
		

		this.constructionManager=new ConstructionManager(this,gameConfig);
		
		

	}
	
	
//	initWithParent(){
//		this.resourcesRepository.initWithParent();
//	}

	
	/*public*/getConstructionManager(){
		return this.constructionManager;
	}
	
	
	// METHODS
	getGameImage(){
		return this.img;
	}
	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}

	
	getIsMovable(){
		return this.isMovable;
	}
	setIsMovable(isMovable){
		this.isMovable=isMovable;
	}
	

	doOnClick(){
		/*DO NOTHING*/
		// (Only used to make this element clickable)
	}
	
	isSelectable(){
		return this.selectable!==false; // CAUTION : By default, it is selectable !
	}

	isClickable(){
		return this.clickable!==false; // CAUTION : By default, it is clickable !
	}

	
	getInfos(){
		return this.infos;
	}
	
	// =================== GAME RULES METHODS ===================
	
	callErgostasio(event, speedParam){
		
		let train=this.getTrain();

		if(this.getErgostasioAtPlatformPosition()){
			// MESSAGE
			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("uneErgostasioEstDéjàACetEndroit"));
			return;
		}
		
		// We call the closest and non-busy ergostasio :
		let ergostasio=train.getClosestNonBusyErgostasio(this.position);
		if(!ergostasio){
			// MESSAGE
			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("aucuneErgostasioDeLibrePourLeMoment"));
			return ;
		}
		
		
		if(0<train.isErgostasioMovementBlockedTillPosition(ergostasio,this.position)){
			// MESSAGE
			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("uneErgostasioEstDisponibleMaisNePeutPasAtteindreCettePlateforme"));
			return;
		}
		
		let destination={x:this.position.x};
		ergostasio.moveToDestination(destination,speedParam);
	}
	
	
	
	buildBasics(event,buildsConfigs){
		this.plantBuildingJob(event,buildsConfigs);
	}
		
	buildProductions(event,buildsConfigs){
		this.plantBuildingJob(event,buildsConfigs);
	}
	
	buildOverEquipments(event,buildsConfigs){
		this.plantBuildingJob(event,buildsConfigs,true);
	}
	
	/*private*/plantBuildingJob(event,buildsConfigs,isOverEquipment=false){
		
		if(!this.canStartBuild(isOverEquipment))	return;
		
		
		const platform=this;
		const train=platform.parent;
		const position=this.getPosition();
		
		// We set the current job manager as the construction : 
		this.activeJobManager=this.getConstructionManager();
		
		const self=this;
		// 1)
		this.activeJobManager.promptConstructions(buildsConfigs,train,position,platform).then((args)=>{
				const protoToBuild=args.result;
				//2)
				self.activeJobManager.start(platform, protoToBuild);
		});
		
	}
	
	

	
	/*private*/canStartBuild(isOverEquipment){
		
//		if(this.buildStarted){
//			// MESSAGE
//			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("buildAlreadyStarted"));
//			return false;
//		}
		
		if(!isOverEquipment){
			if(this.getDockableEquipment()){
				// MESSAGE
				window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("buildAlreadyInstalled"));
				return false;
			}
		}else{
			if(this.getOverEquipment()){
				// MESSAGE
				window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("overAlreadyInstalled"));
				return false;
			}
		}
		
		return true;
	}
	
	
	
	
	
	/*private*/getTrain(){
		return this.parent;
	}
	
	/*private*/getErgostasioAtPlatformPosition(){
		let train=this.getTrain();
		return train.getErgostasioAtPosition(this.position);
	}
	
	/*private*/getErgostasiosAtPlatformPosition(){
		let result=[];
		let train=this.getTrain();
		let foundErgostasio=train.getErgostasioAtPosition(this.position);
		if(foundErgostasio)
			result.push(foundErgostasio);
		if(!this.isLastPlatform()) 
			return result;
		// If is last platform :
		const otherErgostasioPastLastPlatform=train.getErgostasioPastLastPosition();
		if(otherErgostasioPastLastPlatform)			
			result.push(otherErgostasioPastLastPlatform);
		return result;
	}	
	

	
	/*public*/hasEquipmentOfClassName(className){
		let dockableEquipment=this.getDockableEquipment();
		return (dockableEquipment && containsIgnoreCase(getClassName(dockableEquipment),className));
	}
	
	/*public*/retrieveResource(event, resourceName){
		/*DO NOTHING (: read-only field !)*/
//		let isChecked=!!event.target.checked; /*(forced to boolean)*/
//		this.resourcesRepository.setStockRetrieving(resourceName,isChecked);
////	this.getTrain().giveRetrievingOrders(this,resourceName);
	}
	
	/*public*/isRetrieved(resourceName){
		return this.resourcesRepository.isStockRetrieving(resourceName);
	}
	
	
	/*public*/getGivingResources(
//			resourceInNeedNameFilter=null
			){
		return this.resourcesRepository.getGivingResources(
//				resourceInNeedNameFilter
				);
	}
	
	
	
	
	/*public*/getNeedingResources(resourceInNeedNameFilter=null, ignoreRetrievingDirective=false){
		return this.resourcesRepository.getNeedingResources(resourceInNeedNameFilter,ignoreRetrievingDirective);
	}
	
	
	
	getAmountAndTotal(resourceName){
		return this.resourcesRepository.getAmountAndTotal(resourceName);
	}
	
	
	
	// TODO : FIXME : Develop an use position.collidesWith(...) instead :
	/*public*/isOnPosition(pos){
		let demiWidth=Math.round(this.size.w/2);
		return this.position.x-demiWidth<pos.x && pos.x<=this.position.x+demiWidth;
	}
	
	/*public*/getDockableEquipment(){
		// TODO : FIXME : use this.dockableEquipments[0] instead :
		return this.dockableEquipment;
	}
	/*public*/getOverEquipment(){
		// TODO : FIXME : use this.dockableEquipments.length>1?this.dockableEquipments[1]:null instead :
		return this.overEquipment;
	}
	/*public*/setDockableEquipment(equipment){
		// TODO : FIXME : use if(this.dockableEquipments.length==0)	this.dockableEquipments.push(equipment) instead :
		this.dockableEquipment=equipment;
	}
	/*public*/setOverEquipment(equipment){
		// TODO : FIXME : use if(this.dockableEquipments.length==1)	this.dockableEquipments.push(equipment) instead :
		this.overEquipment=equipment;
	}
	
	
	/*private*/isFirstPlatform(){
		let train=this.getTrain()
		return (0===this.position.x);
	}
	
	/*private*/isLastPlatform(){
		let train=this.getTrain()
		return (this.position.x===train.getLastPlatform().position.x);
	}
	
	
	
	/*public*/getNeededCostsNames(){
		
		const train=this.getTrain();
	
		let costsNames=[];
		
		// First, we look if there's no needed resources according to the construction manager :
		if(this.hasRunningActiveJobManager()){
			if((this.getDockableEquipment() && this.getDockableEquipment().isPending())
			 ||(this.getOverEquipment() && this.getOverEquipment().isPending()))
			 		return this.activeJobManager.getNeededCostsNames();
			costsNames=this.activeJobManager.getNeededCostsNames();
		}
		
		// Another special case : if we have an ergostasio nearby building a platform !
		if(this.isLastPlatform() && !this.isPending()){
				const otherErgostasioPastLastPlatform=train.getErgostasioPastLastPosition();
				if(otherErgostasioPastLastPlatform){
					if(otherErgostasioPastLastPlatform.getActiveJobManager() && otherErgostasioPastLastPlatform.getActiveJobManager().isRunning()){
						costsNames=merge(costsNames, otherErgostasioPastLastPlatform.getNeededCostsNames());
					}
				}
		}
		
		
		
		let stationProvidedResources=[];
		let collidedStation=train.getCollidedStation();
		if(collidedStation){
			// If train is at a station, then all the corresponding resources in station are marked as «retrieved» in all platforms !
			stationProvidedResources=collidedStation.getGivingCostsNames();
			costsNames=merge(costsNames,stationProvidedResources);
		}

		// NOT SURE ABOUT THAT :
//		if(!collidedStation || empty(stationProvidedResources)){
			
			// Normal retrieving behavior :
		
		let equipment=null;
		
		
		// TODO : FIXME : use if(this.dockableEquipments.length==0)	this.dockableEquipments.push(equipment) instead :
		equipment=this.getDockableEquipment();
		if(equipment && equipment.getNeededCostsNames){
			costsNames=merge(costsNames,equipment.getNeededCostsNames());
		}
		
		
		// TODO : FIXME : use if(this.dockableEquipments.length==1)	this.dockableEquipments.push(equipment) instead :
		equipment=this.getOverEquipment();
		if(equipment && equipment.getNeededCostsNames){
			costsNames=merge(costsNames,equipment.getNeededCostsNames());
		}


		let ergostasios=this.getErgostasiosAtPlatformPosition();
		if(ergostasios){
			foreach(ergostasios,(ergostasio)=>{
				costsNames=merge(costsNames,ergostasio.getNeededCostsNames());
			});
			
		}
		
//	}
		
		return costsNames;
	}
	
	
	/*public*/hasInsufficientResources(){
		return this.activeJobManager.hasInsufficientResources(this);
	}

	/*public*/getResourcesRepository(){
		return this.resourcesRepository;
	}
	
	
	/*public*/getPosition(){
		return this.position;
	}
	
	/*public*/setPending(pending){
		this.pending=pending;
	}
	
	/*public*/isPending(){
		return this.pending;
	}	

	
	/*public*/getPositionNextToIt(){
		return {x:this.position.x-this.size.w, y:this.position.y};
	}
	
	
	
	/*public*/isClickable(){
		return !this.isPending();
	}
	
	/*public*/removeFromParent(){
			this.parent.removePlatform(this);
	}
	
	/*public*/removeDockableEquipment(){
		// TODO : FIXME : use this.dockableEquipments.length>1?this.dockableEquipments[1]:null instead :
		this.dockableEquipment=null;
	}
	
	/*public*/removeOverEquipment(){
		// TODO : FIXME : use this.dockableEquipments.length>1?this.dockableEquipments[1]:null instead :
		this.overEquipment=null;
	}
	
	/*private*/hasRunningActiveJobManager(){
		return this.activeJobManager && this.activeJobManager.isRunning();
	}
	
	/*public*/hasAvailableWorkPositions(){
		if(!this.hasRunningActiveJobManager())	return false;
		return this.activeJobManager.hasAvailableWorkPositions();
	}
	
	/*public*/addWorkerPeople(people){
		if(!this.hasRunningActiveJobManager())	return;
		this.activeJobManager.addWorkerPeople(people);
	}
	
//	/*public*/removeCurrentlyActuallyWorkingPeople(people){
//		if(!this.hasRunningActiveJobManager())	return;
//		this.activeJobManager.removeCurrentlyActuallyWorkingPeople(people);
//	}
	
	
	
	getDrawables(){
		return [
			this.resourcesRepository,
			this.getDockableEquipment(),
			this.getOverEquipment(),
			this.wheelsSets,
			this.gameSunShadow
		];
	}
	
	
	
	// -----------------------------------------------------
	
	/*public*/canDraw(camera){
		const img=this.getGameImage();
		return img && img.canDraw(camera);
	}
	
	
	drawInUI(ctx,camera){

		if(!this.canDraw(camera))	return false;
		
		let gameConfig=this.gameConfig;
		if(contains(gameConfig.projection,"2D")){
							
			if(!this.activeJobManager || !this.activeJobManager.isRunning()/*excludes case ==0*/)	return false;
		
			this.activeJobManager.drawInUI(ctx,camera,this.getPosition());		
		}
		
	}

	
	draw(ctx,camera,lightsManager){
		let gameImage=this.getGameImage();
		if(!gameImage.canDraw(camera))	return false;
		
		if(this.activeJobManager)	this.activeJobManager.doStep();
		
		if(!this.isPending()){
			gameImage.draw(ctx,camera,lightsManager);
			gameImage.drawSimpleImage(ctx,camera,this.passageWayImg);
		}else{
			// Pending platform special drawing :
			foreach(this.wheelsSets,(wheelsSet)=>{
				wheelsSet.draw(ctx,camera,lightsManager);
			});
			gameImage.drawExtensionImage(ctx,camera,"pending",.5);
		}
		
		return true;
	}


	
}
