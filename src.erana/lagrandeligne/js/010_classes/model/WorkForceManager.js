
class WorkForceManager{
	
	constructor(parent,gameConfig){
		
		this.parent=parent;
		this.gameConfig=gameConfig;
		
		this.affectedPeoples=[];
		this.currentlyWorkingPeoples=[];
		
		this.workersCountMaxCapacity=null;
	}
	
	
	/*public*/setWorkersCount(train,value,maxWorkers){
		
		// In no change:
		const currentWorkersCount=this.getAffectedWorkersCount();
		if(value==currentWorkersCount)		return;
		
		if(this.workersCountMaxCapacity==null)	this.workersCountMaxCapacity=maxWorkers;
		
		const deltaCount=Math.abs(currentWorkersCount-value);
		if(value<currentWorkersCount){

			if(empty(this.affectedPeoples))	return;

			// If we decrease the number of workers :
			const workersToUnaffect=Math.getRandomsInArray(this.affectedPeoples, deltaCount);
			
			this.removeCurrentlyWorkingPeoplesAndAffectations(workersToUnaffect);

			
		}else{

			const availablePeoplesForWork=train.getAvailablePeoplesForWork();
			if(empty(availablePeoplesForWork))	return;
			
			// If we increase the number of workers :
			const workersToAffect=Math.getRandomsInArray(availablePeoplesForWork,deltaCount);
			
			this.setPeoplesAffectations(workersToAffect);
			
			// We do not set them as currently working on the job, for they may have not reached the workplace yet.
		}

	}
	
	
	/*public*/addWorkerPeople(people){
		// At this point this worker should already have the right affectation :
		if(!this.hasAvailableWorkPositions())	return;
		this.currentlyWorkingPeoples.push(people);
	}
	
	
	/*public*/removeAllWorkersAndAffectations(){

		// DBG
		//lognow("1removeAllWorkersAndAffectations "+this.currentlyWorkingPeoples.length+" "+this.affectedPeoples.length);

		const self=this;
		foreach(this.currentlyWorkingPeoples,(people)=>{
			// NO : BECAUSE REMOVING AN ITEM FROM AN ARRAY ON WHICH ITERATING PRODUCES UNPREDICTABLE BEHAVIOR : self.removeCurrentlyActuallyWorkingPeople(p);
				people.setWorkPlacePlatform(null);
		});
		this.currentlyWorkingPeoples=[];
		
		foreach(this.affectedPeoples,(people)=>{
			// NO : BECAUSE REMOVING AN ITEM FROM AN ARRAY ON WHICH ITERATING PRODUCES UNPREDICTABLE BEHAVIOR : self.setPeoplesAffectationsToDefault(p);
				people.setJobType("transporter");
		});
		this.affectedPeoples=[];
		
		// DBG
		//lognow("2removeAllWorkersAndAffectations "+this.currentlyWorkingPeoples.length+" "+this.affectedPeoples.length);
				
	}
	
	/*private*/setPeoplesAffectations(workersToAffect, jobType="builder"){
			const self=this;
			foreach(workersToAffect,(people)=>{
				people.setJobType(jobType);
				self.affectedPeoples.push(people);
				
			},(p)=>p.isAlive());
	}
	
	/*private*/removeCurrentlyWorkingPeoplesAndAffectations(workersToUnaffect){
		const self=this;
		foreach(workersToUnaffect,(people)=>{
			self.removeCurrentlyActuallyWorkingPeople(people);
			self.setPeoplesAffectationsToDefault(people);
		},(p)=>p.isAlive());

	}
	
	/*private*/removeCurrentlyActuallyWorkingPeople(people){
		if(empty(this.currentlyWorkingPeoples))	return;
		people.setWorkPlacePlatform(null);
		remove(this.currentlyWorkingPeoples,people);
	}
	
	/*private*/setPeoplesAffectationsToDefault(people){
		people.setJobType("transporter");
		remove(this.affectedPeoples, people);
	}				
	
	
	
	
	/*public*/getAffectedWorkersCount(){
		return this.affectedPeoples.length;
	}	



	/*public*/hasAvailableWorkPositions(){
		return (this.currentlyWorkingPeoples.length<this.getAffectedWorkersCount());		
	}
	
	
	/*private*/getWorkForceRate(){
		return (this.currentlyWorkingPeoples.length/this.workersCountMaxCapacity);
	}
	
	/*public*/hasWorkersWorkingOnJob(){
		return (!empty(this.currentlyWorkingPeoples) && this.workersCountMaxCapacity);
	}
	
	
	
	/*public*/getReducedCostsSteps(costsStepsParam){
		if(!this.hasWorkersWorkingOnJob())	return null;
		const costsStepsResults={};
		const workForceRate=this.getWorkForceRate();
		foreach(costsStepsParam,(costAmount, costName)=>{
			costsStepsResults[costName]=workForceRate*costAmount;
		});
		return costsStepsResults;
	}

	/*public*/getReducedPercentageStep(calculatedPercentageStepParam){
		if(!this.hasWorkersWorkingOnJob())	return calculatedPercentageStepParam;
		const workForceRate=this.getWorkForceRate();
		const percentageStepResult=workForceRate*calculatedPercentageStepParam;
		return percentageStepResult;
	}


}

