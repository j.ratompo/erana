const STATION_X_START_BASICS=74;
const STATION_X_START_ORES=370;
const STATION_X_SPACING=15;
const STATION_Y_START=67;
		
class Station{

	constructor() {

		this.isVisible=true;

		this.position=new Position(this);

		this.img=new GameImage(this);

	}
	
	
	// INIT

	init(gameConfig){
		
		this.gameConfig=gameConfig;

		this.position.center=this.imageConfig._2D.center;
		
		this.getGameImage().init(gameConfig);

	}

//	initWithParent(){
//		this.resourcesRepository.initWithParent();
//	}
	
	// METHODS
	getGameImage(){
		return this.img;
	}

	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	
	
//	getDrawables(){
//		// NO : This is a special case, because ths drawable child is only to be drawn in the back :
////	return [this.resourcesRepository];
//		return [this.extractors];
//	}
	
	
	/*public*/getGivingResources(
//			resourceInNeedName=null
			){
		return this.resourcesRepository.getGivingResources(
//				resourceInNeedName
				);
	}
	
	// UNUSED
	/*public*/getNeededCostsNames(){
//		let costsNames=[];
//		foreach(this.resourcesRepository.resources,(resource,resourceName)=>{
//			costsNames.push(resourceName);
//		},(resource,resourceName)=>{	return 0<resource.amount;	});
//		return costsNames;
		// For a station, all its resources are giving :
		return [];	
	}
	
	/*public*/getGivingCostsNames(){
		// For a station, all its resources are giving :
		let costsNames=[];
		foreach(this.resourcesRepository.resources,(resource,resourceName)=>{
			costsNames.push(resourceName);
		},(resource,resourceName)=>{	return 0<resource.amount;	});
		return costsNames;
	}
	
	
	
	/*public*/getResourceAmount(resourceName){
		return this.resourcesRepository.getAmountAndTotal(resourceName).amount;
	}
	

	// -----------------------------------------------------

	
	
	drawResourcesLevels(ctx,camera){
		
		const gameConfig=this.gameConfig;
		const zooms=gameConfig.zooms;
					
		let width=this.size.w;
		let height=this.size.h;

		let resources=this.resourcesRepository.resources;
		
		let realCoords=this.getGameImage().getRealImageCoords(camera);
		
		const xStep=48;
		const yStep=10;
		const yLimit=0;

		// Basic resources :
		{
			let x=(realCoords.x-Math.floor(width/2)+STATION_X_START_BASICS);
			let y=(realCoords.y-height+STATION_Y_START);
		
			
			// Empty flag 1 :
			let allBasicsDepleted=true;
			foreach(resources,(r)=>{
				if(0<r.amount) {
					allBasicsDepleted=false;
					return "break";
				}
			},(r)=>{	return r.stationPlace===1;	});
			if(allBasicsDepleted){
				drawSolidText(ctx,x-22, y,"X","#DD0000",null,null,null,zooms);
				drawSolidText(ctx,x+170, y,"X","#DD0000",null,null,null,zooms);
			}
			
			
			let xOff=0;
			let yOff=0;
			foreach(resources,(r)=>{
	
				let color=(r.amount<=0?"#DD2222":nonull(r.color,"#888888"));
				
				drawSolidText(ctx,x+xOff, y+yOff,r.name+":"+r.amount,color,null,null,null,zooms);
				
				if(yLimit <= yOff){
					xOff+=xStep+STATION_X_SPACING;
					yOff=0;
				}else{
					yOff+=yStep;
				}
				
				
			},(r)=>{	return r.stationPlace===1;	});
		
		


		}
		

		// Ores resources :
		{
			
			let x=(realCoords.x-Math.floor(width/2)+STATION_X_START_ORES);
			let y=(realCoords.y-height+STATION_Y_START);
			
			// Empty flag 2 :
			let allOresDepleted=true;
			foreach(resources,(r)=>{
				if(0<r.amount) {
					allOresDepleted=false;
					return "break";
				}
			},(r)=>{	return r.stationPlace===2;	});
			if(allOresDepleted){
				drawSolidText(ctx,x-22, y,"X","#DD0000",null,null,null,zooms);
				drawSolidText(ctx,x+170, y,"X","#DD0000",null,null,null,zooms);
			}

		
			let xOff=0;
			let yOff=0;
			foreach(resources,(r)=>{
	
				let color=(r.amount<=0?"#DD2222":nonull(r.color,"#888888"));
				drawSolidText(ctx,x+xOff, y+yOff,r.name+":"+r.amount,color,null,null,null,zooms);
				
				if(yLimit <= yOff){
					xOff+=xStep+STATION_X_SPACING;
					yOff=0;
				}else{
					yOff+=yStep;
				}
				
				
			},(r)=>{	return r.stationPlace===2;	});

			
		}

		
		
	}
	

	draw(ctx,camera,lightsManager){
		
//	if(!this.getGameImage().draw(ctx,camera,lightsManager))	return false;
		if(!this.getGameImage().draw(ctx,camera,lightsManager,1,false,false,
				// SPECIAL CASE : We don't want the rays casting for this object !
				true))	return false;
		
		this.drawResourcesLevels(ctx,camera);
		
		// This is a special case, because this drawable child is only to be drawn in the back :
		this.resourcesRepository.draw(ctx,camera);

		return true;
	}

	drawInBack(ctx,camera,lightsManager){
		if(!this.getGameImage().drawInBack(ctx,camera,lightsManager)) return false;
		
		// OLD :
		// (Now we display the resources at the front :)
//	// This is a special case, because this drawable child is only to be drawn in the back :
//	this.resourcesRepository.draw(ctx,camera);
		
		return true;
	}
	
}
