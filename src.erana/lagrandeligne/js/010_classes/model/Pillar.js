
class Pillar{

	constructor() {

		this.isVisible=true;
		this.position=new Position(this);

		this.isMovable=true;

		this.img=new GameImage(this);
		
	}
	
	
	// INIT

	init(gameConfig){
		
		this.gameConfig=gameConfig;

		this.position.center=this.imageConfig._2D.center;
		
		this.getGameImage().init(gameConfig);
		
		
	}
	
	// METHODS
	
	getGameImage(){
		return this.img;
	}
	

	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	
	
	getIsMovable(){
		return this.isMovable;
	}
	setIsMovable(isMovable){
		this.isMovable=isMovable;
	}
	
	
//	// SPECIAL CASE
//	drawInBack(ctx,camera,lightsManager){
//		if(!this.getGameImage().draw(ctx,camera,lightsManager))	return false;
//		return true;
//	}

	draw(ctx,camera,lightsManager){
		if(!this.getGameImage().draw(ctx,camera,lightsManager))	return false;
		return true;
	}

}
