
class GrandeLigneGauge extends GameGauge{

	constructor() {
		super();
		
		this.started=false;
		
	}

	// INIT
	
	initWithParent(){
		this.started=true;
		
	}

	// METHODS
	

	
	/*public*/slowlyDamage(daylightManager){

		if(!this.started)	return;
		
		if(!super.isAlive())	return;

		let wearStep=this.normalWear;
		if(!wearStep/*includes case ==0*/)	return;
		
		let durationTimeFactor=window.eranaScreen.getDurationTimeFactor();
		if(!this.conditionTime || hasDelayPassed(this.conditionTime, this.wearRateMillis*durationTimeFactor)){
			this.conditionTime=getNow();
			
			
			let temperature=daylightManager.getTemperature(
//				SIMPLIFIED:	this.parent.position
				);
			
			let workingTemperaturesMin=this.workingTemperatures.min;
			let workingTemperaturesMax=this.workingTemperatures.max;
			if(temperature<=workingTemperaturesMin){
				//wearStep*= this.wearFactorPastLimits.min* (this.workingTemperatures.min-temperature);
				wearStep=nonull(this.underheatWear,wearStep);
			}else if(workingTemperaturesMax<=temperature){
				//wearStep*= this.wearFactorPastLimits.max* (temperature-this.workingTemperatures.max);
				wearStep=nonull(this.overheatWear,wearStep);
			}
			
			
			super.damageCondition(wearStep);
			
			
//		// DBG
//		console.log("this.gaugeValue:"+this.gaugeValue+" "+temperature+"°C");
			
		}
		
		
	}
	
	
	
	

}
