const UI_TEMPERATURE_FIXED_RIGHT=-114;
const UI_TEMPERATURE_FIXED_TOP=20;

const UI_TEMPERATURE_DEFAULT_COLOR="#AAAAAA";
const UI_TEMPERATURE_OVERHEAT_COLOR="#FF0000";
const UI_TEMPERATURE_UNDERHEAT_COLOR="#00FFFF";

class DaylightManager{


		constructor() {
			
			this.isVisible=true;
			
			this.isMovable=true;
			
			this.mover=getMoverMonoThreadedNonStop(this);
			
			this.speed=0;
			
			this.images=[];


		}

		// INIT
		init(gameConfig){
			this.gameConfig=gameConfig;
			
			let self=this;
			foreach(this.imageConfig._2D.horizontal,(imageConf,key)=>{
				let image=self.loadImage(imageConf.imageSrc);
				let splits=Math.getMinMax(key,"=>");
				let min=splits.min;
				let max=splits.max;
				let peekFactor=imageConf.peekFactor;
				self.images.push({lightPercentMin:min,lightPercentMax:max,image:image,peekFactor:peekFactor});
			});
			
			
			// Wrapping handling :
			this.getMover().setWrapping(this.gameConfig.wrapping).setDurationTimeFactorHolder(window.eranaScreen);

			
		}
		
		

		// METHODS
		
		/*private*/loadImage(imageOnlySrc){
			let asyncId=window.eranaScreen.addAsyncStart();
			return loadImageInUniverse(this.gameConfig,imageOnlySrc,this,null,()=>{
        window.eranaScreen.addAsyncEnd(asyncId);
			});
		}
		
		
		
		
		
		getIsVisible(){
			return this.isVisible;
		}
		setIsVisible(isVisible){
			this.isVisible=isVisible;
		}
		
		

		getIsMovable(){
			return this.isMovable;
		}
		setIsMovable(isMovable){
			this.isMovable=isMovable;
		}
		
		
		getMover(){
			return this.mover;
		}
		
		startMovingNonStop(followers){
			
			this.speed=this.nonStopSpeed;
			this.getMover().doStartNonStop({x:this.speed},followers);
		}
		
		move(){
			// Mover stepping :
			// Only for monothreaded movers :
			this.getMover().doStep();
		}
		
		
		/*private*/getAbsoluteDeltaX(
//		SIMPLIFIED:		atPosition=null  : It is quite less realistic, but simpler in the gameplay for the payer !
				){
			
			let region=this.parent;

			// TODO : FIXME : Cela doit dépendre de la sélection en fait ! (pas juste le premier élément de la liste !)
			let train=region.getMainTrain();
			
//		SIMPLIFIED:	const trainPositionX=(atPosition?atPosition.x:train.position.x);
			const trainPositionX=train.position.x;
			
			const daylightManagerPositionX=this.position.x;
			
			let absoluteDirectDistance=Math.abs(trainPositionX-daylightManagerPositionX);
				
			// Wrapping handling :
			let gameConfig=this.gameConfig;
			if(gameConfig.wrapping){
				
				if(gameConfig.wrapping.x) {
					let width=gameConfig.wrapping.x;
					
					let absoluteIndirectDistance=(width-absoluteDirectDistance);
					
					// Daylight effects are only according to the shortest distance to the sun !
					return Math.min(absoluteDirectDistance,absoluteIndirectDistance);
				}
			}
			
			return absoluteDirectDistance;
		}
	

		/*public*/getLightFactor(){
			return this.getLightPercent()*.01;
		}

		
		/*public*/getLightPercent(
//			SIMPLIFIED:	atPosition=null
//				,correctiveFactor=1
				){

			let absoluteDeltaX=this.getAbsoluteDeltaX(
//				SIMPLIFIED:	atPosition
					);

			// Depends on the temperature :
			// Wrapping handling :
			let gameConfig=this.gameConfig;
			if(gameConfig.wrapping){
				
				if(gameConfig.wrapping.x) {
					let width=gameConfig.wrapping.x;
					
					// We have (width/4) for light here, because we modelize the light on a sphere :
					let quarterWidth=(width/4)*nonull(this.lightedHemisphereAreaFactor,1);
					
					// if absoluteDeltaX is greater than the quarter width (since it is an absolute absoluteDeltaX), then we mandatorily are in the shadow zone : 
					if(quarterWidth < absoluteDeltaX) {
						return 0;
					}
					
					//absoluteDeltaX here is mandatorily inferior to quarterWidth:
					
					// if absoluteDeltaX==0, the it should be 100%. Then the more absoluteDeltaX increases, the more the lightPercent decreases 

					// OLD : let result=Math.round((1-(absoluteDeltaX/quarterWidth))*100);

					// We must have a sudden change :
					let result=Math.curve(0, quarterWidth, absoluteDeltaX, "trapeze", nonull(this.duskZoneFactor,0.1)) * 100;
					
					return result;
				}
			}
			
			// case no wrapping :
			const lightPercentFactor=0.01;
			const baseLightPercent=80;
			const minLightPercent=0;
			const maxLightPercent=100;
			
			let baseLightPercentValue= (absoluteDeltaX*lightPercentFactor)+baseLightPercent ;
			return Math.round(Math.coerceInRange(baseLightPercentValue, minLightPercent, maxLightPercent));
		}
		
		
		/*public*/getTemperature(
//			SIMPLIFIED:	atPosition=null
				){
			
			// Actually, the canonical temperature is the one at the beginning of the train.

			
			// CAUTION : light percent calculation and temperature calculation are two separate completly different calculi !!
			
			
			let temperature;
			let totalThermicAmplitude=this.maxTemperature-this.minTemperature;

			
			let gameConfig=this.gameConfig;
			if(gameConfig.wrapping){
				if(gameConfig.wrapping.x) {
					
//				let pointBreakTemperatureInverseCursor=this.maxTemperature-this.pointBreakTemperature;
//				let pointBreakTemperatureIncreaseRatio=(totalThermicAmplitude/pointBreakTemperatureInverseCursor);
					
//				let lightFactor=(this.getLightPercent(atPosition, pointBreakTemperatureIncreaseRatio)*.01);
					
					let lightFactor=(this.getLightPercent(
//						SIMPLIFIED:	atPosition
							)*.01);
					
					temperature=Math.roundTo(((totalThermicAmplitude-this.minTemperature)*lightFactor+this.minTemperature), this.temperatureDecimalPrecision);
					
//					// DBG
//					console.log("lightFactor:"+lightFactor+";temperature:"+temperature);
					
				}
			}else{
				
				let lightFactor=(this.getLightPercent(
//					SIMPLIFIED:	atPosition
						)*.01);
				temperature=Math.roundTo((totalThermicAmplitude*lightFactor+this.minTemperature), this.temperatureDecimalPrecision);
				
			}
			
			
			return temperature;
		}

		
		
		
		
		// ========================================================================================

		// Temperature displaying :
		/*private*/drawTemperatureUI(ctx,camera){
			
			let region=this.parent;
			// TODO : FIXME : Cela doit dépendre de la sélection en fait ! (pas juste le premier élément de la liste !)
			let train=region.getMainTrain();
			
			// Only for selectable trains : (ie. game is effectively started)
			if(	 !train
				|| !train.isSelectable())
				return;
			
			const gameConfig=this.gameConfig;
			const zooms=gameConfig.zooms;
			const resolution=gameConfig.getResolution();

			let temperature=this.getTemperature();
			
			let color=UI_TEMPERATURE_DEFAULT_COLOR;
			let workingTemperaturesMin=this.workingTemperatures.min;
			let workingTemperaturesMax=this.workingTemperatures.max;
			if(temperature<=workingTemperaturesMin){
				color=UI_TEMPERATURE_UNDERHEAT_COLOR;
			}else if(workingTemperaturesMax<=temperature){
				color=UI_TEMPERATURE_OVERHEAT_COLOR;
			}
			
			
			let message=temperature+"°C";
			drawSolidText(ctx,(resolution.w+UI_TEMPERATURE_FIXED_RIGHT),UI_TEMPERATURE_FIXED_TOP,message,color,24,null,null,zooms);
			
			
			// draw light percent :
			if(IS_DEBUG){
				// temperature percent :
				let temperaturePercent=Math.round(this.getLightPercent())+"%";
				drawSolidText(ctx,resolution.w+UI_TEMPERATURE_FIXED_RIGHT,UI_TEMPERATURE_FIXED_TOP+104,temperaturePercent,"#888888",20,null,null,zooms);
				// position
				drawSolidText(ctx,resolution.w+UI_TEMPERATURE_FIXED_RIGHT,UI_TEMPERATURE_FIXED_TOP+124,this.position.x,"#888888",20,null,null,zooms);
				// delta with position of train
				drawSolidText(ctx,resolution.w+UI_TEMPERATURE_FIXED_RIGHT,UI_TEMPERATURE_FIXED_TOP+144,this.position.x-train.position.x,"#888888",20,null,null,zooms);
			}

		}
		


		
		
		// Caution ! The dark layer is drawn lower than the dark images !
		draw(ctx,camera,lightsManager){

			
			// Daylight displaying :
			let lightPercent=this.getLightPercent();
			lightsManager.setLightsPercent(lightPercent);
			
			
		
			
			// Global sun light layouts drawing :
			foreach(this.images,(imageConf)=>{
				ctx.save();
				
				let min=imageConf.lightPercentMin;
				let max=imageConf.lightPercentMax;
				
				let opacity=Math.curve(min,max,lightPercent,"triangle",nonull(imageConf.peekFactor,0.5));
				
				ctx.globalAlpha=opacity;

				let image=imageConf.image;
				drawImageAndCenterWithZooms(ctx,image);

				ctx.restore();
				
			},(imageConf)=>{
				let min=imageConf.lightPercentMin;
				let max=imageConf.lightPercentMax;
				return min<=lightPercent && lightPercent<=max;
			});
			
			
			
			
		}
		
		
		drawInUI(ctx,camera){


			// Temperature displaying :
			this.drawTemperatureUI(ctx,camera);

		}

	
	
}
