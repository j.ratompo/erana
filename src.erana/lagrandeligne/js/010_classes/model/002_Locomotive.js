const IS_DEBUG_LOCOMOTIVE=false;


const ENGINE_WORKING_REFRESH_RATE_MILLIS=200;

class Locomotive extends DockableEquipment{

	
	constructor() {
		super();
		
		this.stopAutomatically=true; // (DEFAULT VALUE)
		
		this.speedingRoutine=getMonoThreadedRoutine(this,ENGINE_WORKING_REFRESH_RATE_MILLIS);
	}
	
	// INIT
	
	init(gameConfig){
		super.init(gameConfig);
		
		// Will vary with the drag (TODO)
		this.maxSpeed=this.nativeMaxSpeed;

		
		this.startSpeedingJob();
		
		
//		if(this.gameSmokePits) {
//			let self=this;
//			foreach(this.gameSmokePits,(s)=>{
//				s.init(self.gameConfig);
//			});
//		}
		
		this.speedingRoutine.setDurationTimeFactorHolder(window.eranaScreen);

	}
	
	// METHODS

	// =================== GAME RULES METHODS ===================
	
	// ---------------------------- Costs of functioning ----------------------------
	/*private*/startSpeedingJob(){
		// We start a speeding job :
		this.speedingRoutine.start();
	}	
	

	/*private*/stopSpeedingJob(){
		this.speedingRoutine.stop();
	}

	// CAUTION : We do not pause nor resume the speeding job : consumptions are calculated from the actual speed :

	/*private*/doOnEachStep(args){
		const selfParam=this;
		
		let train=selfParam.getTrain();
		if(!selfParam.speedsCosts)	return;

		let trainSpeed=train.getSpeed();
		if(trainSpeed===0)	return;

		let trainGoalSpeed=train.getGoalSpeed();
		if(trainGoalSpeed===0)	return;
		
		
		let platform=selfParam.parent;
		let resourcesRepository=platform.resourcesRepository;
		
		
		foreach(selfParam.speedsCosts,(speedCost,speedRangeStr)=>{
			
			if(!contains(speedRangeStr,"=>"))	return "continue";
			if(!Math.isInMinMax(speedRangeStr,trainGoalSpeed,"]]","=>"))	return "continue";

			
			let timeMillis=speedCost.timeSeconds*1000;
			let durationTimeFactor=window.eranaScreen.getDurationTimeFactor();
			if(!speedCost.lastTime || hasDelayPassed(speedCost.lastTime, timeMillis*durationTimeFactor)){
				speedCost.lastTime=getNow();

				let costs=speedCost.costs;
				let insufficientResourcesList=resourcesRepository.getInsufficientResourcesList(costs);
				if(empty(insufficientResourcesList)){

					resourcesRepository.consumeStocks(costs);

				}else{
					
					// MESSAGE
					window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("impossibleDeFonctionnerSansRessources"));
					
					selfParam.setSpeedToZero();
					
				}
				
			}
			
			
		});
		
	}
	
	
	

	/*private*/hasEnoughResourceForSpeeding(newGoalSpeed){

		let train=this.getTrain();
		
		let trainSpeed=train.getSpeed();

		if(!this.speedsCosts)	return true;
		if(trainSpeed===0 && newGoalSpeed===0)		return false;

		let platform=this.parent;
		let resourcesRepository=platform.resourcesRepository;
		
		
		return !!foreach(this.speedsCosts,(speedCost,speedRangeStr)=>{
			
			if(!contains(speedRangeStr,"=>"))	return "continue";
			if(!Math.isInMinMax(speedRangeStr,newGoalSpeed,"]]","=>"))	return "continue";
			
			let costs=speedCost.costs;
			return (!resourcesRepository.hasInsufficientResources(costs));
		});
	}


	// Locomotive
	setSpeed(event,speedParam){
		// UNUSEFUL : Since inputs are deactivated if needed :
//		if(!this.canRestart())			return;
		
		let speed=event.target.value;

		this.setSpeedAndRefreshUI(speed,true);

	}
	
	setSpeedToZero(){
		this.setSpeedAndRefreshUI(0,true);
	}
	
	setDefaultSpeed(){
		// UNUSEFUL : Since inputs are deactivated if needed :
//		if(!this.canRestart())	return;

		let train=this.getTrain();
		this.setSpeedAndRefreshUI(train.nonStopSpeed,true);
	}
	
	setSpeedMax(){
		// UNUSEFUL : Since inputs are deactivated if needed :
//		if(!this.canRestart())	return;
		
		this.setSpeedAndRefreshUI(this.maxSpeed,true);
	}

	
	getTrainGoalSpeed(){
		let train=this.getTrain();
		return Math.round(train.getGoalSpeed());
	}
	
	
	/*public*/setStopAutomatically(event){
		let isChecked=!!event.target.checked; /*(forced to boolean)*/
		this.stopAutomatically=isChecked;
		
		if(!isChecked){
			let infos=this.getInfos();
			// We reactivate the UI inputs if needed :
			this.setSpeedActionsInactive(false);
			window.eranaScreen.controlsManager.refreshInfoPad(this, infos);
	}
		
		
	}
	/*public*/isStoppedAutomatically(){
		return this.stopAutomatically;
	}
	
	
	
	
	/*private*/setSpeedAndRefreshUI(speed,updateUI=true){
		
		if(0<speed && !this.hasEnoughResourceForSpeeding(speed)){
			
			// MESSAGE
			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("impossibleDeFonctionnerSansRessources"));
			
			return;
		}
		
		let train=this.getTrain();
		
		train.changeSpeed(speed);
		
		// Refresh of the UI if needed :
		if(updateUI)
			window.eranaScreen.controlsManager.refreshInfoPad(this);
		
	}
	
	
	
	// UNUSEFUL : Since inputs are deactivated if needed :
//	/*private*/canRestart(){
//		let train=this.getTrain();
//		let collidedStation=train.getCollidedStation();
//		// If train is not set to auto brake, then we ignore the braking distance : 
//		if(!this.isStoppedAutomatically() 
//				|| (collidedStation && train.getSpeed()==0 ) ) return true;
//		if(this.isInAutobrakingDistanceToNextStation()
//				&& train.isAutoBraking()){
//			// MESSAGE
//			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("approcheAutomatique"));
//			return false;
//		}
//		return true;
//	}
	
	
	
	/*private*/isInAutobrakingDistanceToNextStation(){

		let train=this.getTrain();
		
		
		let distanceToNextStation=train.getDistanceToNextStationPixels();
		if(!distanceToNextStation/*includes case==0*/ || distanceToNextStation<0 /*(only «<» because previously we included the case==0)*/)		return true;

		
		// NO : Because at low speeds, then the stop distance could be «inside» the station !
		let collidedStation=train.getCollidedStation();
		// If we are in the «first half» of a collided station : (whatever the speed)
		if(collidedStation && distanceToNextStation<collidedStation.size.w/2)	return true;

		
		
		let durationTimeFactor=window.eranaScreen.getDurationTimeFactor();
		let inertiaTime=train.inertiaTimeMillis*durationTimeFactor;

		// Magic number :
		const AUTO_STOP_DISTANCE_FACTOR=0.02; // a pretty good magic number value
		let calculatedStopDistancePixels = inertiaTime * train.getSpeed() * nonull(this.autoStopDistanceFactor, AUTO_STOP_DISTANCE_FACTOR);

		// If calculatedStopDistancePixels is 0, for instance is train is stopped, then we set it a a minimal value nonetheless:
//		if(calculatedStopDistancePixels<=0)		calculatedStopDistancePixels=20  // Magic number, also
////																					// DBG
//																					*(.125/durationTimeFactor)
//																					;
		
		let stopDistance = Math.round(nonull(this.autoStopDistancePixels, calculatedStopDistancePixels));
		let isCloseEnough=(distanceToNextStation<=stopDistance);
		
		// DBG
		if(IS_DEBUG_LOCOMOTIVE)	lognow("calculatedStopDistancePixels:"+calculatedStopDistancePixels+" isCloseEnough:"+isCloseEnough+" stopDistance:"+stopDistance+" distanceToNextStation:"+distanceToNextStation);
		
		return isCloseEnough;
	}


	
	/*private*/adjustSpeedAccordingToDistanceToStation(){
		
		let train=this.getTrain();
		
		if(!this.isStoppedAutomatically())		return;
		
		
		let distanceToNextStation=train.getDistanceToNextStationPixels();

		
		let isInAutobrakingDistanceToNextStation=this.isInAutobrakingDistanceToNextStation();
		if(!isInAutobrakingDistanceToNextStation || !distanceToNextStation/*includes case ==0*/) {
			
			if(this.hasAutomaticallyStopped)		
				this.hasAutomaticallyStopped=false;
			
			
			// DBG
			if(IS_DEBUG_LOCOMOTIVE)	lognow("isInAutobrakingDistanceToNextStation:"+isInAutobrakingDistanceToNextStation+" distanceToNextStation:"+distanceToNextStation);
			
			return;
		}

		
		// Here we are garanteed being in the automatic slowing zone :
		
		
		if(!this.hasMessageAutoBrakingBeenFired){
			// MESSAGE
			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("approcheAutomatique"));
			this.hasMessageAutoBrakingBeenFired=true;
		}

		
		let realDeltaTimeMillis=window.eranaScreen.refreshRateMillis;
		
		// TODO : FIXME : MAGIC NUMBER
//		const RATIO_SPEED_CORRECTION_FACTOR=.25;
		
//		const RATIO_SPEED_CORRECTION_FACTOR=1;
//		let ratioSpeed=Math.round((distanceToNextStation / realDeltaTimeMillis) * RATIO_SPEED_CORRECTION_FACTOR);

		let ratioSpeed=Math.round(distanceToNextStation / realDeltaTimeMillis);

		
		
		let infos=this.getInfos();
		let calculatedRatioSpeed = Math.min(this.maxSpeed, ratioSpeed)*window.eranaScreen.getDurationTimeFactor();

//			
//	if(IS_DEBUG_LOCOMOTIVE)	lognow("2 ["+distanceToNextStation+"/"+realDeltaTimeMillis+"] ratioSpeed(:"+ratioSpeed+") <<<");
//			


//	// DBG
//	lognow(">>>train.getSpeed:",train.getSpeed());
//	lognow(">>>Math.round( train.getSpeed()):",Math.round( train.getSpeed()));
		
	
		if(!this.hasAutomaticallyStopped){
			
			let trainIsStopped=train.isStopped();
			
			// DBG
			if(IS_DEBUG_LOCOMOTIVE)	lognow("trainIsStopped:"+trainIsStopped);
			
			
			if(!trainIsStopped){
				
				
				// SLOWING :
				let trainSpeed=Math.round( train.getSpeed() );
				let cappedSpeed=Math.min(calculatedRatioSpeed, trainSpeed);
				
				
				
				
				
				// NO : SPECIAL CASE : train.changeSpeed(calculatedRatioSpeed);
				train.doSetSpeed(cappedSpeed, true); // train speed cannot go up
				
				// DBG
				if(IS_DEBUG_LOCOMOTIVE)	lognow("cappedSpeed:"+cappedSpeed+" train.getSpeed():"+train.getSpeed());
				
		
				// We deactivate the UI inputs :
				this.setSpeedActionsInactive(true);
				window.eranaScreen.controlsManager.refreshInfoPad(this, infos);
				
				
	//		// (also equals, because we round all the speeds here :)
	//		if(calculatedRatioSpeed<=trainSpeed){
				train.setIsAutoBraking(true);
	//			}
				
				
//				if(IS_DEBUG_LOCOMOTIVE)	lognow("SLOWING  ["+distanceToNextStation+"/"+realDeltaTimeMillis+"]" +
//						" ratioSpeed(:"+ratioSpeed+") cappedSpeed:"+cappedSpeed+"" +
//						" calculatedRatioSpeed: "+calculatedRatioSpeed+
//						" trainSpeed:"+trainSpeed+"<<<");

//				// We force the total stop ;
//				if(Math.round(cappedSpeed)===0)	cappedSpeed=0;

				
			
			}else{
				
				
//				train.doSetSpeed(0, true);
				
				
				// STOPPED
				// We reactivate the UI inputs :
				this.setSpeedActionsInactive(false);
				window.eranaScreen.controlsManager.refreshInfoPad(this, infos);
				this.hasMessageAutoBrakingBeenFired=false;
				
				train.setIsAutoBraking(false);
	
				
				// We add a «third state» information-bearing :
				this.hasAutomaticallyStopped=true;

				
				if(IS_DEBUG_LOCOMOTIVE)	lognow("STOPPED ratioSpeed(:"+ratioSpeed+")<<<");
				
			}
		
		}
		
		
//			
//		}else{
//
//			
//			train.doSetSpeed(0, true);
//			
//			// We reactivate the UI inputs :
//			this.setSpeedActionsInactive(false);
//			window.eranaScreen.controlsManager.refreshInfoPad(this, infos);
//			
//			train.setIsAutoBraking(false);
//
//			
//			if(IS_DEBUG_LOCOMOTIVE)	lognow("STOPPED ratioSpeed(:"+ratioSpeed+")<<<");

			
			
//		}
		

	
	}
	
	
	
	
	
	
	/*private*/setSpeedActionsInactive(isInactiveParam){
		
		let infos=this.getInfos();
		let actions=infos.actions;
		
		if(actions.setSpeed.inactive===isInactiveParam)		return;
		
		actions.setSpeed.inactive=isInactiveParam;
		actions.setSpeedToZero.inactive=isInactiveParam;
		actions.setDefaultSpeed.inactive=isInactiveParam;
		actions.setSpeedMax.inactive=isInactiveParam;
		
	}
	
	// UNUSED
	/*public*/getNativeMaxSpeed(){
		return this.nativeMaxSpeed;
	}

	/*public*/getMaxSpeed(){
		return this.maxSpeed;
	}
	
	
	/*private*/getTrain(){
		return this.parent.parent;
	}
	

	/*OVERRIDES*//*public*/getNeededCostsNames(){
		let costsNames=[];

		if(!this.isBusy())	return costsNames;

		let train=this.getTrain();

		let trainGoalSpeed=train.getGoalSpeed();

		foreach(this.speedsCosts,(speedCost,speedRangeStr)=>{
			
			if(!contains(speedRangeStr,"=>"))	return "continue";
			
			if(!Math.isInMinMax(speedRangeStr,trainGoalSpeed,
					// CAUTION : This is a special case, different than hasEnoughResourceForSpeeding(...),
					// because we want to be able to gather resources for functionning, even if the locomotive has stopped !
					"[]","=>"))	return "continue";
			
			let costs=speedCost.costs;
			if(costs){
				foreach(costs,(cost,costName)=>{
					if(!contains(costsNames,costName))	costsNames.push(costName);
				},(costValue)=>{	return	0<costValue; /*we exclude the possible wastes*/	});
			}
			
		});
		
		return costsNames;
	}
	
	
	
	/*public*/isBusy(){
		let train=this.getTrain();
		return (0<train.getGoalSpeed());
	}
	

	// --------------------------------------------------------------

	
	/*public*/canDrawSmokePits(){
		return this.isBusy();
	}
	
	
	draw(ctx,camera,lightsManager){
	
		this.adjustSpeedAccordingToDistanceToStation();
		
		
		this.speedingRoutine.doStep(this);


		
		let gameSound=super.getGameSound();
		if(gameSound){
//			let soundSpeed=null; // or 0...
			if(this.isBusy()) {
				gameSound.isAudible=true;
				// CREATES STRANGE PLAYBACK BUG :
//				let train=this.getTrain();
//				soundSpeed=Math.roundTo(Math.coerceInRange((train.goalSpeed / this.maxSpeed)*1.5, .5, 1.5), 2);
			}else{
				gameSound.isAudible=false;
			}
			
//		gameSound.verifyPlayEachStep(camera, soundSpeed);
			gameSound.verifyPlayEachStep(camera);
			
		}

		
		if(!super.draw(ctx,camera,lightsManager))		return false;

		
		return true;
	}
	
	
	

}
