
const BUILD_REFRESH_RATE_MILLIS=500;

class FactoryEquipment extends DockableEquipment{

	
	constructor() {
		super();
		
		this.busy=false;
		this.buildingAnimation=new GameAnimation(this);


	}
	
	// INIT
	
	init(gameConfig){
		super.init(gameConfig);

		this.buildingAnimation.init(this.gameConfig,"build");
		
	}
	
	// METHODS
	
	// =================== GAME RULES METHODS ===================
	startProductionAnimation(){
		
		// We start a building job :
		this.busy=true;
		this.buildingAnimation.loadSprite2DAndSet("build").start();

	}	
	
	stopProductionAnimation(){
		this.busy=false;
		this.buildingAnimation.stop(this);
	}
	

	pauseProductionAnimation(){
		this.buildingAnimation.stop(this);
	}

	resumeProductionAnimation(){
		this.buildingAnimation.start(this);
	}
	

	
	/*private*/isBusy(){
		return this.busy;
	}


	
	// --------------------------------------------------------------

	/*private*/drawUIElements(ctx,camera,thumbImagePosition){
		// TODO ...
	}

	
	drawInUI(ctx,camera){
		let thumbImagePosition=super.drawInUI(ctx,camera);
		
		this.drawUIElements(ctx,camera,thumbImagePosition);
		
		return thumbImagePosition;
	}
	
	
	draw(ctx,camera,lightsManager){
	
	

		let gameSound=super.getGameSound();
		if(gameSound){
			
			if(this.isBusy()) {
				gameSound.isAudible=true;
			}else{
				gameSound.isAudible=false;
			}
			
			gameSound.verifyPlayEachStep(camera);
			
		}
		
		
		if(!super.draw(ctx,camera,lightsManager)) return false;

		
		return true;
	}
	
	


}
