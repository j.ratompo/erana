
class PlasmaTorch extends DockableEquipment{
	// TODO : Develop... : extends ActivableEquipment

	constructor() {
		super();

		this.active=false;

		this.strength=1;
		
		
		this.preheatFlameAnim=new GameAnimation(this);
		this.flameAnim=new GameAnimation(this);
		
		this.phase=null;

		
	}
	
	// INIT


	init(gameConfig){
		super.init(gameConfig);
		
		this.preheatFlameAnim.init(this.gameConfig).loadSprite2DAndSet("default", "preheatImageConfig_1", this.preheatImageConfig_1._2D.size).start();
		this.flameAnim.init(this.gameConfig).loadSprite2DAndSet("default", "flameImageConfig_1", this.flameImageConfig_1._2D.size).start();

	}

	
	// --------------------------------------------------------------------------
	
	// METHODS

	
	activate(event){
		let isChecked=!!event.target.checked; /*(forced to boolean)*/
		
		if(isChecked){
			
			if(this.hasInsufficientResources()){
				// MESSAGE
				window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n({"fr":"Pas assez de ressources pour activer.","en":"Not enough resources to start."}));
				return;
			}
			
			
		}
		
		if(isChecked)
			this.setActive(true);
		else
			this.stopFlame();


	}
	
	

	/*private*/hasInsufficientResources(){
		
		let platform=this.parent;
		let resourcesRepository=platform.resourcesRepository;
		let costs=this.getActivableConfig().costs;
		let insufficientResourcesList=resourcesRepository.getInsufficientResourcesList(costs);
		
		if(!empty(insufficientResourcesList)){
			return true;
		}

		return false;
	}
	
	/*public*/setActive(active){
		this.active=active;
	}
	
	/*public*/isActive(){
		return this.active;
	}
	

	/*public*/getActivableConfig(){
		return this.flamesByStrength[this.strength];
	}

	
	
	/*private*/stopFlame(){
		this.setActive(false);
		this.phase=null;
		this.lastTimeFlame=null;
	}
	
	
	// ----------------------------------------------------------------------------
	
	
	
	/*public*/canDrawSmokePits(){
		return (this.isActive() && this.phase=="flame");
	}
	
	
	
	/*private*/drawEffect(ctx,camera,lightsManager){

		if(!this.isActive())	return false;
		
		if(this.hasInsufficientResources()){
			// MESSAGE
			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("pasAssezDeRessourcesEnPause"));

			this.stopFlame();
			return;
		}


		
		let flameConfig=this.getActivableConfig();
		let durationTimeFactor=window.eranaScreen.getDurationTimeFactor();
		
		// Effect drawing :
		
		if(!this.lastTimeFlame)			this.lastTimeFlame=getNow();
		if(!hasDelayPassed(this.lastTimeFlame, flameConfig.preheatTimeMillis*durationTimeFactor) && this.phase!="flame"){

			
			if(this.phase!="preaheat")	this.phase="preaheat";
			this.preheatFlameAnim.draw(ctx,camera,lightsManager);	
			
		}else{
			
			if(this.phase!="flame") 		this.phase="flame";
			this.flameAnim.draw(ctx,camera,lightsManager);	
		
		}
		
		
		
		// Resources consumption :
		let timeMillis=flameConfig.timeSeconds*1000;

		if(!this.lastTimeConsumption || hasDelayPassed(this.lastTimeConsumption, timeMillis*durationTimeFactor)){
			this.lastTimeConsumption=getNow();

			let platform=this.parent;
			let resourcesRepository=platform.resourcesRepository;
			let costs=flameConfig.costs;
			let insufficientResourcesList=resourcesRepository.getInsufficientResourcesList(costs);
			if(empty(insufficientResourcesList)){
				
				resourcesRepository.consumeStocks(costs);
				
			}else{
				
				// MESSAGE
				window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("impossibleDeFonctionnerSansRessources"));
				
				this.stopFlame();
				
			}
			
		}
		
		
		
	}

	
	draw(ctx,camera,lightsManager){
		
		if(!super.draw(ctx,camera,lightsManager))	return false;
		
		this.drawEffect(ctx,camera,lightsManager);

		return true;
	}

	

	drawInUI(ctx,camera){
		super.drawInUI(ctx,camera);
	}
	
}
