
class GameAutomatonBuilder extends GameAutomaton{
	
	constructor(){
		super();

		this.resourceThatIsInNeed=null;
		
	}
	
	// ---------- BUILDING JOB LOGIC : ----------

	/*public*/hasBuilderJobAndIsAvailableAndNeedingBuildingJobExistsAndIsPeopleNotLoaded(){
		
		
		// CURRENT : DEBUG
		
		// DBG
		lognow("hasBuilderJobAndIsAvailableAndNeedingBuildingJobExistsAndIsPeopleNotLoaded:");
		
		if(this.concernedObject.getJobType()!=="builder")	return false;

		if(this.isConcernedPeopleLoaded())	return false;	
		
		const train=this.getConcernedObjectTrain();
		return train.hasAvailableWorkPositions();
	}
	
	/*public*/goToNeedingBuildingJobIfNecessary(){
		
		const train=this.getConcernedObjectTrain();
		const platformsWithWorkPositionsAvailable=train.getPlatformsWithAvailableWorkPositions();
		if(empty(platformsWithWorkPositionsAvailable)){
			this.next();
			return;
		}

		const chosenPlatform=Math.getRandomInArray(platformsWithWorkPositionsAvailable);
		const width=(chosenPlatform.size.w);
		const demiWidth=(width*.5);
		const xWiggle=Math.getRandomInt(demiWidth,0);
		this.concernedObject.setDestinationPlatform(chosenPlatform);
		const destination={x:chosenPlatform.getPosition().x+xWiggle};

		this.concernedObject.startMovingTo(destination, null,
			function(selfParam){
//			// DBG
//			if(IS_DEBUG_PEOPLE)	lognow(this.uuid+"  REACHED A (end of fetch) !!!"+destination.x);
				
				selfParam.setDestinationPlatform(null);
				
				selfParam.setWorkPlacePlatform(chosenPlatform);
				selfParam.getWorkPlacePlatform().addWorkerPeople(selfParam);

				selfParam.getGameAutomaton().next();
		},this.speed);
		
	}
	

	// TODO : FIXME : KINDA DUPLICATED CODE !	
	/*public*/goToDropLoadToAvailablePlatform(){

		const concernedObject=this.concernedObject;
		
		// 1) We need to know which resource is in need
		let allResourcesInNeed=this.concernedObject.getAllResourcesInNeed(this.concernedObject.getLoadedResourceName());
		if(empty(allResourcesInNeed)){
			allResourcesInNeed=this.concernedObject.getFallbackResourcesInNeed(this.concernedObject.getLoadedResourceName());
			if(empty(allResourcesInNeed))	return;
		}

		const resourceToSupply=Math.getRandomInArray(allResourcesInNeed);
		this.resourceThatIsInNeed=resourceToSupply;
		
		concernedObject.setDestinationPlatform(resourceToSupply.parentPlace);
		
		let destination={x:(resourceToSupply.positionX + resourceToSupply.parentPlace.position.x)};
		
		concernedObject.startMovingTo(destination,null,(selfParam)=>{
					selfParam.getGameAutomaton().next();
		},concernedObject.speed);
		
	}
	
	// TODO : FIXME : KINDA DUPLICATED CODE !	
	/*public*/hasUnloadedResource(){
		const concernedObject=this.concernedObject;
		const targetResource=this.resourceThatIsInNeed;
		if(!concernedObject.getLoadedResourceName() || !targetResource)	return false;
		const result=concernedObject.unloadResource(targetResource);
		if(result)	this.resourceThatIsInNeed=null;
		return result;
	}
	
	
//	/*public*/hasReachedJobPlace(){
// 		return false;
//	}
	/*public*/workOnJobOnce(){
		
	}
	/*public*/isJobUnavailable(){
		
	}
	/*public*/isJobUnfinished(){
		
	}
	
	
	/*private*/getConcernedObjectTrain(){
		return this.concernedObject.parent;
	}
	
	// DELEGATED :
	/*public*/isConcernedPeopleLoaded(){
		return this.concernedObject.isLoaded();
	}
	
	// DELEGATED :
	/*public*/getIsDeadAndUpdateHabitationAffectationIfNeeded(){
		return this.concernedObject.getIsDeadAndUpdateHabitationAffectationIfNeeded();
	}

	// DELEGATED :
	/*public*/wait(){
		return this.concernedObject.wait();
	}
	
	// DELEGATED :
	/*public*/isRestNecessaryAndPossible(){
		return this.concernedObject.isRestNecessaryAndPossible();
	}
	
	// DELEGATED :
	/*public*/goToRest(){
		this.concernedObject.goToRest();
	}
	
	
}
