
class DismantleManager extends JobManager{
	
	constructor(concernedObject,gameConfig){
		super(concernedObject,gameConfig);

	}
	
	/*public*/start(selfParam,args){
		
		super.start(selfParam,args);

		const platform=args.platform;
		
		const self=this;
		return new Promise((resolve,reject)=>{
			
			if(!platform.getDockableEquipment()){
				// MESSAGE
				window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,
						i18n("aucunÉquipementÀDémanteler"));
				return;
			}
			
			let popupConfig=this.gameConfig.popupConfig;
			promptWindow(i18n("sommesNousSûres"),"yesno",null,
					function(){
						
						let overEquipment=platform.getOverEquipment();
						let dockableEquipment=platform.getDockableEquipment();
	
						if(overEquipment){
							if(!overEquipment.costs){
								// MESSAGE
								window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,
										i18n("impossibleDeDémantelerCeSurÉquipement"));
								return;
							}
						}else if(dockableEquipment){
							if(!dockableEquipment.costs){
								// MESSAGE
								window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,
										i18n("impossibleDeDémantelerCetÉquipement"));
								return;
							}
						}
	
						
						selfParam.mainRoutine=getMonoThreadedRoutine(this,BUILD_REFRESH_RATE_MILLIS,false).setDurationTimeFactorHolder(window.eranaScreen).start(this);
						
			
						self.dismantleJobInfos.percentage=0;
						 
						let timeRatio=(BUILD_REFRESH_RATE_MILLIS/(durationMillis*window.eranaScreen.getDurationTimeFactor()));
						self.dismantleJobInfos.percentageStep=timeRatio*100;
						
						self.dismantleJobInfos.platform=platform;
						
						
						
						resolve(selfParam);
			}, null, {
				displayMode:"modal",
				backgroundColor1:popupConfig.backgroundColor1,
				backgroundColor2:popupConfig.backgroundColor2,
				textColor:popupConfig.textColor,
				width:"70%",height:"15%",
			});
			
			
			
		});
	}
	
	
	/*private*/doOnEachStep(args){
		const selfParam=this;
		selfParam.dismantleJobInfos.percentage+=selfParam.dismantleJobInfos.percentageStep;
	}
	
	/*private*/terminateFunction(){
		return super.isJobFinished(this);
	}
	
	/*private*/doOnStop(args){
			const selfParam=this;
			let platformLocal=selfParam.dismantleJobInfos.platform;
			let overEquipmentLocal=platformLocal.getOverEquipment();
			let dockableEquipmentLocal=platformLocal.getDockableEquipment();
			if(overEquipmentLocal){
				platformLocal.resourcesRepository.increaseStocks(overEquipmentLocal.costs);
				if(overEquipmentLocal.doOnDismantle)	overEquipmentLocal.doOnDismantle();
				platformLocal.setOverEquipment(null);
			}else if(dockableEquipmentLocal){
				platformLocal.resourcesRepository.increaseStocks(dockableEquipmentLocal.costs);
				if(dockableEquipmentLocal.doOnDismantle)	dockableEquipmentLocal.doOnDismantle();
				platformLocal.setDockableEquipment(null);
			}
	
			super.stop(selfParam,args);
	 }

	
}