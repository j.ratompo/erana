
class DockableEquipment{

	constructor() {

		this.isVisible=true;
		this.isMovable=true;

		this.mover=getMoverMonoThreaded(this,false,{interruptable:false});
		
		this.position=new Position(this);
		
		this.img=new GameImage(this).addExtensionImage("pending");
		
		this.snd=null;
		
		this.pending=false;		
	}
	
	// INIT
	init(gameConfig){
		
		this.gameConfig=gameConfig;

//		// DBG
//		if(!this.imageConfig){
//			console.log("!!!!",this);
//		}
		
		let position=this.position;
		position.center=this.imageConfig._2D.center;
		
		
		
		let gameImage=this.getGameImage();
		gameImage.init(gameConfig);
		
		if(this.heatImageSrc)	 this.heatImage=gameImage.loadImage(this.heatImageSrc);

		
		if(this.soundConfig) {
			this.snd=new GameSound(this);
			if(this.snd)			this.snd.init(gameConfig);
		}

		
		this.getMover().setDurationTimeFactorHolder(window.eranaScreen);
		
		
		
		
	}

	
	// METHODS
	getGameImage(){
		return this.img;
	}
	getGameSound(){
		return this.snd;
	}
	
	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	
	
	getIsMovable(){
		return this.isMovable;
	}
	setIsMovable(isMovable){
		this.isMovable=isMovable;
	}
	getMover(){
		return this.mover;
	}
	
	
	doOnClick(){
		/*DO NOTHING*/
		// (Only used to make this element clickable)
	}

	isSelectable(){
		return this.selectable!==false; // CAUTION : By default, it is selectable !
	}

	isClickable(){
		return this.clickable!==false; // CAUTION : By default, it is clickable !
	}
	
	
	
	// Useful for movable dockable equipments, like the Ergostasio :
	move(){
		// Mover stepping :
		// Only for monothreaded movers :
		this.getMover().doStep();
	}
	
	
	getInfos(){
		return this.infos;
	}
	
	
	/*public*/getInfosHeaderHTML(){
		if(this.isPending()){
			return i18n("constructionPending");
		}
		return "";
	}
	
	
	
	// =================== GAME RULES METHODS ===================
	
	

	
	/*private*/getTrain(){
		
		let platform=this.parent;
		let train=platform.parent;

		// If we are in the case of an ergostasio:
		if(this.isErgostasio()){
			train=this.parent;
		}
		
		return train;
	}

	
	/*private*/isErgostasio(){
		let platformOrTrain=this.parent;
		let trainOrRegion=platformOrTrain.parent;
		
		// If we are in the case of an ergostasio, since Regions never have a isSelectable() method:
		return !trainOrRegion.isSelectable;
	}
	
	/*private*/checkFunctionnality(){
		this.grandeLigneGauge.checkFunctionnality();
	}
	
	/*public*/doOnChangeState(stateType, isFunctional){
		
		if(!isFunctional){
			// TRACE
			if(stateType==="functional")
				window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,
						i18n("unÉquipementVientDeBriser"));
			else
				window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,
						i18n("unÉquipementEstDétruit"));
				
		}
	}
	
	/*private*/isFunctional(){
		return this.grandeLigneGauge.isFunctional();
	}
	
	
	/*public*/getOverridingPositionReferenceObject(){
		return this.parent;
	}
	
	
	
	/*public*/setPending(pending){
		this.pending=pending;
	}

	/*public*/isPending(){
		return this.pending;
	}	


	/*public*/removeFromParent(){
			if(this.type==="overEquipment")
				this.parent.removeOverEquipment();
			else
				this.parent.removeDockableEquipment();
	}
	
	
	/*public*/setWorkersCount(event,value,inputConfig){
		const platform=this.parent;
		if(!platform.activeJobManager)	return;

		const train=platform.parent;		
		platform.activeJobManager.setWorkersCount(train,parseInt(value),inputConfig);
	}
	
	/*public*/getAffectedWorkersCount(){
		const platform=this.parent;
		if(!platform.activeJobManager)	return 0;
		return platform.activeJobManager.getAffectedWorkersCount();
	}
	
	getDrawables(){
		return [this.gameSmokePits
//			,this.gameThumb
			];
	}
	
	// -----------------------------------------------------------

	/*protected*/canDrawSmokePits(){
		return true;
	}

	/*public*/drawSmokePits(ctx,camera){
		if(this.gameSmokePits) {
			let self=this;
			foreach(this.gameSmokePits,(s)=>{
				s.updateStateAndDraw(ctx,camera);
			});
		}
	}
	
	
	
	/*private*/drawHeatEffect(ctx,camera){
		
		if(!this.heatImage)	return;
		
		// We don't draw the heat effect if the equipment is not an ergostasio and its platform has an overEquipment :
		if(!this.isErgostasio()){
			let platform=this.parent;
			if(platform.getOverEquipment() && this.genericType!=="overEquipment")		return;
		}
		
		let region = this.getTrain().parent;
		let daylightManager=region.daylightManager;
		let temperature=daylightManager.getTemperature();
		
		let opacity=Math.demiCurve(daylightManager.minTempForHeat, daylightManager.maxTempForHeat, temperature);
		

		let realImageCoords=this.getGameImage().getRealImageCoords(camera);
		drawImageAndCenterWithZooms(ctx,this.heatImage,
				realImageCoords.x,
				realImageCoords.y,
				realImageCoords.zooms,
				realImageCoords.center, 
				(realImageCoords.scaleW),
				(realImageCoords.scaleH),
				null,
				null,
				opacity+0.0/*(To force a float !)*/);
				
		
	
	}
	
	


	draw(ctx,camera,lightsManager){
		
		this.checkFunctionnality();
		
		
		let gameImage=this.getGameImage();
		if(!gameImage.canDraw(camera))	return false;
		
		
		if(contains(this.gameConfig.projection,"2D")){
			
			if(!this.isPending()){
				gameImage.draw(ctx,camera,lightsManager);
				this.drawHeatEffect(ctx,camera);
			}else{
				gameImage.drawExtensionImage(ctx,camera,"pending",.5);
			}
			
			
		}
		
		
		return true;
	}
	
	
	drawInUI(ctx,camera){

		let thumbImagePosition;
		
		let train=this.getTrain();
		if(this.gameThumb 
				// Only for selectable trains :
				&& train.isSelectable()){
			
			thumbImagePosition=this.gameThumb.drawDynamicThumbnailUI(ctx,camera,this.grandeLigneGauge,train);

			if(this.grandeLigneGauge){
				if(this.grandeLigneGauge.drawGaugeInUI(ctx,this.gameThumb)){
					this.grandeLigneGauge.slowlyDamage(train.parent.daylightManager);
				}
			}
		}
		
		return thumbImagePosition;
	}


}
