

class GameAutomatonTransporter extends GameAutomaton{
	
	constructor(){
		super();
		
		this.resourceThatCanProvide=null;
		this.resourceThatIsInNeed=null;
	}
	
	// ---------- TRANSPORTER JOB LOGIC : ----------
	
	/*public*/goFetchResource(){
		
		// DBG
		if(IS_DEBUG_PEOPLE)	lognow(this.concernedObject.uuid+" Trying to goFetchResource() : "+this.currentStateName);
		
		
		const train=this.getConcernedObjectTrain();
		const collidedStation=train.getCollidedStation();

		
		this.resourceThatCanProvide = null;
		this.resourceThatIsInNeed = null;
		
		
		// 1) We need to know which resource is in need
		let allNeedingResources = this.concernedObject.getAllResourcesInNeed();
		if(empty(allNeedingResources)){
			// If no resource is in need, then we skip :
			this.next();
			
			// DBG
			if(IS_DEBUG_PEOPLE)	lognow(this.concernedObject.uuid+" 	goFetchResource(): NO RESOURCE IN NEED !");
			
			return;
		}
		
		
		// 2) We need to know which resource can provide
		const allProvidingResources = this.concernedObject.getAllResourcesThatCanProvide(collidedStation);
		if(empty(allProvidingResources)){

			// If no resource can provide, then we skip :
			this.next();
			
			if(IS_DEBUG_PEOPLE)	lognow(this.concernedObject.uuid+" 	goFetchResource(): NO RESOURCE CAN PROVIDE!");

			return;
		}
		

		// 3) We try to find a match:
		let resourcesMatches=findMatches(allProvidingResources,allNeedingResources,function(rProviding, rNeeding){
			// The resource that can provide must be located on another platform than the resource that is needing, ie. it must not be the same ! :
			return  (rProviding!==rNeeding 
						&& rProviding.name===rNeeding.name 
						&& (rNeeding.amount<rProviding.amount || containsIgnoreCase(rProviding.parentPlace.prototypeName,"station")));
		});

	
		
		// Fall-back : if no resource is in need at this point, then we will deliver it to the first found free repository :
		if(empty(resourcesMatches)){
			
			// DBG
			if(IS_DEBUG_PEOPLE)		lognow("	«"+this.concernedObject.uuid+" » TRYING FALLBACK (goFetchResource(...))");

			allNeedingResources=this.concernedObject.getFallbackResourcesInNeed();
			resourcesMatches=findMatches(allProvidingResources, allNeedingResources, function(rProviding, rNeeding){
				// The resource that can provide must be located on another platform than the resource that is needing, ie. it must not be the same ! :
				return  (rProviding!==rNeeding 
							&& rProviding.name===rNeeding.name 
							&& (rNeeding.amount<rProviding.amount || containsIgnoreCase(rProviding.parentPlace.prototypeName,"station")));
			});
		}
			
		
		if(empty(resourcesMatches)){
			// If no resource match is found, then we skip :
			this.next();
			
			// DBG
			if(IS_DEBUG_PEOPLE) {
				lognow("	«"+this.concernedObject.uuid+" ("+this.concernedObject.getLoadedResourceName()+")» goFetchResource(): NO RESOURCE MATCH FOUND !");
				console.log("allProvidingResources:",allProvidingResources);
				console.log("allNeedingResources:",allNeedingResources);
			}
			
			return;
		}
		
		
		const resourceMatch=Math.getRandomInArray(resourcesMatches);
		this.resourceThatCanProvide=resourceMatch[0];
		this.resourceThatIsInNeed=resourceMatch[1];
		
		
		// DBG
		if(IS_DEBUG_PEOPLE)	lognow(this.concernedObject.uuid+" 	goFetchResource(): FOUND A RESOURCE MATCH !"+this.resourceThatCanProvide.name+"->"+this.resourceThatIsInNeed.name);
		
		
		// 4) Make the trip between the two :
		
		let destination=null;
		let parentPlace=this.resourceThatCanProvide.parentPlace;
		
		this.concernedObject.setDestinationPlatform(containsIgnoreCase(parentPlace.prototypeName,"platform")?parentPlace:null);
		
		if(collidedStation && containsIgnoreCase(parentPlace.prototypeName,"station")){
			
			// Here parentPlace is a station :
			// CAUTION : We want to transform the position from the station's reference to the train's reference :
			let xDelta=parentPlace.position.x-train.getPosition().x;
			destination={x:this.resourceThatCanProvide.positionX + xDelta};
			
		}else{
			
			// Here parentPlace is a platform :
			destination={x:this.resourceThatCanProvide.positionX + parentPlace.position.x};
		}
		
		
		const self=this;
		this.concernedObject.startMovingTo(destination,null,
			function(selfParam){

				// DBG
				if(IS_DEBUG_PEOPLE)	lognow(this.concernedObject.uuid+"  REACHED A (end of fetch) !!!"+destination.x);
				
				selfParam.getGameAutomaton().next();
		
		},this.concernedObject.speed);
	
	}
	
	/*public*/goDeliverResource(){

		const concernedObject=this.concernedObject;
		
		// 1) We need to know which resource is in need
		let resourceToSupply=this.resourceThatIsInNeed;

		concernedObject.setDestinationPlatform(resourceToSupply.parentPlace);
		
		let destination={x:(resourceToSupply.positionX + resourceToSupply.parentPlace.position.x)};
		
		concernedObject.startMovingTo(destination,null,
				function(selfParam){
					selfParam.getGameAutomaton().next();
				
		},concernedObject.speed);
		
	}
	
	
	/*public*/goBackOnTrain(){

		this.concernedObject.setDestinationPlatform(this.getConcernedObjectTrain().getClosestPlatform(this.concernedObject.getPosition()));

		const self=this;
		let destination={x:this.concernedObject.getDestinationPlatform().getPosition().x};
		this.concernedObject.startMovingTo(destination,null,
				function(selfParam){
					selfParam.getGameAutomaton().next();
				},this.concernedObject.speed);
		
	}
	
	
	
	/*public*/isResourceTransferTaskIsAvailable(){
		if(!this.concernedObject.isLoaded() && empty(this.concernedObject.getAllResourcesInNeed())) {
			return false;
		}
		return true;
	}
	
	/*public*/isNotOnTrainAndIsInStation(){
		const train=this.getConcernedObjectTrain();
		const collidedStation=train.getCollidedStation();
		return !this.concernedObject.isInTrain() && collidedStation && this.concernedObject.isInStation(collidedStation);
	}
	
	/*public*/hasLoadedResource(){
		const targetResource=this.resourceThatCanProvide;
		if(!targetResource)	return false;
		return this.concernedObject.loadResource(targetResource);
	}

	// TODO : FIXME : DUPLICATED CODE !
	/*public*/hasUnloadedResource(){
		const concernedObject=this.concernedObject;
		const targetResource=this.resourceThatIsInNeed;
		if(!concernedObject.getLoadedResourceName() || !targetResource)	return false;
		return concernedObject.unloadResource(targetResource);
	}
	
	/*public*/hasFoundAnotherResourceInNeedAndIsPeopleLoaded(){
		if(this.isConcernedPeopleNotLoaded())	return false;
		
		let allResourcesInNeed=this.concernedObject.getAllResourcesInNeed(this.concernedObject.getLoadedResourceName());
		if(!empty(allResourcesInNeed)){
			let anotherResourceThatIsInNeed=Math.getRandomInArray(allResourcesInNeed);
			// at this point, it means we are in a «wait» state, so we MANDATORILY already have a load :
			this.resourceThatIsInNeed=anotherResourceThatIsInNeed;
			return true;
		}
		
		// Fall-back : if no resource is in need at this point, then we will deliver it to the first found free repository :
		allResourcesInNeed=this.concernedObject.getFallbackResourcesInNeed(this.concernedObject.getLoadedResourceName());
		if(!empty(allResourcesInNeed)){
			let anotherResourceThatIsInNeed=Math.getRandomInArray(allResourcesInNeed);
			// at this point, it means we are in a «wait» state, so we MANDATORILY already have a load :
			this.resourceThatIsInNeed=anotherResourceThatIsInNeed;
			return true;
		}
	
		return false;
	}
	
	/*private*/getConcernedObjectTrain(){
		return this.concernedObject.parent;
	}
	
	// DELEGATED :
	/*public*/isConcernedPeopleNotLoaded(){
		return !this.concernedObject.isLoaded();
	}
	
	// DELEGATED :
	/*public*/getIsDeadAndUpdateHabitationAffectationIfNeeded(){
		return this.concernedObject.getIsDeadAndUpdateHabitationAffectationIfNeeded();
	}
	
	// DELEGATED :
	/*public*/wait(){
		return this.concernedObject.wait();
	}
	
	// DELEGATED :
	/*public*/isRestNecessaryAndPossible(){
		return this.concernedObject.isRestNecessaryAndPossible();
	}
	
	// DELEGATED :
	/*public*/goToRest(){
		this.concernedObject.goToRest();
	}
	
}
