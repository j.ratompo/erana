
const HABITATION_BUILD_REFRESH_RATE_MILLIS=1000;


class Habitation extends DockableEquipment
// extends FactoryEquipment
{
	
	
	constructor() {
		super();

		this.occupations=[];
		
	}
	
	// INIT
	
	
	// METHODS
	
	// =================== GAME RULES METHODS ===================
	
	
	
	/*public*/hasReachedMaxCapacity(){
		return this.maxCapacity<=this.occupations.length;
	}
	
	/*public*/isOccupant(people){
		return !!foreach(this.occupations,(occupation)=>{
			if(occupation.people===people)	return true;
		}); /*forced to boolean*/
	}
	

	/*public*/getNeededCostsNames(){
		
		let costsNames=[];
		
		let restCostsByJob=this.restCostsByJobType;
		if(restCostsByJob){
			foreach(restCostsByJob,(restCostsForOneJob,jobName)=>{
				foreach(restCostsForOneJob.costs,(restCost,restCostName)=>{
					costsNames.push(restCostName);
				},(restCostValue)=>{	return	0<restCostValue; /*we exclude the possible wastes*/	});
			});

		}
		
		return costsNames;
	}
	
	/*public*/affect(people){
		let index=this.occupations.length;
		let totalWidth=this.size.w;
		let numberOfSlots=this.maxCapacity;
		let step=Math.round(totalWidth/numberOfSlots);
		let positionX=Math.floor(step*index-totalWidth*.5); // (Because all objects are in a «center x bottom» center mode)
		this.occupations.push( {positionX:positionX, people:people} );
		return this;
	}
	/*public*/unaffect(people){
		if(!this.isOccupant(people))	return;
		
		foreach(this.occupations,(occupation)=>{

			if(occupation.people===people){
				remove(this.occupations, occupation);
			}

		});
		
		return this;
	}
	/*public*/canAffect(people){
		if(this.isPending())		  	return false;
		if(this.isOccupant(people))	return false;
		return !this.hasReachedMaxCapacity()
	}
	
	/*public*/doOnDismantle(){
		
		foreach(this.occupations,(occupation)=>{
			occupation.people.setNoAffectedHabitation();
		});
		
		this.occupations=[];
	}
	
	
	/*public*/getSlotPositionForPeople(people){
		let isOccupant=false;
		
		let posX=foreach(this.occupations,(occupation)=>{
			if(occupation.people===people){
				isOccupant=true;
				return occupation.positionX;
			}
		});
		
		if(isOccupant)	return {x:posX,y:0};
		return {x:0,y:0};
	}
	
	
	
}
