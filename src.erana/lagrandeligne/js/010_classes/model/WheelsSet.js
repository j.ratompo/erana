
class WheelsSet{

	constructor() {

		this.isVisible=true;
		this.position=new Position(this);

		this.isMovable=true;

		this.level=1;
		
		this.img=new GameImage(this).addExtensionImage("pending",{opacity:0.2});
		this.anim=new GameAnimation(this);
		this.sparksAnim=new GameAnimation(this);

	}
	
	
	// INIT

	init(gameConfig){
		
		this.gameConfig=gameConfig;

		this.position.center=this.imageConfig._2D.center;
		
		this.img.init(gameConfig);
		
		// First-time init :
		this.getGameAnimation().init(gameConfig).loadSprite2DAndSet().start();

		
		this.sparksAnim.init(gameConfig).loadSprite2DAndSet("default","sparksImageConfig").start();

		
	}
	
	// METHODS
	
	
	getGameAnimation(){
		return this.anim;
	}

	getIsVisible(){
		return this.isVisible;
	}
	setIsVisible(isVisible){
		this.isVisible=isVisible;
	}
	
	
	getIsMovable(){
		return this.isMovable;
	}
	setIsMovable(isMovable){
		this.isMovable=isMovable;
	}
	
	// !!! CAUTION : handled in Train.draw() to override the usual children drawing sequence !!!
//	getDrawables(){
//		return [this.gameSmokePits];
//	}
	
	
	/*public*/canDrawSmokePits(){

		let platform=this.parent;
		let train=platform.parent;
		let speed=train.getSpeed();
		
		return this.smokeSpeedThreshold<speed;
	}
	
	
	// SPECIAL CASE (of a single smoke pit) :
	/*public*/drawSmokePit(ctx,camera){
		if(this.gameSmokePit)		this.gameSmokePit.updateStateAndDraw(ctx,camera);
	}

	
	
	draw(ctx,camera,lightsManager){
		
//	if(!this.img.canDraw(camera))	return false;
		if(!this.anim.canDraw(camera))	return false;

		let platform=this.parent;
		let train=platform.parent;
		let speed=train.getSpeed();
		
		if(!this.parent.isPending()){
			
			let anim=this.getGameAnimation();
			// If we have to change the animation :
			if(this.runningLowSpeedThreshold<speed){  // case «runningLow» or «runningHigh» 
				if(this.runningHighSpeedThreshold<speed){ // case «runningHigh»
					if(anim.getActionType()!=="runningHigh")	anim.loadSprite2DAndSet("runningHigh");
				}else if(anim.getActionType()!=="runningLow"){ // case «runningLow»
					anim.loadSprite2DAndSet("runningLow");
				}
			}else if(anim.started){
				if(anim.getActionType()!=="default")	anim.loadSprite2DAndSet();
			}
			
			anim.draw(ctx,camera,lightsManager);
			
			if(train.isAutoBraking()){
				this.sparksAnim.draw(ctx,camera,lightsManager);
			}
			
//		this.img.draw(ctx,camera,lightsManager);
		
		}else{
			
			this.img.drawExtensionImage(ctx,camera,"pending",.5);
			
		}
			
			
		return true;
	}


}
