
class RepairManager extends JobManager{
	
	constructor(concernedObject,gameConfig){
		super(concernedObject,gameConfig);

	}
	
	/*public*/start(selfParam,args){

		super.start(selfParam,args);

		const platform=args.platform;

		if(!platform.getDockableEquipment()){
			// MESSAGE
			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("aucunÉquipementARéparer"));
			return;
		}
		
		
		let overEquipment=platform.getOverEquipment();
		let dockableEquipment=platform.getDockableEquipment();
		if(		 overEquipment && overEquipment.grandeLigneGauge.isAtMaxCondition()
				&& dockableEquipment && dockableEquipment.grandeLigneGauge.isAtMaxCondition()){
			// MESSAGE
			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("impossibleDeRéparer"));
			return;
		}
		
		
		const self=this;
		this.mainRoutine=getMonoThreadedRoutine(this, BUILD_REFRESH_RATE_MILLIS,false).setDurationTimeFactorHolder(window.eranaScreen).start(this);
		
		
		let minPercentage=100;
		
		let timeRatio=(BUILD_REFRESH_RATE_MILLIS/(durationMillis*window.eranaScreen.getDurationTimeFactor()));
		self.percentageStep=timeRatio*100;
		
		self.repairJobInfos.platform=platform;
		
		self.repairJobInfos.equipmentsToRepair=[];
		
		
		if(overEquipment && !overEquipment.grandeLigneGauge.isAtMaxCondition()){

			let gaugeMax=overEquipment.grandeLigneGauge.gaugeMax;
			let gaugeValue=overEquipment.grandeLigneGauge.gaugeValue;
//			let amountToRepair=gaugeMax-overEquipment.grandeLigneGauge.gaugeValue;
//		let gaugeValueStep=timeRatio*amountToRepair;
			let gaugeValueStep=timeRatio*gaugeMax;
			
			let percentage=(gaugeValue/gaugeMax)*100;
			if(percentage<minPercentage)	minPercentage=percentage;
			
			self.repairJobInfos.equipmentsToRepair.push( {equipment:overEquipment,stepValue:gaugeValueStep} );
		}
		if(dockableEquipment && !dockableEquipment.grandeLigneGauge.isAtMaxCondition()){
			
			let gaugeMax=dockableEquipment.grandeLigneGauge.gaugeMax;
			let gaugeValue=dockableEquipment.grandeLigneGauge.gaugeValue;
//			let amountToRepair=gaugeMax-dockableEquipment.grandeLigneGauge.gaugeValue;
//		let gaugeValueStep=timeRatio*amountToRepair;
			let gaugeValueStep=timeRatio*gaugeMax;

			let percentage=(gaugeValue/gaugeMax)*100;
			if(percentage<minPercentage)	minPercentage=percentage;
			
			self.repairJobInfos.equipmentsToRepair.push( {equipment:dockableEquipment,stepValue:gaugeValueStep} );
		}

		self.percentage=minPercentage;
		
		if(empty(self.repairJobInfos.equipmentsToRepair)){
			// MESSAGE
			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("aucunBesoinDeRéparer"));

			/*super*/this.pause(this);

		}

		return this;
	}
	
	
	/*private*/doOnEachStep(args){

		const self=this;
		self.percentage+=self.percentageStep;

		foreach(selfParam.repairJobInfos.equipmentsToRepair,(equipmentToRepair)=>{
			equipmentToRepair.equipment.grandeLigneGauge.repairCondition(equipmentToRepair.stepValue);
		});
					
	}
	
	/*private*/terminateFunction(){
		return super.isJobFinished(this);
	}

	/*private*/doOnStop(args){
		const selfParam=this;						
		super.stop(selfParam,args);
	}

	
}