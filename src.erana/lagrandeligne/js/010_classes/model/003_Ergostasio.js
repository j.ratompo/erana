
class Ergostasio extends FactoryEquipment{
	
	
	constructor() {
		super();
		
		// TODO :
//		this.moverLoweringAnimation=getMoverMonoThreaded(this,false,null,{interruptable:false});

		
		this.constructionManager=null;
		this.repairManager=null;
		this.dismantleManager=null;
		
		this.activeJobManager=null;
		

	}
	
	// INIT
	
	init(gameConfig){
		super.init(gameConfig);
		
		const self=this;
		
		this.constructionManager=new ConstructionManager(this,gameConfig,false);
		
		this.repairManager=new RepairManager(this,gameConfig)
			.setListener("doOnStart",(args)=>{/*super*/self.startProductionAnimation();})
			.setListener("doOnSuspend",(args)=>{/*super*/self.pauseProductionAnimation();})
			.setListener("doOnResume",(args)=>{/*super*/self.resumeProductionAnimation();})
			.setListener("doOnJobEnd",(args)=>{/*super*/self.stopProductionAnimation();});
		
		this.dismantleManager=new DismantleManager(this,gameConfig)
			.setListener("doOnStart",(args)=>{/*super*/self.startProductionAnimation();})
			.setListener("doOnSuspend",(args)=>{/*super*/self.pauseProductionAnimation();})
			.setListener("doOnResume",(args)=>{/*super*/self.resumeProductionAnimation();})
			.setListener("doOnJobEnd",(args)=>{/*super*/self.stopProductionAnimation();});	
		
	
	}
	
	// METHODS
	
	
		
		
	
	// =================== GAME RULES METHODS ===================
	
	// Ergostasio :
	
	
	doOnStop(){
		// TODO : Develop...
		
//		// DBG
//		console.log("ERGOSTASIO ARRIVED !!!");


		// TODO : ...
		// We start the lowering animation if needed :
//		let train=selfParam.parent;
//		let platform=train.getPlatformAtPosition(selfParam.getPosition());
//		if(!platform){
//			
//			let destination={y:selfParam.getPosition().y-Math.floor(selfParam.size.h/2)};
//
//			selfParam.moverLoweringAnimation.setAbsoluteSpeedY(selfParam.loweringAnimationSpeed)
//			.setDoOnStopSecondaryFunction(function(){
//
//				// DBG
//				console.log("ERGOSTASIO ARRIVED 2 !!!");
//				
//			}).doStartUntilDestinationReach(destination);
//		}
		
		
	}
	
	
	
	
	// These methods are used to build other platforms, and other tasks :	
	startMovingLeft(event,speedXParam){
		
		
		let destination={x:this.getPosition().x-this.size.w
//				,y:0
		};
		
		this.moveToDestination(destination,speedXParam);

	}
	
	
	
	startMovingRight(event,speedXParam){
		
		let destination={x:this.getPosition().x+this.size.w
	//			,y:0
	  };
		
		this.moveToDestination(destination,speedXParam);
	}
	
	
	moveToDestination(destination,speedXParam){
		
		
		let self=this;
		if(this.busy) {
			// MESSAGE
			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("impossibleDeSeDéplacerEnFonctionnement"));
			return;
		}
		
		
		let train=this.getTrain();
		if(train.getErgostasioAtPosition(destination)){
			// MESSAGE
			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("uneErgostasioEstDéjàStationnéeACetEndroit"));
			return;
		}
		
		let mover=this.getMover().setAbsoluteSpeedX(speedXParam);
		
		let speedXParamPillar=.2*speedXParam/window.eranaScreen.getDurationTimeFactor(); // MAGIC NUMBER :  2 for 10 x speed;
		
		const position=this.getPosition();
		if(0<destination.x-position.x){//MOVE RIGHT
		
			// We cannot move the ergostasio past the locomotive
			// OLD, NOT ANYMORE : + one platform :
			if(this.isAtFirstPlatform()) {
				// MESSAGE
				// We cannot move the ergostasio past the locomotive:
				window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("impossibleDeSeDéplacerPlusLoin"));
				return;
			}
			
			
			// This is a quick hack to replace the right pillar in its original position :
			// and to avoid to make and use a mover in the pillar :
			mover.setDoOnEachSecondaryFunction(function(){
					let trainLocal=self.getTrain();
					let rightPillar=self.pillars[1];
					let platformLocal=trainLocal.getPlatformAtPosition(position);
					// If ergostasio steps again inside of the last platform :
					if(!platformLocal && rightPillar.position.x!==self.normalRightPillarPositionX){
						rightPillar.position.x-=speedXParamPillar;
					}
				})
			.setDoOnStopSecondaryFunction(function(){
				let trainLocal=self.getTrain();
				let rightPillar=self.pillars[1];
				let platformLocal=trainLocal.getPlatformAtPosition(position);
				// If ergostasio steps again inside of the last platform :
				if(!platformLocal && rightPillar.position.x!==self.normalRightPillarPositionX){
					rightPillar.position.x=self.normalRightPillarPositionX;
				}
			});
		
		}else{//MOVE LEFT
			
			
			// We cannot move the ergostasio past the last platform
			// + one platform (it can stand above the empty space to build a new platform) :
			if(this.isPastLastPlatform()) {
				// MESSAGE
				window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("impossibleDeSeDéplacerPlusLoin"));
				return;
			}

			let rightPillar=this.pillars[1];
			if(!this.normalRightPillarPositionX)	this.normalRightPillarPositionX=rightPillar.position.x;

			
			// This is a quick hack to place the right pillar on the last platform when the ergostasio moves outside of the last platform :
			// and to avoid to make and use a mover in the pillar :
			mover.setDoOnEachSecondaryFunction(function(){
				let trainLocal=self.getTrain();
				let platformLocal=trainLocal.getPlatformAtPosition(position);
				// If ergostasio steps outside of the last platform :
				if(!platformLocal){
					let rightPillarLocal=self.pillars[1];
					rightPillarLocal.position.x+=speedXParamPillar;
				}
			})
			.setDoOnStopSecondaryFunction(function(){
				let trainLocal=self.getTrain();
				let platformLocal=trainLocal.getPlatformAtPosition(position);
				// If ergostasio steps outside of the last platform :
				if(!platformLocal){
					let rightPillarLocal=self.pillars[1];
					rightPillarLocal.position.x=self.normalRightPillarPositionX+self.pillarMargin;
				}
			});
		
		}
		
		mover.doStartUntilDestinationReach(destination);
		
	}


	
	/*private*/isPastLastPlatform(){
		const train=this.getTrain();
		return (this.getPosition().x<train.getLastPlatform().position.x);
	}
	
	
	
	/*public*/repair(event,durationMillis){

		if(!this.canStartNewJob())	return;
		
		// We set the current job manager as the repair : 
		this.activeJobManager=this.repairManager;
		
		const train=this.getTrain();
		const platform=train.getPlatformAtPosition(this.getPosition());

		this.activeJobManager.start(platform);
		

	}
	
	
	
	dismantle(event,durationMillis){

		if(!this.canStartNewJob())	return;
		
		// We set the current job manager as the repair : 
		this.activeJobManager=this.dismantleManager;
		
		const train=this.getTrain();
		const platform=train.getPlatformAtPosition(this.getPosition());
		
		this.activeJobManager.start(platform).then((selfParam)=>{/*DO NOTHING*/});
		
	}
	

	cancelJob(){
		if(!this.activeJobManager)	return;
		
		// TODO : FIXME : Rembourser les ressources !!

		this.activeJobManager.cancel(this);

		// We set the current job manager to null : 
		this.activeJobManager=null;
	}
	
	pauseJob(){
		this.activeJobManager.pauseJob(this.activeJobManager);
	}

	resumeJob(){
		// TODO : FIXME : Attention au 10% gratuit, ça crée un bug exploit !!
		this.activeJobManager.resumeJob(this.activeJobManager);
	}
	
	/*public*/hasPausedJob(){
		return this.activeJobManager.isJobPaused(this.activeJobManager);
	}
	
	buildPlatform(event, buildsConfigs){

		if(!this.canStartNewJob())	return;
		
		const train=this.getTrain();
		const position=this.getPosition();
		const platform=train.getPlatformAtPosition(position);
		

		if(platform){
			// MESSAGE
			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("unePlateformeExisteDéjàIci"));
			return null;
		}
		 
		const adjacentPlatformWhereResourcesAre=train.getClosestPlatform(position);

		
		const protoToBuildConf=getAt(buildsConfigs,0);
		if(!protoToBuildConf || protoToBuildConf.type!=="platform"){
			// TRACE
			lognow("WARN : No platform prototype config found ! Aborting build.");			
			return;
		}

	
		const protoToBuildType=protoToBuildConf.type;
		const protoToBuildName=getKeyAt(buildsConfigs,0);
		const chosenProtoToBuild={ prototypeName:protoToBuildName, prototypeType:protoToBuildType, protoConfig:protoToBuildConf };
		
		
		this.plantBuildingJob(adjacentPlatformWhereResourcesAre,chosenProtoToBuild);


	}
	
	/*private*/plantBuildingJob(adjacentPlatformWhereResourcesAre, chosenProtoToBuild){

		const self=this;
		const constructionManager=this.constructionManager;

		// We set the current job manager as the construction : 
		this.activeJobManager=constructionManager;

		constructionManager
			.setListener("doOnStart",(args)=>{/*super*/self.startProductionAnimation();})
			.setListener("doOnSuspend",(args)=>{/*super*/self.pauseProductionAnimation();})
			.setListener("doOnResume",(args)=>{/*super*/self.resumeProductionAnimation();})
			.setListener("doOnJobEnd",(args)=>{
				/*super*/self.stopProductionAnimation();
				const prototypeType=args.prototypeType;
				// We replace the pillars if necessary :
				if(prototypeType==="platform"){
					// TODO : FIXME : QUICK FIX THAT NEEDS TO BE REFINED !
					// If ergostasio steps again inside of the last platform :
					let rightPillar=self.pillars[1];
					rightPillar.position.x=self.normalRightPillarPositionX;
				}
				
				constructionManager.clearListeners();
		});
				
		
		
		// Only platform building should be possible at this point, so we start directly :
		this.activeJobManager.start(adjacentPlatformWhereResourcesAre, chosenProtoToBuild);
			
	}

	
	/*private*/getPlatform(){
		const train=this.getTrain();
		const position=this.getPosition();
		let platform=train.getPlatformAtPosition(position);
		if(!platform){
			platform=train.getClosestPlatform(position);
		}
		return platform;
	}
	
	contributeToCurrentJob(event,params){
		// TODO...
	}
	
	// UNUSED
//	buildBasics(event,buildsConfigs){
//		this.buildPrompt(event,buildsConfigs);
//	}
	// UNUSED
//	buildProductions(event,buildsConfigs){
//		this.buildPrompt(event,buildsConfigs);
//	}
	// UNUSED
//	buildOverEquipments(event,buildsConfigs){
//		this.buildPrompt(event,buildsConfigs);
//	}
	
	
	
	
	/*private*/canStartNewJob(){
		
		if(this.busy){
			// MESSAGE
			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("unProcessusEstDéjàEnCours"));
			return false;
		}
		
		if(this.getMover().isStarted()){
			// MESSAGE
			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("impossibleDeConstruireEnDéplacement"));
			return false;
		}
		
		return true;
	}
	
	
//	// UNUSED
//	/*private*/buildPrompt(event,buildsConfigs){
//		
////		// DBG
////		console.log("buildPrompt",buildsConfigs);
//		
//		if(!this.canStartNewJob())	return;
//		
//		const train=this.getTrain();
//		const position=this.getPosition();
//		const platform=train.getPlatformAtPosition(position);
//		
//		const self=this;
//		this.constructionManager.promptConstructions(buildsConfigs,train,position,platform).then((args)=>{
//				const protoToBuild=args.result;
//				self.startBuildingErgostasioJob(platform,protoToBuild);
//		});
//
//	}
	
	
//	/*private*/buildDirectly(event,buildsConfigs){
//		
////		// DBG
////		console.log("buildDirectly",buildsConfigs);
//		
//		if(!this.canStartNewJob())	return;
//		
//		const train=this.getTrain();
//		const position=this.getPosition();
//		const platform=train.getPlatformAtPosition(position);
//		
//		const protoToBuildConf=getAt(buildsConfigs,0);
//		if(!protoToBuildConf || protoToBuildConf.type!=="platform"){
//			// TRACE
//			lognow("WARN : No platform prototype config found ! Aborting build.");			
//			return;
//		}
//		
//		const protoToBuildType=protoToBuildConf.type;
//		const protoToBuildName=getKeyAt(buildsConfigs,0);
//		const chosenProtoToBuild={ prototypeName:protoToBuildName, prototypeType:protoToBuildType, protoConfig:protoToBuildConf };
//
//		// this.startBuildingErgostasioJob(platform,chosenProtoToBuild);
//		
//	}

		
	
	
	/*public*/getResourcesRepository(){
		let platform=this.getPlatform();
		return platform.resourcesRepository;
	}
	
	/*private*/isAtFirstPlatform(){
		const position=this.getPosition();
		return (0<=position.x);
	}
	
	
	// ********************************************************
	
	
	/*private*/checkIfBuildingJobCanResumeAndResumeIfPossible(){
		if(!this.activeJobManager)	return;
		
		if(super.isBusy() && this.hasPausedJob() && !this.activeJobManager.hasInsufficientResources()){
			// MESSAGE
			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("ressourcesUtilisablesReprise"));
			
			this.resumeJob();
		}
	}

	
	 // ****************************
	
	
	
	// TODO : FIXME : Develop an use position.collidesWith(...) instead :
	/*public*/isOnPosition(pos){
		let demiWidth=Math.round(this.size.w/2);
		return (this.getPosition().x-demiWidth<pos.x && pos.x<=this.getPosition().x+demiWidth);
	}
	
	
	/*public*/getOverridingPositionReferenceObject(){
		return this;
	}
	
	
	
	/*public*/getNeededCostsNames(){
		if(!this.activeJobManager)	return [];
		return this.activeJobManager.getNeededCostsNames();
	}
	
	
	/*private*/getPosition(){
		return this.position;
	}
	
	/*public*/getActiveJobManager(){
		return this.activeJobManager;
	}
	
	
	/*public*/removeFromParent(){
			this.parent.removeErgostasio(this);
	}
	
	
	
// //	 SPECIAL CASE
// //	 (Necessary for pillars children moving :)
	getDrawables(){
		return [this.pillars];
	}
	
	
	
	// --------------------------------------------------------------
	
	
//	drawInBack(ctx,camera,lightsManager){
//		if(!this.getGameImage().canDraw(camera))	return false;
//
//		// SPECIAL CASE
//		foreach(this.pillars,(p)=>{
//			p.drawInBack(ctx,camera,lightsManager);
//		});
//		
//		
//		return true;
//	}


	/*public*/drawInUI(ctx,camera){
		// CAUTION : Thumbnail must be drawn at all times !		
		super.drawInUI(ctx,camera);

		if(this.img && !this.img.canDraw(camera))	return false;

		let gameConfig=this.gameConfig;
		if(contains(gameConfig.projection,"2D")){
							
			if(!this.activeJobManager || !this.activeJobManager.isRunning()/*excludes case ==0*/)	return false;
		
			this.activeJobManager.drawInUI(ctx,camera,this.getPosition());
			
		}
	
	}
	
	draw(ctx,camera,lightsManager){
		
		this.checkIfBuildingJobCanResumeAndResumeIfPossible();

		if(this.activeJobManager)	this.activeJobManager.doStep();
		
		this.buildingAnimation.draw(ctx,camera,lightsManager);	
		
		super.draw(ctx,camera,lightsManager);

	}
	
	

}
