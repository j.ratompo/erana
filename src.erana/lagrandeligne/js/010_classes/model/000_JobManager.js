
class JobManager{
	
	constructor(concernedObject,gameConfig){
		
		this.concernedObject=concernedObject;
		this.gameConfig=gameConfig;
		
		
		this.initVariables();

		this.listeners=this.getDefaultListeners();
		
		this.mainRoutine=null;


		this.workForceManager=new WorkForceManager(this,gameConfig);
		
		this.jobPaused=false;
		
//	// DBG
//	this.uuid="JOBMANAGER"+getUUID("short");
		
	}

	/*private*/initVariables(){
		this.percentage=null;
		this.percentageStep=0;
		
		this.platform=null;
		this.costs=null;
		this.costsSteps=null;
	}
		

	/*protected*/start(selfParam, args){
		
		this.percentage=0;
		
		// Here selfParam is a RollingPlatorm :
		this.getListener("doOnStart")(args);

	}

	// DELEGATED
	/*public*/doStep(args={}){
		if(this.mainRoutine){
			this.mainRoutine.doStep(this, args);
		}
	}
	
	/*protected*/doOnEachStep(args){
		
		const selfParam=this;
		
		// ***
		// CAUTION : Paused jobs still must perform their doStep(), in order to be able to check their suspension conditions !!
		// ***
		
		if(!this.isPaused(selfParam) && selfParam.hasInsufficientResources()){
			// MESSAGE
			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("pasAssezDeRessourcesEnPause"));
			
			selfParam.pause(selfParam, args);
			return;
		}
		
		
		let resourcesRepository=selfParam.concernedObject.getResourcesRepository();
		resourcesRepository.consumeStocks(selfParam.costsSteps);

		selfParam.percentage=Math.min(100,selfParam.percentage+selfParam.percentageStep);
		
		
		selfParam.getListener("doOnStep")(args);
	}
	
	
	
	/*protected*/pauseJob(selfParam,args={}){
		// NO : The underlying routine must run at all times ! : this.mainRoutine.pause(selfParam);
		this.jobPaused=true;
		selfParam.getListener("doOnSuspend")(args);
	}

	/*protected*/resumeJob(selfParam,args={}){
		 // NO : The underlying routine must run at all times ! : this.mainRoutine.resume(selfParam);
		this.jobPaused=false;
		selfParam.getListener("doOnResume")(args);
	}
	
	/*public*/isJobPaused(selfParam){
		return selfParam.jobPaused;
	}
	
	/*protected*/cancel(selfParam,args={}){
		// Case «cancel job» :
		if(this.isJobFinished(selfParam))	return;
		
		args.isCancelled=true;
		
		this.mainRoutine.stop(args);
		this.initVariables();
		
		this.getListener("doOnJobEnd")(args);
	}

	/*protected*/stop(selfParam,args){
		// Case «job has finished» :
		if(!selfParam.isJobFinished(selfParam))	return;
		selfParam.initVariables();
		
		selfParam.getListener("doOnJobEnd")(args);
	}

	/*public*/isRunning(){
		return this.getPercentage()!=null;
	}
	
	/*private*/isJobFinished(selfParam){
//		// NO : To avoid one step in extra :
//		const result=(100-selfParam.percentageStep <= selfParam.percentage);
		const result=(100 <= selfParam.percentage);
		return result;
	}
		
	/*public*/getPercentage(){
		return this.percentage;
	}
	
	/*public*/setListener(key, method){
		this.listeners[key]=method;
		return this;
	}
		
	/*public*/getListener(key){
		return this.listeners[key];
	}
	
	/*public*/getNeededCostsNames(){
		let costsNames=[];

		if(!this.isRunning())	return costsNames;

		let costs=this.costs;
		if(costs){
			foreach(costs,(cost,costName)=>{
				costsNames.push(costName);
			},(costValue)=>{	return	0<costValue; /*we exclude the possible wastes*/	});
		}
		
		return costsNames;
	}
	
	/*public*/hasInsufficientResources(){
		let resourcesRepository=this.concernedObject.getResourcesRepository();
		const costsSteps=this.costsSteps;

		let insufficientResourcesList=resourcesRepository.getInsufficientResourcesList(costsSteps);
		if(!empty(insufficientResourcesList)){
			return true;
		}
		return false;
	}	
	
	/*public*/clearListeners(){
		this.listeners=this.getDefaultListeners();
		return this;
	}

	/*private*/getDefaultListeners(){
		return {doOnStart:()=>{/*DO NOTHING*/},doOnStep:()=>{/*DO NOTHING*/},doOnSuspend:()=>{/*DO NOTHING*/},doOnResume:()=>{/*DO NOTHING*/},doOnJobEnd:()=>{/*DO NOTHING*/},};
	}
	
	/*protected*/getWorkForceManager(){
		return this.workForceManager;
	}

	/*public*/setWorkersCount(train,value,inputConfig){
		this.getWorkForceManager().setWorkersCount(train,value,inputConfig.max);
	}
	
	/*public*/getAffectedWorkersCount(){
		return this.getWorkForceManager().getAffectedWorkersCount();
	}
	
	/*public*/hasAvailableWorkPositions(){
		return this.getWorkForceManager().hasAvailableWorkPositions();
	}
	
	/*public*/addWorkerPeople(people){
		this.getWorkForceManager().addWorkerPeople(people);
	}
	
	/*public*/drawInUI(ctx,camera,refPosition){

		if(!refPosition)	return false;
		
		const position=refPosition;
		
		const gameConfig=this.gameConfig;
		if(contains(gameConfig.projection,"2D")){
							
			if(!this.isRunning()/*excludes case ==0*/)	return false;
		
			// CAUTION ! Coordinates for DRAWING are not the same than for CALCULUS !
			const drawableUnzoomedCoordinates=getDrawableZoomedIfNecessaryCoordinates2D(gameConfig,position.getParentPositionOffsetted(),camera);
			const x=drawableUnzoomedCoordinates.x;
			const y=drawableUnzoomedCoordinates.y-60;
			
			const zooms=gameConfig.zooms;
			let percentageStr=Math.round(this.getPercentage())+" %";
			drawSolidText(ctx,x,y,percentageStr,"#FFFFFF",16,"helvetica","bold",zooms);
			
		}

	}
	

}