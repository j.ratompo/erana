
class ConstructionManager extends JobManager{
	
	constructor(concernedObject, gameConfig, acceptsWorkers=true){
		super(concernedObject, gameConfig);
		

		this.subClass=null;
		this.prototypeName=null;
		this.prototypeType=null;
		this.newBuildingPosition=null;
		
		this.newPendingBuilding=null;
		
		this.acceptsWorkers=acceptsWorkers;

		
//	// DBG
//	this.uuid="CONSTRUCTIONMANAGER"+getUUID("short");
		
		
	}

	// 1)
	/*public*/promptConstructions(buildConfig,train,position,referencePlatform){
		
		
		let protosToBuild=[];
		let isFirstItemPassed=false;
		
		let self=this;
		let jsonStr="[";
		foreach(buildConfig, (protoToBuildConf, protoToBuildName)=>{
			
			let protoToBuildType=protoToBuildConf.type;
			
			if(!isFirstItemPassed)	isFirstItemPassed=true;
			else										jsonStr+=",";

			
			jsonStr+="{";
			
			// (Specialcase, needs «+"/"+» here :)
			let iconPath=getConventionalFilePath(self.gameConfig.basePath,"icons/"+protoToBuildName,"ui")+"/"+protoToBuildConf.iconSrc;

			jsonStr+="'width':'95px',";
//		jsonStr+="'height':'200px',";
			jsonStr+="'src':'"+iconPath+"',";
			
			if(protoToBuildConf.inactive)	jsonStr+="'inactive':true,";
			
			jsonStr+="'html':";  
			jsonStr+="'<h4>"+normalizeDisplayQuotesForJSONParsing(protoToBuildConf.title)+"</h4>";
			jsonStr+=" <p style=\\\"text-align:left;\\\">"+normalizeDisplayQuotesForJSONParsing(protoToBuildConf.description)+"</p>";
			
			if(!empty(protoToBuildConf.costs)){

				let costsHtml="<ul style=\\\"text-align:left;\\\">";
				foreach(protoToBuildConf.costs,(cost,costName)=>{
					costsHtml+="<li>"+costName+":"+cost+"</li>";
				});
				costsHtml+="</ul>";
				jsonStr+=" <div>"+i18n("coûts")+":<br />"+costsHtml;
			}
			
//		if(!empty(protoToBuildConf.consumptions)){
//			let consumptionsHtml="<ul style=\\\"text-align:left;\\\">";
//			foreach(protoToBuildConf.consumptions,(consumption,consumptionName)=>{
//				consumptionsHtml+="<li>"+consumptionName+":"+consumption+"</li>";
//			});
//			consumptionsHtml+="</ul>";
//			jsonStr+=" <div>"+i18n("consommations")+":<br />"+consumptionsHtml;
//		}

			
			jsonStr+="'";
			
			jsonStr+="}";
			
			
			protosToBuild.push( { prototypeName:protoToBuildName, prototypeType:protoToBuildType, protoConfig:protoToBuildConf } );
			
		});
		jsonStr+="]";
		
		
		const popupConfig=this.gameConfig.popupConfig;
		const promiseResult=promptWindow(
				i18n("construireUnÉquipement"),
				"uniqueChoice_icons",
				jsonStr,
				function(choiceIndex){
					
					if(choiceIndex==null/*(excludes case ==0)*/)	return null; // case we press on the (here, unuseful) OK button

					let protoToBuild=protosToBuild[choiceIndex];
					
					// DBG
					console.log("TO build:",protoToBuild);

					
//				const prototypeName=protoToBuild.prototypeName;
					const prototypeType=protoToBuild.prototypeType;
					
						
					if(referencePlatform){
						
						if(prototypeType==="platform"){
							// MESSAGE
							window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("unePlateformeExisteDéjàIci"));
							return null;
						}
						if(prototypeType==="ergostasio" && 1<train.getErgostasiosNumberAtPosition(position)){
							// MESSAGE
							window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("impossibleDeConstruirePlusDErgostasiosÀCetEmplacement"));
							return null;
						}
						if(prototypeType==="dockableEquipment" && referencePlatform.getDockableEquipment()){
							// MESSAGE
							window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("cettePlateformeADéjàUnÉquipement"));
							return null;
						}
						if(prototypeType==="overEquipment"){
							if(referencePlatform.getOverEquipment()){
								// MESSAGE
								window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("cettePlateformeADéjàUnSurÉquipement"));
								return null;
							}
							if(!referencePlatform.getDockableEquipment()){
								// WORKAROUND : To avoid a bug of clickables zIndex !!
								// MESSAGE
								window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("impossibleDeConstruireUnSurÉquipementSansUnÉquipementPrésent"));
								return null;
							}
							if(protoToBuild.protoConfig.isAllowedOnFirstPlatformOnly && !atFirstPlatform(position)){
								// MESSAGE
								window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n({"fr":"Cet équipement doit être sur la premiere plateforme.","en":"This equipment must be on the first platform."}));
								return null;
							}
						}
							
						
					}else{
						
					
						if(prototypeType!=="platform"){
							// MESSAGE
							window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("cetÉquipementDoitÊtreConstruitSurUnePlateforme"));
							return null;
						}
						if(train.hasReachedMaxPlatforms()){
							// MESSAGE
							window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("impossibleDeConstruirePlusDePlateformes"));
							return null;
						}
						
						
					}
					
					
					
					return protoToBuild;
				}, null, {displayMode:"modal:0.85",
									backgroundColor1:popupConfig.backgroundColor1,
									backgroundColor2:popupConfig.backgroundColor2,
									textColor:popupConfig.textColor,
				}
		).toPromise("OK");
		
		return promiseResult;
	}
	
	
	
	
	
	// 2) (or called directly as a 1) step) 
	/*public*/start(concernedObjectParam,args){

		let referencePlatform=concernedObjectParam;// IMPORTANT : same as this.concernedObject !
		
		let train=null;
		
		// Here we are in an Ergostasio platform building job :
		if(!referencePlatform){
			train=this.concernedObject.parent;
			// Then use parent's adjacent platform :
			const position=super.concernedObject.getPosition();
			const adjacentPlatformWhereResourcesAre=train.getClosestPlatform(position);
			referencePlatform=adjacentPlatformWhereResourcesAre;
			
		}else{
			// Case normal platform job :
			train=referencePlatform.parent;
		}
		
		
		super.start(referencePlatform,args);

		
		const sceneManager=window.eranaScreen.sceneManager;
		let prototypeNameParam=args.prototypeName;
		let prototypeTypeParam=args.prototypeType;
		let protoConfigParam=args.protoConfig;
		
		
		this.subClass=protoConfigParam.subClass;
		this.prototypeName=prototypeNameParam;
		this.prototypeType=prototypeTypeParam;
		this.percentage=0;
		
		
		const totalTimeMillis=protoConfigParam.timeToBuild*1000; // (in config, it is in seconds.)
		const timeRatio=BUILD_REFRESH_RATE_MILLIS/totalTimeMillis;
		
		this.newBuildingPosition=protoConfigParam.newBuildingPosition;
		
		this.costs=protoConfigParam.costs;

		
		this.percentageStep=timeRatio*100;
		// We calculate the costs steps :
		this.costsSteps={};
		const self=this;
		foreach(protoConfigParam.costs,(totalCost,costName)=>{
			const costStep=timeRatio*totalCost;
			self.costsSteps[costName]=costStep;
		});

		
		
		if(prototypeTypeParam==="ergostasio"){
				
				let newErgostasio=window.eranaScreen.createInstance("Ergostasio",prototypeNameParam);
				train.ergostasios.push(newErgostasio);
				newErgostasio.genericType="ergostasio";
				newErgostasio.costs=this.costs;
				newErgostasio.parent=train;
//NO : DONE IN THE INSTANCIATION MANAGER !
//			if(newErgostasio.init)						newErgostasio.init(self.gameConfig);
				// Because we know the client only here :
				if(newErgostasio.initWithParent)	newErgostasio.initWithParent();
				sceneManager.setPositionFirstTime(newErgostasio,{x:self.getPosition().x,y:self.getPosition().y},train);
				sceneManager.placeAllOnInit(newErgostasio);
				
				
				newErgostasio.setPending(true);
				this.newPendingBuilding=newErgostasio;

		}else if(prototypeTypeParam==="dockableEquipment" || prototypeTypeParam==="overEquipment"){
				
				
				let newBuildingPosition=nonull(self.newBuildingPosition,{x:0,y:0});

				let newEquipment=window.eranaScreen.createInstance(nonull(this.subClass, "DockableEquipment"),prototypeNameParam);
				
				if(prototypeTypeParam==="dockableEquipment"){
					referencePlatform.setDockableEquipment(newEquipment);
					newBuildingPosition.z=1;
					newEquipment.genericType="dockableEquipment";
				}else{
					referencePlatform.setOverEquipment(newEquipment);
					newBuildingPosition.z=0;
					newEquipment.genericType="overEquipment";
				}
				newEquipment.costs=this.costs;
				newEquipment.parent=referencePlatform;
//NO : DONE IN THE INSTANCIATION MANAGER !
//			if(newEquipment.init)						newEquipment.init(self.gameConfig);
				// Because we know the client only here :
				if(newEquipment.initWithParent)	newEquipment.initWithParent();
				sceneManager.setPositionFirstTime(newEquipment,newBuildingPosition,referencePlatform);
				sceneManager.placeAllOnInit(newEquipment);
				
				
				newEquipment.setPending(true);
				this.newPendingBuilding=newEquipment;

				
		} else if(prototypeTypeParam==="platform"){
			const sceneManager=window.eranaScreen.sceneManager;

			let newPlatform=window.eranaScreen.createInstance("RollingPlatform",prototypeNameParam);
			
			train.addRollingPlatform(newPlatform);
			newPlatform.genericType="platform";
			newPlatform.costs=concernedObjectParam.costs;
//NO : DONE IN THE INSTANCIATION MANAGER !
//		if(newPlatform.init)						newPlatform.init(concernedObjectParam.gameConfig);
			// Because we know the client only here :
			if(newPlatform.initWithParent)	newPlatform.initWithParent();
			
			const position=referencePlatform.getPositionNextToIt();
			sceneManager.setPositionFirstTime(newPlatform,{x:position.x,y:0},train);
			sceneManager.placeAllOnInit(newPlatform);
			
			
			newPlatform.setPending(true);
			this.newPendingBuilding=newPlatform;
			
		}
		
		this.mainRoutine=getMonoThreadedRoutine(this,BUILD_REFRESH_RATE_MILLIS,false).setDurationTimeFactorHolder(window.eranaScreen).start(this);


		if(super.hasInsufficientResources()){
			// MESSAGE
			sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("pasAssezDeRessourcesEnPause"));
			
			super.pauseJob(this);
		}

		return this;
	}
	
	
	/*private*/doOnEachStep(args){
		const selfParam=this;
		if((	 selfParam.hasInsufficientResources() 
			|| ( selfParam.acceptsWorkers && !super.getWorkForceManager().hasWorkersWorkingOnJob()))){

			if(!selfParam.isJobPaused(selfParam)){
				
				// MESSAGE
				if(selfParam.hasInsufficientResources())
					window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("pasAssezDeRessourcesEnPause"));
	
				// MESSAGE
				if(selfParam.acceptsWorkers && !super.getWorkForceManager().hasWorkersWorkingOnJob())	
					window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n("aucuneTravailleuse"));
				
				selfParam.pauseJob(selfParam, args);
			}
			return;
		}else if(
			selfParam.isJobPaused(selfParam)){
			
			selfParam.resumeJob(selfParam, args);
		}
		
		
		let resourcesRepository=selfParam.concernedObject.getResourcesRepository();
		
		// We reduce the costs steps according to the workforce occupation rate :
		// TODO : DEVELOP...

		let calculatedCostSteps=selfParam.costsSteps;
		if(selfParam.acceptsWorkers){
			calculatedCostSteps=super.getWorkForceManager().getReducedCostsSteps(calculatedCostSteps);
		}
		
		if(!calculatedCostSteps)		return;
		
		resourcesRepository.consumeStocks(calculatedCostSteps);

		let calculatedPercentageStep=selfParam.percentageStep;
		if(selfParam.acceptsWorkers){
			calculatedPercentageStep=super.getWorkForceManager().getReducedPercentageStep(calculatedPercentageStep);
		}
		selfParam.percentage=Math.min(100,(selfParam.percentage+calculatedPercentageStep));
		
		selfParam.getListener("doOnStep")(args);
	}
	
	/*private*/terminateFunction(){
		return super.isJobFinished(this);
	}
	
	/*private*/doOnStop(args){
		
		// Called after the termination condition :
		const selfParam=this;
																							
		if(args.isCancelled)	return;
		
		// We mark the new building as not pending anymore :
		selfParam.newPendingBuilding.setPending(false);

		let prototypeType=selfParam.prototypeType;

		args.prototypeType=prototypeType;

		super.stop(selfParam,args);
		
		super.getWorkForceManager().removeAllWorkersAndAffectations();

		window.eranaScreen.controlsManager.refreshInfoPad(selfParam.newPendingBuilding);

	}
	
		
	/*OVERRIDES*//*protected*/cancel(selfParam,args){

		// We remove the new building :
		this.newPendingBuilding.removeFromParent();
		this.newPendingBuilding=null;

		super.cancel(selfParam,args);
		
	}


}
