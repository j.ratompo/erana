
// FOR DEBUG ONLY :
const IS_DEBUG_PEOPLE=false;


const RESOURCE_IMAGE_SHIFT_X=2;
const RESOURCE_IMAGE_SHIFT_Y=11;
const PEOPLE_MESSAGE_PERSISTENCE_MILLIS=5000;


//statusChar="▷";// alt:▷
//statusChar="∎";// alt:▶
// alt:∎
const WAIT_GLYPH="◉";
const DELIVER_GLYPH="▷";
const TOOLS_GLYPH="🛠";
const GO_TO_REST_GLYPH="¬";
const DEATH_GLYPH="💀";
const RESTING_GLYPH="_";

const DEFAULT_REST_TIME_MILLIS=20000;
const DEFAULT_REST_TIME_VARIABILITY_MILLIS=4000;


class People extends GameCharacter{

	constructor() {
		super();
		
		this.jobType="transporter";
		
		/*OVERRIDES super BEHAVIOR :*/this.mover=getMoverMonoThreaded(this,false,{interruptable:false});

		this.loadedAmount=0;
		this.loadedResourceName=null;
		
		// DBG
		this.uuid=getUUID();
		
		this.destinationPlatform=null;
		this.workPlacePlatform=null;
	}
	
	// INIT
	initWithParent(){
		
		// We start the default automaton for this people :
		this.getGameAutomaton().start(this);
		
		this.refResourcesRepository=this.parent.getFirstPlatform().resourcesRepository;
		
		this.getMover().setDurationTimeFactorHolder(window.eranaScreen);

	}
	
	getIsMovable(){
		return this.isMovable;
	}
	setIsMovable(isMovable){
		this.isMovable=isMovable;
	}
	
	/*OVERRIDES*/move(){
		
		if(!this.grandeLigneGauge.isAlive()){
			super.stopMoving();
			return;
		}
		
		// Mover stepping :
		// Only for monothreaded movers :
		super.getMover().doStep();
		
	}
	
	/*private*/getTrain(){
		let train=this.parent;
		return train;
	}
	
	/*private*/ isMoving(){
		return super.getMover().isStarted();
	}
	
	/*public*/getPosition(){
		return this.position;
	}
	
	
	
	// --------------------------------------------------------------------------
	
	
	/*public*/getLoadedResourceName(){
		return this.loadedResourceName;
	}

	/*public*/getDestinationPlatform(){
		return this.destinationPlatform;
	}
	/*public*/setDestinationPlatform(destinationPlatform){
		this.destinationPlatform=destinationPlatform;
	}

	/*public*/getWorkPlacePlatform(){
		return this.workPlacePlatform;
	}
	/*public*/setWorkPlacePlatform(chosenPlatform){
		this.workPlacePlatform=chosenPlatform;
	}



	// PRIVATE METHODS




	
	// ---------- RESOURCES HANDLING LOGIC : ----------


	/*private*/getAllResourcesThatCanProvide(collidedStation
//			,resourceInNeedNameFilter=null
			){
			
	//	// DBG
	//	if(!self.isInTrain())
	//		if(IS_DEBUG_PEOPLE)	lognow(this.uuid+" :PAS DANS LE TRAIN !",self);
	
		
		let train=this.parent;
		
		let availableGivingResources=[];
		
		
		// First, we search resources that are giving in *collided station* :
		// Station resources are prioritary !
		if(collidedStation && train.isStopped()){
			
			availableGivingResources = merge(availableGivingResources, collidedStation.getGivingResources(
//					resourceInNeedNameFilter
					));

		}
		
		// We want all the transporters to unload the station first of all !
		if(empty(availableGivingResources)){
	
			// Second, we search resources that are giving in *train* :
			// We do not set a distance priority to ensure that we create no un-served zones :
			let resourcesGivingPlatforms=train.getResourcesGivingPlatforms();
			if(!empty(resourcesGivingPlatforms)){
				
//			let currentPlatform=train.getPlatformAtPosition(this.position);
				
				// We try to find a resource that a platform in train is willing to give :
				foreach(resourcesGivingPlatforms,(p)=>{
					
					availableGivingResources = merge(availableGivingResources, p.getGivingResources(
//							resourceInNeedNameFilter
					));
					
				});
				
			}
		}
		
		
		if(empty(availableGivingResources)){
//			// TRACE
//			if(IS_DEBUG_PEOPLE)	lognow(this.uuid+" :WARN : No available resource in a collided station nor on the train for resource «"+resourceInNeedName+"».");
			// If we have found no resource to go fetch :
			return null;
			
		}
		
		return availableGivingResources;
	}
	
	
	/*public*/getAllResourcesInNeed(resourceInNeedNameFilter=null){

		let train=this.parent;
		
		let collidedStation=train.getCollidedStation();

//	// We only ignore retrieving orders for needing resources calculation if we are not at a station :
//	let ignoreRetrievingOrder=(!!collidedStation);
//		// DBG
//		if(IS_DEBUG_PEOPLE)	lognow(this.uuid+" ignoreRetrievingOrder:"+ignoreRetrievingOrder);

		let resourcesNeedingPlatforms=train.getResourcesNeedingPlatforms(resourceInNeedNameFilter);
		
		// If no platforms can receive resources (ie. train is completly full) :
		if(empty(resourcesNeedingPlatforms))	return null;
		
		// We do not set a distance priority to ensure that we create no un-deserved zone
		
		// Station resources are prioritary !
		// If train is at a station, the we try to retrieve the present resources first !
		if(collidedStation && train.isStopped()){

			let prioritaryPlatforms=[];
			
			// We look in the station provided resources :
			foreach(collidedStation.resourcesRepository.resources,(r,rName)=>{
				
				// If any platform needs any one of them :
				foreach(resourcesNeedingPlatforms,(p)=>{
					
					let neededResourcesForPlatform=p.getNeedingResources(resourceInNeedNameFilter);
					
					let hasFoundNeededResourceInThisPlatform= !!foreach(neededResourcesForPlatform,(nr)=>{
						if(nr.name===rName)	return true;
					});

					if(hasFoundNeededResourceInThisPlatform){
						prioritaryPlatforms.push(p);
						return "break";
					}
				});
			
			},(r,rName)=>{	return 0<r.amount; });
			
			if(!empty(prioritaryPlatforms))		resourcesNeedingPlatforms=prioritaryPlatforms;
			
		}
		
		
		let platformInNeed=Math.getRandomInArray(resourcesNeedingPlatforms);
		
		let availableNeedingResources=platformInNeed.getNeedingResources(resourceInNeedNameFilter);
		return availableNeedingResources;
	}
	
	
	
	/*public*/getFallbackResourcesInNeed(resourceInNeedNameFilter=null){

		let train=this.parent;
		
		let resourcesWithFreeSpaceStoragePlatforms=train.getResourcesNeedingPlatforms(resourceInNeedNameFilter,true);
		// If no platforms can receive resources (ie. train is completly full) :
		if(empty(resourcesWithFreeSpaceStoragePlatforms))	return null;
		
		// We do not set a distance priority to ensure that we create no un-deserved zone :
		
		let platformWithFreeSpaceStorage=Math.getRandomInArray(resourcesWithFreeSpaceStoragePlatforms);
		
		// We need to ignore the retrieving state of the resources repository,
		// in the case of trying to find a fallback repository to deposit the loaded resource :
		let availableWithFreeSpaceStorageResources=platformWithFreeSpaceStorage.getNeedingResources(resourceInNeedNameFilter,true);
		return availableWithFreeSpaceStorageResources;
	}
	
	

	/*public*/loadResource(resourceToLoad){
		
		// DBG
		if(IS_DEBUG_PEOPLE)	lognow(this.uuid+"  LOAD RESOURCE resourceToLoad.name:"+resourceToLoad.name);
		
		if(this.isLoaded()){ // Then we already have a load !
			// DBG
			if(IS_DEBUG_PEOPLE)	lognow(this.uuid+"  ALREADY HAVING A LOAD this.loadedResourceName:"+this.loadedResourceName +" ; this.loadedAmount:"+this.loadedAmount);
			return false;
		}
		
		
		let amountToLoad=Math.min(this.maxLoad, resourceToLoad.amount);
		this.loadedAmount=Math.min(this.maxLoad, this.loadedAmount+amountToLoad);
		
		
		if(!this.isLoaded()){ // Then nothing has been loaded !

			// DBG
			if(IS_DEBUG_PEOPLE)	lognow(this.uuid+"  COULD NOT LOAD RESOURCE resourceToLoad.name:"+resourceToLoad.name+" ; this.loadedAmount:"+this.loadedAmount);
			
			return false;
		}
		
		// NOT SURE : Use resourceToUnload.repository.incrementStock(stockName, amount) instead ?
		resourceToLoad.amount=Math.max(0, resourceToLoad.amount-amountToLoad);
		window.eranaScreen.controlsManager.refreshInfoPad(resourceToLoad.parentPlace);
		
		this.loadedResourceName=resourceToLoad.name;

		// DBG
		if(IS_DEBUG_PEOPLE)	lognow(this.uuid+" :SUCCESSFUL LOAD :"+this.loadedAmount);
		
		return true;
	}
	

	/*public*/unloadResource(resourcePlaceToUnload){
		
		// DBG
		if(IS_DEBUG_PEOPLE)	lognow(this.uuid+"  UNLOAD RESOURCE resourcePlaceToUnload.name:"+resourcePlaceToUnload.name);
		
		if(!this.isLoaded()){ // Then there is no load !
			return false;
		}
		
		let maxLoadable=resourcePlaceToUnload.capacity-resourcePlaceToUnload.amount;
		let amountToUnload=Math.min(maxLoadable, this.loadedAmount);
		this.loadedAmount=Math.max(0, this.loadedAmount-amountToUnload);

		// NOT SURE : use resourcePlaceToUnload.repository.incrementStock(stockName, amount) instead ?
		resourcePlaceToUnload.amount=Math.min(resourcePlaceToUnload.capacity, resourcePlaceToUnload.amount+amountToUnload);
		
		if(this.isLoaded()){ // Then there is still a load !
			return false;
		}
		window.eranaScreen.controlsManager.refreshInfoPad(resourcePlaceToUnload.parentPlace);
		
		this.loadedResourceName=null;

		// DBG
		if(IS_DEBUG_PEOPLE)	lognow(this.uuid+" :UNLOAD : remaining:"+this.loadedAmount);
		
		return true;
		
	}
	
	
	// DELEGATEE :
	/*public*/isLoaded(){
		return 0<this.loadedAmount;
	}


	// ---------- RESTING LOGIC : ----------
	
	// DELEGATEE :
	/*public*/goToRest(){
		
		if(this.isInRest)		return;
		
		// Second we go to it :
		let affectedHabitation=this.affectedHabitation;
		if(!affectedHabitation) {
			return;
		}
		let habitationPlatform=affectedHabitation.parent;
		
		
		this.destinationPlatform=habitationPlatform;
		
		let destination={x:habitationPlatform.position.x+affectedHabitation.getSlotPositionForPeople(this).x};

		
		super.startMovingTo(destination,null,function(selfParam){
			
				// When we arrive at destination :
			
				let isAlive=selfParam.grandeLigneGauge.isAlive();
				if(!isAlive)	return;
				
				selfParam.isInRest=true;
				

				// End of rest :
				let timeMillis=DEFAULT_REST_TIME_MILLIS;
				if(selfParam.getJobType()){
					let restTimeMillis=(selfParam.restTimeMillisByJobType?selfParam.restTimeMillisByJobType[selfParam.getJobType()]:null);
					if(restTimeMillis){
						timeMillis=restTimeMillis;
					}
				}
				let absoluteVariability=nonull( (selfParam.restTimeVariabilityMillisByJobType?selfParam.restTimeVariabilityMillisByJobType[selfParam.getJobType()]:null)
																						,DEFAULT_REST_TIME_VARIABILITY_MILLIS);
				timeMillis+=Math.getRandomInt(absoluteVariability,-absoluteVariability);
				
				setTimeout(function(){
					if(selfParam.getJobType()){
						let affectedHabitation=selfParam.affectedHabitation;
						if(!affectedHabitation){
							// MESSAGE
							window.eranaScreen.sceneManager.uiManager.displayMessage("warn",PEOPLE_MESSAGE_PERSISTENCE_MILLIS, i18n("uneConcertadeNAPuSeReposerNullePart")); 
						}else{
							let restCostsByJobType=affectedHabitation.restCostsByJobType[selfParam.getJobType()];
							if(restCostsByJobType && restCostsByJobType.costs){
									let platform=habitationPlatform;
									let resourcesRepository=platform.resourcesRepository;
									if(empty( resourcesRepository.getInsufficientResourcesList( restCostsByJobType.costs ) )){
										resourcesRepository.consumeStocks( restCostsByJobType.costs );
										selfParam.grandeLigneGauge.repairCondition();
									}else{
										// MESSAGE
										window.eranaScreen.sceneManager.uiManager.displayMessage("warn",PEOPLE_MESSAGE_PERSISTENCE_MILLIS, i18n("uneConcertadeNAPasPuSeRestaurer")); 
									}
							}
						}
					}
					
					selfParam.isInRest=false;
					selfParam.getGameAutomaton().next();
					
				},timeMillis*window.eranaScreen.getDurationTimeFactor());

			
		},this.speed);
		
		
	}
	
	// DELEGATEE :
	/*public*/isRestNecessaryAndPossible(){
		
		let grandeLigneGauge=this.grandeLigneGauge;

		let isExhausted=grandeLigneGauge.isAlive() && !grandeLigneGauge.isFunctional();
		if(!isExhausted)	return false;
		
		// First we check if this people has an affected habitation
		if(!this.affectedHabitation){
			// If not, then we look in train to give her one :
			let train=this.getTrain();
			
			this.affectedHabitation=train.affectHabitation(this);
			
			if(!this.affectedHabitation){
//				 // TRACE
//				console.log("WARN : No habitation to affect found for this people.");
				return false;
			}
		}
		
		return true;
	}
	

	/*private*/isResting(){
		return this.isInRest;
	}
	
	
	// ---------- BUILDING JOB LOGIC : ----------
	
	
	
	// ---------- HABITATION LOGIC : ----------

	/*private*/unaffectHabitation(){
		if(!this.affectedHabitation)	return;
		this.affectedHabitation.unaffect(this);
		this.setNoAffectedHabitation();
	}

	/*public*/setNoAffectedHabitation(){
		this.affectedHabitation=null;
	}

	// DELEGATEE :
	/*public*/getIsDeadAndUpdateHabitationAffectationIfNeeded(){
		if(!this.grandeLigneGauge.isAlive()){
			this.unaffectHabitation();
			return true;
		}
		return false;
	}

	// ---------- PRESENCE LOGIC : ----------


	
	/*public*/isInTrain(){
		let train=this.parent;
		let offsettedPos=this.position.getParentPositionOffsetted();
		let trainBounds=train.getExtremities();
		return (trainBounds.min.x<=offsettedPos.x && offsettedPos.x<=trainBounds.max.x);
	}
	
	/*public*/isInStation(collidedStation){
		let offsettedPos=this.position.getParentPositionOffsetted();
		let stationBounds=collidedStation.position.getExtremities();
		return (stationBounds.min.x<=offsettedPos.x && offsettedPos.x<=stationBounds.max.x);
	}



	// ---------- COMMON LOGIC : ----------
	
	// DELEGATEE :
	/*public*/wait(){
		/*DO NOTHING*/
		
		// TRACE : 
		if(IS_DEBUG_PEOPLE)	lognow(this.uuid+" :WARN : Transporter still has a load ("+this.loadedResourceName+") but cannot do anything. Waiting.");
		
		this.getGameAutomaton().next();
	}
	
	/*private*/isFunctional(){
		return this.grandeLigneGauge.isFunctional();
	}
	
	/*public*/isAvailableForWork(){
		return !this.getJobType() || this.getJobType()==="transporter"; 
	}
	
	
	/*public*/getJobType(){
		return this.jobType;
	}
	
	/*public*/setJobType(newJobType){
		
		const hasChangedJob=(this.getJobType()!==newJobType );
		
		// We stop the current automaton for this people if necessary:
		let currentGameAutomaton=this.getGameAutomaton();
		if(currentGameAutomaton && hasChangedJob && currentGameAutomaton.isStarted()){
			currentGameAutomaton.pause();
		}
		
		this.jobType=newJobType;

		// We start the default automaton for this people if necessary:
		currentGameAutomaton=this.getGameAutomaton();
		if(currentGameAutomaton && hasChangedJob){
			if(!currentGameAutomaton.isStarted())		currentGameAutomaton.start(this);
			else																		currentGameAutomaton.resume();
		}

	}

	
	
	/*public*/isAlive(){
		return this.grandeLigneGauge.isAlive();
	}
	
	
	/*private*/checkFunctionnality(){
		this.grandeLigneGauge.checkFunctionnality();
	}
	
	/*public*/doOnChangeState(stateType, hasRecovered){
		// MESSAGES
		if(!hasRecovered){
			// NOT ANYMORE :
//			if(stateType==="functional")
//				window.eranaScreen.sceneManager.uiManager.displayMessage("warn",PEOPLE_MESSAGE_PERSISTENCE_MILLIS,
//						i18n("uneConcertadeEstÉpuisée")); 
			if(stateType!=="functional")
				window.eranaScreen.sceneManager.uiManager.displayMessage("warn",PEOPLE_MESSAGE_PERSISTENCE_MILLIS, i18n("uneConcertadeEstDécédée")); 
		}
			
	}
	
	
	
	/*private*/getGameAutomaton(){
		const self=this;
		const result=foreach(this.gameAutomatons, g=>{return g;}, g=>(g.getTaskName()===self.getJobType()));
		return result;
	}
	
	// ----------------------------------------------------------------------------
	
	// TODO ...
	drawResourceLoad(ctx,camera){
		
//	// DBG
//	if(IS_DEBUG_PEOPLE)	lognow(":>>>this.loadedAmount:"+this.loadedAmount);

		if(!this.isLoaded())	return;
		
		let loadedResourceName=this.loadedResourceName;
		if(!loadedResourceName)	return;
		
		let direction=this.direction;
		
		let resourceImage=this.refResourcesRepository.getImageForResourceName(loadedResourceName);
		
		const zooms=this.gameConfig.zooms;
		
		const drawableUnzoomedCoordinates=getDrawableZoomedIfNecessaryCoordinates2D(this.gameConfig,this.position.getParentPositionOffsetted(),camera,
														{w:resourceImage.width,h:resourceImage.height},{x:"center",y:"bottom"});
		let x=drawableUnzoomedCoordinates.x;
		let y=drawableUnzoomedCoordinates.y;
		
		// CAUTION : y axis is inverted !
		y+=RESOURCE_IMAGE_SHIFT_Y;

		if(direction==="left"){
			x=x-RESOURCE_IMAGE_SHIFT_X;
		}else{// "right"
			x=x+RESOURCE_IMAGE_SHIFT_X+this.size.w;
		}

		drawImageAndCenterWithZooms(ctx, resourceImage, x, y, zooms, {x:"center",y:"bottom"});

		
	}

	
	draw(ctx,camera,lightsManager){
		
		this.checkFunctionnality();
		
		
		this.getGameAutomaton().doStep();
		
		
		const train=this.parent;

		
//		// OLD :
//		// NO : We do not display the people if she is BEHIND THE TRAIN AND AT THE STATION and the train is stopped !
//		let collidedStation=train.getCollidedStation();
//		if(train.isStopped() && this.isInTrain() && collidedStation && this.isInStation(collidedStation))	return false;
		
		// We do not draw the people if she's outside the train, and the train is moving !
		if( (!train.isStopped() && !this.isInTrain()) 
				|| this.isResting()
				|| !this.grandeLigneGauge.isAlive()
				)	return false;
		
		
		if(!super.draw(ctx,camera,lightsManager))	return false;
		
		this.drawResourceLoad(ctx,camera);
		
		return true;
	}

	
	drawInUI(ctx,camera){
		
		let train=this.getTrain();
		
		if(!this.gameThumb
				// Only for selectable trains :
				|| !train.isSelectable()
				// We only display thumbs for people that are inside the scope of the train :
				|| !this.isInTrain()
//			|| this.isResting()
				) 
			return;
		

		let isAlive=this.grandeLigneGauge.isAlive();
		let isResting=this.isResting();
		
		let canDisplayThumb = isAlive && !isResting;
		const centeredCoords=this.gameThumb.drawDynamicThumbnailUI(ctx,camera,this.grandeLigneGauge,train,canDisplayThumb);
		if(!centeredCoords)	return;

		// OLD : We only displayed the UI icon if the people has a load :
//	if(this.isLoaded()){
		ctx.save();
		ctx.strokeStyle="#FFFFFF";
		ctx.font = "8px Arial";
		let stateName=this.getGameAutomaton().currentStateName;
		
		let statusChar="";
		

		if(isResting){
			statusChar=RESTING_GLYPH;
		}else{
			
			if(isAlive){
				
				if(stateName==="wait") {
					statusChar=WAIT_GLYPH;
					// DBG
					if(IS_DEBUG_PEOPLE)	lognow("	(DRAWING) "+this.uuid+"  en statut «wait» !",this);
					
				} else if(stateName==="goDeliverResource") {
					statusChar=DELIVER_GLYPH;

					// DBG
					if(IS_DEBUG_PEOPLE)	lognow("	(DRAWING) "+this.uuid+"  en statut «goDeliverResource» !",this);
					
				}else if(stateName==="goToRest") {
					statusChar=GO_TO_REST_GLYPH;
				}else if(stateName==="INITIAL"){
					// DBG
				 	if(IS_DEBUG_PEOPLE)	lognow("	(DRAWING) "+this.uuid+"  en statut «INITIAL» !",this);
				}else{
					if(this.getJobType()==="builder"){
						statusChar=TOOLS_GLYPH;
					}
				}

				
			}else{
				statusChar=DEATH_GLYPH;
			}
		}
				
		// DBG
		if(IS_DEBUG_PEOPLE)		statusChar+=this.loadedResourceName?this.loadedResourceName[0]:"";
		
		ctx.strokeText(statusChar,centeredCoords.x, centeredCoords.y -3);
		ctx.restore();
//	}
		
		
		// Wear over time :
		if(this.grandeLigneGauge && isAlive && !this.isInRest){
			if(this.grandeLigneGauge.drawGaugeInUI(ctx,this.gameThumb)){
				this.grandeLigneGauge.slowlyDamage(train.parent.daylightManager);
			}
		}
		
		
	}

	
}
