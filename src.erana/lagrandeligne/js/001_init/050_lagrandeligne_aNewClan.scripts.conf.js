// DEPENDENCY : init.js




// ===============================================================================
// 																			CONFIG
// ===============================================================================





// ===============================================================================
// 																			MODEL
// ===============================================================================





// Prototype can define as a well a group of things as particular individuals : only by not setting a random variance on them.

// ------------------ Scripts objects ------------------		
		
PROTOTYPES_CONFIG.allPrototypes.GameScript={
			

	proto_scriptStartFadeOut:{
		
		conditionsTriggers:[
			{ delayMillis:14000 },
		],
		
		actions:{
			"startGoingOut":{escapeHorizontalSpeed:70}
		}
		
	},
	
	
	proto_scriptStopFadeOut:{
		
		conditionsTriggers:[
			{"isOutOfScreen": {args:null} }
		],
		
		actions:{
			"stopGoingOut":null
		}
		
	},
			
			
};




PROTOTYPES_CONFIG.allPrototypes.GameAutomatonTransporter={
	
	
	proto_peopleTransporterAIProcedure:{
		
		areAllTreatmentsInDedicatedClass:true,
		
		taskName:"transporter",
		
		states:{
			"INITIAL":{
				delayMillis:2000,
				transitions:{
					"goToRest":"isRestNecessaryAndPossible",
					
					"goFetchResource":"isResourceTransferTaskIsAvailable",
					"goBackOnTrain":"isNotOnTrainAndIsInStation",
					
//				"goToNeedingBuildingJobIfNecessary":"hasBuilderJobAndIsAvailableAndNeedingBuildingJobExists",
					
					"FINAL":"getIsDeadAndUpdateHabitationAffectationIfNeeded",
				},
				onElse:"INITIAL",
			},
			
			"goFetchResource":{
				delayMillis:2000,
				transitions:{
					"goToRest":"isRestNecessaryAndPossible",
					
					"goDeliverResource":"hasLoadedResource",
					"goBackOnTrain":"isNotOnTrainAndIsInStation",
					
					"FINAL":"getIsDeadAndUpdateHabitationAffectationIfNeeded",
				},
				onElse:"INITIAL",
			},

			"goDeliverResource":{
				delayMillis:2000,

				transitions:{
					"goToRest":"isRestNecessaryAndPossible",
					
					"INITIAL":"hasUnloadedResource",
					
					"FINAL":"getIsDeadAndUpdateHabitationAffectationIfNeeded",
				},
				onElse:"wait",
			},
			
			"wait":{
				delayMillis:2000,
				transitions:{
					"goToRest":"isRestNecessaryAndPossible",
					
					"goDeliverResource":"hasFoundAnotherResourceInNeedAndIsPeopleLoaded",
					"goFetchResource":"isConcernedPeopleNotLoaded",
					
// NEVER SUPPOSED TO HAPPEN :
// "goBackOnTrain":"isNotOnTrainAndIsInStation",

					"FINAL":"getIsDeadAndUpdateHabitationAffectationIfNeeded",
				},
				onElse:"wait",
			},
			
			"goBackOnTrain":{
				delayMillis:2000,
				transitions:{
					"goBackOnTrain":"isNotOnTrainAndIsInStation",
					
					"FINAL":"getIsDeadAndUpdateHabitationAffectationIfNeeded",
				},
				onElse:"INITIAL" 
			},
			
			"goToRest":{
				delayMillis:2000,
				transitions:{
					"FINAL":"getIsDeadAndUpdateHabitationAffectationIfNeeded",
				},
				onElse : "INITIAL" 
				//onElse:"doRest" 
			},
			


			
			"FINAL":{
				/*DO NOTHING*/
			}
			
		},
		
	},
	
},

PROTOTYPES_CONFIG.allPrototypes.GameAutomatonBuilder={

	proto_peopleBuilderAIProcedure:{
		
		areAllTreatmentsInDedicatedClass:true,
		
		taskName:"builder",
		
		states:{
			"INITIAL":{
				delayMillis:2000,
				transitions:{
					"goToRest":"isRestNecessaryAndPossible",
					
					"goToDropLoadToAvailablePlatform":"isConcernedPeopleLoaded",
					"goToNeedingBuildingJobIfNecessary":"hasBuilderJobAndIsAvailableAndNeedingBuildingJobExistsAndIsPeopleNotLoaded",
					
					"FINAL":"getIsDeadAndUpdateHabitationAffectationIfNeeded",
				},
				onElse:"INITIAL",
			},
		
//		"wait":{
//			delayMillis:2000,
//			transitions:{
//				"goToRest":"isRestNecessaryAndPossible",
//
//				"FINAL":"getIsDeadAndUpdateHabitationAffectationIfNeeded",
//			},
//			onElse:"wait",
//		},
			
			"goToRest":{
				delayMillis:2000,
				transitions:{
					
					"FINAL":"getIsDeadAndUpdateHabitationAffectationIfNeeded",
				},
				onElse : "INITIAL" 
				//onElse:"doRest" 
			},
			
			// Building job :
			"goToDropLoadToAvailablePlatform":{
				delayMillis:2000,

				transitions:{
					"goToRest":"isRestNecessaryAndPossible",
					
					"INITIAL":"hasUnloadedResource",
					
					"FINAL":"getIsDeadAndUpdateHabitationAffectationIfNeeded",
				},
				onElse:"goToDropLoadToAvailablePlatform",
			},

			"goToNeedingBuildingJobIfNecessary":{
				delayMillis:2000,
				transitions:{
//				"workOnJobOnce":"hasReachedJobPlace",
					
					"INITIAL":"isJobUnavailable",

					"FINAL":"getIsDeadAndUpdateHabitationAffectationIfNeeded",
				},
				onElse : "workOnJobOnce" 
			},
			
			"workOnJobOnce":{
				delayMillis:2000,
				transitions:{
					
					"goToRest":"isRestNecessaryAndPossible",

					"workOnJobOnce":"isJobUnfinished",
					
					"INITIAL":"isJobUnavailable",

					"FINAL":"getIsDeadAndUpdateHabitationAffectationIfNeeded",
				},
				onElse : "INITIAL" 
			},
			
			"FINAL":{
				/*DO NOTHING*/
			}
			
		},
		
	},
	
	
	
	
},





PROTOTYPES_CONFIG.allPrototypes.GrandeLigneQuestsManager={
			
	proto_questsManagerNorthPole:{
		
		maxNumberOfCompleteQuests:3,
		
		ui:{
			color:"#AAAAAA",
			center:{	x:"right", y:"top" },
			xOffset:-270,
//		center:{	x:"left", y:"top" },
//		xOffset:20,
			verticalSpacing:12,
			yOffset:60,
//		yOffset:230,
			textSize:14,
		},
		
		GameQuest:{
			
			cardinality:{
				value:"staticGroups",
				prototypesInstantiation:{
					staticInstantiations:[
						{name:"proto_questMandatorySurvival", preinstanciate:true},
						{name:"proto_tutoQuestHabitations", preinstanciate:true},
						{name:"proto_tutoQuestSolarer", preinstanciate:true},
						{name:"proto_tutoQuestWaterer", preinstanciate:true},
						{name:"proto_tutoQuestLoadResources", preinstanciate:true},
						{name:"proto_tutoQuestCooker", preinstanciate:true},
						
					]
				},
			},
			
		},
		
		
	},
			
};






PROTOTYPES_CONFIG.allPrototypes.GameQuest={
	

		
	// Habitations tutorial quest and mandatory quest :


	
	proto_questMandatorySurvival:{
		
		title:i18n("survie"),

		// (If no directive, then we simply hide the quest directive in the UI)
		
		directive:i18n("concertadesAmountTarget"),
		
		// TODO : FIXME : ON TODAY IT IS MANDATORY, OR ELSE WILL NEVER TRIGGER :
		conditionsTriggers:[
			{ delayMillis:800 },
		],
		
		conditionsFail:[
//			{"hasNoItemsInTrainOfType": {args:"people"} }
			{"REACHES": {args:["getAlivePeoplesNumber","<=",10] } }

		],

		actionsOnFail:{
			childDialog:"proto_dialogQuestMandatorySurvivalFail",
		},
		

		GameDialog:{
			cardinality:{
				value: "staticGroups",
				prototypesInstantiation:{
					staticInstantiations: [
						
						{name:"proto_dialogQuestMandatorySurvivalFail", preinstanciate:true},
					]
				},
			},
		},
		
		
		
	},
		
		
		
	
	proto_tutoQuestHabitations:{
	
		title:i18n("premierHébergement"),
		directive:i18n("construireDesHabitationsAmountTarget"),
	
		// TODO : FIXME : ON TODAY IT IS MANDATORY, OR ELSE WILL NEVER TRIGGER :
		conditionsTriggers:[
			{ delayMillis:800 },
		],
		
		conditionsSuccess:[
			{"REACHES": {args:["getEquipmentsNumberInMainTrain",">=",2,{partialPrototypeName:"habitation"}] } }
		],
		
		actionsOnSuccess:{
			childDialog:"proto_dialogTutoQuestHabitationsSuccess",
		},

		GameDialog:{
			cardinality:{
				value: "staticGroups",
				prototypesInstantiation:{
					staticInstantiations: [
						{name:"proto_dialogTutoQuestHabitationsSuccess", preinstanciate:true},
					]
				},
			},
		},
		
	},
	
	
	
	
	
	
	
	// Station tutorial quest :

	
	
	proto_tutoQuestLoadResources:{
		
		title:i18n("ressources"),

		directive:i18n("récupérerLeMétalAmount"),
		
		// TODO : FIXME : ON TODAY IT IS MANDATORY, OR ELSE WILL NEVER TRIGGER :
		conditionsTriggers:[
			{ delayMillis:800 },
		],
		
		
		conditionsSuccess:[
//		{"REACHES": {args:["getAvailableResourceInCurrentStation","<=",0,{resourceName:"metal"}] } },
			{"REACHES": {args:["getAvailableResourceInCurrentStation","<=",0,{resourceName:"metal"}] } , isTriggerableSeveralTimes:true},
			{ "isTrainInStation":{args:{mustBeInStation:true} , isTriggerableSeveralTimes:true} }
			
			
		],
		
		actionsOnSuccess:{
			childDialog:"proto_dialogTutoQuestLoadResourcesSuccess",
		},
		

		GameDialog:{
			cardinality:{
				value: "staticGroups",
				prototypesInstantiation:{
					staticInstantiations: [
						
						{name:"proto_dialogTutoQuestLoadResourcesSuccess", preinstanciate:true},
						
						// CAUTION ! BECAUSE THESE DIALOGS ARE CHAINED, THEIR COMMON PARENT MUST KNOW ALL OF THEM !
						{name:"proto_dialogTutoCooker", preinstanciate:true},

					]
				},
			},
		},
		
		
		
	},
	
	
	// Cooker tutorial quest :

	proto_tutoQuestCooker:{
	
		title:i18n("cuisine"),
		directive:i18n("construireUneCuisineAmount"),
	
		// TODO : FIXME : ON TODAY IT IS MANDATORY, OR ELSE WILL NEVER TRIGGER :
		conditionsTriggers:[
			{ delayMillis:800 },
		],
		
		conditionsSuccess:[
			{"REACHES": {args:["getEquipmentsNumberInMainTrain",">=",1,{partialPrototypeName:"cooker"}] } }
		],
		
		actionsOnSuccess:{
			childDialog:"proto_dialogTutoQuestCookerSuccess",
		},

		GameDialog:{
			cardinality:{
				value: "staticGroups",
				prototypesInstantiation:{
					staticInstantiations: [
						{name:"proto_dialogTutoQuestCookerSuccess", preinstanciate:true},
						
						// CAUTION ! BECAUSE THESE DIALOGS ARE CHAINED, THEIR COMMON PARENT MUST KNOW ALL OF THEM !
						{name:"proto_dialogTutoOverEquipment", preinstanciate:true},
					
						
					]
				},
			},
		},
		
	},
	
	
	
	
	// Over equipements tutorial quest :


	
	
	// After the «Cooker» quest :

	proto_tutoQuestSolarer:{
	
		title:i18n("collecteurSolaire"),
		directive:i18n("construireUnCollecteurSolaireAmount"),
	
		// TODO : FIXME : ON TODAY IT IS MANDATORY, OR ELSE WILL NEVER TRIGGER :
		conditionsTriggers:[
			{ delayMillis:800 },
		],
		
		conditionsSuccess:[
			{"REACHES": {args:["getEquipmentsNumberInMainTrain",">=",1,{partialPrototypeName:"solarer"}] } }
		],
		
		actionsOnSuccess:{
			childDialog:"proto_dialogTutoQuestSolarerSuccess",
		},

		GameDialog:{
			cardinality:{
				value: "staticGroups",
				prototypesInstantiation:{
					staticInstantiations: [
						{name:"proto_dialogTutoQuestSolarerSuccess", preinstanciate:true},
						
						// CAUTION ! BECAUSE THESE DIALOGS ARE CHAINED, THEIR COMMON PARENT MUST KNOW ALL OF THEM !
						{name:"proto_dialogTutoEnd", preinstanciate:true},

					]
				},
			},
		},
		
	},
	
	
	
	proto_tutoQuestWaterer:{
	
		title:i18n("condensateurDEau"),
		directive:i18n("construireUnCondensateurDEauAmount"),
	
		// TODO : FIXME : ON TODAY IT IS MANDATORY, OR ELSE WILL NEVER TRIGGER :
		conditionsTriggers:[
			{ delayMillis:800 },
		],
		
		conditionsSuccess:[
			{"REACHES": {args:["getEquipmentsNumberInMainTrain",">=",1,{partialPrototypeName:"waterer"}] } }
		],
		
		actionsOnSuccess:{
			childDialog:"proto_dialogTutoQuestWatererSuccess",
		},

		GameDialog:{
			cardinality:{
				value: "staticGroups",
				prototypesInstantiation:{
					staticInstantiations: [
						{name:"proto_dialogTutoQuestWatererSuccess", preinstanciate:true},
						
						// CAUTION ! BECAUSE THESE DIALOGS ARE CHAINED, THEIR COMMON PARENT MUST KNOW ALL OF THEM !
						{name:"proto_dialogTutoEnd", preinstanciate:true},
						
					]
				},
			},
		},
		
	},
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
			
};

