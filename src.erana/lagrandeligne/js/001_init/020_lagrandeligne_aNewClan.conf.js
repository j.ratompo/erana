// DEPENDENCY:init.js


window.lang = "fr";


// ===============================================================================
// CONFIG
// ===============================================================================


const PROTO_PREFIX="proto_";


const GAME_CONFIG = {
	
	gameURLs:{
//		"support":"https://paypal.me/pools/c/8szm55N1iC", 
		"other":"https://www.kai-engel.com", "email":"mailto:info@alqemia.com"},
//basePath:"lagrandeligne.medias",
	basePath:"lagrandeligne.2.medias",
	
//	spinnerImage:"aotraLogo_128.gif",
	spinnerImage:"logoErana.gif",

	
	// view config :
	X_OFFSET:0,
	Y_OFFSET:-30,
	// TODO:FIXME:ZOOM DOES NOT WORK AT ALL :
	// Zoom is the most outer function. Camera and Offsets coordinates obeys it!
//	zooms:{zx:0.8,zy:0.8}, 
//	zooms:{zx:0.6,zy:0.6}, 
	zooms:{zx:1,zy:1}, 

	// resolution :
	chosenResolutionIndex:1,
	getResolution:function(){
		// This syntax is quite cool :
		return [{w:getWindowSize("width"),h:getWindowSize("height")},{w:800,h:400},][this.chosenResolutionIndex];
	},
	getViewCenterOffset:function(ignoreOffset=false){
		var result={
				x:Math.floor(this.getResolution().w*.5) + (ignoreOffset?0:this.X_OFFSET),
				y:Math.floor(this.getResolution().h*.5) + (ignoreOffset?0:this.Y_OFFSET)
		};
		return result;
	},
	
	canChangeDurationTimeFactor:true,
	durationTimeFactor:1,
	minDurationTimeFactor:0.125,
	maxDurationTimeFactor:1,


	// lighting config :
	lighting:{ dark:true, darkRaycast:true, bump:true },
	// scroll config :
	scale:0.025,// = 1/40 (40 px = 1m)
//	scroll:"horizontal",
	// DBG
//	wrapping:{x:10000},
	scroll:"horizontal",
	
	// TODO : FIXME : MOVE TO LEVELS CONFIG INSTEAD !
	wrapping:{x:100000000}, // ~2500km
	
	projection:"2D flat",
	// camera config :
	cameraFixed:{name:"trains",index:0},
	// selection config :
	selectionCanChange:true,
	selectionMaxSize:1,
	// controls config :
	controlsMode: "screen-drag camera focus-on-double-click",
	controlsPosition:"direction action",
	controlsPanelHeightRatio:0.3,
	controlsPanelStyle:{
		backgroundGradient:"90deg, #222222, #111111",
		text:"#EEEEEE",
		gaugeBackColor:"transparent",
		gaugeForeColor:"#444444",
	},
	popupConfig:{
		backgroundColor1:"#222222",
		backgroundColor2:"#111111",
		textColor:"#FFFFFF",
	},
	// images config :
	backgroundClipSize:{w:800,h:400},
	// controller config :
	// ALTERNATIVE PARAMETER : refreshRateMillis:33, // :(1000/33)=30 FPS max

//	fps:40, // CAUTION : changes speeds de facto !
	// DBG
	fps:20,
	
	// ui
	dialogs:{	
		autoScroll:"letters", 
		speedFactor:8, /*UNUSEFUL (Since there will always be a «linger» time if you set a speedFactor>1 !) : minimumTimeMillis:1000*/
		backgroundColor:"#D1B69C",
	},
	// model config :
	allowParentLinkForChildren:true,
	gameCentralServer:null,
	
	ignoreSpinner:false,

};





// ===============================================================================
// FLOW
// ===============================================================================



FLOW_CONFIG = {
	
	
	splashPage_erana:{

		delayMillis:(IS_DEBUG?200:3000),
		
//	image: "splashs/aotraLogo_300.png",
		image: "splashs/logoErana_300.png",
		
		background: "#FFFFFF",
		text:{
			font:"helvetica",
			position:{yBottom:20},
			message:i18n("propulséParEranaBrSpanStyleFontSize7emUnMoteurOuv")
		},
		goTo:"splashPage_alqemia"

	},
	
	splashPage_alqemia:{

		delayMillis:(IS_DEBUG?200:3000),
		
		image: "splashs/alqemia_400.png",
		background: "#000000",
		text:{
			color:"#000000",
			font:"helvetica",
			position:{yTop:70},
			message:i18n("leStudioAlqemiaPrésente"),
		},
		
//	goTo:(IS_DEMO?"alphaPage":(IS_DEBUG?"homePage":"audioPage")),

		// FOR NOW ONLY (alpha version) :
		goTo:(IS_DEBUG?"homePage":"alphaPage"),
		
//		goTo:"audioPage"
//		goTo:"alphaPage",

	},

	
	
	
	
	alphaPage:{
		
		image: "splashs/icon_bug.png",


		// WORKAROUND : To force the user to interact to bypass the «user must interact with the page first before autoplay» user annoyance protection politics :

		background: "#000000",
		text:{
			color:"#FFFFFF",
			font:"helvetica",
			position:{yTop:80},
			message:i18n("attentionCeciEstUneVersionAlphaBrDesBugsInacceptable"),
												
		},
		
		
		menus:{
			mainMenu:{
				items:[
					{
						label:i18n("ok"),
						goTo:"audioPage"
					},
				]
			},
		},
	},
	
	
	
	
	
	audioPage:{
		
		image: "splashs/icon_headset.png",


		// WORKAROUND : To force the user to interact to bypass the «user must interact with the page first before autoplay» user annoyance protection politics :

		background: "#000000",
		text:{
			color:"#FFFFFF",
			font:"helvetica",
			position:{yTop:80},
			message:i18n("unDispositifDÉcouteEstConseilléBrPourUneMeilleureExpé"),
		},
		
		
		menus:{
			mainMenu:{
				items:[
					{
						label:i18n({
							"fr":"OK",
							"en":"OK"
						}),
						goTo:"homePage"
					},
				]
			},
		},
	},
	
	
		
	
	
	
	homePage:{
		
		image: "splashs/lagrandeligne-title.png",

		startLevelName:"proto_level_0",

		// WORKAROUND : To bypass the «user must interact with the page first before autoplay» user annoyance protection politics :

		music:{
			pauseBetweenMillis:4000,
			volumePercent:50,
			tracks: ["musics/Mercy.mp3","musics/Evermore.mp3"
				],
		},
	
		
		menus:{
			mainMenu:{
				items:[
					{
						label:i18n("commencer"),
						actionAfter:(IS_DEBUG?"startNewGame":"startCinematic"),
						goTo:(IS_DEBUG?"gamePage":"cinematic1Page"),
					},
					{
						label:i18n("continuer"),
						actionAfter:"continueGame",
						goTo:"gamePage",
						activeOnlyIf:"isPreviousGameDetected()",
						restoreLevel:true,
						isUploader:true,
//						// DEBUG : 
//						inactive:true
					}, 
					{
						label:i18n("crédits"),
						actionAfter:"startCredits",
						goTo:"creditsPage"
					},
					{
						label:i18n("soutenir"),
						goTo:"supportPage",
					},
					{
						label:i18n("quitter"),
						actionAfter:"exitGame",
						//goTo:"homePage",
					},
				]
			},
		},
	},


	cinematic1Page:{

		startLevelName:"proto_level1_cinematic1",
		visibleControls:true,
		hiddenControls:true,

		music:{
			pauseBetweenMillis:4000,
			volumePercent:100,
			tracks: ["musics/Seeker.mp3"],
			loop:false,
		},
		
		onLeave:"stopCinematic",
		
		menus:{
			mainMenu:{
				config:{
					label:i18n("menu"),
					actionAfter:"showMenu",
					position:"top-right" // default position is "top-left"
				},
				items:[
					{
						label:i18n("passer"),
						actionAfter:"startNewGame",
						goTo:"gamePage"
					}, {
						label:i18n("retourALAccueil"),
						actionAfter:"exitGame",
						goTo:"homePage"
					},
				]
			},
		},
	},

	
	
	endOfDemoCinematicPage:{

		startLevelName:"proto_level1_cinematicEndDemo",
		visibleControls:true,
		hiddenControls:true,

// 	UNUSEFUL :
//	onEnter:"startCinematic", // Special case, since this cinematic is not triggered by a button click !
//	onLeave:"stopCinematic",
		
		music:{
			pauseBetweenMillis:4000,
			volumePercent:100,
			tracks: ["musics/Realness.mp3"],
			loop:false,
		},
		
		menus:{
			mainMenu:{
				config:{
					label:i18n("menu"),
					actionAfter:"showMenu",
					position:"top-right" // default position is "top-left"
				},
				items:[
					{
						label:i18n("passer"),
						actionAfter:"startNewGame",
						goTo:"homePage"
					},
				]
			},
		},
		
	},
	
	
	
	
	gamePage:{

		startLevelName:"proto_level_1",
		visibleControls:true,

		music:{
			pauseBetweenMillis:4000,
			volumePercent:50,
			tracks: ["musics/Brooks.mp3","musics/Denouement.mp3","musics/Snowmen.mp3"],
		},
		
		menus:{
			mainMenu:{
				config:{
					label:i18n("menu"),
					actionAfter:"showMenu",
					position:"top-right" // default position is "top-left"
				},
				items:[
					{
						label:i18n("reprendre"),
						actionAfter:"resumeGame",
						resume:true
					},
					{
						label:i18n("sauverEtRevenirÀLAccueil"),
						actionBefore:"interruptGame",
						goTo:"homePage"
					},
					{
						label:i18n("retourALAccueil"),
						actionAfter:"abandonGame",
						goTo:"homePage"
					}
				]
			},
		},
	},


	
	
	
	creditsPage:{

		startLevelName:"proto_level_credits",
		
		
		menus:{
			mainMenu:{
				items:[
					
					{ label:i18n("programmationJeremieR"), inactive:true,},
					{ label:i18n("graphiquesJeremieR"), inactive:true,},
					{ label:i18n("écritureJeremieR"), inactive:true,},
					{ label:i18n("sonsJeremieR"), inactive:true,},
					{ label:i18n("communicationJeremieR"), inactive:true,},
					{ label:i18n("musiqueKaiEngel"), actionAfter:"goToURLOther" },
					{ label:i18n("remerciementsSupportMoralDominiqueB"), inactive:true,},
					{ label:i18n("remerciementsMusiqueÉcoutéeKaiEngelLindseyStirlingOh"), inactive:true,},
					{ label:i18n("remerciementsPodcastsÉcoutésTalesOfPiFranceInterFib"), inactive:true,},
					{ label:i18n("retourALAccueil"), goTo:"homePage"},
										
				]
			},
		},
	},
	
	
	

	
	
	supportPage:{

		startLevelName:"proto_level_credits",
		
		menus:{
			mainMenu:{
				items:[
					
			
					{
						label:i18n({
							"fr":"<strong>Comment nous soutenir ?</strong>",
							"en":"<strong>How to support the game ?</strong>",
						}),
						inactive:true,
					},

					{
						label:i18n({
							"fr":"Faites-nous parvenir vos commentaires / demandez à être informé des prochains développements !",
							"en":"Let us know your comments / ask for updates from the game development !",
						}),
						inactive:true,
					},

					{
						label:i18n({
							"fr":"Envoyer un courriel à l´équipe : info@alqemia.com",
							"en":"Send an email to the team : info@alqemia.com",
						}),
						actionAfter:"goToURLMailTo"

					},
					
//					{
//						label:i18n({
//							"fr":"Faites-nous un petit don qui fera une grande différence ! 😉",
//							"en":"Give a us a little tip that will make a big difference ! 😉",
//						}),
//						actionAfter:"goToURLSupport"
//					},
					
					{
						label:i18n("retourALAccueil"),
						goTo:"homePage"
					},
					
					
					
				]
			},
		},
	},
	
	
	
	
	
	

};



