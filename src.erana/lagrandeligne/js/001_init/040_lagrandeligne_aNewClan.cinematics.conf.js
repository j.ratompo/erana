// DEPENDENCY : init.js




// ===============================================================================
// 																			CONFIG
// ===============================================================================





// ===============================================================================
// 																			MODEL
// ===============================================================================





// Prototype can define as a well a group of things as particular individuals : only by not setting a random variance on them.

// ------------------ Cinematics objects ------------------		
		
PROTOTYPES_CONFIG.allPrototypes.GameCinematic={
			
			proto_cinematic1:{

				
				nonStopSpeed:10,
				
				backgroundColor:"#000000",
				
//			backgroundZIndex:1,
//			backgroundParallax:10,
//			backgroundOffsetY:0,
//			backgrounds : [
//				{images:["backgroundSky1_v1.png"],parallaxAdd:400},
//				{images:["background1_v1.png"],parallaxAdd:200},
//				{images:["backgroundFore1_v1.png"],parallaxAdd:20},
//				{images:[{normal:"foregroundGround1.png"}],offsetY:-230,clipSize:{w:1600,h:100}},
//			],
				
//			foregroundParallax:1,
//// foregroundOffsetY:10,
//			foregrounds:[
//// 				{images:["foreground1.png"],offsetY:-20},
//				{images:["foreground1.dark.png"],offsetY:-20},
//			],
				
				
				
				GameFlyer: {
					cardinality : {
						value:"staticGroups",
						prototypesInstantiation : {
							staticInstantiations:[
								
								// PROD
								
								// PART 0

								// Tutorial
								{name:"proto_c1s0",subClass:"GameSticker",spawningZone:{x:0,y:30,z:1},preinstanciate:true},
								
								// PART I
								
								// In space
								{name:"proto_c1f4Stars",spawningZone:{x:1800,y:-120,z:1,parallax:1.75},preinstanciate:true},
								{name:"proto_c1f0Asteroids",spawningZone:{x:1800,y:-30,z:2,parallax:1.50},preinstanciate:true},
								{name:"proto_c1f3Satellite",spawningZone:{x:2000,y:0,z:3,parallax:1.25},preinstanciate:true},
								{name:"proto_c1f1RocksLots",spawningZone:{x:2200,y:30,z:4,parallax:1},preinstanciate:true},
								{name:"proto_c1f2RocksFew",spawningZone:{x:2400,y:60,z:5,parallax:0.75},preinstanciate:true},
								
								{name:"proto_c1s1_1",subClass:"GameSticker",spawningZone:{x:3000,y:30,z:1},preinstanciate:true},

								{name:"proto_c1f4Stars",spawningZone:{x:3600,y:-120,z:1,parallax:1.75},preinstanciate:true},
								{name:"proto_c1s1_2",subClass:"GameSticker",spawningZone:{x:3400,y:30,z:2},preinstanciate:true},

								
								// In Torus
								
								{name:"proto_c1s2_1",subClass:"GameSticker",spawningZone:{x:7000,y:30,z:1},preinstanciate:true},
								{name:"proto_c1s3_1",subClass:"GameSticker",spawningZone:{x:12000,y:30,z:1},preinstanciate:true},

								
								// PART II
								{name:"proto_c1s4_1",subClass:"GameSticker",spawningZone:{x:15000,y:30,z:1},preinstanciate:true},
								{name:"proto_c1s4_2",subClass:"GameSticker",spawningZone:{x:16000,y:30,z:1},preinstanciate:true},

								{name:"proto_c1s5",subClass:"GameSticker",spawningZone:{x:19000,y:30,z:1},preinstanciate:true},
								
								{name:"proto_c1s6_1",subClass:"GameSticker",spawningZone:{x:21000,y:30,z:1},preinstanciate:true},
//							{name:"proto_c1s6_2",subClass:"GameSticker",spawningZone:{x:20000,y:30,z:1},preinstanciate:true},

								
								
								
								
//												// DBG
//												{name:"proto_c1s1_1",subClass:"GameSticker",spawningZone:{x:600,y:30,z:1},preinstanciate:true},
								
								
//								{name:"proto_c1f1RocksLots",spawningZone:{x:3000,y:-190,parallax:2},preinstanciate:true},
//								{name:"proto_c1f1RocksLots",spawningZone:{x:3000,y:30},preinstanciate:true},
								
//								DBG : 
//								{name:"proto_c1f1_RED",spawningZone:{x:3000,y:30},preinstanciate:true},
//								{name:"proto_c1f1_DEBUG",spawningZone:{x:3500,y:0,parallax:3},preinstanciate:true},
//								{name:"proto_c1f1_DEBUG",spawningZone:{x:3500,y:0,parallax:.5},preinstanciate:true},

								
								
//								{name:"proto_c1_f1",subClass:"GameSticker",spawningZone:{x:1200,y:60},preinstanciate:true},
//								{name:"proto_c1_f2",subClass:"GameSticker",spawningZone:{x:1800,y:60},preinstanciate:true},
//								{name:"proto_c1_f3",subClass:"GameSticker",spawningZone:{x:2400,y:60},preinstanciate:true},
//								{name:"proto_c1_f4",subClass:"GameSticker",spawningZone:{x:3000,y:60},preinstanciate:true},
//								{name:"proto_c1_f5",subClass:"GameSticker",spawningZone:{x:3600,y:60},preinstanciate:true},
							],
						},
					},
				},
				
			},
			
			
			
			
			
			
			
			
			
			
			proto_cinematicEndDemo:{

				nonStopSpeed:7,
				
				backgroundColor:"#000000",
				
				
				GameFlyer: {
					cardinality : {
						value:"staticGroups",
						prototypesInstantiation : {
							staticInstantiations:[
								
								// PROD
								
								{name:"proto_cEndDemoS1",subClass:"GameSticker",spawningZone:{x:1000,y:30,z:1},preinstanciate:true},
								{name:"proto_cEndDemoS2",subClass:"GameSticker",spawningZone:{x:3500,y:30,z:1},preinstanciate:true},
								{name:"proto_cEndDemoS3",subClass:"GameSticker",spawningZone:{x:5500,y:30,z:1},preinstanciate:true},
								{name:"proto_cEndDemoS4",subClass:"GameSticker",spawningZone:{x:7500,y:30,z:1},preinstanciate:true},
								{name:"proto_cEndDemoS5",subClass:"GameSticker",spawningZone:{x:10000,y:30,z:1},preinstanciate:true},
								
								
								
							],
						},
					},
				},
				
			},
			
			
			
			
			
			
			
			
			
			
			
};







PROTOTYPES_CONFIG.allPrototypes.GameFlyer = {
		
	  proto_abstract_nonPersistent_flyer:{
	  	
			showStrategy:{
				persist:false,
				fading:"proportionalToDistance",
				triggerZone:{
					_2D : {
						w:600
					},
				},
			},
			
	  },
	  

//	// DBG
//	proto_c1f1_RED:{
//		usesSuper:"proto_abstract_nonPersistent_flyer",
//		imageConfig : {
//			_2D : {
//				center:{x:"center",y:"center"},
//				sourcePath : "rocksLots.red.png",
//			},
//		},
//	},
//	
//	// DBG
//	proto_c1f1_DEBUG:{
//		usesSuper:"proto_abstract_nonPersistent_flyer",
//		imageConfig : {
//			_2D : {
//				center:{x:"center",y:"center"},
//				sourcePath : "rocksLots.DEBUG_GRID.png",
//			},
//		},
//	},
	  
	  
	  
	  proto_c1f4Stars:{
			usesSuper:"proto_abstract_persistent_flyer",
			
			showStrategy:{
				persist:false,
				fading:"proportionalToDistance",
				triggerZone:{
					_2D : {
						w:1200
					},
				},
			},
			
			imageConfig : {
				_2D : {
					center:{x:"center",y:"center"},
					sourcePath : "spaceBackground.png",
				},
			},
		},
		
	  proto_c1f0Asteroids:{
			usesSuper:"proto_abstract_nonPersistent_flyer",
			
			imageConfig : {
				_2D : {
					center:{x:"center",y:"center"},
					sourcePath : "asteroids.png",
				},
			},
			
		},
		
		
		proto_c1f3Satellite:{
			usesSuper:"proto_abstract_nonPersistent_flyer",
			
//			showStrategy:{
//				persist:false,
//				fading:"proportionalToDistance",
//				triggerZone:{
//					_2D : {
//						w:300
//					},
//				},
//			},
			
			
			imageConfig : {
				_2D : {
					center:{x:"center",y:"center"},
					sourcePath : "satellite.png",
				},
			},
		},
		
		
		proto_c1f1RocksLots:{
			
			usesSuper:"proto_abstract_nonPersistent_sticker_800",
			
	  
			imageConfig : {
				_2D : {
					center:{x:"center",y:"center"},
					sourcePath : "rocksLots.png",
				},
			},
		},
		
		proto_c1f2RocksFew:{
			
			usesSuper:"proto_abstract_nonPersistent_sticker_800",
			
			
			imageConfig : {
				_2D : {
					center:{x:"center",y:"center"},
					sourcePath : "rocksFew.png",
				},
			},
		},
		
		
}






PROTOTYPES_CONFIG.allPrototypes.GameSticker = {
			
		  proto_abstract_nonPersistent_sticker_600:{
		  	
				showStrategy:{
					persist:false,
					
					fading:"proportionalToDistance",
					triggerZone:{
						_2D : {
							w:600
						},
					},
				},
		  },
		  
		  proto_abstract_persistent_sticker_400:{
		  	
				showStrategy:{
					persist:true,
					
					fading:"proportionalToDistance",
					triggerZone:{
						_2D : {
							w:400
						},
					},
				},
				
		  },
		  
		  
		  
		  proto_abstract_nonPersistent_sticker_800:{
				
				usesSuper:"proto_abstract_nonPersistent_sticker_600",

				showStrategy:{
					persist:false,
					
					fading:"proportionalToDistance",
					triggerZone:{
						_2D : {
							w:800
						},
					},
				},
			},
			
		  proto_abstract_persistent_sticker_800:{
				
				usesSuper:"proto_abstract_nonPersistent_sticker_600",

				showStrategy:{
					persist:true,
					
					fading:"proportionalToDistance",
					triggerZone:{
						_2D : {
							w:800
						},
					},
				},
			},
		  
		  
		  // PART 0:
		  
			proto_c1s0:{
				usesSuper:"proto_abstract_nonPersistent_sticker_600",
				
				imageConfig : {
					_2D : {
						center:{x:"center",y:"center"},
						sourcePath : "v0.png",
					},
				},
				
				GameLineTrigger: [
					{message:i18n({"fr":"Défilez vers la droite avec le pointeur →","en":"Scroll to the right with pointer →"}), duration: 4000},
				],
				
			},
			
			
			// PART I:
			
			proto_c1s1_1:{
				usesSuper:"proto_abstract_nonPersistent_sticker_600",
				
				// No image for this one : (black screen)
				
				GameLineTrigger: [
					{message:i18n({"fr":"Torus.","en":"Torus."}), duration: 2000},
//					{  triggerZone:{ _2D:{x:200,y:0,w:100} },
//						message:i18n("Aujourd´hui est un jour exceptionnel"), duration: 4000, delay:4000
//					},
					
//					// DBG
//					{pauseMillis: 4000},
//					{message:i18n("4 secondes plus tard...."), duration: 2000},


				],
				
			},
			
			
			proto_c1s1_2:{
				
				usesSuper:"proto_abstract_persistent_sticker_400",
				
				imageConfig : {
					_2D : {
						center:{x:"center",y:"center"},
						sourcePath : "Torus.png",
					},
				},
				GameLineTrigger: [
					{message:i18n({"fr":"Notre monde-père.","en":"Our fatherworld."}), duration: 3000},
				],
				
			},
			
			
			proto_c1s2_1:{
		  	
				showStrategy:{
					persist:false,
					fading:"proportionalToDistance",
					triggerZone:{
						_2D : {
							w:4200
						},
					},
				},
				
				imageConfig : {
					_2D : {
						center:{x:"center",y:"center"},
						sourcePath : "paysageTorus1.png",
					},
				},
				GameLineTrigger: [
					{message:i18n({"fr":"Aride et inhospitalier. Sa nuit glaciale et son jour brûlant.","en":"Dry and inhospitable. His icy night and his burning day."}), duration: 5000},
					{message:i18n({"fr":"Notre bénédiction et notre malédiction.","en":"Our blessing and our curse."}), duration: 4000},
				],
				
			},
			
			
			
			
			proto_c1s3_1:{
		  	
				showStrategy:{
					persist:true,
					fading:"proportionalToDistance",
					triggerZone:{
						_2D : {
//							w:2600
							w:3000
						},
					},
				},
				
				imageConfig : {
					_2D : {
						center:{x:"center",y:"center"},
						sourcePath : "paysageTorusEtTrain.png",
					},
				},
				GameLineTrigger: [
					{message:i18n({"fr":"Survivre est une course sans fin.","en":"Survival is an endless run."}), duration: 4000},
					{message:i18n({"fr":"À la poursuite éternelle du crépuscule salvateur.","en":"In pursuit of the eternal saving twilight."}), duration: 4000},
				],
				
			},
			
			
			// PART II :
			
			
			proto_c1s4_1:{
				usesSuper:"proto_abstract_nonPersistent_sticker_600",
				
				// No image for this one : (black screen)
				GameLineTrigger: [
					{message:i18n({"fr":"Notre chère enfant.","en":"Dear child."}), duration: 2000},
					{message:i18n({"fr":"Réveillez-vous, Concerta.","en":"Wake up, Concerta."}), duration: 2000},
				],
			},
			
			
			proto_c1s4_2:{
		  	
				usesSuper:"proto_abstract_persistent_sticker_400",

				imageConfig : {
					_2D : {
						center:{x:"center",y:"center"},
						sourcePath : "concertae.png",
					},
				},
				
				GameLineTrigger: [
					{message:i18n({"fr":"Vous êtes la dernière-née des Concertae.","en":"You´re the last born of the Concertae."}), duration: 4000},
					{message:i18n({"fr":"Une tâche difficile vous attend.","en":"A difficult task awaits you."}), duration: 3000},
				],
				
			},
			
			
			
			
			proto_c1s5:{
				
				usesSuper:"proto_abstract_nonPersistent_sticker_800",

				imageConfig : {
					_2D : {
						center:{x:"center",y:"center"},
						sourcePath : "concertades.png",
					},
				},
				GameLineTrigger: [
					{message:i18n({"fr":"Vous devrez guider votre peuple vers son destin.","en":"You´ll have to guide your people to their destiny."}), duration: 4000},
				],
				
			},
			
			
			

			proto_c1s6_1:{
		  	
				usesSuper:"proto_abstract_nonPersistent_sticker_800",

				imageConfig : {
					_2D : {
						center:{x:"center",y:"center"},
						sourcePath : "paysageAiguilles.png",
					},
				},
				GameLineTrigger: [
					{message:i18n({"fr":"Percer les mystères de notre monde.","en":"Unravel the mysteries of our world."}), duration: 5000},
					{message:i18n({"fr":"Et répondre enfin à cette question :","en":"And finally answer this question:"}), duration: 5000},
					{pauseMillis: 4000}, // TODO : Add a pause possibility. like in other dialogues systems (TODO : FIXME : MERGE THEM...)
					{message:i18n({"fr":"Quel est donc le secret de la Grande Ligne ?","en":"What is the secret of the Great Line?"}), duration: 5000},
				],
				
				goTo:"gamePage",
				action:"startNewGame",
				
			},
			
			

			
			// --------------------------------------------------------------------------------------------------
			
			// End of Demo :
			proto_cEndDemoS1:{
				usesSuper:"proto_abstract_persistent_sticker_800",
				
				textLinesAlignY:"center",
				
				imageConfig : {
					_2D : {
						center:{x:"center",y:"center"},
						sourcePath : "sunset.png",
					},
				},
				
				GameLineTrigger: [
					{message:i18n({"fr":"Bon, voilà...c´est la fin de la démo !","en":"Well... we have reached the end of the demo!"}), duration: 4000},
					{message:i18n({"fr":"J´espère que ça vous a plu !","en":"I hope you enjoyed it!"}), duration: 3000},
					{message:i18n({"fr":"Voici les prochains développements prévus du jeu :","en":"Here are a glimpse of future developments :"}), duration: 4000},

				],
				
			},
			
			proto_cEndDemoS2:{
				usesSuper:"proto_abstract_nonPersistent_sticker_800",
				
				textLinesAlignY:"center",
				
				imageConfig : {
					_2D : {
						center:{x:"center",y:"center"},
						sourcePath : "Elyro.png",
					},
				},
				
				GameLineTrigger: [
					{message:i18n({"fr":"La recherche scientifique.","en":"Scientific research."}), duration: 3000},
					{message:i18n({"fr":"Pour améliorer différents aspects de la vie de la kinésipôle.","en":"To enhance the various aspects of life aboard."}), duration: 4000},
					{message:i18n({"fr":"Construire de nouveaux équipements,","en":"To build new equipments,"}), duration: 3000},
					{message:i18n({"fr":"et utiliser de nouvelles technologies.","en":"And use new technologies. "}), duration: 3000},

				],
				
			},
			
			proto_cEndDemoS3:{
				usesSuper:"proto_abstract_nonPersistent_sticker_800",
				
				textLinesAlignY:"center",
				
				imageConfig : {
					_2D : {
						center:{x:"center",y:"center"},
						sourcePath : "Eleaç.png",
					},
				},
				
				GameLineTrigger: [
					{message:i18n({"fr":"L´innovation culturelle et sociale.","en":"Cultural and social innovation."}), duration: 4000},
					{message:i18n({"fr":"Permettra d´améliorer la vie de vos concertades.","en":"To improve the life of your concertads."}), duration: 4000},
					{message:i18n({"fr":"Ce qui affectera leur moral, et leur santé physique et mentale.","en":"Which will affect physical and mental health."}), duration: 5000},

				],
				
				
			},
			
			proto_cEndDemoS4:{
				usesSuper:"proto_abstract_nonPersistent_sticker_800",
				
				textLinesAlignY:"center",
				
				imageConfig : {
					_2D : {
						center:{x:"center",y:"center"},
						sourcePath : "Ianü.png",
					},
				},
				
				GameLineTrigger: [
					{message:i18n({"fr":"La gestion politique","en":"Political management."}), duration: 3000},
					{message:i18n({"fr":"Chaque action aura des implications politiques","en":"Each action will have political implications."}), duration: 4000},
					{message:i18n({"fr":"Ce qui conditionnera la vie en société.","en":"Which, in turn, will affect your community."}), duration: 4000},
					{message:i18n({"fr":"...et bien d´autres encore !","en":"...and a lot more!"}), duration: 3000},
					{pauseMillis:3000},
					{message:i18n({"fr":"Si vous voulez, vous pouvez soutenir le développement du jeu.","en":"If you wish, you can support the game."}), duration: 4000},

				],
				
			},
			
			
			
			proto_cEndDemoS5:{
				
				textLinesAlignY:"center",
				
				showStrategy:{
					persist:true,
					
					fading:"proportionalToDistance",
					triggerZone:{
						_2D : {
							w:600
						},
					},
				},
				
				
				imageConfig : {
					_2D : {
						center:{x:"center",y:"center"},
						sourcePath : "notrePeuple.png",
					},
				},
				
				GameLineTrigger: [
					{message:i18n({"fr":"Nos concertades ont besoin de vous !","en":"Our concertads need you."}), duration: 3000},
					{message:i18n({"fr":"Sur la page d´accueil, cliquez sur le bouton «Soutenir» !","en":"On the home page, click on the «Support» button!"}), duration: 3000},
					{message:i18n({"fr":"Merci pour tout !","en":"Thanks for everything!"}), duration: 1000},

				],
				
				goTo:"homePage",
				
			},
			
			
			
			
			
			
			
};

