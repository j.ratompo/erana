// DEPENDENCY:init.js




// ===============================================================================
// MODEL
// ===============================================================================


// Prototype can define as a well a group of things as particular
// individuals:only by not setting a random variance on them.
// ***
// CAUTION : The attribute «usesSuper» must always be the first attribute ! (All attribute put before will be ignored).
// ***


PROTOTYPES_CONFIG = {

	allPrototypes:{
		
		
		// ------------------ Levels objects ------------------
		
		GameLevel:{
			
			proto_level_0:{

				selectedItem:null,
				
//			Region:{
//				cardinality:{
//					value:1,
//					prototypesInstantiation:"proto_regionNowhere"
//				},
//			},
				
				Region:"proto_regionNowhere",
				
			},
				
				
			proto_level1_cinematic1:{
				isCinematic:true,
				
				limits:{
					_2D:{
						x:{min:-200}
					}
					
				},
				
				
				GameCinematic:"proto_cinematic1",
				
//			GameCinematic:{
//				cardinality:{
//					value:1,
//					// PROD :
//					prototypesInstantiation:"proto_cinematic1"
//				},
//			},
				
			},


			proto_level_1:{

				selectedItem:"getDefaultRollingPlatform()",
				
				Region:"proto_regionNorthPole",
//				Region:{
//					cardinality:{
//						value:1,
//						// PROD
//						prototypesInstantiation:"proto_regionNorthPole",
////					// DBG
////					prototypesInstantiation:"proto_regionDebug",
//					},
//				},
				
			},
			
			
			
			proto_level1_cinematicEndDemo:{
				isCinematic:true,
				
				limits:{
					_2D:{
						x:{min:-200}
					}
				},
				
				GameCinematic:"proto_cinematicEndDemo",
//				GameCinematic:{
//					cardinality:{
//						value:1,
//						prototypesInstantiation:"proto_cinematicEndDemo"
//					},
//				},
				
			},
			
			
			proto_level_credits :{
				
				Region:"proto_regionNowhere",
//				Region:{
//					cardinality:{
//						value:1,
//						prototypesInstantiation:"proto_regionNowhere"
//					},
//				},
				
			},
				
			
			
		},
		
		
		
		
		
		// ------------------ Game objects ------------------
		
		Region:{

			
			proto_abstract_region:{
				

				containerConfig:"starter", // To indicate where the player starts in the tree objects hierarchy
				

				backgroundZIndex:1,
				backgroundParallax:10,
				backgroundOffsetY:0,
				backgrounds:[
// //
// {images:["backgroundFore1.png","backgroundFore2.png","backgroundFore3.png","backgroundFore4.png"],parallaxAdd:10},
// {images:["background1.png","background2.png","background3.png","background4.png"]},

// {images:["backgroundSky1.png"],parallaxAdd:100},
// {images:["background1.png"],parallaxAdd:10},
// {images:["backgroundFore1.png"]},

//				{images:["backgroundSky1_v1.png"],parallaxAdd:400},
//				{images:["backgroundSky1.png","backgroundSky2.png","backgroundSky3.png","backgroundSky4.png"],parallaxAdd:400},

// OLD :
//					{images:["backgroundSky2_v2.png"],parallaxAdd:400,
//							globalCrimson:{image:"backgroundSky.crimson.png",triggerRange:"40=>60"},
//							globalStars:{image:"backgroundSky.stars.png",triggerRange:"30=>50"}
//					},
//					{images:["background1_v1.png"],parallaxAdd:200},
//					{images:["backgroundFore1_v1.png"],parallaxAdd:20},
				
// NEW :
					{images:["backgroundSky2_v2.png"],parallaxAdd:400,
							globalCrimson:{image:"nightSky.crimson.png",triggerRange:"28=>35"},
							globalStars:{image:"nightSky2.stars.png",triggerRange:"20=>30"}
					},
					{images:[{normal:"background1_v1.png",dark:"background1_v1.dark.png"}],parallaxAdd:200},
					{images:[{normal:"backgroundFore1_v1.png",dark:"backgroundFore1_v1.dark.png"}],parallaxAdd:20},
//					{images:[{normal:"foregroundGround1.png",dark:"foregroundGround1.dark.png"}],parallaxAdd:0,offsetY:-230,clipSize:{w:1600,h:100}},
					{images:[{normal:"ground2.png",dark:"ground2.dark.png"}],parallaxOverride:5/*TODO : FIXME : MAGIC NUMBER : PARALLAX 5 IS NO RELATIVE MOTION !!!*/,offsetY:-230},

				],
				
				
				foregroundParallax:.4,
// foregroundOffsetY:10,
				foregrounds:[
// {images:["foreground1.png"],offsetY:-20},
					{images:[ {normal:"foreground1_v1.png",dark:"foreground1.dark.png"} ],offsetY:-20},
				],
				
			},

			
			
			
			
			
			proto_regionNowhere:{
				
				usesSuper:"proto_abstract_region",
				
				
				DaylightManager:{
					cardinality:{
						value:1,
						prototypesInstantiation:{
							staticInstantiations:[
								{name:"proto_daylightManager_nowhere",spawningZone:{x:24800000,y:0},preinstanciate:true },
							]
						},
					},
				},

				// first argument is always cardinality, probability to appear, if and
				// only if property is a collection :
				// (cardinality 1 = A weather is always instantiated)
				Weather:{
					cardinality:{
						value:1,
						prototypesInstantiation:{
							randomInstantiations:["proto_none"
							// ,"proto_windy"
								]
						}
					},
				},
				
				Train:{
					cardinality:{
						value:"staticGroups",
						prototypesInstantiation:{
							staticInstantiations:[
								{name:"proto_clanTrainLambda",spawningZone:{x:200,y:0},preinstanciate:true},
								
							]
						},
					},
				},
				
// Doodle:{
// cardinality:{
// value:"fill", //fillInstantiationMargin:100,
// spawningZone:{w:400, h:0},
// prototypesInstantiation:"proto_lamp1",
// // prototypesInstantiation:{
// // randomInstantiations:["proto_lamp1"],
// // },
// },
// },
				

				Station:{
					
					cardinality:{
// value:"oneGroupAtRandom",
						value:"staticGroups",
						
						prototypesInstantiation:{
// randomInstantiations:[
// {value:"1->2",name:"proto_station", spawningZone:{x:20000, w:1000} },
// {value:"1->2",name:"proto_station", spawningZone:{x:40000, w:1000} },
// {value:"1->2",name:"proto_station", spawningZone:{x:80000, w:1000} }
// ],
							staticInstantiations: [
								{name:"proto_station",spawningZone:{x:-100,y:-7},preinstanciate:true},
							]
							
						}
					},
					
				},
				
				

				
			},
			
			
			

			proto_regionNorthPole:{
				
				usesSuper:"proto_abstract_region",
				
				
				DaylightManager:{
					cardinality:{
						value:1,
						prototypesInstantiation:{
							staticInstantiations:[
//							{name:"proto_daylightManager",spawningZone:{x:24889000,y:0},preinstanciate:true },
//								{name:"proto_daylightManager",spawningZone:{x:24930000,y:0},preinstanciate:true },
								{name:"proto_daylightManager",spawningZone:{x:24820000,y:0},preinstanciate:true },
								
								
							]
						},
					},
				},

				// first argument is always cardinality, probability to appear, if and
				// only if property is a collection :
				// (cardinality 1 = A weather is always instantiated)
				Weather:{
					cardinality:{
						value:1,
						prototypesInstantiation:{
							randomInstantiations:["proto_none"
// ,"proto_windy"
								]
						}
					},
				},
				
				Train:{
					cardinality:{
						value:"staticGroups",
						prototypesInstantiation:{
							staticInstantiations:[
								{name:"proto_clanTrain",spawningZone:{x:200,y:0},preinstanciate:true },
								{name:"proto_clanTrainMater",spawningZone:{x:3401,y:0},preinstanciate:true },
							]
						},
					},
				},
				
				// Doodle:{
				// cardinality:{
				// value:"fill", //fillInstantiationMargin:100,
				// spawningZone:{w:400, h:0},
				// prototypesInstantiation:"proto_lamp1",
				// // prototypesInstantiation:{
				// // randomInstantiations:["proto_lamp1"],
				// // },
				// },
				// },
				
				Station:{

					cardinality:{
// value:"oneGroupAtRandom",
						value:"staticGroups",
						
						avoidOverlap:"x",
						
						prototypesInstantiation:{
// randomInstantiations:[
// {value:"1->2",name:"proto_station", spawningZone:{x:20000, w:1000} },
// {value:"1->2",name:"proto_station", spawningZone:{x:40000, w:1000} },
// {value:"1->2",name:"proto_station", spawningZone:{x:80000, w:1000} }
// ],
								staticInstantiations: [
									
//									// DBG
//									 {value:1,name:"proto_station", spawningZone:{x:3000, y:-7},preinstanciate:true, },
//									 {value:1,name:"proto_station", spawningZone:{x:5000, y:-7},preinstanciate:true, },
								
									// // DBG
									// {value:1,name:"proto_station", spawningZone:{x:10000, y:-7},preinstanciate:true, },

//								{value:1,name:"proto_station", spawningZone:{x:30000, y:-7} ,preinstanciate:true, },
									
									{value:1,name:"proto_station", spawningZone:{x:70000, y:-7},preinstanciate:true, },
//								{value:1,name:"proto_station", spawningZone:{x:100000, y:-7},preinstanciate:true, }, // ~2500m (40px for 1m scale)
									{value:1,name:"proto_station", spawningZone:{x:160000, y:-7},preinstanciate:true, },
									{value:1,name:"proto_station", spawningZone:{x:220000, y:-7},preinstanciate:true, },
									{value:1,name:"proto_station", spawningZone:{x:350000, y:-7},preinstanciate:true, },

									// [500000 , 100000000-500000=] w=99500000 x=99500000/2+500000=50250000
//									{value:300,name:"proto_station",spawningZone:{x:50250000,y:-7,w:99500000},preinstanciate:true}, // every ~12km
									
									// PROD
//								{value:300,name:"proto_station",spawningZone:{x:50250000,y:-7,w:99500000},preinstanciate:true}, // every ~12km
//								{value:300,name:"proto_station",spawningZone:{x:50250000,y:-7,w:99500000},preinstanciate:false}, // every ~12km
									
								]
						}
					},

					
// cardinality:{
// // value:"oneGroupAtRandom",
// value:"staticGroups",
// prototypesInstantiation:{
// // randomInstantiations:[
// // {value:"1->2",name:"proto_station", spawningZone:{x:20000, w:1000} },
// // {value:"1->2",name:"proto_station", spawningZone:{x:40000, w:1000} },
// // {value:"1->2",name:"proto_station", spawningZone:{x:80000, w:1000} }
// // ],
// staticInstantiations: [
// {value:4,name:"proto_station",spawningZone:{x:5000,y:-7,w:10000},preinstanciate:true},
// ]
// }
// },
					
				},
				
				
				
				

				GameDialog:{
					cardinality:{
						value: "staticGroups",
						prototypesInstantiation:{
							staticInstantiations: [
								
//							// FOR DEBUG ONLY :
//							{name:"proto_debugDialog0",preinstanciate:true},
//							{name:"proto_debugDialog1",preinstanciate:true},
//							{name:"proto_debugDialog2",preinstanciate:true},
//							{name:"proto_debugDialog3",preinstanciate:true},

			
								// PROD
								{name:"proto_startDialog", preinstanciate:true},
								{name:"proto_dialogTutoHabitations", preinstanciate:true},
									{name:"proto_dialogTutoQuestHabitationsAcceptation", preinstanciate:true},
									{name:"proto_dialogTutoQuestHabitationsRefusal", preinstanciate:true},

								// NO : because  they´re children of a GameQuest, instead ! : {name:"proto_dialogTutoOverEquipment", preinstanciate:true},
								{name:"proto_dialogTutoFirstStation", preinstanciate:true},

								// NO : because  they´re children of a GameQuest, instead ! : {name:"proto_dialogTutoCooker", preinstanciate:true},
								
								{name:"proto_storyDialog1", preinstanciate:true},
								{name:"proto_storyDialogAwaken", preinstanciate:true},
								{name:"proto_storyDialogLetHerSleep", preinstanciate:true},

								{name:"proto_dialogStoryDialog2", preinstanciate:true},

								


							]
						},
					},
				},
				
				
				GrandeLigneQuestsManager:{
					
					cardinality:{
						value:1,
						prototypesInstantiation:{
							staticInstantiations: [
										{name:"proto_questsManagerNorthPole",preinstanciate:true},
										
							]
						}
					},
					
				},
				
				
				
			},
			
			
		},
		

		Weather:{

			proto_abstract_Weather:{
				backgroundParallax:5,
			},

			proto_none:{
				usesSuper:"proto_abstract_Weather",

				temperature:"-25->0",
				precipitations:"5->30",
				precipitationsType:"snow",
				wind:"0->20",

// backgroundImage:"backgroundSnow.png",

			},
// proto_windy:{
// usesSuper:"proto_abstract_Weather",
//
// temperature:"-10->10",
// wind:"10->50",
//
// backgroundImage:"backgroundWind.png",
// },
			
		},
		
		
		
		
		Train:{

			proto_clanTrain:{
				
				ui:{
					drawSpeed:false,
				},
				
				inertiaTimeMillis:6000,
				nonStopSpeed:10,
				
				maxPlatforms:20,
				// PROD
				thumbsPlatformsPerLine:10,

				RollingPlatform:{
					
					cardinality:{
						value: "staticGroups",
						prototypesInstantiation:{
							staticInstantiations: [
								{name:"proto_platform1",spawningZone:{x:0,y:0},preinstanciate:true},
								{name:"proto_platform2",spawningZone:{x:-200,y:0},preinstanciate:true},
								
								// DBG
//								{name:"proto_platformMaterRandom",spawningZone:{x:-400,y:0},preinstanciate:true},
//								{name:"proto_platformMaterRandom",spawningZone:{x:-600,y:0},preinstanciate:true},
//								{name:"proto_platformMaterRandom",spawningZone:{x:-800,y:0},preinstanciate:true},
//								{name:"proto_platformMaterRandom",spawningZone:{x:-1000,y:0},preinstanciate:true},

							]
						},
						
					},
				},
				
				Ergostasio:{
					cardinality:{

						// No need for a «subClass» attribute, since its class is used
						// directly in attribute definition

						value:"staticGroups",
						prototypesInstantiation:{
							staticInstantiations: [
								{name:"proto_ergostasio",spawningZone:{x:-200,y:18},preinstanciate:true},
							]},
					},
				},
				
				People: {
					
					cardinality:{
						
						value: "staticGroups",

						prototypesInstantiation:{
							
							// randomInstantiations:[ {value:"10->15",name:"proto_normalPeople", spawningZone:{x:-100, y:20, w:200},preinstanciate:true },
							
							
							staticInstantiations: [

								// PROD
								{value:20,name:"proto_normalPeople", spawningZone:{x:-150, y:20, w:200},preinstanciate:true },
								//DBG
								//{value:1,name:"proto_normalPeople", spawningZone:{x:-150, y:20, w:200},preinstanciate:true },

//							// DBG
//							{value:1,name:"proto_normalPeople", spawningZone:{x:-150, y:20, w:200},preinstanciate:true },

								
								
// staticInstantiations: [
// {name:"proto_normalPeople",spawningZone:{x:-5,y:-5},preinstanciate:true},
// {name:"proto_normalPeople",spawningZone:{x:0,y:-5},preinstanciate:true},
// {name:"proto_normalPeople",spawningZone:{x:5,y:-5},preinstanciate:true},
// {name:"proto_normalPeople",spawningZone:{x:10,y:-5},preinstanciate:true},

								
							]},
					},
					
					
				},
				
				
				
				
			},
			
			
			proto_clanTrainMater:{
				
				inertiaTimeMillis:6000,
				nonStopSpeed:10,
				selectable:false,
				clickable:false,
				GameScript:{
					cardinality:{
						value: "staticGroups",
						prototypesInstantiation:{
							staticInstantiations: [
								{name:"proto_scriptStartFadeOut",preinstanciate:true},
								{name:"proto_scriptStopFadeOut",preinstanciate:true},

							]
						},
					},
				},
				RollingPlatform:{
					cardinality:{
						value: "staticGroups",
						prototypesInstantiation:{
							staticInstantiations: [
								{name:"proto_platformMaterLocomotive",spawningZone:{x:0,y:0},preinstanciate:true},
								{name:"proto_platformMaterEmpty",spawningZone:{x:-200,y:0},preinstanciate:true},
								{name:"proto_platformMaterRandom",spawningZone:{x:-400,y:0},preinstanciate:true},
								{name:"proto_platformMaterRandom",spawningZone:{x:-600,y:0},preinstanciate:true},
								{name:"proto_platformMaterRandom",spawningZone:{x:-800,y:0},preinstanciate:true},
								{name:"proto_platformMaterRandom",spawningZone:{x:-1000,y:0},preinstanciate:true},
								{name:"proto_platformMaterRandom",spawningZone:{x:-1200,y:0},preinstanciate:true},
								{name:"proto_platformMaterRandom",spawningZone:{x:-1400,y:0},preinstanciate:true},
								{name:"proto_platformMaterRandom",spawningZone:{x:-1600,y:0},preinstanciate:true},
								{name:"proto_platformMaterRandom",spawningZone:{x:-1800,y:0},preinstanciate:true},
								{name:"proto_platformMaterRandom",spawningZone:{x:-2000,y:0},preinstanciate:true},
								{name:"proto_platformMaterRandom",spawningZone:{x:-2200,y:0},preinstanciate:true},
								{name:"proto_platformMaterRandom",spawningZone:{x:-2400,y:0},preinstanciate:true},
								{name:"proto_platformMaterRandom",spawningZone:{x:-2600,y:0},preinstanciate:true},
								{name:"proto_platformMaterRandom",spawningZone:{x:-2800,y:0},preinstanciate:true},
								{name:"proto_platformMaterEmpty",spawningZone:{x:-3000,y:0},preinstanciate:true},
							]
						},
					},
				},
				
				Ergostasio:{
					cardinality:{
						value:"staticGroups",
						prototypesInstantiation:{
							staticInstantiations: [
								{name:"proto_ergostasio",spawningZone:{x:-200,y:18},preinstanciate:true},
								{name:"proto_ergostasio",spawningZone:{x:-800,y:18},preinstanciate:true},
								{name:"proto_ergostasio",spawningZone:{x:-1000,y:18},preinstanciate:true},
								{name:"proto_ergostasio",spawningZone:{x:-2600,y:18},preinstanciate:true},

							]},
					},
				},
				
				
				People: {
					
					cardinality:{
						
						value: "staticGroups",
						prototypesInstantiation:{
							staticInstantiations: [
								{value:40,name:"proto_normalPeople", spawningZone:{x:-1200, y:20, w:2400},preinstanciate:true },
							]},
					},
				},
				
				
				
			},
			
			
			
			proto_clanTrainLambda:{
				inertiaTimeMillis:5000,
				
				nonStopSpeed:0,
				
				selectable:false,
				clickable:false,
				
				RollingPlatform:{
					
					cardinality:{
						value: "staticGroups",
						prototypesInstantiation:{
							staticInstantiations: [
								{name:"proto_lambdaPlatform1",spawningZone:{x:0,y:0},preinstanciate:true},
								{name:"proto_lambdaPlatformRandom",spawningZone:{x:-200,y:0},preinstanciate:true},
								{name:"proto_lambdaPlatformRandom",spawningZone:{x:-400,y:0},preinstanciate:true},
								{name:"proto_lambdaPlatformRandom",spawningZone:{x:-600,y:0},preinstanciate:true},
								{name:"proto_lambdaPlatformRandom",spawningZone:{x:-800,y:0},preinstanciate:true},
								{name:"proto_lambdaPlatformRandom",spawningZone:{x:-1000,y:0},preinstanciate:true},
							]
						},
						
					},
				},
				
				Ergostasio:{
					cardinality:{
						value:"staticGroups",
						prototypesInstantiation:{
							staticInstantiations: [
								{name:"proto_ergostasio",spawningZone:{x:-600,y:18},preinstanciate:true},
							]},
					},
				},
				
//				People: {
//					
//					cardinality:{
//						
//						value: "staticGroups",
//						prototypesInstantiation:{
//							staticInstantiations: [
//								{value:60,name:"proto_normalPeople", spawningZone:{x:-400, y:20, w:800},preinstanciate:true },
//							]},
//					},
//				},
				
				
			},
			
		
			
			
		
		
		},

		
	
		
		GrandeLigneGauge:{
			
			
			proto_gaugeEquipmentStrong:{
				
				
				gaugeValue:2000000,
				gaugeMax:2000000,
				breakdownThresholdPercent:20,
				
				
				workingTemperatures:"-100=>200",
				normalWear:200,
				underheatWear:5000,
				overheatWear:5000,
				wearRateMillis:1000,
				
				thresholdsMessages:[
					{
						valuePercent:30,
						message:i18n("attentionLÉquipementVaBientôtCesserDeFonctionner")
					},
				],
				
			},
			
			
			proto_gaugeEquipment:{
				
				
				gaugeValue:1600000,
				gaugeMax:1600000,
				
				breakdownThresholdPercent:20,
				
				workingTemperatures:"-100=>200",
				
				normalWear:200,
				underheatWear:5000,
				overheatWear:5000,
				wearRateMillis:1000,
				
				thresholdsMessages:[
					{
						valuePercent:30,
						message:i18n("attentionLÉquipementVaBientôtCesserDeFonctionner")
					},
				],
				
			},
			
			
			proto_gaugePeople:{
				
				
				gaugeValue:92000,
				gaugeMax:92000,
				
				breakdownThresholdPercent:30, // We must keep some time to let them go to the rest place.
				
				workingTemperatures:"-20=>60",
				
				normalWear:100,
				
				underheatWear:4000,
				overheatWear:4000,
				wearRateMillis:1000,
				
//			thresholdsMessages:[
//				{
//					valuePercent:50,
//					message:i18n("attentionUneConcertadeVaÊtreÉpuisée")
//				},
//			],
				
				
				
				
				
			},
			
		},
		

		
		
		ResourcesRepository:{
			
			
			proto_abstract_resourcesRepository: {

				spacingPixelsNumber:5,
				maxLotHeightAmount:5,
				yOffsetRepository:20,

				
				// excludedResourcesImages:["glykergol"],
			},			
			
			
			proto_emptyResourcesRepository:{

				usesSuper:"proto_abstract_resourcesRepository",
				
				resources:{
					
					// BASE RESOURCES
					
					glykergol:{amount:0,capacity:10,stationPlace:1,color:"#FFEE00"},
					water:{amount:0,capacity:10,stationPlace:1,color:"#4488EE"},
					food:{amount:0,capacity:10,stationPlace:1,color:"#22FF44"},

					// ORES
					sand:{amount:0,capacity:10,stationPlace:2,color:"#FFF77E"},
					metal:{amount:0,capacity:10,stationPlace:2,color:"#8CB2C5"},
					coal:{amount:0,capacity:10,stationPlace:2,color:"#818181"},
					
					// PRIMARY RESOURCES
					// (in cooker)
					meals:{amount:0,capacity:10},

					// SECONDARY RESOURCES
					// (in refinery)
					quartzs:{amount:0,capacity:10},
					// (in forge)
					alloys:{amount:0,capacity:10},
					// (in catalyst)
					plastics:{amount:0,capacity:10},
					
					// TERTIARY RESOURCES
					// (in factory)
					manufactured:{amount:0,capacity:10},
					designed:{amount:0,capacity:10},
					
					// WASTES RESOURCES
					darkWater:{amount:0,capacity:10},
					mineralWaste:{amount:0,capacity:10},
					technoWaste:{amount:0,capacity:10},
					
//					// DBG
//					glykergol:{amount:0,capacity:10,stationPlace:1,color:"#FFEE00"},

					
				},
				
			},
			
			
			
			proto_givenResourcesRepository:{
				
				usesSuper:"proto_abstract_resourcesRepository",
				
				resources:{
					
					// BASE RESOURCES
					
					glykergol:{amount:10,capacity:10,stationPlace:1,color:"#FFEE00"},
					water:{amount:10,capacity:10,stationPlace:1,color:"#4488EE"},
					food:{amount:10,capacity:10,stationPlace:1,color:"#22FF44"},

					// ORES
					sand:{amount:10,capacity:10,stationPlace:2,color:"#FFF77E"},
					metal:{amount:10,capacity:10,stationPlace:2,color:"#8CB2C5"},
					coal:{amount:10,capacity:10,stationPlace:2,color:"#818181"},
					
					// PRIMARY RESOURCES
					// (in cooker)
					meals:{amount:10,capacity:10},

					// SECONDARY RESOURCES
					// (in refinery)
					quartzs:{amount:0,capacity:10},
					// (in forge)
					alloys:{amount:0,capacity:10},
					// (in catalyst)
					plastics:{amount:0,capacity:10},
					
					// TERTIARY RESOURCES
					// (in factory)
					manufactured:{amount:0,capacity:10},
					designed:{amount:0,capacity:10},
					
					// WASTES RESOURCES
					darkWater:{amount:0,capacity:10},
					mineralWaste:{amount:0,capacity:10},
					technoWaste:{amount:0,capacity:10},
					
//					// DBG
//					glykergol:{amount:10,capacity:10,stationPlace:1,color:"#FFEE00"},
				},
				
			},
			
			
			proto_randomResourcesRepository:{
				
				usesSuper:"proto_abstract_resourcesRepository",

				yOffsetRepository:26,

				resources:{
					
					// BASE RESOURCES
					glykergol:{amount:"5->10",capacity:10,stationPlace:1,color:"#FFEE00"},
					water:{amount:"5->10",capacity:10,stationPlace:1,color:"#4488EE"},
					food:{amount:"5->10",capacity:10,stationPlace:1,color:"#22FF44"},

					// PRIMARY RESOURCES
					// (ores)
					sand:{amount:"5->10",capacity:10,stationPlace:2,color:"#FFF77E"},
					metal:{amount:"5->10",capacity:10,stationPlace:2,color:"#8CB2C5"},
					coal:{amount:"5->10",capacity:10,stationPlace:2,color:"#818181"},

					// SECONDARY RESOURCES
					// (in cooker)
					meals:{amount:"5->10",capacity:10},
					// (in refinery)
					quartzs:{amount:"5->10",capacity:10},
					// (in forge)
					alloys:{amount:"5->10",capacity:10},
					// (in catalyst)
					plastics:{amount:"5->10",capacity:10},
					
					// TERTIARY RESOURCES
					// (in factory)
					manufactured:{amount:"5->10",capacity:10},
					designed:{amount:"5->10",capacity:10},
					
					// WASTES RESOURCES
					darkWater:{amount:"5->10",capacity:10},
					mineralWaste:{amount:"5->10",capacity:10},
					technoWaste:{amount:"5->10",capacity:10},
					
//					// DBG
//					glykergol:{amount:"5->10",capacity:10,stationPlace:1,color:"#FFEE00"},
					
				},
				
				
			},
			
			
			proto_stationResourcesRepository:{
				
				usesSuper:"proto_abstract_resourcesRepository",

				yOffsetRepository:26,

				resources:{
					
					// BASE RESOURCES
					// PROD
//					glykergol:{amount:"8->20",capacity:20,stationPlace:1,color:"#FFEE00"},
					water:{amount:"8->20",capacity:20,stationPlace:1,color:"#4488EE"},
					food:{amount:"8->20",capacity:20,stationPlace:1,color:"#22FF44"},

					// PRIMARY RESOURCES
					// (ores)
					sand:{amount:"8->20",capacity:20,stationPlace:2,color:"#FFF77E"},
					metal:{amount:"8->20",capacity:20,stationPlace:2,color:"#8CB2C5"},
					coal:{amount:"8->20",capacity:20,stationPlace:2,color:"#818181"},

					// SECONDARY RESOURCES
					// (in cooker)
					meals:{amount:0,capacity:20},
					// (in refinery)
					quartzs:{amount:0,capacity:20},
					// (in forge)
					alloys:{amount:0,capacity:20},
					// (in catalyst)
					plastics:{amount:0,capacity:20},
					
					// TERTIARY RESOURCES
					// (in factory)
					manufactured:{amount:0,capacity:20},
					designed:{amount:0,capacity:20},
					
					// WASTES RESOURCES
					darkWater:{amount:0,capacity:20},
					mineralWaste:{amount:0,capacity:20},
					technoWaste:{amount:0,capacity:20},
					
					// DBG
					glykergol:{amount:"8->20",capacity:10,stationPlace:1,color:"#FFEE00"},
					
					
				},
				
				
			},
			
			
			

			
		},
		

		

		DaylightManager:{
			
			
			
			proto_daylightManager:{

// year:0,
// day:0,
// hour:0,
// dayColor:"#F4E542",
				nightColor:"#000000",
				
				
//				temperatureFactor:0.001,
				
//				baseTemperature:15, // The temperature to start with this light
//														// percent
				
				minTemperature:-200,
//			UNUSED :	pointBreakTemperature:-100,
				maxTemperature:200,
				
				nonStopSpeed:10, // (same as the default speed of the train.)
				
//				duskZoneFactor:0.02, // The factor size of the dusk zone.
				duskZoneFactor:0.019, // The factor size of the dusk zone.
				
				
				temperatureDecimalPrecision:1,
				
				
//				lightPercentFactor:0.01,
//				baseLightPercent:80,
//				minLightPercent:0,
//				maxLightPercent:100,
				
//				backgroundLightFactor:.7,
				
//				lightedHemisphereAreaFactor:1,
				
				
				workingTemperatures:"-20=>60", // (copied values from People gauges set)

				
				minTempForHeat:150,
				maxTempForHeat:200,

				
				imageConfig:{
					_2D:{
						horizontal:{
							"20=>70":{imageSrc:"sunnyEveningLayout.png"},
							"40=>100":{imageSrc:"sunnyLayout.png",peekFactor:1},
						}
					}
				},
				
				
			
			},
			
			
			
			proto_daylightManager_nowhere:{
				usesSuper:"proto_daylightManager",
				
//			baseTemperature:-15,
//			baseLightPercent:30,

			},
			
			
			
//			proto_daylightManagerDebug:{
//				usesSuper:"proto_daylightManager",
//				imageConfig:{
//					_2D:{
//						horizontal:{}
//					}
//				},
//			},

		},
	
	
		
		
		
		
	
		GameSmokePit:{
			
			proto_smokePitDark:{
				
				
//				defaultOpacity:0.5,
//				appearRateMillis:100,
//				puffStartSize:20,
//				puffEndSize:200,
//				puffExpandSpeed:12,
//				maxPuffsNumber:20,

				
				defaultOpacity:.8,
//			appearRateMillis:100,
				puffStartSize:10,
//				puffEndSize:200,
				puffEndSize:200,
//			puffExpandSpeed:5,
				puffExpandSpeed:10,
				//maxPuffsNumber:30,
				maxPuffsNumber:10,

				imageConfig:{
					_2D:{

						
						zIndex:0,
						center:{x:"center",y:"bottom"},
						
						sourcesPaths:[
							"smokeDark1.png","smokeDark2.png","smokeDark3.png","smokeDark4.png","smokeDark5.png",
							"smokeDark6.png","smokeDark7.png","smokeDark8.png","smokeDark9.png","smokeDark10.png",
							"smokeDark11.png",
						]

					},
				},
				
			
			},
			
			proto_smokePitDarkWhite:{
				
				
				defaultOpacity:0.8,
//				appearRateMillis:100,
//				puffStartSize:20,
				puffStartSize:10,
//				puffEndSize:200,
//				puffExpandSpeed:12,
//				maxPuffsNumber:20,
				puffEndSize:200,
				puffExpandSpeed:8,
				maxPuffsNumber:8,


				imageConfig:{
					_2D:{

						
						zIndex:0,
						center:{x:"center",y:"bottom"},
						
						sourcesPaths:[
							"smokeDarkWhite1.png","smokeDarkWhite2.png","smokeDarkWhite3.png","smokeDarkWhite4.png","smokeDarkWhite5.png",
							"smokeDarkWhite6.png","smokeDarkWhite7.png","smokeDarkWhite8.png","smokeDarkWhite9.png","smokeDarkWhite10.png",
							"smokeDarkWhite11.png",
						]

					},
				},
				
				
			
			},
			
			proto_smokePitLight:{
				
				
				defaultOpacity:0.8,
//				appearRateMillis:50,
				puffStartSize:5,
				puffEndSize:80,
				puffExpandSpeed:5,
//				maxPuffsNumber:10,
				maxPuffsNumber:5,


				imageConfig:{
					_2D:{

						
						zIndex:0,
						center:{x:"center",y:"bottom"},
						
						sourcesPaths:[
							"smokeLight1.png","smokeLight2.png","smokeLight3.png","smokeLight4.png","smokeLight5.png",
							"smokeLight6.png","smokeLight7.png","smokeLight8.png","smokeLight9.png","smokeLight10.png",
							"smokeLight11.png","smokeLight12.png","smokeLight13.png",
						]

					},
				},
				
				
				
				
			
			},
			
			
			
				
		},

		GameSunShadow:{
			proto_carShadow:{
				
				
				minimalDarkPercent:40,

				minAngle:5,
				maxAngle:80,
				
				shadowColor:"#010101",
				blur:2,
				
				
				imageConfig:{
					_2D:{
						horizontal:{
							
							thickness:4,
							groundHeight:4,
							sunSide:"right",
								
								
						}
						
//						
// zIndex:0,
// center:{x:"center",y:"bottom"},
// sourcePath:"station.png",
// sourcePathBack:"station.back.png",
// sourcePathDark:"station.dark.png",
// sourcePathBump:"station.bump.png",
//						
//						
					},
				},
				
				
				
				
			},
			
		},
	
	
	
	

		GameThumb:{
			
			proto_abstract_thumbNormal:{
				right:80,
				bottom:145,
				sizeFactor:2,
				scaleFactor:2,
				
				offsetsUI:{y:-22},
				
			},
			
			// Basics
			
			proto_locomotiveThumb:{
				usesSuper:"proto_abstract_thumbNormal",
				thumbSrc:"locomotive.thumb.png",
			},
			proto_ergostasioThumb:{
				usesSuper:"proto_abstract_thumbNormal",
				// CAUTION ! The ergostasio is not at the same tree level as the dockableEquipments, which it is not :
				bottom:152,
				thumbSrc:"ergostasio.thumb.png",
			},
			proto_habitationIIThumb:{
				usesSuper:"proto_abstract_thumbNormal",
				thumbSrc:"habitationII.thumb.png",
			},
			proto_habitationIThumb:{
				usesSuper:"proto_abstract_thumbNormal",
				thumbSrc:"habitationI.thumb.png",
			},
			
			
			// Producers
			proto_refineryThumb:{
				usesSuper:"proto_abstract_thumbNormal",
				thumbSrc:"refinery.thumb.png",
			},
			proto_cookerThumb:{
				usesSuper:"proto_abstract_thumbNormal",
				thumbSrc:"cooker.thumb.png",
			},
			proto_forgeThumb:{
				usesSuper:"proto_abstract_thumbNormal",
				thumbSrc:"forge.thumb.png",
			},
			proto_catalystThumb:{
				usesSuper:"proto_abstract_thumbNormal",
				thumbSrc:"catalyst.thumb.png",
			},
			proto_coreThumb:{
				usesSuper:"proto_abstract_thumbNormal",
				thumbSrc:"core.thumb.png",
			},
			proto_factoryThumb:{
				usesSuper:"proto_abstract_thumbNormal",
				thumbSrc:"factory.thumb.png",
			},
			proto_retreaterThumb:{
				usesSuper:"proto_abstract_thumbNormal",
				thumbSrc:"retreater.thumb.png",
			},
			
			
			// Culturers
			proto_universityThumb:{
				usesSuper:"proto_abstract_thumbNormal",
				thumbSrc:"university.thumb.png",
			},
			
			
			
			proto_abstract_thumbBig:{
				right:80,
				bottom:155,
				sizeFactor:2,
				scaleFactor:2,
				
				offsetsUI:{y:-22},
			},
			proto_watererThumb:{
				usesSuper:"proto_abstract_thumbBig",
				thumbSrc:"waterer.thumb.png",
			},
			proto_solarerThumb:{
				usesSuper:"proto_abstract_thumbBig",
				thumbSrc:"solarer.thumb.png",
			},
			proto_plasmaTorchThumb:{
				usesSuper:"proto_abstract_thumbBig",
				thumbSrc:"plasmaTorch.thumb.png",
			},

		},
		
		
		
		
		
	},
};
