// DEPENDENCY:init.js




// ===============================================================================
// 																			CONFIG
// ===============================================================================



// ===============================================================================
// 																			MODEL
// ===============================================================================




// Prototype can define as a well a group of things as particular individuals:only by not setting a random variance on them.

// ------------------ Basic objects ------------------		




PROTOTYPES_CONFIG.allPrototypes.RollingPlatform={

	proto_abstract_rollingPlatform: {
	
		
		isFocusable:true,
		
		WheelsSet:{
			cardinality:{
				value:"staticGroups",
				prototypesInstantiation:{
					staticInstantiations: [
						{name:"proto_wheelsSetTiresSmall",spawningZone:{x:-80,y:-5},preinstanciate:true},
						{name:"proto_wheelsSetTiresSmall",spawningZone:{x:80,y:-5},preinstanciate:true},
					]},
			},
		},
		
		GrandeLigneGauge:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_gaugeEquipment",
			},
		},
		
		GameSunShadow:{
			cardinality:{
				value:1,
				prototypesInstantiation:{
					staticInstantiations: [
						{name:"proto_carShadow",spawningZone:{x:0,y:-10},preinstanciate:true }
					],
				},
			}
		},
		
		
		size:{w:200,h:20},
			
		ResourcesRepository:{
			cardinality:{
				value:1,
				prototypesInstantiation:{
					staticInstantiations:[
						{name:"proto_emptyResourcesRepository",preinstanciate:true }
					]
				},
			},
		},
			
	
		infos:{
			title:"Platform",
			description:i18n("cetÉquipementEstUneSimplePlateforme"),
		  actions:{

		  	callErgostasio:{label:i18n("appelerUneErgostasio"),value:5},
//				  setRetrieveAllResources:{label:"Select all resources"},
//				  setRetrieveNeededResources:{label:"Select needed resources"},
//				  setRetrieveNoneResources:{label:"Select no resources"},

		  	
		  	buildBasics:{label:i18n("construireBasiques"),value:{
		  	
//		  				// Builders cannot build a platform, we need an ergostasio :
//				  		proto_platformI:{
//				  			type:"platform",
//				  			
//				  			iconSrc:"platformI.png",
//				  			title:i18n("plateforme"),
//				  			description:i18n("cetÉquipementSertDeSupportAuxAutresÉquipementsEtPermet"),
//				  			
//				  			inactive:true,
//				  			
////				  			timeToBuild:(IS_DEBUG?2:30),
////
////				  			costs:{
////				  				glykergol:1,
////				  				metal:3,
////				  			},
//				  		},
				  		
				  		
				  		proto_ergostasio:{
				  			type:"ergostasio",

				  			subClass:"Ergostasio",// Actually, not useful
				  			
				  			iconSrc:"ergostasio.png",
				  			title:i18n("ergostasio"),
				  			description:i18n("lÉquipementPrincipalDuTrainIlPermetDeLAgrandirDeCon"),

								// TODO : Allow to build.
				  			inactive:true,
				  			timeToBuild:(IS_DEBUG?2:60),

				  			costs:{
				  				glykergol:1,
				  				metal:3,
				  				alloys:2,plastics:1,
				  				designed:1,
				  			},
				  		},
				  		
				  		
				  		proto_habitationI:{
				  			type:"dockableEquipment",
				  			
				  			subClass:"Habitation",
				  			
				  			newBuildingPosition:{x:0,y:18},					  			
				  			iconSrc:"habitationI.png",
				  			title:i18n("habitation"),
				  			description:i18n("uneHabitationPourHébergerLaPopulation"),

				  			timeToBuild:(IS_DEBUG?2:30),

				  			
				  			costs:{
				  				glykergol:1,
				  				metal:4,
				  			},
				  			
				  		},
				  		
	  				}
						  		
			  	},

			  	
			  	
			  	
			  	buildProductions:{label:i18n("construireProductions"),value:{
			  		
			  		
						// PRIMARY RESOURCES (total : 2+6)

			  		proto_cooker:{
			  			type:"dockableEquipment",

			  			subClass:"Producer",
			  			newBuildingPosition:{x:0,y:18},
			  			iconSrc:"cooker.png",
			  			title:i18n("cuisine"),
			  			description:i18n("permetDeCuisinerDesPlatsPréparés"),

			  			timeToBuild:(IS_DEBUG?2:30),

			  			costs:{
			  				glykergol:1,
								sand:2, metal:2, coal:2,
			  			},
			  			
			  		},
			  		
			  		
						// SECONDARY RESOURCES (total : 2+10)

			  		proto_refinery:{
			  			type:"dockableEquipment",

			  			subClass:"Producer",
			  			newBuildingPosition:{x:0,y:18},
			  			iconSrc:"refinery.png",
			  			title:i18n("raffinerie"),
			  			description:i18n("uneInstallationQuiPermetDeCréerDesRessourcesPlusÉlaboré"),

			  			timeToBuild:(IS_DEBUG?2:40),

			  			costs:{
			  				glykergol:1,
								sand:3, metal:5, coal:2,
			  			},
			  			
			  		},
			  		

			  		proto_forge:{
			  			type:"dockableEquipment",

			  			subClass:"Producer",
			  			newBuildingPosition:{x:0,y:18},
			  			iconSrc:"forge.png",
			  			title:i18n("forge"),
			  			description:i18n("forgeDesMétauxEtDesAlliages"),

			  			timeToBuild:(IS_DEBUG?2:50),

			  			costs:{
			  				glykergol:2,water:1,
								sand:3, metal:5, coal:2,
			  			},
			  			
			  		},
			  		
			  		
			  		proto_catalyst:{
			  			type:"dockableEquipment",

			  			subClass:"Producer",
			  			newBuildingPosition:{x:0,y:18},
			  			iconSrc:"catalyst.png",
			  			title:i18n("catalyseur"),
			  			description:i18n("synthétiseDesSubstancesIndustrielles"),

			  			timeToBuild:(IS_DEBUG?2:50),

			  			costs:{
			  				glykergol:2,
								sand:4, metal:4, coal:2,
			  			},
			  			
			  		},
			  		
			  		
			  		proto_retreater: {
			  			type:"dockableEquipment",

			  			subClass:"Producer",
			  			newBuildingPosition:{x:0,y:18},
			  			iconSrc:"retreater.png",
			  			title:i18n("retraiteur"),
			  			description:i18n("permetDeRecyclerLEauPolluéeEnEauÀNouveauPropre"),

			  			timeToBuild:(IS_DEBUG?2:80),

			  			costs:{
			  				glykergol:2,
			  				sand:3,
			  				metal:7,
			  				
			  			},
			  		},
			  		
			  		
		  		
		  		}
		  	},
			  	
			  	
		  	
		  	buildOverEquipments:{label:i18n("construireSurÉquipement"),value:{
	  		
		  		proto_waterer:{
		  			type:"overEquipment",
		  			
		  			subClass:"Producer",
		  			newBuildingPosition:{x:0,y:18},					  			
		  			iconSrc:"waterer.png",
		  			title:i18n("condensateurDEau"),
		  			description:i18n("permetDeCollecterDeLEauAtmosphérique"),
		  			
		  			timeToBuild:(IS_DEBUG?2:40),

		  			costs:{
		  				metal:2, coal:1,
		  			},
		  		},
	  		
		  		proto_solarer:{
		  			type:"overEquipment",
		  			
		  			subClass:"Producer",
		  			newBuildingPosition:{x:0,y:18},					  			
		  			iconSrc:"solarer.png",
		  			title:i18n("collecteurSolaire"),
		  			description:i18n("collecteLÉnergieSolaire"),

		  			timeToBuild:(IS_DEBUG?2:40),

		  			costs:{
		  				metal:2, sand:1,
		  			},
		  		},
		  		
		  		

				 }	  		
		  	},
		  	
		  	
		  	
		  	
		  	
		  	


		  	retrieveResource:{label:"%value%",values:[
			  		// Base :
		  			"glykergol","water","food",
		  			// Ores :
						"sand","metal","coal",
						// Cooker :
						"meals",
						// Refinery :
						"quartzs",
						// Forge :
						"alloys",
						// Catalyst :
						"plastics",
						// Factory :
						"manufactured","designed",
						// Wastes :
						"darkWater","mineralWaste","technoWaste"
					],type:"checkbox amount inactive", 
					readCheckedStateForValueMethod:"isRetrieved",readAmountAndTotalMethod:"getAmountAndTotal"},
					
		  },
			  
		},
		
	},
	
	
	
	
	proto_platformI: {

		usesSuper:"proto_abstract_rollingPlatform",
		
		passagewaySrc:"platformI-passageway.png",
		
		imageConfig:{
			_2D:{
				zIndex:3,
				center:{x:"center",y:"bottom"},
				sourcePath:"platformI.png",
				sourcePathDark:"platformI.dark.png",
				sourcePathBump:"platformI.bump.png",
			},
		},
		
	},
	
	
	
	
	proto_platformII: {
		
		usesSuper:"proto_abstract_rollingPlatform",
		
		passagewaySrc:"platformII-passageway.png",
		
		imageConfig:{
			_2D:{
				zIndex:3,
				center:{x:"center",y:"bottom"},
				sourcePath:"platformII.png",
				sourcePathDark:"platformII.dark.png",
				sourcePathBump:"platformII.bump.png",
			},
		},
		
		
	},
	
	
	
	
	proto_platform1: {

		usesSuper:"proto_platformI",
		inheritedPath:"proto_platformI",
		
		DockableEquipment:{
			cardinality:{
				value:1,
				prototypesInstantiation:{
					staticInstantiations: [
						{name:"proto_locomotive",subClass:"Locomotive",spawningZone:{x:0,y:18},preinstanciate:true }
					],
				},
			}
		},
		
		
		ResourcesRepository:{
			cardinality:{
				value:1,
				prototypesInstantiation:{
					staticInstantiations:[
						{name:"proto_givenResourcesRepository",preinstanciate:true }
					]
				},
			},
		},
		
		
		
	},
	proto_platform2: {
		usesSuper:"proto_platformI",
		inheritedPath:"proto_platformI",

		ResourcesRepository:{
			cardinality:{
				value:1,
				prototypesInstantiation:{
					staticInstantiations:[
						{name:"proto_givenResourcesRepository",preinstanciate:true}
					]
				},
			},
		},
		
	},
	
	proto_platformMaterEmpty:{
		usesSuper:"proto_platformI",
		inheritedPath:"proto_platformI",
	},
	proto_platformMaterLocomotive:{
		usesSuper:"proto_platformI",
		inheritedPath:"proto_platformI",
		DockableEquipment:{
			cardinality:{
				value:1,
				prototypesInstantiation:{
					staticInstantiations: [
						{name:"proto_locomotiveMater",subClass:"Locomotive",spawningZone:{x:0,y:18},preinstanciate:true }
					],
				},
			}
		},
	},
	
	
	proto_platformMaterRandom:{
		usesSuper:"proto_platformI",
		inheritedPath:"proto_platformI",
		DockableEquipment:{
			cardinality:{
				value:1,
				prototypesInstantiation:{
					staticInstantiations: [
						{names:[
									{value:"proto_core",subClass:"Producer"},
									{value:"proto_refinery",subClass:"Producer"},
									{value:"proto_factory",subClass:"Producer"},
									{value:"proto_retreater",subClass:"Producer"},
									{value:"proto_catalyst",subClass:"Producer"},
									{value:"proto_forge",subClass:"Producer"},
									{value:"proto_cooker",subClass:"Producer"},
									{value:"proto_university"},
									{value:"proto_habitationII",subClass:"Habitation"},
							],
							spawningZone:{x:0,y:18},
							preinstanciate:true
						}
					],
				},
			}
		},
		
		
		ResourcesRepository:{
			cardinality:{
				value:1,
				prototypesInstantiation:{
					staticInstantiations:[
						{name:"proto_randomResourcesRepository",preinstanciate:true }
					]
				},
			},
		},
		
		
	},
	
	
	
	
	proto_lambdaPlatformEmpty:{
		usesSuper:"proto_platformII",
		inheritedPath:"proto_platformII",
	},
	proto_lambdaPlatform1:{
		usesSuper:"proto_platformII",
		inheritedPath:"proto_platformII",
		DockableEquipment:{
			cardinality:{
				value:1,
				prototypesInstantiation:{
					staticInstantiations: [
						{name:"proto_locomotiveLambda",subClass:"Locomotive",spawningZone:{x:0,y:18},preinstanciate:true }
					],
				},
			}
		},
	},
	proto_lambdaPlatformRandom:{
		usesSuper:"proto_platformII",
		inheritedPath:"proto_platformII",
		DockableEquipment:{
			cardinality:{
				value:1,
				prototypesInstantiation:{
					staticInstantiations: [
						{names:[
									{value:"proto_core",subClass:"Producer"},
									{value:"proto_refinery",subClass:"Producer"},
									{value:"proto_factory",subClass:"Producer"},
									{value:"proto_university"},
									{value:"proto_habitationII",subClass:"Habitation"},
							],
							spawningZone:{x:0,y:18},
							preinstanciate:true
						}
					],
				},
			}
		},
		
		
		ResourcesRepository:{
			cardinality:{
				value:1,
				prototypesInstantiation:{
					staticInstantiations:[
						{name:"proto_emptyResourcesRepository",preinstanciate:true }
					]
				},
			},
		},
		
	},
	
};
		

PROTOTYPES_CONFIG.allPrototypes.WheelsSet={

	proto_wheelsSetTiresSmall: {
		
		
		runningLowSpeedThreshold:0,
		runningHighSpeedThreshold:5,
		smokeSpeedThreshold:5,
		
		
		imageConfig:{
			_2D:{
				center:{x:"center",y:"bottom"},
//				sourcePath:"wheelsSetTiresSmall.png",
				sourcePath:{baseName:"wheelsSetTiresSmall.png",size:{w:45,h:25}},

				
				// We don´t actually see the image, it is hidden by the platform :
				// So it´s a blankly transparent image :
				sourcePathDark:"wheelsSetTiresSmall.dark.png",
				// sourcePathBump:"wheelsSetTiresSmall.bump.png",
				
				
				// Animations :
				refreshMillis:200,
				clipSize:{w:45,h:25},
				animations:{
					"default":"wheelsSetTiresSmall.default.png",
					runningLow:"wheelsSetTiresSmall.runningLow.png",
					runningHigh:"wheelsSetTiresSmall.runningHigh.png",
				},
			},
		},
		
		
		sparksImageConfig:{
			_2D:{
				center:{x:"center",y:"bottom"},
				
				// Animations :
				refreshMillis:200,
				clipSize:{w:100,h:50},
				animations:{
					"default":"brakingSparks.default.png",
				},
			},
		},
		
		
		GameSmokePit:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_smokePitLight",
			}
		},
		
		
	},


};
		



PROTOTYPES_CONFIG.allPrototypes.DockableEquipment={
	
	
	proto_abstract_equipment: {

		size:{w:200,h:50},
		
		

		GrandeLigneGauge:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_gaugeEquipment",
			},
		},
		
		
	},
	


			
	proto_university: {
		
		usesSuper:"proto_abstract_equipment",
		
		infos:{
			title:"University",
			description:i18n("unEndroitOùÉtudierEtFaireDeLaRecherche"),
		  actions:{
		  	openResearch:{label:"Research",value:null},
		  }
		},

		
		heatImageSrc:"university.heat.png",
		imageConfig:{
			_2D:{
				zIndex:0,
				center:{x:"center",y:"bottom"},
				
				sourcePath:"university.png",
				sourcePathDark:"university.dark.png",
				sourcePathBump:"university.bump.png",
			},
		},
		
		
		GameThumb:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_universityThumb",
			},
		},
		
	},
	
	

};
		
		

			
			

PROTOTYPES_CONFIG.allPrototypes.Locomotive={
			
	
	proto_abstract_locomotive: {
		
		size:{w:200,h:50},
		
		GrandeLigneGauge:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_gaugeEquipment",
			},
		},
		
		
		
		infos:{
			title:"Locomotive",
			description:i18n("lÉquipementPrincipalQuiPermetLeMouvementDuTrainSiVou"),
		  actions:{
		  	setSpeed:{label:i18n("vitesse"),value:"0=>40",readCheckedStateForValueMethod:"getTrainGoalSpeed",showValues:false},
		  	setSpeedToZero:{label:i18n("stop")},
				setDefaultSpeed:{label:i18n("défaut")},
				setSpeedMax:{label:i18n("max")},
				
		  	setStopAutomatically:{
	  			label:"Stop automatically",
	  			type:"checkbox",
	  			readCheckedStateForValueMethod:"isStoppedAutomatically"
	  		},



		  },
		},
		
		GameThumb:"proto_locomotiveThumb",
		
		heatImageSrc:"locomotive.heat.png",
		imageConfig:{

//			colorLayer:{color:"#00FF00",opacity:.3},

			_2D:{
				
				
				
				zIndex:0,
				center:{x:"center",y:"bottom"},
				sourcePath:"locomotive.png",
				
				sourcePathDark:"locomotive.dark.png",
				sourcePathBump:"locomotive.bump.png",

			},
		},
		
		
		GameSmokePit:{
			cardinality:{
			value:"staticGroups",
			prototypesInstantiation:{
				staticInstantiations: [
					{name:"proto_smokePitDark",spawningZone:{x:-50,y:30},preinstanciate:true }
				],
			},
		 },
		},
		
		

		

	},
	
	
	proto_locomotive: {
		
		// We need for a «subClass» attribute, since its class is not used
		// directly in *attribute* definition of the other objects referencing it,
		// even if we declare the prototype here in the real class directly :
		subClass:"Locomotive",

		usesSuper:"proto_abstract_locomotive",
		
		nativeMaxSpeed:40,
		
		speedsCosts:{
			"0=>10":{
				costs:{
					glykergol:0.1
				},
				timeSeconds:5,
			},
			"10=>20":{
				costs:{
					glykergol:0.2
				},
				timeSeconds:5,
			},
			"20=>30":{
				costs:{
					glykergol:0.3,
				},
				wastes:{ darkWater:0.1 },
				timeSeconds:5,
			},
			"30=>40":{
				costs:{
					glykergol:0.4
					//,water:.2
				},
				wastes:{ darkWater:0.2 },
				timeSeconds:5,
			},
		},
		
		
		

		soundConfig:{
			_2D:{
				center:{x:"center",y:"bottom"},
				sourcePath:"locomotive.running.ogg",
				
				perceptionSizeFactor:2,
				baseVolumePercent:75,
				baseSpeedPercent:100,
				
				reference:"camera",// ("camera"/"firstSelected")
				
			},
		},
		
		
	
		
	},
	
	// We meed this locomotive to appear almost identical, but to be quicker ! :
	proto_locomotiveMater: {
		// We need for a «subClass» attribute, since its class is not used
		// directly in attribute definition
		subClass:"Locomotive",
		usesSuper:"proto_abstract_locomotive",
		nativeMaxSpeed:50,
	},
	
	
	proto_locomotiveLambda: {
		

		// We need for a «subClass» attribute, since its class is not used
		// directly in attribute definition
		subClass:"Locomotive",
		
		usesSuper:"proto_abstract_locomotive",
		
		nativeMaxSpeed:50,
		

		heatImageSrc:"locomotiveLambda.heat.png",
		imageConfig:{
			_2D:{
				
				zIndex:0,
				center:{x:"center",y:"bottom"},
				sourcePath:"locomotiveLambda.png",
				
				sourcePathDark:"locomotiveLambda.dark.png",
				sourcePathBump:"locomotiveLambda.bump.png",

			},
		},
		

// GameSmokePit:{
// cardinality:{
// value:"staticGroups",
// prototypesInstantiation:{
// staticInstantiations: [
// {name:"proto_smokePitDark",spawningZone:{x:-35,y:18},preinstanciate:true }
// ],
// },
// },
// },
		
	},


};
		




PROTOTYPES_CONFIG.allPrototypes.Station={
			
	proto_station:{
		
		
		size:{w:600,h:200},

		
		heatImageSrc:"station.heat.png",
		imageConfig:{
			_2D:{
				
				zIndex:0,
				center:{x:"center",y:"bottom"},
				sourcePath:"station.png",
				sourcePathBack:"station.back.png",
				sourcePathBackDark:"station.back.dark.png",
				sourcePathDark:"station.dark.png",
// sourcePathBump:"station.bump.png",
				
				
				lights:[
					{x:-150,y:200,radius:300,angle:270,arc:80},
					{x:150,y:200,radius:300,angle:270,arc:80}
				],


			},
		},
		

		ResourcesRepository:{
			cardinality:{
				value:1,
				prototypesInstantiation:{
					staticInstantiations:[
						{name:"proto_stationResourcesRepository",preinstanciate:true }
					]
				},
			},
		},

		
//		Extractor:{
//			cardinality:{
//				value:"staticGroups",
//				prototypesInstantiation:{
//					staticInstantiations: [
//						{name:"proto_extractor",spawningZone:{x:322,y:0},preinstanciate:true },
//						{name:"proto_extractor",spawningZone:{x:-322,y:0},preinstanciate:true },
//					],
//				},
//			},
//		},
		
		
	},
	
};


PROTOTYPES_CONFIG.allPrototypes.Habitation={
	
	proto_abstract_habitation: {

		maxCapacity:10,
		
		size:{w:200,h:50},
		
		GrandeLigneGauge:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_gaugeEquipment",
			},
		},
		
		
		restCostsByJobType:{
			transporter:{ costs:{	meals:.05, water:.05, }, wastes:{ darkWater:.01	} },
		},
		
	},			
	
	proto_habitationI: {
		
		usesSuper:"proto_abstract_habitation",
		
		infos:{
			title:"Habitation I",
			description:i18n("ceciEstUneHabitation"),
		  actions:{
		  	setWorkersCount:{type:"number",label:i18n("setWorkersCount"),value:{min:0,max:10,steps:[1,5]},readCheckedStateForValueMethod:"getAffectedWorkersCount",visibilityCondition:"isPending"},
		  },
		},

		heatImageSrc:"habitationI.heat.png",
		imageConfig:{
			_2D:{
				
				zIndex:2,						
				center:{x:"center",y:"bottom"},
				
				sourcePath:"habitationI.png",
				sourcePathDark:"habitationI.dark.png",
				sourcePathBump:"habitationI.bump.png",
				
			},
		},
		
		GameThumb:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_habitationIThumb",
			},
		},
		
		
	},
	
	
	proto_habitationII: {
		
		
		usesSuper:"proto_abstract_habitation",
		
		maxCapacity:15,

		restCostsByJobType:{
			transporter:{ costs:{	meals:.02, water:.02, }, wastes:{ darkWater:.01	} },
		},
		
		
		infos:{
			title:"Habitation II",
			description:i18n("ceciEstUneHabitationDouble"),
		  actions:{
		  },
		},

		heatImageSrc:"habitationII.heat.png",
		imageConfig:{
			_2D:{
				
				zIndex:2,						
				center:{x:"center",y:"bottom"},
				
				sourcePath:"habitationII.png",
				sourcePathDark:"habitationII.dark.png",
				sourcePathBump:"habitationII.bump.png",
				
			},
		},
		
		GameThumb:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_habitationIIThumb",
			},
		},
		
		
	},
	
	
	
};


PROTOTYPES_CONFIG.allPrototypes.Pillar={
	
	proto_pillarLeft:{
		
		size:{w:40,h:45},

		heatImageSrc:"pillarLeft.heat.png",
		imageConfig:{
			_2D:{
				
				zIndex:0,
				center:{x:"center",y:"bottom"},
				sourcePath:"pillarLeft.png",
				sourcePathDark:"pillarLeft.dark.png",
				sourcePathBump:"pillarLeft.bump.png",

			},
		},
	
	},
	
	proto_pillarRight:{
		
		size:{w:40,h:45},

		heatImageSrc:"pillarRight.heat.png",
		imageConfig:{
			_2D:{
				
				zIndex:0,
				center:{x:"center",y:"bottom"},
				sourcePath:"pillarRight.png",
				sourcePathDark:"pillarRight.dark.png",
				sourcePathBump:"pillarRight.bump.png",

			},
		},

	
	},
	
	
};


//PROTOTYPES_CONFIG.allPrototypes.Extractor={
//		
//	proto_extractor:{
//		
//		
//		size:{w:50,h:100},
//
//		imageConfig:{
//			_2D:{
//				
//				zIndex:0,
//				center:{x:"center",y:"bottom"},
//				sourcePath:"extractor.png",
//				sourcePathDark:"extractor.dark.png",
//				sourcePathBump:"extractor.bump.png",
//
//				
//				// Animations :
//				refreshMillis:200,
//				clipSize:{w:100,h:200},
//				animations:{
//					"default":"extractor.running.png",
//				},
//
//			},
//		},
//
//		
//		GameSmokePit:{
//			cardinality:{
//				value:1,
//				prototypesInstantiation:"proto_smokePitLight",
//			}
//		},
//		
//		
//	},
//	
//};




PROTOTYPES_CONFIG.allPrototypes.Doodle={
	
//proto_lamp1: {
//		
//size:{w:10,h:100},
//
//imageConfig:{
//_2D:{
//center:{x:"center",y:"bottom"},
//sourcePath:"proto_lamp1.png",
//
//lights:[{x:0,y:100,radius:20,angle:270,arc:60}],
//},
//},
//		
//		
//},
	
	proto_rock1: {
		
		size:{w:10,h:10},

		imageConfig:{
			_2D:{
				center:{x:"center",y:"bottom"},
				sourcePaths:["rock1.png","rock2.png","rock3.png","rock4.png","rock5.png",],
			},
		},
		
		
	},
	
	
	
	
};





PROTOTYPES_CONFIG.allPrototypes.People={

	proto_normalPeople: {
		
		jobType:"transporter",
		maxLoad:2,
		// reactionTimeMillis:1000,
		speed:1,
		
		size:{w:10,h:20},

		
		restTimeMillisByJobType:{
			transporter:50000 
		},
		restTimeVariabilityMillisByJobType:{
			transporter:6000 
		},
				
		
		
		imageConfig:{
			_2D:{
				center:{x:"center",y:"bottom"},
				
				refreshMillis:200,
				clipSize:{w:10,h:20},
				idles:{
					triggerAfterMillis:10000,
					sourcesPaths:[
						"idle1.png", "idle2.png", "idle3.png"
					],
				},
				horizontal:{
					correlateYZ:true,
					walkLeft:"walkL.png",
					walkRight:"walkR.png",
					standbyLeft:"standL.png",
					standbyRight:"standR.png",
					
					carryLeft:"carryL.png",
					carryRight:"carryR.png",
				}
			}
		},
		
		GrandeLigneGauge:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_gaugePeople",
			},
		},
		
		
		GameThumb:{
			
			right:40,
			bottom:165,
			sizeFactor:1,
			scaleFactor:.4,
			
			offsetsUI:{y:-10},

			thumbSrc:"people.thumb.png",
		},
		
		
		GameAutomaton:{
			cardinality:{
//			value:1,
//			prototypesInstantiation:"proto_peopleTransporterAIProcedure",
				value:"staticGroups",
				prototypesInstantiation:{
					staticInstantiations:[
						{name:"proto_peopleTransporterAIProcedure",subClass:"GameAutomatonTransporter"/*,preinstanciate:true*/},
						{name:"proto_peopleBuilderAIProcedure",subClass:"GameAutomatonBuilder"/*,preinstanciate:true*/},
					]				
				},
			},
		},
		
		// NO ! : NOT THE RIGHT SYNTAX...
		// GameAutomaton:["proto_peopleTransporterAIProcedure","proto_peopleBuilderAIProcedure"],
		
		
		
		
	},


};



