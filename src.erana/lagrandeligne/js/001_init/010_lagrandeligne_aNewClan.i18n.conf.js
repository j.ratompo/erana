// DEPENDENCY:init.js




// ===============================================================================
// CONFIG
// ===============================================================================

i18n.declare({
	
	/* DBG */
	"commencer":{"capitalize":true,"fr":"commencer","en":"start"},
	"prochainArrêt":{"capitalize":true,"append":": ","fr":"prochain arrêt","en":"next stop"},
	"propulséParEranaBrSpanStyleFontSize7emUnMoteurOuv":{"fr":"Propulsé par erana<br><span style=´font-size:.7em´>Un moteur ouvert de jeu (licence HGPL)</span>","en":"Powered by erana<br><span style=´font-size:.7em´>Open-source javascript game engine (HGPL license)</span>"},
	"leStudioAlqemiaPrésente":{"fr":"Le studio Alqemia présente","en":"Alqemia studio presents"},
	"soutenir":{"capitalize":true,"fr":"soutenir!","en":"support!"},
	"attentionCeciEstUneVersionAlphaBrDesBugsInacceptable":
	{"fr":"ATTENTION ! Ceci est une version démo alpha <strong>NON OPTIMISÉE</strong> (±15 min de jeu)<br/>" +
			"Des bugs inacceptables sont encore présents<br/>" +
			"et des fonctionnalités indispensables sont encore manquantes." +
			"<br/><br/><br/><br/><br/><br/><br/><br/>" +
			"Nous nous en excusons et nous vous remercions de votre patience !<br/>" +
			"Bonne aventure !<br/>" +
			"<br/><br/><br/>" +
			"<small>(icône : Bug par Nociconist du Noun Project)</small>",
		"en":"CAUTION ! This is a <strong>NOT OPTIMIZED</strong> demo alpha version (±15 min of play)<br/>" +
			"Unacceptable bugs are still present<br/>" +
			"and necessary features are not here yet." +
			"<br/><br/><br/><br/><br/><br/><br/><br/>" +
			"We apologize for this and we thank you for your patience!<br/>" +
			"Good adventure !<br/>" +
			"<br/><br/><br/>" +
			"<small>(icon : Bug by Nociconist from the Noun Project)</small>"},
	"ok":{"fr":"OK","en":"OK"},
	"unDispositifDÉcouteEstConseilléBrPourUneMeilleureExpé":{
		"fr":"Un dispositif d´écoute est conseillé<br/>" +
				"pour une meilleure expérience" +
				"<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>" +
				"<small>(icône : Headset par mahdalenyy du Noun Project)</small>",
		"en":"Audio set advised <br/>" +
				"for a better experience" +
				"<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>" +
				"<small>(icon : Headset by mahdalenyy from the Noun Project)</small>"},
	"continuer":{"fr":"Continuer","en":"Continue"},
	"crédits":{"fr":"Crédits","en":"Credits"},
	"quitter":{"fr":"Quitter","en":"Exit"},
	"retourALAccueil":{"fr":"Retour à l´accueil", "en":"Back to Home",},
	"menu":{"fr":"Menu","en":"Menu"},
	"passer":{"fr":"Passer","en":"Skip"},
	"reprendre":{"fr":"Reprendre","en":"Resume"},
	"sauverEtRevenirÀLAccueil":{"fr":"Sauver et revenir à l´accueil","en":"Home"},
	
	// =================================================================================================================
	
	"programmationJeremieR":{"fr":"Programmation : Jérémie R.","en":"Programming : Jeremie R."},
	"graphiquesJérémieR":{"fr":"Graphiques : Jérémie R.","en":"Graphics : Jeremie R."},
	"écritureJeremieR":{"fr":"Écriture : Jeremie R.","en":"Writing : Jeremie R."},
	"sonsJeremieR":{"fr":"Sons : Jérémie R.","en":"Sounds : Jeremie R."},
	"communicationJeremieR":{"fr":"Communication : Jérémie R.","en":"PR : Jeremie R."},
	"musiqueKaiEngel":{"fr":"Musique : Kai Engel","en":"Music : Kai Engel"},
	"remerciementsSupportMoralDominiqueB":{"fr":"Remerciements (support moral): Dominique B.","en":"Thanks to (moral support): Dominique B."},
	"remerciementsMusiqueÉcoutéeKaiEngelLindseyStirlingOh":{"fr":"Remerciements (musique écoutée): Kai Engel, Lindsey Stirling, Oh Pony Boy, ATB","en":"Thanks to (listened music) : Kai Engel, Lindsey Stirling, Oh Pony Boy, ATB"},
	"remerciementsPodcastsÉcoutésTalesOfPiFranceInterFib":{"fr":"Remerciements (podcasts écoutés): Tales of Pi, France Inter, Fibre Tigre, Games of Roles, Globtopus","en":"Thanks to (listened podcasts) : Tales of Pi, France Inter, Fibre Tigre, Games of Roles, Globtopus"},
	
	"cetÉquipementEstUneSimplePlateforme":{"fr":"Cette structure est une simple plateforme.","en":"This structure is a basic platform."},
	"appelerUneErgostasio":{"fr":"Appeler une ergostasio","en":"Call ergostasio"},
	"unEndroitOùÉtudierEtFaireDeLaRecherche":{"fr":"Un endroit où étudier et faire de la recherche.","en":"A place to study and do research."},
	"lÉquipementPrincipalQuiPermetLeMouvementDuTrainSiVou":{"fr":"L´équipement principal, qui permet le mouvement du train. Si vous ne le faites pas prendre assez de vitesse, le train brûlera sous les rayons solaires, si vous allez trop lentement, le froid de la nuit vous rattrapera.","en":"The main equipment, allowing to move the whole clan train. If you set it to  go too fast, your train will be scorched by sunlight, if you go too slow, the ice of the night will catch up your train."},
	"vitesse":{"fr":"Vitesse","en":"Speed"},
	"stop":{"fr":"Stop","en":"Stop"},
	"défaut":{"fr":"Défaut","en":"Default"},
	"max":{"fr":"Max","en":"Max"},
	"ceciEstUneHabitation":{"fr":"Ceci est une habitation.","en":"This is an habitation."},
	"ceciEstUneHabitationDouble":{"fr":"Ceci est une habitation double.","en":"This is a double habitation."},
	"permetDeConstruireDAutresÉquipementsMaisAussiDesPlatef":{"fr":"Permet de construire d´autres équipements, mais aussi des plateformes pour agrandir votre train. Il peut également réparer et améliorer des équipements existants. Vous devez le bouger à l´emplacement voulu pour qu´il puisse effectuer son travail.","en":"Allows to build other equipments, but also rolling platforms to allow your train to grow. It can also repair and enhance existing equipments. You must first move it to the right location before it can do its work."},
	"annuler":{"fr":"Annuler","en":"Cancel"},
	"réparer":{"fr":"Réparer","en":"Repair"},
	"démanteler":{"fr":"Démanteler","en":"Dismantle"},
	"construireBasiques":{"fr":"Construire basiques","en":"Build basics"},
	"plateforme":{"fr":"Plateforme","en":"Platform"},
	"cetÉquipementSertDeSupportAuxAutresÉquipementsEtPermet":{"fr":"Cet équipement sert de support aux autres équipements, et permet d´agrandir le train.","en":"This equipment is on which to build and support equipments, and expand the train."},
	"ergostasio":{"fr":"Ergostasio","en":"Ergostasio"},
	"lÉquipementPrincipalDuTrainIlPermetDeLAgrandirDeCon":{"fr":"L´équipement principal du train, il permet de l´agrandir, de construire d´autres équipements et de faire des réparations.","en":"The main equipment to build and repair the other equipments."},
	"habitation":{"fr":"Habitation","en":"Habitation"},
	"uneHabitationPourHébergerLaPopulation":{"fr":"Une habitation pour héberger la population.","en":"An habitation to house the people."},
	"habitationDouble":{"fr":"Habitation double","en":"Double habitation"},
	"uneHabitationPlusGrandePourHébergerLaPopulation":{"fr":"Une habitation plus grande pour héberger la population.","en":"A bigger habitation to house the people."},
	"construireProductions":{"fr":"Construire productions","en":"Build productions"},
	"cuisine":{"fr":"Cuisine","en":"Cooker"},
	"permetDeCuisinerDesPlatsPréparés":{"fr":"Permet de cuisiner des plats préparés.","en":"Produces cooked meals."},
	"uneInstallationQuiPermetDeCréerDesRessourcesPlusÉlaboré":{"fr":"Une installation qui permet de créer des ressources plus élaborées à partir de ressources de base.","en":"An installation to create more elaborate resources from basic ones."},
	"forge":{"fr":"Forge","en":"Forge"},
	"forgeDesMétauxEtDesAlliages":{"fr":"Forge des métaux et des alliages.","en":"Forges metals and alloys."},
	"catalyseur":{"fr":"Catalyseur","en":"Catalyst"},
	"synthétiseDesSubstancesIndustrielles":{"fr":"Synthétise des substances industrielles.","en":"Synthetizes industrial substances."},
	"retraiteur":{"fr":"Retraiteur","en":"Retreater"},
	"permetDeRecyclerLEauPolluéeEnEauÀNouveauPropre":{"fr":"Permet de recycler l´eau polluée en eau à nouveau propre.","en":"Allows to transform polluted water back into clear water."},
	"usine":{"fr":"Usine","en":"Factory"},
	"uneUsinePourAssemblerDesPiècesEtDesBiensDIngénierie":{"fr":"Une usine pour assembler des pièces et des biens d´ingénierie.","en":"A factory to assemble advanced parts and goods."},
	"cœur":{"fr":"Cœur","en":"Core"},
	"cetÉquipementFournitDeLÉnergie":{"fr":"Cet équipement fournit de l´énergie.","en":"This equipment provides glykergol."},
	"construireSurÉquipement":{"fr":"Construire sur-équipement","en":"Build over-equipments"},
	"condensateurDEau":{"fr":"Condensateur d´eau","en":"Waterer"},
	"permetDeCollecterDeLEauAtmosphérique":{"fr":"Permet de collecter de l´eau atmosphérique.","en":"Collects atmospheric water."},
	"collecteurSolaire":{"fr":"Collecteur solaire","en":"Solarer"},
	"collecteLÉnergieSolaire":{"fr":"Collecte l´énergie solaire.","en":"Harvests solar glykergol."},
	"raffinerie":{"fr":"Raffinerie","en":"Refinery"},
	"raffineLesMineraisPourUtilisationIndustrielle":{"fr":"Raffine les minerais pour utilisation industrielle.","en":"Refines ores for industrial quality-usage."},
	"produire":{"fr":"Produire ","en":"Produce "},
	"cuisineLaNourriture":{"fr":"Cuisine la nourriture.","en":"Cooks food."},
	"forgeDesRessourcesÉlaborées":{"fr":"Forge des ressources élaborées.","en":"Forges elaborated resources."},
	"synthétiseDesRessourcesÉlaborées":{"fr":"Synthétise des ressources élaborées.","en":"Synthetizes elaborated resources."},
	"fabriqueDesPiècesÉlaborées":{"fr":"Fabrique des pièces élaborées.","en":"Creates elaborated technical parts."},
	"recycleLEauSaleEnEauPure":{"fr":"Recycle l´eau sale en eau pure.","en":"Recycles soiled water into clean water."},
	"lÉquipementLePlusImportantQuiPermetDeFournirEnÉnergie":{"fr":"L´équipement le plus important, qui permet de fournir en énergie le train complet. Attention à ne pas le pousser à la surchauffe, ce qui pourrait résulter en une explosion catastrophique.","en":"The most important equipment, allowing to power all the clan train. Be carefull to not overheat it, or else it can result in a catastrophical failure."},
	"permetDeCondenserDeLEauÀPartirDeLaRosée":{"fr":"Permet de condenser de l´eau à partir de la rosée.","en":"Condensates and gather water from dew."},
	"collecteDeLÉnergieSolaire":{"fr":"Collecte de l´énergie solaire.","en":"Harvests solar energy."},
	"survie":{"fr":"Survie","en":"Survival"},
	"concertadesAmountTarget":{"fr":"Concertades : %amount%(>%target%)","en":"Concertads : %amount%(>%target%)"},
	"premierHébergement":{"fr":"Premier hébergement","en":"First housing"},
	"construireDesHabitationsAmountTarget":{"fr":"Construire des habitations (%amount%/%target%)","en":"Build habitations (%amount%/%target%)"},
	"ressources":{"fr":"Ressources","en":"Resources"},
	"récupérerLeMétalAmount":{"fr":"Récupérer le métal (%amount%)","en":"Retrieve metal (%amount%)"},
	"construireUneCuisineAmount":{"fr":"Construire une cuisine (%amount%)","en":"Build a cooker (%amount%)"},
	"construireUnCollecteurSolaireAmount":{"fr":"Construire un collecteur solaire (%amount%)","en":"Build a solarer (%amount%)"},
	"construireUnCondensateurDEauAmount":{"fr":"Construire un condensateur d´eau (%amount%)","en":"Build a waterer (%amount%)"},
	"attentionLÉquipementVaBientôtCesserDeFonctionner":{"fr":"Attention, l´équipement va bientôt cesser de fonctionner.","en":"Caution, an equipment will malfunction."},
	"unÉquipementVientDeBriser":{"fr":"Un équipement vient de briser.","en":"An equipment has broken down."},
	"unÉquipementEstDétruit":{"fr":"Un équipement est détruit.","en":"An equipment is destroyed."},
	"RDT":{"fr":"RDT","en":"YLD"},
	"uneConcertadeNAPuSeReposerNullePart":{"fr":"Une concertade n´a pu se reposer nulle part.","en":"A concertad has not place to rest.",},
	"uneConcertadeNAPasPuSeRestaurer":{"fr":"Une concertade n´a pas pu se restaurer.","en":"A concertad could not restore.",},
	"uneConcertadeEstDécédée":{"fr":"Une concertade est décédée.","en":"A concertad is dead.",},
	"uneErgostasioEstDéjàACetEndroit":{"fr":"Une ergostasio est déjà à cet endroit.","en":"An ergostasio is already at this location.",},
	"aucuneErgostasioDeLibrePourLeMoment":{"fr":"Aucune ergostasio de libre pour le moment.","en":"No currently available ergostasio.",},
	"uneErgostasioEstDisponibleMaisNePeutPasAtteindreCettePlateforme":{"fr":"Une ergostasio est disponible mais ne peut pas atteindre cette plateforme.","en":"Available ergostasio cannot move till this platform."},
	"impossibleDeFonctionnerSansRessources":{"fr":"Impossible de fonctionner sans ressources. Arrêt.","en":"Cannot function without resources. Stopping",},
	"approcheAutomatique":{"fr":"Approche automatique.","en":"Automatic approach."},
	"impossibleDeSeDéplacerEnFonctionnement":{"fr":"Impossible de se déplacer en fonctionnement.","en":"Cannot move while busy."},
	"uneErgostasioEstDéjàStationnéeACetEndroit":{"fr":"Une ergostasio est déjà stationnée à cet endroit.","en":"An ergostasio is already parked at this location.",},
	"impossibleDeSeDéplacerPlusLoin":{"fr":"Impossible de se déplacer plus loin.","en":"Cannot move further."},
	"impossibleDeRéparer":{"fr":"Impossible de réparer. La condition est maximale.","en":"Cannot repair. Maximum condition.",},
	"aucunÉquipementARéparer":{"fr":"Aucun équipement à réparer.","en":"No equipment to repair.",},
	"aucunBesoinDeRéparer":{"fr":"Aucun besoin de réparer. En pause.","en":"No need to repair. Suspending.",},
	"aucunÉquipementÀDémanteler":{"fr":"Aucun équipement à démanteler.","en":"No equipment to dismantle.",},
	"sommesNousSûres":{"fr":"Sommes-nous sûres ?","en":"Are we sure ?",},
	"impossibleDeDémantelerCeSurÉquipement":{"fr":"Impossible de démanteler ce sur-équipement.","en":"Cannot dismantle this over-equipment.",},
	"impossibleDeDémantelerCetÉquipement":{"fr":"Impossible de démanteler cet équipement.","en":"Cannot dismantle this equipment.",},
	"unProcessusEstDéjàEnCours":{"fr":"Un processus est déjà en cours.","en":"A job is already in progress.",},
	"impossibleDeConstruireEnDéplacement":{"fr":"Impossible de construire en déplacement.","en":"Cannot build while moving.",},
	"coûts":{"fr":"Coûts","en":"Costs",},
	"construireUnÉquipement":{"fr":"Construire un équipement","en":"Build equipment",},
	"cetÉquipementDoitÊtreConstruitSurUnePlateforme":{"fr":"Cet équipement doit être construit sur une plateforme.","en":"This equipment must be constructed on a platform.",},
	"impossibleDeConstruirePlusDePlateformes":{"fr":"Impossible de construire plus de plateformes.","en":"Cannot build more platforms.",},
	"unePlateformeExisteDéjàIci":{"fr":"Une plateforme existe déjà ici.","en":"A platform already exists at this location.",},
	"impossibleDeConstruirePlusDErgostasiosÀCetEmplacement":{"fr":"Impossible de construire plus d´ergostasios à cet emplacement.","en":"Cannot build more ergostasios at this location.",},
	"cettePlateformeADéjàUnÉquipement":{"fr":"Cette plateforme a déjà un équipement.","en":"Platform already has an equipment."},
	"cettePlateformeADéjàUnSurÉquipement":{"fr":"Cette plateforme a déjà un sur-équipement.","en":"Platform already has an over-equipment."},
	"pasAssezDeRessourcesEnPause":{"fr":"Pas assez de ressources. En pause.","en":"Not enough resources. Suspending.",},
	"ressourcesUtilisablesReprise":{"fr":"Ressources utilisables. Reprise.","en":"Resources available. Resuming.",},
	"attentionUneConcertadeVaÊtreÉpuisée":{"fr":"Attention, une concertade va être épuisée.","en":"Caution, a concertad will be exhausted"},
	"consommations":{"fr":"Consommations","en":"Consumptions",},
	"impossibleDeConstruireUnSurÉquipementSansUnÉquipementPrésent":{"fr":"Impossible de construire un sur-équipement sans un équipement présent.","en":"Cannot bould an over-equipment without equipment present.",},
	"uneConcertadeEstÉpuisée":{"fr":"Une concertade est épuisée.","en":"A concertad is exhausted.",},
	
	"buildAlreadyStarted":{"fr":"Une construction est déjà en cours.","en":"A construction has been already initiated.",},
	"buildAlreadyInstalled":{"fr":"Un équipement est déjà installé.","en":"A building is already installed here.",},
	"overAlreadyInstalled":{"fr":"Un sur-équipement est déjà installé.","en":"An over-building is already installed here.",},
//	"initiateBuild":{"fr":"Commencer une construction","en":"Initiate construction",},
	
	"buildPlatform":{"fr":"Construire une plateforme","en":"Build platform",},
// TODO : Develop... :	"contribuer":{"fr":"Contribuer","en":"Contribute",},
	"setWorkersCount":{"fr":"Nombre de travailleuses:","en":"Workers count:",},
	"constructionPending":{"fr":"<b>CONSTRUCTION EN COURS</b>","en":"<b>CONSTRUCTION PENDING</b>"},
	"aucuneTravailleuse":{"fr":"Aucune travailleuse sur cette tâche","en":"No workers on this task",},
	
	
});


//i18n.declareSplitted({
//"fr":{
//"commencer":"Commencer",
//},
//"en":{
//"commencer":"Start",
//},
//});

