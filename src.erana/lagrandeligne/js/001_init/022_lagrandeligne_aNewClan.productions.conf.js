// DEPENDENCY:init.js




// ===============================================================================
// 																			CONFIG
// ===============================================================================



// ===============================================================================
// 																			MODEL
// ===============================================================================




// Prototype can define as a well a group of things as particular individuals:only by not setting a random variance on them.

// ------------------ Production equipments objects ------------------		
		
PROTOTYPES_CONFIG.allPrototypes.Ergostasio={


	
	proto_abstract_ergostasio: {
		
		size:{w:200,h:50},
		
		clickableSize:{w:140,h:50},
		
		GrandeLigneGauge:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_gaugeEquipmentStrong",
			},
		},
		
		
		
		Pillar:{
			cardinality:{
				value:"staticGroups",
				prototypesInstantiation:{
					staticInstantiations: [
						{name:"proto_pillarLeft",spawningZone:{x:-80,y:0},preinstanciate:true },
						{name:"proto_pillarRight",spawningZone:{x:80,y:0},preinstanciate:true },
					],
				},
			},
		},
		
	},			
	
	proto_ergostasio: {
		
		usesSuper:"proto_abstract_ergostasio",
		pillarMargin:22,
		
		
		infos:{
			title:"Ergostasio",
			description:i18n("permetDeConstruireDAutresÉquipementsMaisAussiDesPlatef"),
		  
			actions:{
		  	startMovingLeft:{label:"⇦",value:5},
		  	startMovingRight:{label:"⇨",value:5},
		  	cancelJob:{label:i18n("annuler")},
		  	// pauseJob:{label:"Suspend current job"},
		  	// resumeJob:{label:"Resume"},
		  	repair:{label:i18n("réparer"),value:10000},
		  	dismantle:{label:i18n("démanteler"),value:10000},
		  	
		  	
		  	
		  	buildPlatform:{label:i18n("buildPlatform"),value:{
		  		
				  		proto_platformI:{
				  			type:"platform",
				  			
				  			iconSrc:"platformI.png",
				  			title:i18n("plateforme"),
				  			description:i18n("cetÉquipementSertDeSupportAuxAutresÉquipementsEtPermet"),
				  			
				  			timeToBuild:(IS_DEBUG?2:30),

				  			costs:{
				  				glykergol:1,
				  				metal:3,
				  			},
				  		},
						},
					},
					
	  			
	  			contributeToCurrentJob:{label:i18n("contribuer"),value:{efficiencyPercentage:110}},
	  	
//		  	buildBasics:{label:i18n("construireBasiques"),value:{
//		  		
//				  		proto_platformI:{
//				  			type:"platform",
//				  			
//				  			iconSrc:"platformI.png",
//				  			title:i18n("plateforme"),
//				  			description:i18n("cetÉquipementSertDeSupportAuxAutresÉquipementsEtPermet"),
//				  			
//				  			timeToBuild:(IS_DEBUG?2:30),
//
//				  			costs:{
//				  				glykergol:1,
//				  				metal:3,
//				  			},
//				  		},
//				  		
//				  		// An ergostasio cannot build another ergostasio :
//				  		
//				  		proto_ergostasio:{
//				  			type:"ergostasio",
//
//				  			subClass:"Ergostasio",// Actually, not useful
//				  			
//				  			iconSrc:"ergostasio.png",
//				  			title:i18n("ergostasio"),
//				  			description:i18n("lÉquipementPrincipalDuTrainIlPermetDeLAgrandirDeCon"),
//
//				  			inactive:true,
////				  			timeToBuild:(IS_DEBUG?2:60),
////
////				  			costs:{
////				  				glykergol:1,
////				  				metal:3,
////				  				alloys:2,plastics:1,
////				  				designed:1,
////				  			},
//				  		},
//				  		
//				  		
//				  		proto_habitationI:{
//				  			type:"dockableEquipment",
//				  			
//				  			subClass:"Habitation",
//				  			
//				  			newBuildingPosition:{x:0,y:18},					  			
//				  			iconSrc:"habitationI.png",
//				  			title:i18n("habitation"),
//				  			description:i18n("uneHabitationPourHébergerLaPopulation"),
//
//				  			timeToBuild:(IS_DEBUG?2:30),
//
//				  			
//				  			costs:{
//				  				glykergol:1,
//				  				metal:4,
//				  			},
//				  			
//				  		},
//				  		
//				  		proto_habitationII:{
//				  			type:"dockableEquipment",
//
//				  			subClass:"Habitation",
//				  			
//				  			newBuildingPosition:{x:0,y:18},				  			
//				  			iconSrc:"habitationII.png",
//				  			title:i18n("habitationDouble"),
//				  			description:i18n("uneHabitationPlusGrandePourHébergerLaPopulation"),
//
//				  			timeToBuild:(IS_DEBUG?2:40),
//
//				  			costs:{
//				  				glykergol:2,water:1,
//				  				metal:6,
//				  				alloys:1, plastics:1,
//				  				manufactured:1,
//				  			},
//				  			
//				  		},
//				  		
//				  		
//	  				},
//			  	},

			  	
			  	
			  	
//			  	buildProductions:{label:i18n("construireProductions"),value:{
//			  		
//			  		
//						// PRIMARY RESOURCES (total : 2+6)
//
//			  		proto_cooker:{
//			  			type:"dockableEquipment",
//
//			  			subClass:"Producer",
//			  			newBuildingPosition:{x:0,y:18},
//			  			iconSrc:"cooker.png",
//			  			title:i18n("cuisine"),
//			  			description:i18n("permetDeCuisinerDesPlatsPréparés"),
//
//			  			timeToBuild:(IS_DEBUG?2:30),
//
//			  			costs:{
//			  				glykergol:1,
//								sand:2, metal:2, coal:2,
//			  			},
//			  			
//			  		},
//			  		
//			  		
//						// SECONDARY RESOURCES (total : 2+10)
//
//			  		proto_refinery:{
//			  			type:"dockableEquipment",
//
//			  			subClass:"Producer",
//			  			newBuildingPosition:{x:0,y:18},
//			  			iconSrc:"refinery.png",
//			  			title:i18n("raffinerie"),
//			  			description:i18n("uneInstallationQuiPermetDeCréerDesRessourcesPlusÉlaboré"),
//
//			  			timeToBuild:(IS_DEBUG?2:40),
//
//			  			costs:{
//			  				glykergol:1,
//								sand:3, metal:5, coal:2,
//			  			},
//			  			
//			  		},
//			  		
//
//			  		proto_forge:{
//			  			type:"dockableEquipment",
//
//			  			subClass:"Producer",
//			  			newBuildingPosition:{x:0,y:18},
//			  			iconSrc:"forge.png",
//			  			title:i18n("forge"),
//			  			description:i18n("forgeDesMétauxEtDesAlliages"),
//
//			  			timeToBuild:(IS_DEBUG?2:50),
//
//			  			costs:{
//			  				glykergol:2,water:1,
//								sand:3, metal:5, coal:2,
//			  			},
//			  			
//			  		},
//			  		
//			  		
//			  		proto_catalyst:{
//			  			type:"dockableEquipment",
//
//			  			subClass:"Producer",
//			  			newBuildingPosition:{x:0,y:18},
//			  			iconSrc:"catalyst.png",
//			  			title:i18n("catalyseur"),
//			  			description:i18n("synthétiseDesSubstancesIndustrielles"),
//
//			  			timeToBuild:(IS_DEBUG?2:50),
//
//			  			costs:{
//			  				glykergol:2,
//								sand:4, metal:4, coal:2,
//			  			},
//			  			
//			  		},
//			  		
//			  		
//			  		proto_retreater: {
//			  			type:"dockableEquipment",
//
//			  			subClass:"Producer",
//			  			newBuildingPosition:{x:0,y:18},
//			  			iconSrc:"retreater.png",
//			  			title:i18n("retraiteur"),
//			  			description:i18n("permetDeRecyclerLEauPolluéeEnEauÀNouveauPropre"),
//
//			  			timeToBuild:(IS_DEBUG?2:80),
//
//			  			costs:{
//			  				glykergol:2,
//			  				sand:3,
//			  				metal:7,
//			  				
//			  			},
//			  		},
//			  		
//			  		
//			  		
//						// TERTIARY RESOURCES (total : 4+16)
//
//			  		proto_factory: {
//			  			type:"dockableEquipment",
//
//			  			subClass:"Producer",
//			  			newBuildingPosition:{x:0,y:18},
//			  			iconSrc:"factory.png",
//			  			title:i18n("usine"),
//			  			description:i18n("uneUsinePourAssemblerDesPiècesEtDesBiensDIngénierie"),
//
//			  			timeToBuild:(IS_DEBUG?2:70),
//
//			  			costs:{
//			  				glykergol:2, water:2,
//			  				sand:3,
//			  				metal:2,
//			  				alloys:3, plastics:4, quartzs:4,
//			  				
//			  			},
//			  		},
//			  		
//			  		
//
//						// LEVEL IV BUILDINGS (total : 4+20)
//			  		
//			  		proto_core:{
//		  				type:"dockableEquipment",
//
//		  				subClass:"Producer",
//			  			newBuildingPosition:{x:0,y:18},
//			  			iconSrc:"core.png",
//			  			title:i18n("cœur"),
//			  			description:i18n("cetÉquipementFournitDeLÉnergie"),
//			  			
////			  		inactive:true,
//
//			  			timeToBuild:(IS_DEBUG?2:80),
//
//			  			costs:{
//			  				glykergol:3,
//			  				metal:4,
//			  				quartzs:4, alloys:6, plastics:4,
//			  				manufactured:2, designed:2,
//			  			},
//			  		},
//			  		
//		  		}
//		  	},
			  	
			  	
		  	
//		  	buildOverEquipments:{label:i18n("construireSurÉquipement"),value:{
//	  		
//		  		proto_waterer:{
//		  			type:"overEquipment",
//		  			
//		  			subClass:"Producer",
//		  			newBuildingPosition:{x:0,y:18},					  			
//		  			iconSrc:"waterer.png",
//		  			title:i18n("condensateurDEau"),
//		  			description:i18n("permetDeCollecterDeLEauAtmosphérique"),
//		  			
//		  			timeToBuild:(IS_DEBUG?2:40),
//
//		  			costs:{
//		  				metal:2, coal:1,
//		  			},
//		  		},
//	  		
//		  		proto_solarer:{
//		  			type:"overEquipment",
//		  			
//		  			subClass:"Producer",
//		  			newBuildingPosition:{x:0,y:18},					  			
//		  			iconSrc:"solarer.png",
//		  			title:i18n("collecteurSolaire"),
//		  			description:i18n("collecteLÉnergieSolaire"),
//
//		  			timeToBuild:(IS_DEBUG?2:40),
//
//		  			costs:{
//		  				metal:2, sand:1,
//		  			},
//		  		},
//		  		
//		  		proto_plasmaTorch:{
//		  			type:"overEquipment",
//		  			
//		  			isAllowedOnFirstPlatformOnly:true,
//		  			
//		  			subClass:"PlasmaTorch",
//		  			
//		  			newBuildingPosition:{x:0,y:9},					  			
//		  			iconSrc:"plasmaTorch.png",
//		  			title:i18n({"fr":"Torche plasma", "en":"Plasma torch"}),
//		  			description:i18n({"fr":"Permet de vaporiser les obstacles sur le trajet de la kinésipole.","en":"Allows to vaporize obstacles on the kinesipolis way."}),
//
//		  			timeToBuild:(IS_DEBUG?2:40),
//
//		  			costs:{
//		  				metal:2, sand:1,
//		  			},
//		  		},
//		  		
//				 },
//		  	},

		  	
		  }
		},

		heatImageSrc:"ergostasio.heat.png",
		imageConfig:{
			_2D:{
				
				zIndex:2,						
				center:{x:"center",y:"bottom"},
				
				sourcePath:"ergostasio.png",
				sourcePathDark:"ergostasio.dark.png",
				sourcePathBump:"ergostasio.bump.png",
				
				// Animations :
				refreshMillis:200,
				clipSize:{w:200,h:50},
				animations:{
					build:"ergostasio.build.png",
				},
					
			},
		},
		
		
		soundConfig:{
			_2D:{
				center:{x:"center",y:"bottom"},
				sourcePath:"ergostasio.building.ogg",
				
				perceptionSizeFactor:2,
				baseVolumePercent:5,
				baseSpeedPercent:100,
				
				reference:"camera",// ("camera"/"firstSelected")
				
			},
		},
		
		
		GameThumb:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_ergostasioThumb",
			},
		},
		
		
	},

	
};





PROTOTYPES_CONFIG.allPrototypes.Producer={
		
		

	
	proto_abstract_producer: {
		
		size:{w:200,h:50},
		
		GrandeLigneGauge:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_gaugeEquipment",
			},
		},
		
	},
	
	
	proto_refinery: {
		
		
		// We need for a «subClass» attribute, since its class is not used directly in attribute definition
		subClass:"Producer",
		
		usesSuper:"proto_abstract_producer",
		
		productions:{
			quartzs:{
				costs:{
					glykergol:1,water:1,
					sand:1,
				},
				wastes:{ darkWater:1,mineralWaste:1, },
				timeSeconds:3,
				value:1,
			},
		},
		
		
		infos:{
			title:i18n("raffinerie"),
			description:i18n("raffineLesMineraisPourUtilisationIndustrielle"),
			actions:{
			  	activateProduction:{
			  		label:i18n("produire")+"%value%",
			  		values:["quartzs"],
			  		type:"checkbox",
			  		readCheckedStateForValueMethod:"isProductionActive"},
			},
		},
		
		heatImageSrc:"refinery.heat.png",
		imageConfig:{
			_2D:{
				
				zIndex:2,						
				center:{x:"center",y:"bottom"},
				
				sourcePath:"refinery.png",
				sourcePathDark:"refinery.dark.png",
				sourcePathBump:"refinery.bump.png",
				
			},
		},
		
		GameThumb:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_refineryThumb",
			},
		},
		
		
		GameSmokePit:{
			cardinality:{
			value:"staticGroups",
			prototypesInstantiation:{
					 staticInstantiations: [
						 {name:"proto_smokePitDark",spawningZone:{x:-20,y:18},preinstanciate:true }
					 ],
				 },
		  },
		},
		
		
	},
	
	
	

	proto_cooker: {
		
		// We need for a «subClass» attribute, since its class is not used directly in attribute definition
		subClass:"Producer",
		
		usesSuper:"proto_abstract_producer",
		
		productions:{
			meals:{
				costs:{
					glykergol:1,water:2,
					food:2,
				},
				wastes:{ darkWater:1 },
				timeSeconds:2,
				value:1,
			},
			food:{
				costs:{
					glykergol:3,water:5,
				},
				wastes:{ darkWater:1 },
				timeSeconds:2,
				value:1,
			},
			
		},
		
		infos:{
			title:"Cooker",
			description:i18n("cuisineLaNourriture"),
		  actions:{
		  	activateProduction:{
		  		label:i18n("produire")+"%value%",
		  		values:["meals","food"],
		  		type:"checkbox",
		  		readCheckedStateForValueMethod:"isProductionActive"},
		  },
		},
		
		heatImageSrc:"cooker.heat.png",
		imageConfig:{
			_2D:{
				
				zIndex:2,						
				center:{x:"center",y:"bottom"},
				
				sourcePath:"cooker.png",
				sourcePathDark:"cooker.dark.png",
				sourcePathBump:"cooker.bump.png",

				
			},
		},
		
		
		GameThumb:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_cookerThumb",
			},
		},
		
		
		
		GameSmokePit:{
			cardinality:{
			value:"staticGroups",
			prototypesInstantiation:{
					 staticInstantiations: [
						 {name:"proto_smokePitDarkWhite",spawningZone:{x:-25,y:18},preinstanciate:true },
						 {name:"proto_smokePitDarkWhite",spawningZone:{x:25,y:18},preinstanciate:true }
					 ],
				 },
		  },
		},
		
		
	},
	
	
	
	proto_forge: {
		
		
		// We need for a «subClass» attribute, since its class is not used directly in attribute definition
		subClass:"Producer",
		
		usesSuper:"proto_abstract_producer",
		
		productions:{
			
			alloys:{
				costs:{
					glykergol:3,water:2,
					coal:1,metal:2,
				},
				wastes:{ mineralWaste:4 },
				timeSeconds:4,
				value:1,
			},
			
		},
		
		infos:{
			title:i18n("forge"),
			description:i18n("forgeDesRessourcesÉlaborées"),
		  actions:{
		  	activateProduction:{
		  		label:i18n("produire")+"%value%",
		  		values:["alloys"],
		  		type:"checkbox",
		  		readCheckedStateForValueMethod:"isProductionActive"},
		  },
		},
		
		heatImageSrc:"forge.heat.png",
		imageConfig:{
			_2D:{
				
				zIndex:2,						
				center:{x:"center",y:"bottom"},
				
				sourcePath:"forge.png",
				sourcePathDark:"forge.dark.png",
				sourcePathBump:"forge.bump.png",

				
			},
		},
		
		GameThumb:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_forgeThumb",
			},
		},
		
		
		GameSmokePit:{
			cardinality:{
			value:"staticGroups",
			prototypesInstantiation:{
					 staticInstantiations: [
						 {name:"proto_smokePitDark",spawningZone:{x:50,y:18},preinstanciate:true },
					 ],
				 },
		  },
		},
		
		
	},
	
		
	
	
	
	proto_catalyst: {
		
		
		// We need for a «subClass» attribute, since its class is not used directly in attribute definition
		subClass:"Producer",
		
		usesSuper:"proto_abstract_producer",
		
		productions:{
			plastics:{
				costs:{
					glykergol:2,water:1,
					coal:1,
				},
				wastes:{ darkWater:2, mineralWaste:1 },
				timeSeconds:3,
				value:1,
			},
			
		},
		
		infos:{
			title:"Catalyst",
			description:i18n("synthétiseDesRessourcesÉlaborées"),
		  actions:{
		  	activateProduction:{
		  		label:i18n("produire")+"%value%",
		  		values:["plastics"],
		  		type:"checkbox",
		  		readCheckedStateForValueMethod:"isProductionActive"},
		  },
		},
		
		heatImageSrc:"catalyst.heat.png",
		imageConfig:{
			_2D:{
				
				zIndex:2,						
				center:{x:"center",y:"bottom"},
				
				sourcePath:"catalyst.png",
				sourcePathDark:"catalyst.dark.png",
				sourcePathBump:"catalyst.bump.png",

				
			},
		},
		
		
		GameThumb:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_catalystThumb",
			},
		},
		
		GameSmokePit:{
			cardinality:{
			value:"staticGroups",
			prototypesInstantiation:{
					 staticInstantiations: [
						 {name:"proto_smokePitDark",spawningZone:{x:0,y:18},preinstanciate:true },
					 ],
				 },
		  },
		},
		
	},
	
	
	
	
	proto_factory: {
		
		// We need for a «subClass» attribute, since its class is not used directly in attribute definition
		subClass:"Producer",
		
		usesSuper:"proto_abstract_producer",
		
		productions:{
			manufactured:{
				costs:{
					glykergol:2,water:1,
					quartzs:1,alloys:1,
				},
				wastes:{ mineralWaste:4, technoWaste:4 },
				timeSeconds:3,
				value:1,
			},
			designed:{
				costs:{
					glykergol:2,water:2,
					alloys:1,plastics:2,
				},
				wastes:{ darkWater:5,technoWaste:2 },
				timeSeconds:4,
				value:1,
			},
			
		},
		
		infos:{
			title:"Factory",
			description:i18n("fabriqueDesPiècesÉlaborées"),
		  actions:{
		  	activateProduction:{
		  		label:i18n("produire")+"%value%",
		  		values:["manufactured","designed"],
		  		type:"checkbox",
		  		readCheckedStateForValueMethod:"isProductionActive"},
		  },
		},
		
		heatImageSrc:"factory.heat.png",
		imageConfig:{
			_2D:{
				
				zIndex:2,						
				center:{x:"center",y:"bottom"},
				
				sourcePath:"factory.png",
				sourcePathDark:"factory.dark.png",
				sourcePathBump:"factory.bump.png",

				
			},
		},
		
		
		GameThumb:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_factoryThumb",
			},
		},
		
		
		GameSmokePit:{
			cardinality:{
			value:"staticGroups",
			prototypesInstantiation:{
					 staticInstantiations: [
						 {name:"proto_smokePitDark",spawningZone:{x:80,y:18},preinstanciate:true },
					 ],
				 },
		  },
		},
		
	},
	
	
	
	proto_retreater: {
		
		// We need for a «subClass» attribute, since its class is not used directly in attribute definition
		subClass:"Producer",
		
		usesSuper:"proto_abstract_producer",
		
		productions:{
			water:{
				costs:{
					glykergol:1, darkWater:1,
				},
				wastes:{ mineralWaste:1 },
				timeSeconds:3,
				value:1,
			},
			
		},
		
		infos:{
			title:i18n("retraiteur"),
			description:i18n("recycleLEauSaleEnEauPure"),
		  actions:{
		  	activateProduction:{
		  		label:i18n("produire")+"%value%",
		  		values:["water"],
		  		type:"checkbox",
		  		readCheckedStateForValueMethod:"isProductionActive"},
		  },
		},
		
		heatImageSrc:"retreater.heat.png",
		imageConfig:{
			_2D:{
				
				zIndex:2,						
				center:{x:"center",y:"bottom"},
				
				sourcePath:"retreater.png",
				sourcePathDark:"retreater.dark.png",
				sourcePathBump:"retreater.bump.png",

				
			},
		},
		
		GameThumb:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_retreaterThumb",
			},
		},
		
		

		
	},
	
	
	
	
	
	
	
	
	
	proto_core: {
		
		// We need for a «subClass» attribute, since its class is not used directly in attribute definition
		subClass:"Producer",
		
//	usesSuper:"proto_abstract_equipment",
		usesSuper:"proto_abstract_producer",
		
		
		productions:{
			glykergol:{
				costs:{
					water:1,
				},
				timeSeconds:5,
				value:4,
			},
		},
		
		infos:{
			title:"Core",
			description:i18n("lÉquipementLePlusImportantQuiPermetDeFournirEnÉnergie"),

		  actions:{
		  	activateProduction:{
		  		label:i18n("produire")+"%value%",
		  		values:["glykergol"],
		  		type:"checkbox",
		  		readCheckedStateForValueMethod:"isProductionActive"},
		  },
		
		},


		heatImageSrc:"core.heat.png",
		imageConfig:{
			_2D:{
				
				zIndex:0,
				center:{x:"center",y:"bottom"},
				sourcePath:"core.png",
				
				sourcePathDark:"core.dark.png",
				sourcePathBump:"core.bump.png",

			},
		},
		
		
		GameThumb:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_coreThumb",
			},
		},
		
	},
	
	
	
	proto_abstract_overEquipment:{
		usesSuper:"proto_abstract_producer",

		size:{w:200,h:75},
		
	},

	
	
	proto_waterer:{
		
			
		// We need for a «subClass» attribute, since its class is not used directly in attribute definition
		subClass:"Producer",
		
		usesSuper:"proto_abstract_overEquipment",
		
		productionsEfficacity:{
			daylightEfficacityFactor:-1,
			workingTemperatures:"2=>80",
		},
		
		productions:{
			water:{
				timeSeconds:4,
				value:1,
			},
		},
		
		infos:{
			title:i18n("condensateurDEau"),
			description:i18n("permetDeCondenserDeLEauÀPartirDeLaRosée"),
		  actions:{
		  	activateProduction:{
		  		label:i18n("produire")+"%value%",
		  		values:["water"],
		  		type:"checkbox",
		  		readCheckedStateForValueMethod:"isProductionActive"},
		  },
		},

		
		heatImageSrc:"waterer.heat.png",
		imageConfig:{
			_2D:{
				
				zIndex:0,
				center:{x:"center",y:"bottom"},
				sourcePath:"waterer.png",
				
				sourcePathDark:"waterer.dark.png",
				sourcePathBump:"waterer.bump.png",

			},
		},
		
		GameThumb:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_watererThumb",
			},
		},
		
		
	},
	
	
	

	proto_solarer:{
			
		// We need for a «subClass» attribute, since its class is not used directly in attribute definition
		subClass:"Producer",
		
		usesSuper:"proto_abstract_overEquipment",
		
		productionsEfficacity:{
			daylightEfficacityFactor:1,
		},
		
		productions:{
			glykergol:{
//			costs:{
//				water:.1,
//			},
				timeSeconds:5,
				value:1,
			},
		},
		
		
		infos:{
			title:i18n("collecteurSolaire"),
			description:i18n("collecteDeLÉnergieSolaire"),
		  actions:{
		  	activateProduction:{
		  		label:i18n("produire")+"%value%",
		  		values:["glykergol"],
		  		type:"checkbox",
		  		readCheckedStateForValueMethod:"isProductionActive"},
		  },
		},

		
		heatImageSrc:"solarer.heat.png",
		imageConfig:{
			_2D:{
				
				zIndex:0,
				center:{x:"center",y:"bottom"},
				sourcePath:"solarer.png",
				
				sourcePathDark:"solarer.dark.png",
				sourcePathBump:"solarer.bump.png",

			},
		},
		
		GameThumb:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_solarerThumb",
			},
		},
		
		
	},
	
	
		
};






