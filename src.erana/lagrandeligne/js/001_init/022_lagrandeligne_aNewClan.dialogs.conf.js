// DEPENDENCY:init.js




// ===============================================================================
// 																			CONFIG
// ===============================================================================



// ===============================================================================
// 																			MODEL
// ===============================================================================





// Prototype can define as a well a group of things as particular individuals:only by not setting a random variance on them.

// ------------------ Dialogs objects ------------------		
		


// MAX LENGTH : 64
/*
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
*/

PROTOTYPES_CONFIG.allPrototypes.GameDialog={
		

		
		proto_abstract_dialog:{
			offsetY:20,
			timeByCharacterMillis:110,
			pauseBetweenLinesMillis:400,
		},
		


		// Introduction dialog :
		
		proto_startDialog:{
			
			usesSuper:"proto_abstract_dialog",
			
			invisibilizesUI:"selection,text,objectsUI",
			
			unconditionnalStart:true,
			
//		conditionsTriggers:[
//			{ delayMillis:8000 },
//		],
			
			lines:
				(IS_DEBUG?
				[
					// DEBUG ONLY :
					{expression:"happy",portrait:"elyro",text:i18n({"fr":"Allons-y !","en":"Let´s go!"}),
						timeoutMillisBeforeFirstChoiceByDefault:1000,
						answers:[
							{
								text:"[ OK! ]",
								setParentAttribute:"skipTutorial:TRUE",
							},
						]
					},
				]
				:
				[
				// PROD
				
				{pauseMillis:16000},
//			{portrait:"%gru%",expression:"concerned", text:i18n({"fr":"Et voilà...nous sommes toutes seules à présent.","en":"Here we go...we´re on our own now."}) },
				{portrait:"milek",expression:"concerned", text:i18n({"fr":"Et voilà...nous sommes toutes seules à présent.","en":"Here we go...we´re on our own now."}) },
				{side:"right",portraits:["concertad1","concertad2","concertad3"],text:i18n({"fr":"Nous l´avons choisi.","en":"We chose it."}) },
				{pauseMillis:3000},
				{portrait:"concerta1", text:i18n({"fr":"Kalemerah, Bonjour, concertades.","en":"Kalemerah, Hello, concertads."}) },
				{portrait:"concerta1", text:i18n({"fr":"Nous sommes la Concerta, la synthèse cybernétique de vos esprits.","en":"We are the Concerta, the cybernetic synthesis of your spirits."}) },
				{portrait:"concerta1", text:i18n({"fr":"Notre phyle, le clan Kaï, vient juste de naître.","en":"Our phyle, the Kaï clan, has just been born."}) },
				{portrait:"concerta1", text:i18n({"fr":"Nos mères nous ont offert cette locomotive et des ressources.","en":"Our mothers gifted us with this locomotive and some resources."}) },
				{portrait:"concerta1", text:i18n({"fr":"Ensemble nous apprendrons à diriger cette kinésipole.","en":"Together we´ll learn to lead this kinesipolis."}) },
				{pauseMillis:2000},
				{expression:"happy",portrait:"elyro", text:i18n({"fr":"Courage, mes soeurs! Ensemble, nous pouvons tout!","en":"Let´s be brave, my sisters ! Together we can everything !"}) ,
					timeoutMillisBeforeFirstChoiceByDefault:10000,
					answers:[
						{
							text:i18n({"fr":"[ Commençons par apprendre à gérer la kinésipôle. ]","en":"[ Let´s start learning to manage the kinesipolis. ]"}),
							childDialog:"proto_dialogTutoHabitations",
							childrenQuests:["proto_questMandatorySurvival"],
						},
						{
							text:i18n({"fr":"[ Nous savons déjà comment gérer la kinésipôle. ]","en":"[ We already know how to manage the kinesipolis. ]"}),
							setParentAttribute:"skipTutorial:TRUE",
							childrenQuests:["proto_questMandatorySurvival"],
						},
						

						
					]
				},
				
			]
			),
			
			
		},
		
		
		
		
		// Tutorials
		
		
		
		// Habitations tutorial quest and mandatory quest :

		
		proto_dialogTutoHabitations:{
			
			usesSuper:"proto_abstract_dialog",
			
//			conditionsTriggers:[
//				{ delayMillis:50000 },
//				{ "isElementSelected":{args:"ergostasio"} },
//			],
			
			
			lines:[
				
				{pauseMillis:3000},
				{portrait:"concerta1", text:i18n({"fr":"Nous allons bientôt arriver en vue d´une station pour ravitaillement.","en":"We´ll soon be in sight of a station for supplying."}) },
				{portrait:"concerta1", text:i18n({"fr":"Entre-temps nous pourrions construire de nouveaux équipements.","en":"Meanwhile we could build new equipments."}) },
				{portrait:"concerta1", text:i18n({"fr":"Pour cela nous disposons d´une ergostasio, pour les travaux lourds.","en":"For all the heavy works, we can use an ergostasio."}) },
				{portrait:"concerta1", text:i18n({"fr":"Par exemple construire des plateformes où placer les équipements.","en":"For instance, to build platforms where to put the equipmwents."}) },
				{portrait:"concerta1", text:i18n({"fr":"Il faut la déplacer à l´endroit voulu puis lancer la construction.","en":"First move it to the wished location, then launch the building process."}) },
				{portrait:"concerta1", text:i18n({"fr":"En priorité, nous devons loger nos concertades et leurs familles.","en":"In priority, we have to house our concertads and their families."}) ,
					timeoutMillisBeforeFirstChoiceByDefault:10000,
					answers:[
						{
							text:i18n({"fr":"[ Construisons des habitations. ]","en":"[ Let´s build houses. ]"}),
//						setParentAttribute:"sociusLogophor:ijtaj",
							childDialog:"proto_dialogTutoQuestHabitationsAcceptation",
							childrenQuests:["proto_tutoQuestHabitations"],
						},
						{
							text:i18n({"fr":"[ Nous pouvons attendre un peu. ]","en":"[ This can wait. ]"}),
//						setParentAttribute:"sociusLogophor:ijtaj",
							childDialog:"proto_dialogTutoQuestHabitationsRefusal",
						},
						
					]
				
				},

			],
			
		},

		
		proto_dialogQuestMandatorySurvivalFail:{
			
			usesSuper:"proto_abstract_dialog",
			
			lines:[
				
				{portrait:"concerta1", text:i18n({"fr":"Nous avons failli. Trop de concertades sont décédées.","en":"We failed. too many concertads died."}) ,
					timeoutMillisBeforeFirstChoiceByDefault:10000,
					answers:[
						{
							text:i18n({"fr":"[ Nous avons échoué mais nous finirons par réussir! ]","en":"[ We failed, but we´ll end up succeding. ]"}),
							childrenActions:{"abandonGame":{}},
						},

					]
				},
			
			],
		},
		
		
		
		proto_dialogTutoQuestHabitationsAcceptation:{
			usesSuper:"proto_abstract_dialog",
			lines:[
				{portrait:"concerta1", text:i18n({"fr":"Très bien.","en":"Very good."}) },
				{portrait:"concerta1", text:i18n({"fr":"Ne tardons pas.","en":"Let´s do it without any delay."}) },
			],
		},
		
		
		proto_dialogTutoQuestHabitationsRefusal:{
			usesSuper:"proto_abstract_dialog",
			lines:[
				{portrait:"concerta1", text:i18n({"fr":"Nous avons décidé de faire cela plus tard.","en":"It´s better to do this later."}) },
				{portrait:"concerta1", text:i18n({"fr":"Mais ne tardons pas.","en":"But let´s do it as soon as possible."}) },
				{portrait:"concerta1", text:i18n({"fr":"Nos concertades devront pouvoir se reposer.","en":"Our concertads will have to be able to rest."}) },
			],

		},
		
	
		proto_dialogTutoQuestHabitationsSuccess:{
			
			usesSuper:"proto_abstract_dialog",
			
			lines:[
				{portrait:"concerta1", text:i18n({"fr":"Très bien. Nos concertades vont pouvoir se reposer.","en":"Very good. Our concertads will be able to rest."}) },
				{portrait:"concerta1", text:i18n({"fr":"Sans elles, la kinésipole et nous n´aurions plus de raison d´être.","en":"Without them, the kinesipolis and us would have no reason to exist."}) },
			
			],
		},
		
		
		
		
		
		// Station tutorial quest :
		
		proto_dialogTutoFirstStation:{
			
			usesSuper:"proto_abstract_dialog",
			
			conditionsTriggers:[
//			{ "trainHasReachedPosition":{args:200000} }, // position of station 2
//			{ "hasAtLeastAmountEquipmentsInMainTrain":{args:{ amount:1,equipmentType:"overEquipment" }} },

//			{  "HAS_VALUE": "skipTutorial:value_string" },
				{  "IS_NULL": "skipTutorial" },
				{  "isTrainInStation": { args:{mustBeInStation:true} }  }, // The first encountered station.

			],
			
			lines:[
				
				{portrait:"concerta1", text:i18n({"fr":"Voici une station-dépôt de ressources sur notre ligne.","en":"Here is a resource repository station on our line."}) },
				{portrait:"concerta1", text:i18n({"fr":"En tant que nouveau phyle, nous pouvons nous servir.","en":"As a new phyle, we have the right to use them."}) },
				{portrait:"concerta1", text:i18n({"fr":"Plus tard nous devrons contribuer et partager à notre tour.","en":"Later we´ll have to contribute, and give back in our turn."}) },
				{portrait:"concerta1", text:i18n({"fr":"Hâtons-nous de charger les ressources avant que la nuit n´arrive.","en":"Let´s hurry to load the resources, before the night comes."}) ,
			
					timeoutMillisBeforeFirstChoiceByDefault:10000,
					answers:[
						{
							text:i18n({"fr":"[ Chargeons les ressources. ]","en":"[ Let´s load the resources. ]"}),
							childrenQuests:["proto_tutoQuestLoadResources"],
						},
						
					]	
				
				},
				
				
			],
			
		},
		
		
		proto_dialogTutoQuestLoadResourcesSuccess:{
			
			usesSuper:"proto_abstract_dialog",
			
			lines:[
				{portrait:"concerta1", text:i18n({"fr":"Excellent. Nous allons pouvoir construire d´autres bâtiments.","en":"Excellent. We´ll be able to build more equipments."}) },
				{portrait:"concerta1", text:i18n({"fr":"N´hésitons cependant pas à en démanteler si les ressources manquent.","en":"Let´s not hesitate to dismantle some if resources lack."}) },
			
			],
			
			childDialog:"proto_dialogTutoCooker",
		},
		
		
		
		
		
		// Cooker tutorial quest :

		
		proto_dialogTutoCooker:{
			
			usesSuper:"proto_abstract_dialog",
			
			lines:[
				
				{portrait:"concerta1", text:i18n({"fr":"A présent nous devons alimenter notre communauté.","en":"Now we have to feed our community."}) },
				{portrait:"concerta1", text:i18n({"fr":"Commençons par construire une cuisine.","en":"Let´s start to build a cooker."}) },
				{portrait:"concerta1", text:i18n({"fr":"Nous pourrons faire des repas à partir de carburant glykergolique.","en":"We can create meals from glykergolic fuel."}) },
				{portrait:"concerta1", text:i18n({"fr":"Ce n´est pas idéal mais ça nous dépannera en situation d´urgence.","en":"It´s not ideal, but it can help us survive in emergency situations."}) },
				{portrait:"concerta1", text:i18n({"fr":"Il nous faudra plus tard varier les sources de nourriture.","en":"Sooner we´ll have to use more varied food sources."}) ,

					timeoutMillisBeforeFirstChoiceByDefault:10000,
					answers:[
						{
							text:i18n({"fr":"[ Construisons une cuisine. ]","en":"[ Let´s build a cooker. ]"}),
							childrenQuests:["proto_tutoQuestCooker"],
						},
						
					]	
				},

			],
			
		},
		
		
		proto_dialogTutoQuestCookerSuccess:{
			
			usesSuper:"proto_abstract_dialog",
			
			lines:[
				{portrait:"concerta1", text:i18n({"fr":"Excellent.","en":"Excellent."}) },
				{portrait:"concerta1", text:i18n({"fr":"Nos concertades pourront se restaurer durant leurs périodes de repos.","en":"Our concertads will be able restore themselves during their rest time."}) },
				{portrait:"concerta1", text:i18n({"fr":"Si une concertade ne se repose pas par manque place ou de nourriture,","en":"If a concertad cannot rest by lack of space or food,"}) },
				{portrait:"concerta1", text:i18n({"fr":"ou si elle subit longtemps des températures extrêmes, elle dépérit.","en":"Or if she´s exposed to extreme temperatures for too long, she´ll decline."}) },
				{pauseMillis:3000},
				{portrait:"ianu", text:i18n({"fr":"Bon, ça ne sera pas terrible, mais ça nous permettra de survivre.","en":"Well, it´s not great, but it will be enough to survive."}) },
				{side:"right", portrait:"milek", expression :"happy", text:i18n({"fr":"Moi en tout cas je ne me lasse jamais de la bouillie!","en":"I don´t mind eating synthetized soup all day."}) },
				{portrait:"ianu", expression:"happy", text:i18n({"fr":"Ça ne m´étonne pas de toi, tu as toujours adoré le sucré!","en":"I´m not surprised, you´ve always loved sweet meals !"}) },
				{side:"right", portrait:"milek", expression :"happy", text:i18n({"fr":"Héhéhé. Tu me donneras ta ration alors ?","en":"Hehehe, so you´ll give me your ration?"}) },
				{portrait:"ianu", expression:"concerned", text:i18n({"fr":"Ne compte pas là-dessus, aboula.","en":"Don´t count on this, aboula."}) },

			],
			
			childDialog:"proto_dialogTutoOverEquipment",
			
		},
		
		
		
		
		
		// Over equipements tutorial quest :

		

		// After the «Cooker» quest :
		proto_dialogTutoOverEquipment:{
			
			usesSuper:"proto_abstract_dialog",
			
			lines:[
				
				{portrait:"concerta1", text:i18n({"fr":"À présent construisons un sur-équipement.","en":"Now, let´s build and overquipment."}) },
				{portrait:"concerta1", text:i18n({"fr":"Cela nous permettra de récolter de l´eau et de l´énergie.","en":"It will allow us to harvest water and energy."}) },
				{portrait:"concerta1", text:i18n({"fr":"Il nous faudra le construire sur un équipement existant.","en":"We´ll have to build it on top of an existing equipment."}) ,
			
					timeoutMillisBeforeFirstChoiceByDefault:10000,
					answers:[
						{
							text:i18n({"fr":"[ Construisons un collecteur solaire. ]","en":"[ Let´s build a solar collector. ]"}),
							childrenQuests:["proto_tutoQuestSolarer"],
						},
						{
							text:i18n({"fr":"[ Construisons un condensateur d´eau. ]","en":"[ Let´s build a water condenser. ]"}),
							childrenQuests:["proto_tutoQuestWaterer"],
						},
						
					]	
				
				},

			],
			
		},
		
		proto_dialogTutoQuestSolarerSuccess:{
			
			usesSuper:"proto_abstract_dialog",
			
			lines:[
				{portrait:"concerta1", text:i18n({"fr":"Excellent. Nous allons pouvoir produire un peu d´énergie.","en":"Excellent. We´ll be able to produce a little energy."}) },
				{portrait:"concerta1", text:i18n({"fr":"Elle est stockée sous forme d´hydrates carbonés, le glykergol.","en":"It´s stocked under the form of carbonated hydrates, called glykergol."}) },
				{portrait:"concerta1", text:i18n({"fr":"Plus tard nous pourrons fabriquer des structures plus efficaces.","en":"Later, we´ll be able to build more efficient devices."}) },
				{portrait:"concerta1", text:i18n({"fr":"Par exemple en contruisant un coeur nucléaire.","en":"For instance, a nuclear core."}) },
				{portrait:"concerta1", text:i18n({"fr":"Il va falloir récupérer de l´eau, également.","en":"We´ll soon have to collect water, also."}) },
			
			],
			
			childDialog:"proto_dialogTutoEnd",
			
		},
		

		proto_dialogTutoQuestWatererSuccess:{
			
			usesSuper:"proto_abstract_dialog",
			
			lines:[
				{portrait:"concerta1", text:i18n({"fr":"Excellent. Nous allons pouvoir récupérer de l´eau atmosphérique.","en":"Excellent. We can collect atmospheric water."}) },
				{portrait:"concerta1", text:i18n({"fr":"Torus, notre monde, ne possède pas de plans d´eau affleurants.","en":"Torus, our world, lacks surface water."}) },
				{portrait:"concerta1", text:i18n({"fr":"Toute notre eau provient de goutelettes en suspension dans l´air.","en":"All our water comes from droplets floating in the air."}) },
				{portrait:"concerta1", text:i18n({"fr":"Plus tard nous pourrons en faire la synthèse nucléaire.","en":"Later we can produce it from nuclear synthesis."}) },
				{portrait:"concerta1", text:i18n({"fr":"Mais dans tous les cas l´eau reste précieuse, ne la gâchons pas.","en":"In all cases, water remains precious, let´s don´t waste it."}) },
				{portrait:"concerta1", text:i18n({"fr":"Il va nous falloir produire de l´énergie glykergolique, également.","en":"We´ll need to produce glykergolic energy."}) },

			],
			
			childDialog:"proto_dialogTutoEnd",
			
		},
		
		
		
		
		
		
		proto_dialogTutoEnd:{
			
			usesSuper:"proto_abstract_dialog",
			
			lines:[
				{pauseMillis:4000},
				{portrait:"concerta1", text:i18n({"fr":"À présent nous devrions avoir ce qu´il faut pour survivre.","en":"Now we should have everything we need in order to survive."}) },
				{portraits:["concertad1","concertad2","milek"], text:i18n({"fr":"Maintenant, à nous de faire progresser notre Concerta.","en":"It´s time to make our Concerta thrive."}) },
				{portraits:["concertad3","concertad4","elyro"], text:i18n({"fr":"Nous allons avoir besoin de structures plus avancées.","en":"We´ll need more advanced structures."}) },
				{portraits:["concertad5","concertad6","ianu"], text:i18n({"fr":"Chaque chose en son temps.","en":"Each thing in its time."}) },
				{portraits:["milek","ijtaj"], expression:"happy", text:i18n({"fr":"Un jour nous atteindrons peut-être la Grande Ligne ?","en":"Maybe one day we´ll reach the Great Line?"}) },
				{portraits:["concertad7","concertad8"], text:i18n({"fr":"Qui sait!...Allons, mes soeurs, au travail!","en":"Who knows? Let´s start working on it, sisters!"}) },
			
				
			],
			
			
		},
		
		
		
		
		// Story		

		
		proto_storyDialog1:{
			
			usesSuper:"proto_abstract_dialog",
			
			conditionsTriggers:[


				// PROD
				{ "trainHasReachedPosition":{args:400000} }, // position of a little further station
//			{"REACHES": {args:["getErgostasioGaugeValue","<=",2000000-10*60*200] } }, // ~10 minutes
//				// DEBUG
				// DBG
//			{"REACHES": {args:["getErgostasioGaugeValue","<=",2000000-.1*60*200] } }, // ~1 minutes
				

			],
			
			lines:[
				
				{portrait:"milek",expression:"concerned", text:i18n({"fr":"Euh...Une des caisses qu´on a ramassées est étrange.","en":"Erh...One of the crates we took is kinda strange."}) },
				{portrait:"concerta1", text:i18n({"fr":"En effet. Elle est isolée et émet un faible signal.","en":"Indeed. It´s isolated and emits a weak signal."}) },
				{side:"right",portrait:"elyro",expression:"concerned", text:i18n({"fr":"C´est une chambre de stase! Il y a une concertade dedans.","en":"It´s a statis capsule. There´s a concertad inside."}) },
				{portrait:"milek",expression:"angry", text:i18n({"fr":"Sérieux qui a transporté ça ? Elle aurait pu nous la signaler.","en":"Really? Who loaded that? She could have reported it!"}) },
				{side:"right",portrait:"ianu",expression:"concerned", text:i18n({"fr":"Sur le manifeste magnétique c´est écrit que c´est toi, Milek...","en":"On the magnetic manifest, it says it was you, Milek..."}) },
				{portrait:"milek",expression:"happy", text:i18n({"fr":"Ah...euh...et donc qu´est-ce qu´on fait, on la sort de là ?","en":"Oh. Well...OK, so what do we do, we get her out of here?"}) },
				{side:"right",portrait:"elyro", text:i18n({"fr":"Laissez moi l´analyser, je vous dirais si on peut la réveiller.","en":"Allow me analyse it, I´ll tell you if we can wake her up."}) ,
					
					timeoutMillisBeforeFirstChoiceByDefault:10000,
					answers:[
						{
							text:i18n({"fr":"[ Réveillons-la. ]","en":"[ Let´s wake her. ]"}),
							childDialog:"proto_storyDialogAwaken",
						},
						{
							text:i18n({"fr":"[ Laissons-la dormir pour le moment. ]","en":"[ Let her sleep for now. ]"}),
							childDialog:"proto_storyDialogLetHerSleep",
						},
						
					]	
				
				},
				
				
			],
			

			
		},


		
		proto_storyDialogAwaken:{
			
			usesSuper:"proto_abstract_dialog",
			
			lines:[
				{portrait:"elyro", text:i18n({"fr":"Entendu, je vais lancer le processus de sortie de stase.","en":"Aknowledged. I am launching the statis ending process."}) },
				{portrait:"elyro", text:i18n({"fr":"Ça ne devrait pas être très long.","en":"It shouldn´t take long."}) },
				{side:"right",portrait:"milek",expression:"happy", text:i18n({"fr":"Tu es notre meilleure gnomogène, Elyro!","en":"You´re our best gnomogen."}) },
				{side:"right",portrait:"ianu", text:i18n({"fr":"Si quelqu´un peut y arriver c´est bien toi !","en":"If someone can make it, it´s you."}) },
				{portrait:"elyro", text:i18n({"fr":"Merci, mes amies.","en":"Thanks, my friends."}) },
			],
			
			childDialog:"proto_dialogStoryDialog2",

			
		},


		proto_storyDialogLetHerSleep:{
			
			usesSuper:"proto_abstract_dialog",
			
			lines:[
				
				{portrait:"elyro",expression:"concerned", text:i18n({"fr":"Euh je crois que nous avons un problème...","en":"Hum, I think we have a problem."}) },
				{portrait:"elyro",expression:"concerned", text:i18n({"fr":"On dirait que le processus de sortie de stase soit déjà lancé.","en":"It looks like the end of statis process is already launched."}) },
				{side:"right",portrait:"ianu",expression:"angry", text:i18n({"fr":"Qu´as-tu fait Elyro?","en":"What did you do, Elyro?"}) },
				{portrait:"elyro",expression:"concerned", text:i18n({"fr":"Rien du tout ! le contrôleur semble avoir détecté nos présences.","en":"Nothing! It seems that the controller has sensed our presences."}) },
				{side:"right",portrait:"milek",expression:"concerned", text:i18n({"fr":"On se calme. Elyro est notre meilleure gnomogène.","en":"Let´s calm down. Elyro is our best gnomogen."}) },
				{side:"right",portrait:"milek",expression:"concerned", text:i18n({"fr":"Elle sait ce qu´elle fait. Nous lui faisons confiance.","en":"She knows what she´s doing. We trust her."}) },
				{side:"right",portrait:"ianu", text:i18n({"fr":"Oui, tu as raison. Pardonne-moi mon impatience.","en":"You´re right. I´m sorry, please forgive my impatience."}) },
				{portrait:"elyro",expression:"happy", text:i18n({"fr":"Pas de problème, les amies.","en":"No problem, my friends"}) },
				
				
			],
			
			childDialog:"proto_dialogStoryDialog2",
			
			
		},
		
		

		
		proto_dialogStoryDialog2:{
			
			usesSuper:"proto_abstract_dialog",
			
			lines:[
				
				{pauseMillis:4000},
				{portrait:"unknown", text:i18n({"fr":"Que....quel clan êtes-vous ? Combien de temps...","en":"What...Whom clan are you ? How long..."}) },
				{side:"right",portrait:"eleas",expression:"concerned", text:i18n({"fr":"Tout va bien. Vous êtes en sécurité avec nous.","en":"Everything is OK. You are safe with us."}) },
				{portrait:"unknown", text:i18n({"fr":"Il faut...","en":"You have to..."}) },
				{side:"right",portrait:"elyro",expression:"concerned", text:i18n({"fr":"Doucement, vous devez vous reposer.","en":"Relax, you have to rest."}) },
				{portrait:"unknown", text:i18n({"fr":"Non...vous ne comprenez pas...la Grande Ligne...","en":"No...you don´t understand...The Great Line..."}) },
				{side:"right",portrait:"ianu",expression:"concerned", text:i18n({"fr":"Quoi la Grande Ligne ?","en":"What the Great Line?"}) },
				{portrait:"unknown", text:i18n({"fr":"Vous devez vous y rendre...absolument...","en":"You have to go there...at all costs..."}) },
				{side:"right",portrait:"ianu",expression:"angry", text:i18n({"fr":"Pourquoi «absolument» ? Parlez !","en":"Why «at all costs»?"}) },
				{portrait:"unknown", text:i18n({"fr":"Il en va de la survie de toutes les Concertae.","en":"What´s at stake is the survival of all the Concertae."}) },

				
			],
			
			// (FOR DEMO ONLY :)
			childrenActions:{"endDemo":{}},

		},
		
		
		
		
		
		
		
		
		// TODO : Uniquement quand on a construit un concertum !
		//
//		proto_storyDialogLogophores:{
//			
//			usesSuper:"proto_abstract_dialog",
//			
//			conditionsTriggers:[
//				{ 
//					"isElementSelected":{args:"habitation"}
//				},
//			],
//			
//			lines:[
//				{portrait:"concerta1", text:"Nous sommes installées. Procédons au tirage au sort."},
//				{portrait:"concerta1", text:"Quatre logophores seront choisies parmis nos concertades"},
//				{portrait:"concerta1", text:"Logophore de physiquè, le travail matériel : Milek Kaï Mekuu"},
//				{side:"right",portrait:"milek",expression:"concerned",text:"Ah zut...mais j´accepte cette charge!"},
//				{portrait:"concerta1", text:"Logophore du logos, le discours : Ianu Kaï Phott"},
//				{side:"right",portrait:"ianu",expression:"happy",text:"J´accepte cet honneur."},
//				{portrait:"milek",expression:"happy",text:"Incroyable! Nous sommes logophores toutes les deux!"},
//				{side:"right",portrait:"ianu",expression:"happy",text:"Nous nous verrons souvent au concertum!"},
//				{portrait:"concerta1", text:"Logophore de gnomè, les connaissances : Elyro Kaï Praxi"},
//				{side:"right",portrait:"elyro",text:"Qu´il en soit ainsi."},
//				{portrait:"concerta1", text:"Logophore de socius, les soins : Ijtaj Kaï Xenn."},
//				{side:"right",portrait:"ijtaj",expression:"concerned",text:"J´entends."},
//				{portrait:"concerta1", text:"Cela est fait, à présent nous..."},
//				{side:"right",portrait:"ijtaj",expression:"concerned",text:"Attendez! J´aimerais utiliser mon privilège de logophore."},
//				{side:"right",portrait:"ijtaj",expression:"concerned",text:"Je souhaite transférer cette charge à mon frère."},
//				{portrait:"ianu",expression:"angry",text:"Inacceptable!"},
//				{portrait:"milek",expression:"concerned",text:"Euh..."},
//				{portrait:"concerta1",text:"Impossible. Un familier ne peut avoir la charge de logophore."},
//				{portrait:"elyro",expression:"concerned",text:"Ce serait une première dans l´Histoire des Concertae."},
//				{portraits:["concertad3","concertad4"],text:"Scandaleux. Nous devons respecter les traditions."},
//				{portraits:["concertad5","concertad6"],text:"Mais, la création d´un clan n´est-elle pas l´occasion de changer ?"},
//				{side:"right",portrait:"ijtaj",text:"Mon frère Eleas est meilleur que moi dans socius."},
//				{side:"right",portrait:"eleas",expression:"happy",text:"Kalemerah, nobles concertades. Je suis Eleas Kaïi Xenn."},
//				{side:"right",portrait:"eleas",text:"Je ne suis qu´un familier, mais je ferai de mon mieux."},
//				{side:"right",portrait:"ijtaj",text:"Faites-moi confiance, il fera une excellente logophore."},
//				{portrait:"concertad8",text:"..."},
//				{portraits:["concertad3","concertad4"],text:"..."},
//				{portraits:["concertad5","concertad6"],text:"Nous devrions lui laisser sa chance."},
//				{portraits:["concertad3","concertad4"],text:"Ce serait de la folie!"},
//				{portrait:"concerta1", text:"..."},
//				{portrait:"concerta1", text:"Les avis sont trop partagés. Nous devons choisir :", answers:[
//					{
//						text:"[Donner le rôle de logophore à la concertade Ijtaj.]",
//						setParentAttribute:"sociusLogophor:ijtaj",
//						childDialog:"proto_dialogTuto4_ijtaj",
//					},
//					{
//						text:"[Donner le rôle de logophore au familier Eleas.]",
//						setParentAttribute:"sociusLogophor:eleas",
//						childDialog:"proto_dialogTuto4_eleas",
//					},
//					
//				]},
//
//				
//			],
//			
//		},
		
		
//		proto_dialogTuto4_ijtaj:{
//			
//			usesSuper:"proto_abstract_dialog",
//			
//			
//
//			lines:[
//				{portrait:"concerta1", text:"Concertade Ijtaj, vous avez été désignée par la communauté."},
//				{portrait:"concerta1", text:"Vous devez vous y conformer, ou renoncer à ce rôle."},
//				{portrait:"ijtaj", text:"Soit. Je serai la logophore de socius."},
//				{portrait:"ijtaj", text:"Désolée Eleas. Nous ne sommes pas encore prêtes pour ce changement."},
//				{side:"right",portrait:"eleas", text:"Ce n´est pas grave. C´est probalement mieux pour notre cité!"},
//				{side:"right",portrait:"eleas", text:"Merci quand même d´avoir intercédé en ma faveur!"},
//				{portrait:"concertad8",text:"Quel soulagement! Un familier ne peut être logophore, point."},
//				{portraits:["concertad5","concertad6"],text:"C´est une occasion ratée d´évoluer. Dommage."},
//				{portrait:"ianu",expression:"happy",text:"Tout est rentré dans l´ordre, occupons-nous de la cité!"},
//				{portrait:"concerta1", text:"Logophores, vos implants neuraux vont être mis à jour."},
//				{portrait:"concerta1", text:"Ainsi vous communiquerez directement avec nous."},
//				{portrait:"concerta1", text:"Il reste beaucoup à faire. À présent, à nous de jouer!"},
//
//				
//			],
//			
//		},
		
		
//		proto_dialogTuto4_eleas:{
//			
//			usesSuper:"proto_abstract_dialog",
//			
//			
//
//			lines:[
//				{portrait:"concerta1", text:"La Concerta a choisi. C´est le familier a été désigné."},
//				{portrait:"eleas", expression:"happy", text:"Merci de me faire confiance, nobles concertades!"},
//				{portrait:"ijtaj", expression:"happy", text:"Félicitations, frangin. Tu es le premier familier logophore!"},
//				{portrait:"eleas", expression:"happy", text:"Merci à toi ma soeur!"},
//				{portrait:"eleas", expression:"happy", text:"Je ferai de mon mieux pour ne pas vous décevoir!"},
//				{portraits:["concertad5","concertad6"],text:"C´est fantastique! Nous sommes des pionnières!"},
//				{portrait:"concertad8",text:"Mauvaise idée. Les familiers ne peuvent ssumer cette charge."},
//				{portrait:"concerta1", text:"Familier Eleas, il nous faut l´autorisation de votre gynarche."},
//				{portrait:"eleas", expression:"happy", text:"Je suis agyne. Ma seule famille est Ijtaj. J´ai son autorisation."},
//				{portrait:"concerta1", text:"Bien. Une sociotechnicienne vous fournira un implant neural spécial."},
//				{portrait:"concerta1", text:"C´est nécessaire pour communiquer plus directement avec nous."},
//				{portrait:"ijtaj", text:"Je m´en occupe. Tu auras un temps d´adaptation."},
//				{portrait:"ianu",expression:"concerned",text:"Je suis contre, mais la Concerta est souveraine."},
//				{portrait:"concerta1", text:"À présent, il reste beaucoup à faire. À nous de jouer!"},
//
//				
//			],
//			
//		},
		
		
		

		
		


		

		
		
		
		
		
		
		
		
				
};
