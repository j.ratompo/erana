// DEPENDENCY:init.js




// ===============================================================================
// 																			CONFIG
// ===============================================================================



// ===============================================================================
// 																			MODEL
// ===============================================================================




// Prototype can define as a well a group of things as particular individuals:only by not setting a random variance on them.

// ------------------ Special equipments objects ------------------		
		
PROTOTYPES_CONFIG.allPrototypes.PlasmaTorch={



	
	proto_plasmaTorch:{
		

		// We need for a «subClass» attribute, since its class is not used directly in attribute definition
		subClass:"PlasmaTorch",
		
		
		type:"overEquipment",
		
		
		
		size:{w:200,h:80},

		GrandeLigneGauge:{
			cardinality:{
				value:1,
				prototypesInstantiation:"proto_gaugeEquipment",
			},
		},
		
		

		flamesByStrength:{
			
			1:{
				
				costs:{
					glykergol:.1
				},
				wastes:{ },
				timeSeconds:.1,
				preheatTimeMillis:2000,
				
				
				damagePerTimeMillis:1,
				reachSize:{ w:400 },
				
								
			},
			
		},
		

		preheatImageConfig_1:{
			_2D:{
				center:{x:"left",y:"bottom"},
				
				// Animations :
				refreshMillis:200,
				size:{w:50,h:50},
				offsets:{x:98,y:-20},
				animations:{
					"default":["flames.1/littleFlame1.png","flames.1/littleFlame2.png","flames.1/littleFlame3.png","flames.1/littleFlame4.png","flames.1/littleFlame5.png",
										 "flames.1/littleFlame6.png","flames.1/littleFlame7.png","flames.1/littleFlame8.png",],
				},
			},
		},
		
		
		flameImageConfig_1:{
			_2D:{
				center:{x:"left",y:"bottom"},
				
				// Animations :
				refreshMillis:200,
				size:{w:800,h:100},
				offsets:{x:-10,y:10},
				animations:{
					"default":["flames.1/flame1.png","flames.1/flame2.png","flames.1/flame3.png","flames.1/flame4.png","flames.1/flame5.png",
										 "flames.1/flame6.png","flames.1/flame7.png","flames.1/flame8.png","flames.1/flame9.png",],
				},
			},
		},
					
		
		infos:{
			title:i18n({"fr":"Torche plasma","en":"Plasma torch"}),
			description:i18n({"fr":"Cet équipement spécial projette un puissant plasma capable de vaporiser à courte portée n`importe quoi qui se dresserait en travers de notre chemin.","en":"This special equipment can project a powerful plasma flame that will be able to vaporize anything in short range that would stand in our way."}),
		  actions:{

		  	activate:{
		  		label:i18n({"fr":"Activer","en":"Activate"}),
		  		type:"checkbox",
		  		readCheckedStateForValueMethod:"isActive",
		  	},
		  },
		  
		},
	
		
		heatImageSrc:"plasmaTorch.heat.png",
		imageConfig:{
			_2D:{
				
				zIndex:0,
				center:{x:"center",y:"bottom"},
				sourcePath:"plasmaTorch.png",
				
				sourcePathDark:"plasmaTorch.dark.png",
				sourcePathBump:"plasmaTorch.bump.png",
	
			},
		},
		
		
		
		
		GameSmokePit:{
			cardinality:{
			value:"staticGroups",
			prototypesInstantiation:{
					 staticInstantiations: [
						 {name:"proto_smokePitDarkWhite",spawningZone:{x:5,y:10},preinstanciate:true },
					 ],
				 },
		  },
		},
		
		GameThumb:"proto_plasmaTorchThumb",
		
		
	},

	
};








