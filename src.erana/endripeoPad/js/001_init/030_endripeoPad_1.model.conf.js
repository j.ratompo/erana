// DEPENDENCY:init.js




// ===============================================================================
// MODEL
// ===============================================================================




PROTOTYPES_CONFIG = {

	allPrototypes:{
		
		
		// ------------------ Levels objects ------------------
		GameLevel:{
		
			
			proto_level1:{
				
				selectedItem:"getSelectedEntity()",
				
				MusicScene:"proto_musicScene1",
				
			},
			
			
			
		},
		
		
		
		// ------------------ Game objects ------------------
		
		MusicScene:{
			
			proto_musicScene1:{

				containerConfig:"starter", // To indicate where the player starts in the tree objects hierarchy
				
//				backgroundColor:"#FEE7A2",
//				backgroundColor:"#444444",

				backgroundZIndex:1,
				backgroundParallax:1,
				backgroundOffsetY:0,
				backgrounds:[
//// {images:["backgroundSky1.png","backgroundSky2.png","backgroundSky3.png","backgroundSky4.png"],parallaxAdd:100},
//// //
//// {images:["backgroundFore1.png","backgroundFore2.png","backgroundFore3.png","backgroundFore4.png"],parallaxAdd:10},
 					{images:["background1.png","background2.png"]},
//
//// {images:["backgroundSky1.png"],parallaxAdd:100},
//// {images:["background1.png"],parallaxAdd:10},
//// {images:["backgroundFore1.png"]},
//					{images:["backgroundSky1_v1.png"],parallaxAdd:400},
//					{images:["background1_v1.png"],parallaxAdd:200},
//					{images:["backgroundFore1_v1.png"],parallaxAdd:20},
//					{images:[{normal:"foregroundGround1.png"}],offsetY:-230,clipSize:{w:1600,h:100}},
				],
				
//				foregroundParallax:1,
//// foregroundOffsetY:10,
//				foregrounds:[
//// {images:["foreground1.png"],offsetY:-20},
//					{images:[ {normal:"foreground1_v1.png",dark:"foreground1.dark.png"} ],offsetY:-20},
//				],
				
				
//				RoleCharacter:["proto_roleCharacterMichelTremblay"],

				InstrumentPlayer:{
				
				
					cardinality : {
						
//						value:"oneGroupAtRandom",
						value:"staticGroups",
						
						prototypesInstantiation : {
//							randomInstantiations:[
//								{value:1,name:"proto_tiniestSpark", spawningZone:{x:0, y:0, h:1, w:1} },
//				        		{value:"4->6",name:"proto_tiniestSpark", spawningZone:{x:0, y:0, h:100, w:100} },
//				        		{value:"20->30",name:"proto_tiniestSpark", spawningZone:{x:400, y:0, h:100, w:100} },
//							],
						    staticInstantiations:[
						    	{value:1,name:"proto_tiniestSparkPlayer", spawningZone:{x:0, y:0, h:1, w:1}, preinstanciate:true },
//						    	{value:"4->6",name:"proto_tiniestSpark", spawningZone:{x:0, y:0, h:100, w:100} },
						        {value:"10->20",name:"proto_tiniestSpark", spawningZone:{x:400, y:0, h:300, w:300} },
							]
						},
					},
					
					
//					cardinality:{
//						value:"staticGroups",
//						prototypesInstantiation:{
//							staticInstantiations:[
//								{name:"proto_tiniestSpark",spawningZone:{x:200,y:0},preinstanciate:true},
//								{value:1,name:"proto_tiniestSpark", spawningZone:{x:0, y:0, h:1, w:1},preinstanciate:true },
//				        		{value:"4->6",name:"proto_tiniestSpark", spawningZone:{x:0, y:0, h:100, w:100} },
//				        		{value:"20->30",name:"proto_tiniestSpark", spawningZone:{x:0, y:0, h:100, w:100} },
//							]
//						},
//					},
				
				
				},
				
			},
			
		},

		
		InstrumentPlayer:{
		
			proto_abstract_instrumentPlayer:{
			
				
				
			},
		
						
				
			proto_tiniestSpark:{
				usesSuper:"proto_abstract_entity",
				
				speed:6,
				triggerMovingPercent:60,
				movingZone:{w:300,h:300},
				
				
				size:{w:60,h:60},
				
				GameAutomaton:"proto_tiniestSparkBehavior",
				
				EntityAction:["proto_setCollaborate","proto_setOppose"],
				
				imageConfig : {
					
//					colorLayer:{color:"#FF0000",opacity:0.5},
				
					_2D : {
						center:{x:"center",y:"center"},
						refreshMillis:200,
						clipSize:{w:100,h:100},
						idles : {
							triggerAfterMillis : 4000,
							sourcesPaths : [
								"tiniestSparkIdle1.png", "tiniestSparkIdle2.png", 
							],
						},
						bidimensional8 : {
							walk:{ 
								N : "tiniestSparkWalkN.png",
								S : "tiniestSparkWalkS.png",
								E : "tiniestSparkWalkE.png",
								W : "tiniestSparkWalkW.png",
								NE : "tiniestSparkWalkNE.png",
								SE : "tiniestSparkWalkSE.png",
								NW : "tiniestSparkWalkNW.png",
								SW : "tiniestSparkWalkSW.png",
							},
							standby:{
								N : "tiniestSparkStandby.png",
								S : "tiniestSparkStandby.png",
								E : "tiniestSparkStandby.png",
								W : "tiniestSparkStandby.png",
								NE : "tiniestSparkStandby.png",
								SE : "tiniestSparkStandby.png",
								NW : "tiniestSparkStandby.png",
								SW : "tiniestSparkStandby.png",
							}
						}
					}
					
				},
				
				
			},
			
					
			proto_tiniestSparkPlayer:{
				usesSuper:"proto_tiniestSpark",
				
				isAIControlled:false,
				
			},
			
			
			
			proto_quantumFluctuation:{
				usesSuper:"proto_tiniestSpark",
			
			
			
			},
			
		
			
		},
		
		
		
		
		
		GameAutomaton:{
			
			proto_tiniestSparkBehavior:{
				
				states:{
					"INITIAL":{
						delayMillis:1000,
						transitions:{
							"goToRandomPoint":"isMoveTriggered",
							"FINAL":"hasDisappeared",
						},
						onElse:"INITIAL",
					},
					
					"goToRandomPoint":{
						delayMillis:1000,
						transitions:{
							"FINAL":"hasDisappeared",
						},
						onElse:"INITIAL",
					},
					
					"FINAL":{
						/*DO NOTHING*/
					}
					
				},
				
			},
			
		},
	
		
		
		
		EntityAction:{
		
		
			proto_abstract_entityAction:{
			
				size:{w:40,h:45},

			},
		
			proto_setCollaborate:{
			
				usesSuper:"proto_abstract_entityAction",
		
		
				imageConfig:{
					_2D:{
						
						center:{x:"center",y:"bottom"},
						
						sourcePath:"collaborate.png",
		
					},
				},
				
				
			
			},
			
			proto_setOppose:{
			
				usesSuper:"proto_abstract_entityAction",
			
				imageConfig:{
					_2D:{
						
						center:{x:"center",y:"bottom"},
						
						sourcePath:"oppose.png",
		
					},
				},
			},
		},
		
		
		
	},
};
