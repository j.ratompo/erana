/* ## Utility gamu methods - model part
 *
 * This set of methods gathers utility game-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils-gamu» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by its programming egos legions)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 *  
 */




//======================== Level utility methods ======================== 



// GAME LEVELS

function getLevelsManager(levelsConfig, levelsStorageKey, controller, model) {

	// DBG (REMOVE WHEN FINISHED TO ALLOW TO RESUME A PREVIOUS GAME!)
//	var DEBUG_MODE_ALWAYS_CREATE_NEW_GAME=true;

	let self={
			
		
		// Those are the levels that has been initialized and populated :
		instanciatedLevels : {},
		saveGame:function(){
			model.saverLoader.saveState(self.instanciatedLevels, levelsStorageKey);
		},
		startLevelIfNoCurrent : function(pageName,/*NULLABLE*/levelNameParam) {

			
			var levelName=levelNameParam;
			if(contains(levelNameParam,"()")){
				var levelMethodName=levelNameParam.replace("()","");
				if(!nothing(controller[levelMethodName])){
					levelName=controller[levelMethodName].apply(controller,[pageName]);
				}
			}
			
			if(model.saverLoader.hasData(levelsStorageKey)){
				self.instanciatedLevels=model.saverLoader.loadState(levelsStorageKey);
			}
			
			
			let currentLevelData=self.instanciatedLevels[levelName];
			if(!currentLevelData
			// DBG
			//|| true
			) {

				// We start a new state for this level :
				const currentLevelConfig=levelsConfig[levelName];
				currentLevelData=model.instanciationManager.createLevelState(levelName,currentLevelConfig);

				// We save the names of the current levels for each page :
				self.instanciatedLevels[levelName]=currentLevelData;

				self.saveGame();
			}
			

			// ...Else we get the saved level state if it exists  :
			model.level=currentLevelData;
			
			// We set the selection if needed :
			if(levelsConfig[levelName].selectedItems){
				var selectedItems=levelsConfig[levelName].selectedItems;
				if(contains(selectedItems,"()")){
					var methodName=selectedItems.replace("()","");
					var selectedObject=controller[methodName].apply(controller);

					// TODO : Make so it can handle multiple selection :
					// On today, only handle single-element selection  :
//					if(isArray(selectedObject))
						controller.selectionManager.setSelected(selectedObject);
//					else
//						controller.selectionManager.setSelected([selectedObject]);
				}
			}else if(levelsConfig[levelName].selectedItem){
				var selectedItem=levelsConfig[levelName].selectedItem;
				if(contains(selectedItem,"()")){
					var methodName=selectedItem.replace("()","");
					var selectedObject=controller[methodName].apply(controller);
					controller.selectionManager.setSelected(selectedObject);
				}
			}
			
			
			// We launch the level with the properly setted model :
			controller.start(); // cf. GameScreen.start() method.
			
			
		}
	};

	foreach(levelsConfig,(level,key)=>{
		self[key]={
			config : levelsConfig[key]
		};

	});

	return self;
}


// ======================== Model utility methods ======================== 

function getSaverLoader() {
	const DEFAULT_OWNER_KEY="data";

	let self= {

			// DOES NOT WORK :
//		/*private*/removeParents:function(object){
//			
//			if(!object || isPrimitive(object))	return;
//			
//			if(object.parent) {
//				object.parent=null;
//				object.hadParent=true;
//			}
//			
//			foreach(object,(obj,attrName)=>{
//				
//				// DBG
//				lognow("removeParents:"+attrName,obj);
//				
//				if(attrName=="concernedObject")	return "continue";
//				
//				if(isArray(obj)){
//					foreach(obj,(element)=>{
//						self.removeParents(element);
//					});
//				}else{
//					self.removeParents(obj);
//				}
//			});
//			
//		},
//		
//		
//		/*private*/addParents:function(object){
//			
//			if(!object || isPrimitive(object))	return;
//
//			let parent=object;
//			foreach(object,(obj,attrName)=>{
//				
//				if(attrName=="concernedObject")	return "continue";
//
//				// DBG
//				lognow("addParents:"+attrName,obj);
//
//				
//				if(isArray(obj)){
//					foreach(obj,(element)=>{
//						
//						if(element.hadParent){
//							element.parent=parent;
//						}
//					
//						
//						self.addParents(element);
//						
//					});
//				}else{
//					
//					if(obj.hadParent){
//						obj.parent=parent;
//					}
//					self.addParents(obj);
//					
//				}
//			});
//			
//		},

		saveState : function(object,/*NULLABLE*/ownerKey) {

			let key=DEFAULT_OWNER_KEY;
			if(!nothing(ownerKey)) {
				key=ownerKey;
			}

			// CURRENT
			
			
			// (JSON string)
			// DOES NOT WORK (string is too long !)
			let savedMap=getAsFlatStructure(object);
			let savedString=stringifyObject(savedMap);
			
			//DBG
			lognow("savedString:",savedString);

			savedString=LZWString.compress(savedString);

			//DBG
			lognow("COMPRESSED savedString:",savedString);

			
			// DOES NOT WORK : Stack call overflow :
//			let savedWithNoParents=self.removeParents(object);
//			let savedString=stringifyObject(savedWithNoParents);
			

			//			var savedString="{objects:[";
			//			var comma=false;
			//			for (var i=0; i < objects.length; i++) {
			//				if(comma)
			//					savedString += ",";
			//				else
			//					comma=true;
			//				var o=objects[i];
			//				savedString += stringifyObject(o);
			//			}
			//			savedString += "]}";

			storeString(key, savedString);
		},

		loadState : function(/*NULLABLE*/ownerKey) {
			var key=DEFAULT_OWNER_KEY;
			if(!nothing(ownerKey)) {
				key=ownerKey;
			}
			

			let loadedString=getStringFromStorage(key);
			if(!loadedString) return null;
			
			// (JSON string)

			// DOES NOT WORK (string is too long !)
			loadedString=LZWString.decompress(loadedString);
			
			//DBG
			lognow("loadedString:",loadedString);
			
			let modelFlat=parseJSON(loadedString);
			let model=getAsTreeStructure(modelFlat);
			
			// DOES NOT WORK : Stack call overflow :
//			let modelRaw=parseJSON(loadedString);
//			let model=self.addParents(modelRaw);


			return model;
		},

//		,restoreLevel : function(levelName, levelData) {
//			var level={};
//
//			level.levelName={};
//
//			
//			// TODO : develop...!
//
//			return level;
//		}
		
		hasData:function(/*NULLABLE*/ownerKey){
			let key=DEFAULT_OWNER_KEY;
			if(!nothing(ownerKey)) {
				key=ownerKey;
			}
			
			let loadedString=getStringFromStorage(key);
			return loadedString && loadedString.length;
		},
		
		
	};
	
	return self;
}


// MODEL INSTANCES GENERATION

///*private*/function addZonesCoordinates(zone1,zone2){
//	returun {x:nonull(zone1.x,0)+nonull(zone2.x,0), y:nonull(zone1.y,0)+nonull(zone2.y,0),
//					 w:nonull(zone1.w,nonull(zone2.w,0)), h:nonull(zone1.h,nonull(zone2.h,0)) };
//}
//
///*public*/function getSpawningZone(holder,previousHolder=null){
//	if(!previousHolder){
//		if(!holder.spawningZone.cumulative){
//			return holder.spawningZone;
//		}	else{
//			holder.calculatedCumulativeSpawningZone=holder.spawningZone;
//			return holder.spawningZone;
//		}
//	}
//
//	if(!holder.spawningZone.cumulative || !previousHolder.calculatedCumulativeSpawningZone)
//		return holder.spawningZone;
//	
//	holder.calculatedCumulativeSpawningZone=addZonesCoordinates(holder.spawningZone,previousHolder.calculatedCumulativeSpawningZone);
//	
//	return holder.calculatedCumulativeSpawningZone;
//
//}


/*public*/function getSpawningZone(holder){
	return holder.spawningZone;
}


/*public*/function getProtoNameConf(protoConf){
	if(protoConf.names) {
		return Math.getRandomInArray(protoConf.names);
	}
	let result={value:protoConf.name, spawningZone:getSpawningZone(protoConf)};
	if(protoConf.subClass)	result.subClass=protoConf.subClass;
	return result;
}


function getInstanciationManager(prototypesConfig,gameConfig) {

	const ALL_PROTOTYPES_CONFIG=prototypesConfig.allPrototypes;
	const GAME_CONFIG=gameConfig;
	const ABSTRACT_PREFIX="abstract_";

	/*private*/function getNumberToPreInstanciate(cardinality){
		
		var numberToPreInstanciate=0;

		// Ratio or interval collections cardinalities :
		// (cardinality 5/10 = An object has 5 chances out of 10 scroll events to appear)
		// (cardinality 5->10 = Objects total number on display area will always be between 5 min and 10 max on scroll events)
		// TODO :
//		if(isString(cardinality.value) && contains(cardinality.value,"/")){
//			var splits=cardinality.value.split("/");
//			var numerator=parseInt(splits[0]);
//			var denominator=parseInt(splits[1]);
//			
//			// On init, it is the same as the min max interval cardinality value
//			numberToPreInstanciate=Math.getRandomInt(denominator,numerator);
//		}else 
		if(isString(cardinality.value) && contains(cardinality.value,"->")){
			
			// On init, it is the same as the chances fraction cardinality value
			numberToPreInstanciate=Math.getRandomInRange(cardinality.value);

			
		}else if(isString(cardinality.value)){
			numberToPreInstanciate=parseInt(cardinality.value);
		}else if(isNumber(cardinality.value)){
			numberToPreInstanciate=cardinality.value;
		}
	
		return numberToPreInstanciate;
	};
	
	/*public*/function instanciateAndAddToCollection(collection, classNameParam, prototypesInstanciation,
			// If no chosen prototype parameter is passed, then it means we want it to be calculated with the prototype instantiation information only :
			chosenPrototypeParam=null
			// TODO : FIXME : On today, only in 2D !!
			,x=null,y=null){

		var chosenPrototype=chosenPrototypeParam;
		
		let className=classNameParam;
		if(chosenPrototypeParam && chosenPrototypeParam.subClass){
			className=chosenPrototypeParam.subClass;
		}
		
		
		// First OF ALL (it is prioritary on everything else) we check if we ever have to 
		// instanciate all static, fixed objects visible at this location :
		if(prototypesInstanciation.staticInstanciation){

			if(!chosenPrototype){ // Case on-move instantiation :
				
				var staticInstanciation=prototypesInstanciation.staticInstanciation;
				let foundPrototype = foreach(staticInstanciation,(staticConf)=>{
					if( staticConf.preinstanciate ||
							// TODO : FIXME : On today, only in 2D !!
							isInZone({x:x,y:y},getSpawningZone(staticConf))){
						
						let protoNameConf=getProtoNameConf(staticConf);
						return protoNameConf;
					}
				});
				
				if(foundPrototype)	chosenPrototype=foundPrototype;
			}
			
		
		}else if(prototypesInstanciation.randomInstanciation){
		
			// If we want to choose from random prototypes (ie. if we have encountered no static prototype and none is provided as parameter) :
			if(!chosenPrototype){ // Case on-move instantiation :
				// We instanciate yet another single random object among array values:
				chosenPrototype= { value:Math.getRandomInArray(prototypesInstanciation.randomInstanciation) };
			}

		}else{
			
			// Actually equivalent to {... randomInstanciation:["<a single value>"]} )
			if(!chosenPrototype){  // Case on-move instantiation :
				// We instanciate a determined object :
				chosenPrototype= { value:prototypesInstanciation };
			}
			

		}

		var item=getSingleInstanceWithPrototype(className, chosenPrototype);
		collection.push(item);
		
		
		return {item:item, zone:getSpawningZone(chosenPrototype)};
	}
	
	
	/*public*/function getSingleInstanceWithPrototype(classNameParam, chosenPrototypeParam=null,/*FOR DIRECT CLASSES DEFINITIONS ONLY :*/subPrototypeConfigParam=null){
		
		let className=classNameParam ;

		if(chosenPrototypeParam && chosenPrototypeParam.subClass){
			className=chosenPrototypeParam.subClass;
		}

		
		// DOES NOT WORK : var instance= Reflect.construct(className,[],new.target);
		
		// TODO : FIXME : POTENTIAL VULNERABILITY !:
		var instance=eval("new "+className+"()");

		
		var prototypeConfig=subPrototypeConfigParam;
		if(!prototypeConfig){
			prototypeConfig=ALL_PROTOTYPES_CONFIG[className][chosenPrototypeParam.value];
		}
		
		
		// We loop on the attributes of the prototype config :
		foreach(prototypeConfig,(attribute,key)=>{
			
			
			if(!startsWithUpperCase(key)){
				
				// Case plain attribute :

				var attributeName=key;
				if(attributeName==="usesSuper"){

					// If we use the super(parent),
					// then that means this instance prototype is basically its parent prototype :
					
					var prototypedParentObject=getSingleInstanceWithPrototype(className, {value:attribute} );

					instance=prototypedParentObject;
					instance.usesSuper=attribute;
					
					
				}else{
					
					// simple attribute :
					if(isString(attribute)){
						if(contains(attribute,"->")){
							// If we have a numeric range
							instance[attributeName]=Math.getRandomInRange(attribute);
						}else if(contains(attribute,"=>")){
							// If we have a string range
							instance[attributeName]=Math.getMinMax(attribute,"=>");
						}else{ // Simple string :
							instance[attributeName]=attribute;
						}
					}else if(isNumber(parseFloat(attribute))){
						instance[attributeName]=parseFloat(attribute);
					}else if(!nothing(attribute.random)){
						// If we have an array as attribute, we choose one among all the values it holds :
						instance[attributeName]=Math.getRandomInArray(attribute.values);
					
					
					}else if(typeof(attribute)==="object" && !isArray(attribute)){ // Plain objects (only)...:

						// We have to clone the prototype simple object attribute :
						let clonedAttribute=cloneObject(attribute,true);
						instance[attributeName]=clonedAttribute;

						
					}else{ // Primitives, strings, arrays...
						
						instance[attributeName]=attribute;
						
					}

				}
				
			}else{
				
				// Case Class config attribute :
				
				var attributeName=uncapitalize(key);

				
				// If we have an array of prototypes names :
				let prototypedSubObject=null;
				
				//if(!nothing(attribute.cardinality)){
				if(attribute.cardinality){
				
					let prototypeClassName=key;
					var prototypesConfigsObject=attribute;
					var cardinality=prototypesConfigsObject.cardinality;
					
					if(isCardinalityCollection(cardinality))	attributeName+="s";
					
					// Sub-object attribute:
					// Here, attributes holds the prototypes names array or the prototype single value
					prototypedSubObject=getInstancesWithPrototypesConfig(instance,attributeName,prototypeClassName,prototypesConfigsObject);
					
					
				}else if(isArray(attribute)){ // Case we have an array of direct class definitions :
					
					attributeName+="s";

					let subPrototypeClassName=key;
					let subPrototypeConfigs=prototypeConfig[subPrototypeClassName];
					
					prototypedSubObject=[];
					foreach(subPrototypeConfigs,(subPrototypeConfig)=>{
						let item=getSingleInstanceWithPrototype(subPrototypeClassName, null, subPrototypeConfig);
						prototypedSubObject.push(item);
					});
					
					
				}else{ // Else if we have a single direct class definition :
					
					var subPrototypeClassName=key;
					
					var subPrototypeConfig=prototypeConfig[subPrototypeClassName];
					prototypedSubObject=getSingleInstanceWithPrototype(subPrototypeClassName, null, subPrototypeConfig);

				}
				
				// If we should, then we create a link to the parent :
				if(GAME_CONFIG.allowParentLinkForChildren){
//			if(prototypeConfig.allowParentLinkForChildren){
					if(!isArray(prototypedSubObject)){
						prototypedSubObject.parent=instance;

						// Abstract prototyped object are NEVER initialized :
						if(prototypedSubObject.initWithParent && !contains(prototypedSubObject.prototypeName,ABSTRACT_PREFIX)){
							prototypedSubObject.initWithParent();
						}

						
						
					}else{
						foreach(prototypedSubObject,(subItem)=>{
							subItem.parent=instance;

							// Abstract prototyped object are NEVER initialized :
							if(subItem.initWithParent && !contains(subItem.prototypeName,ABSTRACT_PREFIX)){
								subItem.initWithParent();
							}

							

						});
					}
				}
				
				
				instance[attributeName]=prototypedSubObject;
			}
			
		});

		
		if(!nothing(chosenPrototypeParam) && !nothing(chosenPrototypeParam.value)){
			instance.prototypeName=chosenPrototypeParam.value;
		}
		
		// Abstract prototyped object are NEVER initialized :
		
		if(instance.init && (!chosenPrototypeParam/*(case no prototype initialization)*/ || !contains(chosenPrototypeParam.value,ABSTRACT_PREFIX))){
			instance.init(GAME_CONFIG);
		}
		
		return instance;
	};
	
	
	// For prototypes instantiation only :
	function getInstancesWithPrototypesConfig(parentObject, attributeName, classNameParam, prototypesConfigsObject){
		
		let className=classNameParam;
		// The «subClass» eventual attribute in prototype class config overrides the trivial className : 
		if(prototypesConfigsObject && prototypesConfigsObject.subClass){
			className=prototypesConfigsObject.subClass;
		}

		
		var cardinality=prototypesConfigsObject.cardinality;
		var instanceSingleObjectOrCollection=null;
		let numberToPreInstanciate=0;

		
		
		
		if(!isCardinalityCollection(cardinality)){ // case SINGLE ATTRIBUTE :
			
			// UNUSEFUL (but true) : 
			// numberToPreInstanciate=1;
			
			// NO : WE DON'T PLACE THEM YET
//		let spawningLocation={x:0,y:0};
			
			// We calculate here the chosen prototype to instanciate if we have no collection :
			let chosenPrototype=null;
			if(cardinality.prototypesInstanciation.staticInstanciation){
				if(isArray(cardinality.prototypesInstanciation.staticInstanciation)){
					if(!empty(cardinality.prototypesInstanciation.staticInstanciation)){
						// We look into the first (and only) config element :
						let conf=cardinality.prototypesInstanciation.staticInstanciation[0];
						
						let protoNameConf=getProtoNameConf(conf);
						
						chosenPrototype=protoNameConf;
						// CAUTION : WE DON'T PLACE THEM YET
						// SO NO :	if(conf.spawningZone)		spawningLocation=getPositionFor2DObject(conf.spawningZone);
					}
				}else{
					let conf=cardinality.prototypesInstanciation.staticInstanciation;
					
					let protoNameConf=getProtoNameConf(conf);

					chosenPrototype=protoNameConf;
					// CAUTION : WE DON'T PLACE THEM YET
					// SO NO :	if(conf.spawningZone)		spawningLocation=getPositionFor2DObject(conf.spawningZone);
				}
				
			}	else if(cardinality.prototypesInstanciation.randomInstanciation && !empty(cardinality.prototypesInstanciation.randomInstanciation)){
				chosenPrototype={ value:Math.getRandomInArray(cardinality.prototypesInstanciation.randomInstanciation) };
			} else if(isString(cardinality.prototypesInstanciation)){
				chosenPrototype={ value:cardinality.prototypesInstanciation };
			}
			
			if(chosenPrototype){

				// (we don't place them yet : cf. gamu-scenery module for further placing)
				
				instanceSingleObjectOrCollection=getSingleInstanceWithPrototype(className, chosenPrototype);
								
				// We set the cardinalities information for this single attribute :
				if(!parentObject.cardinalities)	parentObject.cardinalities={};
				parentObject.cardinalities[attributeName]=cardinality;


				// NO : WE DON'T PLACE THEM YET
//				if(instanceSingleObjectOrCollection.position) {
//					instanceSingleObjectOrCollection.position.setLocation(spawningLocation);
//					if(parentObject.position)	instanceSingleObjectOrCollection.position.setParentPosition(parentObject.position);
//				}
				
			}
			
			
		}else{ // case COLLECTION ATTRIBUTE :
			
			
			/* !!! CAUTION !!!  
			 * WHEN WE ARE NOT IN «preinstanciate» MODE, 
			 * «cardinality» REPRESENTS THE NUMBER OF ELEMENTS AT THE SAME TIME INSTANCIATED IN *DISPLAY AREA* !!!
			 * WHEN WE ARE IN «preinstanciate» MODE, 
			 * IT REPRESENT THE *TOTAL* AMOUNT OF INSTANCIATED OBJECTS IN THE WHOLE MODEL !!!
			 */
			
			
			// We set the cardinalities information for each collection attribute :
			if(!parentObject.cardinalities)	parentObject.cardinalities={};
			parentObject.cardinalities[attributeName]=cardinality;

			
			if(cardinality.value==="fill"){
				
				instanceSingleObjectOrCollection=[];
				const MAX=999;
				
				let zone = getSpawningZone(cardinality);

				if(contains(GAME_CONFIG.projection, "2D")) {
					if(contains(GAME_CONFIG.scroll, "horizontal")) {
						
						// 0- We instanciate just enough to fill the width :
						// (we don't place them yet : cf. gamu-scenery module for further placing)
						var totalWidth=Math.floor( GAME_CONFIG.getResolution().w / GAME_CONFIG.zoom );
			
						let fillInstanciationMargin= Math.floor( nonull(cardinality.fillInstanciationMargin,0) / GAME_CONFIG.zoom );
						
						var widthCount=0;
						for(var i=0;i<MAX && widthCount < totalWidth + fillInstanciationMargin;i++){

							// We instanciate the object randomly :
							let itemAndZone=instanciateAndAddToCollection(instanceSingleObjectOrCollection,className,cardinality.prototypesInstanciation);
							let elementInstance=itemAndZone.item;
							if(!zone)	zone=elementInstance.zone;
							
							widthCount+= (elementInstance.size.w + (zone?nonull(zone.margin,0):0))  ;
							
						}
						
					}
				}
				
			}else if(cardinality.value==="staticGroups"){ // Static groups cardinality :
				
				
				instanceSingleObjectOrCollection=[];
				
				// (we don't place them yet : cf. gamu-scenery module for further placing)
				let protos=cardinality.prototypesInstanciation;
				if(protos.staticInstanciation){
					
					foreach(protos.staticInstanciation,(protoConf)=>{
						
						let protoNameConf=getProtoNameConf(protoConf);

						// We calculate how many objects we have to preinstanciate in this collection :
						numberToPreInstanciate=1;
						if(protoConf.preinstanciate && protoConf.value)
							numberToPreInstanciate=getNumberToPreInstanciate(protoConf);
						
						for(let i=0;i<numberToPreInstanciate;i++){
							instanciateAndAddToCollection(instanceSingleObjectOrCollection,className,cardinality.prototypesInstanciation,protoNameConf);
						}
						
					});

					
				}
				
				
			}else if(cardinality.value==="randomGroups"){ // Random groups cardinality :
				
				
				instanceSingleObjectOrCollection=[];
				
				// (we don't place them yet : cf. gamu-scenery module for further placing)
				let protos=cardinality.prototypesInstanciation;
				if(protos.randomInstanciation){
					
					let protoConf=Math.getRandomInArray(protos.randomInstanciation);
					
					let protoNameConf=getProtoNameConf(protoConf);

					// We calculate how many objects we have to preinstanciate in this collection :
					numberToPreInstanciate=1;
					if(protoConf.preinstanciate && protoConf.value)
						numberToPreInstanciate=getNumberToPreInstanciate(protoConf);
					
					for(let i=0;i<numberToPreInstanciate;i++){
						instanciateAndAddToCollection(instanceSingleObjectOrCollection,className,cardinality.prototypesInstanciation,protoNameConf);
					}
					
					
					
				}
				
				
			}else{ // Ratio or interval collections cardinalities :
				
				
				instanceSingleObjectOrCollection=[];
				let numberToPreInstanciate=getNumberToPreInstanciate(cardinality);
				// We instanciate the objects randomly or not according if we have to :
				for(var i=0;i<numberToPreInstanciate;i++){
					// If no chosen prototype parameter is passed, then it means we want it to be calculated with the prototype instantiation information !
					instanciateAndAddToCollection(instanceSingleObjectOrCollection, className, cardinality.prototypesInstanciation);							
				}
				
			}
			

		}
		
		return instanceSingleObjectOrCollection;
	};
	



	
	

	
	/*private*/function isCardinalityCollection(cardinality){
		var isCollection=true;
		let val=cardinality.value;
		if((val==="1" || val===1 ) && val!=="staticGroups" && val!=="randomGroups"){
			isCollection=false;
		}
		return isCollection;
	};

	// ---------------------------------------------------
	
	

	
	// The instantiation manager object instance :
	let self= {
		getSingleInstanceWithPrototype:getSingleInstanceWithPrototype,
		instanciateAndAddToCollection:instanciateAndAddToCollection,
		createLevelState : function(levelName,levelConfig) {
			
			// We store the level config inside the level :
			var level={
						config: levelConfig,
						isCinematic: levelConfig.isCinematic, 
						limits: levelConfig.limits
			};

			level.levelName=levelName;

			
			foreach(levelConfig,(configItem,className)=>{

				if(!startsWithUpperCase(className))	return "continue";
				
				
				// We get the prototype for this root class name :
				let prototypesConfigObject=levelConfig[className];
				let cardinality=prototypesConfigObject.cardinality;

				let attributeName=uncapitalize(className);
				if(isCardinalityCollection(cardinality))	attributeName+="s";
				
				level[attributeName]=getInstancesWithPrototypesConfig(level,attributeName,className, prototypesConfigObject,ALL_PROTOTYPES_CONFIG);
				
			});
			
			return level;
		}
	};
	
	
	return self;
	

}

