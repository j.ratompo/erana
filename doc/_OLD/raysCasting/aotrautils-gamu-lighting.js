/* ## Utility gamu methods - view part : Lighting part
 *
 * This set of methods gathers utility game-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils-gamu» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 *  
 */


// ======================== View : Lighting utility methods ========================

// LIGHTING


/*private*/function getLightsManager(gameConfig){
	
	if(!gameConfig.lighting || (!gameConfig.lighting.dark && !gameConfig.lighting.bump))	return null;
	
	const PRECISION=2;// Magic number. No other value seems to work!
	const BUMP_SHADOW_GRAY_VALUE=64;
	const BUMP_SHADOW_WHITE_VALUE=255;
	
	var self={
			
			
			gameConfig:gameConfig,
			allLights:[],
			tmpCtx:document.createElement("canvas").getContext("2d"),
			lightPercent:100,
			setLightsPercent:function(lightPercent){
				self.lightPercent=lightPercent;
			},
			getLightsPercent:function(){
				return self.lightPercent;
			},
			registerLights:function(item){
				
				if(!item.imageConfig)	return;
				
				var projection = self.gameConfig.projection;
				var scroll = self.gameConfig.scroll;
				if(contains(projection, "2D")) {
						if(contains(scroll, "horizontal")) {
							
							let lights=item.imageConfig._2D.lights;
							if(lights){

								foreach(lights,(l)=>{
									
									let light=cloneObject(l)
									
									let pos=item.position;
									light.x+=pos.x;
									light.y+=pos.y;
									
									self.allLights.push(light);
									
								});


							}

							
						}
				}
				
			},
			
			
			/*private*//*treatPixel:function(i,rgba,drawingWidth,light,camera,realImageCoords,rays,lightMode,startAngleRadians,endAngleRadians){
				
				let rIndex=i;
				let gIndex=i+1;
				let bIndex=i+2;
				let aIndex=i+3;
				
				
			  let r = rgba[rIndex];
			  let g = rgba[gIndex];
			  let b = rgba[bIndex];
			  let a = rgba[aIndex];
			  
			  
			  // We skip all the completly transparent pixels :
			 	if(	a===0 ) return null;
			  
				// For the bumping algorithm :
	  		// We DO NOT skip all the gray pixels (ie. those already treated by a previous light source) :
			 	// because if we do then we won't be able to compose multiple light sources : 
			 	// NO: if(lightMode==="bump" && r===128 && g===128 && b===128) 	continue;
			  
			  
			  let index=(i/4);
			  
			  let imageCoords=getCenteredCoords(realImageCoords.x,realImageCoords.y,realImageCoords.w,realImageCoords.h,realImageCoords.center);
			  
			  let px=( (index%drawingWidth) + imageCoords.x);
			  let py=( Math.floor(index/drawingWidth) + imageCoords.y);
			   
				let drawableLightCoordinates=getDrawableCoordinates(self.gameConfig,light,camera);
				let drawablePixelCoordinates={x:px*realImageCoords.zoom,y:py*realImageCoords.zoom};
				
				// This calculus is costly (but necessary...!) :
			  let angle=coerceAngle(
			  				 		Math.TAU-calculateAngleRadians(
										  			{x:drawableLightCoordinates.x,y:drawableLightCoordinates.y},
										  			{x:drawablePixelCoordinates.x,y:drawablePixelCoordinates.y}
										  		,true),
									true).toFixed(PRECISION);
			  
			  
			  
			  // TODO : FOR NOW the light.radius parameter is ignored :
			  
			  // WORKS FOR BEAM LIGHTS:
			  // if(!( (drawableLightCoordinates.x-10)<px &&
			  // px<(drawableLightCoordinates.x+10) )) continue;

			  	
		  	// Accepted pixels:
			  if(startAngleRadians<angle && angle<endAngleRadians) {
			  	

			  	if(lightMode==="bump"){
				  	
			  		
						// For the bumping algorithm :
						let angleKey=angle.toString();
						
						let closestPixelForRay=rays[angleKey];
						
						let distanceSquared=
							 Math.pow(drawableLightCoordinates.x-drawablePixelCoordinates.x,2)
							+Math.pow(drawableLightCoordinates.y-drawablePixelCoordinates.y,2);
						
						let pixelInfo={index:i,dist:distanceSquared};
						
						// OLD UNOPTIMIZED WAY, BUT PLEASE KEEP CODE !
						//// We store the indexes in increasing distance order :
						//addInArrayInIncreasingOrder(pixelsForRay,pixelInfo,(item)=>{ return item.dist;} );

						// We store the pixel in the ray with the smallest distance :
						if(!closestPixelForRay || pixelInfo.dist<closestPixelForRay.dist){
							rays[angleKey]=pixelInfo;
						}
				  	
				  	// We set the accepted pixels in gray to tell the next light sources to ignore them :
				  	rgba[rIndex]=BUMP_SHADOW_GRAY_VALUE;
				  	rgba[gIndex]=BUMP_SHADOW_GRAY_VALUE;
				  	rgba[bIndex]=BUMP_SHADOW_GRAY_VALUE;
				  	

				  	
				  }else{ // «dark» case :
					  
				  	
				  	// Accepted pixels:
					  // We make this accepted pixel transparent :
					  rgba[aIndex]=0;
					  
					  
						  
				  }
			  	
			  	
			  	return {index:i};
			  }
			  	
			  // Rejected pixels:
			  
			  // We leave the rejected pixels alone (ie. we let them stay black as in the original image),
			  // to allow another next light source to be able to treat them :

		  	return null;
			},*/
			
			/*private*//*raysCastForAllVisibleLights:function(rgba,camera,realImageCoords,drawingWidth,drawingHeight,lightMode="dark",acceptedPixelsParam=null){
				
				
				let raysSetsForLights=null;
				if(lightMode==="bump")	raysSetsForLights=[];
				
				
				if(!acceptedPixelsParam)				acceptedPixels=[];
				let resolution=self.gameConfig.getResolution();
				// 1) We check which of the lights are visible in screen :
				foreach(self.allLights,(light)=>{
					
					
					let startAngleRadians=Math.toRadians(coerceAngle(light.angle-light.arc/2),true);
					let endAngleRadians=Math.toRadians(coerceAngle(light.angle+light.arc/2),true);
					// We swap angles if they are in the wrong order :
					if(endAngleRadians<startAngleRadians){
						let tmp=endAngleRadians;
						endAngleRadians=startAngleRadians;
						startAngleRadians=tmp;
					}
					
						
					// For the bumping algorithm only :
					let rays;
					if(lightMode==="bump") {
						rays={};
						raysSetsForLights.push(rays);
					}
					
					if(!acceptedPixelsParam){
						
						for(var i=0; i<drawingWidth*drawingHeight*4; i+=4){
							let p=self.treatPixel(i,rgba,drawingWidth,light,camera,realImageCoords,rays,lightMode,startAngleRadians,endAngleRadians);
							if(p)		acceptedPixels.push(p);
						}
						
					}else{
						
						foreach(acceptedPixels,(acceptedPixel)=>{
							self.treatPixel(acceptedPixel.index,rgba,drawingWidth,light,camera,realImageCoords,rays,lightMode,startAngleRadians,endAngleRadians);
						});
						
					}

					
				},(l)=>{
					// CAUTION ! Coordinates for DRAWING are not the same than for CALCULUS !
					return isInVisibilityZone(self.gameConfig,camera,l);
				});
				
				
				return {rays:raysSetsForLights,pixels:acceptedPixels};
				
			},*/
			
			/* private *//*useImage:function(ctx,camera,realImageCoords,imageToUse,lightMode="dark",acceptedPixels=null){
				
				if(!imageToUse.width || !imageToUse.height)	return;

				let tmpCtx=self.tmpCtx;
				if(imageToUse.width!==tmpCtx.canvas.width || imageToUse.height!==tmpCtx.canvas.height){
					tmpCtx.canvas.width=imageToUse.width;
					tmpCtx.canvas.height=imageToUse.height;
				}
				
				tmpCtx.clearRect(0,0,imageToUse.width,imageToUse.height);
	
				
				// We only use the pixels of the scaled image :
				drawImage(tmpCtx,imageToUse,0,0,null,realImageCoords.scaleW*realImageCoords.zoom,realImageCoords.scaleH*realImageCoords.zoom);
				let drawingWidth= imageToUse.width*realImageCoords.scaleW*realImageCoords.zoom;
				let drawingHeight=imageToUse.height*realImageCoords.scaleH*realImageCoords.zoom;
				let imgData=tmpCtx.getImageData(0,0,drawingWidth,drawingHeight);
				var rgba = imgData.data;
				
				
				
				// We cast the rays :
				let raysSetsForLights=null;
				if(		 (lightMode==="bump" && self.gameConfig.lighting.bump) 
						|| (lightMode==="dark" && self.gameConfig.lighting.dark && self.gameConfig.lighting.darkRaycast===true))
					raysSetsForLights=self.raysCastForAllVisibleLights(rgba,camera,realImageCoords,drawingWidth,drawingHeight,lightMode,acceptedPixels);
				
				
				// (if lightMode is «dark» then raysSetsForLights must be null !)
				
				// For the bumping algorithm only :
				if(lightMode==="bump"){
					
					// For each light ray :
					foreach(raysSetsForLights.rays,(raysSetsForOneLight)=>{
						
						// We only store the closest pixel to light :
						foreach(raysSetsForOneLight,(closestPixel)=>{
							// This pixel is the closest, so it must be lit :
							let i=closestPixel.index;
							let rIndex=i;
							let gIndex=i+1;
							let bIndex=i+2;
							let aIndex=i+3;
							
							rgba[rIndex]=BUMP_SHADOW_WHITE_VALUE;
							rgba[gIndex]=BUMP_SHADOW_WHITE_VALUE;
							rgba[bIndex]=BUMP_SHADOW_WHITE_VALUE;
							
//						TODO : FIXME :
//						// We also highlight the surrounding pixels :
//						rgba[rIndex+4]=BUMP_SHADOW_WHITE_VALUE;
//						rgba[gIndex+4]=BUMP_SHADOW_WHITE_VALUE;
//						rgba[bIndex+4]=BUMP_SHADOW_WHITE_VALUE;
//						
//						rgba[rIndex-4]=BUMP_SHADOW_WHITE_VALUE;
//						rgba[gIndex-4]=BUMP_SHADOW_WHITE_VALUE;
//						rgba[bIndex-4]=BUMP_SHADOW_WHITE_VALUE;
							
							
						});
						
						
					});
					
				}
	
				
				let centeredCoords=getCenteredCoords(
						realImageCoords.x,realImageCoords.y,
						drawingWidth,drawingHeight,
						realImageCoords.center);

				
				ctx.save();
				
				ctx.globalAlpha=(1-(self.lightPercent/100)).toFixed(2);
				
				
				tmpCtx.putImageData(imgData,0,0);
				
				ctx.drawImage(tmpCtx.canvas,
						// Clip source :
						0, 0,
						drawingWidth, drawingHeight,
						// Drawing destination (including scaling) :
						centeredCoords.x, centeredCoords.y,
						drawingWidth, drawingHeight);
			

				ctx.restore();
				
				
				return raysSetsForLights?raysSetsForLights.pixels:null;
				
			},*/
			
			
//			// NOT WORKING :
//			/*private*/addClipsToImageDark(ctx,camera,concernedObject,realImageCoords,imageToUse){
//				
//				if(!imageToUse.width || !imageToUse.height)	return;
//
//				let drawingWidth= imageToUse.width*realImageCoords.scaleW*realImageCoords.zoom;
//				let drawingHeight=imageToUse.height*realImageCoords.scaleH*realImageCoords.zoom;
//
//				
//				let tmpCtx=self.tmpCtx;
////			if(imageToUse.width!==tmpCtx.canvas.width || imageToUse.height!==tmpCtx.canvas.height){
//				if(drawingWidth!==tmpCtx.canvas.width || drawingHeight!==tmpCtx.canvas.height){
////				tmpCtx.canvas.width=imageToUse.width;
////				tmpCtx.canvas.height=imageToUse.height;
//					tmpCtx.canvas.width=drawingWidth;
//					tmpCtx.canvas.height=drawingHeight;
//				}
//				tmpCtx.clearRect(0,0,tmpCtx.canvas.width,tmpCtx.canvas.height);
//
//				
//				let centeredCoords=getCenteredCoords(
//						realImageCoords.x,realImageCoords.y,
//						drawingWidth,drawingHeight,
//						realImageCoords.center);
//				
//				
//				
//				tmpCtx.save();
//
//				// default is tmpCtx.globalCompositeOperation = "source-over";
//				
/				
//				tmpCtx.drawImage(imageToUse,
//						// Clip source :
//						0, 0,
//						imageToUse.width, imageToUse.height,
//						// Drawing destination (including scaling) :
//						0, 0,
//						drawingWidth, drawingHeight,
//				);
//				
//				tmpCtx.restore();
//				
//				
//				
//				
//				ctx.save();
//
//				let opacity=(1-(self.lightPercent/100)).toFixed(2);
//				ctx.globalAlpha=opacity;
//				
//				ctx.globalCompositeOperation = "source-atop";
//				
//				ctx.drawImage(tmpCtx.canvas,
//						// Clip source :
//						0, 0,
//						drawingWidth, drawingHeight,
//						// Drawing destination (including scaling) :
//						centeredCoords.x, centeredCoords.y,
//						drawingWidth, drawingHeight);
//				
//				
//				ctx.drawImage(imageToUse,
//						// Clip source :
//						0, 0,
//						imageToUse.width, imageToUse.height,
//						// Drawing destination (including scaling) :
//						centeredCoords.x, centeredCoords.y,
//						drawingWidth, drawingHeight,
//				);
//				
//				ctx.restore();
//
//				
//				
//				
//			},
			
			
			

			/*private*/addClipsToImageDark(ctx,camera,concernedObject,realImageCoords,imageToUse,normalImage=null,ignoreRaysCasting=false){
				if(!imageToUse.width || !imageToUse.height)	return;

				let drawingWidth=imageToUse.width*realImageCoords.scaleW*realImageCoords.zoom;
				let drawingHeight=imageToUse.height*realImageCoords.scaleH*realImageCoords.zoom;
				
				let centeredCoords=getCenteredCoords(
						realImageCoords.x,realImageCoords.y,
						drawingWidth,drawingHeight,
						realImageCoords.center);
				
				ctx.save();

				let opacity=(1-(self.lightPercent/100)).toFixed(2);
				ctx.globalAlpha=opacity;
				
				ctx.drawImage(imageToUse,
						// Clip source :
						0, 0,
						imageToUse.width, imageToUse.height,
						// Drawing destination (including scaling) :
						centeredCoords.x, centeredCoords.y,
						drawingWidth, drawingHeight,
				);
				
				ctx.restore();

				
				if(!ignoreRaysCasting){

					// We re-draw a chunk in the clip of the regular image,
					// because on today (01/2020), inverted clipping is not supporterd in html5 canvas:
					let hasLightVisible=
						foreach(self.allLights,(light)=>{
							return true;
						},(light)=>{
							// CAUTION ! Coordinates for DRAWING are not the same than for CALCULUS !
							return isInVisibilityZone(self.gameConfig,camera,light);
					}) || false;
					if(normalImage && hasLightVisible)
						self.addClipsToImageBump(ctx,camera,concernedObject,realImageCoords,normalImage);
				
				}
				
				
			},
			
			
			/*private*/addClipsToImageBump(ctx,camera,concernedObject,realImageCoords,imageToUse){
				if(!imageToUse.width || !imageToUse.height)	return;

				let drawingWidth=imageToUse.width*realImageCoords.scaleW*realImageCoords.zoom;
				let drawingHeight=imageToUse.height*realImageCoords.scaleH*realImageCoords.zoom;
				
				let centeredCoords=getCenteredCoords(
						realImageCoords.x,realImageCoords.y,
						drawingWidth,drawingHeight,
						realImageCoords.center);
				
				
//				ctx.save();
//				// default is ctx.globalCompositeOperation = "source-over";
////				ctx.globalCompositeOperation = "source-atop";
//				
//				let opacity=(1-(self.lightPercent/100)).toFixed(2);
//				ctx.globalAlpha=opacity;
//				ctx.drawImage(imageToUse,
//						// Clip source :
//						0, 0,
//						imageToUse.width, imageToUse.height,
//						// Drawing destination (including scaling) :
//						centeredCoords.x, centeredCoords.y,
//						drawingWidth, drawingHeight,
//				);
//				ctx.restore();


			let opacity=(1-(self.lightPercent/100)).toFixed(2);


			// 1) We check which of the lights are visible in screen :
			foreach(self.allLights,(light)=>{

				let radius=light.radius;
				
				// We have to invert the angle, because the y axis is inverted !
				let lightAngle=-light.angle;
				let startAngleRadians=Math.toRadians(coerceAngle(lightAngle-light.arc/2),true);
				let endAngleRadians=Math.toRadians(coerceAngle(lightAngle+light.arc/2),true);

				// We swap angles if they are in the wrong order :
				if(endAngleRadians<startAngleRadians){
					let tmp=endAngleRadians;
					endAngleRadians=startAngleRadians;
					startAngleRadians=tmp;
				}

				let drawableLightCoordinates=getDrawableCoordinates(self.gameConfig,light,camera);
			
				// CURRENT
				// TODO : Add an arbitrary zoom factor on the light shape, to give a deepness 3D effect ! (related to z-index ?)
				if(concernedObject.position.zIndex /*(includes case ==0 !)*/ && concernedObject.zIndex!==1){
					startAngleRadians*=(1/concernedObject.position.zIndex);
					endAngleRadians*=(1/concernedObject.position.zIndex);
				}
				
				
				let x=drawableLightCoordinates.x*realImageCoords.zoom;
				let y=drawableLightCoordinates.y*realImageCoords.zoom;
				
				
				// DBG
				if(endAngleRadians<startAngleRadians)
					console.log("MAUVAIS ORDRE : "+startAngleRadians+":"+endAngleRadians);
				
				
				
				ctx.save();

				// default is ctx.globalCompositeOperation = "source-over";
//				ctx.globalCompositeOperation = "source-atop";
				
				ctx.globalAlpha=opacity;
								
				ctx.beginPath();
				ctx.lineTo(x, y);
				ctx.arc(x, y, radius, startAngleRadians, endAngleRadians);
				ctx.closePath();
				ctx.clip();

				
				// DBG
				ctx.drawImage(imageToUse,
						// Clip source :
						0, 0,
						imageToUse.width, imageToUse.height,
						// Drawing destination (including scaling) :
						centeredCoords.x, centeredCoords.y,
						drawingWidth, drawingHeight,
				);
				
				ctx.restore();

				
			},(light)=>{
				// CAUTION ! Coordinates for DRAWING are not the same than for CALCULUS !
				return isInVisibilityZone(self.gameConfig,camera,light);
			});
			
			
				
			},
			
			
			
			
			
			
		/* public */drawLightsIfNeeded:function(ctx,camera,realImageCoords,concernedObject,darkImage=null,bumpImage=null,normalImage=null,ignoreRaysCasting=false){
				
				
				let tmpCtx=self.tmpCtx;
				if(!tmpCtx){
					// TRACE
					console.log("ERROR : Could not create temporary graphical context, aborting lights drawing.");
					return;
				}
				
			// OLD WAY :
//			let acceptedPixels=null;
				if(darkImage && self.gameConfig.lighting.dark && self.lightPercent<100){
					// OLD WAY :
//				acceptedPixels=self.useImage(ctx,camera,realImageCoords,darkImage,"dark",null);
					// OPTIMIZED WAY :
					self.addClipsToImageDark(ctx,camera,concernedObject,realImageCoords,darkImage,normalImage,ignoreRaysCasting);
				}
				
				if(bumpImage && self.gameConfig.lighting.bump && self.lightPercent<100 && !ignoreRaysCasting){
					// OLD WAY :
//				self.useImage(ctx,camera,realImageCoords,bumpImage,"bump",acceptedPixels);
					// OPTIMIZED WAY :
					self.addClipsToImageBump(ctx,camera,concernedObject,realImageCoords,bumpImage);
					
				}
				
				
		},
	
	};
	
	
	return self;

}
