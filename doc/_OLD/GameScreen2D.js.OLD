
const IS_DEBUG=getURLParameter("debug")=="true";


const TIME_FACTOR_UI_X=6;
const TIME_FACTOR_UI_Y=15;
	

class GameScreen2D{

	constructor(mainId,config,storageKey) {

		// MODEL
		this.storageKey=storageKey;
		this.currentLevel=null;
		this.currentContainer=null;
		this.prototypesConfig=config.prototypesConfig;
		
		// VIEW
		this.mainLoop=null;
		this.mainId=mainId;
		this.backgroundLayers=[];
		this.lockDrawing=false;
		
		// CONTROLLER
		this.flowConfig = config.flowConfig;
		this.gameConfig=config.gameConfig;


		// INIT ALL
		
		// MODEL
		this.saverLoader = getSaverLoader();
		this.instantiationManager = getInstantiationManager(this.prototypesConfig,this.gameConfig);

		
		// VIEW
		this.controlsManager=getControlsManager(this.gameConfig,this,this,this.mainId); // CAUTION : Here, The controller and the view is the same object.
		this.pagesManager=getPagesManager(this.gameConfig,this.flowConfig,this.controlsManager,this,this,this.mainId);

		
		this.tmpCanvas=document.createElement("canvas");
		this.tmpCtx=this.tmpCanvas.getContext("2d");

		
		// CONTROLLER
		this.levelsManager=getLevelsManager(this.prototypesConfig, this.storageKey, this, this); // CAUTION : Here, The controller and the model is the same object.
		this.flowManager=getFlowManager(this.gameConfig,this.flowConfig, this, this); // CAUTION : Here, The controller and the view is the same object.
		this.selectionManager=getSelectionManager(this.gameConfig,this.controlsManager);
		
		
		// TECHNICAL
//		this.sounds=[]; // TODO : Merge into this.startables
		this.startables=[];

	}
	
	
	/*private*/areRootControlsVisible(){
		let currentPage=this.pagesManager.getCurrentPage();
		if(!currentPage || !currentPage.config)	return true;
		return (currentPage.config.hasControls===true && !currentPage.config.hideControls);
	}

	/*protected*/initOnStart() {
		
		var gameConfig=this.gameConfig;
		var backgrounds=this.getCurrentBackgrounds(gameConfig);
		
		const rootContainer=this.getSubclassRootContainer();
		
		let self=this;
		this.sceneManager=getSceneManager(gameConfig, backgrounds, this, this, this.mainId, 
				(concernedCamera)=>{ /*concernedCamera and sceneManager.camera are supposed to be the same here*/ 
					self.onCameraMove(rootContainer); 
				});
		
		
		if(this.controlsManager)	this.controlsManager.clearClickables();
		this.sceneManager.placeAllOnInit(rootContainer);
		
		
		// Time factor buttons :
		let buttonDecreaseTimeFactor={
			visible:true,
			position:null,
			getPosition:function(){	return buttonDecreaseTimeFactor.position; },
			size:{w:16,h:16},
			doOnClick:function(){ 
				if(!self.gameConfig.canChangeDurationTimeFactor)	return;
				if(self.gameConfig.getDurationTimeFactor()<self.gameConfig.maxDurationTimeFactor)	self.gameConfig.setDurationTimeFactor(self.gameConfig.getDurationTimeFactor()*2);
			},
			isClickable:()=>{	return true;	},
			isFixed:true,
			setIsVisible:function(visible){		buttonDecreaseTimeFactor.visible=visible;	}, 
			getIsVisible:function(){	return buttonDecreaseTimeFactor.visible;		},
			glyph:LEFT_ARROW_GLYPH,
		};
		this.buttonDecreaseTimeFactor=this.controlsManager.registerClickable(buttonDecreaseTimeFactor);
		this.buttonDecreaseTimeFactor.position=new Position(this.buttonDecreaseTimeFactor,TIME_FACTOR_UI_X-6,TIME_FACTOR_UI_Y+15,0,{x:"left",y:"bottom"});
		
		
		let buttonIncreaseTimeFactor={
				visible:true,
				position:null,
				getPosition:function(){	return buttonIncreaseTimeFactor.position; },
				size:{w:16,h:16},
				doOnClick:function(){
					if(!self.gameConfig.canChangeDurationTimeFactor)	return;
					if(self.gameConfig.minDurationTimeFactor<self.gameConfig.getDurationTimeFactor())	self.gameConfig.setDurationTimeFactor(self.gameConfig.getDurationTimeFactor()/2);
				},
				isClickable:()=>{	return true;	},
				isFixed:true,
				setIsVisible:function(visible){		buttonIncreaseTimeFactor.visible=visible;	},
				getIsVisible:function(){	return buttonIncreaseTimeFactor.visible;		},
				glyph:RIGHT_ARROW_GLYPH,
		};
		this.buttonIncreaseTimeFactor=this.controlsManager.registerClickable(buttonIncreaseTimeFactor);
		this.buttonIncreaseTimeFactor.position=new Position(this.buttonIncreaseTimeFactor,TIME_FACTOR_UI_X+10,TIME_FACTOR_UI_Y+15,0,{x:"left",y:"bottom"});
			
		
//		// DBG
//		aotest.profile(this,"drawAll2D");
		
	}

	
	
	
	// MANDATORY METHODS
	startGame(){
		
		this.initOnStart();

		let fps=this.gameConfig.fps;
		if(fps)	this.refreshRateMillis=1000/fps;
		else		this.refreshRateMillis=nonull(this.gameConfig.refreshRateMillis, 500);

		this.probeTime=getNow();
				
		// MAIN GAME LOOP :
		let self=this;
		
		// We ensure we have asingle-threaded main loop:
		if(this.mainLoop)	this.stopGame();
		
		// NOT STABLE ENOUGH :
//		const draw=function(timestamp) {
//			self.mainLoop=requestAnimationFrame(draw);
//			if(!self.lastRefreshTime || hasDelayPassed(self.lastRefreshTime, self.refreshRateMillis)){
//				self.lastRefreshTime=timestamp;
//				// Drawing code goes here
//		    self.drawMainLoop(self);
//			}
//		}
//    self.mainLoop=requestAnimationFrame(draw);

    // OLD :
		this.mainLoop=setInterval(function(){
			self.drawMainLoop(self);
		},this.refreshRateMillis);

	}
	
	
	
	stopGame(){
//	OLD
		clearInterval(this.mainLoop);
	// NOT STABLE ENOUGH :
//	cancelAnimationFrame(this.mainLoop);
	}
	
	
//	registerSound(s){
//		if(!contains(this.sounds, s))			this.sounds.push(s);
//	}
//	/*private*/unregisterSounds(){
//		clearArray(this.sounds);
//	}
	
	registerStartable(s){
		if(!contains(this.startables, s))		this.startables.push(s);
	}
	/*private*/unregisterStartables(){
		clearArray(this.startables);
	}
	
	/*public*/startAll(){
		
		foreach(this.startables,(s)=>{
			s.startStartable();
		},(s)=>{	return !s.delayedStartAtBeginning;	});
		
		
//		foreach(this.sounds,(s)=>{
//				s.startStartable();
//		});
	}

	
	/*public*/stopAll(){
//		foreach(this.sounds,(s)=>{	s.stopStartable();	});
//		this.unregisterSounds();
		
		foreach(this.startables,(s)=>{	s.stopStartable();	});
		this.unregisterStartables();
		
		
		// We clear all eventual selected objects :
		this.selectionManager.setSelected(null);
	}
	
	
	//=============== CONTROLLER METHODS ===============
	
	executeActionOnSelection(event,methodName){

		// On today, only handle single-element selection  :
//	let selection = this.selectionManager.getSelected(0);
		let selection = this.selectionManager.getSelected();
		if(!selection)	return;
		
		let param=event.target.methodParam;
		
//	// DBG
//	console.log("EXECUTING METHOD "+methodName+" with param "+param+" on selection",selection);
		
		if(selection[methodName])
			selection[methodName](event,param);
		
		
	}

	
	
	
	
	
	
	
	// FACULTATIVE PUBLIC METHODS
	
	exitGame() {
		this.stopGame();
		// TRACE
		lognow("TRACE : TODO : Stop game !");
		// Does not work if the game is not launched in a popup/new tab : window.close();
	}
	
	isPreviousGameDetected() {
		return this.saverLoader.hasData(this.storageKey);
	}

	goToURLSupport() {
		if(this.gameConfig.gameURLs && this.gameConfig.gameURLs["support"])		window.open(this.gameConfig.gameURLs["support"]); 
	}
	goToURLOther() {
		if(this.gameConfig.gameURLs && this.gameConfig.gameURLs["other"])		window.open(this.gameConfig.gameURLs["other"]); 
	}
	goToURLMailTo() {
		if(this.gameConfig.gameURLs && this.gameConfig.gameURLs["email"])		window.open(this.gameConfig.gameURLs["email"]); 
	}
	
	
	/*public*/setCameraPositionToDeltas(deltas,isCameraOffsetted=false){
		
		if(!this.canSetCameraPositionTo({x:deltas.dx, y:deltas.dy} ,isCameraOffsetted))	return;
		
		// We have to go in the same direction as the drag :
		let camera=this.sceneManager.camera;
		// CAUTION ! With numbers, a simple if(deltas.dx){ is not enough because it is false if its value is 0 ! 
		if(deltas.dx!=null){
			camera.position.centerOffsets.x = (isCameraOffsetted?camera.position.centerOffsets.x:0) + deltas.dx;
		}
		if(deltas.dy!=null){
			camera.position.centerOffsets.y = (isCameraOffsetted?camera.position.centerOffsets.y:0) + deltas.dy;
		}
		
	}
		
	/*protected*/canSetCameraPositionTo(position,isCameraOffsetted=false){
		
		let limits=this.currentLevel.limits;
		if(!limits)	return true;
		
		
		let projection = this.gameConfig.projection;
		if(contains(projection, "2D")) {

			let camera=this.sceneManager.camera;

			
			// CAUTION ! With numbers, a simple if(position.x){ is not enough because it is false if its value is 0 ! 
			if(position.x!=null){

				let offsetX=(isCameraOffsetted?camera.position.centerOffsets.x:0);
				
				if(limits._2D.x!=null){
					if(limits._2D.x.min!=null && position.x + offsetX < limits._2D.x.min ){
						return false;
					}
					if(limits._2D.x.max!=null && limits._2D.x.max < position.x + offsetX){
						return false;
					}
				}
			}
			if(position.y!=null){
				
				let offsetY=(isCameraOffsetted?camera.position.centerOffsets.y:0);

				if(limits._2D.y!=null){
					if(limits._2D.y.min!=null && position.y + offsetY < limits._2D.y.min){
						return false;
					}
					if(limits._2D.y.max!=null && limits._2D.y.max < position.y + offsetY){
						return false;
					}
				}
			}
			
		}
		
		return true;
	}
	
	
	
	
	
	
	/*private*/onCameraMove(rootContainer){
		
		
		let allPrototypesConfig=this.prototypesConfig.allPrototypes;
		
		// Ongoing population of the model :
		let sceneManager=this.sceneManager;
		let camera=sceneManager.camera;
		
		var cardinalities=rootContainer.cardinalities;
	
		if(!cardinalities)	return;
		

		let alreadyPositionnedItems=[];
		
		let self=this;
		foreach(cardinalities,(cardinality,attributeName)=>{
			
			/* !!! CAUTION !!!  WHEN WE ARE IN NOT IN «preinstanciate» MODE, 
			 * «cardinality» REPRESENTS THE NUMBER OF ELEMENTS ART THE SAME TIME INSTANCIATED IN *DISPLAY AREA* !!!
			 * ELSE IT REPRESENT THE *TOTAL* AMOUNT OF INSTANCIATED OBJECTS IN THE WHOLE MODEL !!!
			 */
			
			// We DO NOT only consider drawables for instantiation !
			let collection=rootContainer[attributeName];

			
			// 2- Case we have to instanciate new objects to make the "fill" cardinality work :
			if(cardinality.value==="fill"){

				// If we haven't calculated the max filled yet :
				if(cardinality.xBoundaryLeft==null){
					cardinality.xBoundaryLeft=0;
					cardinality.xBoundaryRight=0;
					
					foreach(collection,(item,key2)=>{
						
							if(contains(self.gameConfig.projection, "2D")) {
								if(contains(self.gameConfig.scroll, "horizontal")) {

									var left=item.position.getLeft();
									if(left<cardinality.xBoundaryLeft){
										cardinality.xBoundaryLeft=left;
									}
									
									var right=item.position.getRight();
									if(cardinality.xBoundaryRight<right){
										cardinality.xBoundaryRight=right;
									}
									
								}
							}						
						
					},(item)=>{ 
						// We skip uninitialized items and items that can never be seen :
						return item && item.getIsVisible && item.getIsVisible() && item.draw;
					});
				}

				// TODO : FIXME : DOES STILL NOT SEEM TO WORK !

				
				let vcenter=self.gameConfig.getViewCenterOffset();
				let spawningZone=getSpawningZone(cardinality);
				
				let cameraPosition=camera.position.getOffsetted();
				if(cameraPosition.x-vcenter.x < cardinality.xBoundaryLeft){

					
					// We add an object to the left :
					// We instanciate the object randomly :
					var className=attributeName.charAt(0).toUpperCase()+attributeName.substr(1,attributeName.length-2);
					
//					// DBG
//					console.log("INSTANCIATED NEW FILL OBJECTS LEFT : "+className);
					
					let itemAndZone=self.instantiationManager.instanciateAndAddToCollectionPrototypeOnly(
							collection,
							className,
							cardinality.prototypesInstantiation,
//							cardinality.prototypeConfig,
							null,
							allPrototypesConfig,
							{x:cardinality.xBoundaryLeft});
					let item=itemAndZone.item;
					if(!spawningZone)	spawningZone=itemAndZone.spawningZone;
					
//					// DBG
//					console.log("cardinality.prototypeConfig:",cardinality.prototypeConfig);

					// (Here, in «fill» mode, then there cannot be overlapping ! (because of how the «fill» mode positioning is done)) 
					let pos=getPositionFor2DObject(spawningZone, cardinality.xBoundaryLeft+item.position.getLeft() - nonull(spawningZone.w,0));
					sceneManager.setPositionFirstTime(item, pos);
					
					// ...and we update the boundary :
					cardinality.xBoundaryLeft-=item.size.w + nonull(spawningZone.w,0);
					
					
					
				}else if(cardinality.xBoundaryRight < cameraPosition.x+vcenter.x){

					
					
					// We add an object to the right :
					// We instanciate the object randomly :
					var className=attributeName.charAt(0).toUpperCase()+attributeName.substr(1,attributeName.length-2);

//					// DBG
//					console.log("INSTANCIATED NEW FILL OBJECTS RIGHT : "+className);
					
					let itemAndZone=self.instantiationManager.instanciateAndAddToCollectionPrototypeOnly(
							collection,
							className,
							cardinality.prototypesInstantiation,
//							cardinality.prototypeConfig,
							null,
							allPrototypesConfig,
							{x:cardinality.xBoundaryRight});
					let item=itemAndZone.item;
					if(!spawningZone)	spawningZone=itemAndZone.spawningZone;

					
//					// DBG
//					console.log("cardinality.prototypeConfig:",cardinality.prototypeConfig);
					
					let pos=getPositionFor2DObject(spawningZone, cardinality.xBoundaryRight+item.position.getRight() + nonull(spawningZone.w,0), cardinality.avoidOverlap, item.size);
					manageOverlappingItems(alreadyPositionnedItems,self.gameConfig,cardinality,item,pos);
					sceneManager.setPositionFirstTime(item, pos);
					
					// ...and we update the boundary :
					cardinality.xBoundaryRight+=item.size.w + nonull(spawningZone.w,0);

				}
				
			
			}else if(cardinality.value==="staticGroups"){ // Static groups Collection cardinality :

				
				if(!cardinality.preinstanciate){ // (If we have preinstantiated, then at this point they're already instanciated !)
					
					foreach(cardinality.prototypesInstantiation.staticInstantiations,(protoConf)=>{
	
						let protoNameFromConf=getProtoNameFromConf(protoConf);
						
						self.simpleCardinalityInstanciateListOfNewInstances(collection,attributeName,protoConf
						// We have to use the parent cardinality's config :
						,cardinality.prototypesInstantiation
						,protoNameFromConf);
						
					},(protoConf)=>{	return !protoConf.preinstanciate; }); // (If we have preinstantiated, then at this point they're already instanciated !)

				}
				
			
			}else if(cardinality.value==="randomGroups"){ // Random groups Collection cardinality :
					
				
					let protoConf=Math.getRandomInArray(cardinality.prototypesInstantiation.randomInstantiation);

					if(!cardinality.preinstanciate && !protoConf.preinstanciate){ // (If we have preinstantiated, then at this point they're already instanciated !)
						
						let protoNameFromConf=getProtoNameFromConf(protoConf);
						
						self.simpleCardinalityInstanciateListOfNewInstances(collection,attributeName,protoConf
						// We have to use the parent cardinality's config :
						,cardinality.prototypesInstantiation
						,protoNameFromConf);
					}
					
				
			}else{  // Ratio or interval collections cardinalities :
				
				// (cardinality 5/10 = An object has 5 chances out of 10 scroll events to appear)
				// (cardinality 5->10 = Objects total number *ON DISPLAY AREA* will always be between 5 min and 10 max on scroll events)
				
				if(!cardinality.preinstanciate){
					
					self.simpleCardinalityInstanciateListOfNewInstances(collection,attributeName,cardinality,cardinality.prototypesInstantiation);
				}
				
			}
			
		});
		
		
	
	}
	
	
	/*private*/simpleCardinalityInstanciateListOfNewInstances(rootContainerCollection, attributeName, cardinality, prototypesInstantiation,
																														chosenPrototypeParam=null){

		let allPrototypesConfig=this.prototypesConfig.allPrototypes;

		let sceneManager=this.sceneManager;
		let camera=sceneManager.camera;

		let gameConfig=this.gameConfig;
		let numberOfInScreen=0;
		
		foreach(rootContainerCollection,(item,key2)=>{
			
			if(!item.position)
				numberOfInScreen++;
			else if(isInVisibilityZone(gameConfig,camera,item.position,item.size))
				numberOfInScreen++;
		
		},(item)=>{	
			// We skip uninitialized items and items that can never be seen :
			return item && item.getIsVisible && item.getIsVisible() && item.draw;
		});

		

		if(cardinality.oldNumberOfInScreen && cardinality.oldNumberOfInScreen!=numberOfInScreen){
			
			// If number of visible things have changed :
			
			var numberToInstanciate=0;
			
			// TODO : ...
//			if(isString(cardinality.value) && contains(cardinality.value,"/")){
//				var splits=cardinality.value.split("/");
//				var numerator=parseInt(splits[0]);
//				var denominator=parseInt(splits[1]);
//				
//				var isToInstanciate=Math.getRandomDice(numerator,denominator);
//				
//				if(isToInstanciate)	numberToInstanciate=1;
//				
//				
//			}else 
			if(isString(cardinality.value) && contains(cardinality.value,"->")){
				var splits=Math.getMinMax(cardinality.value);
				var min=splits.min;
				var max=splits.max;

				if(numberOfInScreen<min){
					numberToInstanciate=Math.getRandomInt(max,min)-numberOfInScreen; 
				}

			}else{
				
				let min=(isNumber(cardinality.value)?cardinality.value:parseInt(cardinality.value));
				
				if(numberOfInScreen<min){
					numberToInstanciate=min-numberOfInScreen; 
				}

			}
			
			
			if(0<numberToInstanciate){
				
				// Caution : this value can be overriden later !
				let className=attributeName.charAt(0).toUpperCase()+attributeName.substr(1,attributeName.length-2);
				
				
				var collection=rootContainerCollection;
				
				
				// TODO : FIXME : There seem to remain a problem in spawning positioning...l
				
				let cameraPosition=camera.position.getOffsetted();
				
				var spawningPositionX=cameraPosition.x;
				var cameraDeltaX=camera.position.getDeltas().x;
				var viewX=this.gameConfig.getViewCenterOffset().x;

				let zoom=this.gameConfig.zoom;
				
				var direction=0;
				if(cameraDeltaX<0){
					spawningPositionX-=viewX*zoom;
					direction=-1;
				}else	if(0<cameraDeltaX){
					spawningPositionX+=viewX*zoom;
					direction=1;
				}
				
				let spawningZone=getSpawningZone(cardinality);
				
				
//				// DBG
//				if(!spawningZone) {
//					console.log("zone is null for cardinality :",cardinality);
//					console.log("zone is null for prototypesInstantiation :",prototypesInstantiation);
//				}
				
				let alreadyPositionnedItems=[];
				
				// We instanciate the new objects :
				const WIGGLE_SPAWNING_AREA=this.gameConfig.getResolution().w;
				for(var i=0;i<numberToInstanciate;i++){
					
					let itemAndZone= this.instantiationManager.instanciateAndAddToCollectionPrototypeOnly(
										collection,
										className,
										prototypesInstantiation,
										chosenPrototypeParam,
										allPrototypesConfig);
					
					let item=itemAndZone.item;
					if(!spawningZone)	spawningZone=itemAndZone.spawningZone;

					var randomWiggle=Math.getRandomInt(WIGGLE_SPAWNING_AREA);
					
//					// DBG
//					console.log("cardinality.prototypeConfig (chosenPrototypeParam):",chosenPrototypeParam.prototypeConfig);

					let pos=getPositionFor2DObject(spawningZone,spawningPositionX+direction*randomWiggle, cardinality.avoidOverlap, item.size);
					manageOverlappingItems(alreadyPositionnedItems, gameConfig, cardinality, item, pos);
					sceneManager.setPositionFirstTime(item, pos);

				}
				
			}
			
			
			
		}
		
		cardinality.oldNumberOfInScreen=numberOfInScreen;
		
	}
	
	/*public*/createInstance(className, prototypeName){
		if(!this.prototypesConfig.allPrototypes[className]){
			// TRACE
			console.log("ERROR : No prototype config found for class «"+className+"».");
			return null;
		}
		let protoConfig=this.prototypesConfig.allPrototypes[className][prototypeName];
		if(!protoConfig){
			// TRACE
			console.log("ERROR : No prototype config found for class «"+className+"» and prototype «"+prototypeName+"»");
			return null;
		}
		
		return this.instantiationManager.getSingleInstanceWithPrototype(className,{value:prototypeName});
	}
	
	
	
	/*protected (static)*/addToBackgrounds(gameConfig,backgroundsArray,backgroundsHolder,zIndexOffset){

		const basePath=gameConfig.basePath;
		const backgroundClipSizeDefault=gameConfig.backgroundClipSize;
		const backgroundOffsetYDefault=backgroundsHolder.backgroundOffsetY;
		const foregroundOffsetYDefault=backgroundsHolder.foregroundOffsetY;

		const pathImageSrc=getConventionalFilePath(basePath,backgroundsHolder,"universe");
		
		
		if(backgroundsHolder.backgroundColor){
			
			let background=new GameBackgroundColor(backgroundsHolder.backgroundColor,gameConfig);
			backgroundsArray.push(background);
			
		}else{
			
			
			if(backgroundsHolder.backgroundImage || backgroundsHolder.backgroundImages){
				let imgConf=(backgroundsHolder.backgroundImages?
										Math.getRandomInArray(backgroundsHolder.backgroundImages):backgroundsHolder.backgroundImage);
				let img=isString(imgConf)?imgConf:imgConf.normal;
				
				let imgDark=imgConf.dark;
					
				let background=
					new GameBackground(
						nonull(backgroundsHolder.backgroundZIndex,0)+zIndexOffset,
						gameConfig, 
						backgroundsHolder.backgroundParallax, 
						pathImageSrc+img,
						imgDark?(pathImageSrc+imgDark):null,
						backgroundClipSizeDefault,
						nonull(backgroundOffsetYDefault,0),
						false);
				backgroundsArray.push(background);
				
			}else if(backgroundsHolder.backgrounds){
				
				foreach(backgroundsHolder.backgrounds,(backgroundInfo)=>{
					let imgConf=(backgroundInfo.images?Math.getRandomInArray(backgroundInfo.images):backgroundInfo.image);
					let img=isString(imgConf)?imgConf:imgConf.normal;
					
					let imgDark=imgConf.dark;
					
					let backgroundClipSize=nonull(backgroundInfo.clipSize,backgroundClipSizeDefault);
					let backgroundOffsetY=nonull(backgroundInfo.offsetY,backgroundOffsetYDefault);
						
					let background=
						new GameBackground(
								nonull(backgroundsHolder.backgroundZIndex,0)+zIndexOffset,
								gameConfig, 
								backgroundsHolder.backgroundParallax+nonull(backgroundInfo.parallaxAdd,0), 
								pathImageSrc+img,
								imgDark?(pathImageSrc+imgDark):null,
								backgroundClipSize,
								nonull(backgroundOffsetY,0)+nonull(backgroundInfo.offsetYAdd,0),
								false);
						backgroundsArray.push(background);
				});
			}
		
		}
			
		if(backgroundsHolder.foregrounds){
			
			
			foreach(backgroundsHolder.foregrounds,(foregroundInfo)=>{
				let imgConf=(foregroundInfo.images?Math.getRandomInArray(foregroundInfo.images):foregroundInfo.image);
				let img=isString(imgConf)?imgConf:imgConf.normal;
				
				let imgDark=imgConf.dark;
				
				let foregroundClipSize=nonull(foregroundInfo.clipSize,backgroundClipSizeDefault);
				let foregroundOffsetY=nonull(foregroundInfo.offsetY,foregroundOffsetYDefault);

				let foreground=
					new GameBackground(
							nonull(backgroundsHolder.backgroundZIndex+999,0)+zIndexOffset,
							gameConfig, 
							backgroundsHolder.foregroundParallax+nonull(foregroundInfo.parallaxAdd,0), 
							pathImageSrc+img,
							imgDark?(pathImageSrc+imgDark):null,
							foregroundClipSize,
							nonull(foregroundOffsetY,0)+nonull(foregroundInfo.offsetYAdd,0),
							true);
					backgroundsArray.push(foreground);
			});
		}
		
	}
	
	
	doLock(){
		this.lockDrawing=true;
	}
	
	doUnlock(){
		this.lockDrawing=false;
	}
	
	getSceneManager(){
		return this.sceneManager;
	}
	
	getSelectionManager(){
		return this.selectionManager;
	}

	//=============== VIEW METHODS ===============
	getLastMesuredRefreshedRateMillis(){
		return nonull(this.deltaTMillis,100);
	}



	
	// -----------------------------------------------------------

	
	/*private*/drawMainLoop(self){
		

		// Some heavy model treatments require the main drawing loop to be suspended :
		if(self.lockDrawing===true)	return;
		
		let currentContainer=self.currentContainer;
		let drawables=currentContainer.getDrawables();
		
		// Moving
		self.sceneManager.moveAll(currentContainer, drawables);
		
		// The view only draws the current level from the model :		
		self.sceneManager.drawAll(currentContainer, drawables);
		
		
		self.sceneManager.doDrawing(function(ctx){

			if(!ctx)	return; // WORKAROUND FOR RARE OCCURENCES BUG WHEN WE ARE IN A TRANSITION BETWEEN PAGES AND THERE IS NO CANVAS (it seems).
			
			// Time factor :
			let buttonDecreaseTimeFactor=self.buttonDecreaseTimeFactor;
			let buttonIncreaseTimeFactor=self.buttonIncreaseTimeFactor;

			if(!self.areRootControlsVisible() || !self.gameConfig.canChangeDurationTimeFactor) {
				buttonDecreaseTimeFactor.setIsVisible(false);
				buttonIncreaseTimeFactor.setIsVisible(false);
				return;
			}
			
			buttonDecreaseTimeFactor.setIsVisible(true);
			buttonIncreaseTimeFactor.setIsVisible(true);
			
			
			
			ctx.save();
			ctx.font = "bold 18px Arial";
			ctx.lineWidth = 4;
			ctx.strokeStyle = "#000000";
			let timeFactor=Math.round(1/self.gameConfig.getDurationTimeFactor());
//			// DBG
//			lognow("GAMESCREEN self.gameConfig:",self.gameConfig);
			
			ctx.strokeText(timeFactor+FACTOR_GLYPH,TIME_FACTOR_UI_X,TIME_FACTOR_UI_Y);
			
			ctx.fillStyle = "#AAAAAA";
			ctx.fillText(timeFactor+FACTOR_GLYPH,TIME_FACTOR_UI_X,TIME_FACTOR_UI_Y);
			ctx.restore();
			
			ctx.save();
			ctx.font = "bold 20px Monospace";
			ctx.lineWidth = 4;
			ctx.strokeStyle = "#000000";
			ctx.fillStyle = "#AAAAAA";

			ctx.strokeText(buttonDecreaseTimeFactor.glyph, buttonDecreaseTimeFactor.getPosition().x, buttonDecreaseTimeFactor.getPosition().y);
			ctx.fillText(buttonDecreaseTimeFactor.glyph, buttonDecreaseTimeFactor.getPosition().x, buttonDecreaseTimeFactor.getPosition().y);
			
			ctx.strokeText(buttonIncreaseTimeFactor.glyph, buttonIncreaseTimeFactor.getPosition().x, buttonIncreaseTimeFactor.getPosition().y);
			ctx.fillText(buttonIncreaseTimeFactor.glyph, buttonIncreaseTimeFactor.getPosition().x, buttonIncreaseTimeFactor.getPosition().y);
			
			ctx.restore();
			
			
			
			if(IS_DEBUG){
				
				self.deltaTMillis=getNow()-self.probeTime;
				self.probeTime=getNow();
				let fps=Math.round((1/self.deltaTMillis)*1000);
				ctx.save();
				ctx.font = "bold 20px Arial";
				ctx.lineWidth = 4;
				ctx.strokeStyle = "#000000";
				ctx.strokeText(fps+" FPS",30,20);
				ctx.fillStyle = "#FFFFFF";
				ctx.fillText(fps+" FPS",30,20);
				ctx.restore();
			}
			
			
			
		});
		
	}
	
	
}
