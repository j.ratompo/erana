/* ## Utility gamu methods - model part
 *
 * This set of methods gathers utility game-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils-gamu» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by its programming egos legions)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 *  
 */




//======================== Level utility methods ======================== 

//const SAVED_SUFFIX="";


// GAME LEVELS

function getLevelsManager(allPrototypesConfigRoot, allPrototypesStorageKey, controller, model) {

	// DBG (REMOVE WHEN FINISHED TO ALLOW TO RESUME A PREVIOUS GAME!)
//	var DEBUG_MODE_ALWAYS_CREATE_NEW_GAME=true;

	const allPrototypesConfig=allPrototypesConfigRoot.allPrototypes;
	const levelsConfig=allPrototypesConfig["GameLevel"];
	
	
	let self={
		
		currentLevelName:null,
		
		/*private*/getWholeReferenceCurrentObjectConfig:function(referenceCurrentObjectProtoConfigParam, className=null){
			
			let referenceCurrentObjectProtoConfig=referenceCurrentObjectProtoConfigParam;
			
			let superPrototypeName=referenceCurrentObjectProtoConfig.usesSuper;
			if(superPrototypeName && className && startsWithUpperCase(className)){
			
				let superProto=allPrototypesConfig[className][superPrototypeName];
				if(!superProto){
					// TRACE
					lognow("WARN : No super prototype found in global config for class name «"+className+"» and super prototype name «"+superPrototypeName+"»");
				}else{
					
					let completeSuperProto=self.getWholeReferenceCurrentObjectConfig(superProto,className);
					foreach(completeSuperProto,(attr,attrName)=>{
						referenceCurrentObjectProtoConfig[attrName]=attr;
					});
				}
				
			}
			
			return referenceCurrentObjectProtoConfig;
		},
		
		
		
		/*private*/generateRefAndPrototype:function(parentReferenceCurrentObjectConfig, subObjectProtoName, staticInstanciation, actualObjectAttribute, resultConstructedAllPrototypesConfig, parentRefConfig=null){
			
			// 		- prototype reference :
			let subObjectProtoReference={name:subObjectProtoName/*+SAVED_SUFFIX*/, preinstanciate:true};
			staticInstanciation.push(subObjectProtoReference);
			if(actualObjectAttribute.position)
				subObjectProtoReference.spawningZone={x:actualObjectAttribute.position.x, y:actualObjectAttribute.position.y};
			
			
			//DBG
			if(contains(subObjectProtoName,"locomotive"))
				lognow("BUG 2 Locomotive");
			
			
			if(self.isSubClassed(parentReferenceCurrentObjectConfig, actualObjectAttribute)){// case sub-class
				let className=getClassName(actualObjectAttribute);
				subObjectProtoReference.subClass=className;
			} 
				
			// 		- actual prototype : (recursive call)
			self.extractStaticConfig(actualObjectAttribute, resultConstructedAllPrototypesConfig, parentRefConfig);
			
			
		},
		
		/*private*/isSubClassed(parentReferenceConfig, obj){
			let className=getClassName(obj);
			return !parentReferenceConfig[className];
		},
		
		
		/*private*/cloneAndRemoveAllNonNativeLinks:function(objParam){
			
			// We only want «simple» objects :
			
			let obj=objParam;
			if(isPrimitive(obj))	return obj;

			
			let currentClone={};
			
			// We remove all possible links to objects (because they are not present in their original prototype !),
			// to avoid possible circular references :
			foreach(obj,(subObjAttr,subObjAttrName)=>{
				
				if(isArray(subObjAttr)){

					if(!currentClone[subObjAttrName])		currentClone[subObjAttrName]=[];
					
					foreach(subObjAttr,(item)=>{

						if(item && !isNativeClass(item)){ /*(case single-attribute class direct definition in parent !)*/
							return "continue";
						}
						
						let cleanedItem=self.cloneAndRemoveAllNonNativeLinks(item);
						currentClone[subObjAttrName].push(cleanedItem);
					
					
					});
					
				}else{

					
					
					if(subObjAttr && !isNativeClass(subObjAttr)){ /*(case single-attribute class direct definition in parent !)*/
						return "continue";
					}
					
					currentClone[subObjAttrName]=self.cloneAndRemoveAllNonNativeLinks(subObjAttr);
					
				}
				
			});
			
			
			return currentClone;
		},
		

		/*private*/extractStaticConfig:function(currentObject, resultConstructedAllPrototypesConfig={}, parentRefConfig=null){
			
			let className=getClassName(currentObject);
			
			let prototypeName=currentObject.prototypeName;
			
			let referenceCurrentObjectProtoConfig;
			if(parentRefConfig) /*(case single-attribute class direct defintion in parent !)*/
				 	 referenceCurrentObjectProtoConfig=parentRefConfig[className];
			else referenceCurrentObjectProtoConfig=allPrototypesConfig[className][prototypeName];
			
			
			if(!resultConstructedAllPrototypesConfig[className])	resultConstructedAllPrototypesConfig[className]={};
			let resultConstructedAllPrototypesConfigClassRoot=resultConstructedAllPrototypesConfig[className];

			if(!resultConstructedAllPrototypesConfigClassRoot[prototypeName/*+SAVED_SUFFIX*/])	resultConstructedAllPrototypesConfigClassRoot[prototypeName/*+SAVED_SUFFIX*/]={};
			let resultConstructedAllPrototypesConfigProtoRoot=resultConstructedAllPrototypesConfigClassRoot[prototypeName/*+SAVED_SUFFIX*/];

			
			let wholeReferenceCurrentObjectConfig=self.getWholeReferenceCurrentObjectConfig(referenceCurrentObjectProtoConfig, className);
			
			
			
			// We merge the reference prototype attributes and the actual attributes of the saved object :
			foreach(currentObject,(attr,attrName)=>{

				if(isNativeClass(attr)){
					
					if(!wholeReferenceCurrentObjectConfig[attrName])
						wholeReferenceCurrentObjectConfig[attrName]=attr;
					
				}else{
					

					
				}
				
			});
			
			
			
			foreach(wholeReferenceCurrentObjectConfig, (refAttr, refAttrName)=>{
				
				
				// DBG
				lognow(refAttrName,refAttr);
				
				
				//	NO : BECAUSE IT IS USED IN THE IMAGE PATH CALCULATIONS ! : Special attribute, must not be set in the actual object :
				// if(refAttrName==="usesSuper")	return "continue";
				
				if(!startsWithUpperCase(refAttrName)){ // If ref attribute object is not of a domain class
					
					// Case simple object attribute :
					let actualObjectAttribute=currentObject[refAttrName];
					
					actualObjectAttribute=self.cloneAndRemoveAllNonNativeLinks(actualObjectAttribute);
					
					resultConstructedAllPrototypesConfigProtoRoot[refAttrName]=actualObjectAttribute;
					
				}else{
					
					// Case class attribute :
					
					
					resultConstructedAllPrototypesConfigProtoRoot[refAttrName]={};
					let subObjectConfig=resultConstructedAllPrototypesConfigProtoRoot[refAttrName];

					subObjectConfig["cardinality"]={};
					let cardinality=subObjectConfig["cardinality"];
					
					
					let actualObjectAttribute;
					let refCardinality=refAttr["cardinality"];
					let uncapitalizedAttributeName=uncapitalize(refAttrName);
					
					
					let isArray;
					if(!refCardinality /*(case single-attribute class direct definition in parent !)*/|| refCardinality.value==1){
						cardinality["value"]=1;
						actualObjectAttribute=currentObject[uncapitalizedAttributeName];
						isArray=false;
					}else{
						cardinality["value"]="staticGroups";
						actualObjectAttribute=currentObject[uncapitalizedAttributeName+"s"];
						isArray=true;
					}
					
					cardinality["prototypesInstanciation"]={};
					let prototypesInstanciation=cardinality["prototypesInstanciation"];
					
					prototypesInstanciation["staticInstanciation"]=[];
					let staticInstanciation=prototypesInstanciation["staticInstanciation"];
					
					
					// We generate a new prototype in all cases :
					
					if(!isArray){
						
						let subObjectProtoName=actualObjectAttribute.prototypeName;
						
						let subObjectParentRefConfig=null;
						if(!refCardinality /*(case single-attribute class direct defintion in parent !)*/)
							subObjectParentRefConfig=wholeReferenceCurrentObjectConfig;
						
						self.generateRefAndPrototype(wholeReferenceCurrentObjectConfig, subObjectProtoName, staticInstanciation, actualObjectAttribute, resultConstructedAllPrototypesConfig, subObjectParentRefConfig);

					}else{
						
						foreach(actualObjectAttribute,(item)=>{

							let subObjectProtoName=item.prototypeName;
							self.generateRefAndPrototype(wholeReferenceCurrentObjectConfig, subObjectProtoName, staticInstanciation, item, resultConstructedAllPrototypesConfig);

						});

						
						
					}
					
					
					
					
				}
				
			});
			
			
			return resultConstructedAllPrototypesConfig;
		},
		
		saveGame:function(){
			
			if(!model.level){
				// TRACE
				lognow("WARN : Cannot save game level from model : ", model);
				return;
			}
			
			const allPrototypesConfig=self.extractStaticConfig(model.level);
			
			// DBG
			lognow("allPrototypesConfig:"+allPrototypesConfig,allPrototypesConfig);
			
			model.saverLoader.saveState(allPrototypesConfig, allPrototypesStorageKey);

		},
		
		
		startLevelIfNoCurrent : function(pageName, /*NULLABLE*/levelName, restoreLevelIfPresent=false) {

			self.currentLevelName=levelName;
			if(contains(self.currentLevelName,"()")){
				var levelMethodName=self.currentLevelName.replace("()","");
				if(!nothing(controller[levelMethodName])){
					self.currentLevelName=controller[levelMethodName].apply(controller,[pageName]);
				}
			}
			
			let currentLevelConfig=null;
			let allInstanciatedSavedPrototypes=null;
			if(model.saverLoader.hasData(allPrototypesStorageKey) && restoreLevelIfPresent){
				allInstanciatedSavedPrototypes=model.saverLoader.loadState(allPrototypesStorageKey);
				currentLevelConfig=allInstanciatedSavedPrototypes["GameLevel"][self.currentLevelName/*+SAVED_SUFFIX*/];
			}

			let referenceCurrentLevelConfig;
			if(currentLevelConfig) {

				// We get the saved level state if it exists  :
				model.level=model.instanciationManager.createLevelState(self.currentLevelName/*+SAVED_SUFFIX*/, currentLevelConfig, allInstanciatedSavedPrototypes);
				
				// DBG
				lognow("model.level:",model.level);
				
			}else{
				
				const referenceCurrentLevelConfig=levelsConfig[self.currentLevelName];

				// ...else we start a new state for this level :
				// (randomized, at this point)
				model.level=model.instanciationManager.createLevelState(self.currentLevelName, referenceCurrentLevelConfig);
				
				
				// TODO : FICME : Also in loading level ?
				
				// We set the default selection if needed :
				if(referenceCurrentLevelConfig.selectedItems){
					var selectedItems=referenceCurrentLevelConfig.selectedItems;
					if(contains(selectedItems,"()")){
						var methodName=selectedItems.replace("()","");
						var selectedObject=controller[methodName].apply(controller);
						
						// TODO : Make so it can handle multiple selection :
						// On today, only handle single-element selection  :
//					if(isArray(selectedObject))
						controller.selectionManager.setSelected(selectedObject);
//					else
//						controller.selectionManager.setSelected([selectedObject]);
					}
				}else if(referenceCurrentLevelConfig.selectedItem){
					var selectedItem=referenceCurrentLevelConfig.selectedItem;
					if(contains(selectedItem,"()")){
						var methodName=selectedItem.replace("()","");
						var selectedObject=controller[methodName].apply(controller);
						controller.selectionManager.setSelected(selectedObject);
					}
				}
				
				
			}

			
			
			// We launch the level with the properly setted model :
			controller.start(); // cf. GameScreen.start() method.
			
			
		}
	};

	foreach(levelsConfig,(level,key)=>{
		self[key]={
			config : levelsConfig[key]
		};

	});

	return self;
}


// ======================== Model utility methods ======================== 

function getSaverLoader() {
	const DEFAULT_OWNER_KEY="data";

	let self= {

		// DBG
		isCompressed:false,
		saveState : function(levelObject,/*NULLABLE*/ownerKey) {

			let key=DEFAULT_OWNER_KEY;
			if(!nothing(ownerKey)) {
				key=ownerKey;
			}

			// (JSON string)
			// DOES NOT WORK (anonymous objects miss their functions and methods :)
//			let savedMap=getAsFlatStructure(object);
//			let savedString=stringifyObject(savedMap);
//			savedString=LZWString.compress(savedString);
			
			// DOES NOT WORK : Stack call overflow :
//			let savedWithNoParents=self.removeParents(object);
//			let savedString=stringifyObject(savedWithNoParents);
			

			//			var savedString="{objects:[";
			//			var comma=false;
			//			for (var i=0; i < objects.length; i++) {
			//				if(comma)
			//					savedString += ",";
			//				else
			//					comma=true;
			//				var o=objects[i];
			//				savedString += stringifyObject(o);
			//			}
			//			savedString += "]}";
			
			// We use string compression to spare storage space and render a very little bit difficult to alter the saved game :
			let savedString;
			if(self.isCompressed)
						savedString=LZWString.compress( stringifyObject(levelObject));
			else	savedString=stringifyObject(levelObject);
			
			
			storeString(key, savedString);
		},

		loadState : function(/*NULLABLE*/ownerKey) {
			var key=DEFAULT_OWNER_KEY;
			if(!nothing(ownerKey)) {
				key=ownerKey;
			}
			

			let loadedString=getStringFromStorage(key);
			if(!loadedString) return null;
			
			// (JSON string)

			// DOES NOT WORK (anonymous objects miss their functions and methods :)
//			loadedString=LZWString.decompress(loadedString);
//			let modelFlat=parseJSON(loadedString);
//			let model=getAsTreeStructure(modelFlat);
			
			// DOES NOT WORK : Stack call overflow :
//			let modelRaw=parseJSON(loadedString);
//			let model=self.addParents(modelRaw);

			// We use string compression to spare storage space and render a very little bit difficult to alter the saved game :
			let modelLevel;
			if(self.isCompressed)
						modelLevel=parseJSON( LZWString.decompress(loadedString) );
			else	modelLevel=parseJSON( loadedString );

			return modelLevel;
		},

//		,restoreLevel : function(levelName, levelData) {
//			var level={};
//
//			level.levelName={};
//
//			
//			// TODO : develop...!
//
//			return level;
//		}
		
		hasData:function(/*NULLABLE*/ownerKey){
			let key=DEFAULT_OWNER_KEY;
			if(!nothing(ownerKey)) {
				key=ownerKey;
			}
			
			let loadedString=getStringFromStorage(key);
			return loadedString && loadedString.length;
		},
		
		
	};
	
	return self;
}


// MODEL INSTANCES GENERATION

///*private*/function addZonesCoordinates(zone1,zone2){
//	returun {x:nonull(zone1.x,0)+nonull(zone2.x,0), y:nonull(zone1.y,0)+nonull(zone2.y,0),
//					 w:nonull(zone1.w,nonull(zone2.w,0)), h:nonull(zone1.h,nonull(zone2.h,0)) };
//}
//
///*public*/function getSpawningZone(holder,previousHolder=null){
//	if(!previousHolder){
//		if(!holder.spawningZone.cumulative){
//			return holder.spawningZone;
//		}	else{
//			holder.calculatedCumulativeSpawningZone=holder.spawningZone;
//			return holder.spawningZone;
//		}
//	}
//
//	if(!holder.spawningZone.cumulative || !previousHolder.calculatedCumulativeSpawningZone)
//		return holder.spawningZone;
//	
//	holder.calculatedCumulativeSpawningZone=addZonesCoordinates(holder.spawningZone,previousHolder.calculatedCumulativeSpawningZone);
//	
//	return holder.calculatedCumulativeSpawningZone;
//
//}


/*public*/function getSpawningZone(holder){
	return holder.spawningZone;
}


/*public*/function getProtoNameConf(protoConf){
	if(protoConf.names) {
		return Math.getRandomInArray(protoConf.names);
	}
	let result={value:protoConf.name, spawningZone:getSpawningZone(protoConf)};
	if(protoConf.subClass)	result.subClass=protoConf.subClass;
	return result;
}


function getInstanciationManager(prototypesConfig,gameConfig) {

	const ALL_PROTOTYPES_CONFIG=prototypesConfig.allPrototypes;
	const GAME_CONFIG=gameConfig;
	const ABSTRACT_PREFIX="abstract_";

	/*private*/function getNumberToPreInstanciate(cardinality){
		
		var numberToPreInstanciate=0;

		// Ratio or interval collections cardinalities :
		// (cardinality 5/10 = An object has 5 chances out of 10 scroll events to appear)
		// (cardinality 5->10 = Objects total number on display area will always be between 5 min and 10 max on scroll events)
		// TODO :
//		if(isString(cardinality.value) && contains(cardinality.value,"/")){
//			var splits=cardinality.value.split("/");
//			var numerator=parseInt(splits[0]);
//			var denominator=parseInt(splits[1]);
//			
//			// On init, it is the same as the min max interval cardinality value
//			numberToPreInstanciate=Math.getRandomInt(denominator,numerator);
//		}else 
		if(isString(cardinality.value) && contains(cardinality.value,"->")){
			
			// On init, it is the same as the chances fraction cardinality value
			numberToPreInstanciate=Math.getRandomInRange(cardinality.value);

			
		}else if(isString(cardinality.value)){
			numberToPreInstanciate=parseInt(cardinality.value);
		}else if(isNumber(cardinality.value)){
			numberToPreInstanciate=cardinality.value;
		}
	
		return numberToPreInstanciate;
	};
	
	/*public*/function instanciateAndAddToCollection(
			collection, 
			classNameParam,
			prototypesInstanciation,
			// If no chosen prototype parameter is passed, then it means we want it to be calculated with the prototype instantiation information only :
			chosenPrototypeParam=null,
			allPrototypesConfig=ALL_PROTOTYPES_CONFIG,
			// TODO : FIXME : On today, only in 2D !!
			position={x:null,y:null}){

		var chosenPrototype=chosenPrototypeParam;
		
		let className=classNameParam;
		if(chosenPrototypeParam && chosenPrototypeParam.subClass){
			className=chosenPrototypeParam.subClass;
		}
		
		
		// First OF ALL (it is prioritary on everything else) we check if we ever have to 
		// instanciate all static, fixed objects visible at this location :
		if(prototypesInstanciation.staticInstanciation){

			if(!chosenPrototype){ // Case on-move instantiation :
				
				var staticInstanciation=prototypesInstanciation.staticInstanciation;
				let foundPrototype = foreach(staticInstanciation,(staticConf)=>{
					if( staticConf.preinstanciate ||
							// TODO : FIXME : On today, only in 2D !!
							isInZone({x:position.x,y:position.y},getSpawningZone(staticConf))){
						
						let protoNameConf=getProtoNameConf(staticConf);
						return protoNameConf;
					}
				});
				
				if(foundPrototype)	chosenPrototype=foundPrototype;
			}
			
		
		}else if(prototypesInstanciation.randomInstanciation){
		
			// If we want to choose from random prototypes (ie. if we have encountered no static prototype and none is provided as parameter) :
			if(!chosenPrototype){ // Case on-move instantiation :
				// We instanciate yet another single random object among array values:
				chosenPrototype= { value:Math.getRandomInArray(prototypesInstanciation.randomInstanciation) };
			}

		}else{
			
			// Actually equivalent to {... randomInstanciation:["<a single value>"]} )
			if(!chosenPrototype){  // Case on-move instantiation :
				// We instanciate a determined object :
				chosenPrototype= { value:prototypesInstanciation };
			}
			

		}

		var item=getSingleInstanceWithPrototype(className, chosenPrototype, null, allPrototypesConfig);
		collection.push(item);
		
		
		return {item:item, zone:getSpawningZone(chosenPrototype)};
	}
	
	
	/*public*/function getSingleInstanceWithPrototype(
			classNameParam,
			chosenPrototypeParam=null,
			/*FOR DIRECT CLASSES DEFINITIONS ONLY :*/subPrototypeConfigParam=null,
			allPrototypesConfig=ALL_PROTOTYPES_CONFIG){
		
		let className=classNameParam ;

		if(chosenPrototypeParam && chosenPrototypeParam.subClass){
			className=chosenPrototypeParam.subClass;
		}

		
		// DOES NOT WORK : var instance= Reflect.construct(className,[],new.target);
		
		// TODO : FIXME : POTENTIAL VULNERABILITY !:
		var instance=eval("new "+className+"()");

		
		var prototypeConfig=subPrototypeConfigParam;
		if(!prototypeConfig){
			
			//DBG
			if(contains(chosenPrototypeParam.value,"platform1"))
				lognow("BUG!!!");
			
			prototypeConfig=allPrototypesConfig[className][chosenPrototypeParam.value];
		}
		
		
		// We loop on the attributes of the prototype config :
		foreach(prototypeConfig,(attribute,key)=>{
			
			
			
			if(!startsWithUpperCase(key)){
				
				// Case plain attribute :

				var attributeName=key;
				if(attributeName==="usesSuper"){

					// If we use the super(parent),
					// then that means this instance prototype is basically its parent prototype :
					
					var prototypedParentObject=getSingleInstanceWithPrototype(className, {value:attribute}, null, allPrototypesConfig );

					instance=prototypedParentObject;
					instance.usesSuper=attribute;
					
					
				}else{
					
					// simple attribute :
					if(isString(attribute)){
						if(contains(attribute,"->")){
							// If we have a numeric range
							instance[attributeName]=Math.getRandomInRange(attribute);
						}else if(contains(attribute,"=>")){
							// If we have a string range
							instance[attributeName]=Math.getMinMax(attribute,"=>");
						}else{ // Simple string :
							instance[attributeName]=attribute;
						}
					}else if(isNumber(parseFloat(attribute))){
						instance[attributeName]=parseFloat(attribute);
					}else if(!nothing(attribute.random)){
						// If we have an array as attribute, we choose one among all the values it holds :
						instance[attributeName]=Math.getRandomInArray(attribute.values);
					
					
					}else if(typeof(attribute)==="object" && !isArray(attribute)){ // Plain objects (only)...:

						// We have to clone the prototype simple object attribute :
						let clonedAttribute=cloneObject(attribute,true);
						instance[attributeName]=clonedAttribute;

						
					}else{ // Primitives, strings, arrays...
						
						instance[attributeName]=attribute;
						
					}

				}
				
			}else{
				
				// Case Class config attribute :
				
				var attributeName=uncapitalize(key);

				
				// If we have an array of prototypes names :
				let prototypedSubObject=null;
				
				//if(!nothing(attribute.cardinality)){
				if(attribute.cardinality){
				
					let prototypeClassName=key;
					var prototypesConfigsObject=attribute;
					var cardinality=prototypesConfigsObject.cardinality;
					
					if(isCardinalityCollection(cardinality))	attributeName+="s";
					
					// Sub-object attribute:
					// Here, attributes holds the prototypes names array or the prototype single value
					prototypedSubObject=getInstancesWithPrototypesConfig(instance, attributeName, prototypeClassName, prototypesConfigsObject, allPrototypesConfig);
					
					
				}else if(isArray(attribute)){ // Case we have an array of direct class definitions :
					
					attributeName+="s";

					let subPrototypeClassName=key;
					let subPrototypeConfigs=prototypeConfig[subPrototypeClassName];
					
					prototypedSubObject=[];
					foreach(subPrototypeConfigs,(subPrototypeConfig)=>{
						let item=getSingleInstanceWithPrototype(subPrototypeClassName, null, subPrototypeConfig, allPrototypesConfig);
						prototypedSubObject.push(item);
					});
					
					
				}else{ // Else if we have a single direct class definition :
					
					var subPrototypeClassName=key;
					
					var subPrototypeConfig=prototypeConfig[subPrototypeClassName];
					prototypedSubObject=getSingleInstanceWithPrototype(subPrototypeClassName, null, subPrototypeConfig, allPrototypesConfig);

				}
				
				// If we should, then we create a link to the parent :
				if(GAME_CONFIG.allowParentLinkForChildren){
//			if(prototypeConfig.allowParentLinkForChildren){
					if(!isArray(prototypedSubObject)){
						prototypedSubObject.parent=instance;

						// Abstract prototyped object are NEVER initialized :
						if(prototypedSubObject.initWithParent && !contains(prototypedSubObject.prototypeName,ABSTRACT_PREFIX)){
							prototypedSubObject.initWithParent();
						}

						
						
					}else{
						foreach(prototypedSubObject,(subItem)=>{
							subItem.parent=instance;

							// Abstract prototyped object are NEVER initialized :
							if(subItem.initWithParent && !contains(subItem.prototypeName,ABSTRACT_PREFIX)){
								subItem.initWithParent();
							}


						});
					}
				}
				
				
				instance[attributeName]=prototypedSubObject;
			}
			
		},null,(i1,i2)=>{ 
			// Classes objects must be processed at the end, since they can rely on parent's simple attributes :
			let isClass1=startsWithUpperCase(i1.key);
			let isClass2=startsWithUpperCase(i2.key);
			if(!isClass1 && isClass2)	return -1; // «good order»
			if(isClass1 && !isClass2)	return 1;
			return 0;
		});

		
		if(!nothing(chosenPrototypeParam) && !nothing(chosenPrototypeParam.value)){
			instance.prototypeName=chosenPrototypeParam.value;
		}
		
		// Abstract prototyped object are NEVER initialized :
		
		if(instance.init && (!chosenPrototypeParam/*(case no prototype initialization)*/ || !contains(chosenPrototypeParam.value,ABSTRACT_PREFIX))){
			instance.init(GAME_CONFIG);
		}
		
		return instance;
	};
	
	
	// For prototypes instantiation only :
	function getInstancesWithPrototypesConfig(
			parentObject,
			attributeName,
			classNameParam,
			prototypesConfigsObject,
			allPrototypesConfig=ALL_PROTOTYPES_CONFIG){
		
		
		let className=classNameParam;
		// The «subClass» eventual attribute in prototype class config overrides and precises the trivial (ie. parent) className : 
		if(prototypesConfigsObject && prototypesConfigsObject.subClass){
			className=prototypesConfigsObject.subClass;
		}

		
		var cardinality=prototypesConfigsObject.cardinality;
		var instanceSingleObjectOrCollection=null;
		let numberToPreInstanciate=0;

		
		
		
		if(!isCardinalityCollection(cardinality)){ // case SINGLE ATTRIBUTE :
			
			// UNUSEFUL (but true) : 
			// numberToPreInstanciate=1;
			
			// NO : WE DON'T PLACE THEM YET
//		let spawningLocation={x:0,y:0};
			
			// We calculate here the chosen prototype to instanciate if we have no collection :
			let chosenPrototype=null;
			if(cardinality.prototypesInstanciation.staticInstanciation){
				if(isArray(cardinality.prototypesInstanciation.staticInstanciation)){
					if(!empty(cardinality.prototypesInstanciation.staticInstanciation)){
						// We look into the first (and only) config element :
						let conf=cardinality.prototypesInstanciation.staticInstanciation[0];
						
						let protoNameConf=getProtoNameConf(conf);
						
						chosenPrototype=protoNameConf;
						// CAUTION : WE DON'T PLACE THEM YET
						// SO NO :	if(conf.spawningZone)		spawningLocation=getPositionFor2DObject(conf.spawningZone);
					}
				}else{
					let conf=cardinality.prototypesInstanciation.staticInstanciation;
					
					let protoNameConf=getProtoNameConf(conf);

					chosenPrototype=protoNameConf;
					// CAUTION : WE DON'T PLACE THEM YET
					// SO NO :	if(conf.spawningZone)		spawningLocation=getPositionFor2DObject(conf.spawningZone);
				}
				
			}	else if(cardinality.prototypesInstanciation.randomInstanciation && !empty(cardinality.prototypesInstanciation.randomInstanciation)){
				chosenPrototype={ value:Math.getRandomInArray(cardinality.prototypesInstanciation.randomInstanciation) };
			} else if(isString(cardinality.prototypesInstanciation)){
				chosenPrototype={ value:cardinality.prototypesInstanciation };
			}
			
			if(chosenPrototype){

				// (we don't place them yet : cf. gamu-scenery module for further placing)
				
				instanceSingleObjectOrCollection=getSingleInstanceWithPrototype(className, chosenPrototype, null, allPrototypesConfig);
								
				// We set the cardinalities information for this single attribute :
				if(!parentObject.cardinalities)	parentObject.cardinalities={};
				parentObject.cardinalities[attributeName]=cardinality;


				// NO : WE DON'T PLACE THEM YET
//				if(instanceSingleObjectOrCollection.position) {
//					instanceSingleObjectOrCollection.position.setLocation(spawningLocation);
//					if(parentObject.position)	instanceSingleObjectOrCollection.position.setParentPosition(parentObject.position);
//				}
				
			}
			
			
		}else{ // case COLLECTION ATTRIBUTE :
			
			
			/* !!! CAUTION !!!  
			 * WHEN WE ARE NOT IN «preinstanciate» MODE, 
			 * «cardinality» REPRESENTS THE NUMBER OF ELEMENTS AT THE SAME TIME INSTANCIATED IN *DISPLAY AREA* !!!
			 * WHEN WE ARE IN «preinstanciate» MODE, 
			 * IT REPRESENT THE *TOTAL* AMOUNT OF INSTANCIATED OBJECTS IN THE WHOLE MODEL !!!
			 */
			
			
			// We set the cardinalities information for each collection attribute :
			if(!parentObject.cardinalities)	parentObject.cardinalities={};
			parentObject.cardinalities[attributeName]=cardinality;

			
			if(cardinality.value==="fill"){
				
				instanceSingleObjectOrCollection=[];
				const MAX=999;
				
				let zone = getSpawningZone(cardinality);

				if(contains(GAME_CONFIG.projection, "2D")) {
					if(contains(GAME_CONFIG.scroll, "horizontal")) {
						
						// 0- We instanciate just enough to fill the width :
						// (we don't place them yet : cf. gamu-scenery module for further placing)
						var totalWidth=Math.floor( GAME_CONFIG.getResolution().w / GAME_CONFIG.zoom );
			
						let fillInstanciationMargin= Math.floor( nonull(cardinality.fillInstanciationMargin,0) / GAME_CONFIG.zoom );
						
						var widthCount=0;
						for(var i=0;i<MAX && widthCount < totalWidth + fillInstanciationMargin;i++){

							// We instanciate the object randomly :
							let itemAndZone=instanciateAndAddToCollection(instanceSingleObjectOrCollection,
																														className,
																														cardinality.prototypesInstanciation,
																														null,
																														allPrototypesConfig);
							let elementInstance=itemAndZone.item;
							if(!zone)	zone=elementInstance.zone;
							
							widthCount+= (elementInstance.size.w + (zone?nonull(zone.margin,0):0))  ;
							
						}
						
					}
				}
				
			}else if(cardinality.value==="staticGroups"){ // Static groups cardinality :
				
				
				instanceSingleObjectOrCollection=[];
				
				// (we don't place them yet : cf. gamu-scenery module for further placing)
				let protos=cardinality.prototypesInstanciation;
				if(protos.staticInstanciation){
					
					foreach(protos.staticInstanciation,(protoConf)=>{
						
						let protoNameConf=getProtoNameConf(protoConf);

						// We calculate how many objects we have to preinstanciate in this collection :
						numberToPreInstanciate=1;
						if(protoConf.preinstanciate && protoConf.value)
							numberToPreInstanciate=getNumberToPreInstanciate(protoConf);
						
						for(let i=0;i<numberToPreInstanciate;i++){
							instanciateAndAddToCollection(instanceSingleObjectOrCollection, 
																						className,
																						cardinality.prototypesInstanciation,
																						protoNameConf,
																						allPrototypesConfig);
						}
						
					});

					
				}
				
				
			}else if(cardinality.value==="randomGroups"){ // Random groups cardinality :
				
				
				instanceSingleObjectOrCollection=[];
				
				// (we don't place them yet : cf. gamu-scenery module for further placing)
				let protos=cardinality.prototypesInstanciation;
				if(protos.randomInstanciation){
					
					let protoConf=Math.getRandomInArray(protos.randomInstanciation);
					
					let protoNameConf=getProtoNameConf(protoConf);

					// We calculate how many objects we have to preinstanciate in this collection :
					numberToPreInstanciate=1;
					if(protoConf.preinstanciate && protoConf.value)
						numberToPreInstanciate=getNumberToPreInstanciate(protoConf);
					
					for(let i=0;i<numberToPreInstanciate;i++){
						instanciateAndAddToCollection(instanceSingleObjectOrCollection,
																					className,
																					cardinality.prototypesInstanciation,
																					protoNameConf,
																					allPrototypesConfig);
					}
					
					
					
				}
				
				
			}else{ // Ratio or interval collections cardinalities :
				
				
				instanceSingleObjectOrCollection=[];
				let numberToPreInstanciate=getNumberToPreInstanciate(cardinality);
				// We instanciate the objects randomly or not according if we have to :
				for(var i=0;i<numberToPreInstanciate;i++){
					// If no chosen prototype parameter is passed, then it means we want it to be calculated with the prototype instantiation information !
					instanciateAndAddToCollection(instanceSingleObjectOrCollection,
																			  className,
																			  cardinality.prototypesInstanciation,
																			  null,
																			  allPrototypesConfig);							
				}
				
			}
			

		}
		
		return instanceSingleObjectOrCollection;
	};
	



	
	

	
	/*private*/function isCardinalityCollection(cardinality){
		var isCollection=true;
		let val=cardinality.value;
		if((val==="1" || val===1 ) && val!=="staticGroups" && val!=="randomGroups"){
			isCollection=false;
		}
		return isCollection;
	};

	// ---------------------------------------------------
	
	

	
	// The instantiation manager object instance :
	let self= {
		getSingleInstanceWithPrototype:getSingleInstanceWithPrototype,
		instanciateAndAddToCollection:instanciateAndAddToCollection,
		
		createLevelState : function(levelName, levelConfig, allPrototypesConfig=ALL_PROTOTYPES_CONFIG) {
			
//			let level={
			// We store the level config inside the level :
//						config: levelConfig,
//						isCinematic: levelConfig.isCinematic, 
//						limits: levelConfig.limits,
//						levelName:levelName,
//			};
			
			let level=new GameLevel(levelConfig, levelConfig.isCinematic, levelConfig.limits, levelName);


			// Level is the absolute root object of the model (so it has a special treatment) :
			foreach(levelConfig,(configItem,className)=>{

				if(!startsWithUpperCase(className))	return "continue";
				
				
				// We get the prototype for this root class name :
				let prototypeLevelConfigObject=levelConfig[className];
				let cardinality=prototypeLevelConfigObject.cardinality;

				let attributeName=uncapitalize(className);
				if(isCardinalityCollection(cardinality))	attributeName+="s";
				
				level[attributeName]=getInstancesWithPrototypesConfig(level, attributeName, className, prototypeLevelConfigObject, allPrototypesConfig);
				
			});
			
			return level;
		}
	
	
	
	
	
	};
	
	
	return self;
	

}

