/*abstract*/Model = class_({

	initialize : function(/*NULLABLE*/saverLoader) {

		// CONSTANTS
		this.DEFAULT_OWNER_KEY="default_owner";

		// ATTRIBUTES
		this.objects=new Array();// ModelObjects

		// COMPOMENTS
		if (!nothing(saverLoader))
			this.saverLoader = saverLoader;
		else
			this.saverLoader = getSaverLoader(saverLoaderLocalStorage, "JSON");

	}

	,save : function(/*NULLABLE*/ownerKey) {
		var key=this.DEFAULT_OWNER_KEY;
		if(nothing(ownerKey)){
			key=ownerKey;
		}
		
		// (JSON string)

		var savedString="{objects:[";
		var comma=false;
		for(var i=0;i<this.objects.length;i++){
			if(comma)	savedString+=",";
			else			comma=true;

			var o=this.objects[i];
			
			savedString+=stringifyObject(o);
		}
		savedString+="]}";
		
		this.saverLoader.save(key,savedString);
		
	}
	,load : function(/*NULLABLE*/ownerKey) {
		var key=this.DEFAULT_OWNER_KEY;
		if(nothing(ownerKey)){
			key=ownerKey;
		}
		var loadedString=this.saverLoader.load(key);
		
		// (JSON string)
		var m=parseJSON(loadedString);
	
		this.objects=m.objects;
	}

});
