
View = class_({

	initialize : function(canvasId,flow) {

		// CONSTANTS
		this.MARGIN_PIXELS=10;
		this.SPACING_PIXELS=5;
		
		this.CENTER="center";
		this.DEFAULT_BUTTON_POSITION=this.CENTER;
		this.VERTICAL="vertical";
		this.DEFAULT_BUTTON_CRAWLING=this.VERTICAL;

		// ATTRIBUTES
		this.display = getDisplay(paperjs, canvasId);
		this.flow=flow;
		
		// Layers
		this.layers={};
		
		
		this.constr();

	}
	,constr : function() {}


	,init:function(controller,model){
		this.controller=controller;
		this.model=model;
		
		// Set layers :
		var view=paper.view;
		var foregroundUILayer = new paper.Layer({
	    position: view.center
		});
		var mainUILayer = new paper.Layer({
	    position: view.center
		});
		var backgroundUILayer = new paper.Layer({
	    position: view.center
		});
		
		var foregroundLayer = new paper.Layer({
	    position: view.center
		});
		var mainLayer = new paper.Layer({
	    position: view.center
		});
		var backgroundLayer = new paper.Layer({
	    position: view.center
		});
		
		this.layers.ui={foreground:foregroundUILayer,main:mainUILayer,background:backgroundUILayer};
		this.layers.play={foreground:foregroundLayer,main:mainLayer,background:backgroundLayer};
		
		
		// Display the first page :
		this.currentPage=this.getFlowFirstPage(this.flow);
		
		this.initDrawButtons(this.currentPage);
		
		this.refresh();
  	
		
		
		var self=this;
		// Paper event :
		window.onresize=function(event) {
			
			self.display.resizeToWindow();
  		
			self.placeButtons(self.currentPage);
			
  		self.refresh();
			
		};
		
		
	}

	
	// METHODS
	,getFlowFirstPage:function(flow){
  	for(var key in flow){
  		if(!flow.hasOwnProperty(key))	continue;
  		return flow[key];
  	}
  	return null;
	}

	,initDrawButtons:function(flowPage){
		
		var self=this;
		
		var buttons=flowPage.buttons;
		if(nothing(buttons))	return;
		
		// First : group them by position :
  	this.buttonsByPositions={};
		for(var key in buttons){
  		if(!buttons.hasOwnProperty(key))	continue;
  		var b=buttons[key];
  		
  		var position=this.DEFAULT_BUTTON_POSITION;
  		if(!nothing(b.position)) position=b.position;
  		if(!this.buttonsByPositions[position])	this.buttonsByPositions[position]=new Array();
  		
  		var displayButton=this.display.createButton(b.label);
  		b.displayer=displayButton;
  		this.buttonsByPositions[position].push(b);

  		// Add button to concerned ui layer :
  		this.layers.ui.main.addChild(b.displayer);
  		
  		// Link to the corresponding controller click action :
  		if(!nothing(b.action) && !nothing(this.controller[b.action])){
  			b.displayer.onClick=function(event){
  				self.controller[b.action]();
  			};
  		}
		}
		
		// Second : calculate buttons positions by position group :
		this.placeButtons(flowPage);
		
	}
	

	,placeButtons:function(flowPage){
		
		var buttons=flowPage.buttons;
		if(nothing(buttons))	return;
	
		for(var key in this.buttonsByPositions){
  		if(!this.buttonsByPositions.hasOwnProperty(key))	continue;
  		var bp=this.buttonsByPositions[key];

  		for(var i=0;i<bp.length;i++){
  			var b=bp[i];
  			var crawling=this.DEFAULT_BUTTON_CRAWLING;
  			if(!nothing(b.crawling))	crawling=b.crawling;
  			
  			
  			if(key==this.CENTER
  					// TODO : Develop... : manage the other position configuration cases !
  					|| true
  			){
  				
  				var startX=Math.round(this.getWidth()/2);
  				var startY=Math.round(this.getHeight()/2);
  				
  				var x;
  				var y;
  				
  				var buttonWidth=b.displayer.bounds.width;
  				var buttonHeight=b.displayer.bounds.height;
  				
  				if(crawling===this.VERTICAL){
  					x=startX;
  					y=startY+i*(buttonHeight+this.SPACING_PIXELS);
  				}else{
  					x=startX+i*(buttonWidth+this.SPACING_PIXELS);
  					y=startY;
  				}
  				
  				this.display.placeObject(b.displayer,x,y);
  			}

  			
  		}
  		
  	}
	
		
	}
	
	
	
	,refresh:function(){
		
		this.placeButtons(this.currentPage);

		this.display.refresh();
	}

	/*private*/,getWidth:function(){
		return this.display.getWidth();
	}
	
	/*private*/,getHeight:function(){
		return this.display.getHeight();
	}
		
});
