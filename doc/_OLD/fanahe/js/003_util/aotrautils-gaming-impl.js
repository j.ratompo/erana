
/* ## Utility global methods for erana project
 *
 *
 * !!!!!!!!!!!! DEPENDENCIES !!!!!!!!!!!! 
 * 		- paperjs
 * 		- box2d-html5
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *
 *
 * This set of methods gathers utility gaming development-purpose methods usable in any JS project containing erana project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils-gaming» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Projet Alambic
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 * 
 */

// ==================================== Display IMPLEMENTATION ==================================== 


/*
 * This class is the root display object using paper.js display library
 * */
/*abstract*/ DisplayPaperjs = class_(Display,{

    constr: function() {
        
    		// Get a reference to the canvas object
    		this.mainCanvas = document.getElementById(this.id);
    		
    }

    /*abstract*/,init: function(){
  		
    	// Create an empty project and a view for the canvas:
  		paper.setup(this.mainCanvas);

    
    }
    
    /*abstract*/,refresh: function(){
    	
    	paper.view.update();
    	
  		// Draw the view now:
  		paper.view.draw();
  		
    }
    

		,resizeToWindow:function(){
			this.mainCanvas.width=window.innerWidth;
			this.mainCanvas.height=window.innerHeight;
		}
		
	
    
  	,createButton:function(label){
  		
  		var button=new paper.PointText(new paper.Point(0,0));
  		button.justification = 'center';

  		// TODO :
  		button.fillColor = 'black';
  		button.content = label;
  		
  		return button;
  	
  	}

  	,placeObject:function(object,x,y){

  		object.position.x=x;
  		object.position.y=y;
  		
  	}



    
//    ,drawButton:function(){
//
//   		// Create a Paper.js Path to draw a line into it:
//  		var path = new paper.Path();
//  		
//  		// Give the stroke a color
//  		path.strokeColor = 'black';
//  		
//  		var start = new paper.Point(100, 100);
//  		
//  		// Move to start and draw a line from there
//  		path.moveTo(start);
//  		
//  		// Note that the plus operator on Point objects does not work
//  		// in JavaScript. Instead, we need to call the add() function:
//  		path.lineTo(start.add([ 200, -50 ]));
// 
//    }

  	/*public*/,getWidth:function(){
  		return this.mainCanvas.width;
  	}
  	
  	/*public*/,getHeight:function(){
  		return this.mainCanvas.height;
  	}
    
});


paperjs=new Object();
paperjs.getDisplay=function (canvasId){
	return new DisplayPaperjs(canvasId);
};


// ==================================== Physics IMPLEMENTATION ==================================== 


// ==================================== Model IMPLEMENTATION ==================================== 

/*abstract*/SaverLoaderLocalStorage = class_(SaverLoader,{

	constr : function() {

	}

  ,save:function(key, value){
  	storeString(key, value);
  }
  ,load:function(key){
  	return getStringFromStorage(key);
  }

});


saverLoaderLocalStorage=new Object();
saverLoaderLocalStorage.getSaverLoader=function(format){
	return new SaverLoaderLocalStorage(format);
};

