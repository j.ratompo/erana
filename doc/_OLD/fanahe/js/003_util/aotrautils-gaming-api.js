
/* ## Utility global methods for games development
 *
 *
 * This set of methods gathers utility gaming development-purpose methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Library name : «aotrautils-gaming» 
 * # Library license : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Projet Alambic
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 * 
 */

// ==================================== Display API ==================================== 

/*
 * This class is the root display abstract object
 * */
/*abstract*/ Display = class_({

    initialize: function(id) {
    	this.id=id;
      this.constr();
    }
    ,constr:function() {}


    /*abstract*/,init: function(){}
    /*abstract*/,refresh: function(){}
    /*abstract*/,drawButton:function(label,x,y){}
    /*abstract*/,resizeToWindow:function(){}
    /*abstract*/,getWidth:function(){}
    /*abstract*/,getHeight:function(){}
    

});


function getDisplay(implementation,canvasId){
	if(nothing(implementation)){
		log("Implementation was not found.");
		return null;
	}
	
	var display=implementation.getDisplay(canvasId);
	
	display.init();
	
	return display;
}

// ==================================== Physics API ==================================== 


// ==================================== Model API ==================================== 

/*abstract*/ SaverLoader = class_({

  initialize: function(format) {
  	this.format = format;
  	this.constr();
  }
	,constr: function() {}


	/*abstract*/,save:function(key, value){}
	/*abstract*/,load:function(key){}
	/*abstract*/,drawButton:function(flowButton,x,y){}

  
});


function getSaverLoader(implementation,format){
	if(nothing(implementation)){
		log("Implementation was not found.");
		return null;
	}
	
	return implementation.getSaverLoader(format);
}
