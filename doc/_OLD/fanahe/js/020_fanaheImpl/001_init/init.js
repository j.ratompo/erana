language="fr";

FLOW_CONFIG={
		
		homePage:{buttons:[
			 {label:i18n({"fr":"Commencer","en":"Start"}), action:"startNewGame", goTo:"gamePage"/* Default goTo is ""*/ }
      ,{label:i18n({"fr":"Continuer","en":"Continue"}), action:"continueGame", goTo:"gamePage", visibleIf:"isPreviousGameDetected" }
  		,{label:i18n({"fr":"Crédits","en":"Credits"}), action:"startCredits", goTo:"creditsPage" }
    	,{label:i18n({"fr":"Quitter","en":"Exit"}), action:"exitGame" }
    ]}
		
		,gamePage:{buttons:[
  		{
  			 label:i18n({"fr":"Menu","en":"Menu"})
  			,action:"showMenu"
 	    	,position:"left-right"	/* default position is "center" */
  			,menuItems:[
  				 {label:i18n({"fr":"Reprendre","en":"Resume"}), action:"resumeGame" }
      		,{label:i18n({"fr":"Accueil","en":"Home"}), action:"exitGame", goTo:"homePage" }
      		,{label:i18n({"fr":"Quitter","en":"Exit"}), action:"exitGame" }  				
  			]
  		}
		]}
		
		,creditsPage:{buttons:[
				{label:i18n({"fr":"Accueil","en":"Home"}), action:"exitCredits", goTo:"homePage" }
		]}
		
};


function init() {

	var ERANA_CANVAS_ID = "eranaCanvas";

	var view=new FanaheView(ERANA_CANVAS_ID,FLOW_CONFIG);
	var controller=new FanaheController();
	var model=new Model();
	
	controller.initAll(view,model);
	
	view.refresh();
	
	
};

window.onload=init;
