/* ## Main FUSRODA server : networking easing server functions
 *
 * This is the main entrypoint for the fusroda server.
 * 
 * # Project name : «fusroda server» 
 * # License : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 * 
 * // - DEPENDENCIES INFORMATION :
 * // (DEPENDENCY : nodejs + browserify.org on compile machine )
 * // (DEPENDENCY : socket.io)
 * 
 * 	execute on a bash server : apt-get install nodejs ; npm install socket.io browserify
 */

//Networking management :
// - Websockets :
// server : 
//https = require("https");
//fs = require("fs");
//path = require("path");


// Vanilla javascript utils used by aotra CMS among others :
require("aotrautils");


//==================================================================
//========================== fusroda SERVER ==========================
//
//  DOES NOT WORK : USE Java FusrodaServer instead :
//
//function fusrodaServerInit(/*OPTIONAL*/portParam,/*OPTIONAL*/certPathParam,/*OPTIONAL*/keyPathParam){
//	const fusrodaServer = createFusrodaServer(certPathParam,keyPathParam,portParam);
//	return fusrodaServer;		
//}
//// Nodejs starting (ie. main) method :
//createFusrodaServer().serverManager.start();
//
////===============================================================

//if(typeof(childProcess)==="undefined")	childProcess = require("child_process");
//const unixCmd="java FusrodaServer.class";
//const bufferConfig={ maxBuffer : 1024 };
//childProcess.exec(
//	unixCmd,
//	bufferConfig, 
//	function(err, stdout, stderr){
//		if(err){
//			console.log(err);
//			return;
//		}
//		if(stderr){
//			console.log(stderr);
//			return;
//		}
////	mediaHandler.webcam.imgData=stdout.replace(/[\n]/igm, ""); // base64 string !
//	}
//);


//WebSocket=require("ws");
//const clientSocket=new WebSocket("ws://192.168.0.111:6080",{ rejectUnauthorized:false });
//clientSocket.addEventListener("message",(event)=>{
//	// DBG
//	lognow("RECEIVED : event",event);
//});

socketIO=require("socket.io-client");

// DBG
console.log("socketIO:",socketIO);

const clientSocket=socketIO.connect("ws://192.168.0.111:6080",{timeout: 1000});
//clientSocket.on("message",(message)=>{
//	// DBG
//	lognow("RECEIVED : message:",message);
//});


