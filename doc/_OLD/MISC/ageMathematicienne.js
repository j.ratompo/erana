
					const NOMBRE_MAX_ENFANTS=8;
					const MIN_AGE=8;
					const MAX_AGE=30;
					const AGE_MADAME_MIN_PREMIER_ENFANT=20;
					const AGE_MADAME_MAX_DERNIER_ENFANT=60;
					
					const tester=(agesEnfants)=>{
						let ageAine=Math.maxInArray(agesEnfants);
						let ageBenjamin=Math.minInArray(agesEnfants);
						let ageMadame=Math.productInArray(agesEnfants);
						let numeroBus=Math.sumInArray(agesEnfants);
						
// 						lognow("Solution testée :"+JSON.stringify(agesEnfants));
						
						if(ageMadame-ageAine<AGE_MADAME_MIN_PREMIER_ENFANT)	return false;;
						if(AGE_MADAME_MAX_DERNIER_ENFANT<ageMadame-ageBenjamin)	return false;
						
						lognow("Solution possible : (âge madame : "+ageMadame+", #bus:"+numeroBus+")"+JSON.stringify(agesEnfants));
						return true;
					};
					
					const testerTousAgesPourEnfant=(agesEnfants,indexEnfant)=>{
						for(let ageEnfant=MIN_AGE;ageEnfant<MAX_AGE;ageEnfant++){
							agesEnfants[indexEnfant]=ageEnfant;
							tester(agesEnfants);
							// On reteste tous les ages des précédents :
							for(let indexEnfantPrecedent=0;indexEnfantPrecedent<indexEnfant;indexEnfantPrecedent++){
								testerTousAgesPourEnfant(agesEnfants,indexEnfantPrecedent);
							}
						}
					};

					for(let nombreEnfants=0;nombreEnfants<NOMBRE_MAX_ENFANTS;nombreEnfants++){

						// On essaie toutes les solutions à N enfants : 
						let agesEnfants=new Array(nombreEnfants);
					
						for(let indexEnfant=0;indexEnfant<nombreEnfants;indexEnfant++){
							testerTousAgesPourEnfant(agesEnfants,indexEnfant);
						}
					}