
const CONSUMPTION_REFRESH_RATE_MILLIS=200;

class Locomotive extends DockableEquipment{

	
	constructor() {
		super();
		
		
		this.stopAutomatically=true; // (DEFAULT VALUE)
		
		
		this.speedingRoutine=getMonoThreadedRoutine(this.doEachStepSpeeding,
																								this.isSpeedingFinishedToOverride,
																								this.doOnEndSpeedingToOverride,
																								CONSUMPTION_REFRESH_RATE_MILLIS);
		
	}
	
	// INIT
	
	init(gameConfig){
		super.init(gameConfig);
		
		// Will vary with the drag (TODO)
		this.maxSpeed=this.nativeMaxSpeed;

		
		this.startSpeedingJob();
		
		if(this.gameSmokePits) {
			let self=this;
			foreach(this.gameSmokePits,(s)=>{
				s.init(self.gameConfig);
			});
		}

	}
	
	// METHODS

	// =================== GAME RULES METHODS ===================
	
	// ---------------------------- Costs of functioning ----------------------------
	/*private*/startSpeedingJob(){
		// We start a speeding job :
		this.speedingRoutine.start(this);
	}	
	

	/*private*/stopSpeedingJob(){
		this.speedingRoutine.stop(this);
	}

	// We do not suspend nor resume the speeding job : consumptions are calculated frome the actual speed :
//	/*private*/suspendSpeedingJob(){
//		this.speedingRoutine.suspend();
//	}
//	/*private*/resumeSpeedingJob(){
//		this.speedingRoutine.resume();
//	}
	

	/*private*/doEachStepSpeeding(selfParam,args){

		let train=selfParam.getTrain();
		if(!selfParam.speedsCosts)	return;

		let trainSpeed=train.getSpeed();
		if(trainSpeed===0)	return;

		let trainGoalSpeed=train.getGoalSpeed();
		if(trainGoalSpeed===0)	return;
		
		
		let platform=selfParam.parent;
		let resourcesRepository=platform.resourcesRepository;
		
		
		foreach(selfParam.speedsCosts,(speedCost,speedRangeStr)=>{
			
			if(!contains(speedRangeStr,"=>"))	return "continue";
			if(!Math.isInMinMax(speedRangeStr,trainGoalSpeed,"]]","=>"))	return "continue";

			
			let needs=speedCost.needs;
			let insufficientResourcesList=resourcesRepository.getInsufficientResourcesList(needs);
			
			
			let timeMillis=speedCost.timeSeconds*1000;
			
			if(!speedCost.lastTime || hasDelayPassed(speedCost.lastTime,timeMillis)){
				speedCost.lastTime=getNow();

				if(empty(insufficientResourcesList)){

					resourcesRepository.consumeStocks(needs);

				}else{
					
					// MESSAGE
					window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n({"en":"Cannot function without resources. Stopping","fr":"Impossible de fonctionner sans ressources. Arrêt."}));
					
					selfParam.setSpeedToZero();
					
				}
				
			}
			
			
		});
		
	}
	
	
	

	/*private*/hasEnoughResourceForSpeeding(newGoalSpeed){

		let train=this.getTrain();
		
		let trainSpeed=train.getSpeed();

		if(!this.speedsCosts)	return true;
		if(trainSpeed===0 && newGoalSpeed===0)		return false;

		let platform=this.parent;
		let resourcesRepository=platform.resourcesRepository;
		
		
		return foreach(this.speedsCosts,(speedCost,speedRangeStr)=>{
			
			if(!contains(speedRangeStr,"=>"))	return "continue";
			if(!Math.isInMinMax(speedRangeStr,newGoalSpeed,"]]","=>"))	return "continue";
			
			let needs=speedCost.needs;
			
			return (!resourcesRepository.hasInsufficientResources(needs));
		}) || false;
		
	}
	
	
	
	
	
	
	/*private*/doEachStepSpeedingToOverride(selfParam,args){
		/*DO NOTHING*/
	}
	
	/*private*/isSpeedingFinishedToOverride(selfParam){
		/*DO NOTHING*/
		return false;
	}
	
	/*private*/doOnEndSpeedingToOverride(selfParam){
		/*DO NOTHING*/
	}

	
	

	// Locomotive
	setSpeed(event,speedParam){
		
		if(!this.canRestart())			return;
		
		let speed=event.target.value;

		// DBG
//	this.setSpeedAndRefreshUI(speed,false);
		this.setSpeedAndRefreshUI(speed,true);

	}
	
	setSpeedToZero(){
		this.setSpeedAndRefreshUI(0,true);
	}
	
	setDefaultSpeed(){

		if(!this.canRestart())	return;

		let train=this.getTrain();
		this.setSpeedAndRefreshUI(train.nonStopSpeed,true);
	}
	
	setSpeedMax(){
		if(!this.canRestart())	return;
		this.setSpeedAndRefreshUI(this.maxSpeed,true);
	}

	
	/*private*/canRestart(){

		let train=this.getTrain();

		let collidedStation=train.getCollidedStation();
		if(!this.isStoppedAutomatically() || collidedStation!=null) return true;
		
		if(this.isCloseEnoughToNextStation()){
			// MESSAGE
			window.eranaScreen.sceneManager.uiManager.displayMessage("warn",MESSAGES_DELAY_MILLIS,i18n({"en":"Current aautomatic approach. No Manual commands.","fr":"Approche automatique en cours. Pas de commandes manuelles."}));
			return false;
		}

		return true;
	}
	
	
	
	/*private*/setSpeedAndRefreshUI(speed,updateUI=true){
		
		if(0<speed && !this.hasEnoughResourceForSpeeding(speed)){
			return;
		}
		
		let train=this.getTrain();
		
		train.changeSpeed(speed);
		// Refresh of the UI if needed :
		if(updateUI)
			window.eranaScreen.controlsManager.refreshInfoPad(this);
		
	}

	getTrainGoalSpeed(){
		let train=this.getTrain();
		return Math.round(train.getGoalSpeed());
	}
	
	
	/*public*/setStopAutomatically(event){
		let isChecked=event.target.checked || false;/*(forced to boolean)*/
		this.stopAutomatically=isChecked;
	}
	/*public*/isStoppedAutomatically(){
		return this.stopAutomatically;
	}
	
	/*private*/isCloseEnoughToNextStation(){

		let train=this.getTrain();
		let collidedStation=train.getCollidedStation();
		if(collidedStation)	return true;
		
		let distanceToNextStation=train.getDistanceToNextStationPixels();
		if(!distanceToNextStation/*includes case ==0*/ || distanceToNextStation<0)		return true;

		let inertiaTime=train.inertiaTimeMillis;

		let staticStopDistance= nonull(this.autoStopDistancePixels, inertiaTime * train.getSpeed() * nonull(this.autoStopDistanceFactor,.01));
		
		let isCloseEnough=distanceToNextStation<staticStopDistance;
		
//		// DBG
//		console.log(getNow()+":isCloseEnough:"+isCloseEnough);
		
		return isCloseEnough;
	}

	
	/*private*/checkDistanceToStation(){
		let train=this.getTrain();
		
		if(train.isStopped() || !this.isStoppedAutomatically())		return;
		
		if(this.isCloseEnoughToNextStation()){
			
				
				let distanceToNextStation=train.getDistanceToNextStationPixels();
				if(!distanceToNextStation/*includes case ==0*/ || distanceToNextStation<0)		return;
				
				
				let realDeltaTMillis=window.eranaScreen.refreshRateMillis;

				let collidedStation=train.getCollidedStation();
				let ratioSpeed=distanceToNextStation/realDeltaTMillis;
				if(!collidedStation){
					let calculatedSpeed = Math.min(this.maxSpeed, ratioSpeed);
					// NO : SPECIAL CASE : train.changeSpeed(calculatedSpeed);
					let trainSpeed=train.getSpeed();
					let cappedSpeed=Math.min(calculatedSpeed,trainSpeed);
					
					train.doSetSpeed(cappedSpeed); // train speed cannot go up
//				window.eranaScreen.controlsManager.refreshInfoPad(this);

					if(calculatedSpeed<trainSpeed)	train.setIsAutoBraking(true);
				}else if(Math.round(ratioSpeed)===0){
					train.doSetSpeed(0);
					window.eranaScreen.controlsManager.refreshInfoPad(this);
					
					train.setIsAutoBraking(false);
				}
				
	
		}
	
	}
	
	// UNUSED
	/*public*/getNativeMaxSpeed(){
		return this.nativeMaxSpeed;
	}

	/*public*/getMaxSpeed(){
		return this.maxSpeed;
	}
	
	
	/*private*/getTrain(){
		return this.parent.parent;
	}
	

	/*OVERRIDES*//*public*/getNeededCostsNames(){
		let costsNames=[];

		let train=this.getTrain();

		let trainGoalSpeed=train.getGoalSpeed();

		foreach(this.speedsCosts,(speedCost,speedRangeStr)=>{
			
			if(!contains(speedRangeStr,"=>"))	return "continue";
			
			if(!Math.isInMinMax(speedRangeStr,trainGoalSpeed,
					// CAUTION : This is a special case, different than hasEnoughResourceForSpeeding(...),
					// because we want to be able to gather resources for functionning, even if the locomotive has stopped !
					"[]","=>"))	return "continue";
			
			let needs=speedCost.needs;
			
			if(needs){
				foreach(needs,(need,needName)=>{
					if(!contains(costsNames,needName))	costsNames.push(needName);
				},(needValue)=>{	return	0<needValue; /*we exclude the possible wastes*/	});
			}
			
		});
		
		
		return costsNames;
	}
	
	
	
	/*public*/isWorking(){
		let train=this.getTrain();
		return (0<train.getGoalSpeed());
	}
	

	// --------------------------------------------------------------


	
	/*public*/canDrawSmokePits(){
		return this.isWorking();
	}
	
	/*public*/drawSmokePits(ctx,camera){
		
		if(this.gameSmokePits) {
			
			let self=this;
			foreach(this.gameSmokePits,(s)=>{
				s.updateStateAndDraw(ctx,camera);
			});
			
		}

	}
	
	draw(ctx,camera,lightsManager){
	
		this.checkDistanceToStation();
		
		
		this.speedingRoutine.step(this);


		
		let gameSound=super.getGameSound();
		if(gameSound){
//			let soundSpeed=null; // or 0...
			if(this.isWorking()) {
				gameSound.isAudible=true;
				// CREATES STRANGE PLAYBACK BUG :
//				let train=this.getTrain();
//				soundSpeed=Math.coerceInRange((train.goalSpeed / this.maxSpeed)*1.5, .5, 1.5).toFixed(2);
			}else{
				gameSound.isAudible=false;
			}
//			gameSound.verifyPlayEachStep(camera,soundSpeed);
			gameSound.verifyPlayEachStep(camera);
			
		}

		
		if(!super.draw(ctx,camera,lightsManager))		return false;

		
		return true;
	}
	
	
	

}
