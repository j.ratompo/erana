

// By Thatcher Ulrich http://tulrich.com 2009
//
// This source code has been donated to the Public Domain.  Do
// whatever you want with it.  Use at your own risk.
//
// Awesome cubemaps: http://www.humus.name/index.php?page=Textures&&start=32


const options = {
	draw_backfaces: true,
	whiteout_alpha: 1,
	subdivide_factor: 10.0,
	nonadaptive_depth: 0,
};



function drawQuad(ctx, img, x0, y0, x1, y1, x2, y2, x3, y3, sx0=1, sy0=1, sx1=1, sy1=1, sx2=1, sy2=1) {
	
  ctx.save();

  // Clip the output to the on-screen triangle boundaries.
  ctx.beginPath();
  ctx.moveTo(x0, y0);
  ctx.lineTo(x1, y1);
  ctx.lineTo(x2, y2);
  ctx.lineTo(x3, y3);
  ctx.closePath();
  //ctx.stroke();//xxxxxxx for wireframe
  ctx.clip();

  /*
    ctx.transform(m11, m12, m21, m22, dx, dy) sets the context transform matrix.

    The context matrix is:

    [ m11 m21 dx ]
    [ m12 m22 dy ]
    [  0   0   1 ]

    Coords are column vectors with a 1 in the z coord, so the transform is:
    x_out = m11 * x + m21 * y + dx;
    y_out = m12 * x + m22 * y + dy;

    From Maxima, these are the transform values that map the source
    coords to the dest coords:

    		sy0 (x2 - x1) - sy1 x2 + sy2 x1 + (sy1 - sy2) x0
    [m11 = - -----------------------------------------------------,
   			sx0 (sy2 - sy1) - sx1 sy2 + sx2 sy1 + (sx1 - sx2) sy0

  		  sy1 y2 + sy0 (y1 - y2) - sy2 y1 + (sy2 - sy1) y0
    m12 = -----------------------------------------------------,
    		sx0 (sy2 - sy1) - sx1 sy2 + sx2 sy1 + (sx1 - sx2) sy0

   		 sx0 (x2 - x1) - sx1 x2 + sx2 x1 + (sx1 - sx2) x0
    m21 = -----------------------------------------------------,
   		 sx0 (sy2 - sy1) - sx1 sy2 + sx2 sy1 + (sx1 - sx2) sy0

   			sx1 y2 + sx0 (y1 - y2) - sx2 y1 + (sx2 - sx1) y0
    m22 = - -----------------------------------------------------,
   			 sx0 (sy2 - sy1) - sx1 sy2 + sx2 sy1 + (sx1 - sx2) sy0

   			 sx0 (sy2 x1 - sy1 x2) + sy0 (sx1 x2 - sx2 x1) + (sx2 sy1 - sx1 sy2) x0
    dx = ----------------------------------------------------------------------,
    		sx0 (sy2 - sy1) - sx1 sy2 + sx2 sy1 + (sx1 - sx2) sy0

    		sx0 (sy2 y1 - sy1 y2) + sy0 (sx1 y2 - sx2 y1) + (sx2 sy1 - sx1 sy2) y0
    dy = ----------------------------------------------------------------------]
    		sx0 (sy2 - sy1) - sx1 sy2 + sx2 sy1 + (sx1 - sx2) sy0
  */

  // TODO: eliminate common subexpressions.
  var denom = sx0 * (sy2 - sy1) - sx1 * sy2 + sx2 * sy1 + (sx1 - sx2) * sy0;
  if (denom == 0) {
    return;
  }
  var m11 = - (sy0 * (x2 - x1) - sy1 * x2 + sy2 * x1 + (sy1 - sy2) * x0) / denom;
  var m12 = (sy1 * y2 + sy0 * (y1 - y2) - sy2 * y1 + (sy2 - sy1) * y0) / denom;
  var m21 = (sx0 * (x2 - x1) - sx1 * x2 + sx2 * x1 + (sx1 - sx2) * x0) / denom;
  var m22 = - (sx1 * y2 + sx0 * (y1 - y2) - sx2 * y1 + (sx2 - sx1) * y0) / denom;
  var dx = (sx0 * (sy2 * x1 - sy1 * x2) + sy0 * (sx1 * x2 - sx2 * x1) + (sx2 * sy1 - sx1 * sy2) * x0) / denom;
  var dy = (sx0 * (sy2 * y1 - sy1 * y2) + sy0 * (sx1 * y2 - sx2 * y1) + (sx2 * sy1 - sx1 * sy2) * y0) / denom;

  ctx.transform(m11, m12, m21, m22, dx, dy);

  // Draw the whole image.  Transform and clip will map it onto the
  // correct output triangle.
  //
  // TODO: figure out if drawImage goes faster if we specify the rectangle that
  // bounds the source coords.
  ctx.drawImage(img, 0, 0);
  ctx.restore();
}




function projectPoint(p) {
  if (p.z <= 0) {
    return {x: 0, y: 0, z: p.z};
  } else {
    return {x: p.x / p.z, y: p.y / p.z, z: p.z};
  }
}





// Return two points, one slightly on either side of the midpoint. Use
// this to cover up seams between triangles.
function bisectFat(p0, p1, lenEstimate) {
	var f0 = 0.5 * (lenEstimate + 5) / lenEstimate;
	var f1 = 0.5 * (lenEstimate - 5) / lenEstimate;
	var ps = [
		{
			x: p0.x + (p1.x - p0.x) * f0,
			y: p0.y + (p1.y - p0.y) * f0,
			z: p0.z + (p1.z - p0.z) * f0,
			u: p0.u + (p1.u - p0.u) * f0,
			v: p0.v + (p1.v - p0.v) * f0
		},
		{
			x: p0.x + (p1.x - p0.x) * f1,
			y: p0.y + (p1.y - p0.y) * f1,
			z: p0.z + (p1.z - p0.z) * f1,
			u: p0.u + (p1.u - p0.u) * f1,
			v: p0.v + (p1.v - p0.v) * f1
		}
	];
	return ps;
}



function drawPerspectiveQuadUnclippedSub(canvas_ctx_, image, v0, tv0, v1, tv1, v2, tv2, v3, tv3, depth_count) {
	var edgelen01 =
		Math.abs(tv0.x - tv1.x) +
		Math.abs(tv0.y - tv1.y);
	var edgelen12 =
		Math.abs(tv1.x - tv2.x) +
		Math.abs(tv1.y - tv2.y);
	var edgelen23 =
		Math.abs(tv2.x - tv3.x) +
		Math.abs(tv2.y - tv3.y);
	var edgelen30 =
		Math.abs(tv3.x - tv0.x) +
		Math.abs(tv3.y - tv0.y);
	var zdepth01 =
		Math.abs(v0.z - v1.z);
	var zdepth12 =
		Math.abs(v1.z - v2.z);
	var zdepth23 =
		Math.abs(v2.z - v3.z);
	var zdepth30 =
		Math.abs(v3.z - v0.z);

	var subdiv = (edgelen01 * zdepth01 > options.subdivide_factor) ||
		(edgelen12 * zdepth12 > options.subdivide_factor) ||
		(edgelen23 * zdepth23 > options.subdivide_factor) ||
		(edgelen30 * zdepth30 > options.subdivide_factor);

	if (depth_count) {
		depth_count--;
		if (depth_count == 0) {
			subdiv = false;
		} else {
			subdiv = true;
		}
	}

	// v0       v1
	//   **----*
	//   *     |
	//   |     *
	//   *----**
	// v3       v2
	//


	if (!subdiv) {
		drawQuad(canvas_ctx_, image,
			tv0.x, tv0.y,
			tv1.x, tv1.y,
			tv2.x, tv2.y,
			tv3.x, tv3.y,
			v0.u, v0.v,
			v1.u, v1.v,
			v2.u, v2.v);
		return;
	}

	// Need to subdivide.
	var [v01a, v01b] = bisectFat(v0, v1, edgelen01);
	var [v12a, v12b] = bisectFat(v1, v2, edgelen12);
	var [v23a, v23b] = bisectFat(v2, v3, edgelen23);
	var [v30a, v30b] = bisectFat(v3, v0, edgelen30);
	var [vCCa, vCCb] = bisectFat(v01a, v23b, edgelen12);
	var [vCCc, vCCd] = bisectFat(v23a, v01b, edgelen30);

	var [tv01a, tv01b] = [projectPoint(v01a), projectPoint(v01b)];
	var [tv12a, tv12b] = [projectPoint(v12a), projectPoint(v12b)];
	var [tv23a, tv23b] = [projectPoint(v23a), projectPoint(v23b)];
	var [tv30a, tv30b] = [projectPoint(v30a), projectPoint(v30b)];
	var [tvCCa, tvCCb] = [projectPoint(vCCa), projectPoint(vCCb)];
	var [tvCCc, tvCCd] = [projectPoint(vCCc), projectPoint(vCCd)];

	drawPerspectiveQuadUnclippedSub(canvas_ctx_, image, v0, tv0, v01a, tv01a, vCCa, tvCCa, v30b, tv30b, depth_count);
	drawPerspectiveQuadUnclippedSub(canvas_ctx_, image, v1, tv1, v12a, tv12a, vCCd, tvCCd, v01b, tv01b, depth_count);
	drawPerspectiveQuadUnclippedSub(canvas_ctx_, image, v2, tv2, v23a, tv23a, vCCc, tvCCc, v12b, tv12b, depth_count);
	drawPerspectiveQuadUnclippedSub(canvas_ctx_, image, v3, tv3, v30a, tv30a, vCCb, tvCCb, v23b, tv23b, depth_count);
}

function drawPerspectiveQuadUnclipped(canvas_ctx_, image, v0, v1, v2, v3, depth_count) {
	var tv0 = projectPoint(v0);
	var tv1 = projectPoint(v1);
	var tv2 = projectPoint(v2);
	var tv3 = projectPoint(v3);
	drawPerspectiveQuadUnclippedSub(canvas_ctx_, image, v0, tv0, v1, tv1, v2, tv2, v3, tv3, depth_count);
}




/*public static*/function drawDeformed(ctx, image, points) {

	const p=[];
	const Z=1;


	// points[3]        points[0]
	//  |										|
	//  |										|
	//  |										|
	//  |										|
	//  |										|
	//  |										|
	// points[2]--------points[1]


	p[0]={x: points[0].x, y: points[0].y, z: Z, u: 0, v: 0};
 	p[1]={x: points[1].x, y: points[1].y, z: Z, u: image.width, v: 0};
  p[2]={x: points[2].x, y: points[2].y, z: Z, u: image.width, v: image.height};
  p[3]={x: points[3].x, y: points[3].y, z: Z, u: 0, v: image.height};


	// p[3]        p[0]
	//  |						|
	//  |						|
	//  |						|
	// p[2]--------p[1]
	drawPerspectiveQuadUnclipped(ctx, image, p[0], p[1], p[2], p[3], options.nonadaptive_depth);
}



// DEGUG ONLY :
//function testDraw(ctx, image) {
//	ctx.globalAlpha = options.whiteout_alpha;
//	ctx.fillStyle = '#FFFFFF';
//	ctx.fillRect(0, 0, canvas_elem.width, canvas_elem.height);
//	ctx.globalAlpha = 1;
//
//	const p=[];
////	p[0]={x: 100, y: 400, z: 1, u: 0, v: 0};
//// 	p[1]={x: 600, y: 500, z: 1, u: 771, v: 0};
////  p[2]={x: 500, y: 200, z: 1, u: 771, v: 480};
////  p[3]={x: 200, y: 100, z: 1, u: 0, v: 480};
//
//	// p[3]        p[0]
//	//  |						|
//	//  |						|
//	//  |						|
//	// p[2]--------p[1]
//	p[0]={x: 100, y: 400, z: 1, u: 0, v: 0};
// 	p[1]={x: 600, y: 500, z: 1, u: image.width, v: 0};
//  p[2]={x: 500, y: 200, z: 1, u: image.width, v: image.height};
//  p[3]={x: 200, y: 100, z: 1, u: 0, v: image.height};
//
//
//	drawPerspectiveQuadUnclipped(ctx, image, p[0], p[1], p[2], p[3], options.nonadaptive_depth);
//}
//// DEGUG ONLY :
//window.addEventListener("load", function() {
//	const image=new Image();
//	image.src = "test.png";
//	image.onload = ()=>{
//		var ctx = document.getElementById("canvas").getContext("2d");
//		drawDeformed(ctx, image, [{x:100,y:400},{x:600,y:500},{x:500,y:200},{x:200,y:100}]);
//	};
//});

