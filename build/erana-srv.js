/* ## Utility global methods : networking easing server functions
 *
 * This set of methods gathers utility networking methods usable in any JS project.
 * Several authors of snippets published freely on the Internet contributed to this library.
 * Feel free to use/modify-enhance/publish them under the terms of its license.
 * 
 * # Project name : «erana server» 
 * # License : HGPL(Help Burma) (see aotra README information for details : https://alqemia.com/aotra.js )
 * # Author name : Jérémie Ratomposon (massively helped by his native country free education system)
 * # Author email : info@alqemia.com
 * # Organization name : Alqemia
 * # Organization email : admin@alqemia.com
 * # Organization website :  https://alqemia.com
 * 
 * 
 * // - DEPENDENCIES INFORMATION :
 * // (DEPENDENCY : nodejs + browserify.org on compile machine )
 * // (DEPENDENCY : socket.io)
 * 
 * 	execute on a bash server : apt-get install nodejs ; npm install socket.io browserify
 */

//Networking management :
// - Websockets :
// server : 
https = require("https");
fs = require("fs");
path = require("path");


// Vanilla javascript utils used by aotra CMS among others :
require("aotrautils");


//==================================================================
//========================== erana SERVER ==========================

function eranaServerInit(codeToLoadFilePath=null, sslConfig={/*OPTIONAL*/port:null,/*OPTIONAL*/certPath:null,/*OPTIONAL*/keyPath:null}){

	////
	if(codeToLoadFilePath){
		require(codeToLoadFilePath);
		
		
	}


	const eranaServer = initNodeServerInfrastructureWrapper(
		// On each client connection :
		null,
		// On client finalization :
		function(server){
	
			// When the server receives a on("..." type signal from the client   
			server.receive("protocol", (message, clientSocket)=>{
				
				// TRACE
				console.log("INFO : SERVER : Client has sent a protocol message: ", message);
	
				// ============== CLIENT REGISTRATION PHASE ==============
	
				if(message.type==="1_registerClient"){
					
					let clientID=getUUID("short");
					
					if(nothing(eranaServer.clientsGames)){
						eranaServer.clientsGames={};
					}
					if(nothing(eranaServer.clientsGames[clientID])){
						eranaServer.clientsGames[clientID]={};
					}
					eranaServer.clientsGames[clientID][clientID]={time:getNow(),isFollower:false};// A client is always, by default, in its own game list of participants !
					
					// We place registered client in its own channel :
					const channelID="clients_"+clientID;
					
					// We add the client to a server-side room :
					server.addToRoom(clientSocket, channelID);
					
					
					// TRACE
					lognow("INFO : SERVER : Server aknowledges client «"+clientID+"».");
					lognow("INFO : SERVER : Client joined channel «"+channelID+"».");
					lognow("INFO : SERVER : eranaServer.clientsGames:",eranaServer.clientsGames);
					
					
	    			server.send("protocol", {type:"2_clientRegistered",clientID:clientID},null,clientSocket);
	    		
	    		
				}else if(message.type==="1_joinGame"){
					// ============== CLIENT JOIN GAME PHASE ==============
	
					// A client has expressed its will to join a game :
					
					const clientID=message.clientID;
					const clientIDToJoin=message.clientIDToJoin;
					
					
					let errorMessage=null;
					if(!eranaServer.clientsGames[clientIDToJoin]){
						errorMessage="ERROR : SERVER : Could not find registered client ID «"+clientIDToJoin+"». Other clients cannot join its game.";
					}else if(eranaServer.clientsGames[clientIDToJoin][clientID] && eranaServer.clientsGames[clientIDToJoin][clientID].isFollower){
						errorMessage="ERROR : SERVER : Could not join client ID «"+clientIDToJoin+"» for client «"+clientID+"» because it's already a follower.";
					}
					
					if(errorMessage){
						// TRACE
						lognow(errorMessage);
						
						// DBG
						lognow("eranaServer.clientsGames:",eranaServer.clientsGames);
						lognow("clientID:",clientID);
						
						
						server.send("protocol", {type:"2_requestModel",error:errorMessage}, "clients_"+clientIDToJoin);
						return;
					}	
					
					// 1- We place a model request for joining the game to join for this client :
					if(!eranaServer.clientsPendingModelRequests){
						eranaServer.clientsPendingModelRequests={};
					}
					if(!eranaServer.clientsPendingModelRequests[clientIDToJoin]){
						eranaServer.clientsPendingModelRequests[clientIDToJoin]=[];
					}
					
					let pendingRequest={clientID:clientID, isFullfilled:false};
					
					eranaServer.clientsPendingModelRequests[clientIDToJoin].push(pendingRequest);
				
					
					// Then we ask for the model to the concerned client, who has the right model :
					server.send("protocol", {type:"2_requestModel"}, "clients_"+clientIDToJoin);
	    		
					// TRACE
					lognow("INFO : SERVER : Client «"+clientIDToJoin+"» is asked its model.");
					
	    		
				}else if(message.type==="3_provideModel"){
				
					// 2- We retrieve the model to join :
					
					let uncycledModelToJoin=message.model;
					let clientIDToJoin=message.clientIDToJoin;
					
					if(!uncycledModelToJoin){
						// TRACE
						lognow("WARN : No model to join for clientIDToJoin «"+clientIDToJoin+"»..");
						return;
					}
					
					// Then we communicate the model to the client who wants to join the game :
					
					let pendingModelRequestsForThisGameID=eranaServer.clientsPendingModelRequests[clientIDToJoin];
					if(empty(pendingModelRequestsForThisGameID)){
						// TRACE
						lognow("WARN : No pending model requests for clientIDToJoin «"+clientIDToJoin+"».");
						return;
					}
					
					
					// DBG
					lognow(">>>eranaServer.clientsPendingModelRequests:",eranaServer.clientsPendingModelRequests);
					lognow(">>>pendingModelRequestsForThisGameID:",pendingModelRequestsForThisGameID);
					
					
					foreach(pendingModelRequestsForThisGameID,(request)=>{
						
						let clientID=request.clientID;
						let isFullfilled=request.isFullfilled;
						
						// DBG
						lognow(">>>sending to :",clientID);
	
						
						server.send("protocol", {type:"4_clientJoined", clientIDToJoin:clientIDToJoin, model: uncycledModelToJoin}, "clients_"+clientID);
						
						request.isFullfilled=true;
						eranaServer.clientsGames[clientIDToJoin][clientID]={time:getNow(),isFollower:true};					

						
					},(request)=>{		return !request.isFullfilled;	});
				
				
				}else if(message.type==="5_followerClientIsReady"){
				
					const clientID=message.clientID;
					const clientIDToJoin=message.clientIDToJoin;
				
					// We notice the other clients in the game :
					let gameParticipants=eranaServer.clientsGames[clientIDToJoin];
					if(!gameParticipants)	return;
					
					// We send them the updated participants list :
					const participantsClientsIDs=Object.keys(gameParticipants);
					foreach(gameParticipants,(participant, currentClientID)=>{
						server.send("protocol", {type:"6_followerClientIsReadyNotice", newClientID:clientID, participantsClientsIDs:participantsClientsIDs}, "clients_"+currentClientID);
					});					
	
				}else if(message.type==="1_leaveGame"){
				
					let clientID=message.clientID;
					let clientIDToLeave=message.clientIDToLeave;
					
					// We delete the client from the game it used to be in :
					delete eranaServer.clientsGames[clientIDToLeave][clientID];					
					
					server.send("protocol", {type:"2_clientLeft"}, "clients_"+clientID);
					
				}
				
			});
		
				
			// ============== CLIENT ACTIONS PHASE ==============
			// 2)
			server.receive("clientProcessings", function(data, clientSocket){
		
				const originatingClientID=data.originatingClientID;
				const originatingClientIsFollower=data.isFollower;
				const broadcastTo=nonull(data.broadcastTo,"excludeOrigin");
			
				// TRACE
				console.log("INFO : SERVER : Client has sent actions to broadcast : ",data);

				// We broadcast to all concerned clients, except the originating ciient : 

			
				// If processing's originating client is a leader in the game :
				if(!originatingClientIsFollower){ // (Leader :)
					let followers=eranaServer.clientsGames[originatingClientID];
					if(empty(followers))	return;
					
					// Then we broadcast the processing to all its follower, except itself : (ie. its children)
					foreach(followers,(followerInfo, followerClientId)=>{
							
							// DBG
							lognow(">>>(CASE ORIGINATING CLIENT IS LEADER) sending processing to :",followerClientId);
							
							server.send("clientProcessings", data, "clients_"+followerClientId);
							
					},(followerInfo, followerClientId)=>{
						// WE DON'T BROADCAST TO THE ORIGINATING CLIENT,					
						// but we broadcast it only to the followers :
						return (broadcastTo=="all" || (followerClientId!=originatingClientID && followerInfo.isFollower));
					});
					
				}else{  // (Follower :)
					// If originating client is a follower :
					// We find the first game this client is in as a follower :
					let gameParticipantsListClientIsIn=foreach(eranaServer.clientsGames,(gameParticipantsList, gameClientId)=>{
						const participantFound=foreach(gameParticipantsList,(participantInfo, participantClientId)=>{
							if(participantClientId==originatingClientID)	return true;
						},(participantInfo, participantClientId)=>{		return participantInfo.isFollower;	});
						if(participantFound)	return gameParticipantsList;
					});
					if(!gameParticipantsListClientIsIn)	return;
					
					
					// Then we broadcast the processing to all its follower, except itself : (ie. its siblings)
					foreach(gameParticipantsListClientIsIn,(participantInfo, participantClientId)=>{
							
							// DBG
							lognow(">>>(CASE ORIGINATING CLIENT IS FOLLOWER) sending processing to :",participantClientId);
							
							
							server.send("clientProcessings", data, "clients_"+participantClientId);
							
					},(participantInfo, participantClientId)=>{
						// WE DON'T BROADCAST TO THE ORIGINATING CLIENT,					
						// but we broadcast it to the leader client, too :
						return (broadcastTo=="all" || (participantClientId!=originatingClientID));
					});
				
				}
		
				
		
			});
			
			
			
			addOtherServerListerners(server);
			
	
	},sslConfig.port, sslConfig.certPath, sslConfig.keyPath);
	
	

	return eranaServer;
}


// UNUSED :
/*private*/addOtherServerListerners=function(server){
	
//	server.receive("???", (message, clientSocket)=>{
//	});

}



//===============================================================

let codeToLoadFilePath=null;
process.argv.forEach(function (val, i){
	if(i<=1)	return;
	else if(i==2)	codeToLoadFilePath=val;
});


// Nodejs starting (ie. main) method :
eranaServerInit(codeToLoadFilePath).serverManager.start();


