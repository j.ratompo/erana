
specificProjectParam="$1"




echo "=== Game project compile script for project «$specificProjectParam» ==="	
echo "(Please run this script in the «./build» folder.)"	
echo "Example usage : sh compile.sh myproject"


# NOT POSSIBLE WITH BASH SCRIPTING LANGUAGE:
#currentPath="$(pwd)"
#if [ $currentPath != *build ]; then
#	echo "ERROR : Please run this script in «build» folder."	
#	exit 0;
#fi


if [ -z "$specificProjectParam"  ]; then
	echo "ERROR : Usage : 	sh compile.sh <game project name>"	
	exit 0;
fi


# /////////////// Config ///////////////

VERSION_FILE="../VERSION.txt"
SRC_DIR="src.erana"
JS_DIR="js"
BUILD_DIR="."
DEPLOY_DIR="$SRC_DIR/$specificProjectParam"
COMPILED_FILENAME="erana.$specificProjectParam.build.js"
COMPILED_COMMONS_FILENAME="erana.commons.build.js"

COMMON_LIBRARY_PATH="../../aotra/src/aotra/SHARED/"
COMMON_GAMU_LIBRARY_PATH="../$SRC_DIR/SHARED/"
COMMON_EXTERNAL_LIBRARY_PATH="../$SRC_DIR/LIB/"

COMMON_EXTERNAL_LIBRARY_FILES_FILTER="*.js"
COMMON_LIBRARY_FILES_FILTER="aotrautils.*.js"
COMMON_GAMU_LIBRARY_FILES_FILTER="*.js"
JS_FILES_FILTER="*.js"
EXCLUDED_JS_PATH_FILTER="*.OFF*"

# /////////////// End of Config ///////////////

#if [ -n "$updateUtils"  ]; then   OR :
#if [ ! -z "$updateUtils"  ]; then
	
echo "********************* Starting aotra compilation... *********************"
oldPath="$(pwd)"
cd ../../aotra/build
sh compile.sh
cd $oldPath

#fi


echo "********************* Starting erana compilation at «$oldPath»... *********************"

compiledFilePath="$BUILD_DIR/$COMPILED_FILENAME"
compiledCommonsFilePath="$BUILD_DIR/$COMPILED_COMMONS_FILENAME"

compilationDate=$(date +%d/%m/%Y-%H:%M:%S)
version=$(cat $VERSION_FILE)
JSRootDir="$DEPLOY_DIR/$JS_DIR"

# Cleaning :

rm -f $compiledFilePath
rm -f $compiledCommonsFilePath


# ===========================================================================

echo ""
echo "	0] Adding date and version..."
echo ""
echo "/*"															  		> $compiledCommonsFilePath
echo "Erana javascript COMMONS code file version $version at $compilationDate"		>> $compiledCommonsFilePath
echo "*/" 															  	>> $compiledCommonsFilePath
echo ""																			>> $compiledCommonsFilePath


echo ""
echo "	1] Compiling common libraries in $COMMON_LIBRARY_PATH and $COMMON_GAMU_LIBRARY_PATH ..."
echo ""


echo "/*"															  		>> $compiledCommonsFilePath
echo "1.0) External library files : "				>> $compiledCommonsFilePath
find "$COMMON_EXTERNAL_LIBRARY_PATH" -name "$COMMON_EXTERNAL_LIBRARY_FILES_FILTER" | sort | xargs echo 		>> $compiledCommonsFilePath
echo "*/" 															  	>> $compiledCommonsFilePath
echo "" 															  		>> $compiledCommonsFilePath
find "$COMMON_EXTERNAL_LIBRARY_PATH" -name "$COMMON_EXTERNAL_LIBRARY_FILES_FILTER" | sort | xargs cat -s 	>> $compiledCommonsFilePath
echo ""


echo "/*"															  		>> $compiledCommonsFilePath
echo "1.1) Common library files : "					>> $compiledCommonsFilePath
find "$COMMON_LIBRARY_PATH" -name "$COMMON_LIBRARY_FILES_FILTER" | sort | xargs echo 		>> $compiledCommonsFilePath
echo "*/" 															  	>> $compiledCommonsFilePath
echo "" 															  		>> $compiledCommonsFilePath
find "$COMMON_LIBRARY_PATH" -name "$COMMON_LIBRARY_FILES_FILTER" | sort | xargs cat -s 	>> $compiledCommonsFilePath
echo ""


echo "/*"															  		>> $compiledCommonsFilePath
echo "1.2) Common gamu library files : "		>> $compiledCommonsFilePath
find "$COMMON_GAMU_LIBRARY_PATH" -name "$COMMON_GAMU_LIBRARY_FILES_FILTER" | sort | xargs echo 	>> $compiledCommonsFilePath
echo "*/" 															  	>> $compiledCommonsFilePath
echo "" 															  		>> $compiledCommonsFilePath
find "$COMMON_GAMU_LIBRARY_PATH" -name "$COMMON_GAMU_LIBRARY_FILES_FILTER" | sort | xargs cat -s 	>> $compiledCommonsFilePath

# ===========================================================================

echo ""
echo "	0] Adding date and version..."
echo ""
echo "/*Erana javascript SPECIFIC code file version $version at $compilationDate */"		> $compiledFilePath
echo ""																			>> $compiledFilePath

echo ""
echo "	1] Compiling all javascript files from ../$JSRootDir ..."
echo ""
# TRACE
find "../$JSRootDir" -name "$JS_FILES_FILTER" ! -path "$EXCLUDED_JS_PATH_FILTER" | sort | xargs echo

echo "/*"													  				>> $compiledFilePath
echo "	2] Javascript files : (compiled directory : ../$JSRootDir) "		>> $compiledFilePath
find "../$JSRootDir" -name "$JS_FILES_FILTER" ! -path "$EXCLUDED_JS_PATH_FILTER" | sort | xargs echo 		>> $compiledFilePath
echo "*/" 															  	>> $compiledFilePath
echo "" 															  		>> $compiledFilePath
find "../$JSRootDir" -name "$JS_FILES_FILTER" ! -path "$EXCLUDED_JS_PATH_FILTER" | sort | xargs cat -s 		>> $compiledFilePath

# ===========================================================================


echo ""
echo "	3] Duplicating compiled javascript files to $DEPLOY_DIR ..."
echo ""

rm -f ../$DEPLOY_DIR/$COMPILED_FILENAME
cp -f $compiledFilePath "../$DEPLOY_DIR/"

rm -f ../$DEPLOY_DIR/$COMPILED_COMMONS_FILENAME
cp -f $compiledCommonsFilePath "../$DEPLOY_DIR/"


echo "********************* End of compilation. *********************"


