echo "=== Game project deploy script ==="

specificProjectParam="$1"
if [ -z "$specificProjectParam"  ]; then
	echo "Usage : 	sh deploy.sh <game project name> [<deploy mode: local/distant/bundle>] [medias]"
	exit 0;
fi

deployMode="$2"

isMedias="$3"


# /////////////// Config ///////////////

SRC_DIR="src.erana/$specificProjectParam/"

SSH_LOGIN="user"
WEB_SERVER="192.168.0.112"
REMOTE_PUBLIC_WEB_HOME="/home/user/sites/erana"

LOCAL_DEPLOY_DIR="/var/www/html/erana/"

COMPILED_JS_FILENAME="erana.$specificProjectParam.build.js"
COMPILED_COMMONS_JS_FILENAME="erana.commons.build.js"

FAVICON_FILENAME="favicon.ico"

HTML_INDEX_FILENAME="index.html"
CSS_FILES="*.css"
BUILD_DIR="."

MEDIAS_DIR="$specificProjectParam.medias"
SRC_ROOT_PATH="src.erana/$specificProjectParam"


BUNDLE_OUT_DIR="./out"
BUNDLE_OUT_SPECIFIC_DIR="$BUNDLE_OUT_DIR/$specificProjectParam"

# /////////////// End of Config ///////////////


if [ -z "$deployMode"  ]; then
	echo "No deploy mode specified, assuming local."
	deployMode="local"
	
else

	if [ "$deployMode" = "bundle" ]; then
		deployMode="bundle"
	fi

fi




echo "********************* Starting erana deploy... *********************"

echo ""
echo "	0] Compiling project «$specificProjectParam»..."
echo ""
sh compile.sh $specificProjectParam

echo ""
echo "	1] Deploying : (mode=$deployMode)..."
echo ""

if [ "$deployMode" = "local" ]; then

	echo "			1.1 Copying code to local server:";


	rsync -r --exclude '*.git*' --exclude '*_OLD*' --exclude '*_BAD*' ../$SRC_DIR/$HTML_INDEX_FILENAME $LOCAL_DEPLOY_DIR
	rsync -r --exclude '*.git*' --exclude '*_OLD*' --exclude '*_BAD*' ../$SRC_DIR/$CSS_FILES $LOCAL_DEPLOY_DIR
	rsync -r --exclude '*.git*' --exclude '*_OLD*' --exclude '*_BAD*' $BUILD_DIR/$COMPILED_JS_FILENAME $LOCAL_DEPLOY_DIR
	rsync -r --exclude '*.git*' --exclude '*_OLD*' --exclude '*_BAD*' $BUILD_DIR/$COMPILED_COMMONS_JS_FILENAME $LOCAL_DEPLOY_DIR
	rsync -r --exclude '*.git*' --exclude '*_OLD*' --exclude '*_BAD*' $BUILD_DIR/$FAVICON_FILENAME $LOCAL_DEPLOY_DIR
	
	
	
else



	if [ "$deployMode" = "bundle" ]; then
	
	
		echo "			1.1 Copying code to bundle dir «$BUNDLE_OUT_SPECIFIC_DIR/»:";
		rsync -r --exclude '*.git*' --exclude '*_OLD*' --exclude '*_BAD*' ../$SRC_DIR/$HTML_INDEX_FILENAME $BUNDLE_OUT_SPECIFIC_DIR/
		rsync -r --exclude '*.git*' --exclude '*_OLD*' --exclude '*_BAD*' ../$SRC_DIR/$CSS_FILES $BUNDLE_OUT_SPECIFIC_DIR/
		rsync -r --exclude '*.git*' --exclude '*_OLD*' --exclude '*_BAD*' $BUILD_DIR/$COMPILED_JS_FILENAME $BUNDLE_OUT_SPECIFIC_DIR/
		rsync -r --exclude '*.git*' --exclude '*_OLD*' --exclude '*_BAD*' $BUILD_DIR/$COMPILED_COMMONS_JS_FILENAME $BUNDLE_OUT_SPECIFIC_DIR/
		rsync -r --exclude '*.git*' --exclude '*_OLD*' --exclude '*_BAD*' $BUILD_DIR/$FAVICON_FILENAME $BUNDLE_OUT_SPECIFIC_DIR/
		
	
		if [ "$isMedias" = "medias" ]; then
	
			echo "			1.2 Copying medias to bundle dir «$BUNDLE_OUT_SPECIFIC_DIR/»:";
		
			rsync -r --exclude '*.git*' --exclude '*_OLD*' --exclude '*_BAD*' ../../eranaMedias/$MEDIAS_DIR/universe $BUNDLE_OUT_SPECIFIC_DIR/$MEDIAS_DIR/
			rsync -r --exclude '*.git*' --exclude '*_OLD*' --exclude '*_BAD*' ../../eranaMedias/$MEDIAS_DIR/ui $BUNDLE_OUT_SPECIFIC_DIR/$MEDIAS_DIR/
			rsync -r --exclude '*.git*' --exclude '*_OLD*' --exclude '*_BAD*' ../../eranaMedias/$MEDIAS_DIR/audio $BUNDLE_OUT_SPECIFIC_DIR/$MEDIAS_DIR/
			
		fi
		
		
		echo "			1.3 Ziping all files in «$specificProjectParam.zip»:";
		
		cd $BUNDLE_OUT_DIR/$specificProjectParam/
		zip -r ../$specificProjectParam.zip *
		cd ../../
		
		
		
	else
	
		echo "			1.1 Copying code to remote server:";
	
		scp ./$COMPILED_JS_FILENAME $SSH_LOGIN@$WEB_SERVER:$REMOTE_PUBLIC_WEB_HOME/
		scp ./$COMPILED_COMMONS_JS_FILENAME $SSH_LOGIN@$WEB_SERVER:$REMOTE_PUBLIC_WEB_HOME/
		scp ../$SRC_ROOT_PATH/$HTML_INDEX_FILENAME $SSH_LOGIN@$WEB_SERVER:$REMOTE_PUBLIC_WEB_HOME/
		scp ../$SRC_ROOT_PATH/$CSS_FILES $SSH_LOGIN@$WEB_SERVER:$REMOTE_PUBLIC_WEB_HOME/
		
		
		
		if [ "$isMedias" = "medias" ]; then
		
			echo "			1.2 Copying medias to remote server:";
		
			echo "MEDIAS SYNCHRONISATION :"
		
			#scp -r ../../eranaMedias/$MEDIAS_DIR/universe $SSH_LOGIN@$WEB_SERVER:$REMOTE_PUBLIC_WEB_HOME/$MEDIAS_DIR
			#scp -r ../../eranaMedias/$MEDIAS_DIR/ui $SSH_LOGIN@$WEB_SERVER:$REMOTE_PUBLIC_WEB_HOME/$MEDIAS_DIR
			#scp -r ../../eranaMedias/$MEDIAS_DIR/audio $SSH_LOGIN@$WEB_SERVER:$REMOTE_PUBLIC_WEB_HOME/$MEDIAS_DIR
				
			rsync -r --exclude '*.git*' --exclude '*_OLD*' --exclude '*_BAD*' ../../eranaMedias/$MEDIAS_DIR/universe $SSH_LOGIN@$WEB_SERVER:$REMOTE_PUBLIC_WEB_HOME/$MEDIAS_DIR
			rsync -r --exclude '*.git*' --exclude '*_OLD*' --exclude '*_BAD*' ../../eranaMedias/$MEDIAS_DIR/ui $SSH_LOGIN@$WEB_SERVER:$REMOTE_PUBLIC_WEB_HOME/$MEDIAS_DIR
			rsync -r --exclude '*.git*' --exclude '*_OLD*' --exclude '*_BAD*' ../../eranaMedias/$MEDIAS_DIR/audio $SSH_LOGIN@$WEB_SERVER:$REMOTE_PUBLIC_WEB_HOME/$MEDIAS_DIR
		
		
		fi
	fi
fi




echo "********************* End of deploy. *********************"


