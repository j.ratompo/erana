echo "USAGE : sh refresh.sh <projectName>"

specificProjectParam="$1"
if [ -z "$specificProjectParam"  ]; then
	echo "Usage : 	sh deploy.sh <game project name> [<deploy mode>"
	exit 0;
fi

medias="$2"

echo "Refreshing project «$specificProjectParam»..."

echo "CODE SYNCHRONISATION :"
##sudo sh deploy.sh $specificProjectParam
##sudo chmod -R 777 /var/www/html/erana/

sh deploy.sh $specificProjectParam

## chmod -R 777 /var/www/html/erana/
if [ "$medias" = "medias" ]; then
	MEDIAS_DIR_VARIANT="$3"
	if [ -n "$MEDIAS_DIR_VARIANT"  ]; then
		MEDIAS_DIR_VARIANT="${MEDIAS_DIR_VARIANT}."
	fi
	
	MEDIAS_DIR="$specificProjectParam.${MEDIAS_DIR_VARIANT}medias"
	echo "MEDIAS SYNCHRONISATION :"
#	sudo rsync -r --exclude '*.git*' --exclude '*_OLD*' ../../eranaMedias/ /var/www/html/erana/
	# !!! CAUTION !!!
##	sudo rm -rf "/var/www/html/erana/$MEDIAS_DIR"
##	sudo mkdir "/var/www/html/erana/$MEDIAS_DIR"
##	sudo mkdir "/var/www/html/erana/$MEDIAS_DIR/universe"
##	sudo cp -r "../../eranaMedias/$MEDIAS_DIR/universe" "/var/www/html/erana/$MEDIAS_DIR/"
##	sudo mkdir "/var/www/html/erana/$MEDIAS_DIR/ui"
##	sudo cp -r "../../eranaMedias/$MEDIAS_DIR/ui" "/var/www/html/erana/$MEDIAS_DIR/"
##	sudo mkdir "/var/www/html/erana/$MEDIAS_DIR/audio"
##	sudo cp -r "../../eranaMedias/$MEDIAS_DIR/audio" "/var/www/html/erana/$MEDIAS_DIR/"

	rm -rf "/var/www/html/erana/$MEDIAS_DIR"
	mkdir "/var/www/html/erana/$MEDIAS_DIR"
	mkdir "/var/www/html/erana/$MEDIAS_DIR/universe"
	cp -rf "../../eranaMedias/$MEDIAS_DIR/universe" "/var/www/html/erana/$MEDIAS_DIR/"
	mkdir "/var/www/html/erana/$MEDIAS_DIR/ui"
	cp -rf "../../eranaMedias/$MEDIAS_DIR/ui" "/var/www/html/erana/$MEDIAS_DIR/"
	mkdir "/var/www/html/erana/$MEDIAS_DIR/audio"
	cp -rf "../../eranaMedias/$MEDIAS_DIR/audio" "/var/www/html/erana/$MEDIAS_DIR/"
	mkdir "/var/www/html/erana/$MEDIAS_DIR/config"
	cp -rf "../../eranaMedias/$MEDIAS_DIR/config" "/var/www/html/erana/$MEDIAS_DIR/"

	cp -rf "./favicon.ico" "/var/www/html/"

fi


# echo "WORKAROUND : Quick update for aotrautils.build.js in node_modules directory..."
# cp -f ../../aotra/build/aotrautils/aotrautils.build.js node_modules/aotrautils/
# echo "WORKAROUND : Quick update for aotrautils.build.js in node_modules directory done."


echo ""
echo "Refresh done."
