###################################################################################################

# Name
«erana» : erana means «space» in Malagasy.

# Abstract
Erana is a MMORPG action-game framework that provides all the mechanics and infrastructure in HTML5 / NodeJS.
All the assets are segreggated and made to allow the easiest customization of the game experience.

# Résumé (French)
Erana iasana signifie «univers» en malgache...

# Author
Jeremie Ratomposon <info@alqemia.com>

# Official website
https://alqemia.com/#erana

###################################################################################################

# Sub-projects :

erana.fanahe 	(~=«soul, awareness») : An abstract game where you represent a field of awareness
erana.lanitse 	(~=«cosmos») : A game about space exploration which story is collaboratively written


###################################################################################################

# Internal libraries used :

aotrautils-gaming.js	: 	(library to help games development)		https://alqemia.com			HGPL


###################################################################################################

# Third-party libraries used :

Strapjs : (some OOP concepts in javascript)			http://strapjs.org						MIT License
Paperjs : (canvas graphics)								http://paperjs.org						MIT License
Box2d-html5 : (2D rigid objects physical engine)	https://github.com/flyover/box2d.js	zlib License
(cf. http://otoro.net/paperbox)

###################################################################################################